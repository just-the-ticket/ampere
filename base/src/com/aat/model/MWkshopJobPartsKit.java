package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MProject;
import org.compiere.model.MStorage;
import org.compiere.util.Env;

public class MWkshopJobPartsKit extends X_JTT_WkshopJobPartsKit {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8336408492424985164L;

	public MWkshopJobPartsKit(Properties ctx, int JTT_WkshopJobPartsKit_ID, String trxName) {
		super(ctx, JTT_WkshopJobPartsKit_ID, trxName);

	}

	public MWkshopJobPartsKit(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public MWkshopJobPartsKit(MVMSvcSchedule schedule, MProject workshopJob, int m_product_id)
	{
		super(schedule.getCtx(), 0, schedule.get_TrxName());
		
		setAD_Client_ID(workshopJob.getAD_Client_ID());
		setAD_Org_ID(workshopJob.getAD_Org_ID());
		setJTT_VMPlanSchedule_ID(schedule.getJTT_VMPlanSchedule_ID());
		setC_Project_ID(workshopJob.getC_Project_ID());
		setM_Product_ID(m_product_id);
		setA_Asset_ID(workshopJob.get_ValueAsInt("A_Asset_ID"));
		setQty(Env.ONE);
		int M_Locator_ID = MStorage.getM_Locator_ID (workshopJob.getM_Warehouse_ID(),
				getM_Product_ID(), 0,
				Env.ONE, null);
		if (M_Locator_ID == 0) {
			M_Locator_ID = MProduct.get(schedule.getCtx(), m_product_id).getM_Locator_ID();
		}
		setM_Locator_ID(M_Locator_ID);
		setProcessed(false);
		
	}
	
	public static void assignPartsKit(MVMSvcSchedule schedule, MProject workshopJob)
	{
		MProduct product  = (MProduct) schedule.getJTT_VMPlanSchedule().getM_Product();
		MProductBOM[] boms = MProductBOM.getBOMLines(product);
		
		for (MProductBOM bom : boms) {
			MWkshopJobPartsKit kit = new MWkshopJobPartsKit(schedule, workshopJob, bom.getM_ProductBOM_ID());
			kit.setQty(bom.getBOMQty());
			kit.save();
			
		}
		
	}
}
