package com.aat.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.Query;

/**
 * Private Coach Charter rates header
 * @author jtrinidad
 *
 */
public class MPCCRHeader extends X_JTT_PCCRHeader {

	public MPCCRHeader(Properties ctx, int JTT_PCCRHeader_ID, String trxName) {
		super(ctx, JTT_PCCRHeader_ID, trxName);
	}

	public MPCCRHeader(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	
	public MPCCRLine[] getLines()
	{
		StringBuffer whereClauseFinal = new StringBuffer(MPCCRLine.COLUMNNAME_JTT_PCCRHeader_ID+"=? ");
		List<MPCCRLine> list = new Query(getCtx()
				, MPCCRLine.Table_Name
				, whereClauseFinal.toString(), get_TrxName())
				.setParameters(get_ID())
				.setOrderBy(MPCCRLine.COLUMNNAME_Value)
				.list();
		
		return list.toArray(new MPCCRLine[list.size()]);
	}
}
