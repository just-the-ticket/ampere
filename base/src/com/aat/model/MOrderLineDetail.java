package com.aat.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;

import com.aat.process.CreateDetailLineForAdjustmentAmt;


/**
 * 
 * @author jtrinidad
 *
 */
public class MOrderLineDetail extends X_C_OrderLine_Detail {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8546463847882029012L;

	public MOrderLineDetail(Properties ctx, int C_OrderLine_Detail_ID, String trxName) {
		super(ctx, C_OrderLine_Detail_ID, trxName);

	}

	public MOrderLineDetail(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	
	public static MOrderLineDetail[] getOrderLineDetails(Properties ctx, int C_OrderLine_ID, String trxName)
	{
		
		List<MOrderLineDetail> list = new Query(ctx, MOrderLineDetail.Table_Name
				, "C_OrderLine_ID = ?", trxName)
				.setParameters(C_OrderLine_ID)
				.setOrderBy("C_OrderLine_Detail_ID ASC")
				.list();
		
		return list.toArray(new MOrderLineDetail[list.size()]);
	}
	
	public static void delete(Properties ctx, int C_OrderLine_ID, String trxName)
	{
		
		List<MOrderLineDetail> list = new Query(ctx, MOrderLineDetail.Table_Name
				, "C_OrderLine_ID = ?", trxName)
				.setParameters(C_OrderLine_ID)
				.setOrderBy("C_OrderLine_Detail_ID ASC")
				.list();
		
		for (MOrderLineDetail detail : list) {
			detail.delete(true);
		}
		return;
	}
	
	/**
	 * Replace the product with another product of the same carrier product
	 * based on the new vehicle size and depot
	 * Replace the price as well
	 * @param vs - Vehicle Size of the new quote
	 * @param M_Warehouse_ID - Depot
	 * @param M_PriceList_Version_ID 
	 * @throws AdempiereUserError 
	 */
	
	public void setProduct(MVehicleSize vs, int M_Warehouse_ID, int M_PriceList_Version_ID) throws AdempiereUserError
	{
		if (getM_Product_ID() == 0) {
			return;
		}
		
		if (getM_Product().getCarrier_Product_ID()  == 0) {
			//possiby lump sump - skip this
			return;
		}
		MProduct carrier = (MProduct) getM_Product().getCarrier_Product();
		
		//replace product with the same carrier product
		MProduct product = MProduct.getMProduct(getCtx(), M_Warehouse_ID, vs, carrier);
		
		//replace with the new product
		setM_Product_ID(product.getM_Product_ID());
		BigDecimal newPrice = null;
		MProductPrice pp = MProductPrice.get(getCtx(), M_PriceList_Version_ID, getM_Product_ID(), get_TrxName());
		if (pp != null) {
			newPrice = pp.getPriceList();
			setPriceEntered(newPrice);
//		setLineNetAmt(LineNetAmt);
			setLineNetAmt(getQty().multiply(getPriceEntered()));
			save();
		} else {
			throw new AdempiereUserError("No Price set for " + product.getName());
		}
		
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		if (is_ValueChanged(COLUMNNAME_Description)) {
			// no need to update orderline amt
			return super.afterSave(newRecord, success);
		}
		//update order line
		MOrderLine orderLine = (MOrderLine) getC_OrderLine();
		
		MOrderLineDetail details[] = MOrderLineDetail.getOrderLineDetails(getCtx(), getC_OrderLine_ID(), get_TrxName());
		
		BigDecimal amt = Env.ZERO;
		for (MOrderLineDetail detail : details) {
			amt = amt.add(detail.getLineNetAmt());
		}

		orderLine.setPrice(amt);
		orderLine.setLineNetAmt(amt);
		orderLine.save();  //header should be updated after this
		
		return super.afterSave(newRecord, success);
	}

	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		int lsProduct_ID = MSysConfig.getIntValue("PRODUCT_LUMPSUMP", 0, getAD_Client_ID());

		if(getM_Product_ID()==lsProduct_ID && is_ValueChanged(COLUMNNAME_Qty))
		{
			setQty(Env.ONE);
			setPriceEntered(getPriceEntered());
			setLineNetAmt(getPriceEntered().multiply(getQty()));
		}
		return true;	
	}
}
