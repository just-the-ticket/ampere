package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * Private Coach Charter rates line
 * @author jtrinidad
 *
 */
public class MPCCRLine extends X_JTT_PCCRLine {

	public MPCCRLine(Properties ctx, int JTT_PCCRLine_ID, String trxName) {
		super(ctx, JTT_PCCRLine_ID, trxName);
	}

	public MPCCRLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public MPCCRLine(MPCCRHeader header)
	{
		this(header.getCtx(), 0, header.get_TrxName());
		super.setClientOrg(header);
		setJTT_PCCRHeader_ID(header.getJTT_PCCRHeader_ID());
	}
}
