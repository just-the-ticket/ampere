package com.aat.model;

import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProductCategory;
import org.compiere.model.MStorage;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;

public class MFuelIssue {

	/**	Logger							*/
	protected static CLogger			s_log = CLogger.getCLogger (MFuelIssue.class);

	public int doctype_fuelissue_id = 1000038;
	
	public static void createIssueFromReceipt(MInOut receipt)
	{
		int doctype_fuelissue_id = MSysConfig.getIntValue("DOCTYPE_FUEL_ISSUE_ID", 0, receipt.getAD_Client_ID());
		
		if (doctype_fuelissue_id == 0) {
			s_log.severe("DOCTYPE_FUEL_ISSUE_ID is not defined in configurator");
		}
		
		MInOutLine[] lines = receipt.getLines(true);
		
		MInventory inv = new MInventory((MWarehouse) receipt.getM_Warehouse(), receipt.get_TrxName());
		
		inv.setMovementDate(receipt.getMovementDate());
		inv.set_ValueOfColumn("UserElement1_ID", receipt.getM_Warehouse_ID());
		inv.setC_DocType_ID(doctype_fuelissue_id);
		inv.save();

		for (MInOutLine ioLine : lines)
		{
			if (ioLine.getM_Product_ID() == 0) {
				continue;
			}
				
			MInventoryLine line = new MInventoryLine(receipt.getCtx(), 0, receipt.get_TrxName());
			MOrderLine olLine = (MOrderLine) ioLine.getC_OrderLine();
			
			line.setAD_Client_ID(inv.getAD_Client_ID());
			line.setAD_Org_ID(inv.getAD_Org_ID());
			line.setM_Inventory_ID(inv.getM_Inventory_ID());
			
			line.set_ValueOfColumn("A_Asset_ID", olLine.get_ValueAsInt("UserElement2_ID"));
			line.set_ValueOfColumn("M_Warehouse_ID", inv.getM_Warehouse_ID());
			line.setM_Product_ID(ioLine.getM_Product_ID());
			line.setM_Locator_ID(ioLine.getM_Locator_ID());

			line.set_ValueOfColumn("UserElement1_ID", olLine.get_ValueAsInt("UserElement1_ID"));
			line.set_ValueOfColumn("UserElement2_ID", olLine.get_ValueAsInt("UserElement2_ID"));
			line.setQtyInternalUse(ioLine.getQtyEntered());
			MProductCategory productCategory = (MProductCategory) line.getM_Product().getM_Product_Category();

			int chargeID = productCategory.get_ValueAsInt("C_Charge_ID");
			line.setC_Charge_ID(chargeID);
			line.set_ValueOfColumn("C_OrderLine_ID", olLine.get_ID());
			line.save();
		}
	}
}
