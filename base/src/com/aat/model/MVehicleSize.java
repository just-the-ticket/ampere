package com.aat.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.pdfbox.contentstream.operator.text.SetCharSpacing;
import org.compiere.model.I_M_Locator;
import org.compiere.model.MLocator;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.CCache;
import org.compiere.util.Env;


/**
 * 
 * @author jtrinidad
 *
 */
public class MVehicleSize extends X_JTT_VehicleSize {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8546463847882029012L;

	private static CCache<Integer,MVehicleSize> s_cache = new CCache<Integer,MVehicleSize>("JTT_VehicleSize", 50 );	
	
	public MVehicleSize(Properties ctx, int JTT_VehicleSize_ID, String trxName) {
		super(ctx, JTT_VehicleSize_ID, trxName);

	}

	public MVehicleSize(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public static MVehicleSize get(int JTT_VehicleSize_ID)
	{
		setCache();
		
		return s_cache.get(JTT_VehicleSize_ID);
	}
	
	public static MVehicleSize get(String value)
	{
		setCache();
		
		for (Map.Entry<Integer, MVehicleSize> entry : s_cache.entrySet()) {
			if (entry.getValue().getValue().equals(value)) {
				return entry.getValue();
			}
		}
		return null;
		
	}
	
	public static MVehicleSize getSize(int passengers)
	{
		setCache();
		
		for (Map.Entry<Integer, MVehicleSize> entry : s_cache.entrySet()) {
			if (entry.getValue().getSeatsMax() >= passengers ) {
				return entry.getValue();
			}
		}
		return null;
		
	}
	

	private static void setCache()
	{
		if (s_cache.size() == 0) {
			List<MVehicleSize> list = new Query(Env.getCtx(), MVehicleSize.Table_Name, "AD_Client_ID=?", null)
					.setParameters(Env.getAD_Client_ID(Env.getCtx()))
					.setOnlyActiveRecords(true)
					.setOrderBy("seatsmax")
					.list();
			
			for (MVehicleSize vs:list) {
				s_cache.put(vs.get_ID(), vs);
			}
		}
	}
	
}
