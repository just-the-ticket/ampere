package com.aat.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MProduct;
import org.compiere.util.CLogger;
import org.compiere.util.DB;


/**
 * @author jtrinidad
 *
 */
public class MVMVehicle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7321876191343908001L;

	/**	Logger							*/
	protected static CLogger			s_log = CLogger.getCLogger (MVMVehicle.class);
	
	/**
	 * creates the next 300K km rounded to 50K
	 */
	private static final String SQL_VEHICLE_SCHEDULE =
			"select vs.name, s1.jtt_vmplan_id, s1.intervalkm, s1.warningkm, s1.cumintervalkm, s1.altgroup, s1.jtt_vmplanschedule_id \n" +
            "from \n" +
            "( \n" +
            "select b.jtt_vmplan_id \n" +
            "  , max(b.intervalkm) as intervalkm \n" +
            "  , max(b.warningkm) as warningkm \n" +
            "  , b.cumintervalkm, b.altgroup \n" +
            ", max(b.jtt_vmplanschedule_id) as jtt_vmplanschedule_id \n" +
            "from ( \n" +
            "select \n" +
            "      a.jtt_vmplan_id , a.ad_client_id, a.ad_org_id, a.isactive, a.created, a.createdby, a.updated, a.updatedby \n" +
            "    , a.intervalkm, a.cumintervalkm, (a.cumintervalkm-warningkm) as warningkm, a.altgroup \n" +
            "    , first_value(a.jtt_vmplanschedule_id) over (partition by a.jtt_vmplan_id, a.altgroup, a.cumintervalkm order by a.intervalkm desc) as jtt_vmplanschedule_id \n" +
            "from \n" +
            "( \n" +
            "select t.jtt_vmplan_id, p.ad_client_id, p.ad_org_id, p.isactive, p.created, p.createdby, p.updated, p.updatedby \n" +
            ",  t.jtt_vmplanschedule_id, t.intervalkm, t.altgroup, t.warningkm, gs.cumintervalkm \n" +
            "from jtt_vmplanschedule t \n" +
            "inner join jtt_vmplan p on t.jtt_vmplan_id = p.jtt_vmplan_id \n" +
            ", lateral (select generate_series(0, (select (max (kmreading)/50000)::int*50000 + 300000  from jtt_vmvehreading  where A_Asset_ID=?) , t.intervalkm) cumintervalkm) as gs \n" +
            "where t.intervalkm > 0 \n" +
            ") a \n" +
            ") b \n" +
            "--where b.km > 200000 \n" +
            "group by b.jtt_vmplan_id, b.ad_client_id, b.ad_org_id, b.isactive, b.created, b.createdby, b.updated, b.updatedby, b.cumintervalkm, b.altgroup \n" +
            "order by b.cumintervalkm \n" +
            ") s1 \n" +
            "inner join jtt_vmplan p on s1.jtt_vmplan_id = p.jtt_vmplan_id \n" +
            "inner join jtt_vmplanschedule vs on vs.jtt_vmplanschedule_id = s1.jtt_vmplanschedule_id \n" +
            "inner join  JTT_VMPlanAssignment pa on p.jtt_vmplan_id = pa.jtt_vmplan_id \n" +
            "--left join JTT_VMSvcSchedule  ssv on ssv.a_asset_id = pa.a_asset_id and s1.jtt_vmplanschedule_id = ssv.jtt_vmplanschedule_id \n" +
            "where  pa.A_Asset_ID=? \n" +
            "and s1.cumintervalkm > (select max (kmreading) from jtt_vmvehreading where a_asset_id = pa.A_Asset_ID) \n" +
            "and not exists (select 1 from JTT_VMSvcSchedule \n" + 
            "		where a_asset_id = pa.a_asset_id and jtt_vmplanschedule_id = s1.jtt_vmplanschedule_id and bookletkmdue = s1.cumintervalkm) "
            ;
	
	
	
	public static int createSvcSchedule(MAsset asset)
	{
		
		int counter = 0;
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (SQL_VEHICLE_SCHEDULE, null);
			pstmt.setInt (1, asset.get_ID());
			pstmt.setInt (2, asset.get_ID());
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ())
			{
				MVMSvcSchedule s = new MVMSvcSchedule(asset.getCtx(), 0, asset.get_TrxName());
				s.setAD_Client_ID(asset.getAD_Client_ID());
				s.setAD_Org_ID(0);
				s.setA_Asset_ID(asset.getA_Asset_ID());
				
				s.setJTT_VMPlanSchedule_ID(rs.getInt("jtt_vmplanschedule_id"));
				s.setWarningKM(rs.getInt("warningkm"));
				s.setExpiryKM(rs.getInt("cumintervalkm"));
				s.setBookletKMDue(rs.getInt("cumintervalkm"));
				s.setKMReading(0);   //not serviced
				s.setProcessed(false);
				s.save();
				counter++;
			}
			rs.close ();
			pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			s_log.log (Level.SEVERE, SQL_VEHICLE_SCHEDULE, e);
		}
		
		return counter;
	}
}
