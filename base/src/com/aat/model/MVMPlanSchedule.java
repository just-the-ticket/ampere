package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MVMPlanSchedule extends X_JTT_VMPlanSchedule {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5711007199435925949L;

	public MVMPlanSchedule(Properties ctx, int JTT_VMPlanSchedule_ID, String trxName) {
		super(ctx, JTT_VMPlanSchedule_ID, trxName);
	}

	public MVMPlanSchedule(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
