package com.aat.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;

import org.compiere.model.I_C_OrderLine;
import org.compiere.model.MBPartner;
import org.compiere.model.MClient;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProject;
import org.compiere.model.MResource;
import org.compiere.model.MResourceAssignment;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

public class JTTValidator implements ModelValidator {

    /** Logger */
    private static CLogger log = CLogger.getCLogger(JTTValidator.class);
    /** Client */
    private int m_AD_Client_ID = -1;
    
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
        if (client != null)
        {
            m_AD_Client_ID = client.getAD_Client_ID();
            log.info(client.toString());
        }
        else
        {
            log.info("Initializing global validator: " + this.toString());
        }
        
        engine.addModelChange(MOrder.Table_Name, this);
        engine.addModelChange(MOrderLine.Table_Name, this);
        engine.addModelChange(MProject.Table_Name, this);
        engine.addModelChange(X_S_ResourceTypeBP.Table_Name, this);
        
        //Workflow validator
        engine.addDocValidate(MOrder.Table_Name, this);

	}

	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		if (po instanceof MOrder) {
			if (type == TYPE_CHANGE) {
//				if (po.is_ValueChanged(MOrder.COLUMNNAME_PackagedPrice)
//						|| po.is_ValueChanged(MOrder.COLUMNNAME_NoOfPassenger)) {
//					log.saveWarning("PubPricechange", "");
//				}
				if (po.is_ValueChanged("JTT_VehicleSize_ID")) {
					String error = updateVehicleSize((MOrder) po);
					return error;
				} 
				if (po.is_ValueChanged("PSAdult")
						|| po.is_ValueChanged("PSChild")
						|| po.is_ValueChanged("PSEscort")) {
					String error = updateAEF((MOrder) po);
					return error;
				}
					
			} else if (type == TYPE_BEFORE_DELETE) {
				//deleting drafted PO - remove reference to this PO
				MOrder order = (MOrder) po;
				if (order.isProcessed())
					return null; // do not do anything
				
				if (!order.isSOTrx()) {
					List<MOrderLine> lines = new Query(order.getCtx()
							, I_C_OrderLine.Table_Name, MOrderLine.COLUMNNAME_C_Order_ID+"=? ".toString(), null)
							.setParameters(po.get_ID())
							.list();
					
					for (MOrderLine line : lines) {
						MOrderLine sol = (MOrderLine) line.getLink_OrderLine();
						sol.setLink_OrderLine_ID(0);
						sol.save();
					}
				}
			}
		} else if (po instanceof MOrderLine) {
			if (type == TYPE_CHANGE) {
				if (po.is_ValueChanged("datetripstart")
						|| po.is_ValueChanged("datetripend")) {
					String error = updateResourceAssignment((MOrderLine) po);
					return error;
				}
			}
		} else if (po instanceof MResourceTypeBP) {
			if (type == TYPE_AFTER_NEW) {
				MResourceTypeBP rbp = (MResourceTypeBP) po;
				MBPartner bp = (MBPartner) rbp.getC_BPartner();
				MResource res = MResource.get(bp, rbp.getS_ResourceType_ID(), true);
				if (res != null) {
					rbp.setM_Product_ID(res.getProduct().getM_Product_ID());
					rbp.save();
				}
				 
			}
		} else if (po instanceof MProject) {
			if (type == TYPE_BEFORE_DELETE) {
				MProject project = (MProject) po;
				if (project.isProcessed()) {
					return "Document Closed; " + project.getValue();
				}

				if (MProject.PROJECTCATEGORY_WorkOrderJob.equals(project.getProjectCategory())) {
					MWorkshopJob.beforeDeleteWorkshopJob(project);
				}
						
			}
		}
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {
		if (po instanceof MOrder) {
			if (timing == TIMING_AFTER_PREPARE) {
				String error = updateTripDate((MOrder) po);
				return error;

			} else if (timing == TIMING_BEFORE_PREPARE) {
//				String error = setPackageTour((MOrder) po);
//				return error;
			}

		}
		return null;
	}

	/**
	 * Update quote price as vehicle size of the quote changed, thus price
	 * for each individual order details could be affected
	 * @param order
	 * @return
	 */
	private String updateVehicleSize(MOrder order)
	{


		if (order.get_ValueAsInt("JTT_VehicleSize_ID") == 0) {
			//do not update when no vehicle size set
			return null;
		}
		MOrderLine lines[] = order.getLines();
		Properties ctx = order.getCtx();
		String trxName = order.get_TrxName();
		
		MPriceListVersion plv = ((MPriceList) order.getM_PriceList()).getPriceListVersion(order.getDateTripStart());
		for (MOrderLine line : lines)
		{
			MOrderLineDetail[] details = MOrderLineDetail.getOrderLineDetails(ctx, line.getC_OrderLine_ID(), trxName);
		
			BigDecimal totalAmt = Env.ZERO;
			//change product based on new vehicle size
			for (MOrderLineDetail detail : details) {
				MVehicleSize vs = new MVehicleSize(ctx, order.get_ValueAsInt("JTT_VehicleSize_ID"), trxName);
				try {
					detail.setProduct(vs, line.getM_Warehouse_ID(),plv.getM_PriceList_Version_ID());  //use depot from line
					
					totalAmt = totalAmt.add(detail.getLineNetAmt());
					
				} catch (AdempiereUserError e) {
					return e.getMessage();
				}
			}
			line.setPrice(totalAmt);
			line.setLineNetAmt(totalAmt);

			//update publish price
			MPCCRLine tour = new MPCCRLine(ctx, line.getJTT_PCCRLine_ID(), trxName);
			BigDecimal publishedPrice = (BigDecimal)tour.get_Value("VSR" + MVehicleSize.get(order.getJTT_VehicleSize_ID()).getValue());
			line.setPackagedPrice(publishedPrice);
			
			line.save();

		}
	
		order.updateLumpSump();
		
		return null;
	}
	
	private String updateAEF(MOrder order)
	{
	
		MOrderLine[] qlines = order.getLines();
		
		for (MOrderLine line : qlines) {
			if (line.getM_Product_ID() > 0) {
				String pstatus = line.getM_Product().getPsStatus();
				//AE&F has particular designation for adult, child and escort
				if (pstatus == null) {
					continue;
				}
				if (MProduct.PSSTATUS_PsAdult.equals(pstatus)) {
					line.setQty(new BigDecimal(order.getPSAdult()));
				} else if (MProduct.PSSTATUS_PsChild.equals(pstatus)) {
					line.setQty(new BigDecimal(order.getPSChild()));
				} else if (MProduct.PSSTATUS_PsEscort.equals(pstatus)) {
					line.setQty(new BigDecimal(order.getPSEscort()));
				} else {
					//null - do nothing
				}
				line.setQtyReserved(Env.ZERO);
				line.setLineNetAmt();
				line.save();
			}
		}
		
		return null;
	}
	
	private String updateResourceAssignment(MOrderLine line)
	{
		Timestamp start = line.getDateTripStart();
		Timestamp end = line.getDateTripEnd();
		MResourceAssignment[] ras = MResourceAssignment.getFromTripLeg(line);
		for (MResourceAssignment ra : ras) {
			ra.setAssignDateFrom(start);
			ra.setAssignDateTo(end);
			ra.save();
		}
		return null;
	}
	
	private String updateTripDate(MOrder order)
	{
		MOrderLine lines[] = order.getLines();
		
		Timestamp startTrip = null;
		Timestamp endTrip = null;
				
		for (MOrderLine line : lines) {
			//find the lowest value
			if (startTrip == null && line.getDateTripStart() != null) {
				startTrip = line.getDateTripStart();
			} else if (line.getDateTripStart() != null && startTrip.after(line.getDateTripStart()) ) {
				startTrip = line.getDateTripStart();
			}
			//find the highest value
			if (endTrip == null && line.getDateTripEnd() != null) {
				endTrip = line.getDateTripEnd();
			} else if (line.getDateTripEnd() != null && endTrip.before(line.getDateTripEnd())) {
				endTrip = line.getDateTripEnd();
			}

		}
		order.set_ValueOfColumn("datetripstart", startTrip);
		order.set_ValueOfColumn("datetripend", endTrip);
		return null;
	}
	
	
	/**
	 * Copy the Itinerary Leg from the selected tour package
	 * adjust order detail product to use based on selected vehicle size
	 * must ensure it doesn't copy twice by using seed_orderline_id as reference
	 * adjust price by adding a lump sump product on the last line - must check not to duplicate
	 * @param order
	 * @return
	 */
//	private String setPackageTour(MOrder order)
//	{
//		Properties ctx = order.getCtx();
//		String trxName = order.get_TrxName();
//		
//		int lsProduct_ID = MSysConfig.getIntValue("PRODUCT_LUMPSUMP", 0, order.getAD_Client_ID());
//		if (lsProduct_ID == 0) {
//			return "Lump Sump Product is not configured in SYSCONFIG. Please update value of PRODUCT_LUMPSUMP";
//		}
//
//		if (order.getJTT_PCCRLine_ID() == 0)
//			return null;
//		
//		MPriceListVersion plv = ((MPriceList) order.getM_PriceList()).getPriceListVersion(order.getDateTripStart());
//		MVehicleSize vs = MVehicleSize.get(order.getJTT_VehicleSize_ID());
//		MTax tax = null;
//
//		MOrderLine[] qlines = order.getLines();
//		
//		MPCCRLine tour = null;
//		boolean isTaxIncluded = false;
//		
//		MOrder seedOrder = null;
//		
//		//TODO - how do you copy packages from Package Tour - JTT_PCCRLine_ID is now in Order Line
//		Map<Integer, MOrderLine> mapLines = new HashMap<Integer, MOrderLine>();  //seed_orderline_id, orderline
//		for (MOrderLine qline : qlines) {
//			tour = new MPCCRLine(ctx, qline.getJTT_PCCRLine_ID(), trxName);
//			isTaxIncluded = tour.getJTT_PCCRHeader().isTaxIncluded();
//			seedOrder = (MOrder) tour.getC_Order();
//			
//			if (qline.getSeed_OrderLine_ID() > 0) {
//				mapLines.put(qline.getSeed_OrderLine_ID(), qline);
//			}
//		}
//		
//		
//		MOrderLine lines[] = seedOrder.getLines();
//		
//		BigDecimal total = Env.ZERO;
//		boolean justUpdate = false;
//		for (MOrderLine fromLine : lines) {
//		
//			MOrderLineDetail[] details = null;
//			MOrderLine newQuoteLine  = mapLines.get(fromLine.getC_OrderLine_ID());
//			if (newQuoteLine == null) {
//				newQuoteLine = new MOrderLine(ctx, 0, trxName);
//				PO.copyValues(fromLine, newQuoteLine);
//				newQuoteLine.setAD_Org_ID(order.getAD_Org_ID());
//				newQuoteLine.setC_Order_ID(order.getC_Order_ID());
//				newQuoteLine.setSeed_OrderLine_ID(fromLine.getC_OrderLine_ID());
//				newQuoteLine.setQtyReserved(Env.ZERO);  //shouldn't copy
//				newQuoteLine.save();
//				mapLines.put(newQuoteLine.getSeed_OrderLine_ID(), newQuoteLine);
//				details = MOrderLineDetail.getOrderLineDetails(ctx, fromLine.getC_OrderLine_ID(), trxName);
//			} else {
//				//pick up the existing one
//				details = MOrderLineDetail.getOrderLineDetails(ctx, newQuoteLine.getC_OrderLine_ID(), trxName);
//				justUpdate = true;
//				
//			}
//			
//			
//			BigDecimal lineTotal = Env.ZERO;
//			for (MOrderLineDetail detail : details)
//			{
//				MOrderLineDetail newDetail = null;
//				if (justUpdate) {
//					newDetail = detail;
//				} else {
//					newDetail = new MOrderLineDetail(ctx, 0, trxName);
//					PO.copyValues(detail, newDetail);
//					newDetail.setAD_Org_ID(order.getAD_Org_ID());
//					newDetail.setC_OrderLine_ID(newQuoteLine.getC_OrderLine_ID());
//					
//				}
//				
//				try {
//					newDetail.setProduct(vs, fromLine.getM_Warehouse_ID(), plv.getM_PriceList_Version_ID());
//					lineTotal = lineTotal.add(detail.getLineNetAmt());
//				} catch (AdempiereUserError e) {
//					return e.getMessage();
//				}
//				
//				newDetail.save();
//				
//			}
//			
//			newQuoteLine.setPrice(lineTotal);
//			newQuoteLine.setLineNetAmt(lineTotal);
//			
//			if (tax == null) {
//				tax = (MTax) newQuoteLine.getC_Tax();
//			}
//			total = total.add(newQuoteLine.getLineNetAmt());
//		}  //for (MOrderLine fromLine : lines) {
//		
//		//Perform the rounding by adjusting it to the lump sump line
//		BigDecimal packagePrice = order.getPackagedPrice();
//		
//		if (isTaxIncluded) {
//			packagePrice = packagePrice.divide((tax.getRate().add(Env.ONE)), 2, RoundingMode.HALF_UP);
//		}
//		MOrderLine lsl = null;  //lump sump orderline
//		qlines = order.getLines(true, "c_orderline_id");
//		for (MOrderLine line : qlines) {
//			if (line.getM_Product_ID() == lsProduct_ID) {
//				lsl = line;
//				break;
//			}
//		}
//		if (lsl == null) {
//			lsl = new MOrderLine(order);
//			lsl.setProduct(MProduct.get(order.getCtx(), lsProduct_ID));
//			lsl.setC_Tax_ID(tax.getC_Tax_ID());
//			lsl.setQty(Env.ONE);
//		}
//		BigDecimal diff = packagePrice.subtract(total);
//		lsl.setPrice(diff);
//		lsl.setLineNetAmt(diff);
//		lsl.save();
//		
//		//update A&EF
//		for (MOrderLine line : qlines) {
//			if (line.getM_Product_ID() > 0 && mapLines.get(line.getSeed_OrderLine_ID()) == null) {
//				String pstatus = line.getM_Product().getPsStatus();
//				if (MProduct.PSSTATUS_PsAdult.equals(pstatus)) {
//					line.setQty(new BigDecimal(order.getPSAdult()));
//				} else if (MProduct.PSSTATUS_PsChild.equals(pstatus)) {
//					line.setQty(new BigDecimal(order.getPSChild()));
//				} else if (MProduct.PSSTATUS_PsEscort.equals(pstatus)) {
//					line.setQty(new BigDecimal(order.getPSEscort()));
//				} else {
//					//null - do nothing
//				}
//				line.setQtyReserved(Env.ZERO);
//				line.setLineNetAmt();
//				line.save();
//			}
//		}
//		order.renumberLines(10);
//		
//		return null;
//	}
	
}
