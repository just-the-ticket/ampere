package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MVMPlanAssignment extends X_JTT_VMPlanAssignment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9040084217878948386L;

	public MVMPlanAssignment(Properties ctx, int JTT_VMPlanAssignment_ID, String trxName) {
		super(ctx, JTT_VMPlanAssignment_ID, trxName);
	}

	public MVMPlanAssignment(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public MVMPlanAssignment(MVMPlan plan)
	{
		this(plan.getCtx(), 0, plan.get_TrxName());
		setAD_Client_ID(plan.getAD_Client_ID());
		setAD_Org_ID(plan.getAD_Org_ID());
		setJTT_VMPlan_ID(plan.getJTT_VMPlan_ID());
		
		
	}

}
