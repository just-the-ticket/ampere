package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MVehicleDefect extends X_JTT_VehicleDefect {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4612881810994860335L;

	public MVehicleDefect(Properties ctx, int JTT_VehicleDefect_ID, String trxName) {
		super(ctx, JTT_VehicleDefect_ID, trxName);
	}

	public MVehicleDefect(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
