package com.aat.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.Query;

public class MVMPlan extends X_JTT_VMPlan {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5354907453070259608L;

	public MVMPlan(Properties ctx, int JTT_VMPlan_ID, String trxName) {
		super(ctx, JTT_VMPlan_ID, trxName);
	}

	public MVMPlan(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}


	public MVMPlanAssignment[] getAssignments ()
	{
		List<MVMPlanAssignment> list = new Query(getCtx(), MVMPlanAssignment.Table_Name, "JTT_VMPlan_ID=?", get_TrxName())
				.setParameters(get_ID())
				.list();
		
		return list.toArray(new MVMPlanAssignment[list.size()]);
	}	//	getLines

}
