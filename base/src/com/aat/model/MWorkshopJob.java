package com.aat.model;

import java.util.List;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MAsset;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProject;
import org.compiere.model.MStorage;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.TimeUtil;

public class MWorkshopJob {

	/**	Logger							*/
	protected static CLogger			s_log = CLogger.getCLogger (MWorkshopJob.class);
	

	public static MProject createWorkshopJob(MAsset asset, int kmReading)
	{
		MProject project = new MProject(asset.getCtx(), 0, asset.get_TrxName());
		project.setAD_Client_ID(asset.getAD_Client_ID());
		project.setAD_Org_ID(asset.getAD_Org_ID());
		project.setName(asset.getName());
		project.setProjectCategory(MProject.PROJECTCATEGORY_WorkOrderJob);
		project.set_ValueOfColumn("A_Asset_ID", asset.getA_Asset_ID());
		project.set_ValueOfColumn("M_Warehouse_ID", asset.get_ValueAsInt("schd_warehouse_id"));
		project.setDateContract(TimeUtil.getDay(null));
		project.set_ValueOfColumn("KMReading", kmReading);
		project.setC_Currency_ID(
				MAcctSchema.getClientAcctSchema(asset.getCtx(), asset.getAD_Client_ID())[0].getC_Currency_ID());
		project.save();
		
		return project;
		
	}
	
	public static void closeWorkshopJob(MProject project) throws Exception
	{
		List<MWrkshopJobSched> schedules = new Query(project.getCtx()
				, MWrkshopJobSched.Table_Name, "C_Project_ID = ?", project.get_TrxName())
				.setParameters(project.get_ID())
				.list();
		for (MWrkshopJobSched s : schedules) {
			MVMSvcSchedule svc = (MVMSvcSchedule) s.getJTT_VMSvcSchedule();
			svc.setProcessed(true);
			svc.setActualSvcDate(project.getDateContract());
			svc.setKMReading(project.get_ValueAsInt("KMReading"));
			svc.save();
			
			s.setProcessed(true);
			s.save();
		}
		
		List<MVehicleDefect> defects = new Query(project.getCtx()
				, MVehicleDefect.Table_Name, "C_Project_ID = ?", project.get_TrxName())
				.setParameters(project.get_ID())
				.list();		
		for (MVehicleDefect defect : defects) {
			if (defect.isDefectFixed()) {
				defect.setJTT_VehDefectFixedDate(project.getDateContract());
				defect.setProcessed(true);
			} else {
				defect.setC_Project_ID(0);
				defect.setJTT_VehDefectFixedBy(0);
				defect.setJTT_VehDefectFixedDate(null);
//				defect.setJTT_VehDefectFixedNotes(null);  - might need to save prior notes
				
			}
			defect.save();
		}
		
		
		// create internal use
		MInventory inv = null;
		List<MWkshopJobPartsKit> kits = new Query(project.getCtx()
				, MWkshopJobPartsKit.Table_Name, "C_Project_ID = ?", project.get_TrxName())
				.setParameters(project.get_ID())
				.list();
		
		for (MWkshopJobPartsKit kit : kits) {
			if (inv == null) {
				inv = new MInventory((MWarehouse) project.getM_Warehouse(), project.get_TrxName());
				inv.setC_DocType_ID(MSysConfig.getIntValue("PARTS_ISSUE_DOCTYPE", 0, project.getAD_Client_ID()));
				inv.setC_Project_ID(project.getC_Project_ID());
				inv.set_ValueOfColumn("A_Asset_ID", project.get_ValueAsInt("A_Asset_ID"));
				inv.setC_Project_ID(project.getC_Project_ID());
				String description = String.format("Parts issue for Workshop Job %s", project.getValue());
				inv.setDescription(description);
				inv.set_ValueOfColumn("UserElement1_ID", project.getM_Warehouse_ID());
				inv.set_ValueOfColumn("UserElement2_ID", project.get_ValueAsInt("A_Asset_ID"));
				inv.save();
			}
			
			MInventoryLine line = new MInventoryLine(project.getCtx(), 0, project.get_TrxName());
			line.setAD_Client_ID(inv.getAD_Client_ID());
			line.setAD_Org_ID(inv.getAD_Org_ID());
			line.setM_Inventory_ID(inv.getM_Inventory_ID());
			
			line.set_ValueOfColumn("A_Asset_ID", project.get_ValueAsInt("A_Asset_ID"));
			line.set_ValueOfColumn("M_Warehouse_ID", inv.getM_Warehouse_ID());
			line.setM_Product_ID(kit.getM_Product_ID());

			line.setM_Locator_ID(kit.getM_Locator_ID());

			MProductCategory pc = (MProductCategory) line.getM_Product().getM_Product_Category();
			line.setC_Charge_ID(pc.get_ValueAsInt("C_Charge_ID"));
			line.set_ValueOfColumn("UserElement1_ID", inv.getM_Warehouse_ID());
			line.set_ValueOfColumn("UserElement2_ID", project.get_ValueAsInt("A_Asset_ID"));
			line.setQtyInternalUse(kit.getQty());
			line.save();
			
			kit.setM_InventoryLine_ID(line.getM_InventoryLine_ID());
			kit.setProcessed(true);
			kit.save();
			
		}
		
		//not at this stage
//		if (inv != null) {
//			inv.setDocAction(MInventory.DOCACTION_Complete);
//			inv.completeIt();
//		}
		
	}
	
	public static void beforeDeleteWorkshopJob(MProject project)
	{
		//clear workshop job schedule
		//MWrkshopJobSched jobSchedule = new MWrkshopJobSched(schedule, workshopJob);
		List<MWrkshopJobSched> schedules = new Query(project.getCtx()
				, MWrkshopJobSched.Table_Name, "C_Project_ID = ?", project.get_TrxName())
				.setParameters(project.get_ID())
				.list();
		for (MWrkshopJobSched s : schedules) {
			MVMSvcSchedule svc = (MVMSvcSchedule) s.getJTT_VMSvcSchedule();
			svc.setKMReading(0);
			svc.setActualSvcDate(null);
			svc.save();
			
			s.delete(false);
		}
		
		// clear defects
		List<MVehicleDefect> defects = new Query(project.getCtx()
				, MVehicleDefect.Table_Name, "C_Project_ID = ?", project.get_TrxName())
				.setParameters(project.get_ID())
				.list();
		
		for (MVehicleDefect defect : defects) {
			defect.setC_Project_ID(0);
			defect.save();
		}
		//clear job parts kit
//		MWkshopJobPartsKit kit = new MWkshopJobPartsKit(schedule, workshopJob);
		
		List<MWkshopJobPartsKit> kits = new Query(project.getCtx()
				, MWkshopJobPartsKit.Table_Name, "C_Project_ID = ?", project.get_TrxName())
				.setParameters(project.get_ID())
				.list();
		
		for (MWkshopJobPartsKit kit : kits) {
			kit.delete(false);
		}
	}
	

}
