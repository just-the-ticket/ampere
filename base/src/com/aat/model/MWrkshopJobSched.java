package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MProject;

public class MWrkshopJobSched extends X_JTT_WrkshopJobSched {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1066266972608280268L;

	public MWrkshopJobSched(Properties ctx, int JTT_WrkshopJobSched_ID, String trxName) {
		super(ctx, JTT_WrkshopJobSched_ID, trxName);
	}

	public MWrkshopJobSched(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	
	public MWrkshopJobSched(MVMSvcSchedule schedule, MProject workshopJob)
	{
		super(schedule.getCtx(), 0, schedule.get_TrxName());
		
		setAD_Client_ID(workshopJob.getAD_Client_ID());
		setAD_Org_ID(workshopJob.getAD_Org_ID());
		setJTT_VMPlanSchedule_ID(schedule.getJTT_VMPlanSchedule_ID());
		setJTT_VMSvcSchedule_ID(schedule.get_ID());
		setC_Project_ID(workshopJob.getC_Project_ID());
		setProcessed(false);
		save();
		
	}
}
