package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MResourceTypeBP extends X_S_ResourceTypeBP {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1770738555758630190L;

	public MResourceTypeBP(Properties ctx, int S_ResourceTypeBP_ID, String trxName) {
		super(ctx, S_ResourceTypeBP_ID, trxName);
	}

	public MResourceTypeBP(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
