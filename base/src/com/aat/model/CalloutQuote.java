package com.aat.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.model.MOrderLine;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;


/**
 * Quote/Order related to Trip 
 * @author jtrinidad
 *
 */
public class CalloutQuote extends CalloutEngine {

	public CalloutQuote() {
	}
	
	/**
	 * Update Order Line trip end date to use start date to reduce
	 * key entry
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String  tripDate (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if (isCalloutActive() || value == null)
			return "";
		
		Timestamp startTrip = null;
		Timestamp endTrip = null;

		if (mField.getColumnName().equals(MOrderLine.COLUMNNAME_DateTripStart)) {
			startTrip = (Timestamp) value;
			endTrip = TimeUtil.addMinutess(startTrip, 15);
			mTab.setValue(MOrderLine.COLUMNNAME_DateTripEnd, endTrip);
		} else if (mField.getColumnName().equals(MOrderLine.COLUMNNAME_DateTripEnd)) {
			startTrip = (Timestamp) mTab.getValue(MOrderLine.COLUMNNAME_DateTripStart);
			endTrip = (Timestamp) value;
			BigDecimal time =  TimeUtil.getHoursBetween(startTrip, endTrip);
			mTab.setValue(MOrderLine.COLUMNNAME_JourneyTime, time);
		}
		return "";
	}
	
	/**
	 * Update line detail amt when user change qty/price
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String  amtOrderDetail (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if (isCalloutActive() || value == null)
			return "";
		
		BigDecimal qty = (BigDecimal)mTab.getValue("Qty");
		BigDecimal priceEntered = (BigDecimal)mTab.getValue("PriceEntered");
		BigDecimal LineNetAmt = qty.multiply(priceEntered);
		
		mTab.setValue("LineNetAmt", LineNetAmt);
		
		return "";
				
	}
	
	public String  noOfPassengers (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if (isCalloutActive() || value == null)
			return "";
		
		int adult, child, escort;
		
		adult = (int)mTab.getValue("psadult");
		child = (int)mTab.getValue("pschild");
		escort = (int)mTab.getValue("psescort");

		setRevenueClass(mTab);
		
		int noPassenger = adult + child + escort;
		mTab.setValue("noofpassenger", noPassenger);
		
		boolean isFixPrice = mTab.getValueAsBoolean("IsFixPrice");
		if (!isFixPrice) {
			return "";
		}
		return setVehicleSize(mTab, noPassenger);
	}

	
	private String setVehicleSize(GridTab mTab, int noPassenger)
	{
				
		
		if (MVehicleSize.get("MINI").getSeatsMax() >= noPassenger) {
			mTab.setValue("JTT_VehicleSize_ID", MVehicleSize.get("MINI").getJTT_VehicleSize_ID());
		} else if (MVehicleSize.get("MIDI").getSeatsMax() >= noPassenger) {
			mTab.setValue("JTT_VehicleSize_ID", MVehicleSize.get("MIDI").getJTT_VehicleSize_ID());
		} else if (MVehicleSize.get("L").getSeatsMax() >= noPassenger) {
			mTab.setValue("JTT_VehicleSize_ID", MVehicleSize.get("L").getJTT_VehicleSize_ID());
		} else {
			return "OverMaxPax";
		}
		
		return "";
	}
	
	private void setRevenueClass(GridTab mTab)
	{
		Object bpID = mTab.getValue("C_BPartner_ID");
		int C_BPartner_ID = 0;
		if (bpID != null) {
			C_BPartner_ID = (int) bpID;
			MBPartner bp = MBPartner.get(Env.getCtx(), C_BPartner_ID);
			
			mTab.setValue("JTT_RevenueClass_ID", bp.get_Value("JTT_RevenueClass_ID"));
		}
	}
	
	
}
