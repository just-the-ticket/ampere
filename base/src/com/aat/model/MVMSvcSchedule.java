package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MVMSvcSchedule extends X_JTT_VMSvcSchedule {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5711007199435925949L;

	public MVMSvcSchedule(Properties ctx, int X_JTT_VMSvcSchedule_ID, String trxName) {
		super(ctx, X_JTT_VMSvcSchedule_ID, trxName);
	}

	public MVMSvcSchedule(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
