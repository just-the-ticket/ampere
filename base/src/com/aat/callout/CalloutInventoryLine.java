package com.aat.callout;

import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MDocType;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProject;
import org.compiere.model.MStorage;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class CalloutInventoryLine extends CalloutEngine
{
	
	public String workshopJob(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if (value == null) {
			return null;
		}
		
		int projectID = (Integer) mTab.getValue("C_Project_ID");
		if (projectID > 0) {
			MProject project = new MProject(ctx, projectID, null);
			
			mTab.setValue("M_Warehouse_ID", project.getM_Warehouse_ID());
			mTab.setValue("A_Asset_ID", project.get_Value("A_Asset_ID"));
			mTab.setValue("UserElement1_ID", project.getM_Warehouse_ID());
			mTab.setValue("UserElement2_ID", project.get_Value("A_Asset_ID"));
			
		}
		
		return null;
	}
	
	/**
	 * Assign Charge from Product category when it is a workshop issue
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @param oldValue
	 * @return
	 */

	public String charge(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if (value == null) {
			return null;
		}
		int inventoryID = (Integer) mTab.getValue("M_Inventory_ID");
		int m_warehouse_id = (Integer) mTab.getValue("M_Warehouse_ID");
		
		MInventory inventory = MInventory.get(ctx, inventoryID);
		MDocType docType = MDocType.get(ctx, inventory.getC_DocType_ID());
		if (docType.get_ValueAsBoolean("IsWorkshopIssue"))
		{
			MProduct product = MProduct.get(ctx, (Integer) value);
			MProductCategory productCategory = MProductCategory.get(ctx, product.getM_Product_Category_ID());

			int chargeID = productCategory.get_ValueAsInt("C_Charge_ID");

			if (chargeID == 0)
			{
				return new StringBuilder(Msg.getMsg(Env.getCtx(), "PCatChargeNotAssigned", new Object[] { productCategory.getName() })).toString();
			}
			//must set this - otherwise it will just drop chargeID when qty=0
			mTab.setValue(MInventoryLine.COLUMNNAME_InventoryType, MInventoryLine.INVENTORYTYPE_ChargeAccount);
			mTab.setValue("C_Charge_ID", chargeID);
			//	Get existing Location
			int M_Locator_ID = MStorage.getM_Locator_ID (m_warehouse_id,
					product.getM_Product_ID(), 0,
					Env.ONE, null);
			if (M_Locator_ID > 0) {
				mTab.setValue("M_Locator_ID", M_Locator_ID);
			}
		}
		return null;
	}
	
	
	/**
	 * Assign UserElement1_ID from Depot
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @param oldValue
	 * @return
	 */
	public String depot(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if (value == null) {
			return null;
		}
		
		mTab.setValue("UserElement1_ID", value);
		
		return null;
	}
	
	
	/**
	 * Assign UserElement2_ID from Vehicle
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @param oldValue
	 * @return
	 */
	public String vehicle(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if (value == null) {
			return null;
		}
		
		mTab.setValue("UserElement2_ID", value);
		
		return null;
	}

}
