package com.aat.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MProject;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;

import com.aat.model.MVMSvcSchedule;
import com.aat.model.MWkshopJobPartsKit;
import com.aat.model.MWorkshopJob;
import com.aat.model.MWrkshopJobSched;


/**
 * 
 * @author jtrinidad
 * Service workshop jobs from selected service schedule
 */
public class CreateWorkshopJob extends SvrProcess {

	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}
		
		Map<Integer, MProject> map = new HashMap<Integer, MProject>();
		for (KeyNamePair key : keys) {
			MVMSvcSchedule schedule = new MVMSvcSchedule(getCtx(), key.getKey(), get_TrxName());
			MAsset asset = (MAsset) schedule.getA_Asset();
			Integer lastKM = getInfoSelectionAsInteger(key, "Last KM Reading");
			MProject workshopJob = map.get(schedule.getA_Asset_ID());
			if (workshopJob == null) {
				workshopJob = MWorkshopJob.createWorkshopJob(asset, lastKM);
				
				if (workshopJob == null) {
					throw new Exception("NoWorkshopJobCreated");
				}
				map.put(asset.getA_Asset_ID(), workshopJob);
			}
			
			MWrkshopJobSched jobSchedule = new MWrkshopJobSched(schedule, workshopJob);
			MWkshopJobPartsKit.assignPartsKit(schedule, workshopJob);
			schedule.setKMReading(lastKM);
			schedule.setActualSvcDate(workshopJob.getDateContract());
			schedule.save();
		}
		

		return String.format("%d workshop jobs created.", map.size());
	}

}
