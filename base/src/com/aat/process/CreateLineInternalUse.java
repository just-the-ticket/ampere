package com.aat.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProductCategory;
import org.compiere.model.MWarehouse;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import com.aat.model.MOrderLineDetail;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateLineInternalUse extends SvrProcess 
{
	@Override
	protected void prepare()
	{

	}

	@Override
	protected String doIt() throws Exception
	{
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		int inventoryID = this.getRecord_ID();
		MInventory inv = new MInventory(getCtx(), inventoryID, get_TrxName());
		BigDecimal totalAmt = Env.ZERO;
		String msg = "";
		
		MInventoryLine[] lines = inv.getLines(true);
		
		if (keys == null) {
			return "";  //TODO getting null from time to time - needs to debug
		}
		if (keys.size() == 0) {
			return "No selected items";
		}
		//M_Product_ID, MOrderLineDetail
		Map<Integer, MInventoryLine> map = new HashMap<Integer, MInventoryLine>();
		for (MInventoryLine line : lines) {
			map.put(line.getM_Product_ID(), line);
		}
		
		for (KeyNamePair key : keys)
		{
			BigDecimal qty = getInfoSelectionAsBigDecimal(key, "Quantity");
			
			// do not add if not set
			if (qty.signum() == 0) {
				continue;
			}
			
			MInventoryLine newline = map.get(key.getKey());
			
			//new record
			if (newline == null) {
				newline = new MInventoryLine(getCtx(), 0, get_TrxName());
				newline.setAD_Client_ID(inv.getAD_Client_ID());
				newline.setAD_Org_ID(inv.getAD_Org_ID());
				newline.setM_Product_ID(key.getKey());
				newline.setM_Inventory_ID(inventoryID);
				MLocator loc = MLocator.getDefault((MWarehouse) inv.getM_Warehouse());
				if (loc == null) {
					throw new AdempiereUserError("No default locator for " + inv.getM_Warehouse().getName());
				}
				newline.setM_Locator_ID(loc.getM_Locator_ID());
				MProductCategory pc = (MProductCategory) newline.getM_Product().getM_Product_Category();
				int c_charge_id = pc.get_ValueAsInt("C_Charge_ID");
				if (c_charge_id == 0) {
					throw new AdempiereUserError("Charge is not set for product category " + pc.getName());
				}
				newline.setC_Charge_ID(c_charge_id);
			}
			
			newline.setQtyInternalUse(qty);
			newline.save();
			
		}
		
		
//		if (totalAmt.signum() != 0) {
//			msg = String.format("Order line details created for  %.2f ", totalAmt);
//		}
		return msg;
	}

	
}
