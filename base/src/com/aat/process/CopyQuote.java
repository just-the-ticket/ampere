package com.aat.process;

import java.util.logging.Level;

import org.compiere.model.MOrder;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;


/**
 * 
 * @author jtrinidad
 *
 */
public class CopyQuote extends SvrProcess 
{
	
	private int			p_JTT_VehicleSize_ID = 0;
	private	int 		noOfPassengers = 0;
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("NoOfPassenger"))
				noOfPassengers = para[i].getParameterAsInt();
			else if (name.equals("JTT_VehicleSize_ID"))
				p_JTT_VehicleSize_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		

		String msg = "";
		
		MOrder from = new MOrder (getCtx(), getRecord_ID(), get_TrxName());
		MOrder newQuote = MOrder.copyAllFrom(from);
		newQuote.set_ValueOfColumn("NoOfPassenger", noOfPassengers);
		newQuote.set_ValueOfColumn("JTT_VehicleSize_ID", p_JTT_VehicleSize_ID);
		newQuote.set_ValueOfColumn("parent_order_id", from.get_ID());
		newQuote.set_ValueOfColumn("TripStatus", "Q");
		newQuote.setDocAction(MOrder.DOCACTION_Complete);

		newQuote.save();
		
		
		
		msg = String.format("New quote created Document No = %s ", newQuote.getDocumentNo());

		return msg;
	}

	
}
