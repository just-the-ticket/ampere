package com.aat.process;

import java.util.List;

import org.compiere.model.MOrder;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;


/**
 * 
 * @author jtrinidad
 * Process to run from Info Window
 */
public class ValidateTripQuoteInfo extends SvrProcess
{

	@Override
	protected void prepare()
	{

	}

	@Override
	protected String doIt() throws Exception
	{
		List<KeyNamePair> keys = getInfoSelectionKeys();

		if (keys == null || keys.size() == 0)
		{
			return "No selected items";
		}
		int n = 0;

		for (KeyNamePair key : keys)
		{
			
			MOrder so = new MOrder(getCtx(), key.getKey(), get_TrxName());
			boolean isCompleted = MOrder.DOCSTATUS_Completed.equals(so.getDocStatus());
			if (isCompleted) {
				so.reActivateIt();
				so.setDocStatus(MOrder.DOCSTATUS_Drafted);
				so.save();
			}
			if (isCompleted) {
				so.completeIt();
				so.save();
			} else {
				so.validateTripQuote();
			}
			n++;
		}
		addLog(String.format("%d trips validated.", n));
		return "";

	}


}
