package com.aat.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateAEFPurchaseOrder extends SvrProcess 
{
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
	}

	@Override
	protected String doIt() throws Exception
	{

		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}

		int nUpdate = 0;
		String msg = "";
		
		
		int C_Order_ID = 0;
		MOrder so = null;
		Map<Integer, MOrder> mapVendorPO = null;
		for (KeyNamePair key : keys)
		{
			//sales order line / quote line
			MOrderLine ql = new MOrderLine(getCtx(), key.getKey(), get_TrxName());
			if (C_Order_ID != ql.getC_Order_ID()) {
				//start new set every Quote
				mapVendorPO = new HashMap<Integer, MOrder>();
				C_Order_ID = ql.getC_Order_ID();
				so = (MOrder) ql.getC_Order();
			}
			MProduct product = ql.getProduct();
			MProductPO[] ppo = MProductPO.getOfProduct(getCtx(), ql.getM_Product_ID(), get_TrxName());
			if (ppo.length == 0) {
				addLog(String.format("Purchasing is currently not setup for %s-%s"
						, product.getValue(), product.getName()));
				continue;
			}
			MOrder po = mapVendorPO.get(ppo[0].getC_BPartner_ID());
			if (po == null) {
				po = createPOForVendor(ppo[0].getC_BPartner_ID(), so, getCtx(), get_TrxName());
				mapVendorPO.put(ppo[0].getC_BPartner_ID(), po);
			}
			createPOLineForVendor(po, ql, getCtx(), get_TrxName());
			nUpdate++;
			
		}
		msg = String.format("Order Line created/updated = %s ", nUpdate);

		return msg;
	}

	/**
	 *	Copied from OrderPOCreate
	 *	Create PO for Vendor
	 *	@param C_BPartner_ID vendor
	 *	@param so sales order
	 */
	public static MOrder createPOForVendor(int C_BPartner_ID, MOrder so, Properties ctx, String trxName)
	{
		MOrder po = new MOrder (ctx, 0, trxName);
		po.setClientOrg(so.getAD_Client_ID(), so.getAD_Org_ID());
		po.setLink_Order_ID(so.getC_Order_ID());
		po.setIsSOTrx(false);
		po.setC_DocTypeTarget_ID();
		//
		po.setDescription(so.getDescription());
		po.setPOReference(so.getDocumentNo());
		po.setPriorityRule(so.getPriorityRule());
		po.setSalesRep_ID(so.getSalesRep_ID());
		po.setM_Warehouse_ID(so.getM_Warehouse_ID());
		//	Set Vendor
		MBPartner vendor = new MBPartner (ctx, C_BPartner_ID, trxName);
		po.setBPartner(vendor);

		//	References
		po.setC_Activity_ID(so.getC_Activity_ID());
		po.setC_Campaign_ID(so.getC_Campaign_ID());
		po.setC_Project_ID(so.getC_Project_ID());
		po.setUser1_ID(so.getUser1_ID());
		po.setUser2_ID(so.getUser2_ID());
//		po.set_ValueOfColumn("UserElement1_ID", so.get_Value("UserElement1_ID"));
//		po.set_ValueOfColumn("UserElement2_ID", so.get_Value("UserElement2_ID"));
		//
		po.saveEx();
		return po;
	}	//	createPOForVendor
	
	
	/**
	 * Create PO Order line
	 * @param po
	 * @param sol
	 * @param ctx
	 * @param trxName
	 * @return
	 */
	public static MOrderLine createPOLineForVendor(MOrder po, MOrderLine sol, Properties ctx, String trxName)
	{
		MOrderLine poLine = new MOrderLine (po);
		poLine.setLink_OrderLine_ID(sol.getC_OrderLine_ID());
		poLine.setM_Product_ID(sol.getM_Product_ID());
		poLine.setC_Charge_ID(sol.getC_Charge_ID());
		poLine.setM_AttributeSetInstance_ID(sol.getM_AttributeSetInstance_ID());
		poLine.setC_UOM_ID(sol.getC_UOM_ID());
		poLine.setQtyEntered(sol.getQtyEntered());
		poLine.setQtyOrdered(sol.getQtyOrdered());
		poLine.setDescription(sol.getDescription());
		poLine.setDatePromised(sol.getDatePromised());
		poLine.setPrice();
		poLine.setC_Activity_ID(sol.getC_Activity_ID());
		poLine.setC_Project_ID(sol.getC_Project_ID());
		poLine.setC_Campaign_ID(sol.getC_Campaign_ID());
		poLine.setUser1_ID(sol.getUser1_ID());
		poLine.setUser2_ID(sol.getUser2_ID());
//		poLine.set_ValueOfColumn("UserElement1_ID", sol.get_Value("UserElement1_ID"));
//		poLine.set_ValueOfColumn("UserElement2_ID", sol.get_Value("UserElement2_ID"));
		
		poLine.saveEx();
		
		sol.setLink_OrderLine_ID(poLine.getC_OrderLine_ID());
		sol.saveEx();
		
		return poLine;
	}
	
	
}
