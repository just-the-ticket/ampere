package com.aat.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MProductPricing;
import org.compiere.model.MRefList;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.ValueNamePair;

import com.aat.model.MOrderLineDetail;
import com.aat.model.MVehicleSize;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateAEFOrderLine extends SvrProcess 
{
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
	}

	@Override
	protected String doIt() throws Exception
	{

		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}

		int refID = MColumn.get(getCtx(), MProduct.Table_Name, MProduct.COLUMNNAME_PsStatus).getAD_Reference_Value_ID();
		
		ValueNamePair[] pss = MRefList.getList(getCtx(), refID, false);
		
		
		int C_OrderLine_ID = this.getRecord_ID();
		MOrderLine refOrderLine = new MOrderLine(getCtx(), C_OrderLine_ID, get_TrxName());
		MOrder order = (MOrder) refOrderLine.getC_Order();
		MOrderLine lines[] = order.getLines();
		Map<Integer, MOrderLine> mapLines = new HashMap<Integer, MOrderLine>();
		for (MOrderLine line : lines) {
			if  (line.getM_Product_ID() != 0) {
				mapLines.put(line.getM_Product_ID(), line);
			}
			
		}
		
		int nUpdate = 0;
		String msg = "";
		
		
		for (KeyNamePair key : keys)
		{
			MProduct cp = new MProduct(getCtx(), key.getKey(), get_TrxName());
			
			for (ValueNamePair ps : pss) {
				
				int qty = getInfoSelectionAsInteger(key, ps.getName());
				if (qty != 0) {
					MProduct product = MProduct.getAEFProduct(getCtx(), ps.getValue(), ps.getName(), cp);
					MProductPricing pp = new MProductPricing (product.getM_Product_ID(), order.getC_BPartner_ID(), new BigDecimal(qty), order.isSOTrx());
					//
					pp.setM_PriceList_ID(order.getM_PriceList_ID());
					pp.setPriceDate(order.getDateOrdered());
					
					MOrderLine ol = mapLines.get(product.getM_Product_ID());
					if (ol == null) {
						ol = new MOrderLine(order);
						ol.setProduct(product);
						ol.setDescription(product.getDescription());
						if (ol.getDescription() == null || ol.getDescription().length() == 0) {
							ol.setDescription(product.getName());
						}
					}
					ol.setPrice(pp.getPriceList());
					ol.setC_Currency_ID(pp.getC_Currency_ID());
					ol.setDiscount(pp.getDiscount());
					ol.setQty(new BigDecimal(qty));
					ol.setLineNetAmt();
					ol.save();
					nUpdate++;
				}
			}
			
		}
		msg = String.format("Order Line created/updated = %s ", nUpdate);

		return msg;
	}

	
}
