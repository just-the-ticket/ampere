package com.aat.process;

import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MResource;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.aat.model.MVehicleSize;

/**
 * 
 * @author jtrinidad
 * Create a Resource Entry for each Fixed Asset / Vehicle
 */
public class CreateAssetResource extends SvrProcess {

	private int S_ResourceType_ID = 0;
	
	@Override
	protected void prepare() {
		// pass resource type - set default parameter in AD
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("S_ResourceType_ID"))
				S_ResourceType_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		// loop through Asset
		List<MAsset> list = new Query(getCtx(), MAsset.Table_Name, "JTT_VehicleSize_ID IS NOT NULL", get_TrxName())
				.list();
		MAsset[] assets = list.toArray(new MAsset[list.size()]);

		for (MAsset asset : assets) {
			// create resource
			MVehicleSize size = MVehicleSize.get(asset.getJTT_VehicleSize_ID());
			if (asset.getSchd_Warehouse_ID() == 0) {
				addLog(String.format(String.format("%s - (%s) - No assigned depot.", asset.getValue(), size.getSeatsMax())));
			} else {
				MResource.get(asset, S_ResourceType_ID);
			}
			
		}
		return "";
	}

}
