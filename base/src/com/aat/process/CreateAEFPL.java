package com.aat.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRefList;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.ValueNamePair;

import com.aat.model.MOrderLineDetail;
import com.aat.model.MVehicleSize;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateAEFPL extends SvrProcess 
{
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
	}

	@Override
	protected String doIt() throws Exception
	{

		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}

		int refID = MColumn.get(getCtx(), MProduct.Table_Name, MProduct.COLUMNNAME_PsStatus).getAD_Reference_Value_ID();
		
		ValueNamePair[] pss = MRefList.getList(getCtx(), refID, false);
		
		
		int priceListVersionID = this.getRecord_ID();
//		MPriceListVersion plv = new MPriceListVersion(getCtx(), priceListVersionID, get_TrxName());
		int nUpdate = 0;
		String msg = "";
		
		for (KeyNamePair key : keys)
		{
			MProduct cp = new MProduct(getCtx(), key.getKey(), get_TrxName());
			
			for (ValueNamePair ps : pss) {
				
				BigDecimal price = getInfoSelectionAsBigDecimal(key, ps.getName());
				if (price != null && price.signum() != 0) {
					//pick up the product
					MProduct product = MProduct.getAEFProduct(getCtx(), ps.getValue(), ps.getName(), cp);
					MProductPrice pp = MProductPrice.get(getCtx(), priceListVersionID, product.getM_Product_ID(), get_TrxName());
					if (pp == null) {
						// not yet defined - create new one
						pp = new MProductPrice(getCtx(), priceListVersionID, product.getM_Product_ID(), get_TrxName());
						pp.setPrices(price, price, price);  // at the moment, they're all the same
						pp.save();
						nUpdate++;
					} else if (pp.getPriceList().compareTo(price) != 0) {
							pp.setPrices(price, price, price);  // at the moment, they're all the same
							pp.save();
							nUpdate++;
					}
					
				}
			}
			
		}
		msg = String.format("Prices created/updated = %s ", nUpdate);

		return msg;
	}

	
}
