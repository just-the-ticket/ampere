package com.aat.process;

import java.util.List;

import org.compiere.model.MBPartner;
import org.compiere.model.MLocator;
import org.compiere.model.MResource;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.process.SvrProcess;


/**
 * 
 * @author jtrinidad
 *
 * Create a Resource of Type Driver, Lang Spec, Guide ...
 */

public class CreateBPEmployeeResource extends SvrProcess {

	
	@Override
	protected void prepare() {

	}

	@Override
	protected String doIt() throws Exception {
		
		int n = 0;
		List<MBPartner> list = new Query(getCtx(), MBPartner.Table_Name, "IsEmployee = 'Y' AND S_ResourceType_ID IS NOT NULL", get_TrxName())
				.list();
		MBPartner[] bps = list.toArray(new MBPartner[list.size()]);

		for (MBPartner bp : bps) {
			// create resource
			if (bp.getM_Warehouse_ID() == 0) {
				addLog("Must assign depot for " + bp.getName());
				continue;
			}
			
			if (MLocator.getDefault((MWarehouse) bp.getM_Warehouse()) == null) {
				addLog("Must assign depot for " + bp.getM_Warehouse().getName() + ". " + bp.getName() + " skipped.");
				continue;
			}
//			MResource.get(bp);  TODO resource type 
			n++;
			
		}
		
		return "Employee processed = " + n;
	}

}
