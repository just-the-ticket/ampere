package com.aat.process;

import java.util.logging.Level;

import org.compiere.model.MOrder;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;


/**
 * Confirm Quote and create Resource Assignment
 * @author jtrinidad
 *
 */
public class QuoteAction extends SvrProcess 
{
	
	
	private static final String ACTION_CONFIRMED = "C";
	private static final String ACTION_UNCONFIRMED = "X";
	
	private String m_action = ACTION_CONFIRMED;
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			if (name.equals("QuoteAction"))
				m_action = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		

		String msg = "";
		
		MOrder order = new MOrder (getCtx(), getRecord_ID(), get_TrxName());
		if (ACTION_CONFIRMED.equals(m_action)) {
			order.confirmQuote();
			msg = String.format("Quote confirmed = %s ", order.getDocumentNo());
		} else if (ACTION_UNCONFIRMED.equals(m_action)) {
			order.unConfirmQuote();
			msg = String.format("Quote unconfirmed = %s ", order.getDocumentNo());
		}
		
		

		return msg;
	}

	
}
