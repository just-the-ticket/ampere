package com.aat.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MStorage;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;


/**
 * Assign Asset/Vehicle to a maintenance plan
 * @author jtrinidad
 *
 */
public class CreateFuelIssue extends SvrProcess 
{
	
	private int			m_Product_ID = 0;
	
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID")) {
				m_Product_ID = para[i].getParameterAsInt();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}

		MInventory inv = new MInventory(getCtx(), getRecord_ID(), get_TrxName());

		MInventoryLine[] lines = inv.getLines(true);
//		Map<Integer, MInventoryLine> maps = new HashMap<Integer, MInventoryLine>();
//		for (MInventoryLine il : lines) {
//			//just check the target product - they can issue different product to a vehicle
//			if (il.getM_Product_ID() == m_Product_ID) {
//				maps.put(il.get_ValueAsInt("A_Asset_ID"), il);
//			}
//		}
		
		int n = 0;
		String msg = "";
		
		MProductCategory productCategory = (MProductCategory) MProduct.get(getCtx(), m_Product_ID).getM_Product_Category();
		int chargeID = productCategory.get_ValueAsInt("C_Charge_ID");
		
		for (KeyNamePair key : keys) {
			MInventoryLine line = null;
			BigDecimal qty = getInfoSelectionAsBigDecimal(key, "Qty Issue");
			if (qty.signum() == 0) {
				continue;
			}
			Integer kmreading = 0;
			
			if ( getInfoSelectionAsInteger(key, "KM Reading") != null) {
				kmreading = getInfoSelectionAsInteger(key, "KM Reading");
			}
			//if duplicate do not assign
//			if (maps.containsKey(key.getKey())) {
//				line = maps.get(key.getKey());
//			}
			if (line == null) {
				line = new MInventoryLine(getCtx(), 0, get_TrxName());
				line.setAD_Client_ID(inv.getAD_Client_ID());
				line.setAD_Org_ID(inv.getAD_Org_ID());
				line.setM_Inventory_ID(inv.getM_Inventory_ID());
				
				line.set_ValueOfColumn("A_Asset_ID", key.getKey());
				line.set_ValueOfColumn("M_Warehouse_ID", inv.getM_Warehouse_ID());
				line.setM_Product_ID(m_Product_ID);
				int M_Locator_ID = MStorage.getM_Locator_ID (inv.getM_Warehouse_ID(),
						m_Product_ID, 0,
						qty, get_TrxName());
				if (M_Locator_ID == 0) {
					M_Locator_ID = MLocator.getDefault((MWarehouse) inv.getM_Warehouse()).getM_Locator_ID();
				}
				line.setM_Locator_ID(M_Locator_ID);

				line.setC_Charge_ID(chargeID);
				line.set_ValueOfColumn("UserElement1_ID", inv.getM_Warehouse_ID());
				line.set_ValueOfColumn("UserElement2_ID", key.getKey());
			}
			line.setQtyInternalUse(qty);
			line.setDescription(getInfoSelectionAsString(key, "Description"));
			if (kmreading != null)
				line.set_ValueOfColumn("KMReading", kmreading);
			
			line.save();
			n++;
		}

		
		msg = String.format("Vehicles added to the maintenance plan  = %s ", n);

		return msg;
	}

	
}
