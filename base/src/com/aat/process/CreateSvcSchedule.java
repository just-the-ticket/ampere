package com.aat.process;

import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MResource;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.aat.model.MVMVehicle;


/**
 * 
 * @author jtrinidad
 * Service schedule for each Vehicle
 */
public class CreateSvcSchedule extends SvrProcess {

	private int m_asset_id = 0;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("A_Asset_ID"))
				m_asset_id = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		int counter = 0;

		if (m_asset_id > 0) {
			MAsset asset = new MAsset(getCtx(), m_asset_id, get_TrxName());
			counter = MVMVehicle.createSvcSchedule(asset);
			return String.format("%d service schedules created.", counter);
			
		}
		// loop through Asset
		List<MAsset> list = new Query(getCtx(), MAsset.Table_Name, "JTT_VehicleSize_ID IS NOT NULL", get_TrxName())
				.list();
		MAsset[] assets = list.toArray(new MAsset[list.size()]);

		
		for (MAsset asset : assets) {
			int n = MVMVehicle.createSvcSchedule(asset);  //svc schedules created for this vehicle
			
			counter+=n;
		}
		return String.format("%d service schedules created.", counter);
	}

}
