package com.aat.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MOrder;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;

import com.aat.model.MVMPlan;
import com.aat.model.MVMPlanAssignment;


/**
 * Assign Asset/Vehicle to a maintenance plan
 * @author jtrinidad
 *
 */
public class AssignVMPLan extends SvrProcess 
{
	
	private int			p_JTT_VehicleSize_ID = 0;
	private	int 		noOfPassengers = 0;
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}

		int vmPlanID = getRecord_ID();
		MVMPlan plan = new MVMPlan(getCtx(), vmPlanID, get_TrxName());
		
		Map<Integer, MVMPlanAssignment> maps = new HashMap<Integer, MVMPlanAssignment>();
		MVMPlanAssignment[] ass = plan.getAssignments();
		for (MVMPlanAssignment as : ass) {
			maps.put(as.getA_Asset_ID(), as);
		}
		
		int n = 0;
		String msg = "";
		for (KeyNamePair key : keys) {
			//if duplicate do not assign
			if (maps.containsKey(key.getKey())) {
				continue;
			}
			MVMPlanAssignment as = new MVMPlanAssignment(plan);
			as.setA_Asset_ID(key.getKey());
			as.save();
			n++;
		}

		
		msg = String.format("Vehicles added to the maintenance plan  = %s ", n);

		return msg;
	}

	
}
