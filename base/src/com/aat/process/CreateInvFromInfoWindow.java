package com.aat.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;

public class CreateInvFromInfoWindow extends SvrProcess
{

	@Override
	protected void prepare()
	{

	}

	@Override
	protected String doIt() throws Exception
	{
		List<KeyNamePair> keys = getInfoSelectionKeys();

		if (keys == null || keys.size() == 0)
		{
			return "No selected items";
		}

		Map<Integer, List<MOrder>> ordersByPartner = new HashMap<>();

		for (KeyNamePair key : keys)
		{
			// sales order line / quote line
			MOrder so = new MOrder(getCtx(), key.getKey(), get_TrxName());
			if (!ordersByPartner.containsKey(so.getC_BPartner_ID()))
			{
				ordersByPartner.put(so.getC_BPartner_ID(), new ArrayList<>());
			}

			ordersByPartner.get(so.getC_BPartner_ID()).add(so);
		}
		StringBuilder msg =  new StringBuilder();

		for (Map.Entry<Integer, List<MOrder>> entry : ordersByPartner.entrySet())
		{

			List<MOrder> orders = entry.getValue();

			// Create a single invoice for the partner
			MOrder firstOrder = orders.get(0);

			MInvoice m_invoice = new MInvoice(firstOrder, 0,
					firstOrder.getDateTripEnd());
			m_invoice.set_ValueOfColumn("JTT_RevenueClass_ID", firstOrder.get_ValueAsInt("JTT_RevenueClass_ID"));
			if (!m_invoice.save())
				throw new IllegalStateException("Could not create Invoice");
			
			msg.append("Invoice Document No."+m_invoice.getDocumentNo()+" has been created for Business Partner : "+firstOrder.getC_BPartner().getName()+"\n");
			for (MOrder order : orders)
			{
				MOrderLine[] oLines = order.getLines(" AND IsActive ='Y'", MOrderLine.COLUMNNAME_Line);
				for (MOrderLine oLine : oLines)
				{
					createLine(order, oLine, m_invoice);
				}
			}
		}
		addLog(msg.toString());
		return "";

	}

	private void createLine(MOrder so, MOrderLine oLine, MInvoice m_invoice)
	{
		//
		MInvoiceLine iLine = new MInvoiceLine(m_invoice);
		iLine.setOrderLine(oLine);
		iLine.setDescription(so.getDocumentNo() + "_" + oLine.getLine() + "_" + oLine.getDescription());
		iLine.setM_Product_ID(oLine.getM_Product_ID());
		iLine.setC_Charge_ID(oLine.getC_Charge_ID());
		iLine.setQtyInvoiced(oLine.getQtyEntered());
		iLine.setQtyEntered(oLine.getQtyEntered());
		iLine.setLine(oLine.getLine());
		iLine.setLineNetAmt(oLine.getPriceList());
		iLine.setPrice(oLine.getPackagedPrice());
		iLine.setPriceEntered(oLine.getPriceEntered());
		iLine.setPriceActual(oLine.getPriceActual());
		iLine.set_ValueOfColumn("PackagedPrice", oLine.get_Value("PackagedPrice"));
		iLine.set_ValueOfColumn("UserElement1_ID", oLine.getM_Warehouse_ID());  //assign from depot
		//not needed at this stage
//		iLine.set_ValueOfColumn("UserElement2_ID", oLine.getA_Asset_ID());  //assign from vehicle - upon trip complete - update a_asset_id
		if (!iLine.save())
			throw new IllegalStateException("Could not create Invoice Line");
		log.fine(iLine.toString());
	} // createLine

}
