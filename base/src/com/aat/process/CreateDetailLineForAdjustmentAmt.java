package com.aat.process;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTax;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.aat.model.MOrderLineDetail;

/**
 * @author Logilite Technologies
 * 
 * process to Adjust the lump sump amount and add line in the orderline detail
 *
 */
public class CreateDetailLineForAdjustmentAmt extends SvrProcess
{

	@Override
	protected void prepare()
	{

	}

	@Override
	protected String doIt() throws Exception
	{

		int orderLine_ID = this.getRecord_ID();
		MOrderLine ol = new MOrderLine(getCtx(), orderLine_ID, get_TrxName());
		ol.overrideLineTotal();

			
		MOrder order = (MOrder) ol.getC_Order();
		order.updateLumpSump();
		return null;
	}

}
