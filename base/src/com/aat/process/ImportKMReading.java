package com.aat.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.aat.model.X_I_VMVehReading;
import com.aat.model.X_JTT_VMVehReading;

/**
 * @author Logilite Technologies
 * 
 * Process to import the kmreading 
 *
 */
public class ImportKMReading extends SvrProcess
{

	private static String	COLUMNNAME_A_Asset_ID	= "A_Asset_ID";
	private static String	COLUMNNAME_KMReading	= "KMReading";
	private static String	COLUMNNAME_AD_User_ID	= "AD_User_ID";
	private static String	COLUMNNAME_T_Date		= "T_Date";
	private static String	COLUMNNAME_T_Time		= "T_Time";
	private static String	COLUMNNAME_VehKMSource	= "VehKMSource";
	private static String	COLUMNAME_FLEETID		= "Value";
	private static String	COLUMNAME_AD_CLIENT_ID	= "AD_Client_ID";
	private static String	COLUMNAME_AD_ORG_ID		= "AD_Org_ID";

	/**	Delete old Imported				*/
	private boolean			m_deleteOldImported = false;

	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOldImported"))
				m_deleteOldImported = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception
	{
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + Env.getAD_Client_ID(getCtx()) + " AND isActive='Y'";
	
		if (m_deleteOldImported)
		{
			sql = new StringBuffer("DELETE FROM I_VMVehReading " + "WHERE I_IsImported='Y'").append(clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			log.fine("Delete Old Imported =" + no);
		}

		//Update the A_Asset	
		sql = new StringBuffer("UPDATE I_VMVehReading ir  "
				+ "SET A_Asset_ID = (SELECT aa.A_Asset_ID FROM A_Asset aa"
				+ " 	WHERE aa.value = ir.value), "
				+ " 	I_IsImported = 'N' "
				+ " WHERE I_IsImported<>'Y' And ir.Value IS NOT NULL ").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.info("Update A_Asset_ID value from the fleetID value " + no);
		
		sql = new StringBuffer ("UPDATE I_VMVehReading "
							+ " SET I_IsImported='E', I_ErrorMsg=")
							.append(" 'Cannot Update I_VMVehReading First because T_Date OR T_Time IS NULL,' ")
							.append(" WHERE (T_Date IS NULL  OR T_Time IS NULL)").append(clientCheck);;
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.info("Cannot Update Record T_Date is null " + no);


		commitEx();

		sql = new StringBuffer("SELECT DISTINCT ON (value, t_date)VehKMSource, KMReading, T_Date, T_Time, Value, I_VMVehReading_ID,"
							+ "		 A_Asset_ID, AD_Client_ID, AD_Org_ID "
							+ " FROM I_VMVehReading "
							+ " WHERE A_Asset_ID IS NOT NULL  "
							+ " 	 AND I_IsImported='N' AND (T_Date IS NOT NULL OR T_Time IS NOT NULL) ").append(clientCheck);
		sql.append(" ORDER BY Value, T_Date, KMReading DESC ");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				String fleetID = rs.getString(COLUMNAME_FLEETID);
				Timestamp date = rs.getTimestamp(COLUMNNAME_T_Date);
				X_JTT_VMVehReading impVehReading = new X_JTT_VMVehReading (getCtx(), 0, get_TrxName());
				impVehReading.set_ValueOfColumn(COLUMNNAME_AD_User_ID, Env.getAD_User_ID(getCtx()));
				impVehReading.setVehKMSource(rs.getString(COLUMNNAME_VehKMSource));
				impVehReading.setKMReading(rs.getBigDecimal(COLUMNNAME_KMReading).intValue());
				impVehReading.setDateReport(TimeUtil.getDayTime(date, rs.getTimestamp(COLUMNNAME_T_Time)));
				impVehReading.setA_Asset_ID(rs.getInt(COLUMNNAME_A_Asset_ID));
				impVehReading.setAD_Client_ID(rs.getInt(COLUMNAME_AD_CLIENT_ID));
				impVehReading.setAD_Org_ID(rs.getInt(COLUMNAME_AD_ORG_ID));
				
				if (Util.isEmpty(rs.getString(COLUMNNAME_VehKMSource)))
					impVehReading.setVehKMSource("T");
				else
					impVehReading.setVehKMSource(rs.getString(COLUMNNAME_VehKMSource));
				
				impVehReading.saveEx();
				
				X_I_VMVehReading vehReading = new X_I_VMVehReading (getCtx(), rs.getInt("I_VMVehReading_ID"), get_TrxName());
				vehReading.setJTT_VMVehReading_ID(impVehReading.getJTT_VMVehReading_ID());
				vehReading.saveEx();
				
				sql = new StringBuffer("UPDATE I_VMVehReading " 
									+ " SET Processed = 'Y', " 
									+ " 	I_IsImported = 'Y' "
									+ " WHERE Value = ? "
									+ " 	AND Processed = 'N' AND I_IsImported <> 'Y' "
									+ "		AND A_Asset_ID IS NOT NULL  ").append(clientCheck);
				no = DB.executeUpdateEx(sql.toString(), new Object[] { fleetID }, get_TrxName());
				log.info("Update processed and Imported flag =" + no);
				
				sql = new StringBuffer("DELETE FROM I_VMVehReading "
										+ "	  WHERE I_IsImported = 'Y' AND Processed = 'Y' AND  JTT_VMVehReading_ID IS NULL"
										+ "	  AND  Value = ? AND T_Date = ? OR (T_Date IS NULL OR T_Time IS NULL)").append(clientCheck);							
				no = DB.executeUpdateEx(sql.toString(), new Object[] { fleetID,date }, get_TrxName());
				log.info("Delete Unwanted Recods From I_VMVehReading =" + no);

				commitEx();
			}
			DB.close(rs, pstmt);
		}
		catch (SQLException e)
		{
			rollback();
			//log.log(Level.SEVERE, "", e);
			throw new DBException(e, sql.toString());
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		

		return null;
	}

}
