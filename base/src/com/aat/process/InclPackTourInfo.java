package com.aat.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTax;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.TimeUtil;

import com.aat.model.MOrderLineDetail;
import com.aat.model.MPCCRLine;
import com.aat.model.MVehicleSize;

public class InclPackTourInfo extends SvrProcess {

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {

		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}
		
		int lsProduct_ID = MSysConfig.getIntValue("PRODUCT_LUMPSUMP", 0, getAD_Client_ID());
		if (lsProduct_ID == 0) {
			return "Lump Sump Product is not configured in SYSCONFIG. Please update value of PRODUCT_LUMPSUMP";
		}
		
		MOrder order = new MOrder(getCtx(), this.getRecord_ID(), get_TrxName());
		int tripOffset = 0;
		Timestamp runningDate = order.getDateTripEnd() != null ? order.getDateTripEnd() : null;
		
		if (runningDate == null) {
			runningDate = order.getDateTripStart() != null ? order.getDateTripStart() : null;
		}
		
		MPriceListVersion plv = ((MPriceList) order.getM_PriceList()).getPriceListVersion(order.getDateTripStart());
		MVehicleSize vs = MVehicleSize.get(order.getJTT_VehicleSize_ID());
		MTax tax = null;
		
		//loop through all selected package
		for (KeyNamePair key : keys) {
			MPCCRLine tour = new MPCCRLine(getCtx(), key.getKey(), get_TrxName());
			MOrder seedOrder = (MOrder) tour.getC_Order();
			MOrderLine[] seedLines = seedOrder.getLines();
			MOrderLineDetail[] details = null;
			
			
			tripOffset = TimeUtil.getDaysBetween(seedOrder.getDateTripStart(), runningDate);
			
			//VSR + L = column vsrl
			BigDecimal publishedPrice = (BigDecimal)tour.get_Value("VSR" + MVehicleSize.get(order.getJTT_VehicleSize_ID()).getValue());
			
			//copy all seed lines
			for (MOrderLine seedLine : seedLines) {
				MOrderLine ol = new MOrderLine(getCtx(), 0, get_TrxName());
				PO.copyValues(seedLine, ol);
				ol.setAD_Org_ID(order.getAD_Org_ID());
				ol.setC_Order_ID(order.getC_Order_ID());
				ol.setC_BPartner_ID(order.getC_BPartner_ID());
				ol.setC_BPartner_Location_ID(order.getC_BPartner_Location_ID());
				ol.setSeed_OrderLine_ID(seedLine.getC_OrderLine_ID());
				ol.setQtyReserved(Env.ZERO);  //shouldn't copy
				ol.setQtyDelivered(Env.ZERO);
				ol.setQtyInvoiced(Env.ZERO);
				ol.setQtyLostSales(Env.ZERO);
				ol.setQty(seedLine.getQtyEntered());				
				ol.setJTT_PCCRLine_ID(tour.getJTT_PCCRLine_ID());
//				ol.setDescription(tour.getName());
				ol.setPackagedPrice(publishedPrice);
				details = MOrderLineDetail.getOrderLineDetails(getCtx(), seedLine.getC_OrderLine_ID(), get_TrxName());
				
				//adjust trip date
//				int legOffset = TimeUtil.getDaysBetween(seedOrder.getDateTripStart(), seedLine.getDateTripStart());
				
				ol.setDateTripStart(TimeUtil.addDays(seedLine.getDateTripStart(), tripOffset));
				ol.setDateTripEnd(TimeUtil.addDays(seedLine.getDateTripEnd(), tripOffset));
				
				ol.save();
				BigDecimal lineTotal = Env.ZERO;
				for (MOrderLineDetail detail : details)
				{
					MOrderLineDetail newDetail = null;

					newDetail = new MOrderLineDetail(Env.getCtx(), 0, get_TrxName());
					PO.copyValues(detail, newDetail);
					newDetail.setAD_Org_ID(order.getAD_Org_ID());
					newDetail.setC_OrderLine_ID(ol.getC_OrderLine_ID());
						
					
					try {
						newDetail.setProduct(vs, ol.getM_Warehouse_ID(), plv.getM_PriceList_Version_ID());
						lineTotal = lineTotal.add(detail.getLineNetAmt());
					} catch (AdempiereUserError e) {
						return e.getMessage();
					}
					
					newDetail.save();
					
				}

				
				if (tax == null) {
					tax = (MTax) ol.getC_Tax();
				}
			}
			
		}
		order.updateLumpSump();
		order.renumberLines(10);
		
		return null;
	}

}
