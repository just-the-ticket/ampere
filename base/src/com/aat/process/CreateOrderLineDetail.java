package com.aat.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.compiere.model.MOrderLine;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import com.aat.model.MOrderLineDetail;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateOrderLineDetail extends SvrProcess 
{
	@Override
	protected void prepare()
	{

	}

	@Override
	protected String doIt() throws Exception
	{
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		int orderLine_ID = this.getRecord_ID();
		MOrderLine ol = new MOrderLine(getCtx(), orderLine_ID, get_TrxName());
		BigDecimal totalAmt = Env.ZERO;
		String msg = "";
		
		MOrderLineDetail[] details = MOrderLineDetail.getOrderLineDetails(getCtx(), orderLine_ID, get_TrxName());
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}
		//M_Product_ID, MOrderLineDetail
		Map<Integer, MOrderLineDetail> map = new HashMap<Integer, MOrderLineDetail>();
		for (MOrderLineDetail detail : details) {
			map.put(detail.getM_Product_ID(), detail);
		}
		
		for (KeyNamePair key : keys)
		{
			BigDecimal qty = getInfoSelectionAsBigDecimal(key, "Quantity");
			BigDecimal price = getInfoSelectionAsBigDecimal(key, "Price");
			
			// do not add if not set
			if (qty.multiply(price).signum() == 0) {
				continue;
			}
			
			MOrderLineDetail detail = map.get(key.getKey());
			
			//new record
			if (detail == null) {
				detail = new MOrderLineDetail(getCtx(), 0, get_TrxName());
				detail.setAD_Client_ID(ol.getAD_Client_ID());
				detail.setAD_Org_ID(ol.getAD_Org_ID());
				detail.setC_OrderLine_ID(orderLine_ID);
				detail.setM_Product_ID(key.getKey());
			}

			detail.setC_UOM_ID(detail.getM_Product().getC_UOM_ID());
			detail.setQty(qty);
			detail.setPriceEntered(price);
			detail.setLineNetAmt(qty.multiply(price));
			detail.save();
			
			totalAmt = totalAmt.add(detail.getLineNetAmt());
		}
		
		ol.setPrice(totalAmt);
		ol.setLineNetAmt(totalAmt);
		ol.save();
		
		if (totalAmt.signum() != 0) {
			msg = String.format("Order line details created for  %.2f ", totalAmt);
		}
		return msg;
	}

	
}
