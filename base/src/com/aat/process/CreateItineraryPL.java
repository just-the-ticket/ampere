package com.aat.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import com.aat.model.MOrderLineDetail;
import com.aat.model.MVehicleSize;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateItineraryPL extends SvrProcess 
{
	int m_warehouse_id = 0;
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Warehouse_ID"))
				m_warehouse_id = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		//parameter is readonly and has default value
		if (m_warehouse_id == 0) {
			return "Unexpected error - no depot selected";
		}
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}

		int priceListVersionID = this.getRecord_ID();
		MPriceListVersion plv = new MPriceListVersion(getCtx(), priceListVersionID, get_TrxName());
		MWarehouse depot = MWarehouse.get(getCtx(), m_warehouse_id);
		int nUpdate = 0;
		String msg = "";
		
		MProduct[] carrierProducts = MProduct.getCarrierProduct();
		
		for (KeyNamePair key : keys)
		{
			MVehicleSize vehicleSize = new MVehicleSize(getCtx(), key.getKey(), get_TrxName());
			
			for (MProduct carrierProduct : carrierProducts) {
				BigDecimal price = getInfoSelectionAsBigDecimal(key, carrierProduct.getValue());
				if (price != null && price.signum() != 0) {
					//pick up the product
					MProduct product = MProduct.getMProduct(getCtx(), depot.getM_Warehouse_ID(), vehicleSize, carrierProduct);
					MProductPrice pp = MProductPrice.get(getCtx(), priceListVersionID, product.getM_Product_ID(), get_TrxName());
					if (pp == null) {
						// not yet defined - create new one
						pp = new MProductPrice(getCtx(), priceListVersionID, product.getM_Product_ID(), get_TrxName());
						pp.setPrices(price, price, price);  // at the moment, they're all the same
						pp.save();
						nUpdate++;
					} else if (pp.getPriceList().compareTo(price) != 0) {
							pp.setPrices(price, price, price);  // at the moment, they're all the same
							pp.save();
							nUpdate++;
					}
					
				}
			}
			
		}
		msg = String.format("Prices created/updated = %s ", nUpdate);

		return msg;
	}

	
}
