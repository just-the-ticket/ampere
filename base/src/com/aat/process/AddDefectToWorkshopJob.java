package com.aat.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MProject;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.KeyNamePair;

import com.aat.model.MVMSvcSchedule;
import com.aat.model.MVehicleDefect;
import com.aat.model.MWkshopJobPartsKit;
import com.aat.model.MWorkshopJob;
import com.aat.model.MWrkshopJobSched;


/**
 * 
 * @author jtrinidad
 * Add defects to 
 */
public class AddDefectToWorkshopJob extends SvrProcess {

	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}
		
		MProject project = new MProject(getCtx(), getRecord_ID(), get_TrxName());
		
		for (KeyNamePair key : keys) {
			MVehicleDefect defect = new MVehicleDefect(getCtx(), key.getKey(), get_TrxName());
			defect.setC_Project_ID(getRecord_ID());
			defect.setJTT_VehDefectFixedDate(project.getDateContract());
			defect.save();
		}
		

		return String.format("%d defects added to workshop job.", keys.size());
	}

}
