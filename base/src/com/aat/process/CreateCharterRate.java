package com.aat.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MProductPricing;
import org.compiere.model.MRefList;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.ValueNamePair;

import com.aat.model.MOrderLineDetail;
import com.aat.model.MPCCRHeader;
import com.aat.model.MPCCRLine;
import com.aat.model.MVehicleSize;


/**
 * 
 * @author jtrinidad
 *
 */
public class CreateCharterRate extends SvrProcess 
{
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
	}

	@Override
	protected String doIt() throws Exception
	{

		List<KeyNamePair> keys = getInfoSelectionKeys();
		
		if (keys == null || keys.size() == 0) {
			return "No selected items";
		}
		
		int headerID = this.getRecord_ID();
		MPCCRHeader header = new MPCCRHeader(getCtx(), headerID, get_TrxName());
		Map<Integer, MPCCRLine> mapsLines = new HashMap<Integer, MPCCRLine>();
		MPCCRLine[] lines = header.getLines();
		
		for (MPCCRLine line : lines) {
			mapsLines.put(line.getC_Order_ID(), line);
		}
		
		int nUpdate = 0;
		String msg = "";
		
		
		for (KeyNamePair key : keys)
		{
			MOrder order = new MOrder(getCtx(), key.getKey(), get_TrxName());
			
			MPCCRLine line = mapsLines.get(order.getC_Order_ID());
			if (line == null) {
				line = new MPCCRLine(header);
				line.setC_Order_ID(order.getC_Order_ID());
				line.setValue(order.getPOReference());
				line.setName(order.getDescription());
			}
				
			BigDecimal small = getInfoSelectionAsBigDecimal(key, "Small");
			BigDecimal medium = getInfoSelectionAsBigDecimal(key, "Medium");
			BigDecimal large = getInfoSelectionAsBigDecimal(key, "Large");
			
			line.setVSRL(large);
			line.setVSRMIDI(medium);
			line.setVSRMINI(small);
			line.save();
			nUpdate++;
			
		}
		msg = String.format("Charter Rate Line created/updated = %s ", nUpdate);

		return msg;
	}

	
}
