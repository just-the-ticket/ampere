package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;

public class MConvCurrency extends X_X_Conv_Currency {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6974561858974297504L;

	public MConvCurrency(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public MConvCurrency(Properties ctx, int X_Conv_Currency_ID, String trxName) {
		super(ctx, X_Conv_Currency_ID, trxName);
	}

	@Override
	protected boolean beforeSave(boolean newRecord) {

		return true;
	}

	protected boolean afterSave(boolean newRecord, boolean success) {

		StringBuffer sql = null;

		if (!success)
			return success;

		if (newRecord) {

			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				sql = new StringBuffer(
						"SELECT ISO_Code, C_Currency_ID FROM C_Currency WHERE IsActive='Y' AND C_Currency_ID<>?");
				pstmt = DB.prepareStatement(sql.toString(),
						get_TrxName());
				pstmt.setInt(1, getC_Currency_ID());
				rs = pstmt.executeQuery();

				while (rs.next()) {

					MConvCurrencyRate rate = new MConvCurrencyRate(getCtx(), 0,
							get_TrxName());

					rate.setAD_Org_ID(getAD_Org_ID());
					rate.setC_Currency_ID(rs.getInt("C_Currency_ID"));
					rate.setX_Conv_Currency_ID(getX_Conv_Currency_ID());

					rate.save();

				}

			} catch (SQLException e) {
				log.log(Level.SEVERE, "CURR - " + sql.toString(), e);
			}
			finally
			{
				DB.close(rs, pstmt);
			}
		}

		return true;
	} // afterSave

}
