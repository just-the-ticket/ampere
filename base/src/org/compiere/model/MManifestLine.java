package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MManifestLine extends X_M_InOut_ManifestLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6637893323173504134L;
	private MManifest m_parent = null;

	public MManifestLine(Properties ctx, int M_InOut_ManifestLine_ID,
			String trxName) {
		super(ctx, M_InOut_ManifestLine_ID, trxName);
	}

	
	public MManifestLine (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MManifestLines

	public MManifestLine(MManifest manifest) {
		this (manifest.getCtx(), 0, manifest.get_TrxName());
		setClientOrg (manifest);
		setM_InOut_Manifest_ID(manifest.getM_InOut_Manifest_ID());
		m_parent  = manifest;
	}
}
