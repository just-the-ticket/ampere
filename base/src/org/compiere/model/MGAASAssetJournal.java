/**
 * 
 */
package org.compiere.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

/**
 * @author Ashley G Ramdass
 * 
 */
public class MGAASAssetJournal extends X_GAAS_AssetJournal implements DocAction
{
    private static final long serialVersionUID = 8882336469077833691L;
    private static final CLogger logger = CLogger
            .getCLogger(MGAASAssetJournal.class);

    /** Process Message */
    private String m_processMsg = null;
    /** Just Prepared Flag */
    private boolean m_justPrepared = false;
    /** Lines */
    private MGAASAssetJournalLine[] lines = null;

    public MGAASAssetJournal(Properties ctx, int GAAS_AssetJournal_ID, String trxName)
    {
        super(ctx, GAAS_AssetJournal_ID, trxName);
        if (GAAS_AssetJournal_ID == 0)
        {
            setBeginningBalance(Env.ZERO);
            setEndingBalance(Env.ZERO);
            setStatementDifference(Env.ZERO);
            setDocAction(DOCACTION_Complete);
            setDocStatus(DOCSTATUS_Drafted);
            //
            Timestamp today = TimeUtil.getDay(System.currentTimeMillis());
            setStatementDate(today);
            setDateAcct(today);
            setIsApproved(false);
            setPosted(false);
            setProcessed(false);
        }
    }

    public MGAASAssetJournal(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }

    public static MGAASAssetJournal get(Properties ctx, int AD_Org_ID,
            String entryType, Timestamp statementDate,
            String trxName)
    {
        MGAASAssetJournal retValue = null;

        String sql = "SELECT * FROM GAAS_AssetJournal aj"
                + " WHERE aj.AD_Org_ID=?"
                + " AND TRUNC(aj.StatementDate, 'DD')=?"
                + " AND aj.Processed='N' AND aj.EntryType=?";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, AD_Org_ID);
            pstmt.setTimestamp(2, TimeUtil.getDay(statementDate));
            pstmt.setString(3, entryType);
            rs = pstmt.executeQuery();
            if (rs.next())
            {
                retValue = new MGAASAssetJournal(ctx, rs, trxName);
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, sql, ex);
            throw new IllegalStateException("Could not get Asset Journal");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        if (retValue != null)
        {
            return retValue;
        }

        retValue = new MGAASAssetJournal(ctx, 0, trxName);
        retValue.setAD_Org_ID(AD_Org_ID);
        retValue.setDateAcct(statementDate);
        retValue.setStatementDate(statementDate);
        retValue.setEntryType(entryType);
        String name = getName(ctx, AD_Org_ID, statementDate);
        retValue.setName(name);
        retValue.saveEx();

        return retValue;
    }

    private static String getName(Properties ctx, int orgId, Timestamp date)
    {
        String name = DisplayType.getDateFormat(DisplayType.Date)
                .format(date) + " " + MOrg.get(ctx, orgId).getValue();
        return name;
    }

    /**
     * Get Lines
     * 
     * @param requery
     *            requery
     * @return lines
     */
    public MGAASAssetJournalLine[] getLines(boolean requery)
    {
        if (lines != null && !requery)
        {
            return lines;
        }

        ArrayList<MGAASAssetJournalLine> list = new ArrayList<MGAASAssetJournalLine>();
        String sql = "SELECT * FROM GAAS_AssetJournalLine WHERE GAAS_AssetJournal_ID=?";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getGAAS_AssetJournal_ID());
            rs = pstmt.executeQuery();
            while (rs.next())
            {
                list.add(new MGAASAssetJournalLine(getCtx(), rs, get_TrxName()));
            }
        }
        catch (Exception ex)
        {
            log.log(Level.SEVERE, sql, ex);
            throw new IllegalStateException("Could not get lines");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        lines = new MGAASAssetJournalLine[list.size()];
        list.toArray(lines);
        return lines;
    } // getLines

    @Override
    public boolean processIt(String action) throws Exception
    {
        m_processMsg = null;
        DocumentEngine engine = new DocumentEngine(this, getDocStatus());
        return engine.processIt(action, getDocAction());
    }

    /**
     * Unlock Document.
     * 
     * @return true if success
     */
    public boolean unlockIt()
    {
        log.info(toString());
        setProcessing(false);
        return true;
    } // unlockIt

    /**
     * Invalidate Document
     * 
     * @return true if success
     */
    public boolean invalidateIt()
    {
        log.info(toString());
        setDocAction(DOCACTION_Prepare);
        return true;
    } // invalidateIt

    @Override
    public String prepareIt()
    {
        log.info(toString());
        m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
                ModelValidator.DOCTIMING_BEFORE_PREPARE);
        if (m_processMsg != null)
        {
            return DocAction.STATUS_Invalid;
        }

        // Check Lines
        MGAASAssetJournalLine[] lines = getLines(false);
        if (lines.length == 0)
        {
            m_processMsg = "@NoLines@";
            return DocAction.STATUS_Invalid;
        }
        
        if (ENTRYTYPE_New.equals(getEntryType()))
        {
            for (MGAASAssetJournalLine line : lines)
            {
                if (line.getA_Asset_Group_ID() == 0)
                {
                    m_processMsg = "No asset group selected on line";
                    return DocAction.STATUS_Invalid;
                }
                
                if ( line.getC_InvoiceLine_ID() <= 0 && (line.getCostAmt() == null || line.getCostAmt().signum() <=0) ) 
                {
                    m_processMsg = "Either cost or invoice line must be provided for new asset";
                    return DocAction.STATUS_Invalid;
                }
            }
        }

        // Check whether the Period is open
        boolean periodOpen = MPeriod.isOpen(getCtx(), getDateAcct(),
                MDocType.DOCBASETYPE_GLJournal, getAD_Org_ID());
        
        if (!periodOpen)
        {
        	m_processMsg = "@PeriodClosed@";
            return DocAction.STATUS_Invalid;
        }

        // Add up Amounts
        BigDecimal difference = Env.ZERO;
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            difference = difference.add(line.getCostAmt());
        }

        // Set the statement difference based on the lines
        setStatementDifference(difference);

        m_justPrepared = true;
        if (!DOCACTION_Complete.equals(getDocAction()))
        {
            setDocAction(DOCACTION_Complete);
        }
        return DocAction.STATUS_InProgress;
    }

    /**
     * Approve Document
     * 
     * @return true if success
     */
    public boolean approveIt()
    {
        log.info(toString());
        setIsApproved(true);
        return true;
    } // approveIt

    /**
     * Reject Approval
     * 
     * @return true if success
     */
    public boolean rejectIt()
    {
        log.info(toString());
        setIsApproved(false);
        return true;
    } // rejectIt

    private boolean completeDepreciationJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        int count = 0;
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }
            count++;
            int deprExpenseId = line.getGAAS_Depreciation_Expense_ID();

            if (deprExpenseId > 0)
            {
                MGAASDepreciationExpense deprExpense = new MGAASDepreciationExpense(
                        getCtx(), deprExpenseId, get_TrxName());
                if(deprExpense.getC_Period_ID() == 0)
                {
                	line.getGAAS_AssetJournal().getC_Period_ID();
                	deprExpense.setC_Period_ID(line.getGAAS_AssetJournal().getC_Period_ID());
                	deprExpense.saveEx();
                }
                deprExpense.setProcessed(true);
                deprExpense.saveEx();
            }
        }

        if (count == 0)
        {
            m_processMsg = "No Active lines to process";
            return false;
        }

        return true;
    }

    private boolean completeTransferJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        int count = 0;
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            count++;

            if (line.getA_Asset_ID() <= 0 || line.getA_Asset_Group_ID() <= 0
                    || line.getOld_Asset_Group_ID() <= 0)
            {
                m_processMsg = "Invalid line";
                return false;
            }
            MAsset asset = new MAsset(getCtx(), line.getA_Asset_ID(),
                    get_TrxName());

            
            if ( asset.isDisposed() )
            {
            	 m_processMsg = "Asset already disposed";
                 return false;
            }
            
            int oldAssetAcctId = MGAASAssetAcct.getDefaultAssetAcctId(getCtx(),
                    line.getA_Asset_ID(), get_TrxName());
            line.setOld_Asset_Acct_ID(oldAssetAcctId);

            MGAASAssetAcct oldAssetAcct = new MGAASAssetAcct(getCtx(),
                    oldAssetAcctId, get_TrxName());
            oldAssetAcct.setIsActive(false);
            oldAssetAcct.setIsTransferred(true);
            oldAssetAcct.saveEx();
            
            asset.setA_Asset_Group_ID(line.getA_Asset_Group_ID());
            asset.saveEx();

            int newAssetAcctId = MGAASAssetAcct.getDefaultAssetAcctId(getCtx(),
                    line.getA_Asset_ID(), get_TrxName());
            line.setNew_Asset_Acct_ID(newAssetAcctId);
            line.saveEx();
        }

        if (count == 0)
        {
            m_processMsg = "No Active lines to process";
            return false;
        }

        return true;
    }

    private boolean completeRevaluationJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        int count = 0;
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            count++;

            if (line.getA_Asset_ID() <= 0)
            {
                m_processMsg = "Invalid line";
                return false;
            }

            MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile.get(
                    getCtx(), line.getA_Asset_ID(), get_TrxName());
            workfile.setAssetCost(line.getCostAmt());
            workfile.saveEx();
        }

        if (count == 0)
        {
            m_processMsg = "No Active lines to process";
            return false;
        }

        return true;
    }

    private boolean completeDisposalJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        int count = 0;
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            count++;

            if (line.getA_Asset_ID() <= 0)
            {
                m_processMsg = "Invalid line";
                return false;
            }

            MAsset asset = new MAsset(getCtx(), line.getA_Asset_ID(),
                    get_TrxName());
            
            if ( asset.isDisposed() )
            {
            	 m_processMsg = "Asset already disposed";
                 return false;
            }
            
            asset.setIsDisposed(true);
            asset.setAssetDisposalDate(getStatementDate());
            asset.saveEx();
        }

        if (count == 0)
        {
            m_processMsg = "No Active lines to process";
            return false;
        }

        return true;
    }

    @Override
    public String completeIt()
    {
        // Re-Check for prepare status
        if (!m_justPrepared)
        {
            String status = prepareIt();
            if (!DocAction.STATUS_InProgress.equals(status))
            {
                return status;
            }
        }

        // Implicit Approval
        if (!isApproved())
            approveIt();
        //
        log.info(toString());
        log.info("Entry type: " + getEntryType());

        // Completion for Depreciation journal (mark depreciation expense as
        // processed)

        boolean success = false;

        if (ENTRYTYPE_New.equals(getEntryType()))
        {
            success = true;
        }
        else if (ENTRYTYPE_Depreciation.equals(getEntryType()))
        {
            success = completeDepreciationJournal();
        }
        else if (ENTRYTYPE_Transfers.equals(getEntryType()))
        {
            success = completeTransferJournal();
        }
        else if (ENTRYTYPE_Revaluation.equals(getEntryType()))
        {
            success = completeRevaluationJournal();
        }
        else if (ENTRYTYPE_Disposal.equals(getEntryType()))
        {
            success = completeDisposalJournal();
        }

        if (!success)
        {
            return DocAction.STATUS_Invalid;
        }

        // User Validation
        String valid = ModelValidationEngine.get().fireDocValidate(this,
                ModelValidator.DOCTIMING_AFTER_COMPLETE);
        if (valid != null)
        {
            m_processMsg = valid;
            return DocAction.STATUS_Invalid;
        }
        //
        setProcessed(true);
        setDocAction(DOCACTION_Close);
        return DocAction.STATUS_Completed;
    }

    /**
     * Delete Accounting
     * 
     * @return number of records
     */
    private int deleteAcct()
    {
        StringBuffer sql = new StringBuffer(
                "DELETE FROM Fact_Acct WHERE AD_Table_ID=")
                .append(get_Table_ID()).append(" AND Record_ID=")
                .append(getGAAS_AssetJournal_ID());
        int no = DB.executeUpdate(sql.toString(), get_TrxName());
        if (no != 0)
        {
            log.info("#=" + no);
        }

        return no;
    } // deleteAcct

    private boolean voidDepreciationJournal()
    {
        String sql = "UPDATE GAAS_Depreciation_Expense SET Processed='N'"
                + " WHERE EXISTS (SELECT * FROM GAAS_AssetJournalLine ajl"
                + " WHERE ajl.GAAS_AssetJournal_ID=?"
                + " AND ajl.GAAS_Depreciation_Expense_ID="
                + "GAAS_Depreciation_Expense.GAAS_Depreciation_Expense_ID)";

        int count = DB.executeUpdate(sql, getGAAS_AssetJournal_ID(),
                get_TrxName());

        if (count < 0)
        {
            log.severe("Could not update Depreciation Expense records");
            return false;
        }

        log.info("Depreciation Expense records updated: " + count);

        sql = "UPDATE GAAS_AssetJournalLine "
                + "SET GAAS_Depreciation_Expense_ID=null "
                + "WHERE GAAS_AssetJournal_ID=?";

        int no = DB
                .executeUpdate(sql, getGAAS_AssetJournal_ID(), get_TrxName());

        if (no < 0)
        {
            log.severe("Could not update Asset Journal lines");
            return false;
        }

        log.info("Lines updated: " + no);
        return true;
    }

    public boolean voidTransferJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            if (line.getA_Asset_ID() <= 0 || line.getA_Asset_Group_ID() <= 0
                    || line.getOld_Asset_Group_ID() <= 0
                    || line.getOld_Asset_Acct_ID() <= 0
                    || line.getNew_Asset_Acct_ID() <= 0)
            {
                m_processMsg = "Invalid line";
                return false;
            }

            MAsset asset = new MAsset(getCtx(), line.getA_Asset_ID(),
                    get_TrxName());
            asset.setA_Asset_Group_ID(line.getOld_Asset_Group_ID());
            asset.saveEx();

            MGAASAssetAcct oldAssetAcct = new MGAASAssetAcct(getCtx(),
                    line.getOld_Asset_Acct_ID(), get_TrxName());
            oldAssetAcct.setIsActive(true);
            oldAssetAcct.setIsTransferred(false);
            oldAssetAcct.saveEx();

            MGAASAssetAcct newAssetAcct = new MGAASAssetAcct(getCtx(),
                    line.getNew_Asset_Acct_ID(), get_TrxName());
            newAssetAcct.setIsActive(false);
            newAssetAcct.setIsTransferred(true);
            newAssetAcct.saveEx();
        }

        return true;
    }

    private boolean voidRevaluationJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            if (line.getA_Asset_ID() <= 0)
            {
                m_processMsg = "Invalid line";
                return false;
            }

            MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile.get(
                    getCtx(), line.getA_Asset_ID(), get_TrxName());
            workfile.setAssetCost(line.getInitialCost());
            workfile.saveEx();
        }

        return true;
    }

    private boolean voidDisposalJournal()
    {
        MGAASAssetJournalLine[] lines = getLines(false);
        for (MGAASAssetJournalLine line : lines)
        {
            if (!line.isActive())
            {
                continue;
            }

            if (line.getA_Asset_ID() <= 0)
            {
                m_processMsg = "Invalid line";
                return false;
            }

            MAsset asset = new MAsset(getCtx(), line.getA_Asset_ID(),
                    get_TrxName());
            asset.setIsDisposed(false);
            asset.setAssetDisposalDate(null);
            asset.saveEx();
        }
        return true;
    }

    /**
     * Void Document. Same as Close.
     * 
     * @return true if success
     */
    public boolean voidIt()
    {
        log.info(toString());

        if (DOCSTATUS_Closed.equals(getDocStatus())
                || DOCSTATUS_Voided.equals(getDocStatus()))
        {
            m_processMsg = "Document Closed: " + getDocStatus();
            setDocAction(DOCACTION_None);
            return false;
        }

        boolean success = false;

        if (ENTRYTYPE_New.equals(getEntryType()))
        {
            success = true;
        }
        else if (ENTRYTYPE_Depreciation.equals(getEntryType()))
        {
            success = voidDepreciationJournal();
        }
        else if (ENTRYTYPE_Transfers.equals(getEntryType()))
        {
            success = voidTransferJournal();
        }
        else if (ENTRYTYPE_Revaluation.equals(getEntryType()))
        {
            success = voidRevaluationJournal();
        }
        else if (ENTRYTYPE_Disposal.equals(getEntryType()))
        {
            success = voidDisposalJournal();
        }

        if (!success)
        {
            return success;
        }

        deleteAcct();
        setPosted(false);
        setProcessed(true);
        setDocAction(DOCACTION_None);
        return true;
    } // voidIt

    /**
     * Close Document. Cancel not delivered Quantities
     * 
     * @return true if success
     */
    public boolean closeIt()
    {
        log.info(toString());
        setDocAction(DOCACTION_None);
        return true;
    } // closeIt

    /**
     * Reverse Correction
     * 
     * @return true if success
     */
    public boolean reverseCorrectIt()
    {
        log.info(toString());
        return false;
    } // reverseCorrectionIt

    /**
     * Reverse Accrual - none
     * 
     * @return true if success
     */
    public boolean reverseAccrualIt()
    {
        log.info(toString());
        return false;
    } // reverseAccrualIt

    /**
     * Re-activate
     * 
     * @return true if success
     */
    public boolean reActivateIt()
    {
    	

		log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;	
		
		MPeriod.testPeriodOpen(getCtx(), getDateAcct(), getC_DocType_ID(), getAD_Org_ID());
		MFactAcct.deleteEx(MGAASAssetJournal.Table_ID, get_ID(), get_TrxName());
		setPosted(false);
		setProcessed(false);
		setDocAction(DOCACTION_Complete);
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		return true;
    
    } // reActivateIt

    /**
     * String Representation
     * 
     * @return info
     */
    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer("GAASAssetJournal[");
        sb.append(get_ID()).append("-").append(getName()).append(", Balance=")
                .append(getBeginningBalance()).append("->")
                .append(getEndingBalance()).append("]");
        return sb.toString();
    } // toString

    /**
     * Get Summary
     * 
     * @return Summary of Document
     */
    public String getSummary()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(getName());
        // : Total Lines = 123.00 (#1)
        sb.append(": ").append(Msg.translate(getCtx(), "BeginningBalance"))
                .append("=").append(getBeginningBalance()).append(",")
                .append(Msg.translate(getCtx(), "EndingBalance")).append("=")
                .append(getEndingBalance()).append(" (#")
                .append(getLines(false).length).append(")");
        // - Description
        if (getDescription() != null && getDescription().length() > 0)
            sb.append(" - ").append(getDescription());
        return sb.toString();
    } // getSummary

    @Override
    public String getDocumentNo()
    {
        return getName();
    }

    @Override
    public String getDocumentInfo()
    {
        return Msg.getElement(getCtx(), "GAAS_AssetJournal_ID") + " "
                + getDocumentNo();
    }

    @Override
    public File createPDF()
    {
        return null;
    }

    @Override
    public String getProcessMsg()
    {
        return m_processMsg;
    }

    @Override
    public int getDoc_User_ID()
    {
        return getCreatedBy();
    }

    @Override
    public int getC_Currency_ID()
    {
        int acctSchemaId = MClientInfo.get(getCtx()).getC_AcctSchema1_ID();
        return MAcctSchema.get(getCtx(), acctSchemaId).getC_Currency_ID();
    }

    @Override
    public BigDecimal getApprovalAmt()
    {
        return getStatementDifference();
    }

    /**
     * Before Save
     * 
     * @param newRecord
     * @return true
     */
    @Override
    protected boolean beforeSave(boolean newRecord)
    {
        // Calculate End Balance
        setEndingBalance(getBeginningBalance().add(getStatementDifference()));

        if (getName() == null || Util.isEmpty(getName()))
        {
            setName(getName(getCtx(), getAD_Org_ID(), getStatementDate()));
        }

        // Clear all the lines
        if (!newRecord
                && (is_ValueChanged("C_Period_ID") || is_ValueChanged("EntryType")))
        {
            String sql = "DELETE FROM GAAS_AssetJournalLine WHERE GAAS_AssetJournal_ID=?";
            int count = DB.executeUpdate(sql, getGAAS_AssetJournal_ID(),
                    get_TrxName());

            if (count < 0)
            {
                log.saveError("Error", "Could not clean lines");
                return false;
            }

            log.saveInfo("Lines", "Lines cleared #" + count);
        }
        return true;
    } // beforeSave
}
