/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.CCache;

import com.aat.model.MResourceTypeBP;
import com.aat.model.MVehicleSize;


/**
 *	Resource Model
 *	
 *  @author Jorg Janke
 *  @version $Id: MResource.java,v 1.2 2006/07/30 00:51:05 jjanke Exp $
 * 
 * @author Teo Sarca, www.arhipac.ro
 * 				<li>FR [ 2051056 ] MResource[Type] should be cached
 * 				<li>BF [ 2227901 ] MRP (Calculate Material Plan) fails if resource is empty
 * 				<li>BF [ 2824795 ] Deleting Resource product should be forbidden
 * 					https://sourceforge.net/tracker/?func=detail&aid=2824795&group_id=176962&atid=879332
 */
public class MResource extends X_S_Resource
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6799272062821593975L;
	/** Cache */
	private static CCache<Integer, MResource> s_cache = new CCache<Integer, MResource>(Table_Name, 20);
	
	/**
	 * Get from Cache
	 * @param ctx
	 * @param S_Resource_ID
	 * @return MResource
	 */
	public static MResource get(Properties ctx, int S_Resource_ID)
	{
		if (S_Resource_ID <= 0)
			return null;
		MResource r = s_cache.get(S_Resource_ID);
		if (r == null) {
			r = new MResource(ctx, S_Resource_ID, null);
			if (r.get_ID() == S_Resource_ID) {
				s_cache.put(S_Resource_ID, r);
			}
		}
		return r;
	}

	/**
	 * 	Standard Constructor
	 *	@param ctx context
	 *	@param S_Resource_ID id
	 */
	public MResource (Properties ctx, int S_Resource_ID, String trxName)
	{
		super (ctx, S_Resource_ID, trxName);
	}	//	MResource

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 */
	public MResource (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MResource
	
	
	/** Cached Resource Type	*/
	private MResourceType	m_resourceType = null;
	/** Cached Product			*/
	private MProduct		m_product = null;
	
	
	/**
	 * 	Get cached Resource Type
	 *	@return Resource Type
	 */
	public MResourceType getResourceType()
	{
		// Use cache if we are outside transaction:
		if (get_TrxName() == null && getS_ResourceType_ID() > 0)
			return MResourceType.get(getCtx(), getS_ResourceType_ID());
		//
		if (m_resourceType == null && getS_ResourceType_ID() != 0) {
			m_resourceType = new MResourceType (getCtx(), getS_ResourceType_ID(), get_TrxName());
		}
		return m_resourceType;
	}	//	getResourceType
	
	/**
	 * 	Get Product (use cache)
	 *	@return product
	 */
	public MProduct getProduct()
	{
		if (m_product == null)
		{
			m_product = MProduct.forS_Resource_ID(getCtx(), getS_Resource_ID(), get_TrxName());
		}
		else
		{
			m_product.set_TrxName(get_TrxName());
		}
		return m_product;
	}	//	getProduct
	
	public int getC_UOM_ID()
	{
		return getProduct().getC_UOM_ID();
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (newRecord)
		{
			if (getValue() == null || getValue().length() == 0)
				setValue(getName());
			m_product = new MProduct(this, getResourceType());
			//M_Locator_ID is now mandatory in the product - jtrinidad
			MLocator def = MLocator.getDefault((MWarehouse) getM_Warehouse());
			m_product.setM_Locator_ID(def.getM_Locator_ID());
			
			m_product.saveEx(get_TrxName());
		}
		//
		// Validate Manufacturing Resource
		if (isManufacturingResource()
				&& MANUFACTURINGRESOURCETYPE_Plant.equals(getManufacturingResourceType())
				&& getPlanningHorizon() <= 0)
		{
			throw new AdempiereException("@"+COLUMNNAME_PlanningHorizon+"@ <= @0@ !");
		}
		return true;
	}	//	beforeSave

	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (!success)
			return success;
			
		MProduct prod = getProduct();
		if (prod.setResource(this))
			prod.saveEx(get_TrxName());
		
		return success;
	}	//	afterSave
	
	@Override
	protected boolean beforeDelete()
	{
		// Delete product
		MProduct product = getProduct();
		if (product != null && product.getM_Product_ID() > 0)
		{
			product.setS_Resource_ID(0); // unlink resource
			product.deleteEx(true);
		}
		return true;
	}

	@Override
	public String toString()
	{
	      StringBuffer sb = new StringBuffer ("MResource[")
	        .append(get_ID())
	        .append(", Value=").append(getValue())
	        .append(", Name=").append(getName())
	        .append("]");
	      return sb.toString();
	}
	
	/**
	 * Create or return existing Resource from Asset
	 * @param asset
	 * @param S_ResourceType_ID
	 * @return
	 */
	public static MResource get(MAsset asset, int S_ResourceType_ID)
	{
		if (asset == null) {
			return null;
		}
		if (asset.getM_Product_ID() == 0) {
			//create new Resource
			MResource resource = new MResource(asset.getCtx(), 0, asset.get_TrxName());
			resource.setS_ResourceType_ID(S_ResourceType_ID);
			MVehicleSize size = MVehicleSize.get(asset.getJTT_VehicleSize_ID());
			String fleetNo = String.format("%s - (%s)", asset.getValue(), size.getSeatsMax());
			resource.setValue(fleetNo);
			resource.setName(asset.getName());
			resource.setM_Warehouse_ID(asset.getSchd_Warehouse_ID());
			resource.save();
			
			asset.setM_Product_ID(resource.getProduct().getM_Product_ID());
			asset.save();
			return resource;
		} else {
			MResource resource = (MResource) asset.getM_Product().getS_Resource();
			resource.setName(asset.getName());  //sync the names again
			resource.save();
		}
		
		return null;
	}
	
	
	/**
	 * Create or return existing Resource from BPartner / Employee
	 * @param asset
	 * @param S_ResourceType_ID
	 * @return
	 */
	public static MResource get(MBPartner bp, int S_ResourceType_ID, boolean create)
	{
		
		if (create) {
			//create new Resource
			MResource resource = new MResource(bp.getCtx(), 0, bp.get_TrxName());
			resource.setS_ResourceType_ID(S_ResourceType_ID);
			resource.setValue(bp.getValue()+ "-" + resource.getResourceType().getValue());
			resource.setName(bp.getName());
			resource.setM_Warehouse_ID(bp.getM_Warehouse_ID());
			MUser[] users = MUser.getOfBPartner(bp.getCtx(), bp.getC_BPartner_ID(), bp.get_TrxName());
			if (users.length > 0) {
				resource.setAD_User_ID(users[0].getAD_User_ID());
			}
			resource.save();
			
			return resource;
		} else {
			MResourceTypeBP rbp = new Query (bp.getCtx(), MResourceTypeBP.Table_Name
					, "C_BPartner_ID = ? AND S_ResourceType_ID = ?", bp.get_TrxName())
					.setParameters(bp.getC_BPartner_ID(), S_ResourceType_ID)
					.firstOnly();
			if  (rbp != null) {
				return  (MResource) rbp.getM_Product().getS_Resource();
			}
		}

		return null;
	}
}	//	MResource
