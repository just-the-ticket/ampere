/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 * Contributor(s): Carlos Ruiz - globalqss                                    *
 *****************************************************************************/
package org.compiere.model;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.model.GenericPO;
import org.apache.commons.lang.StringUtils;
import org.compiere.util.CCache;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Util;

/**
 *	Persistent Table Model
 * <p>
 * Change log:
 * <ul>
 * <li>2007-02-01 - teo_sarca - [ 1648850 ] MTable.getClass works incorrect for table "Fact_Acct"
 * </ul>
 * <ul>
 * <li>2007-08-30 - vpj-cd - [ 1784588 ] Use ModelPackage of EntityType to Find Model Class
 * </ul>
 *  @author Jorg Janke
 *  @author Teo Sarca, teo.sarca@gmail.com
 *  		<li>BF [ 3017117 ] MTable.getClass returns bad class
 *  			https://sourceforge.net/tracker/?func=detail&aid=3017117&group_id=176962&atid=879332
 *  		<li>BF [ 3133032 ] Adempiere is not loading classes from org.compiere.report
 *  			https://sourceforge.net/tracker/?func=detail&aid=3133032&group_id=176962&atid=879332
 *  @version $Id: MTable.java,v 1.3 2006/07/30 00:58:04 jjanke Exp $
 */
public class MTable extends X_AD_Table
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2367316254623142732L;

	/**
	 * minimum number of characters before firing Token Search
	 */
	public static final int TS_MIN_CHARACTERS = 2;
	
	/**
	 * 	Get Table from Cache
	 *	@param ctx context
	 *	@param AD_Table_ID id
	 *	@return MTable
	 */
	public static MTable get (Properties ctx, int AD_Table_ID)
	{
		Integer key = AD_Table_ID;
		MTable retValue = s_cache.get (key);
		if (retValue != null && retValue.getCtx() == ctx) {
			return retValue;
		}
		retValue = new MTable (ctx, AD_Table_ID, null);
		if (retValue.get_ID () != 0) {
			s_cache.put (key, retValue);
		}
		return retValue;
	}	//	get

	/**
	 * 	Get Table from Cache
	 *	@param ctx context
	 *	@param tableName case insensitive table name
	 *	@return Table
	 */
	public static MTable get (Properties ctx, String tableName)
	{
		if (tableName == null)
			return null;
		Iterator<MTable> it = s_cache.values().iterator();
		while (it.hasNext())
		{
			MTable retValue = it.next();
			if (tableName.equalsIgnoreCase(retValue.getTableName()) 
					&& retValue.getCtx() == ctx 
				) 
			{
				return retValue;
		}
		}
		//
		MTable retValue = null;
		String sql = "SELECT * FROM AD_Table WHERE UPPER(TableName)=?";
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, null);
			pstmt.setString(1, tableName.toUpperCase());
			ResultSet rs = pstmt.executeQuery ();
			if (rs.next ())
				retValue = new MTable (ctx, rs, null);
			rs.close ();
			pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, sql, e);
		}
		try
		{
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			pstmt = null;
		}
		
		if (retValue != null)
		{
			Integer key = retValue.getAD_Table_ID();
			s_cache.put (key, retValue);
		}
		return retValue;
	}	//	get
	
	/**
	 * 	Get Table Name
	 *	@param ctx context
	 *	@param AD_Table_ID table
	 *	@return tavle name
	 */
	public static String getTableName (Properties ctx, int AD_Table_ID)
	{
		return MTable.get(ctx, AD_Table_ID).getTableName();
	}	//	getTableName
	
	
	/**	Cache						*/
	private static CCache<Integer,MTable> s_cache = new CCache<Integer,MTable>("AD_Table", 20);
	private static CCache<String,Class<?>> s_classCache = new CCache<String,Class<?>>("PO_Class", 20);
	
	/**	Static Logger	*/
	private static CLogger	s_log	= CLogger.getCLogger (MTable.class);
	
	/**	Packages for Model Classes	*/
	private static final String[]	s_packages = new String[] {

		"org.compiere.model", "org.compiere.wf", 
		"org.compiere.report", // teo_sarca BF[3133032]
		"org.compiere.print", "org.compiere.impexp",
		"compiere.model",			//	globalqss allow compatibility with other plugins 	
		"adempiere.model",			//	Extensions
		"org.adempiere.model"
		, "com.aat.model"
	};
	
	/**	Special Classes				*/
	private static final String[]	s_special = new String[] {
		"AD_Element", "org.compiere.model.M_Element",
		"AD_Registration", "org.compiere.model.M_Registration",
		"AD_Tree", "org.compiere.model.MTree_Base",
		"R_Category", "org.compiere.model.MRequestCategory",
		"GL_Category", "org.compiere.model.MGLCategory",
		"K_Category", "org.compiere.model.MKCategory",
		"C_ValidCombination", "org.compiere.model.MAccount",
		"C_Phase", "org.compiere.model.MProjectTypePhase",
		"C_Task", "org.compiere.model.MProjectTypeTask",
		"AD_View_Column", "org.adempiere.model.MViewColumn",
		"AD_View","org.adempiere.model.MView",
		"AD_View_Definition","org.adempiere.model.MViewDefinition",
		"AD_Browse","org.adempiere.model.MBrowse",
		"AD_Browse_Field","org.adempiere.model.MBrowseField",
		"T_Selection","org.adempiere.model.X_T_Selection"
	//	AD_Attribute_Value, AD_TreeNode
	};
	
	/**
	 * 	Get Persistence Class for Table
	 *	@param tableName table name
	 *	@return class or null
	 */
	public static Class<?> getClass (String tableName)
	{
		//	Not supported
		if (tableName == null || tableName.endsWith("_Trl"))
			return null;
		
		//check cache
		Class<?> cache = s_classCache.get(tableName);
		if (cache != null) 
		{
			//Object.class indicate no generated PO class for tableName
			if (cache.equals(Object.class))
				return null;
			else
				return cache;
		}

		MTable table = MTable.get(Env.getCtx(), tableName);
		String entityType = table.getEntityType();
		
		//	Import Tables (Name conflict)
		//  Import Tables doesn't manage model M classes, just X_
		if (tableName.startsWith("I_"))
		{
			MEntityType et = MEntityType.get(Env.getCtx(), entityType);
			String etmodelpackage = et.getModelPackage();
			if (etmodelpackage == null || MEntityType.ENTITYTYPE_Dictionary.equals(entityType))
				etmodelpackage = "org.compiere.model"; // fallback for dictionary or empty model package on entity type
			Class<?> clazz = getPOclass(etmodelpackage + ".X_" + tableName, tableName);
			if (clazz != null)
			{
				s_classCache.put(tableName, clazz);
				return clazz;
			}
			s_log.warning("No class for table: " + tableName);
			return null;
		}
		
		//	Special Naming
		for (int i = 0; i < s_special.length; i++)
		{
			if (s_special[i++].equals(tableName))
			{
				Class<?> clazz = getPOclass(s_special[i], tableName);
				if (clazz != null)
				{
					s_classCache.put(tableName, clazz);
					return clazz;
				}
				break;
			}
		}
		

		//	Strip table name prefix (e.g. AD_) Customizations are 3/4
		String className = tableName;
		int index = className.indexOf('_');
		if (index > 0)
		{
			if (index <= 3)		//	AD_, A_
				 className = className.substring(index+1);
		}
		//	Remove underlines
		className = Util.replace(className, "_", "");
	
		//	Search packages
		for (int i = 0; i < s_packages.length; i++)
		{
			StringBuilder name = new StringBuilder(s_packages[i]).append(".M").append(className);
			Class<?> clazz = getPOclass(name.toString(), tableName);
			if (clazz != null)
			{
				s_classCache.put(tableName, clazz);
				return clazz;
			}
		}
		
		
		//	Adempiere Extension
		Class<?> clazz = getPOclass("adempiere.model.X_" + tableName, tableName);
		if (clazz != null)
		{
			s_classCache.put(tableName, clazz);
			return clazz;
		}
		
		//hengsin - allow compatibility with compiere plugins
		//Compiere Extension
		clazz = getPOclass("compiere.model.X_" + tableName, tableName);
		if (clazz != null)
		{
			s_classCache.put(tableName, clazz);
			return clazz;
		}

		//	Default
		clazz = getPOclass("org.compiere.model.X_" + tableName, tableName);
		if (clazz != null)
		{
			s_classCache.put(tableName, clazz);
			return clazz;
		}

		//Object.class to indicate no PO class for tableName
		s_classCache.put(tableName, Object.class);
		return null;
	}	//	getClass
	
	/**
	 * Get PO class
	 * @param className fully qualified class name
	 * @param tableName Optional. If specified, the loaded class will be validated for that table name
	 * @return class or null
	 */
	private static Class<?> getPOclass (String className, String tableName)
	{
		try
		{
			Class<?> clazz = Class.forName(className);
			// Validate if the class is for specified tableName
			if (tableName != null)
			{
				String classTableName = clazz.getField("Table_Name").get(null).toString();
				if (!tableName.equals(classTableName))
				{
					s_log.finest("Invalid class for table: " + className+" (tableName="+tableName+", classTableName="+classTableName+")");
					return null;
				}
			}
			//	Make sure that it is a PO class
			Class<?> superClazz = clazz.getSuperclass();
			while (superClazz != null)
			{
				if (superClazz == PO.class)
				{
					s_log.fine("Use: " + className);
					return clazz;
				}
				superClazz = superClazz.getSuperclass();
			}
		}
		catch (Exception e)
		{
		}
		s_log.finest("Not found: " + className);
		return null;
	}	//	getPOclass

	
	/**************************************************************************
	 * 	Standard Constructor
	 *	@param ctx context
	 *	@param AD_Table_ID id
	 *	@param trxName transaction
	 */
	public MTable (Properties ctx, int AD_Table_ID, String trxName)
	{
		super (ctx, AD_Table_ID, trxName);
		if (AD_Table_ID == 0)
		{
		//	setName (null);
		//	setTableName (null);
			setAccessLevel (ACCESSLEVEL_SystemOnly);	// 4
			setEntityType (ENTITYTYPE_UserMaintained);	// U
			setIsChangeLog (false);
			setIsDeleteable (false);
			setIsHighVolume (false);
			setIsSecurityEnabled (false);
			setIsView (false);	// N
			setReplicationType (REPLICATIONTYPE_Local);
		}	
	}	//	MTable

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName transaction
	 */
	public MTable (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MTable
	
	/**	Columns				*/
	private MColumn[]	m_columns = null;
	private String m_displayTSSQL = null;
	
	/**
	 * 	Get Columns
	 *	@param requery requery
	 *	@return array of columns
	 */
	public MColumn[] getColumns (boolean requery)
	{
		if (m_columns != null && !requery)
			return m_columns;
		String sql = "SELECT * FROM AD_Column WHERE AD_Table_ID=? ORDER BY ColumnName";
		ArrayList<MColumn> list = new ArrayList<MColumn>();
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt (1, getAD_Table_ID());
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ())
				list.add (new MColumn (getCtx(), rs, get_TrxName()));
			rs.close ();
			pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		try
		{
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			pstmt = null;
		}
		//
		m_columns = new MColumn[list.size ()];
		list.toArray (m_columns);
		return m_columns;
	}	//	getColumns
	
	/**
	 * 	Get Column
	 *	@param columnName (case insensitive)
	 *	@return column if found
	 */
	public MColumn getColumn (String columnName)
	{
		if (columnName == null || columnName.length() == 0)
			return null;
		getColumns(false);
		//
		for (int i = 0; i < m_columns.length; i++)
		{
			if (columnName.equalsIgnoreCase(m_columns[i].getColumnName()))
				return m_columns[i];
		}
		return null;
	}	//	getColumn
	
	/**
	 * 	Table has a single Key
	 *	@return true if table has single key column
	 */
	public boolean isSingleKey()
	{
		String[] keys = getKeyColumns();
		return keys.length == 1;
	}	//	isSingleKey
	
	/**
	 * 	Get Key Columns of Table
	 *	@return key columns
	 */
	public String[] getKeyColumns()
	{
		getColumns(false);
		ArrayList<String> list = new ArrayList<String>();
		//
		for (int i = 0; i < m_columns.length; i++)
		{
			MColumn column = m_columns[i];
			if (column.isKey())
				return new String[]{column.getColumnName()};
			if (column.isParent())
				list.add(column.getColumnName());
		}
		String[] retValue = new String[list.size()];
		retValue = list.toArray(retValue);
		return retValue;
	}	//	getKeyColumns
	
	/**
	 * 	Get Identifier Columns of Table
	 *	@return Identifier columns
	 */
	public String[] getIdentifierColumns() {
		ArrayList<KeyNamePair> listkn = new ArrayList<KeyNamePair>();
		for (MColumn column : getColumns(false)) {
			if (column.isIdentifier())
				listkn.add(new KeyNamePair(column.getSeqNo(), column.getColumnName()));
		}
		// Order by SeqNo
		Collections.sort(listkn, new Comparator<KeyNamePair>(){
			public int compare(KeyNamePair s1,KeyNamePair s2){
				if (s1.getKey() < s2.getKey())
					return -1;
				else if (s1.getKey() > s2.getKey())
					return 1;
				else
					return 0;
			}});
		String[] retValue = new String[listkn.size()];
		for (int i = 0; i < listkn.size(); i++) {
			retValue[i] = listkn.get(i).getName();
		}
		return retValue;
	}	//	getIdentifierColumns
	
	/**************************************************************************
	 * 	Get PO Class Instance
	 *	@param Record_ID record
	 *	@param trxName
	 *	@return PO for Record or null
	 */
	public PO getPO (int Record_ID, String trxName)
	{
		// allow GridTable insert on multi-key
		if ( Record_ID == -2 && !isSingleKey())
			Record_ID = 0;
		
		String tableName = getTableName();
		if (Record_ID != 0 && !isSingleKey())
		{
			log.log(Level.WARNING, "(id) - Multi-Key " + tableName);
			return null;
		}
		Class<?> clazz = getClass(tableName);
		if (clazz == null)
		{
			//log.log(Level.WARNING, "(id) - Class not found for " + tableName);
			//return null;
			log.log(Level.INFO, "Using GenericPO for " + tableName);
			GenericPO po = new GenericPO(tableName, getCtx(), Record_ID, trxName);
			return po;
		}
		
		boolean errorLogged = false;
		try
		{
			Constructor<?> constructor = null;
			try
			{
				constructor = clazz.getDeclaredConstructor(new Class[]{Properties.class, int.class, String.class});
			}
			catch (Exception e)
			{
				String msg = e.getMessage();
				if (msg == null)
					msg = e.toString();
				log.warning("No transaction Constructor for " + clazz + " (" + msg + ")");
			}
			
			PO po = (PO)constructor.newInstance(new Object[] {getCtx(), Record_ID, trxName});
			if (po != null && po.get_ID() != Record_ID && Record_ID > 0)
				return null;
			return po;
		}
		catch (Exception e)
		{
			if (e.getCause() != null)
			{
				Throwable t = e.getCause();
				log.log(Level.SEVERE, "(id) - Table=" + tableName + ",Class=" + clazz, t);
				errorLogged = true;
				if (t instanceof Exception)
					log.saveError("Error", (Exception)e.getCause());
				else
					log.saveError("Error", "Table=" + tableName + ",Class=" + clazz);
			}
			else
			{
				log.log(Level.SEVERE, "(id) - Table=" + tableName + ",Class=" + clazz, e);
				errorLogged = true;
				log.saveError("Error", "Table=" + tableName + ",Class=" + clazz);
			}
		}
		if (!errorLogged)
			log.log(Level.SEVERE, "(id) - Not found - Table=" + tableName 
				+ ", Record_ID=" + Record_ID);
		return null;
	}	//	getPO
	
	/**
	 * 	Get PO Class Instance
	 *	@param rs result set
	 *	@param trxName transaction
	 *	@return PO for Record or null
	 */
	public PO getPO (ResultSet rs, String trxName)
	{
		String tableName = getTableName();
		Class<?> clazz = getClass(tableName);
		if (clazz == null)
		{
			//log.log(Level.SEVERE, "(rs) - Class not found for " + tableName);
			//return null;
			log.log(Level.INFO, "Using GenericPO for " + tableName);
			GenericPO po = new GenericPO(tableName, getCtx(), rs, trxName);
			return po;
		}
		
		boolean errorLogged = false;
		try
		{
			Constructor<?> constructor = clazz.getDeclaredConstructor(new Class[]{Properties.class, ResultSet.class, String.class});
			PO po = (PO)constructor.newInstance(new Object[] {getCtx(), rs, trxName});
			return po;
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "(rs) - Table=" + tableName + ",Class=" + clazz, e);
			errorLogged = true;
			log.saveError("Error", "Table=" + tableName + ",Class=" + clazz);
		}
		if (!errorLogged)
			log.log(Level.SEVERE, "(rs) - Not found - Table=" + tableName);
		return null;
	}	//	getPO

	/**
	 * 	Get PO Class Instance
	 *	@param whereClause where clause
	 *	@param trxName transaction
	 *	@return PO for Record or null
	 */
	public PO getPO (String whereClause, String trxName)
	{
		return getPO(whereClause, null, trxName);
	}	//	getPO
	
	/**
	 * Get PO class instance
	 * @param whereClause
	 * @param params
	 * @param trxName
	 * @return
	 */
	public PO getPO(String whereClause, Object[] params, String trxName)
	{
		if (whereClause == null || whereClause.length() == 0)
			return null;
		//
		PO po = null;
		POInfo info = POInfo.getPOInfo(getCtx(), getAD_Table_ID(), trxName);
		if (info == null) return null;
		StringBuffer sqlBuffer = info.buildSelect();
		sqlBuffer.append(" WHERE ").append(whereClause);
		String sql = sqlBuffer.toString(); 
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, trxName);
			if (params != null && params.length > 0) 
			{
				for (int i = 0; i < params.length; i++)
				{
					pstmt.setObject(i+1, params[i]);
				}
			}
			ResultSet rs = pstmt.executeQuery ();
			if (rs.next ())
			{
				po = getPO(rs, trxName);
			}
			rs.close ();
			pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
			log.saveError("Error", e);
		}
		try
		{
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			pstmt = null;
		}
		
		return po;
	}
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{
		if (isView() && isDeleteable())
			setIsDeleteable(false);
		//
		return true;
	}	//	beforeSave
	
	/**
	 * 	After Save
	 *	@param newRecord new
	 *	@param success success
	 *	@return success
	 */
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		//	Sync Table ID
		if (newRecord)
		{
			createMandatoryColumns();
			
			MSequence seq = MSequence.get(getCtx(), getTableName(), get_TrxName());
			if (seq == null || seq.get_ID() == 0)
			{	
				MSequence.createTableSequence(getCtx(), getTableName(), get_TrxName(), false);
			}
			
		}
		else
		{
			MSequence seq = MSequence.get(getCtx(), getTableName(), get_TrxName());
			if (seq == null || seq.get_ID() == 0)
				MSequence.createTableSequence(getCtx(), getTableName(), get_TrxName(), false);
			else if (!seq.getName().equals(getTableName()))
			{
				seq.setName(getTableName());
				seq.saveEx();
			}
		}	
		
		return success;
	}	//	afterSave
	
	/**
	 * 	Get SQL Create
	 *	@return create table DDL
	 */
	public String getSQLCreate()
	{
		StringBuilder sb = new StringBuilder("CREATE TABLE ")
			.append(getTableName()).append(" (");
		//
		boolean hasPK = false;
		boolean hasParents = false;
		StringBuilder constraints = new StringBuilder();
		getColumns(true);
		for (int i = 0; i < m_columns.length; i++)
		{
			MColumn column = m_columns[i];
			String colSQL = column.getSQLDDL();
			if ( colSQL != null )
			{
				if (i > 0)
					sb.append(", ");
					sb.append(column.getSQLDDL());
			}
			else // virtual column
				continue;
			//
			if (column.isKey())
				hasPK = true;
			if (column.isParent())
				hasParents = true;
			String constraint = column.getPrimaryKeyConstraint(getTableName());
			if (constraint != null && constraint.length() > 0)
				constraints.append(", ").append(constraint);
		}
		//	Multi Column PK 
		if (!hasPK && hasParents)
		{
			StringBuilder cols = new StringBuilder();
			for (int i = 0; i < m_columns.length; i++)
			{
				MColumn column = m_columns[i];
				if (!column.isParent())
					continue;
				if (cols.length() > 0)
					cols.append(", ");
				cols.append(column.getColumnName());
			}
			sb.append(", CONSTRAINT ")
				.append(getTableName()).append("_Key PRIMARY KEY (")
				.append(cols).append(")");
		}

		sb.append(constraints)
			.append(")");
		return sb.toString();
	}	//	getSQLCreate
	
	// globalqss
	/**
	 * 	Grant independence to GenerateModel from AD_Table_ID
	 *	@param String tableName
	 *	@return int retValue
	 */
	public static int getTable_ID(String tableName) {
		int retValue = 0;
		String SQL = "SELECT AD_Table_ID FROM AD_Table WHERE tablename = ?";
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(SQL, null);
			pstmt.setString(1, tableName);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
				retValue = rs.getInt(1);
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, SQL, e);
			retValue = -1;
		}
		return retValue;
	}
	
	/**
	 * Create query to retrieve one or more PO.
	 * @param whereClause
	 * @param trxName
	 * @return Query
	 */
	public Query createQuery(String whereClause, String trxName) 
	{
		return new Query(this.getCtx(), this, whereClause, trxName);
	}

	/*
	 * Create Mandatory Fields
	 */
	public void createMandatoryColumns()
	{		
		MColumn column = null;
		//M_Element.get(getCtx(),COLUMNNAME_AD_Client_ID);
		
		column = new MColumn(this, COLUMNNAME_AD_Client_ID	, 22 , DisplayType.TableDir , "@#AD_Client_ID@");
		column.setUpdateable(false);
		column.setAD_Val_Rule_ID(129);
		column.saveEx();
		column = new MColumn(this, COLUMNNAME_AD_Org_ID	, 22 , DisplayType.TableDir , "@#AD_Org_ID@");
		column.setUpdateable(true);
		column.setAD_Val_Rule_ID(104);
		column.saveEx();
		column = new MColumn(this, COLUMNNAME_IsActive	, 1 , DisplayType.YesNo , "Y");
		column.setUpdateable(true);
		column.saveEx();
		column = new MColumn(this, COLUMNNAME_Created	, 7 , DisplayType.DateTime , "");
		column.saveEx();		
		column = new MColumn(this, COLUMNNAME_Updated	, 7 , DisplayType.DateTime , "");
		column.saveEx();
		column = new MColumn(this, COLUMNNAME_CreatedBy	, 22 , DisplayType.TableDir, "");
		column.setAD_Reference_Value_ID(110);
		column.saveEx();
		column = new MColumn(this, COLUMNNAME_UpdatedBy	, 22 , DisplayType.TableDir, "");
		column.setAD_Reference_Value_ID(110);
		column.saveEx();
		if(!isView())
		{	
			if(getTableName().endsWith("_Trl") || getTableName().endsWith("_Access"))
				return;
			
			M_Element element = M_Element.get(getCtx(), getTableName()+"_ID", get_TrxName());
			if(element != null)
				return;				
			element = new M_Element(getCtx(), 0 , get_TrxName());
			element.setColumnName(getTableName()+"_ID");
			element.setName(getName() + " ID");
			element.setPrintName(getName() + " ID");
			element.setEntityType(getEntityType());
			element.saveEx();
			
			column = new MColumn(this, element.getColumnName(), 22 , DisplayType.ID, "");
			column.setAD_Element_ID(element.get_ID());
			column.setIsKey(true);
			column.setUpdateable(false);
			column.setIsMandatory(true);
			column.saveEx();
		}	
	}

	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder ("MTable[");
		sb.append (get_ID()).append ("-").append (getTableName()).append ("]");
		return sb.toString ();
	}	//	toString
	

	/**
	 * Check if the table already has tscolumn by looking on the metadata
	 * @return
	 */
	public boolean isTSColumnExists()
	{
		ResultSet rs = null;
		Connection conn = DB.getConnectionRO();
		try {
			DatabaseMetaData md = conn.getMetaData();
			String catalog = DB.getDatabase().getCatalog();
			String schema = DB.getDatabase().getSchema();
			String tableName = getTableName();
			String columnName = "tscolumn";
			if (md.storesUpperCaseIdentifiers())
			{
				tableName = tableName.toUpperCase();
				columnName = columnName.toUpperCase();
			}
			else if (md.storesLowerCaseIdentifiers())
			{
				tableName = tableName.toLowerCase();
			}
			
			rs = md.getColumns(catalog, schema, tableName, columnName);
			if (rs.next()) {
				return true;
			}
			
		} catch (Exception e) {
			
			log.severe(e.getMessage());;
		}
		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			DB.close(rs);
		}
		
		return false;
	}
	
	/**
	 * Dynamically create the tscolumn to the specified table
	 */
	public void createTSColumn() {
		if (isTSColumnExists())
			return;

		StringBuilder sql = new StringBuilder("ALTER TABLE ").append(getTableName()).append(" ADD tscolumn tsvector");

		DB.executeUpdateEx(sql.toString(), get_TrxName());

		sql = new StringBuilder("CREATE INDEX ").append(getTableName() + "_tscolumn_idx").append(" ON ")
				.append(getTableName()).append(" USING gin (tscolumn)");

		DB.executeUpdateEx(sql.toString(), get_TrxName());

	}
	
	public void populateTSColumn()
	{
		createTSColumn();
		
		String sql = getUpdateTSColumnSQL(null);
		
		DB.executeUpdateEx(sql.toString(), get_TrxName());
	}
	
	
	public String getUpdateTSColumnSQL(String where)
	{
		MColumn[] cols = getTSColumns();
		String colList = "";
		
		log.info("Populate for Text Search, Table=" + getTableName() );
		for (MColumn col : cols) {
				if ((col.getAD_Reference_ID() == DisplayType.String 
						|| col.getAD_Reference_ID() == DisplayType.Text
						|| col.getAD_Reference_ID() == DisplayType.TextLong
						|| col.getAD_Reference_ID() == DisplayType.TextLongStretchable
						|| col.getAD_Reference_ID() == DisplayType.Memo
					)	&& !col.isVirtualColumn()) {
					if (colList.length() > 0) {
						colList = colList + ", ";
					}
					colList = colList +  col.getColumnName();
				} else {
					log.warning(getTableName() + "->" + col.getColumnName() + " TS not supported for this column.");
				}
			
		}
		StringBuilder sql = new StringBuilder ("UPDATE ")
				.append(getTableName())
				.append(" SET tscolumn = to_tsvector(concat_ws(' ', "  + colList + "))");
		
		if (where != null) {
			sql.append(" WHERE " + where);
		}
		return sql.toString();
	}
	
	
	
	
	
	
	
	public String getTSDisplaySQL()
	{
		
		if (m_displayTSSQL != null)
			return m_displayTSSQL;
		
		MColumn[] cols = getTSColumns();
		if (cols.length ==0) {
			m_displayTSSQL = null;
			return m_displayTSSQL;
		}
			
		m_displayTSSQL = "";
		String[] preferredOrder = {"value", "documentno", "name"};
		 ArrayList<String> list = new ArrayList<String>();
		 
		// add preferred columns first
		for (String c : preferredOrder) {
			for (MColumn col : cols) {
				if (c.equalsIgnoreCase(col.getColumnName())) {
					list.add(col.getColumnName());
				}
			}
		}
		
		// add remaining columns
		for (MColumn col : cols) {
			if (!list.contains(col.getColumnName())) {
				list.add(col.getColumnName());
			}
		}

		m_displayTSSQL = "concat_ws('_', " + StringUtils.join(list.iterator(), ",") + ")";		
		return m_displayTSSQL;
	}
	
	public String getTSVDefinition()
	{
		return getTSVDefinition(null);
	}
	
	//add alias option in case you get an ambiguous error message
	public String getTSVDefinition(String alias)
	{
		MColumn[] cols = getTSColumns();
		StringBuilder sb = new StringBuilder();
		ArrayList<String> list = new ArrayList<String>();
		for (MColumn col : cols) {
			if (!list.contains(col.getColumnName())) {
				if (col.isMandatory()) {
					list.add(alias== null ? col.getColumnName() : alias + "." + col.getColumnName());
				} else {
					//it's possible to get null value - to_tsvector will not find it
					list.add("COALESCE(" + (alias== null ? col.getColumnName() : alias + "." + col.getColumnName()) + ", '')");
				}
			}
		}
		sb.append("to_tsvector('english', ");
		sb.append(StringUtils.join(list.iterator(), " || ' ' || "));
		sb.append(")");
		return sb.toString();
	}
	
	public MColumn[] getTSColumns ()
	{
		String sql = "SELECT * FROM AD_Column WHERE isIncludeinTS='Y' AND AD_Table_ID=? ORDER BY ColumnName";
		ArrayList<MColumn> list = new ArrayList<MColumn>();
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt (1, getAD_Table_ID());
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ())
				list.add (new MColumn (getCtx(), rs, get_TrxName()));
			rs.close ();
			pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		try
		{
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			pstmt = null;
		}
		//
		MColumn[] tsColumns = new MColumn[list.size ()];
		list.toArray (tsColumns);
		return tsColumns;
	}	//	getTSColumns
	
	
	/** Is this a table that has existing valid record with ID = 0
	 * 
	 * @return
	 */
	public static boolean isTableWithZeroId (String tablename)
	{
		if (tablename.equals("AD_Client") ||
				tablename.equals("AD_Org") ||
				tablename.equals("AD_ReportView") ||
				tablename.equals("AD_Role") ||
				tablename.equals("AD_System") ||
				tablename.equals("AD_User") ||
				tablename.equals("C_DocType") ||
				tablename.equals("GL_Category") ||
				tablename.equals("M_AttributeSet") ||
				tablename.equals("M_AttributeSetInstance")) {
			return true;
		}
		return false;
	}
}	//	MTable