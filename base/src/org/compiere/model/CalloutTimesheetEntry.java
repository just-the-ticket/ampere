package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;

public class CalloutTimesheetEntry extends CalloutEngine {

	/**
	 *	Timesheet Entry Resource selected
	 *  sets user and bpartner from resource
	 *  @param ctx context
	 *  @param WindowNo current Window No
	 *  @param mTab Grid Tab
	 *  @param mField Grid Field
	 *  @param value New Value
	 *  @return null or error message
	 */
	public String resource (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		Integer S_Resource_ID = (Integer)value;
		if (S_Resource_ID == null || S_Resource_ID.intValue() == 0)
			return "";
		
		String sql = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{

			//	get resource user, bpartner and product
			sql = "SELECT r.AD_User_ID, u.C_BPartner_ID, p.M_Product_ID "
				+ " FROM S_Resource r "
				+ " INNER JOIN AD_User u ON (r.AD_User_ID=u.AD_User_ID) "
				+ " LEFT OUTER JOIN M_Product p ON (r.S_Resource_ID=p.S_Resource_ID) "
				+ " WHERE r.S_Resource_ID=?";		//	1
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, S_Resource_ID.intValue());
			
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				int userId = rs.getInt(1);
				int bpartnerId = rs.getInt(2);
				int productId = rs.getInt(3);

				if ( userId > 0 )
					mTab.setValue("AD_User_ID", userId);
				else
					return "Resource not linked to user";
				if ( bpartnerId > 0 )
					mTab.setValue("C_BPartner_ID", bpartnerId);
				else
					return "Resource user not linked to BP";
				
				if ( productId > 0 )
					mTab.setValue("M_Product_ID", productId);
			}
			else 
				return "Resource user not found";
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
			return e.getLocalizedMessage();
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return "";
	}	//	resource
	
}
