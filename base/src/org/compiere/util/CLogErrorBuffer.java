/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.util;

import java.util.LinkedList;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *	Client Error Buffer
 *	
 *  @author Jorg Janke
 *  @version $Id: CLogErrorBuffer.java,v 1.3 2006/07/30 00:54:36 jjanke Exp $
 * 
 * @author Teo Sarca, teo.sarca@gmail.com
 * 		<li>BF [ 2973298 ] NPE on CLogErrorBuffer
 */
public class CLogErrorBuffer extends Handler
{
	private static final String LOGS_KEY = "org.compiere.util.CLogErrorBuffer.logs";


	/**
	 * 	Get Client Log Handler
	 *	@param create create if not exists
	 * 	@return handler
	 */
	public static CLogErrorBuffer get(boolean create)
	{
		if (s_handler == null && create)
			s_handler = new CLogErrorBuffer();
		return s_handler;
	}	//	get

	/**	Appender				*/
	private static CLogErrorBuffer	s_handler;

	
	/**************************************************************************
	 * 	Constructor
	 */
	public CLogErrorBuffer ()
	{
		if (s_handler == null)
			s_handler = this;
		else
			reportError("Error Handler exists already", 
				new IllegalStateException("Existing Handler"), 
				ErrorManager.GENERIC_FAILURE);
		initialize();
	}	//	CLogErrorBuffer

	/** Log Size					*/
	private static int		LOG_SIZE = 100;
	
    /**
     * 	Initialize
     */
    private void initialize()
    {
    	//	Formatting
		setFormatter(CLogFormatter.get());
		//	Default Level
		super.setLevel(Level.INFO);
    }	//	initialize

    
	/**
	 *	Set Level.
	 *	Ignore OFF - and higher then FINE
	 *	@see java.util.logging.Handler#setLevel(java.util.logging.Level)
	 *	@param newLevel ignored
	 *	@throws java.lang.SecurityException
	 */
	public synchronized void setLevel (Level newLevel)
		throws SecurityException
	{
		if (newLevel == null)
			return;
		
		if ( Level.FINE.intValue() >= newLevel.intValue() )
			LOG_SIZE = 1000;
		else
			LOG_SIZE = 100;
		
		if (newLevel == Level.OFF)
			super.setLevel(Level.SEVERE);
		else if (newLevel == Level.ALL || newLevel == Level.FINEST || newLevel == Level.FINER)
			super.setLevel(Level.FINE);
		else
			super.setLevel(newLevel);
	}	//	SetLevel
    
	/**
	 *	Publish
	 *	@see java.util.logging.Handler#publish(java.util.logging.LogRecord)
	 *	@param record log record
	 */
	public void publish (LogRecord record)
	{
		checkContext();
		
		LinkedList<LogRecord> m_logs = (LinkedList<LogRecord>) Env.getCtx().get(LOGS_KEY);
		if (!isLoggable (record) || m_logs == null)
			return;
		
		//	Output
		synchronized (m_logs)
		{
			if (m_logs.size() >= LOG_SIZE)
				m_logs.removeFirst();
			m_logs.add(record);
		}
		
		
	}	// publish

	/**
	 * Flush (NOP)
	 * @see java.util.logging.Handler#flush()
	 */
	public void flush ()
	{
	}	// flush

	/**
	 * Close
	 * @see java.util.logging.Handler#close()
	 * @throws SecurityException
	 */
	public void close () throws SecurityException
	{
		Env.getCtx().remove(LOGS_KEY);
	}	// close

	
	/**************************************************************************
	 * 	Get ColumnNames of Log Entries
	 * 	@param ctx context (not used)
	 * 	@return string vector
	 */	
	public Vector<String> getColumnNames(Properties ctx)
	{
		Vector<String> cn = new Vector<String>();
		cn.add(Msg.getMsg(ctx, "DateTime"));
		cn.add(Msg.getMsg(ctx, "Level"));
		//
		cn.add(Msg.getMsg(ctx, "Class.Method"));
		cn.add(Msg.getMsg(ctx, "Message"));
		//2
		cn.add(Msg.getMsg(ctx, "Parameter"));
		cn.add(Msg.getMsg(ctx, "Trace"));
		//
		return cn;
	}	//	getColumnNames
	
	/**
	 * 	Get Array of events with most recent first
	 * 	@param errorsOnly if true errors otherwise log
	 * 	@return array of events 
	 */
	public LogRecord[] getRecords (boolean errorsOnly)
	{
		checkContext();
		
		LinkedList<LogRecord> m_logs = (LinkedList<LogRecord>) Env.getCtx().get(LOGS_KEY);
		LogRecord[] retValue = null;

		synchronized (m_logs)
		{
			retValue = new LogRecord[m_logs.size()];
			m_logs.toArray(retValue);
		}

		return retValue;
	}	//	getEvents
	
	/**
	 * 	Reset Log Buffer
	 */
	public void resetBuffer ()
	{
		checkContext();
		
		LinkedList<LogRecord> m_logs = (LinkedList<LogRecord>) Env.getCtx().get(LOGS_KEY);

		synchronized (m_logs)
		{
			m_logs.clear();
		}

	}	//	resetBuffer
	
	/**
	 * 	Get Log Info in String
	 *	@param ctx context
	 *	@return log info
	 */
	public String getLogInfo (Properties ctx)
	{
		checkContext();
		
		StringBuffer sb = new StringBuffer();
		sb.append("===== Application Log (most recent first) ===== ");
		sb.append("\n\n");
		//

		LinkedList<LogRecord> m_logs = (LinkedList<LogRecord>) Env.getCtx().get(LOGS_KEY);
		for (int i = m_logs.size() - 1; i >= 0; i--)
		{
			LogRecord record = (LogRecord)m_logs.get(i);
			sb.append(getFormatter().format(record));
		}

		sb.append("\n\n");

		sb.append("===== Summary Info ===== ");
		sb.append("\n\n");
		CLogMgt.getInfo(sb);
		sb.append("\n\n");
		sb.append("===== Detailed Info =====");
		sb.append("\n\n");
		CLogMgt.getInfoDetail(sb, ctx);
		//
		return sb.toString();
	}	//	getErrorInfo

	private void checkContext()
	{
		if (!Env.getCtx().containsKey(LOGS_KEY))
		{
			LinkedList<LogRecord> m_logs = new LinkedList<LogRecord>();
			Env.getCtx().put(LOGS_KEY, m_logs);
		}
		
	}
	
	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString ()
	{
		checkContext();
		
		LinkedList<LogRecord> m_logs = (LinkedList<LogRecord>) Env.getCtx().get(LOGS_KEY);
		StringBuffer sb = new StringBuffer ("CLogErrorBuffer[");
		sb.append("Logs=").append(m_logs.size())
			.append(",Level=").append(getLevel())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
}	//	CLogErrorBuffer
