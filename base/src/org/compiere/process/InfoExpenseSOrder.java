package org.compiere.process;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.util.DateUtil;
import org.compiere.model.MActivity;
import org.compiere.model.MBPartner;
import org.compiere.model.MConversionRate;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MProductPrice;
import org.compiere.model.MProject;
import org.compiere.model.MTimeExpense;
import org.compiere.model.MTimeExpenseLine;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.MUOM;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.TimeUtil;

/**
 * @author jtrinidad
 *	Info Expense Report to Sales Order 
 */

public class InfoExpenseSOrder extends SvrProcess
{
	
	private int m_expenses = 0;
	private int m_headers = 0;
	private boolean isCompleteExpense = true;

	private BigDecimal increment = BigDecimal.ONE;
	
	//Mapping of Expense report to the resource as each expense report is grouped by the customer/invoice BP and resource 
	// Bill_Bpartner, C_BPartner_ID
	private Map<Integer, List<MTimeExpense>> mapBillBPExpense = new HashMap<Integer, List<MTimeExpense>>();

	private List<MOrder> m_orders = new ArrayList<MOrder>();
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			
		}
		
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		processFromInfo(keys);

		createOrders();
		
		return "@Processed@ " + m_expenses;
	}	//	doIt

	private void processFromInfo(List<KeyNamePair> keys )
	{
		for (KeyNamePair key : keys) {
			int id = key.getKey();
			MTimeExpense expense = new MTimeExpense(getCtx(), id, get_TrxName());
			
			addTimeExpensetoMap(expense);
			m_expenses++;
		}

		addLog(0, null, null, "@Processed@ " + m_expenses);

	}
	
	private void addTimeExpensetoMap(MTimeExpense expense)
	{
		if (expense.getLines().length == 0) {
			return;  // don't add
		}
		List<MTimeExpense> listExpense = mapBillBPExpense.get(expense.getBill_BPartner_ID());
	
		if (listExpense == null) {
			listExpense = new ArrayList<MTimeExpense>();
			mapBillBPExpense.put(expense.getBill_BPartner_ID(), listExpense);
		}
		listExpense.add(expense);
		
	}
	
	private void createOrders()
	{
		for (Map.Entry<Integer, List<MTimeExpense>> pair  : mapBillBPExpense.entrySet()) {
			MOrder header = createOrderHeader(pair.getKey(), pair.getValue().get(0));
			for (MTimeExpense expense : pair.getValue()) {
				MTimeExpenseLine[] lines = expense.getLines();
				for (MTimeExpenseLine line : lines) {
					addOrderLine(header, line);
				}
			}
			header.processIt(DocAction.ACTION_Prepare);
			if (!header.save())
				throw new IllegalStateException("Cannot save Order");
		}
	}
	
	private MOrder createOrderHeader(int billBPartnerID, MTimeExpense expense)
	{
		MOrder order = new MOrder(getCtx(), 0, get_TrxName());
		
		order = new MOrder (getCtx(), 0, get_TrxName());
		order.setAD_Org_ID(expense.getAD_Org_ID());
		order.setC_DocTypeTarget_ID(MOrder.DocSubTypeSO_OnCredit);
		order.setBPartner((MBPartner) expense.getBill_BPartner());
		order.setM_Warehouse_ID(expense.getM_Warehouse_ID());
		order.setM_PriceList_ID(expense.getM_PriceList_ID());
		order.setC_PaymentTerm_ID(expense.getLines()[0].getC_Project().getC_PaymentTerm_ID());
		order.setSalesRep_ID(getAD_User_ID());  //current user or should pickup the default salesrep
		order.setDocAction(DocAction.ACTION_Prepare);

		//
		if (!order.save())
		{
			throw new IllegalStateException("Cannot save Order");
		}
		
		return order;
		
	}
	
	private void addOrderLine(MOrder order, MTimeExpenseLine tel)
	{
		if (tel.getS_RecoveryAdjustment_ID() > 0) {
			//skip this as it should not be created to an order
			return;
		}
		MOrderLine ol = new MOrderLine (order);
		//
		if (tel.getM_Product_ID() != 0) {
			ol.setM_Product_ID(tel.getM_Product_ID(), tel.getC_UOM_ID());
		}
		ol.setQty(tel.getQtyInvoiced());		//	
		ol.setDescription(tel.getDescription());
		//
		ol.setC_Project_ID(tel.getC_Project_ID());
		ol.setC_ProjectPhase_ID(tel.getC_ProjectPhase_ID());
		ol.setC_ProjectTask_ID(tel.getC_ProjectTask_ID());
		ol.setC_Activity_ID(tel.getC_Activity_ID());
		ol.setC_Campaign_ID(tel.getC_Campaign_ID());
		
		if (order.getC_Project_ID() == 0) {
			order.setC_Project_ID(tel.getC_Project_ID());
			order.save();
		}
		
		//jobriant - author
		ol.set_ValueOfColumn("AD_User_ID", tel.getAD_User_ID());

		//
		BigDecimal price = tel.getPriceInvoiced();	//	
		if (price != null && price.compareTo(Env.ZERO) != 0)
		{
			if (tel.getC_Currency_ID() != order.getC_Currency_ID())
				price = MConversionRate.convert(getCtx(), price, 
					tel.getC_Currency_ID(), order.getC_Currency_ID(), 
					order.getAD_Client_ID(), order.getAD_Org_ID());
			ol.setPrice(price);
		}
		else
			ol.setPrice();
		if (tel.getC_UOM_ID() != 0 && ol.getC_UOM_ID() == 0)
			ol.setC_UOM_ID(tel.getC_UOM_ID());
		ol.setTax();
		if (!ol.save())
		{
			throw new IllegalStateException("Cannot save Order Line");
		}
		//	Update TimeExpense Line
		tel.setC_OrderLine_ID(ol.getC_OrderLine_ID());
		if (tel.save())
			log.fine("Updated " + tel + " with C_OrderLine_ID");
		else
			log.log(Level.SEVERE, "Not Updated " + tel + " with C_OrderLine_ID");
			
	}
	

}	//	InfoExpenseSOrder