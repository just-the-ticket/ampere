package org.compiere.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.MClientInfo;

/**
 * @author jtrinidad
 *	Update Billing Period 
 */

public class InfoUpdateBillingPeriod extends SvrProcess
{
	

	Timestamp p_BillingDateFrom = null;
	Timestamp p_BillingDateTo = null;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("BillingDateFrom"))
				p_BillingDateFrom = para[i].getParameterAsTimestamp();
			else if (name.equals("BillingDateTo"))
				p_BillingDateTo = para[i].getParameterAsTimestamp();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
		
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		
		MClientInfo ci = MClientInfo.get(getCtx());
		
		ci.set_ValueOfColumn("BillingDateFrom", p_BillingDateFrom);
		ci.set_ValueOfColumn("BillingDateTo", p_BillingDateTo);
		ci.saveEx();
		return "@OK@";
	}	//	doIt



}	//	InfoUpdateBillingPeriod