/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.util.List;
import java.util.logging.Level;

import org.compiere.Adempiere;
import org.compiere.model.MColumn;
import org.compiere.model.MTable;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogMgt;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Trx;

/**
 *	Synchronize Column with Database
 *	
 *  @author Victor Perez, Jorg Janke
 *  @version $Id: ColumnSync.java,v 1.2 2006/07/30 00:51:01 jjanke Exp $
 *  
 *  @author Teo Sarca
 *  	<li>BF [ 2854358 ] SyncColumn should load table in transaction
 *  		https://sourceforge.net/tracker/?func=detail&aid=2854358&group_id=176962&atid=879332
 */
public class ColumnSync extends SvrProcess
{
	/** The Column				*/
	private int			p_AD_Column_ID = 0;
	
	/** DisplayType      */
	private int p_displayType = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;			
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		if ( getRecord_ID() > 0 )
			p_AD_Column_ID = getRecord_ID();
	}	//	prepare

	/**
	 * 	Process
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt() throws Exception
	{
		log.info("C_Column_ID=" + p_AD_Column_ID);
		if (p_AD_Column_ID == 0 && p_displayType == 0)
			throw new AdempiereUserError("@No@ @AD_Column_ID@ or @DisplayType@");

		String sql = null;
		if ( p_AD_Column_ID > 0)
		{
			MColumn column = new MColumn (getCtx(), p_AD_Column_ID, get_TrxName());
			if (column.get_ID() == 0)
				throw new AdempiereUserError("@NotFound@ @AD_Column_ID@ " + p_AD_Column_ID);

			sql = column.syncDatabase();
			addLog(sql);
		}
		
		
		if ( p_displayType > 0)
		{
			
			List<MColumn> columns = MTable.get(getCtx(), MColumn.Table_Name)
			.createQuery("AD_Column.AD_Table_ID IN (SELECT t.AD_Table_ID FROM AD_Table t WHERE t.isView = 'N') "
					+ "AND ColumnSQL IS NULL AND AD_Reference_ID = " + p_displayType, null)
			.setOrderBy("AD_Table_ID, ColumnName")
			.list();
			
			for (MColumn column : columns ) {

				Trx trx = Trx.get(Trx.createTrxName("ColSync"), true);
				column.set_TrxName(trx.getTrxName());
				sql = column.syncDatabase();
				trx.commit();
				trx.close();
				addLog(sql);
			}
		}
		
		return sql;
	}	//	doIt
	
	// Run column sync for columns matching displaytype
	public static void main(String[] args) 
	{
		Adempiere.startupEnvironment(false);
		CLogMgt.setLevel(Level.FINE);
		ProcessInfo pi = new ProcessInfo("ColumnSync", 181);
		pi.setAD_Client_ID(0);
		pi.setAD_User_ID(100);
		
		
		
		ColumnSync cs = new ColumnSync();
		cs.p_displayType = DisplayType.String;
		//cs.p_AD_Column_ID = 52104;
		cs.startProcess(Env.getCtx(), pi, null);
		
		System.out.println("Process=" + pi.getTitle() + " Error="+pi.isError() + " Summary=" + pi.getSummary());
	}
}	//	ColumnSync
