/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.DateUtil;
import org.compiere.model.MActivity;
import org.compiere.model.MBPartner;
import org.compiere.model.MClient;
import org.compiere.model.MPriceList;
import org.compiere.model.MProductPrice;
import org.compiere.model.MTimeExpense;
import org.compiere.model.MTimeExpenseLine;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.MUOM;
import org.compiere.model.Query;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DisplayType;
import org.compiere.util.KeyNamePair;
import org.compiere.util.TimeUtil;

/**
 *	Process timesheet entry to expense report
 */

public class TimesheetEntrytoExpense extends SvrProcess
{
	private static final int C_UOM_ID_HR = 101; 
	
	private int m_entries = 0;
//	private int m_headers = 0;
	private boolean isCompleteExpense = true;

	private BigDecimal increment = BigDecimal.ONE;
	
	//Mapping of Expense report to the resource as each expense report is grouped by the customer/invoice BP and resource 
	// Bill_Bpartner, C_BPartner_ID
	private Map<Integer, Map<Integer, MTimeExpense>> mapResourceTimeExpense = new HashMap<Integer, Map<Integer,MTimeExpense>>();

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			
		}

		
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		
		List<KeyNamePair> keys = getInfoSelectionKeys();
		processFromInfo(keys);

		return "OK";
	}	//	doIt

	private void processFromInfo(List<KeyNamePair> keys ) throws AdempiereUserError, RuntimeException
	{
		for (KeyNamePair key : keys) {
			int id = key.getKey();
			MTimesheetEntry entry = new MTimesheetEntry(getCtx(), id, get_TrxName());
			addTimesheetEntry(entry);
			m_entries++;
		}
		if (isCompleteExpense) {
			completeAll();
		}
		addLog(0, null, null, "@Processed@ " + m_entries);

	}
	
	private void completeAll()
	{
		for (Map<Integer, MTimeExpense> mapResourceExpense : mapResourceTimeExpense.values()) {
			for (MTimeExpense expense : mapResourceExpense.values()) {
//				expense.processIt(expense.getDocAction());
				expense.getLines(true);
				if (!expense.processIt(MTimeExpense.DOCACTION_Prepare)) {
					addLog(expense.getDocumentNo() + "-" + expense.getProcessMsg());
				}
				expense.save();
				
				addLog(0, null, null, String.format("Expense report %s created.", expense.getDocumentNo()));
			}
		}
	}
	/**
	 * Get existing TimeExpense header if exists, or create a new one
	 * @param entry
	 * @return
	 */
	private MTimeExpense getHeader (MTimesheetEntry entry)
	{
	
		int invoiceBP = entry.getC_Project().getC_BPartner_ID();
		int projectID = entry.getC_Project_ID();
		int pricelistID = entry.getC_Project().getM_PriceList_Version().getM_PriceList_ID();
		
		Map<Integer, MTimeExpense> mapProjectExpense = mapResourceTimeExpense.get(invoiceBP);
		if (mapProjectExpense == null) {
			mapProjectExpense = new HashMap<Integer, MTimeExpense>();
			mapResourceTimeExpense.put(invoiceBP, mapProjectExpense);
		}
		MTimeExpense expense = mapProjectExpense.get(projectID);
		if (expense == null) {
			expense = new MTimeExpense(getCtx(), 0, get_TrxName());
			expense.setAD_Org_ID(entry.getAD_Org_ID());
			expense.setM_Warehouse_ID(entry.getC_Project().getM_Warehouse_ID());
			expense.setM_PriceList_ID(pricelistID);
			expense.setC_Project_ID(projectID);
			expense.setC_BPartner_ID(entry.getC_BPartner_ID());  //first author when created
			expense.setBill_BPartner_ID(invoiceBP);
			expense.setDescription(DisplayType.getDateFormat().format(TimeUtil.getDay(null)) + " " + expense.getBill_BPartner().getName());
			expense.setDateReport(TimeUtil.getDay(null));
			expense.setDocStatus(MTimeExpense.DOCSTATUS_Drafted);
			expense.setDocAction(MTimeExpense.DOCACTION_Complete);
			expense.saveEx();
			
			mapProjectExpense.put(projectID, expense);
//			m_headers++;
		}
		return expense;
	}
	
	private void addTimesheetEntry(MTimesheetEntry entry) throws AdempiereUserError, RuntimeException
	{
		if (entry.getS_TimeExpenseLine_ID() > 0) {
			//already has expense line - exclude
			return;
		}
		int invoiceBP = entry.getC_Project().getC_BPartner_ID();
		int currencyID = entry.getC_Project().getC_Currency_ID();
		MTimeExpense expense = getHeader(entry);
//		MPriceList priceList = (MPriceList) expense.getM_PriceList(); 
		
		MTimeExpenseLine line = new MTimeExpenseLine(getCtx(), 0, get_TrxName());
		line.setS_TimeExpense_ID(expense.getS_TimeExpense_ID());
		line.setAD_Org_ID(expense.getAD_Org_ID());
		
		int lineNo = expense.getLines().length*10 + 10;
		line.setLine(lineNo);
		line.setC_BPartner_ID(invoiceBP);
		line.setC_Currency_ID(currencyID);
		line.setC_Activity_ID(entry.getC_Activity_ID());
		line.setC_Project_ID(entry.getC_Project_ID());
		MActivity activity = (MActivity) entry.getC_Activity();
		
		line.setIsInvoiced(activity.isInvoiced());
		line.setAD_User_ID(entry.getAD_User_ID());
		line.setIsTimeReport(true);
		
		BigDecimal qtyEntered = entry.getQtyEntered();
		BigDecimal qtyInvoice = qtyEntered.divide(increment, 0, RoundingMode.UP).multiply(increment);
	
		line.setC_UOM_ID(C_UOM_ID_HR);
		MUOM uom = (MUOM) line.getC_UOM();
		BigDecimal qty = qtyInvoice.divide(new BigDecimal(60), uom.getStdPrecision(), RoundingMode.HALF_UP);
		line.setQty(qty);
		line.setDateExpense(entry.getStartDate());
		
		MProductPrice pp = MProductPrice.get(getCtx(), entry.getC_Project().getM_PriceList_Version_ID(), 
				entry.getM_Product_ID(), null);
		if (pp == null) {
			throw new AdempiereUserError(String.format("There is no price defined for %s in %s Price List Version."
					, entry.getM_Product().getName(), entry.getC_Project().getM_PriceList_Version().getName()));
			
		}
		line.setM_Product_ID(entry.getM_Product_ID());
		line.isTimeReport();
		line.setPriceInvoiced(pp.getPriceStd());
		line.setExpenseAmt(pp.getPriceStd().multiply(qty, new MathContext(4, RoundingMode.HALF_UP)));
		line.setNote(entry.getComments());
		line.saveEx();
		
		//
		entry.setProcessed(true);
		entry.setS_TimeExpenseLine_ID(line.getS_TimeExpenseLine_ID());
		entry.save();

	}

}	//	TimesheetEntryProcess