package org.compiere.process;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.util.DateUtil;
import org.compiere.model.MActivity;
import org.compiere.model.MBPartner;
import org.compiere.model.MClientInfo;
import org.compiere.model.MConversionRate;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MProductPrice;
import org.compiere.model.MProject;
import org.compiere.model.MTimeExpense;
import org.compiere.model.MTimeExpenseLine;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.MUOM;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.TimeUtil;

/**
 * @author jtrinidad
 *	Update Recovery Adjustment
 */

public class InfoUpdateExpenseLineRecoveryAdjustment extends SvrProcess
{
	

	int p_RecoveryAdjustment_ID = 0;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("S_RecoveryAdjustment_ID"))
				p_RecoveryAdjustment_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);			
		}
		
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		List<KeyNamePair> keys = getInfoSelectionKeys();
		int n = 0;
		for (KeyNamePair key : keys) {
			int id = key.getKey();
			MTimeExpenseLine line = new MTimeExpenseLine(getCtx(), id, get_TrxName());
		
			line.setS_RecoveryAdjustment_ID(p_RecoveryAdjustment_ID);
			line.saveEx();
			n++;
		}
		addLog(0, null, null, "@Processed@ " + n);
		return "@OK@";
	}	//	doIt



}	//	InfoUpdateBillingPeriod