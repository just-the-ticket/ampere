/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 Adempiere, Inc. All Rights Reserved.               *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.pdf;

import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MArchive;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.MSysConfig;
import org.compiere.model.PrintInfo;
import org.compiere.model.Query;
import org.compiere.print.MPrintFormat;
import org.compiere.print.layout.LayoutEngine;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Generate PDF document using iText
 * @author Low Heng Sin
 *
 */
public class Document {

	static {
		FontFactory.registerDirectories();
	}
	
	public final static String PDF_FONT_DIR = "PDF_FONT_DIR";
	
	/**	Logger			*/
	private static CLogger	log	= CLogger.getCLogger (Document.class);
	
	private static void writePDF(Pageable pageable, OutputStream output)
	{
		try {
            final PageFormat pf = pageable.getPageFormat(0);
            
            final com.itextpdf.text.Document document =
                	new com.itextpdf.text.Document(new Rectangle(
                			(int) pf.getWidth(), (int) pf.getHeight()));
            final PdfWriter writer = PdfWriter.getInstance(
                    document, output);
            writer.setPdfVersion(PdfWriter.VERSION_1_7);
            document.open();
            final DefaultFontMapper mapper = new DefaultFontMapper();     
            
            //Elaine 2009/02/17 - load additional font from directory set in PDF_FONT_DIR of System Configurator 
            String pdfFontDir = MSysConfig.getValue(PDF_FONT_DIR, ""); 
            if(pdfFontDir != null && pdfFontDir.trim().length() > 0)
            {
            	pdfFontDir = pdfFontDir.trim();
	            File dir = new File(pdfFontDir);
	            if(dir.exists() && dir.isDirectory())
	            	mapper.insertDirectory(pdfFontDir);
            }
            //
            
            final float w = (float) pf.getWidth();
            final float h = (float) pf.getHeight();
            final PdfContentByte cb = writer.getDirectContent();
            for (int page = 0; page < pageable.getNumberOfPages(); page++) {
            	if (page != 0) {
            		document.newPage();
            	}
            	log.log(Level.FINE, "Writing page: " + page + " to pdf");
            	
	            final Graphics2D g2 = cb.createGraphics(w, h, mapper);
	            pageable.getPrintable(page).print(g2, pf, page);
	            g2.dispose();
            }
            document.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public static File getPDFAsFile(String filename, Pageable pageable) {
        final File result = new File(filename);
        
        try {
        	writePDF(pageable, new FileOutputStream(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
	/**
	 * Return the file after attaching prepend and append pdf file
	 * 
	 * @param file
	 * @param printFormat
	 * @param pageable
	 * @return
	 * @throws IOException
	 */
	public static File getMergerdPDFAsFile(File file, MPrintFormat printFormat, Pageable pageable) throws IOException
	{

		List<File> pdfs = new ArrayList<File>();

		writePDF(pageable, new FileOutputStream(file));
		pdfs.add(file);
		
		MAttachment attachments = new Query(Env.getCtx(), MAttachment.Table_Name, "AD_Table_ID = 493 AND Record_ID = ?",
				null).setParameters(printFormat.getAD_PrintFormat_ID()).first();

		if (attachments != null) {
		    String prependFile = printFormat.getPrependPDFFile();
		    String appendFile = printFormat.getAppendPDFFile();


		    for (MAttachmentEntry entry : attachments.getEntries()) {
		        // Check if this is the prepend file
		        if (prependFile != null && prependFile.equalsIgnoreCase(entry.getFileName())) {
		            pdfs.add(0, entry.getFile());
		        }

		        // Check if this is the append file
		        if (appendFile != null && appendFile.equalsIgnoreCase(entry.getFileName())) {
		            pdfs.add(entry.getFile());
		        }
		    }
		}
		File mergedFile = null;
		try
		{
			mergedFile = File.createTempFile("MergedReportEngine", ".pdf");
			mergePdf(pdfs, mergedFile);
			Files.copy(mergedFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "PDF", e);
			return null;
		}
		return file;
	}
	
	/**
	 * 
	 * Merge Prepend and Append PDF with the PrintFormat PDF
	 * @param pdfList
	 * @param outFile
	 * @throws IOException
	 * @throws DocumentException
	 * @throws FileNotFoundException
	 */
	public static void mergePdf(List<File> pdfList, File outFile)
			throws IOException, DocumentException, FileNotFoundException
	{
		com.itextpdf.text.Document document = null;
		PdfWriter copy = null;
		for (File f : pdfList)
		{
			PdfReader reader = new PdfReader(f.getAbsolutePath());
			if (document == null)
			{
				document = new com.itextpdf.text.Document(reader.getPageSizeWithRotation(1));
				copy = PdfWriter.getInstance(document, new FileOutputStream(outFile));
				document.open();
			}
			int pages = reader.getNumberOfPages();
			PdfContentByte cb = copy.getDirectContent();
			for (int i = 1; i <= pages; i++)
			{
				document.newPage();
				PdfImportedPage page = copy.getImportedPage(reader, i);
				cb.addTemplate(page, 0, 0);
			}
		}
		document.close();
	}

    public static byte[] getPDFAsArray(Pageable pageable) {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream(10240);
            writePDF(pageable, output);
            return output.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } 

        return null;
    }
    
	/**
	 * Return bytes of the PDF file after attaching prepend and append PDF 
	 * @param layout
	 * @param pageable
	 * @return
	 */
	public static byte[] getMergedPDFAsArray(LayoutEngine layout, Pageable pageable)
	{
		try
		{
			File file = File.createTempFile(layout.getFormat().getName(), ".pdf");
			Document.getMergerdPDFAsFile(file, layout.getFormat(), layout.getPageable(false));
			return java.nio.file.Files.readAllBytes(file.toPath());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
    
    public static boolean isValid(Pageable layout) {
    	return true;
    }
    
    public static boolean isLicensed() {
    	return true;
    }    
}
