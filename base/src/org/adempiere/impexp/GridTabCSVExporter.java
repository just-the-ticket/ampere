package org.adempiere.impexp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridTable;
import org.compiere.model.GridWindow;
import org.compiere.model.GridWindowVO;
import org.compiere.model.MColumn;
import org.compiere.model.MLocation;
import org.compiere.model.MRefList;
import org.compiere.model.MTab;
import org.compiere.model.MTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Evaluator;
import org.compiere.util.Ini;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.supercsv.cellprocessor.FmtBool;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.FmtNumber;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

public class GridTabCSVExporter implements IGridTabExporter
{

	private static CLogger		log							= CLogger.getCLogger(GridTabCSVExporter.class);
	public final static int		REFERENCE_PAYMENTRULE		= 195;
	public static final String	DEFAULT_TIME_FORMAT			= "HH:mm:ss";
	public static final String	DEFAULT_TIMESTAMP_FORMAT	= "yyyy-MM-dd HH:mm:ss";
	public static final String	DEFAULT_DATE_FORMAT			= "yyyy-MM-dd";

	public GridTabCSVExporter()
	{
		super();
	}

	@Override
	public void export(GridTab gridTab, List<GridTab> childs, boolean currentRowOnly, File file, int indxDetailSelected)
	{
		ICsvMapWriter mapWriter = null;
		Map<GridTab, GridField[]> tabMapDetails = new HashMap<GridTab, GridField[]>();
		MTable table = null;
		MTable tableDetail = null;
		try
		{
			FileOutputStream fileOut = new FileOutputStream(file);
			OutputStreamWriter oStrW = new OutputStreamWriter(fileOut, Ini.getCharset());
			BufferedWriter bw = new BufferedWriter(oStrW);
			mapWriter = new CsvMapWriter(bw, CsvPreference.STANDARD_PREFERENCE);
			String isValidTab = isValidTabToExport(gridTab);
			if (isValidTab != null)
			{
				throw new AdempiereException(isValidTab);
			}
			GridTable girdTableModel = gridTab.getTableModel();
			GridField[] gridFields = gridTab.getExportFields();
			if (gridFields.length == 0)
				throw new AdempiereException(gridTab.getName() + ": Did not find any available field to be exported.");

			List<String> headArray = new ArrayList<String>();
			List<CellProcessor> procArray = new ArrayList<CellProcessor>();
			table = MTable.get(Env.getCtx(), gridTab.getTableName());
			int specialHDispayType = 0;

			// master tab
			for (int idxfld = 0; idxfld < gridFields.length; idxfld++)
			{
				GridField field = gridFields[idxfld];
				MColumn column = MColumn.get(Env.getCtx(), field.getAD_Column_ID());

				// Special Columns
				if (DisplayType.Location == field.getDisplayType())
				{
					specialHDispayType = DisplayType.Location;
					continue;
				}
				else if (!(field.isDisplayed() || field.isDisplayedGrid()))
				{
					continue;
				}

				if (field.getAD_Column_ID() < 1)
				{
					continue;
				}

				String headName = resolveColumnName(table, column);
				headArray.add(headName);
				if (DisplayType.Date == field.getDisplayType())
				{
					procArray.add(new Optional(new FmtDate(DEFAULT_DATE_FORMAT)));
				}
				else if (DisplayType.DateTime == field.getDisplayType())
				{
					procArray.add(new Optional(new FmtDate(DEFAULT_TIMESTAMP_FORMAT)));
				}
				else if (DisplayType.Time == field.getDisplayType())
				{
					procArray.add(new Optional(new FmtDate(DEFAULT_TIME_FORMAT)));
				}
				else if (DisplayType.Integer == field.getDisplayType() || DisplayType.isNumeric(field.getDisplayType()))
				{
					DecimalFormat nf = DisplayType.getNumberFormat(field.getDisplayType());
					nf.setGroupingUsed(false);
					procArray.add(new Optional(new FmtNumber(nf)));
				}
				else if (DisplayType.YesNo == field.getDisplayType())
				{
					procArray.add(new Optional(new FmtBool("Y", "N")));
				}
				else
				{ // lookups
					procArray.add(new Optional());
				}
			}

			if (specialHDispayType > 0)
			{
				for (String specialHeader : resolveSpecialColumnName(specialHDispayType))
				{
					headArray.add(gridTab.getTableName() + ">" + specialHeader);
					procArray.add(null);
				}
			}
			// Details up to tab level 1
			if (childs.size() > 0)
			{
				int specialDetDispayType = 0;

				for (GridTab detail : childs)
				{

					if (!detail.isDisplayed())
						continue;

					if (detail.getDisplayLogic() != null)
					{
						if (currentRowOnly && !Evaluator.evaluateLogic(detail, detail.getDisplayLogic()))
							continue;
					}

					if (detail.getTabLevel() > 1)
						continue;

					isValidTab = isValidTabToExport(detail);
					if (isValidTab != null)
					{
						log.log(Level.INFO, isValidTab);
						continue;
					}
					tableDetail = MTable.get(Env.getCtx(), detail.getTableName());
					gridFields = detail.getExportFields();
					for (GridField field : gridFields)
					{
						MColumn columnDetail = MColumn.get(Env.getCtx(), field.getAD_Column_ID());
						if (DisplayType.Location == field.getDisplayType())
						{
							specialDetDispayType = DisplayType.Location;
							continue;
						}
						if (field.getAD_Column_ID() < 1)
						{
							continue;
						}
						String headNameDetail = detail.getTableName() + ">"
								+ resolveColumnName(tableDetail, columnDetail);
						headArray.add(headNameDetail);
						if (DisplayType.Date == field.getDisplayType())
						{
							procArray.add(new Optional(new FmtDate(DEFAULT_DATE_FORMAT)));
						}
						else if (DisplayType.DateTime == field.getDisplayType())
						{
							procArray.add(new Optional(new FmtDate(DEFAULT_TIMESTAMP_FORMAT)));
						}
						else if (DisplayType.Time == field.getDisplayType())
						{
							procArray.add(new Optional(new FmtDate(DEFAULT_TIME_FORMAT)));
						}
						else if (DisplayType.Integer == field.getDisplayType()
								|| DisplayType.isNumeric(field.getDisplayType()))
						{
							DecimalFormat nf = DisplayType.getNumberFormat(field.getDisplayType());
							nf.setGroupingUsed(false);
							procArray.add(new Optional(new FmtNumber(nf)));
						}
						else if (DisplayType.YesNo == field.getDisplayType())
						{
							procArray.add(new Optional(new FmtBool("Y", "N")));
						}
						else
						{ // lookups and text
							procArray.add(new Optional());
						}
					}
					if (specialDetDispayType > 0)
					{
						for (String specialHeader : resolveSpecialColumnName(specialDetDispayType))
						{
							headArray.add(detail.getTableName() + ">" + specialHeader);
							procArray.add(null);
						}
						specialDetDispayType = 0;
					}
					tabMapDetails.put(detail, gridFields);
					// numOfTabs++;
				}
				gridFields = null;
			}

			// the header elements are used to map the bean values to each
			// column (names must match)
			String[] header = headArray.toArray(new String[headArray.size()]);
			CellProcessor[] processors = procArray.toArray(new CellProcessor[procArray.size()]);
			// write the header
			mapWriter.writeHeader(header);
			// write the beans
			int start = 0;
			int end = 0;
			if (currentRowOnly)
			{
				start = gridTab.getCurrentRow();
				end = start + 1;
			}
			else
			{
				end = girdTableModel.getRowCount();
			}
			
			for (int idxrow = start; idxrow < end; idxrow++)
			{
				Map<String, Object> row = new HashMap<String, Object>();
				int idxfld = 0;
				int index = 0;
				int rowDetail = 0;
				int record_Id = 0;
				boolean isActiveRow = true;
				gridTab.setCurrentRow(idxrow);
				for (GridField field : gridTab.getExportFields())
				{
					MColumn column = MColumn.get(Env.getCtx(), field.getAD_Column_ID());
					Object value = null;
					String headName = header[idxfld];
					if (field.getAD_Column_ID() < 1)
					{
						continue;
					}
					
					if (DisplayType.Location == field.getDisplayType())
					{
						Object fResolved = resolveValue(gridTab, table, column, idxrow, column.getColumnName());
						if (fResolved != null)
							record_Id = Integer.parseInt(fResolved.toString());

						continue;
					}
					else if (DisplayType.Payment == field.getDisplayType())
					{
						value = MRefList.getListName(Env.getCtx(), REFERENCE_PAYMENTRULE,
								gridTab.getValue(idxrow, header[idxfld]).toString());
					}
					else
					{
						value = resolveValue(gridTab, table, column, idxrow, headName);
					}
					// Ignore row
					if ("IsActive".equals(headName) && value != null && Boolean.valueOf((Boolean) value) == false)
					{
						isActiveRow = false;
						break;
					}
					row.put(headName, value);
					idxfld++;
					index++;
				}
				if (!isActiveRow)
					continue;

				if (specialHDispayType > 0 && record_Id > 0)
				{
					switch (specialHDispayType)
					{
						case DisplayType.Location:
							MLocation address = new MLocation(Env.getCtx(), record_Id, null);
							for (String specialHeader : resolveSpecialColumnName(specialHDispayType))
							{
								String columnName = specialHeader.substring(specialHeader.indexOf(">") + 1,
										specialHeader.length());
								Object sValue = null;
								if (columnName.indexOf("[") >= 0 && columnName.endsWith("]"))
								{
									int indx = columnName.indexOf("[");
									String columnRef = columnName.substring(indx + 1, columnName.length() - 1);
									String tableRef = columnName.substring(0, indx);
									Object record_id = address.get_Value(tableRef);
									if (record_id != null)
										sValue = queryExecute(columnRef, tableRef, record_id);
								}
								else
								{
									sValue = address.get_Value(columnName);
									if (DisplayType.YesNo == MColumn.get(Env.getCtx(), MLocation.Table_Name, columnName)
											.getAD_Reference_ID())
									{
										if (sValue != null && (Boolean) sValue)
											sValue = "Y";
										else if (sValue != null && !(Boolean) sValue)
											sValue = "N";
									}
								}
								row.put(gridTab.getTableName() + ">" + specialHeader, sValue);
								idxfld++;
								index++;
							}
							break;
					}
				}

				if (childs.size() > 0)
				{
					for (GridTab childTab : childs)
					{
						if (!childTab.isLoadComplete())
						{
							childTab.initTab(false);
						}

						childTab.query(false, 0, 0);
					}

					while (true)
					{
						Map<String, Object> tmpRow = resolveMasterDetailRow(rowDetail, tabMapDetails, headArray, index);
						if (tmpRow != null)
						{
							for (Map.Entry<String, Object> details : tmpRow.entrySet())
							{
								String detailColumn = details.getKey();
								Object value = details.getValue();
								row.put(detailColumn, value);
							}
							rowDetail++;
							
							// on null leave blank cell.
							for (int i = 0; i < header.length; i++)
							{
								if (row.get(header[i]) == null || !row.containsKey(header[i]))
								{
									row.put(header[i], " ");
									processors[i] = null;
								}
							}
							mapWriter.write(row, header, processors);
						}
						else
						{
							break;
						}

					}
				}

				if (rowDetail == 0)
				{
					// Solve "Input cannot be null" error.
					for (int i = 0; i < header.length; i++)
					{
						if (!row.containsKey(header[i]) || row.get(header[i]) == null)
						{
							row.put(header[i], " ");
							processors[i] = null;
						}
					}
					mapWriter.write(row, header, processors);
				}

				idxfld = 0;
			}
		}
		catch (IOException e)
		{
			throw new AdempiereException(e);
		}
		finally
		{
			if (mapWriter != null)
			{
				try
				{
					mapWriter.close();
				}
				catch (IOException e)
				{
					log.log(Level.SEVERE, "(IOException)", e);
				}
			}
		}
	}

	private Object queryExecute(String selectColumn, String tableName, Object record_id)
	{

		StringBuilder select = new StringBuilder("SELECT ").append(selectColumn).append(" FROM ")
				.append(tableName.substring(0, tableName.length() - 3)).append(" WHERE ").append(tableName)
				.append("=?");

		return DB.getSQLValueStringEx(null, select.toString(), record_id);
	}

	private ArrayList<String> resolveSpecialColumnName(int displayType)
	{

		ArrayList<String> specialColumnNames = new ArrayList<String>();
		if (DisplayType.Location == displayType)
		{
			GridWindowVO gWindowVO = GridWindowVO.create(Env.getCtx(), 0, 121, 0);
			GridWindow m_mWindow = new GridWindow(gWindowVO);
			GridTab m_mTab = m_mWindow.getTab(0);
			m_mWindow.initTab(0);
			for (GridField locField : m_mTab.getFields())
			{
				if ("AD_Client_ID".equals(locField.getColumnName()))
					continue;
				if ("AD_Org_ID".equals(locField.getColumnName()))
					continue;
				if ("IsActive".equals(locField.getColumnName()))
					continue;
				if (!locField.isDisplayed())
					continue;
				if (locField.getAD_Column_ID() < 1) // ADD
				{
					continue;
				}
				String fName = resolveColumnName(MTable.get(Env.getCtx(), m_mTab.getTableName()),
						MColumn.get(Env.getCtx(), locField.getAD_Column_ID()));
				specialColumnNames.add(m_mTab.getTableName() + ">" + fName);
			}
		}

		return specialColumnNames;
	}

	private String resolveColumnName(MTable table, MColumn column)
	{
		StringBuilder name = new StringBuilder(column.getColumnName());
		if (DisplayType.isLookup(column.getAD_Reference_ID()))
		{
			// resolve to identifier - search for value first, if not search for
			// name - if not use the ID
			String foreignTable = column.getReferenceTableName();
			if (!("AD_Language".equals(foreignTable) || "AD_EntityType".equals(foreignTable)
					|| "AD_Ref_List".equals(foreignTable)))
			{
				MTable fTable = MTable.get(Env.getCtx(), foreignTable);
				// Hard-coded / do not check for Value on AD_Org, AD_User and
				// AD_Ref_List, must use name for these two tables
				if (!("AD_Org".equals(foreignTable) || "AD_User".equals(foreignTable))
						&& fTable.getColumn("Value") != null)
				{
					name.append("[Value]"); // fully qualified
				}
				else if (fTable.getColumn("Name") != null)
				{
					name.append("[Name]");
				}
				else if (fTable.getColumn("DocumentNo") != null)
				{
					name.append("[DocumentNo]");
				}
			}
		}
		return name.toString();
	}

	private Map<String, Object> resolveMasterDetailRow(int currentDetRow, Map<GridTab, GridField[]> tabMapDetails,
			List<String> headArray, int idxfld)
	{
		Map<String, Object> activeRow = new HashMap<String, Object>();
		Object value = null;
		boolean hasDetails = false;
		int specialDetDispayType = 0;

		if (currentDetRow > 0)
		{
			for (int j = 0; j < idxfld; j++)
			{
				activeRow.put(headArray.get(j), null);
			}
		}
		for (Map.Entry<GridTab, GridField[]> childTabDetail : tabMapDetails.entrySet())
		{
			GridTab childTab = childTabDetail.getKey();
			Map<String, Object> row = new HashMap<String, Object>();
			boolean isActiveRow = true;
			if (childTab.getRowCount() > 0)
			{
				int specialRecordId = 0;
				for (GridField field : childTabDetail.getValue())
				{
					MColumn column = MColumn.get(Env.getCtx(), field.getAD_Column_ID());
					if (DisplayType.Location == column.getAD_Reference_ID())
					{
						specialDetDispayType = DisplayType.Location;
						Object fResolved = resolveValue(childTab, MTable.get(Env.getCtx(), childTab.getTableName()),
								column, currentDetRow, column.getColumnName());
						if (fResolved != null)
							specialRecordId = Integer.parseInt(fResolved.toString());

						continue;
					}
					if (field.getAD_Column_ID() < 1)
					{
						continue;
					}
					MTable tableDetail = MTable.get(Env.getCtx(), childTab.getTableName());
					String headName = headArray.get(
							headArray.indexOf(childTab.getTableName() + ">" + resolveColumnName(tableDetail, column)));
					value = resolveValue(childTab, MTable.get(Env.getCtx(), childTab.getTableName()), column,
							currentDetRow, headName.substring(headName.indexOf(">") + 1, headName.length()));

					if (DisplayType.Payment == field.getDisplayType())
						value = MRefList.getListName(Env.getCtx(), REFERENCE_PAYMENTRULE, value.toString());
					if (value != null)
						hasDetails = true;
					row.put(headName, value);
					// Ignore row
					if (headName.contains("IsActive") && value != null && Boolean.valueOf((Boolean) value) == false)
					{
						isActiveRow = false;
						break;
					}
				}
				if (isActiveRow && specialDetDispayType > 0 && specialRecordId > 0)
				{
					MLocation address = new MLocation(Env.getCtx(), specialRecordId, null);
					for (String specialHeader : resolveSpecialColumnName(specialDetDispayType))
					{
						String columnName = specialHeader.substring(specialHeader.indexOf(">") + 1,
								specialHeader.length());
						Object sValue = null;
						if (columnName.indexOf("[") >= 0 && columnName.endsWith("]"))
						{
							int indx = columnName.indexOf("[");
							String columnRef = columnName.substring(indx + 1, columnName.length() - 1);
							String tableRef = columnName.substring(0, indx);
							Object record_id = address.get_Value(tableRef);
							if (record_id != null)
								sValue = queryExecute(columnRef, tableRef, record_id);
						}
						else
						{
							sValue = address.get_Value(columnName);
							if (DisplayType.YesNo == MColumn.get(Env.getCtx(), MLocation.Table_Name, columnName)
									.getAD_Reference_ID())
							{
								if (sValue != null && (Boolean) sValue)
									sValue = "Y";
								else if (sValue != null && !(Boolean) sValue)
									sValue = "N";
							}
						}
						row.put(childTab.getTableName() + ">" + specialHeader, sValue);
					}
				}
			}
			if (isActiveRow)
				activeRow.putAll(row);
		}
		if (hasDetails)
			return activeRow;
		else
			return null;
	}

	private Object resolveValue(GridTab gridTab, MTable table, MColumn column, int currentDetRow, String headName)
	{
		Object value = null;
		if (headName.indexOf("[") >= 0 && headName.endsWith("]"))
		{
			String foreignTable = column.getReferenceTableName();
			Object idO = gridTab.getValue(currentDetRow, column.getColumnName());
			if (idO != null)
			{
				if (foreignTable.equals("AD_Ref_List"))
				{
					if (idO instanceof String[])
					{
						StringBuilder sb = new StringBuilder();
						String[] arr = (String[]) idO;
						int iMax = arr.length - 1;
						if (iMax != -1)
						{
							for (int j = 0;; j++)
							{
								sb.append(MRefList.getListName(Env.getCtx(), column.getAD_Reference_Value_ID(),
										arr[j].trim()));
								if (j == iMax)
									break;
								sb.append(", ");
							}
						}

						if (sb != null && sb.length() > 0)
						{
							value = sb.insert(0, "\"").append("\"").toString();
						}
					}
					else

					{
						String ref = (String) idO;
						value = MRefList.getListName(Env.getCtx(), column.getAD_Reference_Value_ID(), ref);
					}
				}
				else
				{
					int start = headName.indexOf("[") + 1;
					int end = headName.length() - 1;
					String foreignColumn = headName.substring(start, end);

					if (idO instanceof Integer[])
					{
						StringBuilder sb = new StringBuilder();
						Integer[] arr = (Integer[]) idO;
						int iMax = arr.length - 1;
						if (iMax != -1)
						{
							for (int j = 0;; j++)
							{
								sb.append(String.valueOf(arr[j]));
								if (j == iMax)
									break;
								sb.append(", ");
							}
						}

						if (sb != null && sb.length() > 0)
						{
							StringBuilder select = new StringBuilder("SELECT '\"' || STRING_AGG(").append(foreignColumn)
									.append(",', ')").append(" || '\"' FROM ")
									.append(foreignTable).append(" WHERE ").append(foreignTable).append("_ID IN (")
									.append(sb.toString()).append(")");
							value = DB.getSQLValueStringEx(null, select.toString());
						}
					}
					else
					{
						int id = (Integer) idO;
						StringBuilder select = new StringBuilder("SELECT ").append(foreignColumn).append(" FROM ")
								.append(foreignTable).append(" WHERE ").append(foreignTable).append("_ID=?");
						value = DB.getSQLValueStringEx(null, select.toString(), id);
					}
				}
			}
		}
		else
		{
			// e.g warehouse address
			value = gridTab.getValue(currentDetRow, headName);
			if (value instanceof String[] || value instanceof Integer[])
			{
				String s = Util.convertArrayToStringForDB(value);
				if (s != null && s.length() > 0)
					s = s.replaceAll("([{}])", "\"");
				value = s;
			}
		}
		return value;
	}

	@Override
	public String getFileExtension()
	{
		return "csv";
	}

	@Override
	public String getFileExtensionLabel()
	{
		return Msg.getMsg(Env.getCtx(), "FileCSV");
	}

	@Override
	public String getContentType()
	{
		return "application/csv";
	}

	@Override
	public String getSuggestedFileName(GridTab gridTab)
	{
		return "Export_" + gridTab.getTableName() + "." + getFileExtension();
	}

	@Override
	public boolean isExportableTab(GridTab gridTab)
	{
		if (!gridTab.isDisplayed())
			return false;

		if (gridTab.getTabLevel() > 1)
			return false;

		if (isValidTabToExport(gridTab) != null)
		{
			return false;
		}

		return true;
	}

	private String isValidTabToExport(GridTab gridTab)
	{
		String result = null;

		MTab tab = new MTab(Env.getCtx(), gridTab.getAD_Tab_ID(), null);

		if (tab.isReadOnly())
			result = Msg.getMsg(Env.getCtx(), "FieldIsReadOnly", new Object[] { gridTab.getName() });

		if (gridTab.getTableName().endsWith("_Acct"))
			result = "Accounting Tab are not exported by default: " + gridTab.getName();

		return result;
	}

}
