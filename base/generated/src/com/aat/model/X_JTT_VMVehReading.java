/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for JTT_VMVehReading
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_VMVehReading extends PO implements I_JTT_VMVehReading, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240514L;

    /** Standard Constructor */
    public X_JTT_VMVehReading (Properties ctx, int JTT_VMVehReading_ID, String trxName)
    {
      super (ctx, JTT_VMVehReading_ID, trxName);
      /** if (JTT_VMVehReading_ID == 0)
        {
			setJTT_VMVehReading_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_VMVehReading (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_VMVehReading[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Report Date.
		@param DateReport 
		Expense/Time Report Date
	  */
	public void setDateReport (Timestamp DateReport)
	{
		set_Value (COLUMNNAME_DateReport, DateReport);
	}

	/** Get Report Date.
		@return Expense/Time Report Date
	  */
	public Timestamp getDateReport () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateReport);
	}

	/** Set VM Veh Reading.
		@param JTT_VMVehReading_ID VM Veh Reading	  */
	public void setJTT_VMVehReading_ID (int JTT_VMVehReading_ID)
	{
		if (JTT_VMVehReading_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_VMVehReading_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_VMVehReading_ID, Integer.valueOf(JTT_VMVehReading_ID));
	}

	/** Get VM Veh Reading.
		@return VM Veh Reading	  */
	public int getJTT_VMVehReading_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMVehReading_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set KM Reading.
		@param KMReading KM Reading	  */
	public void setKMReading (int KMReading)
	{
		set_Value (COLUMNNAME_KMReading, Integer.valueOf(KMReading));
	}

	/** Get KM Reading.
		@return KM Reading	  */
	public int getKMReading () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_KMReading);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Remarks.
		@param Remarks Remarks	  */
	public void setRemarks (String Remarks)
	{
		set_Value (COLUMNNAME_Remarks, Remarks);
	}

	/** Get Remarks.
		@return Remarks	  */
	public String getRemarks () 
	{
		return (String)get_Value(COLUMNNAME_Remarks);
	}

	/** VehKMSource AD_Reference_ID=400012 */
	public static final int VEHKMSOURCE_AD_Reference_ID=400012;
	/** Fuel = F */
	public static final String VEHKMSOURCE_Fuel = "F";
	/** Job = J */
	public static final String VEHKMSOURCE_Job = "J";
	/** Tele = T */
	public static final String VEHKMSOURCE_Tele = "T";
	/** Set Veh KM Reading Src.
		@param VehKMSource 
		source of KM reading - Fuel, Job or Tele
	  */
	public void setVehKMSource (String VehKMSource)
	{

		set_Value (COLUMNNAME_VehKMSource, VehKMSource);
	}

	/** Get Veh KM Reading Src.
		@return source of KM reading - Fuel, Job or Tele
	  */
	public String getVehKMSource () 
	{
		return (String)get_Value(COLUMNNAME_VehKMSource);
	}
}