/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for JTT_VMPlan
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_VMPlan extends PO implements I_JTT_VMPlan, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240514L;

    /** Standard Constructor */
    public X_JTT_VMPlan (Properties ctx, int JTT_VMPlan_ID, String trxName)
    {
      super (ctx, JTT_VMPlan_ID, trxName);
      /** if (JTT_VMPlan_ID == 0)
        {
			setJTT_VMPlan_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_VMPlan (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_VMPlan[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Veh Maint. Plan.
		@param JTT_VMPlan_ID Veh Maint. Plan	  */
	public void setJTT_VMPlan_ID (int JTT_VMPlan_ID)
	{
		if (JTT_VMPlan_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_VMPlan_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_VMPlan_ID, Integer.valueOf(JTT_VMPlan_ID));
	}

	/** Get Veh Maint. Plan.
		@return Veh Maint. Plan	  */
	public int getJTT_VMPlan_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlan_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Assign Vehicle.
		@param Processing Assign Vehicle	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Assign Vehicle.
		@return Assign Vehicle	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}