/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.aat.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for JTT_VMSvcSchedule
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_JTT_VMSvcSchedule 
{

    /** TableName=JTT_VMSvcSchedule */
    public static final String Table_Name = "JTT_VMSvcSchedule";

    /** AD_Table_ID=400014 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_ID */
    public static final String COLUMNNAME_A_Asset_ID = "A_Asset_ID";

	/** Set Asset.
	  * Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID);

	/** Get Asset.
	  * Asset used internally or by customers
	  */
	public int getA_Asset_ID();

	public I_A_Asset getA_Asset() throws RuntimeException;

    /** Column name ActualSvcDate */
    public static final String COLUMNNAME_ActualSvcDate = "ActualSvcDate";

	/** Set Actual Service Date	  */
	public void setActualSvcDate (Timestamp ActualSvcDate);

	/** Get Actual Service Date	  */
	public Timestamp getActualSvcDate();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BookletKMDue */
    public static final String COLUMNNAME_BookletKMDue = "BookletKMDue";

	/** Set Booklet KM Due.
	  * This is the KM Due not taking into account prior service
	  */
	public void setBookletKMDue (int BookletKMDue);

	/** Get Booklet KM Due.
	  * This is the KM Due not taking into account prior service
	  */
	public int getBookletKMDue();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name ExpiryDate */
    public static final String COLUMNNAME_ExpiryDate = "ExpiryDate";

	/** Set Expiry Date.
	  * Date when Vehicle is due for service
	  */
	public void setExpiryDate (Timestamp ExpiryDate);

	/** Get Expiry Date.
	  * Date when Vehicle is due for service
	  */
	public Timestamp getExpiryDate();

    /** Column name ExpiryKM */
    public static final String COLUMNNAME_ExpiryKM = "ExpiryKM";

	/** Set Expiry KM	  */
	public void setExpiryKM (int ExpiryKM);

	/** Get Expiry KM	  */
	public int getExpiryKM();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name JTT_ProjServiceDate */
    public static final String COLUMNNAME_JTT_ProjServiceDate = "JTT_ProjServiceDate";

	/** Set Projected Service Date.
	  * Projected date of Service Using Current Odo reading and projecting to the Service planned KMs from the KM Use Rate Per Day
	  */
	public void setJTT_ProjServiceDate (Timestamp JTT_ProjServiceDate);

	/** Get Projected Service Date.
	  * Projected date of Service Using Current Odo reading and projecting to the Service planned KMs from the KM Use Rate Per Day
	  */
	public Timestamp getJTT_ProjServiceDate();

    /** Column name JTT_ProjWarningDate */
    public static final String COLUMNNAME_JTT_ProjWarningDate = "JTT_ProjWarningDate";

	/** Set Projected Warning Date.
	  * Projected date of Service Using Current Odo reading and projecting the Warning Date from the KM Use Rate Per Day
	  */
	public void setJTT_ProjWarningDate (Timestamp JTT_ProjWarningDate);

	/** Get Projected Warning Date.
	  * Projected date of Service Using Current Odo reading and projecting the Warning Date from the KM Use Rate Per Day
	  */
	public Timestamp getJTT_ProjWarningDate();

    /** Column name JTT_VehKMUseRate */
    public static final String COLUMNNAME_JTT_VehKMUseRate = "JTT_VehKMUseRate";

	/** Set Vehicle KM Use Rate.
	  * Calculated rate of KMs consumed by each Vehicle per day Done from the KM Vehicle readings ( Telematix)
	  */
	public void setJTT_VehKMUseRate (BigDecimal JTT_VehKMUseRate);

	/** Get Vehicle KM Use Rate.
	  * Calculated rate of KMs consumed by each Vehicle per day Done from the KM Vehicle readings ( Telematix)
	  */
	public BigDecimal getJTT_VehKMUseRate();

    /** Column name JTT_VMPlanSchedule_ID */
    public static final String COLUMNNAME_JTT_VMPlanSchedule_ID = "JTT_VMPlanSchedule_ID";

	/** Set Veh Maint. Plan Schedule	  */
	public void setJTT_VMPlanSchedule_ID (int JTT_VMPlanSchedule_ID);

	/** Get Veh Maint. Plan Schedule	  */
	public int getJTT_VMPlanSchedule_ID();

	public com.aat.model.I_JTT_VMPlanSchedule getJTT_VMPlanSchedule() throws RuntimeException;

    /** Column name JTT_VMSvcSchedule_ID */
    public static final String COLUMNNAME_JTT_VMSvcSchedule_ID = "JTT_VMSvcSchedule_ID";

	/** Set Veh Maint Svc Schedule ID	  */
	public void setJTT_VMSvcSchedule_ID (int JTT_VMSvcSchedule_ID);

	/** Get Veh Maint Svc Schedule ID	  */
	public int getJTT_VMSvcSchedule_ID();

    /** Column name KMReading */
    public static final String COLUMNNAME_KMReading = "KMReading";

	/** Set KM Reading	  */
	public void setKMReading (int KMReading);

	/** Get KM Reading	  */
	public int getKMReading();

    /** Column name LastKMReading */
    public static final String COLUMNNAME_LastKMReading = "LastKMReading";

	/** Set Last KM Reading	  */
	public void setLastKMReading (BigDecimal LastKMReading);

	/** Get Last KM Reading	  */
	public BigDecimal getLastKMReading();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VMPerformedBy */
    public static final String COLUMNNAME_VMPerformedBy = "VMPerformedBy";

	/** Set Veh Maint Performed By	  */
	public void setVMPerformedBy (int VMPerformedBy);

	/** Get Veh Maint Performed By	  */
	public int getVMPerformedBy();

	public I_C_BPartner getVMPerforme() throws RuntimeException;

    /** Column name WarningDate */
    public static final String COLUMNNAME_WarningDate = "WarningDate";

	/** Set Warning Date	  */
	public void setWarningDate (Timestamp WarningDate);

	/** Get Warning Date	  */
	public Timestamp getWarningDate();

    /** Column name WarningKM */
    public static final String COLUMNNAME_WarningKM = "WarningKM";

	/** Set Warning KM	  */
	public void setWarningKM (int WarningKM);

	/** Get Warning KM	  */
	public int getWarningKM();
}
