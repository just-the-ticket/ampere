/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for JTT_VehicleDefect
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_VehicleDefect extends PO implements I_JTT_VehicleDefect, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240827L;

    /** Standard Constructor */
    public X_JTT_VehicleDefect (Properties ctx, int JTT_VehicleDefect_ID, String trxName)
    {
      super (ctx, JTT_VehicleDefect_ID, trxName);
      /** if (JTT_VehicleDefect_ID == 0)
        {
			setJTT_VehicleDefect_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_VehicleDefect (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_VehicleDefect[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Vehicle.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Vehicle.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Project getC_Project() throws RuntimeException
    {
		return (I_C_Project)MTable.get(getCtx(), I_C_Project.Table_Name)
			.getPO(getC_Project_ID(), get_TrxName());	}

	/** Set Project.
		@param C_Project_ID 
		Financial Project
	  */
	public void setC_Project_ID (int C_Project_ID)
	{
		if (C_Project_ID < 1) 
			set_Value (COLUMNNAME_C_Project_ID, null);
		else 
			set_Value (COLUMNNAME_C_Project_ID, Integer.valueOf(C_Project_ID));
	}

	/** Get Project.
		@return Financial Project
	  */
	public int getC_Project_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Project_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Defect Fixed Y/N.
		@param IsDefectFixed Defect Fixed Y/N	  */
	public void setIsDefectFixed (boolean IsDefectFixed)
	{
		set_Value (COLUMNNAME_IsDefectFixed, Boolean.valueOf(IsDefectFixed));
	}

	/** Get Defect Fixed Y/N.
		@return Defect Fixed Y/N	  */
	public boolean isDefectFixed () 
	{
		Object oo = get_Value(COLUMNNAME_IsDefectFixed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Veh Defect Date .
		@param JTT_VehDefectDate Veh Defect Date 	  */
	public void setJTT_VehDefectDate (Timestamp JTT_VehDefectDate)
	{
		set_Value (COLUMNNAME_JTT_VehDefectDate, JTT_VehDefectDate);
	}

	/** Get Veh Defect Date .
		@return Veh Defect Date 	  */
	public Timestamp getJTT_VehDefectDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_JTT_VehDefectDate);
	}

	/** Set Vehicle Defect Description.
		@param JTT_VehDefectDesc Vehicle Defect Description	  */
	public void setJTT_VehDefectDesc (String JTT_VehDefectDesc)
	{
		set_Value (COLUMNNAME_JTT_VehDefectDesc, JTT_VehDefectDesc);
	}

	/** Get Vehicle Defect Description.
		@return Vehicle Defect Description	  */
	public String getJTT_VehDefectDesc () 
	{
		return (String)get_Value(COLUMNNAME_JTT_VehDefectDesc);
	}

	public I_AD_User getJTT_VehDefectFixe() throws RuntimeException
    {
		return (I_AD_User)MTable.get(getCtx(), I_AD_User.Table_Name)
			.getPO(getJTT_VehDefectFixedBy(), get_TrxName());	}

	/** Set Vehicle Defect Fixed By.
		@param JTT_VehDefectFixedBy Vehicle Defect Fixed By	  */
	public void setJTT_VehDefectFixedBy (int JTT_VehDefectFixedBy)
	{
		set_Value (COLUMNNAME_JTT_VehDefectFixedBy, Integer.valueOf(JTT_VehDefectFixedBy));
	}

	/** Get Vehicle Defect Fixed By.
		@return Vehicle Defect Fixed By	  */
	public int getJTT_VehDefectFixedBy () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VehDefectFixedBy);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Veh Defect Fixed Date.
		@param JTT_VehDefectFixedDate Veh Defect Fixed Date	  */
	public void setJTT_VehDefectFixedDate (Timestamp JTT_VehDefectFixedDate)
	{
		set_Value (COLUMNNAME_JTT_VehDefectFixedDate, JTT_VehDefectFixedDate);
	}

	/** Get Veh Defect Fixed Date.
		@return Veh Defect Fixed Date	  */
	public Timestamp getJTT_VehDefectFixedDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_JTT_VehDefectFixedDate);
	}

	/** Set Veh Defect Fixed Notes.
		@param JTT_VehDefectFixedNotes Veh Defect Fixed Notes	  */
	public void setJTT_VehDefectFixedNotes (String JTT_VehDefectFixedNotes)
	{
		set_Value (COLUMNNAME_JTT_VehDefectFixedNotes, JTT_VehDefectFixedNotes);
	}

	/** Get Veh Defect Fixed Notes.
		@return Veh Defect Fixed Notes	  */
	public String getJTT_VehDefectFixedNotes () 
	{
		return (String)get_Value(COLUMNNAME_JTT_VehDefectFixedNotes);
	}

	/** Set Vehicle Defect Number.
		@param JTT_VehDefectNo Vehicle Defect Number	  */
	public void setJTT_VehDefectNo (String JTT_VehDefectNo)
	{
		set_Value (COLUMNNAME_JTT_VehDefectNo, JTT_VehDefectNo);
	}

	/** Get Vehicle Defect Number.
		@return Vehicle Defect Number	  */
	public String getJTT_VehDefectNo () 
	{
		return (String)get_Value(COLUMNNAME_JTT_VehDefectNo);
	}

	public I_AD_User getJTT_VehDefectReporte() throws RuntimeException
    {
		return (I_AD_User)MTable.get(getCtx(), I_AD_User.Table_Name)
			.getPO(getJTT_VehDefectReportedBy(), get_TrxName());	}

	/** Set Vehicle Defect Reported By.
		@param JTT_VehDefectReportedBy Vehicle Defect Reported By	  */
	public void setJTT_VehDefectReportedBy (int JTT_VehDefectReportedBy)
	{
		set_Value (COLUMNNAME_JTT_VehDefectReportedBy, Integer.valueOf(JTT_VehDefectReportedBy));
	}

	/** Get Vehicle Defect Reported By.
		@return Vehicle Defect Reported By	  */
	public int getJTT_VehDefectReportedBy () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VehDefectReportedBy);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Vehicle Defects ID.
		@param JTT_VehicleDefect_ID Vehicle Defects ID	  */
	public void setJTT_VehicleDefect_ID (int JTT_VehicleDefect_ID)
	{
		if (JTT_VehicleDefect_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_VehicleDefect_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_VehicleDefect_ID, Integer.valueOf(JTT_VehicleDefect_ID));
	}

	/** Get Vehicle Defects ID.
		@return Vehicle Defects ID	  */
	public int getJTT_VehicleDefect_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VehicleDefect_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}