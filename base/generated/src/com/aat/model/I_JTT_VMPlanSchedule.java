/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.aat.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for JTT_VMPlanSchedule
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_JTT_VMPlanSchedule 
{

    /** TableName=JTT_VMPlanSchedule */
    public static final String Table_Name = "JTT_VMPlanSchedule";

    /** AD_Table_ID=400012 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AltGroup */
    public static final String COLUMNNAME_AltGroup = "AltGroup";

	/** Set Alt Group.
	  * Must set to particular name if this maint. schedule can be replaced if it overlaps with another schedule of the same group
	  */
	public void setAltGroup (String AltGroup);

	/** Get Alt Group.
	  * Must set to particular name if this maint. schedule can be replaced if it overlaps with another schedule of the same group
	  */
	public String getAltGroup();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IntervalKM */
    public static final String COLUMNNAME_IntervalKM = "IntervalKM";

	/** Set Interval KM.
	  * Must set to 0 if Interval in Months takes precedence
	  */
	public void setIntervalKM (int IntervalKM);

	/** Get Interval KM.
	  * Must set to 0 if Interval in Months takes precedence
	  */
	public int getIntervalKM();

    /** Column name IntervalMo */
    public static final String COLUMNNAME_IntervalMo = "IntervalMo";

	/** Set Interval in Months	  */
	public void setIntervalMo (int IntervalMo);

	/** Get Interval in Months	  */
	public int getIntervalMo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name JTT_VMPlan_ID */
    public static final String COLUMNNAME_JTT_VMPlan_ID = "JTT_VMPlan_ID";

	/** Set Veh Maint. Plan	  */
	public void setJTT_VMPlan_ID (int JTT_VMPlan_ID);

	/** Get Veh Maint. Plan	  */
	public int getJTT_VMPlan_ID();

	public I_JTT_VMPlan getJTT_VMPlan() throws RuntimeException;

    /** Column name JTT_VMPlanSchedule_ID */
    public static final String COLUMNNAME_JTT_VMPlanSchedule_ID = "JTT_VMPlanSchedule_ID";

	/** Set Veh Maint. Plan Schedule	  */
	public void setJTT_VMPlanSchedule_ID (int JTT_VMPlanSchedule_ID);

	/** Get Veh Maint. Plan Schedule	  */
	public int getJTT_VMPlanSchedule_ID();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Req Svc Kit.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Req Svc Kit.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name WarningKM */
    public static final String COLUMNNAME_WarningKM = "WarningKM";

	/** Set Warning KM	  */
	public void setWarningKM (int WarningKM);

	/** Get Warning KM	  */
	public int getWarningKM();

    /** Column name WarningNoDay */
    public static final String COLUMNNAME_WarningNoDay = "WarningNoDay";

	/** Set Warning No Day.
	  * No of days before due date to warn
	  */
	public void setWarningNoDay (int WarningNoDay);

	/** Get Warning No Day.
	  * No of days before due date to warn
	  */
	public int getWarningNoDay();
}
