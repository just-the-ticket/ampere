/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for JTT_VMPlanAssignment
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_VMPlanAssignment extends PO implements I_JTT_VMPlanAssignment, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240514L;

    /** Standard Constructor */
    public X_JTT_VMPlanAssignment (Properties ctx, int JTT_VMPlanAssignment_ID, String trxName)
    {
      super (ctx, JTT_VMPlanAssignment_ID, trxName);
      /** if (JTT_VMPlanAssignment_ID == 0)
        {
			setJTT_VMPlanAssignment_ID (0);
			setJTT_VMPlan_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_VMPlanAssignment (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_VMPlanAssignment[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Veh Maint. Plan Assignment ID.
		@param JTT_VMPlanAssignment_ID Veh Maint. Plan Assignment ID	  */
	public void setJTT_VMPlanAssignment_ID (int JTT_VMPlanAssignment_ID)
	{
		if (JTT_VMPlanAssignment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_VMPlanAssignment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_VMPlanAssignment_ID, Integer.valueOf(JTT_VMPlanAssignment_ID));
	}

	/** Get Veh Maint. Plan Assignment ID.
		@return Veh Maint. Plan Assignment ID	  */
	public int getJTT_VMPlanAssignment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlanAssignment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_JTT_VMPlan getJTT_VMPlan() throws RuntimeException
    {
		return (I_JTT_VMPlan)MTable.get(getCtx(), I_JTT_VMPlan.Table_Name)
			.getPO(getJTT_VMPlan_ID(), get_TrxName());	}

	/** Set Veh Maint. Plan.
		@param JTT_VMPlan_ID Veh Maint. Plan	  */
	public void setJTT_VMPlan_ID (int JTT_VMPlan_ID)
	{
		if (JTT_VMPlan_ID < 1) 
			set_Value (COLUMNNAME_JTT_VMPlan_ID, null);
		else 
			set_Value (COLUMNNAME_JTT_VMPlan_ID, Integer.valueOf(JTT_VMPlan_ID));
	}

	/** Get Veh Maint. Plan.
		@return Veh Maint. Plan	  */
	public int getJTT_VMPlan_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlan_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}