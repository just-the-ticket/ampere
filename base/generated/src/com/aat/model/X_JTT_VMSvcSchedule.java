/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for JTT_VMSvcSchedule
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_VMSvcSchedule extends PO implements I_JTT_VMSvcSchedule, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240827L;

    /** Standard Constructor */
    public X_JTT_VMSvcSchedule (Properties ctx, int JTT_VMSvcSchedule_ID, String trxName)
    {
      super (ctx, JTT_VMSvcSchedule_ID, trxName);
      /** if (JTT_VMSvcSchedule_ID == 0)
        {
			setJTT_VMSvcSchedule_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_VMSvcSchedule (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_VMSvcSchedule[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Actual Service Date.
		@param ActualSvcDate Actual Service Date	  */
	public void setActualSvcDate (Timestamp ActualSvcDate)
	{
		set_Value (COLUMNNAME_ActualSvcDate, ActualSvcDate);
	}

	/** Get Actual Service Date.
		@return Actual Service Date	  */
	public Timestamp getActualSvcDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ActualSvcDate);
	}

	/** Set Booklet KM Due.
		@param BookletKMDue 
		This is the KM Due not taking into account prior service
	  */
	public void setBookletKMDue (int BookletKMDue)
	{
		set_Value (COLUMNNAME_BookletKMDue, Integer.valueOf(BookletKMDue));
	}

	/** Get Booklet KM Due.
		@return This is the KM Due not taking into account prior service
	  */
	public int getBookletKMDue () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_BookletKMDue);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Expiry Date.
		@param ExpiryDate 
		Date when Vehicle is due for service
	  */
	public void setExpiryDate (Timestamp ExpiryDate)
	{
		set_Value (COLUMNNAME_ExpiryDate, ExpiryDate);
	}

	/** Get Expiry Date.
		@return Date when Vehicle is due for service
	  */
	public Timestamp getExpiryDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ExpiryDate);
	}

	/** Set Expiry KM.
		@param ExpiryKM Expiry KM	  */
	public void setExpiryKM (int ExpiryKM)
	{
		set_Value (COLUMNNAME_ExpiryKM, Integer.valueOf(ExpiryKM));
	}

	/** Get Expiry KM.
		@return Expiry KM	  */
	public int getExpiryKM () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ExpiryKM);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Projected Service Date.
		@param JTT_ProjServiceDate 
		Projected date of Service Using Current Odo reading and projecting to the Service planned KMs from the KM Use Rate Per Day
	  */
	public void setJTT_ProjServiceDate (Timestamp JTT_ProjServiceDate)
	{
		set_Value (COLUMNNAME_JTT_ProjServiceDate, JTT_ProjServiceDate);
	}

	/** Get Projected Service Date.
		@return Projected date of Service Using Current Odo reading and projecting to the Service planned KMs from the KM Use Rate Per Day
	  */
	public Timestamp getJTT_ProjServiceDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_JTT_ProjServiceDate);
	}

	/** Set Projected Warning Date.
		@param JTT_ProjWarningDate 
		Projected date of Service Using Current Odo reading and projecting the Warning Date from the KM Use Rate Per Day
	  */
	public void setJTT_ProjWarningDate (Timestamp JTT_ProjWarningDate)
	{
		set_Value (COLUMNNAME_JTT_ProjWarningDate, JTT_ProjWarningDate);
	}

	/** Get Projected Warning Date.
		@return Projected date of Service Using Current Odo reading and projecting the Warning Date from the KM Use Rate Per Day
	  */
	public Timestamp getJTT_ProjWarningDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_JTT_ProjWarningDate);
	}

	/** Set Vehicle KM Use Rate.
		@param JTT_VehKMUseRate 
		Calculated rate of KMs consumed by each Vehicle per day Done from the KM Vehicle readings ( Telematix)
	  */
	public void setJTT_VehKMUseRate (BigDecimal JTT_VehKMUseRate)
	{
		set_Value (COLUMNNAME_JTT_VehKMUseRate, JTT_VehKMUseRate);
	}

	/** Get Vehicle KM Use Rate.
		@return Calculated rate of KMs consumed by each Vehicle per day Done from the KM Vehicle readings ( Telematix)
	  */
	public BigDecimal getJTT_VehKMUseRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JTT_VehKMUseRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.aat.model.I_JTT_VMPlanSchedule getJTT_VMPlanSchedule() throws RuntimeException
    {
		return (com.aat.model.I_JTT_VMPlanSchedule)MTable.get(getCtx(), com.aat.model.I_JTT_VMPlanSchedule.Table_Name)
			.getPO(getJTT_VMPlanSchedule_ID(), get_TrxName());	}

	/** Set Veh Maint. Plan Schedule.
		@param JTT_VMPlanSchedule_ID Veh Maint. Plan Schedule	  */
	public void setJTT_VMPlanSchedule_ID (int JTT_VMPlanSchedule_ID)
	{
		if (JTT_VMPlanSchedule_ID < 1) 
			set_Value (COLUMNNAME_JTT_VMPlanSchedule_ID, null);
		else 
			set_Value (COLUMNNAME_JTT_VMPlanSchedule_ID, Integer.valueOf(JTT_VMPlanSchedule_ID));
	}

	/** Get Veh Maint. Plan Schedule.
		@return Veh Maint. Plan Schedule	  */
	public int getJTT_VMPlanSchedule_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlanSchedule_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Veh Maint Svc Schedule ID.
		@param JTT_VMSvcSchedule_ID Veh Maint Svc Schedule ID	  */
	public void setJTT_VMSvcSchedule_ID (int JTT_VMSvcSchedule_ID)
	{
		if (JTT_VMSvcSchedule_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_VMSvcSchedule_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_VMSvcSchedule_ID, Integer.valueOf(JTT_VMSvcSchedule_ID));
	}

	/** Get Veh Maint Svc Schedule ID.
		@return Veh Maint Svc Schedule ID	  */
	public int getJTT_VMSvcSchedule_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMSvcSchedule_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set KM Reading.
		@param KMReading KM Reading	  */
	public void setKMReading (int KMReading)
	{
		set_Value (COLUMNNAME_KMReading, Integer.valueOf(KMReading));
	}

	/** Get KM Reading.
		@return KM Reading	  */
	public int getKMReading () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_KMReading);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Last KM Reading.
		@param LastKMReading Last KM Reading	  */
	public void setLastKMReading (BigDecimal LastKMReading)
	{
		throw new IllegalArgumentException ("LastKMReading is virtual column");	}

	/** Get Last KM Reading.
		@return Last KM Reading	  */
	public BigDecimal getLastKMReading () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LastKMReading);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public I_C_BPartner getVMPerforme() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getVMPerformedBy(), get_TrxName());	}

	/** Set Veh Maint Performed By.
		@param VMPerformedBy Veh Maint Performed By	  */
	public void setVMPerformedBy (int VMPerformedBy)
	{
		set_Value (COLUMNNAME_VMPerformedBy, Integer.valueOf(VMPerformedBy));
	}

	/** Get Veh Maint Performed By.
		@return Veh Maint Performed By	  */
	public int getVMPerformedBy () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_VMPerformedBy);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Warning Date.
		@param WarningDate Warning Date	  */
	public void setWarningDate (Timestamp WarningDate)
	{
		set_Value (COLUMNNAME_WarningDate, WarningDate);
	}

	/** Get Warning Date.
		@return Warning Date	  */
	public Timestamp getWarningDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_WarningDate);
	}

	/** Set Warning KM.
		@param WarningKM Warning KM	  */
	public void setWarningKM (int WarningKM)
	{
		set_Value (COLUMNNAME_WarningKM, Integer.valueOf(WarningKM));
	}

	/** Get Warning KM.
		@return Warning KM	  */
	public int getWarningKM () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_WarningKM);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}