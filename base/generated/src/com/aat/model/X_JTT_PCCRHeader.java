/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for JTT_PCCRHeader
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_PCCRHeader extends PO implements I_JTT_PCCRHeader, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240507L;

    /** Standard Constructor */
    public X_JTT_PCCRHeader (Properties ctx, int JTT_PCCRHeader_ID, String trxName)
    {
      super (ctx, JTT_PCCRHeader_ID, trxName);
      /** if (JTT_PCCRHeader_ID == 0)
        {
			setJTT_PCCRHeader_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_PCCRHeader (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_PCCRHeader[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Price includes Tax.
		@param IsTaxIncluded 
		Tax is included in the price 
	  */
	public void setIsTaxIncluded (boolean IsTaxIncluded)
	{
		set_Value (COLUMNNAME_IsTaxIncluded, Boolean.valueOf(IsTaxIncluded));
	}

	/** Get Price includes Tax.
		@return Tax is included in the price 
	  */
	public boolean isTaxIncluded () 
	{
		Object oo = get_Value(COLUMNNAME_IsTaxIncluded);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set PCCR Header.
		@param JTT_PCCRHeader_ID PCCR Header	  */
	public void setJTT_PCCRHeader_ID (int JTT_PCCRHeader_ID)
	{
		if (JTT_PCCRHeader_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_PCCRHeader_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_PCCRHeader_ID, Integer.valueOf(JTT_PCCRHeader_ID));
	}

	/** Get PCCR Header.
		@return PCCR Header	  */
	public int getJTT_PCCRHeader_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_PCCRHeader_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Valid_From.
		@param Valid_From Valid_From	  */
	public void setValid_From (Timestamp Valid_From)
	{
		set_Value (COLUMNNAME_Valid_From, Valid_From);
	}

	/** Get Valid_From.
		@return Valid_From	  */
	public Timestamp getValid_From () 
	{
		return (Timestamp)get_Value(COLUMNNAME_Valid_From);
	}

	/** Set Valid_To.
		@param Valid_To Valid_To	  */
	public void setValid_To (Timestamp Valid_To)
	{
		set_Value (COLUMNNAME_Valid_To, Valid_To);
	}

	/** Get Valid_To.
		@return Valid_To	  */
	public Timestamp getValid_To () 
	{
		return (Timestamp)get_Value(COLUMNNAME_Valid_To);
	}
}