/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for JTT_WrkshopJobSched
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_WrkshopJobSched extends PO implements I_JTT_WrkshopJobSched, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240827L;

    /** Standard Constructor */
    public X_JTT_WrkshopJobSched (Properties ctx, int JTT_WrkshopJobSched_ID, String trxName)
    {
      super (ctx, JTT_WrkshopJobSched_ID, trxName);
      /** if (JTT_WrkshopJobSched_ID == 0)
        {
			setJTT_WrkshopJobSched_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_WrkshopJobSched (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_WrkshopJobSched[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_Project getC_Project() throws RuntimeException
    {
		return (I_C_Project)MTable.get(getCtx(), I_C_Project.Table_Name)
			.getPO(getC_Project_ID(), get_TrxName());	}

	/** Set Project.
		@param C_Project_ID 
		Financial Project
	  */
	public void setC_Project_ID (int C_Project_ID)
	{
		if (C_Project_ID < 1) 
			set_Value (COLUMNNAME_C_Project_ID, null);
		else 
			set_Value (COLUMNNAME_C_Project_ID, Integer.valueOf(C_Project_ID));
	}

	/** Get Project.
		@return Financial Project
	  */
	public int getC_Project_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Project_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.aat.model.I_JTT_VMPlanSchedule getJTT_VMPlanSchedule() throws RuntimeException
    {
		return (com.aat.model.I_JTT_VMPlanSchedule)MTable.get(getCtx(), com.aat.model.I_JTT_VMPlanSchedule.Table_Name)
			.getPO(getJTT_VMPlanSchedule_ID(), get_TrxName());	}

	/** Set Veh Maint. Plan Schedule.
		@param JTT_VMPlanSchedule_ID Veh Maint. Plan Schedule	  */
	public void setJTT_VMPlanSchedule_ID (int JTT_VMPlanSchedule_ID)
	{
		if (JTT_VMPlanSchedule_ID < 1) 
			set_Value (COLUMNNAME_JTT_VMPlanSchedule_ID, null);
		else 
			set_Value (COLUMNNAME_JTT_VMPlanSchedule_ID, Integer.valueOf(JTT_VMPlanSchedule_ID));
	}

	/** Get Veh Maint. Plan Schedule.
		@return Veh Maint. Plan Schedule	  */
	public int getJTT_VMPlanSchedule_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlanSchedule_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.aat.model.I_JTT_VMSvcSchedule getJTT_VMSvcSchedule() throws RuntimeException
    {
		return (com.aat.model.I_JTT_VMSvcSchedule)MTable.get(getCtx(), com.aat.model.I_JTT_VMSvcSchedule.Table_Name)
			.getPO(getJTT_VMSvcSchedule_ID(), get_TrxName());	}

	/** Set Veh Maint Svc Schedule.
		@param JTT_VMSvcSchedule_ID Veh Maint Svc Schedule	  */
	public void setJTT_VMSvcSchedule_ID (int JTT_VMSvcSchedule_ID)
	{
		if (JTT_VMSvcSchedule_ID < 1) 
			set_Value (COLUMNNAME_JTT_VMSvcSchedule_ID, null);
		else 
			set_Value (COLUMNNAME_JTT_VMSvcSchedule_ID, Integer.valueOf(JTT_VMSvcSchedule_ID));
	}

	/** Get Veh Maint Svc Schedule.
		@return Veh Maint Svc Schedule	  */
	public int getJTT_VMSvcSchedule_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMSvcSchedule_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Workshop Job Schedule ID.
		@param JTT_WrkshopJobSched_ID Workshop Job Schedule ID	  */
	public void setJTT_WrkshopJobSched_ID (int JTT_WrkshopJobSched_ID)
	{
		if (JTT_WrkshopJobSched_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_WrkshopJobSched_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_WrkshopJobSched_ID, Integer.valueOf(JTT_WrkshopJobSched_ID));
	}

	/** Get Workshop Job Schedule ID.
		@return Workshop Job Schedule ID	  */
	public int getJTT_WrkshopJobSched_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_WrkshopJobSched_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}