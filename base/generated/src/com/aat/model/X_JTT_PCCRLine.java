/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for JTT_PCCRLine
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_PCCRLine extends PO implements I_JTT_PCCRLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240517L;

    /** Standard Constructor */
    public X_JTT_PCCRLine (Properties ctx, int JTT_PCCRLine_ID, String trxName)
    {
      super (ctx, JTT_PCCRLine_ID, trxName);
      /** if (JTT_PCCRLine_ID == 0)
        {
			setJTT_PCCRLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_PCCRLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_PCCRLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_Order getC_Order() throws RuntimeException
    {
		return (I_C_Order)MTable.get(getCtx(), I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_JTT_PCCRHeader getJTT_PCCRHeader() throws RuntimeException
    {
		return (I_JTT_PCCRHeader)MTable.get(getCtx(), I_JTT_PCCRHeader.Table_Name)
			.getPO(getJTT_PCCRHeader_ID(), get_TrxName());	}

	/** Set PCCR Header.
		@param JTT_PCCRHeader_ID PCCR Header	  */
	public void setJTT_PCCRHeader_ID (int JTT_PCCRHeader_ID)
	{
		if (JTT_PCCRHeader_ID < 1) 
			set_Value (COLUMNNAME_JTT_PCCRHeader_ID, null);
		else 
			set_Value (COLUMNNAME_JTT_PCCRHeader_ID, Integer.valueOf(JTT_PCCRHeader_ID));
	}

	/** Get PCCR Header.
		@return PCCR Header	  */
	public int getJTT_PCCRHeader_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_PCCRHeader_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Packaged Tour.
		@param JTT_PCCRLine_ID Packaged Tour	  */
	public void setJTT_PCCRLine_ID (int JTT_PCCRLine_ID)
	{
		if (JTT_PCCRLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_PCCRLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_PCCRLine_ID, Integer.valueOf(JTT_PCCRLine_ID));
	}

	/** Get Packaged Tour.
		@return Packaged Tour	  */
	public int getJTT_PCCRLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_PCCRLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Large.
		@param VSRL 
		Rate for Large Vehicle
	  */
	public void setVSRL (BigDecimal VSRL)
	{
		set_Value (COLUMNNAME_VSRL, VSRL);
	}

	/** Get Large.
		@return Rate for Large Vehicle
	  */
	public BigDecimal getVSRL () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VSRL);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Medium.
		@param VSRMIDI 
		Rate for Medium Vehicle
	  */
	public void setVSRMIDI (BigDecimal VSRMIDI)
	{
		set_Value (COLUMNNAME_VSRMIDI, VSRMIDI);
	}

	/** Get Medium.
		@return Rate for Medium Vehicle
	  */
	public BigDecimal getVSRMIDI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VSRMIDI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Small.
		@param VSRMINI 
		Rate for Mini Vehicle
	  */
	public void setVSRMINI (BigDecimal VSRMINI)
	{
		set_Value (COLUMNNAME_VSRMINI, VSRMINI);
	}

	/** Get Small.
		@return Rate for Mini Vehicle
	  */
	public BigDecimal getVSRMINI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VSRMINI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}