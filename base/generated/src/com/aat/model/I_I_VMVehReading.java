/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.aat.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for I_VMVehReading
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_I_VMVehReading 
{

    /** TableName=I_VMVehReading */
    public static final String Table_Name = "I_VMVehReading";

    /** AD_Table_ID=1000004 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_ID */
    public static final String COLUMNNAME_A_Asset_ID = "A_Asset_ID";

	/** Set Asset.
	  * Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID);

	/** Get Asset.
	  * Asset used internally or by customers
	  */
	public int getA_Asset_ID();

	public I_A_Asset getA_Asset() throws RuntimeException;

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateReport */
    public static final String COLUMNNAME_DateReport = "DateReport";

	/** Set Report Date.
	  * Expense/Time Report Date
	  */
	public void setDateReport (Timestamp DateReport);

	/** Get Report Date.
	  * Expense/Time Report Date
	  */
	public Timestamp getDateReport();

    /** Column name I_ErrorMsg */
    public static final String COLUMNNAME_I_ErrorMsg = "I_ErrorMsg";

	/** Set Import Error Message.
	  * Messages generated from import process
	  */
	public void setI_ErrorMsg (String I_ErrorMsg);

	/** Get Import Error Message.
	  * Messages generated from import process
	  */
	public String getI_ErrorMsg();

    /** Column name I_IsImported */
    public static final String COLUMNNAME_I_IsImported = "I_IsImported";

	/** Set Imported.
	  * Has this import been processed
	  */
	public void setI_IsImported (boolean I_IsImported);

	/** Get Imported.
	  * Has this import been processed
	  */
	public boolean isI_IsImported();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name I_VMVehReading_ID */
    public static final String COLUMNNAME_I_VMVehReading_ID = "I_VMVehReading_ID";

	/** Set Import Vehicle KM Reading ID	  */
	public void setI_VMVehReading_ID (int I_VMVehReading_ID);

	/** Get Import Vehicle KM Reading ID	  */
	public int getI_VMVehReading_ID();

    /** Column name JTT_VMVehReading_ID */
    public static final String COLUMNNAME_JTT_VMVehReading_ID = "JTT_VMVehReading_ID";

	/** Set VM Veh Reading	  */
	public void setJTT_VMVehReading_ID (int JTT_VMVehReading_ID);

	/** Get VM Veh Reading	  */
	public int getJTT_VMVehReading_ID();

	public com.aat.model.I_JTT_VMVehReading getJTT_VMVehReading() throws RuntimeException;

    /** Column name KMReading */
    public static final String COLUMNNAME_KMReading = "KMReading";

	/** Set KM Reading	  */
	public void setKMReading (int KMReading);

	/** Get KM Reading	  */
	public int getKMReading();

    /** Column name Process */
    public static final String COLUMNNAME_Process = "Process";

	/** Set Process.
	  * Process the reversal immediately
	  */
	public void setProcess (String Process);

	/** Get Process.
	  * Process the reversal immediately
	  */
	public String getProcess();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name T_Date */
    public static final String COLUMNNAME_T_Date = "T_Date";

	/** Set Date	  */
	public void setT_Date (Timestamp T_Date);

	/** Get Date	  */
	public Timestamp getT_Date();

    /** Column name T_Time */
    public static final String COLUMNNAME_T_Time = "T_Time";

	/** Set T_Time	  */
	public void setT_Time (Timestamp T_Time);

	/** Get T_Time	  */
	public Timestamp getT_Time();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Fleet ID.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Fleet ID.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();

    /** Column name VehKMSource */
    public static final String COLUMNNAME_VehKMSource = "VehKMSource";

	/** Set Veh KM Reading Src.
	  * source of KM reading - Fuel, Job or Tele
	  */
	public void setVehKMSource (String VehKMSource);

	/** Get Veh KM Reading Src.
	  * source of KM reading - Fuel, Job or Tele
	  */
	public String getVehKMSource();
}
