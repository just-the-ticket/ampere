/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.aat.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for JTT_VMPlanSchedule
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_JTT_VMPlanSchedule extends PO implements I_JTT_VMPlanSchedule, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240514L;

    /** Standard Constructor */
    public X_JTT_VMPlanSchedule (Properties ctx, int JTT_VMPlanSchedule_ID, String trxName)
    {
      super (ctx, JTT_VMPlanSchedule_ID, trxName);
      /** if (JTT_VMPlanSchedule_ID == 0)
        {
			setJTT_VMPlanSchedule_ID (0);
        } */
    }

    /** Load Constructor */
    public X_JTT_VMPlanSchedule (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_JTT_VMPlanSchedule[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Alt Group.
		@param AltGroup 
		Must set to particular name if this maint. schedule can be replaced if it overlaps with another schedule of the same group
	  */
	public void setAltGroup (String AltGroup)
	{
		set_Value (COLUMNNAME_AltGroup, AltGroup);
	}

	/** Get Alt Group.
		@return Must set to particular name if this maint. schedule can be replaced if it overlaps with another schedule of the same group
	  */
	public String getAltGroup () 
	{
		return (String)get_Value(COLUMNNAME_AltGroup);
	}

	/** Set Interval KM.
		@param IntervalKM 
		Must set to 0 if Interval in Months takes precedence
	  */
	public void setIntervalKM (int IntervalKM)
	{
		set_Value (COLUMNNAME_IntervalKM, Integer.valueOf(IntervalKM));
	}

	/** Get Interval KM.
		@return Must set to 0 if Interval in Months takes precedence
	  */
	public int getIntervalKM () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_IntervalKM);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Interval in Months.
		@param IntervalMo Interval in Months	  */
	public void setIntervalMo (int IntervalMo)
	{
		set_Value (COLUMNNAME_IntervalMo, Integer.valueOf(IntervalMo));
	}

	/** Get Interval in Months.
		@return Interval in Months	  */
	public int getIntervalMo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_IntervalMo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_JTT_VMPlan getJTT_VMPlan() throws RuntimeException
    {
		return (I_JTT_VMPlan)MTable.get(getCtx(), I_JTT_VMPlan.Table_Name)
			.getPO(getJTT_VMPlan_ID(), get_TrxName());	}

	/** Set Veh Maint. Plan.
		@param JTT_VMPlan_ID Veh Maint. Plan	  */
	public void setJTT_VMPlan_ID (int JTT_VMPlan_ID)
	{
		if (JTT_VMPlan_ID < 1) 
			set_Value (COLUMNNAME_JTT_VMPlan_ID, null);
		else 
			set_Value (COLUMNNAME_JTT_VMPlan_ID, Integer.valueOf(JTT_VMPlan_ID));
	}

	/** Get Veh Maint. Plan.
		@return Veh Maint. Plan	  */
	public int getJTT_VMPlan_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlan_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Veh Maint. Plan Schedule.
		@param JTT_VMPlanSchedule_ID Veh Maint. Plan Schedule	  */
	public void setJTT_VMPlanSchedule_ID (int JTT_VMPlanSchedule_ID)
	{
		if (JTT_VMPlanSchedule_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_JTT_VMPlanSchedule_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_JTT_VMPlanSchedule_ID, Integer.valueOf(JTT_VMPlanSchedule_ID));
	}

	/** Get Veh Maint. Plan Schedule.
		@return Veh Maint. Plan Schedule	  */
	public int getJTT_VMPlanSchedule_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JTT_VMPlanSchedule_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Req Svc Kit.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Req Svc Kit.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Warning KM.
		@param WarningKM Warning KM	  */
	public void setWarningKM (int WarningKM)
	{
		set_Value (COLUMNNAME_WarningKM, Integer.valueOf(WarningKM));
	}

	/** Get Warning KM.
		@return Warning KM	  */
	public int getWarningKM () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_WarningKM);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Warning No Day.
		@param WarningNoDay 
		No of days before due date to warn
	  */
	public void setWarningNoDay (int WarningNoDay)
	{
		set_Value (COLUMNNAME_WarningNoDay, Integer.valueOf(WarningNoDay));
	}

	/** Get Warning No Day.
		@return No of days before due date to warn
	  */
	public int getWarningNoDay () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_WarningNoDay);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}