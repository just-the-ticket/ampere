/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for AD_Table_Attribute
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_AD_Table_Attribute extends PO implements I_AD_Table_Attribute, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20221102L;

    /** Standard Constructor */
    public X_AD_Table_Attribute (Properties ctx, int AD_Table_Attribute_ID, String trxName)
    {
      super (ctx, AD_Table_Attribute_ID, trxName);
      /** if (AD_Table_Attribute_ID == 0)
        {
			setAD_Table_Attribute_ID (0);
			setAD_Table_ID (0);
			setName (null);
			setTableAttributeDefinition (null);
        } */
    }

    /** Load Constructor */
    public X_AD_Table_Attribute (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AD_Table_Attribute[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Table Attribute.
		@param AD_Table_Attribute_ID 
		Defines an attribute (e.g. constraint or index) of the database table
	  */
	public void setAD_Table_Attribute_ID (int AD_Table_Attribute_ID)
	{
		if (AD_Table_Attribute_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Table_Attribute_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Table_Attribute_ID, Integer.valueOf(AD_Table_Attribute_ID));
	}

	/** Get Table Attribute.
		@return Defines an attribute (e.g. constraint or index) of the database table
	  */
	public int getAD_Table_Attribute_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_Attribute_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Table getAD_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Table_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Message getError() throws RuntimeException
    {
		return (I_AD_Message)MTable.get(getCtx(), I_AD_Message.Table_Name)
			.getPO(getErrorMsg(), get_TrxName());	}

	/** Set Error Msg.
		@param ErrorMsg Error Msg	  */
	public void setErrorMsg (int ErrorMsg)
	{
		set_Value (COLUMNNAME_ErrorMsg, Integer.valueOf(ErrorMsg));
	}

	/** Get Error Msg.
		@return Error Msg	  */
	public int getErrorMsg () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ErrorMsg);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Attribute Definition.
		@param TableAttributeDefinition 
		DDL snippet defining a postgresql table index or constraint
	  */
	public void setTableAttributeDefinition (String TableAttributeDefinition)
	{
		set_Value (COLUMNNAME_TableAttributeDefinition, TableAttributeDefinition);
	}

	/** Get Attribute Definition.
		@return DDL snippet defining a postgresql table index or constraint
	  */
	public String getTableAttributeDefinition () 
	{
		return (String)get_Value(COLUMNNAME_TableAttributeDefinition);
	}

	/** TableAttributeType AD_Reference_ID=400002 */
	public static final int TABLEATTRIBUTETYPE_AD_Reference_ID=400002;
	/** Primary key = CP */
	public static final String TABLEATTRIBUTETYPE_PrimaryKey = "CP";
	/** Foreign key = CF */
	public static final String TABLEATTRIBUTETYPE_ForeignKey = "CF";
	/** Full text search index = IF */
	public static final String TABLEATTRIBUTETYPE_FullTextSearchIndex = "IF";
	/** Index = I */
	public static final String TABLEATTRIBUTETYPE_Index = "I";
	/** Check constraint = CC */
	public static final String TABLEATTRIBUTETYPE_CheckConstraint = "CC";
	/** Unique constraint = CU */
	public static final String TABLEATTRIBUTETYPE_UniqueConstraint = "CU";
	/** Unique index = IU */
	public static final String TABLEATTRIBUTETYPE_UniqueIndex = "IU";
	/** Exclusion Constraint = CE */
	public static final String TABLEATTRIBUTETYPE_ExclusionConstraint = "CE";
	/** Set Attribute Type.
		@param TableAttributeType 
		Type of table attribute (primary key, index, constraint)
	  */
	public void setTableAttributeType (String TableAttributeType)
	{

		set_Value (COLUMNNAME_TableAttributeType, TableAttributeType);
	}

	/** Get Attribute Type.
		@return Type of table attribute (primary key, index, constraint)
	  */
	public String getTableAttributeType () 
	{
		return (String)get_Value(COLUMNNAME_TableAttributeType);
	}
}