/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for AX_Seasonality
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_AX_Seasonality 
{

    /** TableName=AX_Seasonality */
    public static final String Table_Name = "AX_Seasonality";

    /** AD_Table_ID=53950 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Apr */
    public static final String COLUMNNAME_Apr = "Apr";

	/** Set April	  */
	public void setApr (int Apr);

	/** Get April	  */
	public int getApr();

    /** Column name Aug */
    public static final String COLUMNNAME_Aug = "Aug";

	/** Set August	  */
	public void setAug (int Aug);

	/** Get August	  */
	public int getAug();

    /** Column name AX_Seasonality_ID */
    public static final String COLUMNNAME_AX_Seasonality_ID = "AX_Seasonality_ID";

	/** Set Seasonality Type.
	  * Seasonality Type
	  */
	public void setAX_Seasonality_ID (int AX_Seasonality_ID);

	/** Get Seasonality Type.
	  * Seasonality Type
	  */
	public int getAX_Seasonality_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Dec */
    public static final String COLUMNNAME_Dec = "Dec";

	/** Set December	  */
	public void setDec (int Dec);

	/** Get December	  */
	public int getDec();

    /** Column name Feb */
    public static final String COLUMNNAME_Feb = "Feb";

	/** Set February	  */
	public void setFeb (int Feb);

	/** Get February	  */
	public int getFeb();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Jan */
    public static final String COLUMNNAME_Jan = "Jan";

	/** Set January	  */
	public void setJan (int Jan);

	/** Get January	  */
	public int getJan();

    /** Column name Jul */
    public static final String COLUMNNAME_Jul = "Jul";

	/** Set July	  */
	public void setJul (int Jul);

	/** Get July	  */
	public int getJul();

    /** Column name Jun */
    public static final String COLUMNNAME_Jun = "Jun";

	/** Set June	  */
	public void setJun (int Jun);

	/** Get June	  */
	public int getJun();

    /** Column name Mar */
    public static final String COLUMNNAME_Mar = "Mar";

	/** Set March	  */
	public void setMar (int Mar);

	/** Get March	  */
	public int getMar();

    /** Column name May */
    public static final String COLUMNNAME_May = "May";

	/** Set May	  */
	public void setMay (int May);

	/** Get May	  */
	public int getMay();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Nov */
    public static final String COLUMNNAME_Nov = "Nov";

	/** Set November	  */
	public void setNov (int Nov);

	/** Get November	  */
	public int getNov();

    /** Column name Oct */
    public static final String COLUMNNAME_Oct = "Oct";

	/** Set October	  */
	public void setOct (int Oct);

	/** Get October	  */
	public int getOct();

    /** Column name Sep */
    public static final String COLUMNNAME_Sep = "Sep";

	/** Set September	  */
	public void setSep (int Sep);

	/** Get September	  */
	public int getSep();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
