ALTER TABLE ampere.ad_column DROP CONSTRAINT ad_column_isencrypted_check;
ALTER TABLE ampere.ad_infoprocess ADD COLUMN isalwaysenabled char(1) DEFAULT 'N'::bpchar;
ALTER TABLE ampere.ad_user ADD COLUMN emailshortname text;

--DATA

INSERT INTO ad_element (ad_element_id,ad_client_id,ad_org_id,isactive,created,createdby,updated,updatedby,columnname,entitytype,name,printname,description,help,po_name,po_printname,po_description,po_help,ad_reference_id,fieldlength,ad_reference_value_id) 
VALUES
  (300012,0,0,'Y',TIMESTAMP '2023-03-17 12:54:56.000000',100,TIMESTAMP '2023-03-17 12:54:56.000000',100,'IsAlwaysEnabled','U','Always Enabled','Always Enabled',NULL,NULL,NULL,NULL,NULL,NULL,20,1,NULL);

INSERT INTO ad_column (ad_column_id,ad_client_id,ad_org_id,isactive,created,updated,createdby,updatedby,name,description,help,version,entitytype,columnname,ad_table_id,ad_reference_id,ad_reference_value_id,ad_val_rule_id,fieldlength,defaultvalue,iskey,isparent,ismandatory,isupdateable,readonlylogic,isidentifier,seqno,istranslated,isencrypted,callout,vformat,valuemin,valuemax,isselectioncolumn,ad_element_id,ad_process_id,issyncdatabase,isalwaysupdateable,columnsql,mandatorylogic,infofactoryclass,isautocomplete,isallowlogging,formatpattern,ad_chart_id,isallowcopy,issecure,istoolbarbutton,ad_infowindow_id,isincludeints,next_column_id) 
VALUES
  (300067,0,0,'Y',TIMESTAMP '2023-03-17 12:55:46.000000',TIMESTAMP '2023-03-17 12:55:46.000000',100,100,'Always Enabled',NULL,NULL,0,'Application Dictionary','IsAlwaysEnabled',54073,20,NULL,NULL,1,'N','N','N','N','Y',NULL,'N',0,'N','N',NULL,NULL,NULL,NULL,'N',300012,NULL,'N','N',NULL,NULL,NULL,'N','Y',NULL,NULL,'Y','N','N',NULL,'N',NULL);

INSERT INTO ad_field (ad_field_id,ad_client_id,ad_org_id,isactive,created,createdby,updated,updatedby,name,description,help,iscentrallymaintained,ad_tab_id,ad_column_id,ad_fieldgroup_id,isdisplayed,displaylogic,displaylength,isreadonly,seqno,sortno,issameline,isheading,isfieldonly,isencrypted,entitytype,obscuretype,ad_reference_id,ismandatory,included_tab_id,defaultvalue,ad_reference_value_id,ad_val_rule_id,infofactoryclass,isdisplayedgrid,seqnogrid,isallowcopy,isallownewattributeinstance,isquickentry,xposition,numlines,columnspan) 
VALUES
  (300046,0,0,'Y',TIMESTAMP '2023-03-17 12:56:39.000000',100,TIMESTAMP '2023-03-17 12:58:04.000000',100,'Always Enabled',NULL,NULL,'Y',54178,300067,NULL,'Y',NULL,1,'N',100,NULL,'N','N','N','N','Application Dictionary',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,'N',NULL,5,1,1);
  
UPDATE ad_column SET callout = 'org.adempiere.model.CalloutInfoWindow.reference' WHERE ad_column_id = 15791;
UPDATE ad_column SET callout = 'org.adempiere.model.CalloutInfoWindow.element' WHERE ad_column_id = 15790;


ALTER TABLE ampere.t_selection_infowindow ADD COLUMN viewid varchar(30);
ALTER INDEX ampere.t_selection_infowindow_key RENAME TO pk_t_selection_infowindow;
