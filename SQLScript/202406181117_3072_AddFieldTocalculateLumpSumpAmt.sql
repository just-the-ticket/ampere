-- Create Element of Calculate Adjustment Amt

INSERT INTO ad_element
(ad_element_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, columnname, entitytype, "name", printname, description, help, po_name, po_printname, po_description, po_help, ad_reference_id, fieldlength, ad_reference_value_id)
VALUES(400077, 0, 0, 'Y', '2024-06-17 11:45:34.000', 100, '2024-06-17 18:29:17.000', 100, 'CalculateAdjustmentAmt', 'U', 'Calculate Adjustment Amt', 'Calculate Adjustment Amount', NULL, NULL, NULL, NULL, NULL, NULL, 28, 0, NULL);


-- Insert calcuateAdjAmt button in the column 

INSERT INTO ad_column
(ad_column_id, ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, "name", description, help, "version", entitytype, columnname, ad_table_id, ad_reference_id, ad_reference_value_id, ad_val_rule_id, fieldlength, defaultvalue, iskey, isparent, ismandatory, isupdateable, readonlylogic, isidentifier, seqno, istranslated, isencrypted, callout, vformat, valuemin, valuemax, isselectioncolumn, ad_element_id, ad_process_id, issyncdatabase, isalwaysupdateable, columnsql, mandatorylogic, infofactoryclass, isautocomplete, isallowlogging, formatpattern, ad_chart_id, isallowcopy, issecure, istoolbarbutton, ad_infowindow_id, isincludeints, next_column_id)
VALUES(400242, 0, 0, 'Y', '2024-06-17 11:47:07.000', '2024-06-17 14:13:36.000', 100, 100, 'Calculate Adjustment Amt', NULL, NULL, 0, 'Quote-to-Invoice', 'CalculateAdjustmentAmt', 260, 28, NULL, NULL, 1, NULL, 'N', 'N', 'N', 'Y', NULL, 'N', 0, 'N', 'N', NULL, NULL, NULL, NULL, 'N', 400077, 400022, 'N', 'N', NULL, NULL, NULL, 'N', 'Y', NULL, NULL, 'Y', 'N', 'N', NULL, 'N', NULL);


-- Insert calcuateAdjAmt button in the field 

INSERT INTO ad_field
(ad_field_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "name", description, help, iscentrallymaintained, ad_tab_id, ad_column_id, ad_fieldgroup_id, isdisplayed, displaylogic, displaylength, isreadonly, seqno, sortno, issameline, isheading, isfieldonly, isencrypted, entitytype, obscuretype, ad_reference_id, ismandatory, included_tab_id, defaultvalue, ad_reference_value_id, ad_val_rule_id, infofactoryclass, isdisplayedgrid, seqnogrid, isallowcopy, isallownewattributeinstance, isquickentry, xposition, numlines, columnspan)
VALUES(400520, 0, 0, 'Y', '2024-06-17 12:19:01.000', 100, '2024-06-17 12:24:28.000', 100, 'Calculate Adjustment Amt', NULL, NULL, 'Y', 400007, 400242, NULL, 'Y', NULL, NULL, 'N', 266, 0, 'N', 'N', 'N', 'N', 'U', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 330, NULL, 'N', 'N', 5, 1, 2);

--Add process to calculate lumpsump amount

INSERT INTO ad_process
(ad_process_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, value, "name", description, help, accesslevel, entitytype, procedurename, isreport, isdirectprint, ad_reportview_id, classname, statistic_count, statistic_seconds, ad_printformat_id, workflowvalue, ad_workflow_id, isbetafunctionality, isserverprocess, showhelp, jasperreport, ad_form_id, copyfromprocess, ad_tableselection_id, ad_browse_id)
VALUES(400022, 0, 0, 'Y', '2024-06-17 12:39:43.000', 100, '2024-06-17 18:27:15.000', 100, 'CreateAdjustmentLine', 'Create AdjustmentLine', NULL, NULL, '3', 'U', NULL, 'N', 'N', NULL, 'com.aat.process.CreateDetailLineForAdjustmentAmt', 0, 0, NULL, NULL, NULL, 'N', 'N', 'Y', NULL, NULL, 'N', NULL, NULL);


ALTER TABLE C_OrderLine ADD CalculateAdjustmentAmt CHAR(1) DEFAULT NULL;

