--Add Element of PrependPDFFile
INSERT INTO ad_element
(ad_element_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, columnname, entitytype, "name", printname, description, help, po_name, po_printname, po_description, po_help, ad_reference_id, fieldlength, ad_reference_value_id)
VALUES(300080, 0, 0, 'Y', '2024-08-08 14:36:35.000', 100, '2024-08-08 14:58:35.000', 100, 'PrependPDFFile', 'JTT', 'PrependPDF File', 'PrependPDF File', NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, NULL);

--Add Element of AppendPDFFile
INSERT INTO ad_element
(ad_element_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, columnname, entitytype, "name", printname, description, help, po_name, po_printname, po_description, po_help, ad_reference_id, fieldlength, ad_reference_value_id)
VALUES(300081, 0, 0, 'Y', '2024-08-08 14:36:59.000', 100, '2024-08-08 14:59:06.000', 100, 'AppendPDFFile', 'JTT', 'AppendPDF File', 'AppendPDF File', NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, NULL);

--Add Column of PrependPDFFile
INSERT INTO ad_column
(ad_column_id, ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, "name", description, help, "version", entitytype, columnname, ad_table_id, ad_reference_id, ad_reference_value_id, ad_val_rule_id, fieldlength, defaultvalue, iskey, isparent, ismandatory, isupdateable, readonlylogic, isidentifier, seqno, istranslated, isencrypted, callout, vformat, valuemin, valuemax, isselectioncolumn, ad_element_id, ad_process_id, issyncdatabase, isalwaysupdateable, columnsql, mandatorylogic, infofactoryclass, isautocomplete, isallowlogging, formatpattern, ad_chart_id, isallowcopy, issecure, istoolbarbutton, ad_infowindow_id, isincludeints, next_column_id)
VALUES(300246, 0, 0, 'Y', '2024-08-08 14:42:47.000', '2024-08-08 15:21:01.000', 100, 100, 'PrependPDF File', NULL, NULL, 0, 'JTT', 'PrependPDFFile', 493, 10, NULL, NULL, 125, NULL, 'N', 'N', 'N', 'Y', NULL, 'N', 0, 'N', 'N', NULL, NULL, NULL, NULL, 'N', 300080, NULL, 'N', 'N', NULL, NULL, NULL, 'N', 'Y', NULL, NULL, 'Y', 'N', 'N', NULL, 'N', NULL);


-- sysnchronise the PrependPDFFile
ALTER TABLE AD_PrintFormat ADD prependpdffile TEXT DEFAULT NULL ;

--Add Column of AppendPDFFile
INSERT INTO ad_column
(ad_column_id, ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, "name", description, help, "version", entitytype, columnname, ad_table_id, ad_reference_id, ad_reference_value_id, ad_val_rule_id, fieldlength, defaultvalue, iskey, isparent, ismandatory, isupdateable, readonlylogic, isidentifier, seqno, istranslated, isencrypted, callout, vformat, valuemin, valuemax, isselectioncolumn, ad_element_id, ad_process_id, issyncdatabase, isalwaysupdateable, columnsql, mandatorylogic, infofactoryclass, isautocomplete, isallowlogging, formatpattern, ad_chart_id, isallowcopy, issecure, istoolbarbutton, ad_infowindow_id, isincludeints, next_column_id)
VALUES(300245, 0, 0, 'Y', '2024-08-08 14:42:10.000', '2024-08-08 15:15:38.000', 100, 100, 'AppendPDF File', NULL, NULL, 0, 'JTT', 'AppendPDFFile', 493, 10, NULL, NULL, 125, NULL, 'N', 'N', 'N', 'Y', NULL, 'N', 0, 'N', 'N', NULL, NULL, NULL, NULL, 'N', 300081, NULL, 'N', 'N', NULL, NULL, NULL, 'N', 'Y', NULL, NULL, 'Y', 'N', 'N', NULL, 'N', NULL);

-- sysnchronise the AppendPDFFile
ALTER TABLE AD_PrintFormat ADD appendpdffile TEXT DEFAULT NULL ;

-- Add field of PrependPDFFile
INSERT INTO ad_field
(ad_field_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "name", description, help, iscentrallymaintained, ad_tab_id, ad_column_id, ad_fieldgroup_id, isdisplayed, displaylogic, displaylength, isreadonly, seqno, sortno, issameline, isheading, isfieldonly, isencrypted, entitytype, obscuretype, ad_reference_id, ismandatory, included_tab_id, defaultvalue, ad_reference_value_id, ad_val_rule_id, infofactoryclass, isdisplayedgrid, seqnogrid, isallowcopy, isallownewattributeinstance, isquickentry, xposition, numlines, columnspan)
VALUES(300525, 0, 0, 'Y', '2024-08-08 14:43:15.000', 100, '2024-08-08 17:20:34.000', 100, 'PrependPDF File', NULL, NULL, 'Y', 425, 300246, NULL, 'Y', NULL, 125, 'N', 240, NULL, 'N', 'N', 'N', 'N', 'System Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, 'N', NULL, 1, 1, 2);


-- Add field of AppendPDFFile
INSERT INTO ad_field
(ad_field_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "name", description, help, iscentrallymaintained, ad_tab_id, ad_column_id, ad_fieldgroup_id, isdisplayed, displaylogic, displaylength, isreadonly, seqno, sortno, issameline, isheading, isfieldonly, isencrypted, entitytype, obscuretype, ad_reference_id, ismandatory, included_tab_id, defaultvalue, ad_reference_value_id, ad_val_rule_id, infofactoryclass, isdisplayedgrid, seqnogrid, isallowcopy, isallownewattributeinstance, isquickentry, xposition, numlines, columnspan)
VALUES(300526, 0, 0, 'Y', '2024-08-08 14:44:07.000', 100, '2024-08-08 17:20:43.000', 100, 'AppendPDF File', NULL, NULL, 'Y', 425, 300245, NULL, 'Y', NULL, NULL, 'N', 250, 0, 'N', 'N', 'N', 'N', 'U', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 240, NULL, 'N', 'N', 4, 1, 2);

