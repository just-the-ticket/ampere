-- Add Message for charge not assigned in the product Category 
INSERT INTO ad_message
(ad_message_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, value, msgtext, msgtip, msgtype, entitytype)
VALUES(523860, 0, 0, 'Y', '2024-07-17 17:20:27.000', 100, '2024-07-17 18:32:43.000', 100, 'PCatChargeNotAssigned', 'No Charge Configured For The Product Category : {0}', NULL, 'I', 'U');



-- Add callout in the product column in the inventory line table 
UPDATE ad_column
SET ad_client_id=0, ad_org_id=0, isactive='Y', created='1999-12-19 20:39:43.000', updated='2024-07-17 18:27:09.000', createdby=0, updatedby=100, "name"='Product', description='Product, Service, Item', help='Identifies an item which is either purchased or sold in this organization.', "version"=1, entitytype='Material Management', columnname='M_Product_ID', ad_table_id=322, ad_reference_id=30, ad_reference_value_id=171, ad_val_rule_id=NULL, fieldlength=22, defaultvalue=NULL, iskey='N', isparent='N', ismandatory='Y', isupdateable='Y', readonlylogic=NULL, isidentifier='N', seqno=NULL, istranslated='N', isencrypted='N', callout='org.compiere.model.CalloutInventory.product,com.aat.callout.CalloutInventoryLine.charge', vformat=NULL, valuemin=NULL, valuemax=NULL, isselectioncolumn='N', ad_element_id=454, ad_process_id=NULL, issyncdatabase='N', isalwaysupdateable='N', columnsql=NULL, mandatorylogic=NULL, infofactoryclass=NULL, isautocomplete='N', isallowlogging='Y', formatpattern=NULL, ad_chart_id=NULL, isallowcopy='Y', issecure='N', istoolbarbutton='N', ad_infowindow_id=NULL, isincludeints='N', next_column_id=NULL
WHERE ad_column_id=3565;


-- Create element for the isworkshopissue column
INSERT INTO ad_element
(ad_element_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, columnname, entitytype, "name", printname, description, help, po_name, po_printname, po_description, po_help, ad_reference_id, fieldlength, ad_reference_value_id)
VALUES(400079, 0, 0, 'Y', '2024-07-17 14:27:20.000', 100, '2024-07-17 14:56:22.000', 100, 'IsWorkshopIssue', 'JTT', 'Workshop Issue', 'Workshop Issue', NULL, NULL, NULL, NULL, NULL, NULL, 20, 0, NULL);


-- Create isworkshopissue column in the doctype 
INSERT INTO ad_column
(ad_column_id, ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, "name", description, help, "version", entitytype, columnname, ad_table_id, ad_reference_id, ad_reference_value_id, ad_val_rule_id, fieldlength, defaultvalue, iskey, isparent, ismandatory, isupdateable, readonlylogic, isidentifier, seqno, istranslated, isencrypted, callout, vformat, valuemin, valuemax, isselectioncolumn, ad_element_id, ad_process_id, issyncdatabase, isalwaysupdateable, columnsql, mandatorylogic, infofactoryclass, isautocomplete, isallowlogging, formatpattern, ad_chart_id, isallowcopy, issecure, istoolbarbutton, ad_infowindow_id, isincludeints, next_column_id)
VALUES(400244, 0, 0, 'Y', '2024-07-17 14:28:01.000', '2024-07-17 14:28:01.000', 100, 100, 'Workshop Issue', NULL, NULL, 0, 'Accounting Rules', 'IsWorkshopIssue', 217, 20, NULL, NULL, 10, NULL, 'N', 'N', 'N', 'Y', NULL, 'N', 0, 'N', 'N', NULL, NULL, NULL, NULL, 'N', 400079, NULL, 'N', 'N', NULL, NULL, NULL, 'N', 'Y', NULL, NULL, 'Y', 'N', 'N', NULL, 'N', NULL);


-- Create isworkshopissue field in the doctype window
INSERT INTO ad_field
(ad_field_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "name", description, help, iscentrallymaintained, ad_tab_id, ad_column_id, ad_fieldgroup_id, isdisplayed, displaylogic, displaylength, isreadonly, seqno, sortno, issameline, isheading, isfieldonly, isencrypted, entitytype, obscuretype, ad_reference_id, ismandatory, included_tab_id, defaultvalue, ad_reference_value_id, ad_val_rule_id, infofactoryclass, isdisplayedgrid, seqnogrid, isallowcopy, isallownewattributeinstance, isquickentry, xposition, numlines, columnspan)
VALUES(400522, 0, 0, 'Y', '2024-07-17 14:30:20.000', 100, '2024-07-17 14:57:06.000', 100, 'Workshop Issue', NULL, NULL, 'Y', 167, 400244, NULL, 'Y', '@DocBaseType@=''MMI''', NULL, 'N', 440, 0, 'N', 'N', 'N', 'N', 'U', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 440, NULL, 'N', 'N', 2, 1, 1);

