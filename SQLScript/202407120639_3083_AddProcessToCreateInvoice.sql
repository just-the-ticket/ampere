
-- Add New process to create Invoice From the Selected order
INSERT INTO ad_process
(ad_process_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, value, "name", description, help, accesslevel, entitytype, procedurename, isreport, isdirectprint, ad_reportview_id, classname, statistic_count, statistic_seconds, ad_printformat_id, workflowvalue, ad_workflow_id, isbetafunctionality, isserverprocess, showhelp, jasperreport, ad_form_id, copyfromprocess, ad_tableselection_id, ad_browse_id)
VALUES(400027, 0, 0, 'Y', '2024-07-12 16:05:40.000', 100, '2024-07-12 16:05:40.000', 100, 'CreateInvoiceFromInfoWindow', 'Generate Invoice', NULL, NULL, '3', 'U', NULL, 'N', 'N', NULL, 'com.aat.process.CreateInvFromInfoWindow', 0, 0, NULL, NULL, NULL, 'N', 'N', 'Y', NULL, NULL, 'N', NULL, NULL);

-- Add Process to create Invoice From the Selected order in the Create Trip Invoice 
INSERT INTO ad_infoprocess
(ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, ad_infoprocess_id, ad_infowindow_id, ad_process_id, seqno, displaylogic, ad_infocolumn_id, imageurl, layouttype, ad_table_id, isreport, "name", whereclause, isalwaysenabled)
VALUES(0, 0, 'Y', '2024-07-12 16:08:13.000', '2024-07-12 16:08:13.000', 100, 100, 400012, 1000000, 400027, 10, NULL, NULL, 'ellipsis-v', 'B', NULL, 'N', 'Invoice Create', NULL, 'N');
