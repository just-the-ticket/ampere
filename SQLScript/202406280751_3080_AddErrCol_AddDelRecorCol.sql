-- Insert Delete old imported records Column in the importKMReading process
INSERT INTO ad_process_para
(ad_process_para_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "name", description, help, ad_process_id, seqno, ad_reference_id, ad_reference_value_id, ad_val_rule_id, columnname, iscentrallymaintained, fieldlength, ismandatory, isrange, defaultvalue, defaultvalue2, vformat, valuemin, valuemax, ad_element_id, entitytype, readonlylogic, displaylogic)
VALUES(400008, 0, 0, 'Y', '2024-06-28 18:32:18.000', 100, '2024-06-28 18:32:18.000', 100, 'Delete old imported records', 'Before processing delete old imported records in the import table', NULL, 1000000, 10, 20, NULL, NULL, 'DeleteOldImported', 'Y', 0, 'N', 'N', NULL, NULL, NULL, NULL, NULL, 1922, 'JTT', NULL, NULL);

-- Insert Import Error Message Column in the I_VMVehReading table
INSERT INTO ad_column
(ad_column_id, ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, "name", description, help, "version", entitytype, columnname, ad_table_id, ad_reference_id, ad_reference_value_id, ad_val_rule_id, fieldlength, defaultvalue, iskey, isparent, ismandatory, isupdateable, readonlylogic, isidentifier, seqno, istranslated, isencrypted, callout, vformat, valuemin, valuemax, isselectioncolumn, ad_element_id, ad_process_id, issyncdatabase, isalwaysupdateable, columnsql, mandatorylogic, infofactoryclass, isautocomplete, isallowlogging, formatpattern, ad_chart_id, isallowcopy, issecure, istoolbarbutton, ad_infowindow_id, isincludeints, next_column_id)
VALUES(400237, 0, 0, 'Y', '2024-06-28 18:26:47.000', '2024-06-28 18:26:47.000', 100, 100, 'Import Error Message', 'Messages generated from import process', 'The Import Error Message displays any error messages generated during the import process.', 0, 'JTT', 'I_ErrorMsg', 1000004, 10, NULL, NULL, 2000, NULL, 'N', 'N', 'N', 'Y', NULL, 'N', 0, 'N', 'N', NULL, NULL, NULL, NULL, 'N', 912, NULL, 'N', 'N', NULL, NULL, NULL, 'N', 'Y', NULL, NULL, 'Y', 'N', 'N', NULL, 'N', NULL);


-- To Set Error to show the Error in the particular Record

ALTER TABLE i_vmvehreading
DROP CONSTRAINT i_vmvehreading_i_isimported_check,
ADD CONSTRAINT i_vmvehreading_i_isimported_check 
CHECK (i_isimported = ANY (ARRAY['Y'::bpchar, 'N'::bpchar, 'E'::bpchar]));

-- Update the Entity Type JTT Related Mods
UPDATE AD_EntityType SET ModelPackage = 'com.aat.model' WHERE AD_EntityType_ID = 1000001;

