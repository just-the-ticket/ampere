-- Update the Resource Type Column set readonly flag false to get the value in the code side in the info window
UPDATE ad_infocolumn
SET ad_client_id=0, ad_org_id=0, isactive='Y', created='2024-07-03 09:34:25.000', createdby=100, updated='2024-07-15 15:41:40.000', updatedby=100, "name"='Resource Type', description=NULL, help=NULL, ad_infowindow_id=1000001, entitytype='U', selectclause='a.S_ResourceType_ID', seqno=70, isdisplayed='Y', isquerycriteria='Y', ad_element_id=1779, ad_reference_id=19, iscentrallymaintained='Y', columnname='S_ResourceType_ID', defaultvalue=NULL, displaylogic=NULL, ad_val_rule_id=NULL, isidentifier='N', ismandatory='N', queryfunction=NULL, queryoperator='=', isreadonly='N', ad_reference_value_id=NULL, seqnoselection=0, isparent='N', istreecolumn='N', istreeroot='N', isvariableonly='N', whereclause=NULL
WHERE ad_infocolumn_id=1000031;


-- Update the Resource Column set readonly flag false to get the value in the code side in the info window
UPDATE ad_infocolumn
SET ad_client_id=0, ad_org_id=0, isactive='Y', created='2024-07-03 09:42:09.000', createdby=100, updated='2024-07-15 15:45:52.000', updatedby=100, "name"='Resource', description='Resource', help=NULL, ad_infowindow_id=1000001, entitytype='U', selectclause='a.S_Resource_ID', seqno=80, isdisplayed='Y', isquerycriteria='N', ad_element_id=1777, ad_reference_id=19, iscentrallymaintained='Y', columnname='S_Resource_ID', defaultvalue=NULL, displaylogic=NULL, ad_val_rule_id=NULL, isidentifier='N', ismandatory='N', queryfunction=NULL, queryoperator='=', isreadonly='N', ad_reference_value_id=NULL, seqnoselection=0, isparent='N', istreecolumn='N', istreeroot='N', isvariableonly='N', whereclause=NULL
WHERE ad_infocolumn_id=1000032;


-- Create new Process to print the orderlines
INSERT INTO ad_process
(ad_process_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, value, "name", description, help, accesslevel, entitytype, procedurename, isreport, isdirectprint, ad_reportview_id, classname, statistic_count, statistic_seconds, ad_printformat_id, workflowvalue, ad_workflow_id, isbetafunctionality, isserverprocess, showhelp, jasperreport, ad_form_id, copyfromprocess, ad_tableselection_id, ad_browse_id)
VALUES(400027, 0, 0, 'Y', '2024-07-12 16:05:40.000', 100, '2024-07-12 16:05:40.000', 100, 'CreateInvoiceFromInfoWindow', 'Generate Invoice', NULL, NULL, '3', 'U', NULL, 'N', 'N', NULL, 'com.aat.process.CreateInvFromInfoWindow', 0, 0, NULL, NULL, NULL, 'N', 'N', 'Y', NULL, NULL, 'N', NULL, NULL);


-- Add Process in the Print Waybill by Trip info window
INSERT INTO ad_infoprocess
(ad_client_id, ad_org_id, isactive, created, updated, createdby, updatedby, ad_infoprocess_id, ad_infowindow_id, ad_process_id, seqno, displaylogic, ad_infocolumn_id, imageurl, layouttype, ad_table_id, isreport, "name", whereclause, isalwaysenabled)
VALUES(0, 0, 'Y', '2024-07-11 10:07:10.000', '2024-07-11 10:10:10.000', 100, 100, 400011, 1000001, 400026, 10, NULL, NULL, 'ellipsis-v', 'B', NULL, 'N', NULL, NULL, 'N');


-- Create Report view to add whereclause in the Print Format
INSERT INTO ad_reportview
(ad_reportview_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "name", description, ad_table_id, whereclause, orderbyclause, entitytype)
VALUES(400001, 0, 0, 'Y', '2024-07-12 10:38:07.000', 100, '2024-07-16 10:39:40.000', 100, 'Print_OrderLines', NULL, 497, 'C_OrderLine_ID IN (@PrintOrderLines@)', NULL, 'U');


-- Set Reort view ID in the In the Order_LineTax Waybill Mockup Print Format
UPDATE ad_printformat
SET ad_client_id=1000001, ad_org_id=0, isactive='Y', created='2024-07-09 09:25:52.000', createdby=100, updated='2024-07-12 10:41:03.000', updatedby=100, "name"='Order_LineTax Waybill Mockup', description='Standard Order Line&Tax', istablebased='N', isform='N', ad_table_id=497, ad_printpaper_id=103, ad_printcolor_id=100, ad_printfont_id=1000105, isstandardheaderfooter='Y', headermargin=0, footermargin=0, createcopy=NULL, ad_reportview_id=400001, ad_printtableformat_id=101, printername=NULL, isdefault='N', jasperprocess_id=NULL, classname=NULL, args=NULL, isprintparameters='Y', isnewpageafterrh='N', r_mailtext_id=NULL
WHERE ad_printformat_id=1000478;
