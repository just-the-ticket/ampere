package com.aat.form;

import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;

import org.adempiere.webui.apps.form.WMultidateSelector;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Trx;

public class WCreateChildQuote extends WMultidateSelector {

	public WCreateChildQuote() {
		super();
//		setMessagePrompt("ConfirmChildQuote");
	}

	/**
	 * response is answer to
	 * Do you wish to confirm created child quotes Y/N?
	 */
	
	@Override
	public void onProcess(boolean response) throws Exception
	{
		List<Timestamp> dates = getSelectedDates();
		
		Properties ctx = Env.getCtx();
		String trxName = Trx.createTrxName();
		
		MOrder parent = new MOrder(ctx, getForm().getProcessInfo().getRecord_ID(), trxName);
		for (Timestamp date : dates) {
			MOrder child = MOrder.copyAllFrom(parent);
			updateRelativeDates(child, date);
			
			child.set_ValueOfColumn("parent_order_id", parent.get_ID());
//			if (response) {
			if (MOrder.DOCSTATUS_Completed.equals(parent.getDocStatus())) {
				child.setDocAction(MOrder.DOCACTION_Complete);
				child.setDocStatus(child.completeIt());
				if (parent.get_ValueAsString("TripStatus").equals("C")) {
					child.confirmQuote();
				}
			}
//			}
			child.save();
		}
		DB.commit(true, trxName);
	}
	
	private void updateRelativeDates(MOrder order, Timestamp newDate)
	{
		//use addMinutess as addDays truncate time
		int offset = TimeUtil.getDaysBetween((Timestamp) order.get_Value("datetripstart"), newDate) * 60 * 24;
		
		MOrderLine lines[] = order.getLines();
		
		for (MOrderLine line : lines) {
			line.setDateTripStart(TimeUtil.addMinutess(line.getDateTripStart(), offset));
			line.setDateTripEnd(TimeUtil.addMinutess(line.getDateTripEnd(), offset));
			line.save();
		}
		
		Timestamp startTrip = TimeUtil.addMinutess((Timestamp) order.get_Value("datetripstart"), offset);
		Timestamp endTrip = TimeUtil.addMinutess((Timestamp) order.get_Value("datetripend"), offset);
		order.set_ValueOfColumn("datetripstart", startTrip);
		order.set_ValueOfColumn("datetripend", endTrip);
	}
	
}
