package com.aat.form;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.CWindowToolbar;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.SimpleListModel;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.DialogForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.apache.commons.lang.StringUtils;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.I_C_OrderLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTabCustomization;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window.Mode;

import com.aat.model.MOrderLineDetail;


/**
 * 
 * @author jtrinidad
 *
 */

public class WCopyQuoteLeg 
	implements IFormController, EventListener<Event>, WTableModelListener
{

	public static final String ATT_QUOTE_LEVEL = "ATT_QUOTE_LEVEL";
	public static final String ATT_QUOTE_ID = "ATT_QUOTE_ID";
	
	public static final int QUOTE_LEVEL_ORDER_HEADER = 1;
	public static final int QUOTE_LEVEL_ORDER_LINE = 2;
	
	private static final String AVL_QUOTE_HEADER_STYLE = "font-weight: bolder;";
	private static final String AVL_QUOTE_LINE_STYLE = "";
	private static final String ROW_STYLE_START_OF_DAY= "background-color: #7D443E;";

	
	private static int WINDOW_COPYQUOTELEG = 90001;  // hard code id as reference to customisation
	
	protected CLogger log = CLogger.getCLogger(getClass());
	
	private DialogForm form = new DialogForm();
	
	
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Panel centerPanel = new Panel();
	private Panel southPanel = new Panel();
	private Grid centerLayout = GridFactory.newGridLayout();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true, true, true, true, false, false);
	
	private Label depotLabel = new Label();
	private Combobox depotPick = null;

	private Label bpLabel = new Label();
	private Combobox bpPick = null;

	private Label tokenSearchLabel = new Label();
	private Textbox textToken = new Textbox();
	
	private Integer warehouseID;
	public HashMap<String, Integer> warehouseMap = new HashMap<String, Integer>();
	public HashMap<String, Integer> bpMap = new HashMap<String, Integer>();

	
	private Tree leftSide = new Tree();
	private WListbox rightSide = new WListbox();
	
	//	UI variables
	private Label					noLabel						= new Label();
	private Label					yesLabel					= new Label();
	private Button					bStartOfDay					= new Button();
	private Button					bChooseAll					= new Button();
	private Button					bAdd						= new Button();
	private Button					bRemove						= new Button();
	private Button					bRemoveAll					= new Button();
	private Button					bTop						= new Button();
	private Button					bUp							= new Button();
	private Button					bDown						= new Button();
	private Button					bBottom						= new Button();
	SimpleListModel					yesModel					= new SimpleListModel();
//	SimpleListModel					noModel						= new SimpleListModel();
	Listbox							yesList						= new Listbox();
//	Listbox							noList						= new Listbox();
	private static int				i_OrderLine_ID				= 0;
	private static int				i_Line						= 1;
	private static int				i_Description				= 2;
	private static int				i_Day						= 3;
	private static int				i_Date						= 4;
	private static int				i_StartTime					= 5;
	private static int				i_EndTime					= 6;
	private static int				i_JourneyTime				= 7;
	private static int				i_IsFiller					= 8;
	
	private static String			FILLER_TRIP_START			= "***Trip Start";
	private static String			FILLER_TRIP_END				= "***Trip End";
	private static String			FILLER_NEXT_DAY				= "***Next Day";
	private static String			FILLER_FREE_TIME			= "***Free Time";
	Map<Integer, Treeitem>			mapOrderHeader				= null;
	Map<Integer, MOrderLine>		mapSelectedLine				= null;
	Map<Integer, MOrderLine>		mapCurrentLinesOfOrder		= null;
	public static final String		ON_AFTER_LOAD_FORM			= "onAfterLoadForm";

	
	//Order Line in linked list to maintain sequence
	private LinkedList<MOrderLine> llOrderLine = new LinkedList<MOrderLine>();
	private Timestamp curDateStart;
	private String trxName = null;
	private MOrder m_order = null;
	private MOrderLine activeOrderLine;
	private MOrderLine prevOrderLineLeg;
	
	private Keylistener keyListener;
	private int m_selOrderLineID;

	
	public WCopyQuoteLeg()
		
	{
		try {
			dynInit();
			zkInit();
			confirmPanel.addKeyEventListener(this, ConfirmPanel.ALT_KEYS + "@l");
		} catch (Exception e) {
			log.severe(e.getMessage());
			
		}
	}
	
	private void zkInit() throws Exception
	{
		ZKUpdateUtil.setWidth(form, "90%");
		ZKUpdateUtil.setHeight(form, "80%");

		form.appendChild(mainLayout);
		ZKUpdateUtil.setWidth(mainLayout, "100%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		
		depotLabel.setText(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		Button button = confirmPanel.createButton("list", CWindowToolbar.VK_L);
		button.setTooltiptext("View summary");
		confirmPanel.addButton(button);

		EventListener<Event> mouseListener = new EventListener<Event>()
		{
			public void onEvent(Event event) throws Exception
			{
				if (Events.ON_DOUBLE_CLICK.equals(event.getName()))
				{
					migrateValueAcrossLists(event);
				}
			}
		};

		leftSide.addEventListener(Events.ON_DOUBLE_CLICK, mouseListener);
		rightSide.addEventListener(Events.ON_CLICK, this);
		rightSide.addEventListener(Events.ON_SELECTION, this);

		EventListener<Event> actionListener = new EventListener<Event>()
		{
			public void onEvent(Event event) throws Exception {
				migrateValueAcrossLists(event);
			}
		};

		bChooseAll.setIconSclass("z-icon-MoveRight2");
		bChooseAll.addEventListener(Events.ON_CLICK, actionListener);
		bChooseAll.setEnabled(false);

		bAdd.setIconSclass("z-icon-MoveRight");
		bAdd.addEventListener(Events.ON_CLICK, actionListener);

		bRemove.setIconSclass("z-icon-MoveLeft");
		bRemove.addEventListener(Events.ON_CLICK, actionListener);

		bRemoveAll.setIconSclass("z-icon-MoveLeft2");
		bRemoveAll.addEventListener(Events.ON_CLICK, actionListener);


		actionListener = new EventListener<Event>()
		{
			public void onEvent(Event event) throws Exception {
				onEditTime();
			}
		};		
		
		rightSide.addEventListener(Events.ON_DOUBLE_CLICK, actionListener);

		actionListener = new EventListener<Event>()
		{
			public void onEvent(Event event) throws Exception {
				//onNewGap();
			}
		};

		bStartOfDay.setIconSclass("z-icon-sun-o");
		bStartOfDay.setTooltiptext("Add a start of day to move to the following day");

		bStartOfDay.addEventListener(Events.ON_CLICK, actionListener);

		 actionListener = new EventListener<Event>() {
			    public void onEvent(Event event) throws Exception {
//			    	migrateValueWithinYesList(event);
			    	moveSelectedItem(event);
			    }
		};
		bTop.setIconSclass("z-icon-MoveUp2");
		bTop.addEventListener(Events.ON_CLICK, actionListener);

		bUp.setIconSclass("z-icon-MoveUp");
		bUp.addEventListener(Events.ON_CLICK, actionListener);

		bDown.setIconSclass("z-icon-MoveDown");
		bDown.addEventListener(Events.ON_CLICK, actionListener);

		bBottom.setIconSclass("z-icon-MoveDown2");
		bBottom.addEventListener(Events.ON_CLICK, actionListener);

		Columns columns = new Columns();
		parameterLayout.appendChild(columns);
		for (int i = 0; i < 6; i++)
			columns.appendChild(new Column());

		parameterPanel.appendChild(parameterLayout);
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		Rows rows = null;
		Row row = null;
		ZKUpdateUtil.setWidth(parameterLayout, "100%");
		rows = parameterLayout.newRows();
		row = rows.newRow();

		tokenSearchLabel.setValue("Key Word");
		row.appendChild(depotLabel.rightAlign());
		row.appendCellChild(depotPick, 2);
		depotPick.setWidth("75%");
		row.appendChild(tokenSearchLabel.rightAlign());
		row.appendCellChild(textToken, 2);
		textToken.setWidth("75%");

		row = rows.newRow();
		bpLabel.setValue("Seed BP");
		row.appendChild(bpLabel.rightAlign());
		row.appendCellChild(bpPick, 2);
		bpPick.setWidth("75%");

		
		centerPanel.appendChild(centerLayout);
		ZKUpdateUtil.setWidth(centerLayout, "100%");

		Center center = new Center();
		mainLayout.appendChild(center);
		center.setStyle("border: none");
		center.appendChild(centerPanel);

		ZKUpdateUtil.setVflex(center, "flex");

		Treecols cols = new Treecols();
		Treecol col = null;
		col = new Treecol("Description");
		col.setHflex("2");
		cols.appendChild(col);
		col = new Treecol("Time");
		col.setHflex("1");
		cols.appendChild(col);
		leftSide.appendChild(cols);
		Hbox hbox = new Hbox();

//		ZKUpdateUtil.setVflex(hbox, "true");
		ZKUpdateUtil.setHeight(hbox, "650px");
		ZKUpdateUtil.setHflex(hbox, "false");

		Space space = new Space();
		ZKUpdateUtil.setHeight(space, "10px");

		ZKUpdateUtil.setHeight(leftSide, "640px");
		ZKUpdateUtil.setHeight(rightSide, "640px");

		hbox.appendChild(leftSide);
		Vbox vbox = new Vbox();
//		vbox.appendChild(bChooseAll);
//		vbox.appendChild((Component) space.clone());
		vbox.appendChild(bAdd);
		vbox.appendChild((Component) space.clone());
		vbox.appendChild(bRemove);
		vbox.appendChild((Component) space.clone());
//		vbox.appendChild(bRemoveAll);
//		vbox.appendChild((Component) space.clone());

//		vbox.appendChild((Component) space.clone());
//		vbox.appendChild((Component) space.clone());
//		vbox.appendChild(bStartOfDay);
		ZKUpdateUtil.setWidth(vbox, "50px");
		hbox.appendChild(vbox);

		hbox.appendChild(rightSide);
		rightSide.setStyle("overflow: auto;"); 
		
		vbox = new Vbox();
		vbox.appendChild(bTop);
		vbox.appendChild((Component) space.clone());
		vbox.appendChild(bUp);
		vbox.appendChild((Component) space.clone());
		vbox.appendChild(bDown);
		vbox.appendChild((Component) space.clone());
		vbox.appendChild(bBottom);
		ZKUpdateUtil.setWidth(vbox, "50px");
//		ZKUpdateUtil.setVflex(vbox, "true");
		ZKUpdateUtil.setHeight(vbox, "500px");

		hbox.appendChild(vbox);
		hbox.setParent(centerPanel);

		South south = new South();
		south.appendChild(southPanel);
		southPanel.appendChild(confirmPanel);
		ZKUpdateUtil.setWidth(southPanel, "100%");
		mainLayout.appendChild(south);
		confirmPanel.addActionListener(this);
		rightSide.setMultiple(false);

		if (keyListener != null)
		{
			keyListener.setCtrlKeys(keyListener.getCtrlKeys().replaceAll("#del#enter", ""));
			keyListener.removeEventListener(Events.ON_CTRL_KEY, this);
		}

    	keyListener = SessionManager.getSessionApplication().getKeylistener();
    	keyListener.setCtrlKeys(keyListener.getCtrlKeys() + "#del#enter");
    	keyListener.addEventListener(Events.ON_CTRL_KEY, this);
    	keyListener.setAutoBlur(false);

    	confirmPanel.getButton(ConfirmPanel.A_REFRESH).setTooltiptext("Refresh"+  "	Alt+R" );
    	confirmPanel.getButton(ConfirmPanel.A_RESET).setTooltiptext("Reset"+  "	Alt+U" );
    	confirmPanel.getButton(ConfirmPanel.A_OK).setTooltiptext("Ok"+  "	Alt+K" );
    	confirmPanel.getButton(ConfirmPanel.A_CANCEL).setTooltiptext("Cancel"+  "	Alt+C" );

    	
//		depotPick.addEventListener(Events.ON_SELECT, this);
		form.addEventListener(ON_AFTER_LOAD_FORM, this);
		Events.postEvent(ON_AFTER_LOAD_FORM, form, this);
	}	
	
	private void dynInit() throws Exception
	{
		mapSelectedLine = new HashMap<Integer, MOrderLine>();
		fillDepotPick();
		fillBPPick();
		setupRightSideListbox();
		
	}
	
	/**
	 * 
	 */
	private void fillDepotPick() {
		String sql = "SELECT M_Warehouse_ID, Name FROM M_Warehouse";
		sql = MRole.getDefault().addAccessSQL(sql, "M_Warehouse", MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
		depotPick = new Combobox();
//		depotPick.setWidth("300px");
		
		PreparedStatement pstmt1 = DB.prepareStatement(sql, null);
		ResultSet rs1 = null;

		String depotName = null;
		Integer warehouseID = null;

		try 
		{
			rs1 = pstmt1.executeQuery();
			while (rs1.next()) 
			{
				warehouseID = rs1.getInt(1);
				depotName = new String(rs1.getString(2));
				depotPick.appendItem(depotName, warehouseID);
				warehouseMap.put(depotName, warehouseID);
			}
		} 
		catch (SQLException e) 
		{
			System.out.println(e);
		} 
		finally 
		{
			DB.close(rs1);
			DB.close(pstmt1);
		}
	}
	
	private void fillBPPick() {
		String sql = "SELECT C_BPartner_ID, Name FROM C_BPartner";
		sql += " WHERE c_bp_group_id=1000003";
		sql = MRole.getDefault().addAccessSQL(sql, "C_BPartner", MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
		bpPick = new Combobox();
//		depotPick.setWidth("300px");
		
		PreparedStatement pstmt1 = DB.prepareStatement(sql, null);
		ResultSet rs1 = null;

		String bpName = null;
		Integer bpID = null;

		try 
		{
			rs1 = pstmt1.executeQuery();
			while (rs1.next()) 
			{
				bpID = rs1.getInt(1);
				bpName = new String(rs1.getString(2));
				bpPick.appendItem(bpName, bpID);
				bpMap.put(bpName, bpID);
			}
		} 
		catch (SQLException e) 
		{
			System.out.println(e);
		} 
		finally 
		{
			DB.close(rs1);
			DB.close(pstmt1);
		}
	}
	private void setupRightSideListbox()
	{
		ColumnInfo[] layout = new ColumnInfo[] { new ColumnInfo(" ", ".", IDColumn.class, false, false, ""),
				new ColumnInfo("Line", ".", Integer.class), // 1
				new ColumnInfo("Description", ".", String.class), // 2
				new ColumnInfo("Day", ".", Integer.class), // 3
				new ColumnInfo("Date", ".", Timestamp.class),//4
				new ColumnInfo("Start", ".", String.class), // 4
				new ColumnInfo("End", ".", String.class), // 5
				new ColumnInfo("Time", ".", BigDecimal.class), // 6
				new ColumnInfo("Is Filler", ".", Boolean.class) // 7
		};

		rightSide.prepareTable(layout, "", "", true, "");

		// Init with Start of day
//		addOrderLineToSelection(0);
		rightSide.setRowCount(1); // just to initiate table
		initTableColumnWidth();
	}
	
	/**
	 * Display a dialog form to list all selected events
	 * This makes it convenient to display everything
	 * if the events are spread out through multiple
	 * months
	 */
	public void onSummary()
	{
//		List<Timestamp> list = 
//		Collections.sort(selectedDates);
//		EventSummaryDialog.show(selectedDates);
	}
	
	public void onProcess() throws Exception
	{
		
		m_order.save();
		for (MOrderLine ol : llOrderLine) {
			if (ol == null) continue;
			
			ol.save();
		}
		
		DB.commit(true, trxName);

	}
	
	
	private void onEditTime()
	{
		Listitem item = rightSide.getSelectedItem();
		if ((boolean) rightSide.getValueAt(item.getIndex(), i_IsFiller))
			return;
		
		IDColumn selItem = (IDColumn) rightSide.getValueAt(item.getIndex(), i_OrderLine_ID);
		int lineID = selItem.getRecord_ID();
		
		int idxOl = 0;
		activeOrderLine = null;
		prevOrderLineLeg = null;
		for (MOrderLine l : llOrderLine) {
			if (l == null) 	continue;

			if (l.getC_OrderLine_ID() == lineID) {
				activeOrderLine = l;
				break;
			}
			if (l.getJourneyTime().signum() > 0) {
				//a leg
				prevOrderLineLeg = l;
			}
			idxOl++;
		}
		
		WSetStartTimeWindow stW = new WSetStartTimeWindow();
		Timestamp earliest = null;
		if (prevOrderLineLeg != null) {
			earliest = prevOrderLineLeg.getDateTripEnd();
		}
		stW.setStartDate(activeOrderLine.getDateTripStart(), earliest, activeOrderLine.getJourneyTime());
		
		stW.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		stW.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (!stW.isCancelled()) {
					BigDecimal jt = activeOrderLine.getJourneyTime();
					Timestamp dtStart = stW.getNewStartTime();
					activeOrderLine.setDateTripStart(dtStart);
					int min = jt.multiply(new BigDecimal(60)).intValue();
					Timestamp dtEnd = TimeUtil.addMinutess(dtStart, min);
					activeOrderLine.setDateTripEnd(dtEnd);
					//start of trip
					if (prevOrderLineLeg == null) {
						m_order.setDateTripStart(dtStart);
					}
					reloadOrderLine();
				}
			}
		});
		SessionManager.getAppDesktop().showWindow(stW);
		
	}

	private void onRefresh()
	{
		leftSide.clear();
		mapOrderHeader = new HashMap<Integer, Treeitem>();
		fillupLeftSideTree();
	}
	
	protected void customize()
	{
		ListitemRenderer<Object> render = rightSide.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer renderer = (WListItemRenderer) render;
			String customize = renderer.getColumnCustomization();
			boolean userPreference = false;
			MRole currRole = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
			if (currRole.isCanSaveColumnWidthEveryone())
			{
				FDialog.ask(getForm().getWindowNo(), this.getForm(), 
						"This change apply to everyone who has not set their own preference.", 
						new Callback<Boolean>() {

					@Override
					public void onCallback(Boolean userPreference) {
						customize0(customize, userPreference);
					}
					
				});
			}
			else
			{
				customize0(customize, userPreference);
			}
			
		}
	}
	
	protected void initTableColumnWidth()
	{
		ListitemRenderer<Object> render = rightSide.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer infoRenderer = (WListItemRenderer) render;

			MTabCustomization tabCustom = MTabCustomization.getInfo(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()),
					WINDOW_COPYQUOTELEG, null);

			if (tabCustom == null)
				tabCustom = MTabCustomization.getInfo(Env.getCtx(), MTabCustomization.SUPERUSER, WINDOW_COPYQUOTELEG, null);

			if (tabCustom != null)
			{
				infoRenderer.setHeaderColumnWidth(tabCustom.getcustom());
			}
			rightSide.repaint();
		}
	}
	
	private void customize0(String customize, boolean userPreference) {
		WListItemRenderer renderer;

		boolean ok = MTabCustomization.saveInfoData(Env.getCtx(), WINDOW_COPYQUOTELEG, Env.getAD_User_ID(Env.getCtx()),
				customize, null);
		if (ok && userPreference && Env.getAD_User_ID(Env.getCtx()) != MTabCustomization.SUPERUSER)
			ok = MTabCustomization.saveInfoData(Env.getCtx(), WINDOW_COPYQUOTELEG, MTabCustomization.SUPERUSER,
					customize, null);
		
		if (ok)
			FDialog.info(getForm().getWindowNo(), getForm(), "Preference save successfully.");
		else
			FDialog.info(getForm().getWindowNo(), getForm(), "Preference cannot be saved");
	}
	/**
	 * Whenever an item is added, recalculate
	 * all the legs to adjust all the time
	 */
	public  void recalculateTime()
	{
		//renumber line number
		//adjust trip start and trip based on duration
		//when it's new day, adjust day +1
		//start time of day comes from the user prompt, starting day 1 comes from start day time of c_order

		//highlight start of day
		for (Listitem item : rightSide.getItems()) 
		{
			IDColumn selItem = (IDColumn) rightSide.getValueAt(item.getIndex(), i_OrderLine_ID);
			if (selItem.getRecord_ID()==0 && rightSide.getValueAt(item.getIndex(), i_Description)!=null && !rightSide.getValueAt(item.getIndex(), i_Description).equals(FILLER_NEXT_DAY))
			{
				item.setSclass("line-start-of-day");
				item.setSelectable(false);
			}
			if ( mapCurrentLinesOfOrder!=null && mapCurrentLinesOfOrder.containsKey(selItem.getRecord_ID()))
			{
				item.setSclass("line-current");
			}
		}
	}
	
	@Override
	public void tableChanged(WTableModelEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equals(Events.ON_CLICK)) {
			if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))) {
				onProcess();
				form.onClose();
			} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE))) {
				customize();
			} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_REFRESH))) {
				onRefresh();
			} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL))) {
				DB.rollback(true, trxName);
				form.onClose();
			} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_RESET))) {
				DB.rollback(true, trxName);
				fillRightSide(form.getProcessInfo().getRecord_ID());
			} else if (event.getTarget().equals(confirmPanel.getButton("list"))) {
				onSummary();
			}
			
		} else if(event.getName().equals(ON_AFTER_LOAD_FORM)) {
			fillRightSide(form.getProcessInfo().getRecord_ID());
		}  else if(event.getName().equals(Events.ON_CTRL_KEY)) {
			//TODO - add keyboard shortcut
			//ReloadOrderline
			//RemoveSelection
			KeyEvent keyEvent = (KeyEvent) event;
			int code=keyEvent.getKeyCode(); 
			boolean isAlt = keyEvent.isAltKey();

			if(code == confirmPanel.getButtonKeyEvent(ConfirmPanel.A_REFRESH) && isAlt)
			{
				onRefresh();
			}
			else if(code == confirmPanel.getButtonKeyEvent(ConfirmPanel.A_RESET) && isAlt)
			{
				DB.rollback(true, trxName);
				fillRightSide(form.getProcessInfo().getRecord_ID());
			}
			else if(code == confirmPanel.getButtonKeyEvent(ConfirmPanel.A_PROCESS) && isAlt)
			{
				onProcess();
				form.onClose();
			}
			else if(code == confirmPanel.getButtonKeyEvent(ConfirmPanel.A_CANCEL) && isAlt)
			{
				DB.rollback(true, trxName);
				form.onClose();
			} 
			else if (code == confirmPanel.getButtonKeyEvent("list") && isAlt)
			{
				onSummary();
			}
		}
//		recalculateTime();
	}
	
	/**
	 * Load orderlines from a given C_Order_ID
	 * Ensure the integrity and order of the line
	 * meaning LumpSum  should be pushed down the bottom
	 * None leg Order Line (e.g. A&EF should have 0 journey time) 
	 * @param C_Order_ID
	 */
	private  void fillRightSide(int C_Order_ID)
	{
		if (trxName == null) {
			trxName = Trx.createTrxName();
		}
		m_order = new MOrder(Env.getCtx(), C_Order_ID, trxName);
		
		if (m_order.getDateTripStart() == null) {
			//must not happen - hide this button if start date is null
			//use current date for now
			m_order.setDateTripStart(TimeUtil.getDay(null));
		}
		List<MOrderLine> orderLines = new Query(Env.getCtx(), I_C_OrderLine.Table_Name,
				MOrderLine.COLUMNNAME_C_Order_ID + "=? ", trxName)
				.setParameters(C_Order_ID)
				.setOrderBy("datetripstart asc")
				.list();
		
		llOrderLine.clear();
		
		int lsProduct_ID = MSysConfig.getIntValue("PRODUCT_LUMPSUMP", 0, m_order.getAD_Client_ID());
		MOrderLine lsol = null;  //lump sump order line - add at the end
		for (MOrderLine ol : orderLines)
		{
			if (ol.getM_Product_ID() == lsProduct_ID) {
				lsol = ol;
			} else {
				llOrderLine.add(ol);
			}
		}
		if (lsol != null)
			llOrderLine.add(lsol);
		
		reloadOrderLine();
		

	}
	void migrateValueAcrossLists (Event event)
	{
		if (event instanceof MouseEvent &&
				event.getName().equals(Events.ON_DOUBLE_CLICK)) {
			if (event.getTarget() == leftSide) {
				addSelection();
			} 
		} else if (event.getTarget().equals(bAdd)) {
			addSelection();
		} else if (event.getTarget().equals(bRemove)) {
			removeSelection();
		}

	}	//	migrateValueAcrossLists



	public void moveSelectedItem(Event event) throws Exception
	{
		Object source = event.getTarget();
		
		Listitem item = rightSide.getSelectedItem();
		if (item ==null || (boolean) rightSide.getValueAt(item.getIndex(), i_IsFiller))
			return;
		
		IDColumn selItem = (IDColumn) rightSide.getValueAt(item.getIndex(), i_OrderLine_ID);
		m_selOrderLineID = selItem.getRecord_ID();
		
		activeOrderLine = null;
		prevOrderLineLeg = null;
		int idxOl = 0;
		for (MOrderLine l : llOrderLine) {
			if (l.getC_OrderLine_ID() == m_selOrderLineID) {
				activeOrderLine = l;
				break;
			}
			if (l.getJourneyTime().signum() > 0) {
				//a leg
				prevOrderLineLeg = l;
			}
			idxOl++;
		}
		
		
		if (source == bUp) {
			if (idxOl == 0) {
				return;  //nothing to move up
			}
			MOrderLine toSwap = llOrderLine.get(idxOl - 1);
			llOrderLine.set(idxOl - 1, activeOrderLine);
			llOrderLine.set(idxOl, toSwap);
			
		} else if (source == bDown) {
			if (idxOl == llOrderLine.size() - 1) {
				return;  //nothing to move down
			}

			MOrderLine toSwap = llOrderLine.get(idxOl + 1);
			llOrderLine.set(idxOl + 1, activeOrderLine);
			llOrderLine.set(idxOl, toSwap);
			
		} else if (source == bTop) {
			llOrderLine.remove(idxOl);
			llOrderLine.push(activeOrderLine);  //add to the first
		} else if (source == bBottom) {
			llOrderLine.remove(idxOl);
			llOrderLine.add(activeOrderLine);
		}
		
		reloadOrderLine();
		

	} //moveSelectedItem
	
	
	private void removeSelection()
	{
		
		Listitem item = rightSide.getSelectedItem();
		if ((boolean) rightSide.getValueAt(item.getIndex(), i_IsFiller))
			return;
		
		IDColumn selItem = (IDColumn) rightSide.getValueAt(item.getIndex(), i_OrderLine_ID);
		int lineID = selItem.getRecord_ID();
		
		int idxOl = 0;
		for (MOrderLine l : llOrderLine) {
			if (l.getC_OrderLine_ID() == lineID) {
				activeOrderLine = l;
				break;
			}

			idxOl++;
		}
		activeOrderLine.delete(true);
		activeOrderLine = null;
		llOrderLine.remove(idxOl);
		
		m_selOrderLineID = 0;
		reloadOrderLine();
	}


	private void addSelection() {
		Treeitem item = leftSide.getSelectedItem();
		int treeLevel = (int) item.getAttribute(ATT_QUOTE_LEVEL);
		MOrderLine[] fromLines = null;
		if (treeLevel == QUOTE_LEVEL_ORDER_HEADER) {
			//add all children
			int c_order_id =   (int) item.getAttribute(ATT_QUOTE_ID);
			MOrder fromOrder = new MOrder(Env.getCtx(), c_order_id, trxName);
			fromLines = fromOrder.getLines();
		} else {
			int c_orderline_id =   (int) item.getAttribute(ATT_QUOTE_ID);
			fromLines = new MOrderLine[1];
			fromLines[0] = new MOrderLine(Env.getCtx(), c_orderline_id, trxName);
		}
		copyOrderLine(fromLines);
		reloadOrderLine();
	}
	
	private void copyOrderLine(MOrderLine[] fromLines)
	{
		for (MOrderLine fromLine : fromLines) {
			MOrderLine toOrderLine = new MOrderLine(Env.getCtx(), 0, trxName);
			PO.copyValues(fromLine, toOrderLine);
			toOrderLine.setAD_Org_ID(m_order.getAD_Org_ID());
			toOrderLine.setC_Order_ID(m_order.getC_Order_ID());
			toOrderLine.setQtyReserved(Env.ZERO);  //shouldn't copy
			toOrderLine.setQtyDelivered(Env.ZERO);
			toOrderLine.setQtyInvoiced(Env.ZERO);
			toOrderLine.setQtyLostSales(Env.ZERO);
			toOrderLine.setQty(fromLine.getQtyEntered());			
			toOrderLine.setDateTripStart(curDateStart);
			curDateStart = TimeUtil.addDays(curDateStart, fromLine.getJourneyTime().multiply(new BigDecimal(60)).intValue());
			toOrderLine.setDateTripEnd(curDateStart);
			toOrderLine.save();
			
			//copy order line detail
			MOrderLineDetail[] details = MOrderLineDetail.getOrderLineDetails(Env.getCtx(), fromLine.getC_OrderLine_ID(), trxName);
			for (MOrderLineDetail detail : details)
			{
				MOrderLineDetail newDetail = new MOrderLineDetail(Env.getCtx(), 0, trxName);
				PO.copyValues(detail, newDetail);
				newDetail.setAD_Org_ID(m_order.getAD_Org_ID());
				newDetail.setC_OrderLine_ID(toOrderLine.getC_OrderLine_ID());
				newDetail.save();
				
			}
			m_selOrderLineID = toOrderLine.getC_OrderLine_ID();
			llOrderLine.add(toOrderLine);
		}
	}


	/**
	 * Rearrange items according to the sequence
	 * Recalculate time interval
	 */
	private void reloadOrderLine()
	{
		rightSide.setRowCount(0);
		Timestamp lastDateStart = null;
		//if quote has null starting date - must prompt the user
		curDateStart = m_order.getDateTripStart();
		int row = 0;
		int lineNo = 1;
		int nLine = 0;
		int nDay = 1;
		int legIdx = 0;

		addFiller(FILLER_TRIP_START, lineNo, curDateStart, 0);
		
		for (MOrderLine ol : llOrderLine) {
			if (ol == null) {
				continue;
			}
			nLine++;
			lineNo = nLine * 10;
			ol.setLine(lineNo);
			if (ol.getM_Product_ID() > 0 && ol.getM_Product().isBOM() && ol.getM_Product().getM_PartType_ID() == 0) {
				//ol is a itenary leg
				legIdx++;
			} else {
				ol.setJourneyTime(Env.ZERO);
				Timestamp timeStart = lastDateStart == null ? curDateStart : lastDateStart;
				ol.setDateTripStart(timeStart);  // should start from beginning of the leg
				ol.setDateTripEnd(curDateStart);
				addOrderLine(ol);
				continue;
			}
			if (ol.getDateTripStart() == null) {
				ol.setDateTripStart(curDateStart);
				if (ol.getJourneyTime().signum() > 0) {
					curDateStart = TimeUtil.addMinutess(curDateStart, ol.getJourneyTime().intValue()*60);
					ol.setDateTripEnd(curDateStart);
				}
			} else if (TimeUtil.isSameHourMin(ol.getDateTripStart(), curDateStart)) {
				//contiguous
				if (ol.getDateTripEnd() == null) {
					ol.setDateTripEnd(TimeUtil.addMinutess(ol.getDateTripStart(), ol.getJourneyTime().multiply(new BigDecimal(60)).intValue()));
				}
				curDateStart = ol.getDateTripEnd();
			} else if (TimeUtil.isSameDay(ol.getDateTripStart(), curDateStart) &&
					!TimeUtil.isSameHourMin(ol.getDateTripStart(), curDateStart)) {
				
				if (legIdx == 1 || ol.getDateTripStart().before(curDateStart) ) {
					//first leg - adjust trip start
					ol.setDateTripStart(curDateStart);
				} else {
					//free time
					int gapMin = TimeUtil.getHoursBetween(curDateStart, ol.getDateTripStart()).multiply(new BigDecimal(60)).intValue();
					int fLineno = (nLine-1)*10 + 1;  //must be before the orderline
					addFiller(FILLER_FREE_TIME, fLineno, curDateStart, gapMin );
				}
				
				ol.setDateTripEnd(TimeUtil.addMinutess(ol.getDateTripStart(), ol.getJourneyTime().multiply(new BigDecimal(60)).intValue()));
				curDateStart = ol.getDateTripEnd();
			} else if (ol.getDateTripStart().before(curDateStart)) {
				// date cannot move backwards - push everything
				ol.setDateTripStart(curDateStart);
				ol.setDateTripEnd(TimeUtil.addMinutess(ol.getDateTripStart(), ol.getJourneyTime().multiply(new BigDecimal(60)).intValue()));
				curDateStart = ol.getDateTripEnd();
				
			} else {
				//this is a next day
				int fLineno = (nLine-1)*10 + 1;  //must be before the orderline
				addFiller(FILLER_NEXT_DAY, fLineno, ol.getDateTripStart(), 0 );
				if (ol.getDateTripEnd() == null) {
					ol.setDateTripEnd(TimeUtil.addMinutess(ol.getDateTripStart(), ol.getJourneyTime().multiply(new BigDecimal(60)).intValue()));
				}
				curDateStart = ol.getDateTripEnd();
			}
			lastDateStart = ol.getDateTripStart();
			addOrderLine(ol);
		}

		addFiller(FILLER_TRIP_END, lineNo + 1, curDateStart, 0);

		rightSide.repaint();
		
		if  (m_selOrderLineID == 0) {
			return;
		}
		//restore previous selection
		for (Listitem i : rightSide.getItems()) {
			IDColumn idItem = (IDColumn) rightSide.getValueAt(i.getIndex(), i_OrderLine_ID);
			int id = idItem.getRecord_ID();
			if (id == m_selOrderLineID) {
				rightSide.selectItem(i);
				break;
			}
		}
	}
	
	private void addOrderLine(MOrderLine ol)
	{
		String description = ol.getDescription();
		if (StringUtils.isEmpty(description)) {
			if (ol.getM_Product_ID() > 0) {
				description = ol.getM_Product().getName(); 
			}
		}
		
		Timestamp legStart = ol.getDateTripStart();
		Timestamp legEnd = ol.getDateTripEnd();
		addItem(ol.getC_OrderLine_ID(), ol.getLine(), description
				, legStart, legEnd, false);
	}
	
	/**
	 * 
	 * @param description
	 * @param dtStart
	 * @param gapInMin -in minutes
	 */
	private void addFiller(String description, int lineNo, Timestamp dtStart, int gapInMin)
	{
		addItem(0, lineNo, description, dtStart, TimeUtil.addMinutess(dtStart, gapInMin), true);
	}
	
	private void addItem(
			int id, int lineNo, String description, Timestamp legStart, Timestamp legEnd, boolean isFiller)
	{
		int day = TimeUtil.getDaysBetween(m_order.getDateTripStart(), legStart) + 1;
		String startStr = TimeUtil.toHHmmFormat(legStart);
		String endStr = TimeUtil.toHHmmFormat(legEnd);
		BigDecimal jt = TimeUtil.getHoursBetween(legStart, legEnd);
		
		int row = rightSide.getRowCount();
		rightSide.setRowCount(row + 1);
		IDColumn itemID = new IDColumn(id);
		rightSide.setValueAt(itemID, row, i_OrderLine_ID);
		rightSide.setValueAt(description, row, i_Description);
		rightSide.setValueAt(lineNo, row, i_Line);
		rightSide.setValueAt(day, row, i_Day);
		rightSide.setValueAt(legStart, row, i_Date);
		rightSide.setValueAt(startStr, row, i_StartTime);
		rightSide.setValueAt(endStr, row, i_EndTime);
		rightSide.setValueAt(jt, row, i_JourneyTime);
		rightSide.setValueAt(isFiller, row, i_IsFiller);
		
		if (isFiller) {
			Listitem item = rightSide.getItems().get(row);
			item.setSclass("line-filler");
		}
		
	}
	

	/**
	 * 
	 * @param level
	 * @param c_order_id
	 * @param c_orderline_id
	 * @param journeyTime
	 * @param description
	 */
	private void addItemtoTree(int level, int c_order_id, int c_orderline_id, BigDecimal journeyTime, String description) {
		Treeitem parent = null;
		Treeitem item = new Treeitem();
		Treechildren parentTC = null;
		String style = null;
		String journeyTimeStr = "";
		
		item.setAttribute(ATT_QUOTE_LEVEL, level);
		
		if (level == QUOTE_LEVEL_ORDER_HEADER) {
			parentTC = leftSide.getTreechildren();
			if (parentTC == null) {
				parentTC = new Treechildren();
				leftSide.appendChild(parentTC);
			}
			item.setAttribute(ATT_QUOTE_ID, c_order_id);
			journeyTimeStr = "-";
			mapOrderHeader.put(c_order_id, item);
			style = AVL_QUOTE_HEADER_STYLE;
		} else if (level == QUOTE_LEVEL_ORDER_LINE) {
			parent = mapOrderHeader.get(c_order_id);
			parentTC = parent.getTreechildren();
			if (parentTC == null) {
				parentTC = new Treechildren();
				parent.appendChild(parentTC);
			}
			
			if (journeyTime != null) {
				journeyTime=journeyTime.setScale(2, RoundingMode.HALF_UP);
				journeyTimeStr = DisplayType.getNumberFormat(DisplayType.Quantity).format(journeyTime);
			}
			item.setAttribute(ATT_QUOTE_ID, c_orderline_id);
			style = AVL_QUOTE_LINE_STYLE;
		} else {
			log.severe("Unhandled order quote level error");
		}
		
		parentTC.appendChild(item);
		
		Treecell cell = null;
		Treerow row = new Treerow();
		item.appendChild(row);
		cell = new Treecell(description);
		cell.setStyle(style);
		row.appendChild(cell);
		
		cell = new Treecell(journeyTimeStr);
		cell.setStyle(style);
		cell.setStyle(style + "text-align: center;");
		
		row.appendChild(cell);
		
		
	}
	
	private void fillupLeftSideTree()
	{
		String sql = "SELECT o.c_order_id, " + "       ol.c_orderline_id, "
				+ "		 coalesce(NULLIF(concat_ws('-', o.poreference, o.description), ''), o.documentno) AS header_description, "
				+ "       COALESCE(NULLIF(ol.description, ''), ol.line || '-' || 'No Description')  AS line_description, "
				+ "       journeytime " 
				+ " FROM c_order o "
				+ "  INNER JOIN c_orderline ol ON (o.c_order_id = ol.c_order_id) " 
				+ "  INNER JOIN c_bpartner bp ON (o.c_bpartner_id = bp.c_bpartner_id) " 
				+ " WHERE o.issotrx = 'Y' and bp.c_bp_group_id= 1000003"
				+ " AND ol.C_Order_ID <> " + form.getProcessInfo().getRecord_ID();
		if (!Util.isEmpty(depotPick.getValue()))
		{
			sql += " AND ol.M_Warehouse_ID = " + depotPick.getSelectedItem().getValue();
		}
		if (!Util.isEmpty(bpPick.getValue()))
		{
			sql += " AND o.C_BPartner_ID = " + bpPick.getSelectedItem().getValue();
		}
		if (!Util.isEmpty(textToken.getValue()))
		{
			sql+= " AND findkeyword(to_tsvector('english', o.poreference || ' ' || o.description || ' ' || ol.description), ?) ";
		}
		sql += " ORDER BY o.c_order_id, " + "         ol.line";

		sql = MRole.getDefault().addAccessSQL(sql, "o", MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			int last_order_id = 0;
			pstmt = DB.prepareStatement(sql, null);
			if (!Util.isEmpty(textToken.getValue())) {
				String tsquery = org.adempiere.util.StringUtils.rewritetoTSQuery(textToken.getValue());
				pstmt.setString(1, tsquery);
			}
				
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				int c_order_id = rs.getInt(1);
				int c_orderline_id = rs.getInt(2);
				String headerDesc = rs.getString(3);
				String lineDesc = rs.getString(4);
				BigDecimal time = rs.getBigDecimal(5);
				if (last_order_id != c_order_id) {
					addItemtoTree(QUOTE_LEVEL_ORDER_HEADER, c_order_id, 0, time, headerDesc);
					last_order_id = c_order_id;
				}
				addItemtoTree(QUOTE_LEVEL_ORDER_LINE, c_order_id, c_orderline_id, time, lineDesc);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		if(!Util.isEmpty(depotPick.getValue()) && !Util.isEmpty(textToken.getValue()))
		{
			bChooseAll.setEnabled(true);
		}
		else
		{
			bChooseAll.setEnabled(false);
		}
	}
	@Override
	public ADForm getForm() {
		return form;
	}

	/**
	 * 
	 * @return
	 */
	public Mode getWindowMode() {
		return Mode.HIGHLIGHTED;
	}
}
