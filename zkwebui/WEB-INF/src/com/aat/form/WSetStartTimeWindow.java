package com.aat.form;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Datebox;
import org.adempiere.webui.component.DatetimeBox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.util.TimeUtil;
import org.zkoss.calendar.impl.SimpleDateFormatter;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Space;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Vlayout;

public class WSetStartTimeWindow extends Window implements  EventListener<Event>
{
	
	private static String 		STYLE_LABEL = "font-size: larger; line-height: 50px; padding-left: 10px; font-weight: bold; color: #326F70;";
	
	public Label				lblStartTime		= new Label("New Time:");
	public Label				lblDuration			= new Label("Duration (hrs)");
	public Label				lblCurrentStart		= new Label("");
	
	public Label				lblDayBefore		= new Label("Day Before");
	public Label				lblNextDay			= new Label("Start Next Day");
	public Label				lblNewTime			= new Label("Adjust Time to");
	public Label				lblSpecifyDate		= new Label("Specify Date");

//	private Datebox							
	private Radiogroup			radioGroup			= new Radiogroup();

	private Radio				dayBeforeRadio		= new Radio();
	private Radio				startNewRadio		= new Radio();
	private Radio				freeTimeRadio		= new Radio();
	private Radio				specifyDateRadio	= new Radio();

	private ConfirmPanel		confirmPanel		= new ConfirmPanel(true);

	private Timebox				newStartTime		= new Timebox();
	private Datebox				tripStartDate		= new Datebox();
//	private NumberBox			startTimeNumbox		= new NumberBox(true);
//	private NumberBox			durationNumbox		= new NumberBox(true);

	private Grid				radioLayout 		= GridFactory.newGridLayout();
	
	private boolean 			m_cancelled			= false;
	private Timestamp			m_EarliestStart = null;
	private	Timestamp			m_StartDate = null;
	private	BigDecimal			m_journeyTime = null;
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1146904965446680449L;

	public WSetStartTimeWindow()
	{

		initZK();
		setupRadioButtons();
		confirmPanel.addActionListener(this);
//		confirmPanel.getOKButton().addEventListener(Events.ON_CLICK, this);
//		confirmPanel.getButton(ConfirmPanel.A_CANCEL).addEventListener(Events.ON_CLICK, this);
	}

	private void initZK() {
	    this.setHeight("350px");
	    this.setWidth("700px");
	    this.setTitle("Set Trip Start Time");
	    this.setClosable(true);
	    this.setBorder("normal");
//		this.setAttribute("mode", "modal");
		
	    newStartTime.setFormat("HH:mm");
	    newStartTime.setWidth("180px");
	    tripStartDate.setWidth("180px");
	    
	    Vlayout vlayout = new Vlayout();

	    Columns columns = new Columns();
	    Column column;
	    column = new Column();
	    column.setWidth("20px");
	    columns.appendChild(column);
	    column = new Column();
	    column.setWidth("150px");
	    columns.appendChild(column);
	    column = new Column();
	    column.setWidth("200px");
	    columns.appendChild(column);
	    
		radioLayout.appendChild(columns);
	    ZKUpdateUtil.setWidth(startNewRadio, "40px");
	    ZKUpdateUtil.setWidth(freeTimeRadio, "40px");
	    
		Rows rows = null;
		Row row = null;
		ZKUpdateUtil.setWidth(radioLayout, "80%");
		ZKUpdateUtil.setVflex(radioLayout, "true");

		dayBeforeRadio.setRadiogroup(radioGroup);
		startNewRadio.setRadiogroup(radioGroup);
		freeTimeRadio.setRadiogroup(radioGroup);
		specifyDateRadio.setRadiogroup(radioGroup);

		rows = radioLayout.newRows();
		
		row = rows.newRow();
	    row.appendChild(startNewRadio);
		row.appendChild(lblNextDay);
		
		row = rows.newRow();
		row.appendChild(freeTimeRadio);
		row.appendChild(lblNewTime);

		row = rows.newRow();
		row.appendChild(specifyDateRadio);
		row.appendChild(lblSpecifyDate);
		row.appendChild(tripStartDate);

//		row.appendChild(startTimeNumbox);
//		row.appendChild(durationNumbox);
		
		row = rows.newRow();
	    row.appendChild(dayBeforeRadio);
		row.appendChild(lblDayBefore);
		
		row = rows.newRow();
		row.appendCellChild(new Space());
	    row.appendCellChild(lblStartTime);
	    row.appendCellChild(newStartTime, 1);

	    
	    vlayout.appendChild(lblCurrentStart);
	    vlayout.appendChild(radioLayout);
	    vlayout.appendChild(confirmPanel);
	    vlayout.appendChild(radioGroup);

	    this.appendChild(vlayout);
	}

	private void setupRadioButtons() {
		radioGroup.addEventListener(Events.ON_CHECK, event -> {
	        if (startNewRadio.isChecked()) {
	        	tripStartDate.setEnabled(false);
	        } else if (dayBeforeRadio.isChecked()) {
	        	tripStartDate.setEnabled(false);
	        	newStartTime.setValue(m_EarliestStart);
	        } else if (freeTimeRadio.isChecked()) {
	        	tripStartDate.setEnabled(false);
	            newStartTime.setValue(m_StartDate);
	        } else if (specifyDateRadio.isChecked()) {
	        	tripStartDate.setValue(m_StartDate);
	        	tripStartDate.setEnabled(true);
	        	newStartTime.setValue(m_StartDate);
	        }
		});
		

	}
	
	@Override
	public void onEvent(Event event) throws Exception
	{
		if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))) {
			if (isValid()) {
//				onClose();
				detach();
			}
		} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL))) {
			m_cancelled = true;
//			onClose();
			detach();
		}
	}
	
	public boolean isStartDaySelected()
	{
		return startNewRadio.isChecked();
	}
	
	private boolean isValid()
	{
		if (newStartTime.getValue() == null)
			return false;
		
		if (m_EarliestStart != null && getNewStartTime().before(m_EarliestStart)) {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			FDialog.error(0, "Conflict with other leg. Can't adjust time earlier than " +  sdf.format(m_EarliestStart));
			return false;
		} 
		
		return true;
	}
	
	//TODO - must replace with time editor
//	public BigDecimal getStartTime()
//	{
//		return startTimeNumbox.getValue();
//	}
//	
//	public int getFreeTimeInMin()
//	{
//		BigDecimal bd = durationNumbox.getValue();
//		
//		return bd.intValue();
//	}
//	
	public void setStartDate(Timestamp start, Timestamp earliest, BigDecimal journeyTime)
	{
		m_StartDate = start;
		m_EarliestStart = earliest;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		lblCurrentStart.setStyle(STYLE_LABEL);
		lblCurrentStart.setValue(sdf.format(m_StartDate) + String.format(" (%.1f)", journeyTime));
	}
	
	public Timestamp getNewStartTime()
	{
		if (isStartDaySelected()) {
			m_StartDate = TimeUtil.addDays(m_StartDate, 1);
		} else if (dayBeforeRadio.isChecked()) {
			m_StartDate = TimeUtil.addDays(m_StartDate, -1);			
		} else if (specifyDateRadio.isChecked()) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(tripStartDate.getValue());
			m_StartDate  = new Timestamp(cal.getTimeInMillis());
		}
		
		m_StartDate = TimeUtil.addHHmmToDay(m_StartDate, newStartTime.getText());
		return m_StartDate;
	}
	
	public Button getOKButton()
	{
		return confirmPanel.getOKButton();
	}
	
	public boolean isCancelled()
	{
		return m_cancelled;
	}
	
}
