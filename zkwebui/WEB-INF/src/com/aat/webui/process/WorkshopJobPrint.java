package com.aat.webui.process;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.model.MAttachment;
import org.compiere.model.MProject;
import org.compiere.model.MQuery;
import org.compiere.model.MSysConfig;
import org.compiere.model.PrintInfo;
import org.compiere.model.Query;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;

import com.aat.model.MVMPlanSchedule;
import com.aat.model.MVehicleDefect;
import com.aat.model.MWkshopJobPartsKit;
import com.aat.model.MWrkshopJobSched;
import com.adaxa.adempiere.util.PdfUtil;

/**
 *	Print Waybill
 *
 */
public class WorkshopJobPrint extends SvrProcess
{
	private int			m_AD_PrintFormat_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null && para[i].getParameter_To() == null)
				;
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}	//	prepare

	/**
	 *  Perrform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{

		MProject workshopJob = new MProject(getCtx(), getRecord_ID(), get_TrxName());
		m_AD_PrintFormat_ID = MSysConfig.getIntValue("WORKSHOP_JOB_PF", 0, Env.getAD_Client_ID(getCtx()));
		if (m_AD_PrintFormat_ID == 0) {
			throw new AdempiereUserError ("@NoPrintFormat@");
		}
		
		List<File> pdfList = new ArrayList<File>();
		File pdf = null;
		//Create the workshop job header
		pdf = getWorkshopJobHeader(workshopJob.getValue());
		pdfList.add(pdf);
		
		//Add workshop task list
		List<MWrkshopJobSched> schedules = new Query(getCtx()
				, MWrkshopJobSched.Table_Name, "C_Project_ID = ?", get_TrxName())
				.setParameters(getRecord_ID())
				.list();
		
		for (MWrkshopJobSched sched : schedules) {
			//sched.getJTT_VMPlanSchedule(()
			
			MAttachment attachment =  MAttachment.get(getCtx(), MVMPlanSchedule.Table_ID, sched.getJTT_VMPlanSchedule_ID());
			if (attachment != null && attachment.getEntries().size() > 0) {
				pdfList.add(attachment.getEntries().get(0).getFile());
			}
						
		}
		
		//add defect if set
		m_AD_PrintFormat_ID = MSysConfig.getIntValue("WORKSHOP_JOB_DEFECT_PF", 0, Env.getAD_Client_ID(getCtx()));
		if (m_AD_PrintFormat_ID > 0) {
			pdf = getWorkshopJobDefect(workshopJob.getValue());
			pdfList.add(pdf);
		}
		//add parts issue if set
		m_AD_PrintFormat_ID = MSysConfig.getIntValue("WORKSHOP_JOB_PARTSKIT_PF", 0, Env.getAD_Client_ID(getCtx()));
		if (m_AD_PrintFormat_ID > 0) {
			pdf = getWorkshopPartsIssue(workshopJob.getValue());
			pdfList.add(pdf);
		}

		
		if (pdfList.size() == 0) {
			return "@Printed@=0" ;
		}
		
		File outFile = File.createTempFile("WorkshopJob" + workshopJob.getValue(), ".pdf");	

		if (pdfList.size() > 1) {
			PdfUtil.mergePdf(pdfList, outFile);
		} 
		else {
			outFile = pdfList.get(0);
		}
		
//
//		ReportCtl.getReportViewerProvider().openViewer("Invoice Print",
//				new FileInputStream(outFile));
		
		SimplePDFViewer pdfViewer = new SimplePDFViewer("Workshop Job Print", new FileInputStream(outFile));
		pdfViewer.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
		ZKUpdateUtil.setWidth(pdfViewer, "100%"); 

		final SimplePDFViewer workshopPDFViewer = pdfViewer;
		if (workshopPDFViewer != null)
		{
			//
			AEnv.executeAsyncDesktopTask(new Runnable() {

				@Override
				public void run()
				{
					SessionManager.getAppDesktop().showWindow(workshopPDFViewer);
				}
			});
		}

		return "@Printed@=" + pdfList.size();
	}	//	doIt

	private File getWorkshopJobHeader(String documentNo)
	{
		MPrintFormat workshopPF = new MPrintFormat(getCtx(), m_AD_PrintFormat_ID, null);
		PrintInfo info = new PrintInfo(
				workshopPF.getName() + "-" + documentNo,
				MProject.Table_ID,
				getRecord_ID());
			info.setCopies(1);
		MQuery query = new MQuery("C_Project");
		query.addRestriction("C_Project_ID", MQuery.EQUAL, getRecord_ID());

		ReportEngine re = new ReportEngine(getCtx(), workshopPF, query, info);
			
		return re.getPDF();
	}
	
	private File getWorkshopJobDefect(String documentNo)
	{
		MPrintFormat workshopDefect = new MPrintFormat(getCtx(), m_AD_PrintFormat_ID, null);
		PrintInfo info = new PrintInfo(
				workshopDefect.getName() + "-" + documentNo,
				MProject.Table_ID,
				getRecord_ID());
			info.setCopies(1);
		MQuery query = new MQuery(MVehicleDefect.Table_Name);
		query.addRestriction(MVehicleDefect.COLUMNNAME_C_Project_ID, MQuery.EQUAL, getRecord_ID());

		ReportEngine re = new ReportEngine(getCtx(), workshopDefect, query, info);
			
		return re.getPDF();
	}
	
	private File getWorkshopPartsIssue(String documentNo)
	{
		MPrintFormat workshopDefect = new MPrintFormat(getCtx(), m_AD_PrintFormat_ID, null);
		PrintInfo info = new PrintInfo(
				workshopDefect.getName() + "-" + documentNo,
				MProject.Table_ID,
				getRecord_ID());
			info.setCopies(1);
		MQuery query = new MQuery(MVehicleDefect.Table_Name);
		query.addRestriction(MWkshopJobPartsKit.COLUMNNAME_C_Project_ID, MQuery.EQUAL, getRecord_ID());

		ReportEngine re = new ReportEngine(getCtx(), workshopDefect, query, info);
			
		return re.getPDF();
	}

}	//	InvoicePrint
