package com.aat.webui.process;

import java.io.File;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MClient;
import org.compiere.model.MInvoice;
import org.compiere.model.MMailText;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MQuery;
import org.compiere.model.MUser;
import org.compiere.model.MUserMail;
import org.compiere.model.PrintInfo;
import org.compiere.model.X_C_Invoice;
import org.compiere.model.X_C_Order;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.EMail;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Language;
import com.adaxa.adempiere.util.PdfUtil;

/**
 *	Print Waybill
 *
 */
public class QuoteWaybillPrint extends SvrProcess
{
	private int			m_AD_PrintFormat_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null && para[i].getParameter_To() == null)
				;
			else if (name.equals("AD_PrintFormat_ID"))
				m_AD_PrintFormat_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}	//	prepare

	/**
	 *  Perrform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{

		List<KeyNamePair> keys = getInfoSelectionKeys();
		List<Integer> C_Order_IDs = new ArrayList<>();
		if (keys == null || keys.size() == 0 ) {
			return null;
		}
		
		if (m_AD_PrintFormat_ID == 0) {
			throw new AdempiereUserError ("@NoPrintFormat@");
		}
		
		MPrintFormat waybillPF = new MPrintFormat(getCtx(), m_AD_PrintFormat_ID, null);
		List<File> pdfList = new ArrayList<File>();
		
		for (KeyNamePair key : keys) {
			MOrderLine ol = new MOrderLine(getCtx(), key.getKey(), null);
			if (C_Order_IDs.contains(ol.getC_Order_ID())) {
				continue;
			}
			
			C_Order_IDs.add(ol.getC_Order_ID());
			
			MOrder order = (MOrder) ol.getC_Order();
			MQuery query = new MQuery("C_Order_Header_v");
			query.addRestriction("C_Order_ID", MQuery.EQUAL, ol.getC_Order_ID());
			
			PrintInfo info = new PrintInfo(
					waybillPF.getName() + "-" + order.getDocumentNo(),
					X_C_Order.Table_ID,
					ol.getC_Order_ID(),
					order.getC_BPartner_ID());
				info.setCopies(1);
				ReportEngine re = new ReportEngine(getCtx(), waybillPF, query, info);
				pdfList.add(re.getPDF());
		}
		
		
		if (pdfList.size() == 0) {
			return "@Printed@=0" ;
		}
		
		File outFile = File.createTempFile(waybillPF.getName(), ".pdf");	

		if (pdfList.size() > 1) {
			PdfUtil.mergePdf(pdfList, outFile);
		} 
		else {
			outFile = pdfList.get(0);
		}
		

		ReportCtl.getReportViewerProvider().openViewer("Invoice Print",
				new FileInputStream(outFile));

		return "@Printed@=" + pdfList.size();
	}	//	doIt

}	//	InvoicePrint
