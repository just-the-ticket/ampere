package org.adempiere.webui.dashboard;

public class CalendarColor
{

	public static final String	DPCALENDAR_HDR_EMAIL		= "#aa4b09";
	public static final String	DPCALENDAR_CNT_EMAIL		= "#f49a5c";

	public static final String	DPCALENDAR_HDR_PHONECALL	= "red";
	public static final String	DPCALENDAR_CNT_PHONECALL	= "#ee999c";

	public static final String	DPCALENDAR_HDR_MEETING		= "green";
	public static final String	DPCALENDAR_CNT_MEETING		= "#81d4b8";

	public static final String	DPCALENDAR_HDR_TASK			= "blue";
	public static final String	DPCALENDAR_CNT_TASK			= "#789edc";
}
