/**
 * 
 */
package org.adempiere.webui.info;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.model.MInfoRelated;
import org.adempiere.model.MInfoTreeNode;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.EditorBox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Menupopup;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WebEditorFactory;
import org.adempiere.webui.panel.InfoPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.TreeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.ReorderItemsDialog;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.EmbedWinInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.AccessSqlParser;
import org.compiere.model.AccessSqlParser.TableInfo;
import org.compiere.model.GridField;
import org.compiere.model.GridFieldVO;
import org.compiere.model.MColumn;
import org.compiere.model.MInfoColumn;
import org.compiere.model.MInfoProcess;
import org.compiere.model.MInfoTree;
import org.compiere.model.MInfoWindow;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MLookupInfo;
import org.compiere.model.MProcess;
import org.compiere.model.MQuery;
import org.compiere.model.MRole;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.model.X_AD_InfoColumn;
import org.compiere.model.X_AD_InfoTree;
import org.compiere.util.CPreparedStatement;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Evaluatee;
import org.compiere.util.Evaluator;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Center;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.North;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;
import org.zkoss.zul.Toolbar;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.West;

/**
 * Feature #1449 AD_InfoWindow implementation
 * 
 * @author Sachin Bhimani
 */
public class InfoWindow extends InfoPanel implements ValueChangeListener, EventListener<Event>
{

	private static final String TREE_AD_WINDOW_ID			= "AD_Window_ID";
	private static final String ATT_TREENODELEVEL			= "NODE_LEVEL";
	private static final String ATT_TREEITEM				= "TREE_ITEM";
	private static final String ATT_TREEITEM_ISREADONLY		= "TREE_ITEM_ISREADONLY";
	private static final String ATT_TREEITEM_SEQCOL			= "TREE_ITEM_SEQCOL";
	private static final String ATT_MENUACTION				= "MENU";
	private static final String TREE_MENUACTION_ZOOM				= "ZOOM"; 	
	private static final String TREE_MENUACTION_SORT				= "SORT"; 	
	private static final String TREE_MENUACTION_MOVETO			= "MOVE_TO"; 	
	private static final String TREE_MENUACTION_DELETE			= "DELETE"; 	

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3855084389937134885L;

	/** Max Length of Fields */
	public static final int		FIELDLENGTH			= 20;

	protected Grid				parameterGrid;
	private Borderlayout		layout;
	private Vlayout				southBody;
	/** List of WEditors */
	protected List<WEditor>		editors;
	protected List<WEditor>		identifiers;
	protected Properties		infoContext;

	/** embedded Panel **/
	Tabbox						embeddedPane		= new Tabbox();
	ArrayList<EmbedWinInfo>		embeddedWinList		= new ArrayList<EmbedWinInfo>();

	protected ColumnInfo[]		columnInfos;
//	protected MInfoWindow		infoWindow;
	protected TableInfo[]		tableInfos;
	protected MInfoColumn[]		infoColumns;
	protected String			queryValue;

	protected boolean			hasTree = false;
	protected Tree				m_Tree = null;
	protected MInfoColumn			m_LinkColumn = null;
	protected MInfoColumn			m_ParentColumn = null;
	protected MInfoColumn			m_TreeRootColumn = null;
	protected ArrayList<Integer>	m_VisibleID = null;
	protected String			m_FilterColumn = null;
	private   MInfoTree[] 		m_infoTrees;
	private   int 				m_ParentColumnIdx = 0;
    private Checkbox 			chkExpand;
    private Menupopup			m_rootPopup = null;
    private boolean				m_TreeSelected = false;
    private boolean				m_IsTreeFilterOnDB = false;
    
	
	protected ArrayList<MInfoTreeNode> m_RootNodes = null;
	protected Map<String, MInfoTreeNode> m_TreeNodes;
	/**
	 * Key = String / Columname
	 * Object = Editor default value
	 * Must save Initial value upon load to temporary variable 
	 * as context values updates on ValueChanged event
	 */
	protected Map<String, Object> m_DefaultParameters = new HashMap<String, Object>();  
	
	private List<GridField>		gridFields;
	private int					AD_InfoWindow_ID;
	private Checkbox			checkAND;
	/**
	 * Menu contain process menu item
	 */
	protected Menupopup			ipMenu;

	private ListModelTable m_modelAll;

	

	/**
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 */
	public InfoWindow(int WindowNo, String tableName, String keyColumn, String queryValue, boolean multipleSelection,
			String whereClause, int AD_InfoWindow_ID)
	{
		this(WindowNo, tableName, keyColumn, queryValue, multipleSelection, whereClause, AD_InfoWindow_ID, false);
	}

	public InfoWindow(int WindowNo, String tableName, String keyColumn, String queryValue, boolean multipleSelection,
			String whereClause, int AD_InfoWindow_ID, boolean lookup)
	{
		this(WindowNo, tableName, keyColumn, queryValue, multipleSelection, whereClause, AD_InfoWindow_ID, lookup, 0);
	}
	
	/**
	 * @param WindowNo
	 * @param tableName
	 * @param keyColumn
	 * @param multipleSelection
	 * @param whereClause
	 * @param lookup
	 */
	public InfoWindow(int WindowNo, String tableName, String keyColumn, String queryValue, boolean multipleSelection,
			String whereClause, int AD_InfoWindow_ID, boolean lookup, int record_ID)
	{

//		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause, lookup, AD_InfoWindow_ID);
		super(WindowNo, tableName, keyColumn, multipleSelection, whereClause, lookup, AD_InfoWindow_ID, record_ID);
		this.queryValue = queryValue;
		this.AD_InfoWindow_ID = AD_InfoWindow_ID;
//		contentPanel.setZclass("grid-info-window");

   		//Xolali IDEMPIERE-1045
   		contentPanel.addActionListener(new EventListener<Event>() {
   			public void onEvent(Event event) throws Exception {
   				updateSubcontent();
   			}
   		}); //xolali --end-
   		
		infoContext = new Properties(Env.getCtx());
		p_loadedOK = loadInfoDefinition();

		initialId = Env.getContextAsInt(Env.getCtx(), WindowNo, Env.TAB_INFO, keyColumn);
		
		// make process button only in window mode
		if (!m_lookup)
		{
			initInfoProcess();
			
			if (infoWindow.getAD_Process_ID() != 0)
				confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(true);
			
			if (infoWindow != null)
				p_multipleSelection = infoWindow.isMultiSelection();
		}
		loadInfoRelatedTabs();
		if (infoWindow != null) {
			m_infoTrees = infoWindow.getInfoTree(false);
			if (m_infoTrees != null && m_infoTrees.length > 0) {
				hasTree = true;
			}
		}
		if (loadedOK())
		{
			if (isLookup())
			{
				Env.clearTabContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO);
			}

			renderWindow();

			if (queryValue != null && queryValue.trim().length() > 0)
			{
				prepareTable();
				processQueryValue();
			}
			if (initialId > 0 || infoWindow.isQueryOnFirstLoad()) {
//				prepareTable();  seems duplicate
				executeQuery();
				renderItems();
			} else {
				//even without record - render to display columns 
				prepareTable();
				renderItems();
			}
		}
	}

	/**
	 * Load info process info separate by layout type init drop list and menu
	 * control set status of haveProcess flag
	 * 
	 * @return true when have process, false when no process
	 */
	protected void initInfoProcess()
	{
		if (infoWindow == null)
		{
			return;
		}

		infoProcessList = infoWindow.getInfoProcess(false);

		if (infoProcessList.length == 0)
			return;

		// make list process for drop down
		for (MInfoProcess infoProcess : infoProcessList)
		{
			String name = infoProcess.getName();
			if (name == null || name.length() == 0) {
				if (infoProcess.isReport()) {
					name = infoProcess.getAD_Table().getName();
				} else {
					MProcess process = MProcess.get(Env.getCtx(), infoProcess.getAD_Process_ID());
					name = process.get_Translation(MProcess.COLUMNNAME_Name);

				}
			}

			String tooltip = name;
			final String label = name.replaceAll("\\s+", "");
			
			if (MInfoProcess.LAYOUTTYPE_Button.equals(infoProcess.getLayoutType()))
			{
				if (infoProcessBtList == null) {
					infoProcessBtList = new ArrayList<MInfoProcess>();
				}
				
				// make process button
				Button btProcess = null;
				if (infoProcess.isReport()) {
					btProcess = confirmPanel.addReportButton(label, infoProcess.getImageURL());
				} else {
					btProcess = confirmPanel.addProcessButton(label,	infoProcess.getImageURL());
				}
				btProcess.setAttribute(ATT_INFO_PROCESS_KEY, infoProcess);
				btProcess.setAttribute(ATT_BTN_ALWAYS_ON , infoProcess.isAlwaysEnabled());
				btProcess.setTooltiptext(tooltip);
				
				btProcess.addEventListener(Events.ON_CLICK, this);
				// update tooltip help when focus
				btProcess.addEventListener(Events.ON_FOCUS, this);
				btProcessList.add(btProcess);
				

			} else if (MInfoProcess.LAYOUTTYPE_List.equals(infoProcess.getLayoutType())) {
				if (infoProcessDropList == null) {
					infoProcessDropList = new ArrayList<MInfoProcess>();
				}
				
				{
					cbbProcess = new Combobox();

					// render item, use name to display
					cbbProcess.setItemRenderer(new ComboitemRenderer<Object>() {

						@Override
						public void render(Comboitem item, Object data, int index) throws Exception
						{
							MInfoProcess data1 = (MInfoProcess) data;
							item.setValue(data1);
							item.setLabel(label);
							item.setTooltip(tooltip);
							if (!Util.isEmpty(data1.getImageURL(), true))
							{
								item.setImage(ThemeUtils.resolveImageURL(data1.getImageURL()));
							}

						}
					});

					// update tooltip, help when select a item
					cbbProcess.addEventListener(Events.ON_SELECT, this);

					confirmPanel.addComponentsCenter(cbbProcess);

					btCbbProcess = confirmPanel.addProcessButton(Msg.getMsg(Env.getCtx(), ConfirmPanel.A_PROCESS), null);

					btCbbProcess.addEventListener(Events.ON_CLICK, this);
				}
				
			} else if (MInfoProcess.LAYOUTTYPE_Menu.equals(infoProcess.getLayoutType())) {
				if (infoProcessMenuList == null) {
					infoProcessMenuList = new ArrayList<MInfoProcess>();
				}
				
				 infoProcessMenuList.add(infoProcess);
				  
				 // init popup menu if (ipMenu == null) { ipMenu = new Menupopup();
				 ipMenu.setId("ipMenu"); 
				 confirmPanel.appendChild(ipMenu);
				  
				 // init button to show menu btMenuProcess =
				 confirmPanel.addProcessButton("ProcessMenu", null);
				 btMenuProcess.setPopup("ipMenu, before_start"); 
			}
		}
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void bindInfoProcess()
	{
		bindInfoProcessBt();
		bindInfoProcessDropDown();
		bindInfoProcessMenu();
	}

	/**
	 * Evaluate display logic of process info button set visible of button base
	 * in display logic when two button set for same process, two button can
	 * hidden too, or display too. this is bug of implementor by never have this
	 * case
	 */
	protected void bindInfoProcessBt()
	{
		if (infoProcessBtList == null)
		{
			return;
		}

		// Display process in button style
		for (MInfoProcess infoProcessBt : infoProcessBtList)
		{
			// Evaluate display logic
			for (Button evlBt : btProcessList)
			{
				Integer processId = (Integer) evlBt.getAttribute(PROCESS_ID_KEY);
				if (processId.intValue() == infoProcessBt.getAD_Process_ID())
				{
					// display or hidden button
					evlBt.setVisible(infoProcessBt.isDisplayed(infoContext, p_WindowNo));
					break;
				}
			}
		}
	} // bindInfoProcessBt

	/**
	 * Recreate drop down item by recreate model to set hidden, display of drop
	 * down item when all item is hidden, hidden combo-box and process button
	 * too
	 */
	protected void bindInfoProcessDropDown()
	{
		if (infoProcessDropList == null || infoProcessDropList.size() == 0)
		{
			return;
		}

		// list info process after evaluate display logic
		List<MInfoProcess> infoProcessDropListTmp = new ArrayList<MInfoProcess>();

		// Filter item not display
		for (MInfoProcess infoProcessDropDown : infoProcessDropList)
		{
			if (infoProcessDropDown.isDisplayed(infoContext, p_WindowNo))
			{
				infoProcessDropListTmp.add(infoProcessDropDown);
			}
		}

		// when item is filter out all. don't show combobox
		cbbProcess.setVisible(infoProcessDropListTmp.size() > 0);
		btCbbProcess.setVisible(infoProcessDropListTmp.size() > 0);
		if (infoProcessDropListTmp.size() > 0)
		{
			ListModelList<MInfoProcess> infoProccessModel = new ListModelList<MInfoProcess>(infoProcessDropListTmp);
			cbbProcess.setModel(infoProccessModel);
		}

	} // bindInfoProcessDropDown

	/**
	 * recreate menu item by set hidden, display of menu item when all menu item
	 * is hidden, hidden process menu button too
	 */
	protected void bindInfoProcessMenu()
	{
		if (infoProcessMenuList == null || infoProcessMenuList == null)
			return;

		ipMenu.getChildren().clear();
		for (MInfoProcess infoProcess : infoProcessMenuList)
		{
			if (!infoProcess.isDisplayed(infoContext, p_WindowNo))
			{
				continue;
			}

			MProcess process = MProcess.get(Env.getCtx(), infoProcess.getAD_Process_ID());
			// Make menu item for each info process
			Menuitem ipMenuItem = new Menuitem();
			ipMenuItem.setLabel(process.get_Translation(MProcess.COLUMNNAME_Name));
			if (!Util.isEmpty(infoProcess.getImageURL(), true))
			{
				ipMenuItem.setIconSclass(ThemeUtils.getIconSclass(infoProcess.getImageURL()));
			}
			ipMenuItem.setAttribute(PROCESS_ID_KEY, infoProcess.getAD_Process_ID());
			ipMenuItem.addEventListener(Events.ON_CLICK, this);
			ipMenu.appendChild(ipMenuItem);
		}

		btMenuProcess.setVisible(ipMenu.getChildren().size() > 0);

	} // bindInfoProcessMenu

	protected void processQueryValue()
	{
		isQueryByUser = true;
		for (int i = 0; i < identifiers.size(); i++)
		{
			WEditor editor = identifiers.get(i);
			editor.setValue(queryValue);
			testCount(false);
			if (m_count > 0)
			{
				break;
			}
			else
			{
				editor.setValue(null);
			}
		}

		boolean splitValue = false;
		if (m_count <= 0)
		{
			String[] values = queryValue.split("[_]");
			if (values.length == 2)
			{
				splitValue = true;
				for (int i = 0; i < values.length && i < identifiers.size(); i++)
				{
					WEditor editor = identifiers.get(i);
					editor.setValue(values[i]);
				}
				testCount(false);
			}
		}

		if (m_count > 0)
		{
			executeQuery();
			renderItems();
		}
		else if (!splitValue)
		{
			editors.get(0).setValue(queryValue);
		}
		isQueryByUser = false;
	}

	protected void loadInfoWindowData (){
		if (m_infoWindowID > 0) {
			infoWindow = MInfoWindow.get(m_infoWindowID, null);
		}else {
			infoWindow = MInfoWindow.get(p_tableName, (String)null);
			if (infoWindow != null) {
				m_infoWindowID = infoWindow.getAD_InfoWindow_ID();
			}
		}
		
		if (infoWindow == null)
			return;
		if (!infoWindow.isValid()) {
//			infoWindow = null;
			throw new AdempiereException("Info Window is Invalid= " + infoWindow.getName() + ". Please run Validate Info Window." );
		} else {
			String tableName = MTable.getTableName(Env.getCtx(), infoWindow.getAD_Table_ID());
			if (!tableName.equalsIgnoreCase(p_tableName)) {
				throw new IllegalArgumentException("AD_InfoWindow.TableName <> TableName argument. ("+tableName + " <> " + p_tableName+")");
			}
		}
	}
	
	protected boolean loadInfoDefinition()
	{
		String tableName = null;
		if (AD_InfoWindow_ID > 0)
		{
			infoWindow = MInfoWindow.get(AD_InfoWindow_ID, null);
			if (!infoWindow.isValid())
			{
				infoWindow = null;
			}
			else
			{
				tableName = MTable.getTableName(Env.getCtx(), infoWindow.getAD_Table_ID());
				if (!tableName.equalsIgnoreCase(p_tableName))
				{
					throw new IllegalArgumentException("AD_InfoWindow.TableName <> TableName argument. (" + tableName
							+ " <> " + p_tableName + ")");
				}
			}
		}
		else
		{
			infoWindow = MInfoWindow.get(p_tableName, (String) null);
		}

		if (infoWindow != null)
		{
			if (tableName == null)
				tableName = MTable.getTableName(Env.getCtx(), infoWindow.getAD_Table_ID());

			AccessSqlParser sqlParser = new AccessSqlParser("SELECT * FROM " + infoWindow.getFromClause());
			tableInfos = sqlParser.getTableInfo(0);
			if (tableInfos[0].getSynonym() != null && tableInfos[0].getSynonym().trim().length() > 0)
			{
				p_tableName = tableInfos[0].getSynonym().trim();
				if (p_whereClause != null && p_whereClause.trim().length() > 0)
				{
					p_whereClause = p_whereClause.replace(tableName + ".", p_tableName + ".");
				}
			}

			infoColumns = infoWindow.getInfoColumns(tableInfos);

			gridFields = new ArrayList<GridField>();
			for (MInfoColumn infoColumn : infoColumns)
			{

				String columnName = infoColumn.getColumnName();
				/*
				 * !m_lookup && infoColumn.isMandatory():apply Mandatory only
				 * case open as window and only for criteria field
				 */
				boolean isMandatory = !m_lookup && infoColumn.isMandatory() && infoColumn.isQueryCriteria();
				GridFieldVO vo = GridFieldVO.createParameter(infoContext, p_WindowNo, 0, columnName,
						infoColumn.get_Translation("Name"), infoColumn.getAD_Reference_ID(),
						infoColumn.getAD_Reference_Value_ID(), isMandatory, false);
				if (infoColumn.getAD_Val_Rule_ID() > 0)
				{
					vo.ValidationCode = infoColumn.getAD_Val_Rule().getCode();
					if (vo.lookupInfo != null)
					{
						vo.lookupInfo.ValidationCode = vo.ValidationCode;
						vo.lookupInfo.IsValidated = false;
					}
				}
				if (infoColumn.getDisplayLogic() != null)
					vo.DisplayLogic = infoColumn.getDisplayLogic();
				if (infoColumn.isQueryCriteria() && infoColumn.getDefaultValue() != null)
					vo.DefaultValue = infoColumn.getDefaultValue();
				String desc = infoColumn.get_Translation("Description");
				vo.Description = desc != null ? desc : "";
				String help = infoColumn.get_Translation("Help");
				vo.Help = help != null ? help : "";
				GridField gridField = new GridField(vo);
				gridFields.add(gridField);
				
				if (infoColumn.isTreeColumn()) {
					m_LinkColumn = infoColumn;
				}
				if (infoColumn.isParent()) {
					m_ParentColumn = infoColumn;
				}
				if (infoColumn.isTreeRoot()) {
					m_TreeRootColumn = infoColumn;
				}
			}

			return true;
		}
		else
		{
			return false;
		}
	}
	//private MInfoColumn[] topinfoColumns;//infoWindow.getInfoColumns(tableInfos);
	protected boolean loadInfoRelatedTabs() {
		if (infoWindow == null)
			return false;

		// topinfoColumns = infoWindow.getInfoColumns();
		relatedInfoList = infoWindow.getInfoRelated(true);
		Tabpanels tabPanels = new Tabpanels();
		Tabs tabs = new Tabs();

		if (relatedInfoList.length > 0) { // setup the panel

			//embeddedPane.setTitle(Msg.translate(Env.getCtx(), "Related Information"));
			ZKUpdateUtil.setHeight(embeddedPane, "100%");
			//tabPanels = new Tabpanels();
			embeddedPane.appendChild(tabPanels);
			//tabs = new Tabs();
			embeddedPane.appendChild(tabs);

		}

		//	for(int i=0; i <  relatedinfoList.length - 1 ; i++) {
		for (MInfoRelated relatedInfo:relatedInfoList) {

			String tableName = null;		
			int infoRelatedID = relatedInfo.getRelatedInfo_ID(); 

			MInfoWindow embedInfo = MInfoWindow.get(infoRelatedID, null);

			AccessSqlParser sqlParser = new AccessSqlParser("SELECT * FROM " + embedInfo.getFromClause());
			TableInfo[] tableInfos = sqlParser.getTableInfo(0);
			if (tableInfos[0].getSynonym() != null && tableInfos[0].getSynonym().trim().length() > 0){
				tableName = tableInfos[0].getSynonym().trim();
			}

			WListbox embeddedTbl = new WListbox();
			String m_sqlEmbedded;

			//MInfoWindow.getInfoWindow(infoRelatedID);

			if (embedInfo != null) {
				ArrayList<ColumnInfo> list = new ArrayList<ColumnInfo>();
				list = getInfoColumnslayout(embedInfo);
				//  Convert ArrayList to Array
				ColumnInfo[] s_layoutEmbedded  = new ColumnInfo[list.size()];
				list.toArray(s_layoutEmbedded);

				/**	From Clause							*/
				String s_sqlFrom = embedInfo.getFromClause();
				/** Where Clause						*/
				String s_sqlWhere = relatedInfo.getLinkColumnName() + "=?";
				m_sqlEmbedded = embeddedTbl.prepareTable(s_layoutEmbedded, s_sqlFrom, s_sqlWhere, false, tableName);

				embeddedTbl.setMultiSelection(false);

				embeddedTbl.autoSize();

				embeddedTbl.getModel().addTableModelListener(this);
				ZKUpdateUtil.setVflex(embeddedTbl, "1");

				
				//Xolali - add embeddedTbl to list, add m_sqlembedded to list
				EmbedWinInfo ewinInfo = new EmbedWinInfo(embedInfo,embeddedTbl,m_sqlEmbedded,relatedInfo.getLinkColumnName(), relatedInfo.getLinkInfoColumn(), relatedInfo.getParentRelatedColumn_ID());
				embeddedWinList.add(ewinInfo);

				MInfoWindow riw = (MInfoWindow) relatedInfo.getRelatedInfo();
				String tabTitle;
				if (riw != null)
					tabTitle = Util.cleanAmp(riw.get_Translation("Name"));
				else
					tabTitle = relatedInfo.getName();
				Tab tab = new Tab(tabTitle);
				tabs.appendChild(tab);
				Tabpanel desktopTabPanel = new Tabpanel();
				//desktopTabPanel.
				ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
				desktopTabPanel.appendChild(embeddedTbl);
				tabPanels.appendChild(desktopTabPanel);
			}

		}

		return true;
	}

	protected void prepareTable()
	{
		List<ColumnInfo> list = new ArrayList<ColumnInfo>();
		String keyTableAlias = tableInfos[0].getSynonym() != null && tableInfos[0].getSynonym().trim().length() > 0
				? tableInfos[0].getSynonym() : tableInfos[0].getTableName();

		String keySelectClause = keyTableAlias + "." + p_keyColumn;
		list.add(new ColumnInfo(" ", keySelectClause, IDColumn.class));

		int i = 0;
		for (MInfoColumn infoColumn : infoColumns)
		{
			if (!infoColumn.isVariableOnly() && (infoColumn.isDisplayed(infoContext, p_WindowNo) || infoColumn.isParent()))  // Must force display when it's a parent
			{
				ColumnInfo columnInfo = null;
				String colSQL = infoColumn.getSelectClause();
				if (!colSQL.toUpperCase().contains(" AS "))
					colSQL += " AS " + infoColumn.getColumnName();
				if (infoColumn.getAD_Reference_ID() == DisplayType.ID)
				{
					if (infoColumn.getSelectClause().equalsIgnoreCase(keySelectClause)) {
						i++;
						continue;
					}

					columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL,
							DisplayType.getClass(infoColumn.getAD_Reference_ID(), true), infoColumn.isReadOnly(), infoColumn.getAD_InfoColumn_ID());
				}
				else if (DisplayType.isLookup(infoColumn.getAD_Reference_ID()))
				{
					if (infoColumn.getAD_Reference_ID() == DisplayType.List)
					{
						WEditor editor = null;
						editor = WebEditorFactory.getEditor(gridFields.get(i), true);
						editor.setMandatory(false);
						editor.setReadWrite(false);
						editorMap.put(colSQL, editor);
						columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL, ValueNamePair.class,
								(String) null, infoColumn.isReadOnly(), infoColumn.getAD_InfoColumn_ID());
					}
					else
					{
						columnInfo = createLookupColumnInfo(tableInfos, gridFields.get(i), infoColumn);
					}
				}
				else
				{
					columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL,
							DisplayType.getClass(infoColumn.getAD_Reference_ID(), true), infoColumn.isReadOnly(), infoColumn.getAD_InfoColumn_ID());
				}
				columnInfo.setAD_Reference_ID(infoColumn.getAD_Reference_ID());
				columnInfo.setGridField(gridFields.get(i));
				list.add(columnInfo);
			}
			i++;
		}

		columnInfos = list.toArray(new ColumnInfo[0]);
		prepareTable(columnInfos, infoWindow.getFromClause(), p_whereClause, infoWindow.getOrderByClause());
	}

	protected ColumnInfo createLookupColumnInfo(TableInfo[] tableInfos, GridField gridField, MInfoColumn infoColumn)
	{
		String columnName = gridField.getColumnName();
		String validationCode = "";
		//check HERE JO
		MLookupInfo lookupInfo = MLookupFactory.getLookupInfo(Env.getCtx(), p_WindowNo, 0,
				infoColumn.getAD_Reference_ID(), Env.getLanguage(Env.getCtx()), columnName,
				infoColumn.getAD_Reference_Value_ID(), false, validationCode);
//		String displayColumn = lookupInfo.DisplayColumn;

		// TODO:When select clause without alias given index error.

		WEditor editor = null;
		editor = WebEditorFactory.getEditor(gridField, true);
		editor.setMandatory(false);
		editor.setReadWrite(false);

		String colSQL = infoColumn.getSelectClause();
		if (!colSQL.toUpperCase().contains(" AS "))
			colSQL += " AS " + infoColumn.getColumnName();
		editorMap.put(colSQL, editor);
		ColumnInfo columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL, KeyNamePair.class,
				(String) null, infoColumn.isReadOnly(), infoColumn.getAD_InfoColumn_ID());
		return columnInfo;
	}

	/*
	 * (non-Javadoc)
	 * @see org.adempiere.webui.panel.InfoPanel#getSQLWhere()
	 */
	@Override
	protected String getSQLWhere()
	{
		/**
		 * when query not by click requery button, reuse prev where clause
		 */
		if (!isQueryByUser && prevWhereClause != null)
		{
			if (m_VisibleID!= null && m_VisibleID.size() > 0) {
				return prevWhereClause + " AND " 
						+ m_ParentColumn.getSelectClause() + " IN (" + StringUtils.join(m_VisibleID, ",") + ")";
			}			
			return prevWhereClause;
		}

		StringBuilder builder = new StringBuilder();
		MTable table = MTable.get(Env.getCtx(), infoWindow.getAD_Table_ID());
		if (!hasIsActiveEditor() && table.get_ColumnIndex("IsActive") >= 0)
		{
			if (p_whereClause != null && p_whereClause.trim().length() > 0)
			{
				builder.append(" AND ");
			}
			builder.append(tableInfos[0].getSynonym()).append(".IsActive='Y'");
		}
		int count = 0;
		for (WEditor editor : editors)
		{
			if (!editor.isVisible())
				continue;

			// if (editor instanceof IWhereClauseEditor) {
			// String whereClause = ((IWhereClauseEditor)
			// editor).getWhereClause();
			// if (whereClause != null && whereClause.trim().length() > 0) {
			// count++;
			// if (count == 1) {
			// if (builder.length() > 0) {
			// builder.append(" AND ");
			// if (!checkAND.isChecked()) builder.append(" ( ");
			// } else if (p_whereClause != null && p_whereClause.trim().length()
			// > 0) {
			// builder.append(" AND ");
			// if (!checkAND.isChecked()) builder.append(" ( ");
			// }
			// } else {
			// builder.append(checkAND.isChecked() ? " AND " : " OR ");
			// }
			// builder.append(whereClause);
			// }
			// } else
			if (editor.getGridField() != null && editor.getValue() != null
					&& editor.getValue().toString().trim().length() > 0)
			{
				MInfoColumn mInfoColumn = findInfoColumn(editor.getGridField());
				if (mInfoColumn == null || mInfoColumn.getSelectClause().equals(MInfoColumn.SELECT_VARIABLE_ONLY))
				{
					continue;
				}
				String columnName = mInfoColumn.getSelectClause();
				int asIndex = columnName.toUpperCase().lastIndexOf(" AS ");
				if (asIndex > 0)
				{
					columnName = columnName.substring(0, asIndex);
				}

				count++;
				if (count == 1)
				{
					if (builder.length() > 0)
					{
						builder.append(" AND ");
						if (!checkAND.isChecked())
							builder.append(" ( ");
					}
					else if (p_whereClause != null && p_whereClause.trim().length() > 0)
					{
						builder.append(" AND ");
						if (!checkAND.isChecked())
							builder.append(" ( ");
					}
					else if (hasIsActiveEditor() && !checkAND.isChecked())
					{
						builder.append(" ( ");
					}
				}
				else
				{
					builder.append(checkAND.isChecked() ? " AND " : " OR ");
				}

				if (mInfoColumn.getWhereClause() != null &&  mInfoColumn.getWhereClause().length() > 0)
				{
					builder.append(mInfoColumn.getWhereClause());
					continue;
				}
				String columnClause = null;
				if (mInfoColumn.getQueryFunction() != null && mInfoColumn.getQueryFunction().trim().length() > 0)
				{
					String function = mInfoColumn.getQueryFunction();
					if (function.indexOf("@") >= 0)
					{
						String s = Env.parseContext(infoContext, p_WindowNo, function, true, false);
						if (s.length() == 0)
						{
							log.log(Level.SEVERE, "Failed to parse query function. " + function);
						}
						else
						{
							function = s;
						}
					}
					if (function.indexOf("?") >= 0)
					{
						columnClause = function.replaceFirst("[?]", columnName);
					}
					else
					{
						columnClause = function + "(" + columnName + ")";
					}
				}
				else
				{
					columnClause = columnName;
				}
				builder.append(columnClause).append(" ").append(mInfoColumn.getQueryOperator()).append(" ?");
			}
		}
		if (count > 0 && !checkAND.isChecked())
		{
			builder.append(" ) ");
		}
		String sql = builder.toString();
		if (sql.indexOf("@") >= 0)
		{
			sql = Env.parseContext(infoContext, p_WindowNo, sql, true, true);
		}

		prevWhereClause = sql;

		return sql;
	}

	protected MInfoColumn findInfoColumn(GridField gridField)
	{
		for (int i = 0; i < gridFields.size(); i++)
		{
			if (gridFields.get(i) == gridField)
			{
				return infoColumns[i];
			}
		}
		return null;
	}

	/**
	 * Check has new parameter is change or new input in case first time search,
	 * return true
	 * 
	 * @return
	 */
	protected boolean isParameterChangeValue()
	{
		if (prevParameterValues == null)
		{
			// never process query, because consider as changed value to process
			// current search
			return true;
		}

		// compare old and new value of parameter input at prev time
		for (int parameterIndex = 0; parameterIndex < prevParameterValues.size(); parameterIndex++)
		{
			Object newValue = prevRefParmeterEditor.get(parameterIndex).getValue();
			if (!prevParameterValues.get(parameterIndex).equals(newValue))
			{
				return true;
			}
		}

		// in case new field is entered value
		for (WEditor editor : editors)
		{
			if (!editor.isVisible() || prevRefParmeterEditor.contains(editor))
				continue;

			if (editor.getGridField() != null && editor.getValue() != null
					&& editor.getValue().toString().trim().length() > 0)
			{
				MInfoColumn mInfoColumn = findInfoColumn(editor.getGridField());
				if (mInfoColumn == null || mInfoColumn.getSelectClause().equals(MInfoColumn.SELECT_VARIABLE_ONLY))
				{
					continue;
				}
				return true;
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.adempiere.webui.panel.InfoPanel#setParameters(java.sql.
	 * PreparedStatement, boolean)
	 */
	@Override
	protected void setParameters(PreparedStatement pstmt, boolean forCount) throws SQLException
	{
		// when query not by click requery button, reuse parameter value
		if (!isQueryByUser && prevParameterValues != null)
		{
			for (int parameterIndex = 0; parameterIndex < prevParameterValues.size(); parameterIndex++)
			{
				setParameter(pstmt, parameterIndex + 1, prevParameterValues.get(parameterIndex),
						prevQueryOperators.get(parameterIndex));
			}
			return;
		}

		// init collection to save current parameter value
		if (prevParameterValues == null)
		{
			prevParameterValues = new ArrayList<Object>();
			prevQueryOperators = new ArrayList<String>();
			prevRefParmeterEditor = new ArrayList<WEditor>();
		}
		else
		{
			prevParameterValues.clear();
			prevQueryOperators.clear();
			prevRefParmeterEditor.clear();
		}

		int parameterIndex = 0;
		for (WEditor editor : editors)
		{
			if (!editor.isVisible())
				continue;

			if (editor.getGridField() != null && editor.getValue() != null
					&& editor.getValue().toString().trim().length() > 0)
			{
				MInfoColumn mInfoColumn = findInfoColumn(editor.getGridField());
				if (mInfoColumn == null || mInfoColumn.getSelectClause().equals(MInfoColumn.SELECT_VARIABLE_ONLY))
				{
					continue;
				}
				Object value = editor.getValue();
				parameterIndex++;
				prevParameterValues.add(value);
				prevQueryOperators.add(mInfoColumn.getQueryOperator());
				prevRefParmeterEditor.add(editor);
				setParameter(pstmt, parameterIndex, value, mInfoColumn.getQueryOperator());
			}
		}

	}

	/**
	 * set parameter for statement. not need check null for value
	 * 
	 * @param pstmt
	 * @param parameterIndex
	 * @param value
	 * @param queryOperator
	 * @throws SQLException
	 */
	protected void setParameter(PreparedStatement pstmt, int parameterIndex, Object value, String queryOperator)
			throws SQLException
	{
		if (value instanceof Boolean)
		{
			pstmt.setString(parameterIndex, ((Boolean) value).booleanValue() ? "Y" : "N");
		}
		else if (value instanceof String)
		{
			if (queryOperator.equals(X_AD_InfoColumn.QUERYOPERATOR_Like))
			{
				StringBuilder valueStr = new StringBuilder(value.toString().toUpperCase());
				if (!valueStr.toString().endsWith("%"))
					valueStr.append("%");
				pstmt.setString(parameterIndex, valueStr.toString());
			}
			else
			{
				pstmt.setString(parameterIndex, (String) value);
			}
		}
		else
		{
			pstmt.setObject(parameterIndex, value);
		}
	}

	@Override
	protected void prepareTable(ColumnInfo[] layout, String from, String where, String orderBy)
	{
		super.prepareTable(layout, from, where, orderBy);
		if (m_sqlMain.indexOf("@") >= 0)
		{
			String sql = Env.parseContext(infoContext, p_WindowNo, m_sqlMain, true);
			if (sql == null || sql.length() == 0)
			{
				log.severe("Failed to parsed sql. sql=" + m_sqlMain);
			}
			else
			{
				m_sqlMain = sql;
			}
		}

		addViewIDToQuery();

		if (m_sqlMain.length() > 0 && infoWindow.isDistinct())
		{
			m_sqlMain = m_sqlMain.substring("SELECT ".length());
			m_sqlMain = "SELECT DISTINCT " + m_sqlMain;
		}

		if (m_sqlOrder != null && m_sqlOrder.indexOf("@") >= 0)
		{
			String sql = Env.parseContext(infoContext, p_WindowNo, m_sqlOrder, true, false);
			if (sql == null || sql.length() == 0)
			{
				log.severe("Failed to parsed sql. sql=" + m_sqlOrder);
			}
			else
			{
				m_sqlOrder = sql;
			}
		}
	}

	/**
	 * add all ViewID in each MInfoProcess to query if main query have subquery
	 * in SELECT, it will beak or incorrect
	 */
	protected void addViewIDToQuery()
	{

		if (m_sqlMain.length() > 0 && infoProcessList != null && infoProcessList.length > 0)
		{
			int fromIndex = m_sqlMain.indexOf("FROM");
			// split Select and from clause
			String selectClause = m_sqlMain.substring(0, fromIndex);
			String fromClause = m_sqlMain.substring(fromIndex);

			// get alias of main table
			StringBuilder sqlBuilder = new StringBuilder(selectClause);

			// reset flag relate viewID to recount
			numOfViewID = 0;
			isHasViewID = false;

			// add View_ID column to select clause
			for (MInfoProcess infoProcess : infoProcessList)
			{
				// this process hasn't viewID column, next other infoProcess
				if (infoProcess.getAD_InfoColumn_ID() <= 0)
					continue;

				MInfoColumn infocol = (MInfoColumn) infoProcess.getAD_InfoColumn();
				// maintain variable relate to ViewID, it can init just one time
				// when load infoWindow define but let it here for simple logic
				// control
				numOfViewID++;
				isHasViewID = true;

				if (!infocol.isDisplayed())
				{
					// add column to SELECT clause of main sql
					sqlBuilder.append(", ");
					sqlBuilder.append(infocol.getSelectClause());
					sqlBuilder.append(" AS ");
					sqlBuilder.append(infocol.getColumnName());
					sqlBuilder.append(" ");
				}
			}

			sqlBuilder.append(fromClause);
			m_sqlMain = sqlBuilder.toString();
		}
	}

	protected void renderWindow()
	{
		setTitle(infoWindow.get_Translation("Name"));
		layout = new Borderlayout();
		ZKUpdateUtil.setWidth(layout, "100%");
		ZKUpdateUtil.setWidth(layout, "100%");
		if (!isModal())
		{
			layout.setStyle("position: absolute");
		}

		this.appendChild(layout);

		if (isModal())
			ZKUpdateUtil.setWidth(contentPanel, "99%");
		else
			contentPanel.setStyle("width: 99%; margin: 0px auto;");
		ZKUpdateUtil.setVflex(contentPanel, true);

		if (hasTree) {
			int width = SessionManager.getAppDesktop().getClientInfo().desktopWidth * 20 / 100;
			West west = new West();
			west.setCollapsible(true);
			west.setSplittable(true);
			west.setAutoscroll(true);	
			west.setTitle("Tree");
			ZKUpdateUtil.setWidth(west, width + "px");
			layout.appendChild(west);
			renderTree(west);
		}
		
		North north = new North();
        north.setCollapsible(true);
        north.setSplittable(true);
        north.setAutoscroll(true);	
        north.setTitle(Msg.translate(Env.getCtx(), "Parameter"));
		layout.appendChild(north);
		renderParameterPane(north);

		Center center = new Center();
		layout.appendChild(center);
		renderContentPane(center);

		South south = new South();
		layout.appendChild(south);
		renderFooter(south);

		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(hasZoom());
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setDisabled(true);

		// Start when init all button process is disable because no record is
		// selected

		for (Button btProcess : btProcessList)
		{
			boolean isAlwaysOn = (boolean) btProcess.getAttribute(ATT_BTN_ALWAYS_ON);
			if (!isAlwaysOn) {
				btProcess.setDisabled(true);
			}
		}
		if (btCbbProcess != null)
		{
			btCbbProcess.setDisabled(true);
		}

		if (btMenuProcess != null)
		{
			btMenuProcess.setDisabled(true);
		}

		if (cbbProcess != null)
		{
			cbbProcess.setDisabled(true);
		}
	}

	protected void renderFooter(South south)
	{
		southBody = new Vlayout();
		south.appendChild(southBody);
		southBody.appendChild(new Separator());
		southBody.appendChild(confirmPanel);
		southBody.appendChild(statusBar);
		ZKUpdateUtil.setWidth(southBody, "100%");
	}

	protected void insertPagingComponent()
	{
		southBody.insertBefore(paging, southBody.getFirstChild());
		layout.invalidate();
	}

	protected void renderContentPane(Center center)
	{
		Div div = new Div();
		div.setStyle("width :100%; height: 100%");
		div.appendChild(contentPanel);

		Borderlayout inner = new Borderlayout();
		ZKUpdateUtil.setWidth(inner, "100%");
		ZKUpdateUtil.setHeight(inner, "100%");
		int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 90 / 100;
		if (isLookup() || m_Record_ID > 0)
			inner.setStyle("border: none; position: relative; ");
		else
			inner.setStyle("border: none; position: absolute; ");
		inner.appendCenter(div);
		// true will conflict with listbox scrolling
		inner.getCenter().setAutoscroll(false);

		if (embeddedWinList.size() > 0)
		{
			South south = new South();
			int detailHeight = (height * 25 / 100);
			ZKUpdateUtil.setHeight(south, detailHeight + "px");
			south.setAutoscroll(true);
			south.setCollapsible(true);
			south.setSplittable(true);
			south.setTitle(Msg.translate(Env.getCtx(), "Related Information"));
			south.setTooltiptext(Msg.translate(Env.getCtx(), "Related Information"));

			// south.addEventListener(Events.ON_SWIPE, new
			// EventListener<SwipeEvent>() {
			// @Override
			// public void onEvent(SwipeEvent event) throws Exception {
			// South south = (South) event.getTarget();
			// if ("down".equals(event.getSwipeDirection())) {
			// south.setOpen(false);
			// }
			// }
			// });
			south.setSclass("south-collapsible-with-title");
			south.setAutoscroll(true);

			embeddedPane.setSclass("info-product-tabbedpane");
			south.appendChild(embeddedPane);
			inner.appendChild(south);

		} // render embedded

		center.appendChild(inner);
	}

	protected void renderTree(West west) {
		m_Tree = new Tree();
		m_Tree.setMultiple(true);
		m_Tree.setCheckmark(true);
		m_Tree.setNonselectableTags("*");
		m_Tree.addEventListener(Events.ON_SELECT, this);
//		m_Tree.addEventListener("onAfterRender", this);
		Panel vbox = new Panel();
		ZKUpdateUtil.setVflex(vbox, "1");
		ZKUpdateUtil.setHflex(vbox, "1");
		
		Panelchildren pc = new Panelchildren();
		Toolbar toolbar = new Toolbar();
        toolbar = new Toolbar();
        chkExpand = new Checkbox();
        chkExpand.setText(Msg.getMsg(Env.getCtx(), "ExpandTree"));
        chkExpand.addEventListener(Events.ON_CHECK, this);
        toolbar.appendChild(chkExpand);
        pc.appendChild(m_Tree);
        pc.appendChild(toolbar);
        vbox.appendChild(pc);
        west.appendChild(vbox);
        ZKUpdateUtil.setVflex(m_Tree, true);
        ZKUpdateUtil.setWidth(m_Tree, "100%");

		MColumn keyCol = MColumn.get(Env.getCtx(), m_infoTrees[m_infoTrees.length-1].getAD_Key());
		m_FilterColumn = keyCol.getColumnName();
		m_VisibleID = new ArrayList<>();

//		m_rootPopup = new Menupopup();
//		Menuitem zoom = new Menuitem(Msg.getMsg(Env.getCtx(), "Zoom"), ITheme.TOOLBAR_FOLDER_IMAGE + "Zoom24.png");
//		zoom.addEventListener(Events.ON_CLICK, this);
//		m_rootPopup.appendChild(zoom);
	}
	
	protected void populateTree()
	{
		m_Tree.clear();
        chkExpand.setChecked(true);

		//recursively populate up to the last level
        m_TreeNodes = new HashMap<String, MInfoTreeNode>();
		createNodes(0, null, false);
		Treechildren rootTreeChildren = m_Tree.getTreechildren();
		if (rootTreeChildren == null) {
			rootTreeChildren = new Treechildren();
			m_Tree.appendChild(rootTreeChildren);
		}
		for (MInfoTreeNode node : m_RootNodes) 
		{
			Treeitem rootitem = new Treeitem();
			rootitem.setId(node.getNodeKey());
			rootitem.setLabel(node.getName());
			rootitem.setAttribute(ATT_TREENODELEVEL, 0);
			rootitem.setAttribute(TREE_AD_WINDOW_ID, node.getADWindow_ID());
			rootitem.setAttribute(ATT_TREEITEM_ISREADONLY, node.isReadOnly());
			rootitem.setPopup(m_rootPopup);
			Treecell cell = (Treecell) rootitem.getFirstChild().getFirstChild();
			cell.setStyle("font-size:large;");
			cell.setIconSclass("z-icon-home");
			if (node.getChildCount() > 0) {
				Treechildren treeItemChildren = new Treechildren();
				generateTreeItem(treeItemChildren, node);
				if(treeItemChildren.getChildren().size() != 0) {
	                  rootitem.appendChild(treeItemChildren);
				}
				rootitem.getTreerow().addEventListener(Events.ON_CLICK, this);
			}
			rootTreeChildren.appendChild(rootitem);
		}

		
	}
	
	protected void createNodes(int level, MInfoTreeNode parent, boolean isReadOnly) {
		boolean isRoot = false;
		
		ArrayList<MInfoTreeNode> nodes = new ArrayList<MInfoTreeNode>();
		int[] pkID = null;
		
//		boolean nodeIsReadOnly = isReadOnly;
		MInfoTree tree = m_infoTrees[level];
		if (level == 0) {
			m_RootNodes = new ArrayList<>();
			isRoot = true;
			if (m_TreeRootColumn == null) {
				FDialog.error(0, "Info Window is configured to have tree grouping.  One column must be set as [Tree Root].");
				return;
			}
			pkID = getDistinctIDs(m_TreeRootColumn);
			if (pkID.length == 0) {
				//nothing to load as there is no visible rows
				return;
			}
		}

		MColumn keyCol = MColumn.get(Env.getCtx(), tree.getAD_Key());
		StringBuffer sql = new StringBuffer();
		String alias = null;
		
		AccessSqlParser parser = new AccessSqlParser("SELECT * FROM " + tree.getFromClause());
		TableInfo[] tableInfos = parser.getTableInfo(0);
		boolean hasReadOnlyLogic =  tree.getReadOnlySQL() != null && tree.getReadOnlySQL().length() > 1;
		if (tableInfos.length > 0) {
			alias = tableInfos[0].getSynonym();
		}
		sql.append("SELECT ");
		if (alias != null) {
			sql.append(alias + ".");
		}
		sql.append(keyCol.getColumnName());
		sql.append(", ")
			.append(tree.getDisplaySQL());
		
		if (hasReadOnlyLogic) {
			sql.append(", ")
			.append(tree.getReadOnlySQL());
			
		}
		sql.append(" FROM ");
		sql.append(tree.getFromClause());
		
		if (tree.getWhereClause() != null || parent != null || (pkID != null && pkID.length > 0)) {
			boolean addAnd = false;
			sql.append(" WHERE ");
			if (tree.getWhereClause() != null) {
				sql.append(tree.getWhereClause());
				addAnd = true;
			}
			if (parent != null) {
				if (addAnd) {
					sql.append(" AND ");
				}
				if (alias != null) {
					sql.append(alias + ".");
				}
				sql.append(tree.getParent_Column().getColumnName() + "=" + parent.getID());
			}
			if (isRoot && pkID != null && pkID.length > 0) {
				if (addAnd) {
					sql.append(" AND ");
				}
				if (alias != null) {
					sql.append(alias + ".");
				}
				sql.append(keyCol.getColumnName() + " IN (");
				sql.append(StringUtils.join(ArrayUtils.toObject(pkID), ", "));
				sql.append(")");
			} 
		}
		if (level > 0) {
			//TODO jtrinidad- check exists statement
			// append EXISTS statement to exclude
			StringBuffer existSQL  = new StringBuffer(" EXISTS (SELECT 1 FROM ");
			existSQL.append(infoWindow.getFromClause() + " ");
			existSQL.append("WHERE ");
			existSQL.append(p_whereClause);
			existSQL.append(getSQLWhere() + " AND ");
			existSQL.append(alias + "." + keyCol.getColumnName() +  "=");
			MInfoColumn colSearch = null;
			for (MInfoColumn col :  infoColumns) {
				if (col.getColumnName().equals(keyCol.getColumnName())) {
					colSearch = col;
					break;
				}
			}
			existSQL.append(colSearch.getSelectClause() + ")");
			sql.append(" AND ");
			sql.append(existSQL.toString());
		}
		
		String orderBy = tree.getOrderByClause();
		
		if (orderBy != null && orderBy.length() > 0) {
			sql.append(" ORDER BY " + orderBy);
		}
		
		String finalSQL =  Env.parseContext(infoContext, p_WindowNo, sql.toString(), true, false);
		PreparedStatement pstmt = DB.prepareStatement( finalSQL, null);
        ResultSet rs = null;
        try {
        	if (level > 0) {
        		setParameters(pstmt, false); // no count
        	}
        	rs = pstmt.executeQuery();
        	while (rs.next())
        	{
        		boolean nodeIsReadOnly = isReadOnly;
        		int id = rs.getInt(1);
        		String label = rs.getString(2);
        		if (hasReadOnlyLogic) {
        			nodeIsReadOnly = rs.getString(3).equalsIgnoreCase("Y");
        		}
        		String nodeKey = null;
        		if (parent != null) {
        			nodeKey = parent.getNodeKey();
        		}
				MInfoTreeNode node = new MInfoTreeNode(label, label, nodeKey, tree.getAD_Table().getTableName()
        				, id, level, tree.getParent_Column().getColumnName(), nodeIsReadOnly);
        		if (tree.getAD_Window_ID() != 0) {
        			node.setADWindowID(tree.getAD_Window_ID());
        		}
        		
        		if (isRoot) {
        			m_RootNodes.add(node);
        		} else {
        			parent.add(node);
        		}
        		m_TreeNodes.put(node.getNodeKey(), node);
        		nodes.add(node);
        	}
        	
        } catch (Exception e) {
        	log.severe(e.getMessage());
        	return;
        }
        finally
        {
            DB.close(rs, pstmt);
            rs= null;
            pstmt = null;
        }
		
        if (level+1 < m_infoTrees.length) {
        	for (MInfoTreeNode node : nodes) {
        		createNodes(level+1, node, node.isReadOnly());
        	}
        	
        }
        return;
	}

	
	protected void generateTreeItem(Treechildren treeChildren, MInfoTreeNode node)
	{
		Enumeration<?> nodeEnum = node.children();
		
		while (nodeEnum.hasMoreElements()) {
			MInfoTreeNode child = (MInfoTreeNode) nodeEnum.nextElement();
			Treeitem treeitem = new Treeitem();
			treeitem.setId(child.getNodeKey());
			treeitem.setLabel(child.getName());
			treeitem.setAttribute(ATT_TREENODELEVEL, child.getNodeLevel());
			treeitem.setAttribute(TREE_AD_WINDOW_ID, child.getADWindow_ID());
			treeitem.setAttribute(ATT_TREEITEM_ISREADONLY, node.isReadOnly());

			if (child.getChildCount() > 0) {
                Treechildren treeItemChildren = new Treechildren();
                generateTreeItem(treeItemChildren, child);
                if(treeItemChildren.getChildren().size() != 0)
                    treeitem.appendChild(treeItemChildren);
                
			}
			treeitem.getTreerow().addEventListener(Events.ON_CLICK, this);

			treeChildren.appendChild(treeitem);
		}
	}
	//protected KeyNamePair[] 
	protected int[] getDistinctIDs(MInfoColumn column) {
		if (column == null)
			return null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT(")
			.append(column.getSelectClause())
			.append(") FROM ")
			.append(infoWindow.getFromClause());
			
		if (editors != null) {
			sql.append(" WHERE ");
			sql.append(p_whereClause);
			sql.append(getSQLWhere());
			
		}
		
		
		String finalSQL = MRole.getDefault().addAccessSQL(sql.toString(), getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);

		PreparedStatement pstmt = DB.prepareStatement( finalSQL, null);
        ResultSet rs = null;
        ArrayList<Integer> list = new ArrayList<Integer>();
		
		try {
			
			setParameters(pstmt, false);
            rs = pstmt.executeQuery();
            while (rs.next())
            {
                list.add(rs.getInt(1));
            }
		} catch (SQLException e) {
			log.severe(e.getMessage());
		}
        finally
        {
            DB.close(rs, pstmt);
            rs= null;
            pstmt = null;
        }
		
		//	Convert to array
		int[] retValue = new int[list.size()];
		for (int i = 0; i < retValue.length; i++)
		{
			retValue[i] = list.get(i);
		}
        return retValue;
	}
	
	protected void renderParameterPane(North north)
	{
		createParameterPanel();
		north.appendChild(parameterGrid);
	}

	protected void createParameterPanel()
	{
		parameterGrid = GridFactory.newGridLayout();
		parameterGrid.setStyle("width: 95%; margin: auto !important;");
		Columns columns = new Columns();
		parameterGrid.appendChild(columns);
		for (int i = 0; i < 6; i++)
			columns.appendChild(new Column());

		Column column = new Column();
		ZKUpdateUtil.setWidth(column, "100px");
		column.setAlign("right");
		columns.appendChild(column);

		Rows rows = new Rows();
		parameterGrid.appendChild(rows);

		editors = new ArrayList<WEditor>();
		identifiers = new ArrayList<WEditor>();
		TreeMap<Integer, List<Object[]>> tree = new TreeMap<Integer, List<Object[]>>();
		for (int i = 0; i < infoColumns.length; i++)
		{
			if (infoColumns[i].isQueryCriteria() || infoColumns[i].isVariableOnly() )
			{
				List<Object[]> list = tree.get(infoColumns[i].getSeqNoSelection());
				if (list == null)
				{
					list = new ArrayList<Object[]>();
					tree.put(infoColumns[i].getSeqNoSelection(), list);
				}
				list.add(new Object[] { infoColumns[i], gridFields.get(i) });
			}
		}

		for (Integer i : tree.keySet())
		{
			List<Object[]> list = tree.get(i);
			for (Object[] value : list)
			{
				addSelectionColumn((MInfoColumn) value[0], (GridField) value[1]);
			}
		}

		if (checkAND == null)
		{
			if (parameterGrid.getRows() != null && parameterGrid.getRows().getFirstChild() != null)
			{
				Row row = (Row) parameterGrid.getRows().getFirstChild();
				int col = row.getChildren().size();
				while (col < 6)
				{
					row.appendChild(new Space());
					col++;
				}
				createAndCheckbox();
				row.appendChild(checkAND);
			}
		}
		evalDisplayLogic();
		initParameters();
		dynamicDisplay(null);
	}

	protected void evalDisplayLogic()
	{
		for (WEditor editor : editors)
		{
			if (editor.getGridField() != null && !editor.getGridField().isDisplayed(true))
			{
				editor.getComponent().setVisible(false);
				if (editor.getLabel() != null)
					editor.getLabel().setVisible(false);
			}
			else if (!editor.getComponent().isVisible())
			{
				editor.getComponent().setVisible(true);
				if (editor.getLabel() != null)
					editor.getLabel().setVisible(true);
			}
		}
	}

	/**
	 * Add Selection Column to first Tab
	 * 
	 * @param infoColumn
	 * @param mField field
	 **/
	protected void addSelectionColumn(MInfoColumn infoColumn, GridField mField)
	{
		int displayLength = mField.getDisplayLength();
		if (displayLength <= 0 || displayLength > FIELDLENGTH)
			mField.setDisplayLength(FIELDLENGTH);
		else
			displayLength = 0;

		WEditor editor = null;
		// if (mField.getDisplayType() == DisplayType.PAttribute)
		// {
		// editor = new WInfoPAttributeEditor(infoContext, p_WindowNo, mField);
		// editor.setReadWrite(true);
		// }
		// else
		{
			editor = WebEditorFactory.getEditor(mField, false);
			editor.setReadWrite(true);
			editor.dynamicDisplay();
			editor.addValueChangeListener(this);
			editor.fillHorizontal();
		}
		Label label = editor.getLabel();
		Component fieldEditor = editor.getComponent();

		//
		if (displayLength > 0) // set it back
			mField.setDisplayLength(displayLength);
		//
		if (label != null)
		{
			if (infoColumn.getQueryOperator().equals(X_AD_InfoColumn.QUERYOPERATOR_Gt)
					|| infoColumn.getQueryOperator().equals(X_AD_InfoColumn.QUERYOPERATOR_GtEq)
					|| infoColumn.getQueryOperator().equals(X_AD_InfoColumn.QUERYOPERATOR_Le)
					|| infoColumn.getQueryOperator().equals(X_AD_InfoColumn.QUERYOPERATOR_LeEq)
					|| infoColumn.getQueryOperator().equals(X_AD_InfoColumn.QUERYOPERATOR_NotEq))
			{
				label.setValue(label.getValue() + " " + infoColumn.getQueryOperator());
			}
		}

		addSearchParameter(label, fieldEditor);
		editor.fillHorizontal();

		editors.add(editor);
		if (infoColumn.isIdentifier())
		{
			identifiers.add(editor);
		}

		fieldEditor.addEventListener(Events.ON_OK, this);

		mField.addPropertyChangeListener(editor);

		if (!Util.isEmpty(mField.getVO().DefaultValue, true))
		{
			// set default value
			Object editorValue = mField.getDefault(null);
			mField.setValue(editorValue, true);
			//TODO - update context
			ValueChangeEvent changeEvent = new ValueChangeEvent(editor, "", null, editorValue);
			valueChange(changeEvent);
			m_DefaultParameters.put(editor.getColumnName(), editorValue);
		}
	} // addSelectionColumn

	protected void addSearchParameter(Label label, Component fieldEditor)
	{
		Row panel = null;
		if (parameterGrid.getRows().getChildren().isEmpty())
		{
			panel = new Row();
			parameterGrid.getRows().appendChild(panel);
		}
		else
		{
			panel = (Row) parameterGrid.getRows().getLastChild();
			if (panel.getChildren().size() == 6)
			{
				if (parameterGrid.getRows().getChildren().size() == 1)
				{
					createAndCheckbox();
					panel.appendChild(checkAND);
				}
				else
				{
					panel.appendChild(new Space());
				}
				panel = new Row();
				parameterGrid.getRows().appendChild(panel);
			}
		}
		if (!(fieldEditor instanceof Checkbox))
		{
			Div div = new Div();
			div.setStyle("text-align: right;");
			div.appendChild(label);
			if (label.getDecorator() != null)
			{
				div.appendChild(label.getDecorator());
			}
			panel.appendCellChild(div);
		}
		else
		{
			panel.appendChild(new Space());
		}
		panel.appendCellChild(fieldEditor, 2);
	} // addSearchParameter

	protected void createAndCheckbox()
	{
		checkAND = new Checkbox();
		checkAND.setLabel(Msg.getMsg(Env.getCtx(), "SearchAND", true));
		String tips = Msg.getMsg(Env.getCtx(), "SearchAND", false);
		if (!Util.isEmpty(tips))
		{
			checkAND.setTooltiptext(tips);
		}
		checkAND.setChecked(true);
		checkAND.addEventListener(Events.ON_CHECK, this);
	} // createAndCheckbox

	protected int findColumnIndex(String columnName)
	{
		for (int i = 0; i < columnInfos.length; i++)
		{
			GridField field = columnInfos[i].getGridField();
			if (field != null && field.getColumnName().equalsIgnoreCase(columnName))
			{
				return i;
			}
		}
		return -1;
	} // findColumnIndex

	/**
	 * Save Selection Details Get Location/Partner Info
	 */
	@Override
	protected void saveSelectionDetail()
	{
		int row = contentPanel.getSelectedRow();
		if (row == -1)
			return;

		int column = -1;
		for (ColumnInfo columnInfo : columnInfos)
		{
			column++;
			GridField field = columnInfo.getGridField();
			if (field == null)
				continue;

			String columnName = field.getColumnName();
			if (columnInfo.getColClass().equals(KeyNamePair.class))
			{
				KeyNamePair knp = (KeyNamePair) contentPanel.getValueAt(row, column);
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, columnName, knp == null ? "0" : knp.getID());
			}
			else if (columnName.endsWith("_ID"))
			{
				Object id = contentPanel.getValueAt(row, column);
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, columnName, id == null ? "0" : id.toString());
			}
			else
			{
				Object value = contentPanel.getValueAt(row, column);
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, field.getColumnName(),
						value == null ? "" : value.toString());
			}
		}
	} // saveSelectionDetail

	@Override
	protected String buildDataSQL(int start, int end)
	{
		String dataSql;
		String dynWhere = getSQLWhere();
		StringBuilder sql = new StringBuilder(m_sqlMain);
		if (dynWhere.length() > 0)
			sql.append(dynWhere); // includes first AND

		if (sql.toString().trim().endsWith("WHERE"))
		{
			int index = sql.lastIndexOf(" WHERE");
			sql.delete(index, sql.length());
		}

		//why do you need to translate variable? - jtrinidad 27032024
//		dataSql = Msg.parseTranslation(Env.getCtx(), sql.toString()); // Variables
		dataSql = Env.parseContext(infoContext, p_WindowNo, sql.toString(), true, false);
		dataSql = MRole.getDefault().addAccessSQL(dataSql, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);

		if (infoWindow.getOtherClause() != null && infoWindow.getOtherClause().trim().length() > 0)
		{
			String otherClause = infoWindow.getOtherClause();
			if (otherClause.indexOf("@") >= 0)
			{
				String s = Env.parseContext(infoContext, p_WindowNo, otherClause, true, false);
				if (s.length() == 0)
				{
					log.severe("Failed to parse other clause. " + otherClause);
				}
				else
				{
					otherClause = s;
				}
			}
			dataSql = dataSql + " " + otherClause;
		}

		if (m_sqlUserOrder != null && m_sqlUserOrder.trim().length() > 0)
			dataSql = dataSql + m_sqlUserOrder;
		else
			dataSql = dataSql + m_sqlOrder;

		if (end > start && isUseDatabasePaging() && DB.getDatabase().isPagingSupported())
		{
			dataSql = DB.getDatabase().addPagingSQL(dataSql, getCacheStart(), getCacheEnd());
		}
		return dataSql;
	} // buildDataSQL

	@Override
	protected void executeQuery()
	{
    	if (!isRequeryByRunSuccessProcess)
    		prepareTable();
		super.executeQuery();
		
		if (isQueryByUser && hasTree) {
			populateTree();
			if (m_ParentColumn == null) {
				throw new AdempiereException("Must set one Info Column as Parent Link Column for Tree to work.");
			}
			m_ParentColumnIdx = findColumnIndex(m_ParentColumn.getColumnName());

		}
	}

	@Override
	protected boolean hasReset() {
		return true;
	};

	@Override
	protected boolean hasZoom()
	{
		return !isLookup() && infoWindow != null && !MTable.get(Env.getCtx(), infoWindow.getAD_Table_ID()).isView();
	}

	@Override
	public void valueChange(ValueChangeEvent evt)
	{
		if (evt != null && evt.getSource() instanceof WEditor)
		{
			WEditor editor = (WEditor) evt.getSource();
			if (evt.getNewValue() == null)
			{
				Env.setContext(infoContext, p_WindowNo, editor.getColumnName(), "");
				Env.setContext(infoContext, p_WindowNo, Env.TAB_INFO, editor.getColumnName(), "");
				
				Env.setContext(Env.getCtx(), p_WindowNo, editor.getColumnName(), "");
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, editor.getColumnName(), "");
			}
			else if (evt.getNewValue() instanceof Boolean)
			{
				Env.setContext(infoContext, p_WindowNo, editor.getColumnName(), (Boolean) evt.getNewValue());
				Env.setContext(infoContext, p_WindowNo, Env.TAB_INFO, editor.getColumnName(),
						(Boolean) evt.getNewValue());
				
				Env.setContext(Env.getCtx(), p_WindowNo, editor.getColumnName(), (Boolean) evt.getNewValue());
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, editor.getColumnName(),
						(Boolean) evt.getNewValue());
			}
			else if (evt.getNewValue() instanceof Timestamp)
			{
				Env.setContext(infoContext, p_WindowNo, editor.getColumnName(), (Timestamp) evt.getNewValue());
				Env.setContext(infoContext, p_WindowNo, Env.TAB_INFO + "|" + editor.getColumnName(),
						(Timestamp) evt.getNewValue());

				Env.setContext(Env.getCtx(), p_WindowNo, editor.getColumnName(), (Timestamp) evt.getNewValue());
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO + "|" + editor.getColumnName(),
						(Timestamp) evt.getNewValue());
			}
			else
			{
				Env.setContext(infoContext, p_WindowNo, editor.getColumnName(), evt.getNewValue().toString());
				Env.setContext(infoContext, p_WindowNo, Env.TAB_INFO, editor.getColumnName(),
						evt.getNewValue().toString());
				
				Env.setContext(Env.getCtx(), p_WindowNo, editor.getColumnName(), evt.getNewValue().toString());
				Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, editor.getColumnName(),
						evt.getNewValue().toString());
			}
			dynamicDisplay(editor);
		}

	} // valueChange

	protected void dynamicDisplay(WEditor editor)
	{
		validateField(editor);
		// if attribute set changed (from any value to any value) clear the
		// attribute set instance m_pAttributeWhere
		// boolean asiChanged = false;
		// if (editor != null && editor instanceof WTableDirEditor &&
		// editor.getColumnName().equals("M_AttributeSet_ID"))
		// asiChanged = true;

		for (WEditor otherEditor : editors)
		{
			if (otherEditor == editor)
				continue;

			// // reset value of WInfoPAttributeEditor to null when change
			// M_AttributeSet_ID
			// if (asiChanged && otherEditor instanceof WInfoPAttributeEditor)
			// ((WInfoPAttributeEditor)otherEditor).clearWhereClause();

			otherEditor.dynamicDisplay();
		}

		evalDisplayLogic();

	} // dynamicDisplay

	public void onEvent(Event event)
	{
//		log.severe("EVENT DEBUG NAME=" + event.getName());
//		if (event.getName().equals("onAfterRender") && !(event.getTarget() instanceof WListbox)) {
//			log.severe("not listbox");
//		}
		if (event.getName().equals(Events.ON_OK) && event.getTarget() != null)
		{ // event when push enter at parameter
			Component tagetComponent = event.getTarget();
			boolean isCacheEvent = false;// when event from parameter component,
											// catch it and process at there
			for (WEditor editor : editors)
			{
				Component editorComponent = editor.getComponent();
				if (editorComponent instanceof EditorBox)
				{
					editorComponent = ((EditorBox) editorComponent).getTextbox();
				}
				if (editorComponent.equals(tagetComponent))
				{
					if (editor instanceof WSearchEditor)
					{
						if (((WSearchEditor) editor).isShowingDialog())
						{
							return;
						}
					}
					isCacheEvent = true;
					break;
				}
			}

			if (isCacheEvent)
			{
				boolean isParameterChange = isParameterChangeValue();
				// when change parameter, also requery
				if (isParameterChange)
				{
					onUserQuery();
				}
				else if (m_lookup && contentPanel.getSelectedIndex() >= 0)
				{
					// do nothing when parameter not change and at window mode,
					// or at dialog mode but select non record
					onOk();
				}
				else
				{
					// parameter not change. do nothing.
				}
			}
			else
			{
				super.onEvent(event);
			}
		} else if (event.getName().equals(Events.ON_CLICK) && event.getTarget() instanceof Treerow) {
			onTreeItemClicked(event);
			
		} else if (event.getName().equals(Events.ON_CLICK) && event.getTarget() instanceof Menuitem) {
			onTreeItemMenuClick(event);
			
		} else if (event.getTarget() == m_Tree && event.getName().equals(Events.ON_SELECT)) {
			onTreeItemSelected(event);
			super.onEvent(event);
		} else if (event.getTarget() == m_Tree && event.getName().equals("onAfterRender")) {
			log.severe("EVENT DEBUG NAME=ONAFTER RENDER m_tree");
//			filterRowsFromTree();
			
		} else if (event.getName().equals(Events.ON_CHECK) && event.getTarget() == chkExpand)
        {
        	expandOnCheck();
        }
		else
		{
			super.onEvent(event);
		}
	}

	private void onTreeItemSelected(Event event) {
		//(SelectEvent<Treeitem, String>) event
		@SuppressWarnings("unchecked")
		SelectEvent<Treeitem, String> selEvt = (SelectEvent<Treeitem, String>) event;
		Set<Treeitem> items = selEvt.getUnselectedItems();
		
		for (Treeitem item : items) {
			updateChildren(item.getChildren(), false);
			if (item.getChildren().size() == 1) {
				String[] a = item.getId().split("-");
				m_VisibleID.remove((Object)Integer.parseInt(a[1]));
			}
			m_TreeSelected = true;
		}
		if (items.size() == 0) {
			Treeitem item = selEvt.getReference();
			if (item.isSelected()) {
				updateChildren(item.getChildren(), true);
				if (item.getChildren().size() == 1) {
					String[] a = item.getId().split("-");
					m_VisibleID.add(Integer.parseInt(a[1]));
				}
				m_TreeSelected = true;
			}
		}
		if (!m_IsTreeFilterOnDB) {
			filterRowsFromTree();
		} else {
			populateFromTreeFilter();
		}
		enableButtons();
	}

	private void onTreeItemClicked(Event event) {
		if (m_TreeSelected) {
			m_TreeSelected = false;
			return;
		}
		Treerow row = (Treerow) event.getTarget();
		Treeitem item = (Treeitem) row.getParent();
		int windowID = (int) item.getAttribute(TREE_AD_WINDOW_ID);
		boolean isReadOnly = (boolean) item.getAttribute(ATT_TREEITEM_ISREADONLY);
		int level = (int)item.getAttribute(ATT_TREENODELEVEL);
		MInfoTree infoTree = m_infoTrees[level];
		m_rootPopup = new Menupopup();
		
		Menuitem menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), "Zoom"), ThemeUtils.resolveImageURL( "Zoom24.png"));
		menuItem.addEventListener(Events.ON_CLICK, this);
		m_rootPopup.appendChild(menuItem);
		menuItem.setValue(item.getId());
		menuItem.setAttribute(ATT_MENUACTION, TREE_MENUACTION_ZOOM);
		menuItem.setAttribute(TREE_AD_WINDOW_ID, windowID);

		if (!isReadOnly) {
			menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), "Delete"), ThemeUtils.resolveImageURL( "Delete24.png"));
			menuItem.addEventListener(Events.ON_CLICK, this);
			m_rootPopup.appendChild(menuItem);
			menuItem.setValue(item.getId());
			menuItem.setAttribute(ATT_MENUACTION, TREE_MENUACTION_DELETE);
			menuItem.setAttribute(ATT_TREEITEM, item);
			
			if (infoTree.getAD_ColumnSortOrder_ID() != 0 && 
					item.getChildren().size() > 1 && ((Treechildren)item.getLastChild()).getChildren().size() > 1) {
				//TODO get Attribute of first child for sequence
				menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), "Sort"));
				menuItem.setIconSclass(Env.getZKFontIcon("sort"));
				menuItem.addEventListener(Events.ON_CLICK, this);
				m_rootPopup.appendChild(menuItem);
				menuItem.setValue(item.getId());
				menuItem.setAttribute(ATT_MENUACTION, TREE_MENUACTION_SORT);
				menuItem.setAttribute(ATT_TREEITEM_SEQCOL, infoTree.getAD_ColumnSortOrder().getColumnName());
				menuItem.setAttribute(ATT_TREEITEM, item);
			}
			
			Menupopup moveTo = getDestinations(item);
			if (moveTo != null && moveTo.getChildren().size() > 0) {
				Menu menu = new Menu();
				menu.setLabel("Move to");
				menu.setIconSclass(Env.getZKFontIcon("arrow-right"));
				menu.appendChild(moveTo);
				
				m_rootPopup.appendChild(menu);
			}
		}


		
		m_rootPopup.setPage(m_Tree.getPage());
		m_rootPopup.open(item.getTreerow(), "at_pointer");
		
	}

	private void onTreeItemMenuClick(Event event) {
		Menuitem item = (Menuitem) event.getTarget();
		String menuAction = (String) item.getAttribute(ATT_MENUACTION);
		
		boolean treeUpdated = false;
		String[] a = item.getValue().split("-");
		int Record_ID = Integer.parseInt(a[1]);
		if (menuAction.equals(TREE_MENUACTION_ZOOM)) {
			int windowID = (int) item.getAttribute(TREE_AD_WINDOW_ID);
			if (windowID == 0) {
				int AD_Table_ID = MTable.getTable_ID(a[0]);
				AEnv.zoom(AD_Table_ID, Record_ID);
			} else {
				MQuery query = new MQuery();
				//MQuery query = MQuery.getEqualQuery(MTable.getTableName(Env.getCtx(), AD_Table_ID) + "_ID", Record_ID);
				query.setZoomValue(Record_ID);
				query.setTableName(a[0]);
				query.setZoomColumnName(a[0] + "_ID");
				AEnv.zoom(windowID, query);
			}
		} else if (menuAction.equals(TREE_MENUACTION_SORT)) {
			Treeitem treeItem = (Treeitem) item.getAttribute(ATT_TREEITEM);
			if (treeItem.getChildren().size() == 1)
				return;
			int AD_Table_ID = 0;
			LinkedHashMap<Integer, String> treeItems = new LinkedHashMap<>();
			
			int targetLevel = (int)treeItem.getAttribute(ATT_TREENODELEVEL) + 1;
			for ( Component comp : treeItem.getChildren()) {
				
				if (comp instanceof Treechildren) {
					Treechildren children = (Treechildren) comp;
					Collection<Treeitem> nodes = children.getItems();
					for (Treeitem node : nodes) {
						int level = (int)node.getAttribute(ATT_TREENODELEVEL);
						if (level > targetLevel) {
							continue;
						}
						
						String[] b = node.getId().split("-");
						treeItems.put(Integer.parseInt(b[1]), node.getLabel());
						AD_Table_ID = MTable.getTable_ID(b[0]);
					}
				} 
			}

			String seqColName =  (String) item.getAttribute(ATT_TREEITEM_SEQCOL);
			treeUpdated = ReorderItemsDialog.showRearrangeTreeItems(p_WindowNo, seqColName, treeItem.getLabel(), AD_Table_ID, treeItems);
			
		} else if (menuAction.equals(TREE_MENUACTION_MOVETO)) {
			Treeitem treeItem = (Treeitem) item.getAttribute(ATT_TREEITEM);;
			MInfoTreeNode node = m_TreeNodes.get(treeItem.getId());
			
			MTable table = MTable.get(Env.getCtx(), node.getTableName());
			PO po = table.getPO(node.getID(), null);
			po.set_ValueOfColumn(node.getParentColumn(), Record_ID);
			treeUpdated = po.save();
			
		} else if (menuAction.equals(TREE_MENUACTION_DELETE)) {
			Treeitem treeItem = (Treeitem) item.getAttribute(ATT_TREEITEM);;
			MInfoTreeNode node = m_TreeNodes.get(treeItem.getId());

			MTable table = MTable.get(Env.getCtx(), node.getTableName());
			PO po = table.getPO(node.getID(), null);
			treeUpdated =  po.delete(false);
			
			
		}
		
		if (treeUpdated) {
			populateTree();
		}
	}

	private Menupopup getDestinations(Treeitem item)
	{
		Menupopup popup = new Menupopup();
		
		Treeitem parent = item.getParentItem();
		if (parent == null) {
			return null;
		}
		int targetLevel = (int) parent.getAttribute(ATT_TREENODELEVEL);
		
		//retrieve the siblings by getting its parent, then children
		Treeitem grandParent = parent.getParentItem();
		if (grandParent == null) {
			//no possible siblings
			return null;
			
		}
		
		Treechildren siblings = (Treechildren) grandParent.getLastChild();
		Menuitem menu;
		Collection<Treeitem> nodes = siblings.getItems();
		for (Treeitem node : nodes) {
			if ((int)node.getAttribute(ATT_TREENODELEVEL) == targetLevel && node.getId() != parent.getId()) {
				
				menu = new Menuitem(node.getLabel());
				menu.addEventListener(Events.ON_CLICK, this);
				menu.setValue(node.getId());
				menu.setAttribute(ATT_MENUACTION, TREE_MENUACTION_MOVETO);
				menu.setAttribute(ATT_TREEITEM, item);
				
				popup.appendChild(menu);
			}
		}
		return popup;
		
	}
	/**
	 *  On check event for the expand checkbox
	 */
	private void expandOnCheck()
	{
		if (chkExpand.isChecked())
			expandAll();
		else
			collapseAll();
	}

	/**
	* expand all node
	*/
	public void expandAll()
	{
		if (!chkExpand.isChecked())
			chkExpand.setChecked(true);
	
		TreeUtils.expandAll(m_Tree);
	}
	
	/**
	 * collapse all node
	 */
	public void collapseAll()
	{
		if (chkExpand.isChecked())
			chkExpand.setChecked(false);
	
		TreeUtils.collapseAll(m_Tree);
	}
	
	protected void filterRowsFromTree()
	{
		if (m_VisibleID.size() == 0) {
			contentPanel.setModel(m_modelAll);
			setStatusSelected();
			return;
		}
		ListModelTable current = new ListModelTable();
		
		current.setMultiple(m_modelAll.isMultiple());
		for (int i=0; i < m_modelAll.size(); i++)
		{
			Object data = m_modelAll.getValueAt(i, m_ParentColumnIdx);
			
			if (data instanceof IDColumn) {
				IDColumn id = (IDColumn) data;
				
				if (m_VisibleID.contains(id.getRecord_ID())) {
					current.add(m_modelAll.get(i));
				}
			} else if (data instanceof KeyNamePair) {
				KeyNamePair id = (KeyNamePair) data;
				if (m_VisibleID.contains(id.getKey())) {
					current.add(m_modelAll.get(i));
				}
				
			}
		
			
		}
		
		
		contentPanel.setModel(current);
		
		//contentPanel.selectAll();  --this is not marking the record as selected
        int no = current.getSize();
        if (paging != null) {
        	paging.setTotalSize(no);
        }
        setStatusLine(Integer.toString(no) + " " + Msg.getMsg(Env.getCtx(), "SearchRows_EnterQuery"), false);
        setStatusDB(Integer.toString(no));
		setStatusSelected();
	}
	
	protected boolean hasChildrenChecked(List<Component> items)
	{
		return false;
	}
	protected void updateChildren(List<Component> items, boolean checked)
	{
		for (Component item : items) {
			if (item instanceof Treechildren) {
				Treechildren children = (Treechildren) item;
				Collection<Treeitem> nodes = children.getItems();
				for (Treeitem node : nodes) {
					if (node.getChildren().size() == 1) {
						node.setSelected(checked);
						if (!node.isOpen() && node.isSelected()) {
							node.setOpen(true);
						}

						String[] a = node.getId().split("-");
						if ((a[0]+"_ID").equals(m_FilterColumn)) {
							if (checked) {
								m_VisibleID.add(Integer.parseInt(a[1]));
								
							} else {
								Iterator<Integer> itr = m_VisibleID.iterator();
								while (itr.hasNext()) {
								    Integer number = itr.next();

								       if (number == Integer.parseInt(a[1])) {
								       itr.remove();
								    }

								}
							}
						}
					} else {
						node.setSelected(checked);
						if (!node.isOpen() && node.isSelected()) {
							node.setOpen(true);
						}
						updateChildren(node.getChildren(), checked);
					}
				}
			}
			
		}
	}
	
	/**
	 * {@inheritDoc-}
	 */
	@Override
	protected void resetParameters()
	{
		// reset value of parameter to null, just reset display parameter
		for (WEditor editor : editors)
		{
			GridField gField = editor.getGridField();
			if (gField == null || !gField.isDisplayed())
			{
				continue;
			}

			// just reset to default Field set explicit DefaultValue
			Object resetValue = null;
			if (m_DefaultParameters.containsKey(editor.getColumnName())) {
				resetValue = m_DefaultParameters.get(editor.getColumnName());
			}
//			if (!Util.isEmpty(gField.getVO().DefaultValue, true))
//			{
//				resetValue = gField.getDefault(null);
//			}
			Object oldValue = gField.getValue();
			gField.setValue(resetValue, true);

			// call valueChange to update env
			ValueChangeEvent changeEvent = new ValueChangeEvent(editor, "", oldValue, resetValue);
			valueChange(changeEvent);
		}

		// init again parameter
		initParameters();

		// filter dynamic value
		dynamicDisplay(null);
	}

	@Override
	public void onPageAttached(Page newpage, Page oldpage)
	{
		super.onPageAttached(newpage, oldpage);
		if (newpage != null)
		{
			for (WEditor editor : editors)
			{
				editor.getComponent().addEventListener(Events.ON_FOCUS, this);
			}
		}
	}

	/**
	 * Test Row Count
	 * 
	 * @return true if display
	 */
	protected boolean testCount()
	{
		return testCount(true);
	}

	/**
	 * Test Row Count
	 * 
	 * @return true if display
	 */
	protected boolean testCount(boolean promptError)
	{
		long start = System.currentTimeMillis();
		String dynWhere = getSQLWhere();
		StringBuilder sql = new StringBuilder(m_sqlMain);

		if (dynWhere.length() > 0)
			sql.append(dynWhere); // includes first AND

		String countSql = Msg.parseTranslation(Env.getCtx(), sql.toString()); // Variables
		if (countSql.trim().endsWith("WHERE"))
		{
			countSql = countSql.trim();
			countSql = countSql.substring(0, countSql.length() - 5);
		}
		String otherClause = "";
		if (infoWindow.getOtherClause() != null && infoWindow.getOtherClause().trim().length() > 0)
		{
			otherClause = infoWindow.getOtherClause();
			if (otherClause.indexOf("@") >= 0)
			{
				String s = Env.parseContext(infoContext, p_WindowNo, otherClause, true, false);
				if (s.length() == 0)
				{
					log.severe("Failed to parse other clause. " + otherClause);
				}
				else
				{
					otherClause = s;
				}
			}
		}
		countSql = MRole.getDefault().addAccessSQL(countSql, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);

		countSql = "SELECT COUNT(*) FROM ( " + countSql + " " + otherClause + " ) a";

		if (log.isLoggable(Level.FINER))
			log.finer(countSql);
		m_count = -1;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(countSql, null);
			setParameters(pstmt, true);
			rs = pstmt.executeQuery();

			if (rs.next())
				m_count = rs.getInt(1);

		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, countSql, e);
			m_count = -2;
		}
		finally
		{
			DB.close(rs, pstmt);
		}

		if (log.isLoggable(Level.FINE))
			log.fine("#" + m_count + " - " + (System.currentTimeMillis() - start) + "ms");

		if (infoWindow.getMaxQueryRecords() > 0 && m_count > infoWindow.getMaxQueryRecords())
		{
			if (promptError)
			{
				FDialog.error(getWindowNo(), this, "InfoFindOverMax",
						m_count + " > " + infoWindow.getMaxQueryRecords());
			}
			m_count = 0;
		}

		return true;
	} // testCount

	/** Return true if there is an 'IsActive' criteria */
	boolean hasIsActiveEditor()
	{
		for (WEditor editor : editors)
		{
			if (editor.getGridField() != null && "IsActive".equals(editor.getGridField().getColumnName()))
			{
				return true;
			}
		}
		return false;
	}

	public ArrayList<ColumnInfo> getInfoColumnsLayout(MInfoWindow info)
	{
		AccessSqlParser sqlParser = new AccessSqlParser("SELECT * FROM " + info.getFromClause());
		TableInfo[] tableInfos = sqlParser.getTableInfo(0);

		MInfoColumn[] infoColumns = info.getInfoColumns(tableInfos);
		ArrayList<ColumnInfo> list = new ArrayList<ColumnInfo>();
		String keyTableAlias = tableInfos[0].getSynonym() != null && tableInfos[0].getSynonym().trim().length() > 0
				? tableInfos[0].getSynonym() : tableInfos[0].getTableName();

		String keySelectClause = keyTableAlias + "." + p_keyColumn;

		for (MInfoColumn infoColumn : infoColumns)
		{
			if (infoColumn.isDisplayed(infoContext, p_WindowNo) && !infoColumn.isVariableOnly())
			{
				ColumnInfo columnInfo = null;
				String colSQL = infoColumn.getSelectClause();
				if (!colSQL.toUpperCase().contains(" AS "))
					colSQL += " AS " + infoColumn.getColumnName();
				if (infoColumn.getAD_Reference_ID() == DisplayType.ID)
				{
					if (infoColumn.getSelectClause().equalsIgnoreCase(keySelectClause))
						continue;

					columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL,
							DisplayType.getClass(infoColumn.getAD_Reference_ID(), true));
				}
				else if (DisplayType.isLookup(infoColumn.getAD_Reference_ID()))
				{
					if (infoColumn.getAD_Reference_ID() == DisplayType.List)
					{
						WEditor editor = null;
						editor = WebEditorFactory.getEditor(getGridField(infoColumn), true);
						editor.setMandatory(false);
						editor.setReadWrite(false);
						editorMap.put(colSQL, editor);
						columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL, ValueNamePair.class,
								(String) null);
					}
					else
					{
						GridField field = getGridField(infoColumn);
						columnInfo = createLookupColumnInfo(tableInfos, field, infoColumn);
					}
				}
				else
				{
					columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL,
							DisplayType.getClass(infoColumn.getAD_Reference_ID(), true));
				}
				columnInfo.setGridField(getGridField(infoColumn));
				list.add(columnInfo);
			}

		}

		return list;
	}

	protected void refresh(Object obj, EmbedWinInfo relatedInfo)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(relatedInfo.getInfoSql()); // delete get sql method from
												// MInfoWindow
		if (log.isLoggable(Level.FINEST))
			log.finest(sql.toString());
		IDColumn ID = (IDColumn) obj;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(relatedInfo.getInfoSql(), null);
			pstmt.setObject(1, ID.getRecord_ID());
			rs = pstmt.executeQuery();
			loadEmbedded(rs, relatedInfo);
		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	} // refresh

	protected void refresh(EmbedWinInfo relatedInfo){
		if (relatedInfo.getInfoTbl() != null){
			if (((WListbox)relatedInfo.getInfoTbl()).getModel() != null)
				((WListbox)relatedInfo.getInfoTbl()).getModel().clear();
			else
				((WListbox)relatedInfo.getInfoTbl()).clear();
		}
	}
	
	public void loadEmbedded(ResultSet rs, EmbedWinInfo info) throws SQLException
	{
		ListModelTable model;
		ArrayList<ColumnInfo> list = new ArrayList<ColumnInfo>();
		list = getInfoColumnsLayout(info.getInfowin());

		// Convert ArrayList to Array
		ColumnInfo[] s_layoutEmbedded = new ColumnInfo[list.size()];
		list.toArray(s_layoutEmbedded);
		List<Object> data = new ArrayList<Object>();
		ArrayList<Object> lines = new ArrayList<Object>();

		while (rs.next())
		{
			try
			{
				data = readData(rs, s_layoutEmbedded);
			}
			catch (SQLException e)
			{
				throw new AdempiereException(e);
			}
			lines.add(data);
		}
		model = new ListModelTable(lines);

		WListbox content = (WListbox) info.getInfoTbl();
		content.setData(model, null);
	}

	protected GridField getGridField(MInfoColumn infoColumn)
	{
		String columnName = infoColumn.getColumnName();
		GridFieldVO vo = GridFieldVO.createParameter(infoContext, p_WindowNo, 0, columnName,
				infoColumn.get_Translation("Name"), infoColumn.getAD_Reference_ID(),
				infoColumn.getAD_Reference_Value_ID(), false, false);
		if (infoColumn.getAD_Val_Rule_ID() > 0)
		{
			vo.ValidationCode = infoColumn.getAD_Val_Rule().getCode();
			if (vo.lookupInfo != null)
			{
				vo.lookupInfo.ValidationCode = vo.ValidationCode;
				vo.lookupInfo.IsValidated = false;
			}
		}
		vo.DisplayLogic = infoColumn.getDisplayLogic() != null ? infoColumn.getDisplayLogic() : "";
		String desc = infoColumn.get_Translation("Description");
		vo.Description = desc != null ? desc : "";
		String help = infoColumn.get_Translation("Help");
		vo.Help = help != null ? help : "";
		GridField gridField = new GridField(vo);

		return gridField;
	}

	protected ArrayList<Object> readData(ResultSet rs, ColumnInfo[] p_layout) throws SQLException
	{
		int colOffset = 1;
		ArrayList<Object> data = new ArrayList<Object>();

		for (int col = 0; col < p_layout.length; col++)
		{
			Object value = null;
			Class<?> c = p_layout[col].getColClass();
			int colIndex = col + colOffset;
			
			if (c == IDColumn.class)
			{
				value = new IDColumn(rs.getInt(colIndex));
			}
			else if (c == Boolean.class)
				value = "Y".equals(rs.getString(colIndex));
			else if (c == Timestamp.class)
				value = rs.getTimestamp(colIndex);
			else if (c == BigDecimal.class)
				value = rs.getBigDecimal(colIndex);
			else if (c == Double.class)
				value = rs.getDouble(colIndex);
			else if (c == Integer.class)
				value = rs.getInt(colIndex);
			else if (c == KeyNamePair.class)
			{
				if (p_layout[col].isKeyPairCol())
				{
					String display = rs.getString(colIndex);
					int key = rs.getInt(colIndex + 1);
					if (!rs.wasNull())
					{
						value = new KeyNamePair(key, display);
					}
					colOffset++;
				}
				else
				{
					int key = rs.getInt(colIndex);
					if (!rs.wasNull())
					{
						// rework this, it will fail
						WEditor editor = editorMap.get(p_layout[col].getColSQL());
						if (editor != null)
						{
							editor.setValue(key);
							value = new KeyNamePair(key, editor.getDisplayTextForGridView(key));
						}
						else
						{
							value = new KeyNamePair(key, Integer.toString(key));
						}
					}
				}
			}
			else if (c == ValueNamePair.class)
			{
				String key = rs.getString(colIndex);
				WEditor editor = editorMap.get(p_layout[col].getColSQL());
				if (editor != null)
				{
					value = new ValueNamePair(key, editor.getDisplayTextForGridView(key));
				}
				else
				{
					value = new ValueNamePair(key, key);
				}
			}
			else
			{
				value = rs.getString(colIndex);
			}
			data.add(value);
		}

		return data;
	}

	/**
	 * {@inheritDoc} evaluate input value of mandatory field, if null show field
	 * in red color
	 */
	@Override
	public boolean validateParameters()
	{
		boolean isValid = true;

		for (int i = 0; i < editors.size(); i++)
		{
			WEditor wEditor = editors.get(i);
			// cancel editor not display
			if (wEditor == null || !wEditor.isVisible() || wEditor.getGridField() == null)
			{
				continue;
			}

			isValid = isValid & validateField(wEditor);
		}

		return isValid;
	}

	/**
	 * valid mandatory of a not null, display field display red color when a
	 * mandatory field is not input
	 * 
	 * @param wEditor
	 * @return
	 */
	protected boolean validateField(WEditor wEditor)
	{
		if (wEditor == null || !wEditor.isVisible() || wEditor.getGridField() == null)
		{
			return true;
		}

		GridField validateGrid = wEditor.getGridField();
		// evaluate only mandatory field
		if (validateGrid.isMandatory(true))
		{
			// update color of field
			wEditor.updateLabelStyle();
			Object data = wEditor.getValue();
			if (data == null || data.toString().length() == 0)
			{
				FDialog.error(0, wEditor.getLabel().getValue() + " is mandatory.  Please set valid value.");
				return false;
			}
		}
		return true;
	}

	@Override
	public String getSortDirection(Comparator cmpr) {
		return "natural";
	}

	
	/**
	 * @author xolali IDEMPIERE-1045
	 * getInfoColumnslayout(MInfoWindow info)
	 */
	public ArrayList<ColumnInfo> getInfoColumnslayout(MInfoWindow info){

		AccessSqlParser sqlParser = new AccessSqlParser("SELECT * FROM " + info.getFromClause());
		TableInfo[] tableInfos = sqlParser.getTableInfo(0);

		MInfoColumn[] infoColumns = info.getInfoColumns(tableInfos);
		ArrayList<ColumnInfo> list = new ArrayList<ColumnInfo>();
		String keyTableAlias = tableInfos[0].getSynonym() != null && tableInfos[0].getSynonym().trim().length() > 0
				? tableInfos[0].getSynonym()
						: tableInfos[0].getTableName();

				String keySelectClause = keyTableAlias + "." + p_keyColumn;

				for (MInfoColumn infoColumn : infoColumns)
				{
					if (infoColumn.isDisplayed(infoContext, p_WindowNo) && !infoColumn.isVariableOnly())
					{
						ColumnInfo columnInfo = null;
						String colSQL = infoColumn.getSelectClause();
						if (! colSQL.toUpperCase().contains(" AS "))
							colSQL += " AS " + infoColumn.getColumnName();
						if (infoColumn.getAD_Reference_ID() == DisplayType.ID)
						{
							if (infoColumn.getSelectClause().equalsIgnoreCase(keySelectClause))
								continue;

							columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL, DisplayType.getClass(infoColumn.getAD_Reference_ID(), true));
						}
						else if (DisplayType.isLookup(infoColumn.getAD_Reference_ID()))
						{
							if (infoColumn.getAD_Reference_ID() == DisplayType.List)
							{
								WEditor editor = null;							
						        editor = WebEditorFactory.getEditor(getGridField(infoColumn), true);
						        editor.setMandatory(false);
						        editor.setReadWrite(false);
						        editorMap.put(colSQL, editor);
								columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL, ValueNamePair.class, (String)null);
							}
							else
							{
								GridField field = getGridField(infoColumn);
								columnInfo = createLookupColumnInfo(tableInfos, field, infoColumn);
							}
						}
						else
						{
							columnInfo = new ColumnInfo(infoColumn.get_Translation("Name"), colSQL, DisplayType.getClass(infoColumn.getAD_Reference_ID(), true));
						}
//						columnInfo.setColDescription(infoColumn.get_Translation("Description"));
						columnInfo.setGridField(getGridField(infoColumn));
						list.add(columnInfo);
					}

				}

				return   list;
	}

	protected void updateSubcontent (){
		int row = contentPanel.getSelectedRow();
		if (row >= 0) {
			for (EmbedWinInfo embed : embeddedWinList) {
				// default link column is key column
				int indexData = 0;
				if (columnDataIndex.containsKey(embed.getParentLinkColumnID())){
					// get index of link column
					indexData = p_layout.length + columnDataIndex.get(embed.getParentLinkColumnID());
				}
				refresh(contentPanel.getValueAt(row,indexData),embed);
			}// refresh for all
		}else{
			for (EmbedWinInfo embed : embeddedWinList) {
				refresh(embed);
			}
		}
	}


	@Override
	protected void renderItems() {
		super.renderItems();
		m_modelAll = contentPanel.getModel();
		if (hasTree && isQueryByUser) {
			m_VisibleID.clear();
			if (m_count >= m_modelAll.getSize()) {
				m_IsTreeFilterOnDB = true;
			} else {
				m_IsTreeFilterOnDB = false;
			}
		}
		
		if (infoWindow.isSelectAllOnLoad() && p_multipleSelection) {
			contentPanel.setNonselectableTags("*");
			contentPanel.selectAll();
		}
	}

	@Override
	protected void onOk() {
		if (infoWindow.getAD_Process_ID() != 0) {
			runProcess(infoWindow.getAD_Process_ID());
		}
		super.onOk();
	}
}
