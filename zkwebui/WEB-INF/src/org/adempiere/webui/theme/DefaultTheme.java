package org.adempiere.webui.theme;

public class DefaultTheme {

	public static final String ZK_DEFAULT_THEME_ORIGIN = "FOLDER";

	public String get_themeName() {
		return "atlantic";
	}

	public void registerDefaultTheme() {
		
		ThemeUtils.register(get_themeName());
		
	}

}
