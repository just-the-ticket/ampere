package org.adempiere.webui.panel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WFileImportByDrop;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.DropUploadEvent;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.util.ZKUtils;
import org.apache.commons.io.FilenameUtils;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.North;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.impl.FileuploadDlg;

/**
 * Form for Drop files
 * 
 * @author Sachin Bhimani
 * @since Feb 28, 2018
 */
public class WDropHerePanel extends Window implements EventListener<Event>
{
	/**
	 * 
	 */
	private static final long			serialVersionUID	= -3903135666047550475L;

	// Supported dropped file from Toolbar
	public static final List<String>	mediaType			= new ArrayList<String>() {
																private static final long serialVersionUID = 389082164471527398L;

																{
//																	add("text/csv");
//																	add("application/vnd.ms-excel");
//																	add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//																	add("application/vnd.oasis.opendocument.spreadsheet");
																	add("txt");
																	add("csv");
																	add("xls");
																	add("xlsx");
																	add("ods");
																}
															};

	private AbstractADWindowPanel		winPanel			= null;
	private Label 						sizeLabel 			= new Label();
	private Progressmeter 				progressmeter 		= new Progressmeter(0);
	private FileuploadDlg				fileUploadDlg		= new FileuploadDlg();
	private Borderlayout				layout				= new Borderlayout();
	private ConfirmPanel 				confirmPanel		= new ConfirmPanel(true);
	private Button 						bLoad 				= new Button();
	private Vbox 						box 				= new Vbox();

	/**
	 * Constructor
	 * 
	 * @param winPanel
	 */
	public WDropHerePanel(AbstractADWindowPanel winPanel)
	{
		this.winPanel = winPanel;
		init();
		AEnv.showWindow(this);
		initJS();
	}

	/**
	 * 
	 */
	private void init()
	{
		this.setWidth("700px");
		this.setHeight("400px");
		
		ZKUpdateUtil.setHflex(layout, "true");
		ZKUpdateUtil.setVflex(layout, "true");
		
		North north = new North();
		ZKUpdateUtil.setHeight(north, "40px");
		Hbox hbox = new Hbox();
		ZKUpdateUtil.setWidth(hbox, "95%");
		fileUploadDlg.setHeight("40px"); 
		fileUploadDlg.setWidth("40px"); //it's corrupting the display
		hbox.appendChild(sizeLabel);
		hbox.appendChild(fileUploadDlg);
		hbox.appendChild(progressmeter);
		north.appendChild(hbox);
		
		Center center = new Center();
		ZKUpdateUtil.setVflex(center, "true");

		box.appendChild(bLoad);
		ZKUpdateUtil.setHflex(box, "true");
		ZKUpdateUtil.setVflex(box, "true");
		bLoad.setLabel("Click to upload OR Drop files here");
		bLoad.setImage(ThemeUtils.resolveImageURL( "Import24.png"));
		bLoad.setTooltiptext(Msg.getMsg(Env.getCtx(), "Load"));
		bLoad.addEventListener(Events.ON_CLICK, this);
		ZKUpdateUtil.setHflex(bLoad, "true");
		ZKUpdateUtil.setVflex(bLoad, "true");
		
		South south = new South();
		south.setStyle("border: none");
		ZKUpdateUtil.setHeight(south, "40px");
		confirmPanel.addActionListener(this);
		
		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);  //Hide OK Button

		this.appendChild(layout);
		layout.appendChild(north);
		layout.appendChild(center);
		layout.appendChild(south);
		center.appendChild(box);
		south.appendChild(confirmPanel);
		

		progressmeter.setVisible(false);
		progressmeter.setClass("drop-progress-meter");

		setVisible(true);
		setSizable(true);
		setMaximizable(true);
		setMaximized(false);
		setClosable(true);
		setBorder("normal");
		setPosition("center");
		setSclass("popup-dialog");
		setStyle("position:absolute");
		setTitle(Msg.getMsg(Env.getCtx(), "DropToImport"));
		setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
	} // init

	public void initJS() {
//		Clients.evalJavaScript(
//				"dropToAttachFiles.init('" + dropHereDiv.getUuid() + "','" + dropHereDiv.getUuid() + "','" + fileUploadDlg.getUuid()
//						+ "','" + AEnv.getDesktop().getId() + "','" + progressmeter.getUuid()
//						+ "','" + sizeLabel.getUuid() + "','" + ZKUtils.getMaxUploadSize() + "');");

		Clients.evalJavaScript("dropToAttachFiles.init('" + bLoad.getUuid() + "','" + box.getUuid() + "','"
				+ fileUploadDlg.getUuid() + "','" + AEnv.getDesktop().getId() + "','" + progressmeter.getUuid()
				+ "','" + sizeLabel.getUuid() + "','" + ZKUtils.getMaxUploadSize() + "');");
		
//		dropHereDiv.addEventListener(DropUploadEvent.ON_DROP_UPLOAD_EVENT, this);
		bLoad.addEventListener(Events.ON_UPLOAD, this);
		bLoad.addEventListener(DropUploadEvent.ON_DROP_UPLOAD_EVENT, this);

	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		sizeLabel.setValue("");
		
		if (event instanceof UploadEvent)
		{
			fileUploadDlg.onUpload(new ForwardEvent(event.getName(), event.getTarget(), event));
		}

		else if (event.getName().equals(DropUploadEvent.ON_DROP_UPLOAD_EVENT))
		{
			sizeLabel.setValue("Status: " + event.getData().toString());
			dropToUpload();
		}
		else if (event.getTarget() == bLoad) {
			loadFile();
		}
		else if(event.getTarget() == confirmPanel.getButton(ConfirmPanel.A_CANCEL))
		{
			dispose();
		}
	} // onEvent

	/**
	 * Load file for attachment
	 **/
	private void loadFile()
	{
		Media media = null;

		try
		{
			media = Fileupload.get(true);
			processFile(media);
		}
		catch (Exception e)
		{
//			log.log(Level.WARNING, e.getLocalizedMessage(), e);
		}


	} // loadFile
	
	/**
	 * 
	 */
	private void dropToUpload()
	{
		if (fileUploadDlg != null && fileUploadDlg.getResult() != null)
		{
			for (Media media : fileUploadDlg.getResult())
			{
				media = processFile(media);
			}
		}
	} // dropToUpload

	private Media processFile(Media media) {
		if (media != null)
		{
			String extension = FilenameUtils.getExtension(media.getName());
			
			if (mediaType.contains(extension)) 
			{
				if (extension.equalsIgnoreCase("xls") || extension.equalsIgnoreCase("xlsx"))
				{
					media = ZKUtils.converXLSToCSV(media);
				}

				WFileImportByDrop form = new WFileImportByDrop(winPanel, media);

				form.setVisible(true);
				form.setSizable(true);
				form.setMaximizable(true);
				form.setMaximized(false);
				form.setClosable(true);
				ZKUpdateUtil.setWidth(form, "550px");
				ZKUpdateUtil.setHeight(form, "350px");
				form.setBorder("normal");
				form.setPosition("center");
				form.setSclass("popup-dialog");
				form.setStyle("position:absolute");
				form.setTitle(Msg.getMsg(Env.getCtx(), "Import") + ": " + media.getName());

				AEnv.showWindow(form);
			}
			else
			{
				throw new AdempiereException(
						"Must require CSV file. \n" + media.getFormat().toUpperCase() + " is not valid.");
			}
		}
		return media;
	}
}
