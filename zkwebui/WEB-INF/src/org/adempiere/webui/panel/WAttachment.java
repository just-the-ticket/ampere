/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin  All Rights Reserved.                      *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.AdempiereWebUI;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.event.DropUploadEvent;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.util.ZKUtils;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.au.out.AuScript;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.impl.FileuploadDlg;

/**
 * 
 * @author Low Heng Sin
 *
 */
public class WAttachment extends Window implements EventListener<Event>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2923895336573554570L;

	private static CLogger log = CLogger.getCLogger(WAttachment.class);

	/**	Window No				*/
	private int	m_WindowNo;
	
	/** Attachment				*/
	private MAttachment	m_attachment;
	
	/** Change					*/
	private boolean m_change = false;

	private Iframe preview = new Iframe();

	private Textbox text = new Textbox();

	private Listbox cbContent = new Listbox();

	private Button bDelete = new Button();
	private Button bSave = new Button();
	private Button bDeleteAll = new Button();
	private Button bLoad = new Button();
	private Button bCancel = new Button();
	private Button bOk = new Button();
	private Button bRefresh = new Button();
	
	private Label sizeLabel = new Label();
	private Progressmeter progressmeter =  new Progressmeter(0);
	private FileuploadDlg fileUploadDlg = new FileuploadDlg();
	
	private Panel previewPanel = new Panel();

	private Borderlayout mainPanel = new Borderlayout();

	private Vbox box = new Vbox();

	private Hbox toolBar = new Hbox();	
	
	private Hbox confirmPanel = new Hbox();

	private int displayIndex;

	/**
	 *	Constructor.
	 *	loads Attachment, if ID <> 0
	 *  @param WindowNo window no
	 *  @param AD_Attachment_ID attachment
	 *  @param AD_Table_ID table
	 *  @param Record_ID record key
	 *  @param trxName transaction
	 */
	
	public WAttachment(	int WindowNo, int AD_Attachment_ID,
						int AD_Table_ID, int Record_ID, String trxName)
	{
		this(WindowNo, AD_Attachment_ID, AD_Table_ID, Record_ID, trxName, (EventListener<Event>)null);
	}

	/**
	 *	Constructor.
	 *	loads Attachment, if ID <> 0
	 *  @param WindowNo window no
	 *  @param AD_Attachment_ID attachment
	 *  @param AD_Table_ID table
	 *  @param Record_ID record key
	 *  @param trxName transaction
	 */

	public WAttachment(	int WindowNo, int AD_Attachment_ID,
						int AD_Table_ID, int Record_ID, String trxName, EventListener<Event> eventListener)
	{
		super();
		
		log.config("ID=" + AD_Attachment_ID + ", Table=" + AD_Table_ID + ", Record=" + Record_ID);

		m_WindowNo = WindowNo;
		
		if (eventListener != null) 
		{
			this.addEventListener(DialogEvents.ON_WINDOW_CLOSE, eventListener);
		}

		try
		{
			staticInit();
		}
		catch (Exception ex)
		{
			log.log(Level.SEVERE, "", ex);
		}
		
		//	Create Model
		
		if (AD_Attachment_ID == 0)
			m_attachment = new MAttachment (Env.getCtx(), AD_Table_ID, Record_ID, trxName);
		else
			m_attachment = new MAttachment (Env.getCtx(), AD_Attachment_ID, trxName);
		
		loadAttachments();

		try
		{
			setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
			AEnv.showWindow(this);
			displayData(0, true);
			String script = "setTimeout(\"zk.Widget.$('" + preview.getUuid() + "').src = zk.Widget.$('" + preview.getUuid()
					+ "').src\", 1000)";
			Clients.response(new AuScript(null, script));

			Clients.evalJavaScript("dropToAttachFiles.init('" + bLoad.getUuid() + "','" + box.getUuid() + "','"
					+ fileUploadDlg.getUuid() + "','" + AEnv.getDesktop().getId() + "','" + progressmeter.getUuid()
					+ "','" + sizeLabel.getUuid() + "','" + ZKUtils.getMaxUploadSize() + "');");

			// enter modal
			doHighlighted();
		}
		catch (Exception e)
		{
		}
	} // WAttachment

	/**
	 *	Static setup.
	 *  <pre>
	 *  - northPanel
	 *      - toolBar
	 *      - title
	 *  - centerPane [split]
	 * 		- previewPanel (left)
	 *  	- text (right)
	 *  - confirmPanel
	 *  </pre>
	 *  @throws Exception
	 */
	
	void staticInit() throws Exception
	{
		this.setMaximizable(true);
		ZKUpdateUtil.setWidth(this, "950px");
		ZKUpdateUtil.setHeight(this, "600px");
		this.setTitle("Attachment");
		this.setClosable(true);
		this.setSizable(true);
		this.setBorder("normal");
		this.appendChild(mainPanel);

		bLoad.addEventListener(Events.ON_UPLOAD, this);
		bLoad.addEventListener(DropUploadEvent.ON_DROP_UPLOAD_EVENT, this);
		
		North northPanel = new North();
		northPanel.setCollapsible(false);
		northPanel.setSplittable(false);
		northPanel.setStyle("padding: 4px; background: #cfcfcf;");
		
		ZKUpdateUtil.setWidth(cbContent, "100%");
		cbContent.setMold("select");
		cbContent.setRows(0);
		cbContent.addEventListener(Events.ON_SELECT, this);

		box.appendChild(bLoad);
		ZKUpdateUtil.setHeight(box, "100px");
		ZKUpdateUtil.setWidth(box, "633px");

		Vbox vbox = new Vbox();
		Separator space = new Separator();
		space.setSpacing("6px");

		Hbox hbox = new Hbox();
		ZKUpdateUtil.setWidth(hbox, "100%");
		hbox.appendChild(bDelete);
		hbox.appendChild(bSave);
		hbox.appendChild(cbContent);
		vbox.appendChild(hbox);
		vbox.appendChild((Component) space.clone());

		hbox = new Hbox();
		ZKUpdateUtil.setWidth(hbox, "100%");
		hbox.appendChild(sizeLabel);
		hbox.appendChild(fileUploadDlg);
		vbox.appendChild(hbox);
		vbox.appendChild((Component) space.clone());

		hbox = new Hbox();
		ZKUpdateUtil.setWidth(hbox, "100%");
		hbox.appendChild(progressmeter);
		vbox.appendChild(hbox);
		vbox.appendChild(space);

		toolBar.appendChild(box);
		toolBar.appendChild(vbox);
		mainPanel.appendChild(northPanel);

		vbox = new Vbox();
		vbox.appendChild(toolBar);
		vbox.appendChild(text);
		ZKUpdateUtil.setWidth(vbox, "100%");
		northPanel.appendChild(vbox);

		text.setRows(3);
		ZKUpdateUtil.setWidth(text, "99%");
		text.setStyle("margin: 3px 0px;");

		progressmeter.setVisible(false);
		progressmeter.setClass("drop-progress-meter");

		bSave.setEnabled(false);
		bSave.setIconSclass("z-icon-Export");
		bSave.setTooltiptext(Msg.getMsg(Env.getCtx(), "AttachmentSave"));
		bSave.addEventListener(Events.ON_CLICK, this);

		bLoad.setLabel("Click to upload OR Drop files here");
		bLoad.setIconSclass("z-icon-Import");
		bLoad.setTooltiptext(Msg.getMsg(Env.getCtx(), "Load"));
		bLoad.setUpload("true");
		bLoad.addEventListener(Events.ON_UPLOAD, this);
		ZKUpdateUtil.setWidth(bLoad, "633px");
		ZKUpdateUtil.setHeight(bLoad, "97px");

		bDelete.setIconSclass("z-icon-Delete");
		bDelete.setTooltiptext(Msg.getMsg(Env.getCtx(), "Delete"));
		bDelete.addEventListener(Events.ON_CLICK, this);

		previewPanel.appendChild(preview);
		previewPanel.setStyle("border: 3px solid #cfcfcf; background: #efefef;");

		ZKUpdateUtil.setHeight(preview, "99%");
		ZKUpdateUtil.setWidth(preview, "99%");
		
		ZKUpdateUtil.setHeight(previewPanel, "100%");
		ZKUpdateUtil.setWidth(previewPanel, "100%");
			
		Center centerPane = new Center();
		centerPane.setAutoscroll(true);
		ZKUpdateUtil.setVflex(centerPane, "flex");
		mainPanel.appendChild(centerPane);
		centerPane.appendChild(previewPanel);
		
		South southPane = new South();
		mainPanel.appendChild(southPane);
		southPane.appendChild(confirmPanel);
		ZKUpdateUtil.setHeight(southPane, "30px");
		
		bCancel.setIconSclass("z-icon-Cancel");
		bCancel.addEventListener(Events.ON_CLICK, this);

		bOk.setIconSclass("z-icon-Ok");
		bOk.addEventListener(Events.ON_CLICK, this);
		
		bDeleteAll.setIconSclass("z-icon-Delete");
		bDeleteAll.addEventListener(Events.ON_CLICK, this);
		
		bRefresh.setIconSclass("z-icon-Refresh");
		bRefresh.addEventListener(Events.ON_CLICK, this);
		
		confirmPanel.appendChild(bDeleteAll);
		confirmPanel.appendChild(bRefresh);
		confirmPanel.appendChild(bCancel);
		confirmPanel.appendChild(bOk);
		
		text.setTooltiptext(Msg.getElement(Env.getCtx(), "TextMsg"));
		
		this.addEventListener(Events.ON_UPLOAD, this);
	}
	
	/**
	 * 	Dispose
	 */
	
	public void dispose ()
	{
		preview = null;
		this.detach();
	} // dispose
	
	/**
	 *	Load Attachments
	 */
	
	private void loadAttachments()
	{
		log.config("");
		
		//	Set Text/Description
		
		String sText = m_attachment.getTextMsg();
		
		if (sText == null)
			text .setText("");
		else
			text.setText(sText);

		//	Set Combo
		
		int size = m_attachment.getEntryCount();
		
		for (int i = 0; i < size; i++)
			cbContent.appendItem(m_attachment.getEntryName(i), m_attachment.getEntryName(i));
		
		if (size > 0)
		{
			cbContent.setSelectedIndex(0);					
		}		
		
	} // loadAttachment
	
	/**
	 *  Display gif or jpg in gifPanel
	 * 	@param index index
	 */
	
	public void displayData (int index, boolean immediate)
	{
		//	Reset UI		
		preview.setSrc(null);

		bDelete.setEnabled(false);
		bSave.setEnabled(false);
		
		displayIndex = index;
		cbContent.setSelectedIndex(index);

		if (immediate)
			displaySelected();
		else
			Clients.response(new AuEcho(this, "displaySelected", null));
	}   //  displayData

	/**
	 * Use to refresh preview frame, don't call directly.
	 */
	public void displaySelected() {
		MAttachmentEntry entry = m_attachment.getEntry(displayIndex); 
		log.config("Index=" + displayIndex + " - " + entry);
		if (entry != null && entry.getData() != null)
		{
			bSave.setEnabled(true);
			bDelete.setEnabled(true);
			
			log.config(entry.toStringX());

			try
			{
				AMedia media = new AMedia(entry.getFileName(), null, entry.getContentType(), entry.getData());
				
				preview.setContent(media);
				preview.setVisible(true);
				preview.invalidate();
			}
			catch (Exception e)
			{
				log.log(Level.SEVERE, "attachment", e);
			}
		}
	}
	
	/**
	 * 	Get File Name with index
	 *	@param index index
	 *	@return file name or null
	 */
	
	private String getFileName (int index)
	{
		String fileName = null;
	
		if (cbContent.getItemCount() > index)
		{
			ListItem listitem = cbContent.getItemAtIndex(index);
			fileName = (String)listitem.getValue();
		}
		
		return fileName;
	}	//	getFileName

	/**
	 * Listener
	 * 
	 * @param e event
	 */
	public void onEvent(Event e)
	{
		sizeLabel.setValue("");
		
		if (e instanceof UploadEvent)
		{
			preview.setVisible(false);
			UploadEvent ue = (UploadEvent) e;
			for (Media media : ue.getMedias()) {
				processUploadMedia(media);
			}
		}
		else if (e.getTarget() == bOk)
		{
			String newText = text.getText();

			if (newText == null)
				newText = "";

			String oldText = m_attachment.getTextMsg();

			if (oldText == null)
				oldText = "";

			if (!m_change)
				m_change = !newText.equals(oldText);

			if (newText.length() > 0 || m_attachment.getEntryCount() > 0)
			{
				if (m_change)
				{
					// ATTENTION! HEAVY HACK HERE... Else it will not save :(
					m_attachment.setBinaryData(new byte[0]);
					m_attachment.setTextMsg(text.getText());
					m_attachment.saveEx();
				}
			}
			else
				m_attachment.delete(true);

			dispose();
		}
		// Cancel
		else if (e.getTarget() == bCancel)
		{
			dispose();
		}
		// Delete Attachment
		else if (e.getTarget() == bDeleteAll)
		{
			deleteAttachment();
			dispose();
		}
		// Delete individual entry and Return
		else if (e.getTarget() == bDelete)
			deleteAttachmentEntry();
		// Show Data
		else if (e.getTarget() == cbContent)
			displayData(cbContent.getSelectedIndex(), false);
		// Open Attachment
		else if (e.getTarget() == bSave)
			saveAttachmentToFile();
		else if (e.getTarget() == bRefresh)
			displayData(displayIndex, true);
		else if (e.getTarget() instanceof Timer)
			displayData(displayIndex, true);
		else if (e.getName().equals(DropUploadEvent.ON_DROP_UPLOAD_EVENT))
		{
			sizeLabel.setValue("Status: " + e.getData().toString());
			if (fileUploadDlg.getResult() != null) {
				for (Media media : fileUploadDlg.getResult())
				{
					if (media != null)
						attachData(media);
				}
			}
		}
		else if (e instanceof UploadEvent) 
		{
			UploadEvent ue = (UploadEvent) e;
			processUploadMedia(ue.getMedia());
		}
		
	} // onEvent

	private void processUploadMedia(Media media) {

		attachData(media);

	}

	/**
	 * Attaching Data
	 * 
	 * @param media
	 */
	private void attachData(Media media)
	{
		String fileName = media.getName();
		log.config(fileName);
		int cnt = m_attachment.getEntryCount();

		// update
		for (int i = 0; i < cnt; i++)
		{
			if (m_attachment.getEntryName(i).equals(fileName))
			{
				m_attachment.updateEntry(i, getMediaData(media));
				cbContent.setSelectedIndex(i);
				displayData(cbContent.getSelectedIndex(), false);
				m_change = true;
				return;
			}
		}

		// new
		if (m_attachment.addEntry(fileName, getMediaData(media)))
		{
			cbContent.appendItem(media.getName(), media.getName());
			cbContent.setSelectedIndex(cbContent.getItemCount() - 1);
			displayData(cbContent.getSelectedIndex(), false);
			m_change = true;
		}
	} // attachData

	/* calling twice on same media object may fail, see Media.getByteData/getStringData */
	private byte[] getMediaData(Media media) {
		byte[] bytes = null;
		
		if ( media.isBinary() )
			bytes = media.getByteData();
		else
			bytes = media.getStringData().getBytes();

		return bytes;
	}

	/**
	 *	Delete entire Attachment
	 */
	private void deleteAttachment()
	{
		log.info("");
		
		FDialog.ask(m_WindowNo, this, "AttachmentDelete?", new Callback<Boolean>() {

			@Override
			public void onCallback(Boolean result) 
			{
				if (result)
				{
					m_attachment.delete(true);
					displaySelected();
				}					
			}
		});
	}	//	deleteAttachment

	/**
	 *	Delete Attachment Entry
	 */
	
	private void deleteAttachmentEntry()
	{
		log.info("");
		
		final int index = cbContent.getSelectedIndex();
		String fileName = getFileName(index);
		
		if (fileName == null)
			return;

		FDialog.ask(m_WindowNo, this, "AttachmentDeleteEntry?", new Callback<Boolean>() {
			
			@Override
			public void onCallback(Boolean result) 
			{
				if (result)
				{
					if (m_attachment.deleteEntry(index))
						cbContent.removeItemAt(index);

					m_change = true;
				}				
			}
		});
	}	//	deleteAttachment

	/**
	 *	Save Attachment to File
	 */
	
	private void saveAttachmentToFile()
	{
		int index = cbContent.getSelectedIndex();
		log.info("index=" + index);
	
		if (m_attachment.getEntryCount() < index)
			return;

		MAttachmentEntry entry = m_attachment.getEntry(index);
		if (entry != null && entry.getData() != null)
		{
			try
			{
				AMedia media = new AMedia(entry.getFileName(), null, entry.getContentType(), entry.getData());
				Filedownload.save(media);
			}
			catch (Exception e)
			{
				log.log(Level.SEVERE, "attachment", e);
			}
		}
	}	//	saveAttachmentToFile

}
