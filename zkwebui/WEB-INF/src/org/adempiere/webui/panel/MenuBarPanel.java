package org.adempiere.webui.panel;

import java.util.Enumeration;
import java.util.Properties;

import org.adempiere.webui.component.Menupopup;
import org.adempiere.webui.exception.ApplicationException;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTree;
import org.compiere.model.MTreeNode;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;

public class MenuBarPanel extends Menubar implements EventListener<Event> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1287592052221771111L;
	private Properties ctx;
	private MTreeNode rootNode = null; 
    private int treeID =0;
	
	public MenuBarPanel() {
		ctx = Env.getCtx();
        int roleID = Env.getAD_Role_ID(ctx);
        treeID = getTreeId(ctx, roleID);
        MTree tree = new MTree(ctx, treeID, false, true, null);
        rootNode = tree.getRoot();
        init();
	}
	
	private void init() {
		setSclass("desktop-header-menubar");
		
		int menubarSize = MSysConfig.getIntValue(MSysConfig.ZK_MENUBAR_SIZE, 6, Env.getAD_Client_ID(ctx));
		int count = 0;
		Menu menuMore = null;
		Menupopup popupMore = null;
		Enumeration<?> nodeEnum = rootNode.children();
		while(nodeEnum.hasMoreElements()) {
			MTreeNode childNode = (MTreeNode)nodeEnum.nextElement();
			
			if (count < menubarSize) {
				// display the top bar menu
				createTopMenu(null, childNode);
			} else {
				// hide excess menu under more...
				if (menuMore == null && popupMore == null) {
					menuMore = new Menu("More...");
					appendChild(menuMore);
					
					popupMore = new Menupopup();
					popupMore.setParent(menuMore);
				}
				createTopMenu(popupMore, childNode);
			}
			count++;
		}		
	}
	
	private int getTreeId(Properties ctx, int roleID)
    {
        int AD_Tree_ID = DB.getSQLValue(null,
                "SELECT COALESCE(r.AD_Tree_Menu_ID, ci.AD_Tree_Menu_ID)" 
                + "FROM AD_ClientInfo ci" 
                + " INNER JOIN AD_Role r ON (ci.AD_Client_ID=r.AD_Client_ID) "
                + "WHERE AD_Role_ID=?", roleID);
        if (AD_Tree_ID <= 0)
            AD_Tree_ID = 10;    //  Menu
        return AD_Tree_ID;
    }
	
	private void createTopMenu(Menupopup menuPopup, MTreeNode node) {
		if (node.getChildCount() > 0) {
			Menu menu = new Menu(node.getName());
			if (menuPopup != null)
				menuPopup.appendChild(menu);
			else
				appendChild(menu);
			
			Menupopup popup = new Menupopup();
			popup.setParent(menu);
			
			generateMenu(popup, node);
		} else {
			Menuitem item = new Menuitem(node.getName());
			if (menuPopup != null)
				menuPopup.appendChild(item);
			else
				appendChild(item);
			item.addEventListener(Events.ON_CLICK, this);
		}
	}
	
	private void generateMenu(Menupopup menuPopup, MTreeNode node)
    {
        Enumeration<?> nodeEnum = node.children();
      
        while(nodeEnum.hasMoreElements())
        {
            MTreeNode childNode = (MTreeNode)nodeEnum.nextElement();
           
            if(childNode.getChildCount() > 0 )
            {
            	Menu menu = new Menu(childNode.getName());
                menuPopup.appendChild(menu);
                menu.setTooltiptext(childNode.getDescription());
            	Menupopup menuPopupChildren = new Menupopup();
            	menuPopupChildren.setParent(menu);
                generateMenu(menuPopupChildren, childNode);
            }
            else 
            {
            	Menuitem menuItem = new Menuitem(childNode.getName());
                menuPopup.appendChild(menuItem);
                menuItem.setTooltiptext(childNode.getDescription());
                menuItem.setValue(String.valueOf(childNode.getNode_ID()));
                if (childNode.isReport())
                	menuItem.setIconSclass("z-icon-Report");
                else if (childNode.isInfo())
                	menuItem.setIconSclass("z-icon-Info");
                else
                	menuItem.setIconSclass("z-icon-Window");

                menuItem.setDraggable("favourite");
                menuItem.addEventListener(Events.ON_CLICK, this);
            }
        }
    }

	@Override
	public void onEvent(Event event) throws Exception {
		Component comp = event.getTarget();
        String eventName = event.getName();
        
        if (eventName.equals(Events.ON_CLICK)) {
        	if (comp instanceof Menuitem) {
        		Menuitem item = (Menuitem) comp;
        		fireMenuSelectedEvent(item);
        	}
        }
	}
	
	protected void fireMenuSelectedEvent(Menuitem selectedItem) {
    	int nodeId = Integer.parseInt((String)selectedItem.getValue());
       
    	try {
    		SessionManager.getAppDesktop().onMenuSelected(nodeId);
        }
        catch (Exception e) {
            throw new ApplicationException(e.getMessage(), e);
        }		
	}
}
