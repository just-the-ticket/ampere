/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.model.IInfoColumn;
import org.adempiere.model.MInfoRelated;
import org.adempiere.util.Callback;
import org.adempiere.util.StringUtils;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.BusyDialog;
import org.adempiere.webui.apps.ProcessModalDialog;
import org.adempiere.webui.apps.WProcessCtl;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.info.InfoWindow;
import org.adempiere.webui.part.ITabOnSelectHandler;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.apache.commons.lang.StringEscapeUtils;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MInfoColumn;
import org.compiere.model.MInfoProcess;
import org.compiere.model.MInfoWindow;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.MQuery;
import org.compiere.model.MRole;
import org.compiere.model.MTab;
import org.compiere.model.MTabCustomization;
import org.compiere.model.MTable;
import org.compiere.model.PrintInfo;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoUtil;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;
import org.zkoss.lang.Strings;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.event.ZulEvents;
import org.zkoss.zul.ext.Sortable;

/**
 *	Search Information and return selection - Base Class.
 *  Based on Info written by Jorg Janke
 *  
 *  @author Sendy Yagambrum
 *  
 * Zk Port
 * @author Elaine
 * @version	Info.java Adempiere Swing UI 3.4.1
 */
public abstract class InfoPanel extends Window implements EventListener<Event>, WTableModelListener, Sortable<Object>
{

	/**
	 * generated serial version ID
	 */
	private static final long										serialVersionUID		= 325050327514511004L;
	protected CLogger												log						= CLogger
			.getCLogger(getClass());

	public final static int											INFO_PRODUCT			= 50000;
	public final static int											INFO_BPARTNER			= 50001;
	public final static int											INFO_ASSET				= 50002;
	public final static int											INFO_ORDER				= 50003;
	public final static int											INFO_INVOICE			= 50004;
	public final static int											INFO_SHIPMENT			= 50005;
	public final static int											INFO_PAYMENT			= 50006;
	public final static int											INFO_CASHJOURNAL		= 50007;
	public final static int											INFO_RESOURCE			= 50008;
	public final static int											INFO_SALESFORECAST		= 50009;
	
	private final static int										PAGE_SIZE				= 1000;
	private static final String[]									lISTENER_EVENTS			= {};
	protected final static String									ITEM_ISREPORT			= "isReport";
	protected final static String									PROCESS_ID_KEY			= "processId";
	protected final static String									REPORT_TABLE_ID			= "reportTableId";
	protected final static String									REPORT_WHERECLAUSE		= "reportWhereClause";
	protected final static String									ON_RUN_PROCESS			= "onRunProcess";
	protected final static String									ATT_INFO_PROCESS_KEY	= "INFO_PROCESS";
	protected final static String 									ATT_BTN_ALWAYS_ON 		= "ButtonAlwaysOn";
	protected final static String									INFO_SELECTED_IDS		= "@SELECTED_IDS@";
	protected static final int										INFO_WIDTH				= 800;

	protected static final String 									FORCE_SELECT_ALL = "selectAll";
	
	protected Label lblTokenSearch = new Label("Token Search");
	protected Textbox fieldTokenSearch = new Textbox();

	protected List<Button>											btProcessList			= new ArrayList<Button>();
	protected Map<String, WEditor>									editorMap				= new HashMap<String, WEditor>();
	public LinkedHashMap<KeyNamePair,LinkedHashMap<String, Object>> m_values = null;
	protected MInfoRelated[] relatedInfoList;
	
	/** Initial ID value from context */
	protected int													initialId				= 0;
	protected ConfirmPanel											confirmPanel;
	/** Master (owning) Window */
	protected int													p_WindowNo;
	/** Table Name */
	protected String												p_tableName;
	/** Key Column Name */
	protected String												p_keyColumn;
	/** Enable more than one selection */
	protected boolean												p_multipleSelection;
	/** Query needs to rerun after a process is run */
	protected boolean												p_ReRunQuery = true;
	/** Initial WHERE Clause */
	protected String												p_whereClause			= "";

	protected StatusBarPanel										statusBar				= new StatusBarPanel();
	private List<Object>											line;
	protected boolean												m_lookup;
	private boolean													m_ok					= false;
	/** Cancel pressed - need to differentiate between OK - Cancel - Exit */
	private boolean													m_cancel				= false;
	/** Result IDs */
	private ArrayList<Integer>										m_results				= new ArrayList<Integer>(3);
	private ListModelTable									model;
	/** Layout of Grid */
	protected ColumnInfo[]											p_layout;
	/** Main SQL Statement */
	protected String												m_sqlMain;
	/** Count SQL Statement */
	private String													m_sqlCount;
	/** Order By Clause */
	protected String												m_sqlOrder;

	protected String												m_sqlUserOrder;
	/** ValueChange listeners */
	private ArrayList<ValueChangeListener>							listeners				= new ArrayList<ValueChangeListener>();
	/** Loading success indicator */
	protected boolean												p_loadedOK				= false;
	/** SO Zoom Window */
	private int														m_SO_Window_ID			= -1;
	/** PO Zoom Window */
	private int														m_PO_Window_ID			= -1;

	protected MInfoWindow											infoWindow;
	protected MInfoColumn[]											infoColumns;

	protected WListbox												contentPanel			= new WListbox();
	protected Paging												paging;
	protected int													pageNo;
	protected int													m_count;
	private int														cacheStart;
	private int														cacheEnd;
	private boolean													m_useDatabasePaging		= false;
	private BusyDialog												progressWindow;
	private int														m_lastSelectedIndex		= -1;
	// false, use saved where clause
	protected boolean												isQueryByUser			= false;
	// save where clause of prev requery
	protected String												prevWhereClause			= null;
	// save value of parameter to set info query paramenter
	protected List<Object>											prevParameterValues		= null;
	protected List<String>											prevQueryOperators		= null;
	protected List<WEditor>											prevRefParmeterEditor	= null;

	// All info process of this infoWindow
	protected MInfoProcess[]										infoProcessList;
	// Info process have style is button
	protected List<MInfoProcess>									infoProcessBtList;
	// Info process have style is drop down list
	protected List<MInfoProcess>									infoProcessDropList;
	// Info process have style is menu
	protected List<MInfoProcess>									infoProcessMenuList;
	// save selected id and viewID

	// Sub Tabs - manually added on custom info window e.g. InfoProductPanel to allow column customization
	protected List<WListbox>										infoSubTabs;
	
//	protected Map<Integer, List<String>>							m_viewIDMap				= new HashMap<Integer, List<String>>();
	protected Collection<KeyNamePair> m_viewIDMap = new ArrayList <KeyNamePair>();

	protected boolean												hasOrderColumn = true;
	Button 	btnOrder = null;
	/**
	 * Tab ID from the window/tab that executes it
	 */
	protected int m_Tab_ID = 0;
	
	/**
	 * Record ID when you execute infowindow process from Standard Window/button
	 */
	protected int m_Record_ID = 0;
	/**
	 * store index of infoColumn have data append. each infoColumn just append only one time.
	 * index increase from 0.
	 */
	protected Map <Integer, Integer> columnDataIndex = new HashMap <Integer, Integer> ();
	/**
	 * after load first record, set it to false. 
	 * when need update index of column data append to end of list {@link #columnDataIndex}, set it to true 
	 */
	protected boolean isMustUpdateColumnIndex = true;
	/**
	 * When start update index of column data append to end of list {@link #columnDataIndex}, reset it to 0,
	 * each read data for new append column, increase it up 1
	 */
	protected int indexColumnCount = 0;
	/**
	 * to prevent append duplicate data, when begin read each record reset this list, 
	 * when read a column store id of infoColumn to list to check duplicate
	 */
	protected List <Integer> lsReadedColumn = new ArrayList <Integer> ();
	
	protected int m_infoWindowID;
	/**
	 * MInfoColumn has isKey = true, play as key column in case non column has
	 * isKey = true, this column is null and we use {@link #p_keyColumn}
	 */
	protected MInfoColumn keyColumnOfView = null;
	
	/**
	 * index of {@link #keyColumnOfView} in data model, set when prepare listbox
	 */
	protected int indexKeyOfView = -1;
	
	protected boolean isIDColumnKeyOfView = false;
	protected boolean hasRightQuickEntry = true;
	protected boolean isHasNextPage = false;
	/**
	 * store selected record info
	 * key of map is value of column play as keyView
	 * in case has no key coloumn of view, use value of {@link #p_keyColumn}
	 * zk6.x listview don't provide event when click to checkbox select all, 
	 * so we can't manage selectedRecord time by time. 
	 * each time change page we will update this list with current
	 * selected record of this page by call function
	 * {@link #updateListSelected()} when move to zk7, just enough handle
	 * onclick. because don't direct use recordSelectedData, call
	 * {@link #getSelectedRowInfo()}
	 */
	protected LinkedHashMap<Integer, List<Object>> recordSelectedData = new LinkedHashMap<Integer, List<Object>>();

	/**
	 * when requery but don't clear selected record (example after run process)
	 * set flag to true to run sync selected record, also
	 * {@link #syncSelectedAfterRequery()}
	*/
	protected boolean isRequeryByRunSuccessProcess = false;
	
	/**
	 * Ensure you don't run the same process
	 */
	protected boolean isProcessRun = false;

	// flag indicate have infoOProcess define ViewID
	protected boolean												isHasViewID				= false;
	// Subtable column widths are initialised
	private boolean 												m_subTableInit			= false;

	// number of infoProcess contain ViewID
	protected int													numOfViewID				= 0;
	protected Button												btCbbProcess;
	protected Combobox												cbbProcess;
	protected Button												btMenuProcess;
//	protected int InfoWindow_ID;

	
	protected String												m_TSQuery = null;
	protected boolean												m_TSCapable = false;
	
	public static InfoPanel create(int WindowNo, String tableName, String keyColumn, String value,
			boolean multiSelection, String whereClause, int AD_InfoWindow_ID, boolean lookup, int record_ID, String columnName) 
	{
		InfoPanel info = null;
		info = new InfoWindow(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, AD_InfoWindow_ID,
				lookup, record_ID);
		if (!info.loadedOK())
		{
			info = create(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, lookup, columnName);
			if (!info.loadedOK())
			{
				info.dispose(false);
				info = null;
			}
		}

		return info;
	}
	
	public static InfoPanel create(int WindowNo, String tableName, String keyColumn, String value,
			boolean multiSelection, String whereClause, int AD_InfoWindow_ID, boolean lookup, String columnName)
	{
		return create(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, AD_InfoWindow_ID, lookup, 0, columnName);
	}
	
    public static InfoPanel create (int WindowNo,
            String tableName, String keyColumn, String value,
            boolean multiSelection, String whereClause, boolean lookup, String columnName)
        {
            InfoPanel info = null;

            if (tableName.equals("C_BPartner"))
                info = new InfoBPartnerPanel (value,WindowNo, !Env.getContext(Env.getCtx(),"IsSOTrx").equals("N"),
                        multiSelection, whereClause, columnName);
            else if (tableName.equals("M_Product"))
                info = new InfoProductPanel ( WindowNo,  0,0, 
                        multiSelection, value,whereClause);
            else if (tableName.equals("C_Invoice"))
                info = new InfoInvoicePanel ( WindowNo, value,
                        multiSelection, whereClause, columnName);
            else if (tableName.equals("A_Asset"))
                info = new InfoAssetPanel (WindowNo, 0, value,
                        multiSelection, whereClause);
            else if (tableName.equals("C_Order"))
                info = new InfoOrderPanel ( WindowNo, value,
                        multiSelection, whereClause, columnName);
            else if (tableName.equals("M_InOut"))
                info = new InfoInOutPanel (WindowNo, value,
                        multiSelection, whereClause);
            else if (tableName.equals("C_Payment"))
                info = new InfoPaymentPanel (WindowNo, value, multiSelection, whereClause);
            else if (tableName.equals("S_ResourceAssigment"))
                info = new InfoAssignmentPanel (WindowNo, value,
                        multiSelection, whereClause);
            else
			{
            	int AD_InfoWindow_ID = 0;
            	
            	MInfoWindow iw = MInfoWindow.get(tableName, null);
            	if ( iw !=  null) {
            		AD_InfoWindow_ID = iw.getAD_InfoWindow_ID();
            	}

				info = new InfoWindow(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, AD_InfoWindow_ID, lookup);
				if (!info.loadedOK())
				{
					info = new InfoGeneralPanel(value, WindowNo, tableName, keyColumn, multiSelection, whereClause, columnName);
					if (!info.loadedOK())
					{
						info.dispose(false);
						info = null;
					}
				}
			}
            
            return info;
        }
    
	/**
	 * Show BPartner Info (non modal)
	 * @param WindowNo window no
	 */
	public static void showBPartner (int WindowNo)
	{
		InfoBPartnerPanel info = new InfoBPartnerPanel ( "", WindowNo,
			!Env.getContext(Env.getCtx(),"IsSOTrx").equals("N"),false, "", false, null);
		AEnv.showWindow(info);
	}   //  showBPartner

	/**
	 * Show Asset Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 */
	public static void showAsset (int WindowNo)
	{
		InfoPanel info = new InfoAssetPanel (WindowNo, 0, "", false, "", false);
		AEnv.showWindow(info);
	}   //  showBPartner

	/**
	 * Show Product Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 */
	public static void showProduct (int WindowNo)
	{
		InfoPanel info = new InfoProductPanel(WindowNo, 
//				Env.getContextAsInt(Env.getCtx(), WindowNo, "M_Warehouse_ID"),
//				Env.getContextAsInt(Env.getCtx(), WindowNo, "M_PriceList_ID"),
				0,
				0,
				false, "", "", false);
		AEnv.showWindow(info);
	}   //  showProduct
	
	/**
	 * Show Order Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showOrder (int WindowNo, String value)
	{
		InfoPanel info = new InfoOrderPanel(WindowNo, "", false, "", false, null);
		AEnv.showWindow(info);
	}   //  showOrder

	/**
	 * Show Order Info but Only on Credit (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showOrderOnCredit (int WindowNo, String value)
	{
		InfoOrderPanel info = new InfoOrderPanel(WindowNo, "", false, "", false, true, null);
		AEnv.showWindow(info);
		info.onUserQuery();
	}   //  showOrder

	/**
	 * Show Invoice Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showInvoice (int WindowNo, String value)
	{
		InfoPanel info = new InfoInvoicePanel(WindowNo, "", false, "", false, null);
		AEnv.showWindow(info);
	}   //  showInvoice

	/**
	 * Show Shipment Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showInOut (int WindowNo, String value)
	{
		InfoPanel info = new InfoInOutPanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showInOut

	/**
	 * Show Payment Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showPayment (int WindowNo, String value)
	{
		InfoPanel info = new InfoPaymentPanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showPayment

	/**
	 * Show Assignment Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showAssignment (int WindowNo, String value)
	{
		InfoPanel info = new InfoAssignmentPanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showAssignment

	/**************************************************
     *  Detail Constructor 
     * @param WindowNo  WindowNo
     * @param tableName tableName
     * @param keyColumn keyColumn   
     * @param whereClause   whereClause
	 */
	protected InfoPanel (int WindowNo,
		String tableName, String keyColumn,boolean multipleSelection,
		 String whereClause)
	{
		this(WindowNo, tableName, keyColumn, multipleSelection, whereClause, true);
	}
	
	protected InfoPanel (int WindowNo,
			String tableName, String keyColumn,boolean multipleSelection,
			 String whereClause, boolean lookup){
		this(WindowNo, tableName, keyColumn, multipleSelection, whereClause,
				lookup, 0);
	}
	
	protected InfoPanel(int WindowNo, String tableName, String keyColumn, boolean multipleSelection, String whereClause,
			boolean lookup, int ADInfoWindowID) 
	{
		this(WindowNo, tableName, keyColumn, multipleSelection, whereClause,
				lookup, ADInfoWindowID, 0);	
	}
	
	/**************************************************
	 * Detail Constructor
	 * 
	 * @param WindowNo WindowNo
	 * @param tableName tableName
	 * @param keyColumn keyColumn
	 * @param whereClause whereClause
	 */
	protected InfoPanel(int WindowNo, String tableName, String keyColumn, boolean multipleSelection, String whereClause,
			boolean lookup, int ADInfoWindowID, int record_ID)
	{
		log.info("WinNo=" + p_WindowNo + " " + whereClause);
		if (WindowNo <= 0)
		{
			p_WindowNo = SessionManager.getAppDesktop().registerWindow(this);
		}
		else
		{
			p_WindowNo = WindowNo;
		}

    	MTable table = MTable.get(Env.getCtx(), tableName);
    	if (table != null)
    		m_TSCapable = table.isTSCapable();

    	m_Record_ID = record_ID;
		p_tableName = tableName;
		this.m_infoWindowID = ADInfoWindowID;

		p_keyColumn = keyColumn;
		p_multipleSelection = multipleSelection;
		m_lookup = lookup;

		loadInfoWindowData();
		
		
		if (whereClause == null || whereClause.indexOf('@') == -1)
		{
			p_whereClause = whereClause == null ? "" : whereClause;
		}
		else
		{
			p_whereClause = Env.parseContext(Env.getCtx(), p_WindowNo, whereClause, false, false);
			if (p_whereClause.length() == 0)
				log.log(Level.SEVERE, "Cannot parse context= " + whereClause);
		}

		init();

		this.setAttribute(ITabOnSelectHandler.ATTRIBUTE_KEY, new ITabOnSelectHandler() {
			public void onSelect()
			{
				scrollToSelectedRow();
			}

			@Override
			public void onSelectionRemove()
			{
			}
		});

		// infoWindow = MInfoWindow.get(p_keyColumn.replace("_ID", ""), null);
		// addEventListener(WindowContainer.ON_WINDOW_CONTAINER_SELECTION_CHANGED_EVENT,
		// this);
		addEventListener(ON_RUN_PROCESS, this);
		addEventListener(Events.ON_CLOSE, this);
		contentPanel.setMultiple(p_multipleSelection);
		contentPanel.setMultiSelection(true);
		contentPanel.setCheckmark(true);

	} // InfoPanel
	
	private void init()
	{
		if (isModal())
		{
			setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
			setBorder("normal");
			setClosable(true);
			int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 85 / 100;
			int width = SessionManager.getAppDesktop().getClientInfo().desktopWidth * 80 / 100;
			ZKUpdateUtil.setWindowWidthX(this, width);
			ZKUpdateUtil.setWindowHeightX(this, height);
    		this.setContentStyle("overflow: auto");
		}
		else
		{
			setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
			setBorder("none");
			ZKUpdateUtil.setWidth(this, "100%");
			ZKUpdateUtil.setHeight(this, "100%");
			setStyle("position: absolute;");
		}
		
        confirmPanel = new ConfirmPanel(true, true, true, true, true, true);  // Elaine 2008/12/16
        confirmPanel.addActionListener(Events.ON_CLICK, this);
        ZKUpdateUtil.setHeight(confirmPanel, "30px");

//		no need for now        
//		if (p_multipleSelection) {
//			Button button = confirmPanel.createButton(FORCE_SELECT_ALL);
//			button.setIconSclass(Env.getZKFontIcon("check-square"));
//			button.setTooltiptext("Force Select All");
//			confirmPanel.addButton(button);
//			button.addEventListener(Events.ON_CLICK, this);
//		}
		
        // Elaine 2008/12/16
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(hasCustomize());
		ZKUpdateUtil.setHeight(confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE), "30px");
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(hasHistory());
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(hasZoom());
		confirmPanel.getButton(ConfirmPanel.A_RESET).setVisible(hasReset());
		
		//
		if (!isLookup()) 
		{
			confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
		}
		
        this.setSizable(true);      
        this.setMaximizable(true);
        
        this.addEventListener(Events.ON_OK, this);
        if (isLookup())
        	addEventListener(Events.ON_CANCEL, this);
        contentPanel.setOddRowSclass(null);
        contentPanel.addEventListener("onAfterRender", this);
        this.setSclass("info-panel");
		contentPanel.setStyle("overflow: hidden"); // not working - force in CSS - jobrian
		
	}  //  init

	/**
	 *  Loaded correctly
	 *  @return true if loaded OK
	 */
	public boolean loadedOK()
	{
		return p_loadedOK;
	}   //  loadedOK

	/**
	 *	Set Status Line
	 *  @param text text
	 *  @param error error
	 */
	public void setStatusLine (String text, boolean error)
	{
		statusBar.setStatusLine(text, error);
	}	//	setStatusLine

	/**
	 *	Set Status DB
	 *  @param text text
	 */
	public void setStatusDB (String text)
	{
		statusBar.setStatusDB(text);
	}	//	setStatusDB

	/**
	 *	Set Status DB
	 *  @param text text
	 */
	public void setStatusSelected ()
	{
		if (!p_multipleSelection)
			return;
		
		int selectedCount = contentPanel.getSelectedCount();		

		if (selectedCount == 0) {
			statusBar.setSelectedRowNumber(null);
			return;
		}
		String msg = MessageFormat.format("[ Selected {0} Record{1} ] ", selectedCount, selectedCount > 1 ? "s" : "");  
		statusBar.setSelectedRowNumber(msg);
	}	//	setStatusSelected
	
	

	protected void prepareTable(ColumnInfo[] layout, String from, String where, String orderBy)
	{
		String sql = contentPanel.prepareTable(layout, from, where, p_multipleSelection, getTableName(), false);
		p_layout = contentPanel.getLayout();
		m_sqlMain = sql;
		m_sqlCount = "SELECT COUNT(*) FROM " + from + " WHERE " + where;
		m_sqlOrder = "";
		// m_sqlUserOrder = "";
		if (orderBy != null && orderBy.length() > 0)
			m_sqlOrder = " ORDER BY " + orderBy;
	} // prepareTable

	
	/**************************************************************************
	 *  Execute Query
	 */
	protected void executeQuery()
	{
		line = new ArrayList<Object>();
		cacheStart = -1;
		cacheEnd = -1;
		
		indexColumnCount = 0;
		columnDataIndex.clear();
		 
		testCount();
		m_useDatabasePaging = (m_count > 1000);
		if (m_useDatabasePaging || m_count <= 0)
		{			
			return ;
		}
		else
		{
			readLine(0, -1);
		}
	}
            
	private void readData(ResultSet rs) throws SQLException
	{
		lsReadedColumn.clear();

		int colOffset = 1; // columns start with 1
		List<Object> data = new ArrayList<Object>();
		for (int col = 0; col < p_layout.length; col++)
		{
			Object value = null;
			Class<?> c = p_layout[col].getColClass();
			int colIndex = col + colOffset;
			if (c == IDColumn.class)
			{
				value = new IDColumn(rs.getInt(colIndex));

			}
			else if (c == Boolean.class)
				value = "Y".equals(rs.getString(colIndex));
			else if (c == Timestamp.class)
				value = rs.getTimestamp(colIndex);
			else if (c == BigDecimal.class)
				value = rs.getBigDecimal(colIndex);
			else if (c == Double.class)
				value = rs.getDouble(colIndex);
			else if (c == Integer.class)
				value = rs.getInt(colIndex);
			else if (c == KeyNamePair.class)
			{
				if (p_layout[col].isKeyPairCol())
				{
					String display = rs.getString(colIndex);
					int key = rs.getInt(colIndex + 1);
					if (!rs.wasNull())
					{
						value = new KeyNamePair(key, display);
					}

					colOffset++;
				}
				else
				{
					int key = rs.getInt(colIndex);
					if (!rs.wasNull())
					{
						WEditor editor = editorMap.get(p_layout[col].getColSQL());
						if (editor != null)
						{
							editor.setValue(key);
							value = new KeyNamePair(key, editor.getDisplayTextForGridView(key));
						}
						else
						{
							value = new KeyNamePair(key, Integer.toString(key));
						}
					}
				}
			}
			else if (c == ValueNamePair.class)
			{
				String key = rs.getString(colIndex);
				WEditor editor = editorMap.get(p_layout[col].getColSQL());
				if (editor != null)
				{
					value = new ValueNamePair(key, editor.getDisplayTextForGridView(key));
				}					
				else
				{
					value = new ValueNamePair(key, key);
				}
			}			
			else
			{
				value = rs.getString(colIndex);
			}
			data.add(value);
		}
		line.add(data);

        
        appendDataForViewID(rs, data, lsReadedColumn);
        
        appendDataForParentLink(rs, data, lsReadedColumn);
        
        appendDataForKeyView (rs, data, lsReadedColumn);
	}
    
	/**
	 * save data of all viewID column in infoProcessList to end of data line
	 * when override {@link #readData(ResultSet)} consider call this method 
	 * IDEMPIERE-1970
	 * @param rs record set to read data
	 * @param data data line to append
	 * @param listReadedColumn list column is appended
	 * @throws SQLException
	 */
	protected void appendDataForViewID(ResultSet rs, List<Object> data, List<Integer> listReadedColumn) throws SQLException {
		appendInfoColumnData(rs, data, infoProcessList, listReadedColumn);
	}
	
	/**
	 * save data of all viewID column in infoProcessList to end of data line
	 * when override {@link #readData(ResultSet)} consider call this method 
	 * IDEMPIERE-2152
	 * @param rs
	 * @param data
	 * @param listReadedColumn
	 * @throws SQLException
	 */
	protected void appendDataForParentLink(ResultSet rs, List<Object> data, List<Integer> listReadedColumn) throws SQLException {
		appendInfoColumnData(rs, data, relatedInfoList, listReadedColumn);
	}
	
	/**
	 * save data of all viewID column in infoProcessList to end of data line
	 * when override {@link #readData(ResultSet)} consider call this method 
	 * IDEMPIERE-1970
	 * @param rs record set to read data
	 * @param data data line to append
	 * @param listReadedColumn list column is appended
	 * @throws SQLException
	 */
	protected void appendDataForKeyView(ResultSet rs, List<Object> data, List<Integer> listReadedColumn) throws SQLException {
		if (isNeedAppendKeyViewData())
			appendInfoColumnData(rs, data, new IInfoColumn [] {keyColumnOfView}, listReadedColumn);
	}
	
	/**
	 * save data of all infoColumn in listModelHaveInfoColumn to end of data line
	 * @param rs record set to read data
	 * @param data data line to append
	 * @param listModelHasInfoColumn
	 * @param listReadedColumn list column is appended
	 * @throws SQLException
	 */
	protected void appendInfoColumnData(ResultSet rs, List<Object> data, IInfoColumn [] listModelHasInfoColumn, List<Integer> listReadedColumn) throws SQLException {
		if (listModelHasInfoColumn == null || listModelHasInfoColumn.length == 0) {
			return;
		}
		
		// get InfoColumn from each modelHaveInfoColumn, read it form resultSet by name and append to data line
		for (IInfoColumn modelHasInfoColumn : listModelHasInfoColumn){
			// have no InfoColumn or this column is readed, read next column
			if (modelHasInfoColumn.getInfoColumnID() <= 0 || listReadedColumn.contains(modelHasInfoColumn.getInfoColumnID()))
				continue;

			MInfoColumn infoColumnAppend = (MInfoColumn) modelHasInfoColumn.getAD_InfoColumn();
			//TODO: improve read data to get data by data type of column.
			String appendData = null;
			try {
				appendData = rs.getString(infoColumnAppend.getColumnName());
			} catch (SQLException e) {
				appendData = null;
			}
			if (rs.wasNull()) {
				appendData = null;
			}
			data.add(appendData);
			
			// when need update append column index, just update it.
			if (isMustUpdateColumnIndex && !columnDataIndex.containsKey(modelHasInfoColumn.getInfoColumnID())){
				columnDataIndex.put(modelHasInfoColumn.getInfoColumnID(), indexColumnCount);
				indexColumnCount++;
			}
			
			// mark this column is readed
			listReadedColumn.add(modelHasInfoColumn.getInfoColumnID());
		}

	}
	
    protected void renderItems()
    {
        if (m_count > 0)
        {
        	if (m_count > PAGE_SIZE)
        	{
        		if (paging == null) 
        		{
	        		paging = new Paging();
	    			paging.setPageSize(PAGE_SIZE);
	    			paging.setTotalSize(m_count);
	    			paging.setDetailed(true);
	    			paging.addEventListener(ZulEvents.ON_PAGING, this);
	    			insertPagingComponent();
        		}
        		else
        		{
        			paging.setTotalSize(m_count);
        			paging.setActivePage(0);
        		}
    			List<Object> subList = readLine(0, PAGE_SIZE);
    			model = new ListModelTable(subList);
    			model.setSorter(this);
    			model.setMultiple(p_multipleSelection);
	            model.addTableModelListener(this);
	            contentPanel.setData(model, null);
	            
	            pageNo = 0;
        	}
        	else
        	{
        		if (paging != null) 
        		{
        			paging.setTotalSize(m_count);
        			paging.setActivePage(0);
        			pageNo = 0;
        		}
	            model = new ListModelTable(readLine(0, -1));
	            model.setSorter(this);
	            model.addTableModelListener(this);
	            model.setMultiple(p_multipleSelection);
	            contentPanel.setData(model, null);
        	}
        }
        else
		{
			if (paging != null)
			{
				paging.setTotalSize(m_count);
				paging.setActivePage(0);
				pageNo = 0;
			}
			model = new ListModelTable(new ArrayList<Object>());
			model.setSorter(this);
			model.addTableModelListener(this);
			model.setMultiple(p_multipleSelection);
			contentPanel.setData(model, null);
		}
        
        //jobriant
        if  (m_count == 1) {
        	contentPanel.setSelectedIndex(0);
        	SelectEvent se = new SelectEvent("onSelect", contentPanel, null, contentPanel.getSelectedItem());
        	Events.sendEvent(contentPanel, se);
        	//refresh(contentPanel.getValueAt(row,2), M_Warehouse_ID, M_PriceList_Version_ID);
        }
        // metas c.ghita@metas.ro : start
        int no = m_count;
        setStatusLine(Integer.toString(no) + " " + Msg.getMsg(Env.getCtx(), "SearchRows_EnterQuery"), false);
        setStatusDB(Integer.toString(no));
        setStatusSelected ();
        addDoubleClickListener();
        initTableColumnWidth();
    }
    
	private List<Object> readLine(int start, int end)
	{
		// cacheStart & cacheEnd - 1 based index, start & end - 0 based index
		if (cacheStart >= 1 && cacheEnd > cacheStart)
		{
			if (start + 1 >= cacheStart && end + 1 <= cacheEnd)
			{
				return end == -1 ? line : line.subList(start - cacheStart + 1, end - cacheStart + 2);
			}
		}

		cacheStart = start + 1 - (PAGE_SIZE * 4);
		if (cacheStart <= 0)
			cacheStart = 1;

		if (end == -1)
		{
			cacheEnd = m_count;
		}
		else
		{
			cacheEnd = end + 1 + (PAGE_SIZE * 4);
			if (cacheEnd > m_count)
				cacheEnd = m_count;
		}

		line = new ArrayList<Object>();

		PreparedStatement m_pstmt = null;
		ResultSet m_rs = null;

		long startTime = System.currentTimeMillis();
		//
		String dataSql = buildDataSQL(start, end);
		if (log.isLoggable(Level.FINER))
			log.finer(dataSql);
		try
		{
			m_pstmt = DB.prepareStatement(dataSql, null);
			setParameters(m_pstmt, false); // no count
			if (log.isLoggable(Level.FINE))
				log.fine("Start query - " + (System.currentTimeMillis() - startTime) + "ms");
			m_pstmt.setFetchSize(100);
			m_rs = m_pstmt.executeQuery();
			if (log.isLoggable(Level.FINE))
				log.fine("End query - " + (System.currentTimeMillis() - startTime) + "ms");
			// skips the row that we dont need if we can't use native db paging
			if (end > start && m_useDatabasePaging && !DB.getDatabase().isPagingSupported())
			{
				for (int i = 0; i < cacheStart - 1; i++)
				{
					if (!m_rs.next())
						break;
				}
			}

			int rowPointer = cacheStart - 1;
			while (m_rs.next())
			{
				rowPointer++;
				readData(m_rs);
				// check now of rows loaded, break if we hit the suppose end
				if (m_useDatabasePaging && rowPointer >= cacheEnd)
				{
					break;
				}
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, dataSql, e);
		}
		finally
		{
			DB.close(m_rs, m_pstmt);
			m_rs = null;
			m_pstmt = null;
		}

		if (end >= cacheEnd || end <= 0)
		{
			end = cacheEnd - 1;
		}

		if (end == -1)
		{
			return line;
		}
		else
		{
			int fromIndex = start - getCacheStart() + 1;
			int toIndex = end - getCacheStart() + 2;
			if (toIndex > line.size())
				toIndex = line.size();
			return line.subList(fromIndex, toIndex);
		}
	}

	protected String buildDataSQL(int start, int end)
	{
		String dataSql;
		String dynWhere = getSQLWhere();
		StringBuilder sql = new StringBuilder(m_sqlMain);
		if (dynWhere.length() > 0)
			sql.append(dynWhere); // includes first AND

		if (sql.toString().trim().endsWith("WHERE"))
		{
			int index = sql.lastIndexOf(" WHERE");
			sql.delete(index, sql.length());
		}
		if (m_sqlUserOrder != null && m_sqlUserOrder.trim().length() > 0)
			sql.append(m_sqlUserOrder);
		else
			sql.append(m_sqlOrder);
		dataSql = Msg.parseTranslation(Env.getCtx(), sql.toString()); // Variables
		dataSql = MRole.getDefault().addAccessSQL(dataSql, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
		if (end > start && m_useDatabasePaging && DB.getDatabase().isPagingSupported())
		{
			dataSql = DB.getDatabase().addPagingSQL(dataSql, getCacheStart(), cacheEnd);
		}
		return dataSql;
	}

    private void addDoubleClickListener() {
		Iterator<?> i = contentPanel.getEventListeners(Events.ON_DOUBLE_CLICK).iterator();
		while (i.hasNext()) {
			if (i.next() == this)
				return;
		}
		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		contentPanel.addEventListener(Events.ON_SELECT, this);
	}
    
    protected void insertPagingComponent() {
		contentPanel.getParent().insertBefore(paging, contentPanel.getNextSibling());
	}
    
    public Vector<String> getColumnHeader(ColumnInfo[] p_layout)
    {
        Vector<String> columnHeader = new Vector<String>();
        
        for (ColumnInfo info: p_layout)
        {
             columnHeader.add(info.getColHeader());
        }
        return columnHeader;
    }
	/**
	 * 	Test Row Count
	 *	@return true if display
	 */
	private boolean testCount()
	{
		long start = System.currentTimeMillis();
		String dynWhere = getSQLWhere();
		StringBuffer sql  = null;
		String countSql = null;
		
		if (infoWindow != null && !Strings.isBlank(infoWindow.getOtherClause())) {
			//needs to rewrite count sql because of grouping
			dynWhere = MRole.getDefault().addAccessSQL(dynWhere, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
			
			dynWhere = dynWhere.replace("WHERE", " AND ");
			sql = new StringBuffer (
					"SELECT COUNT (*) FROM ( "
					+ " SELECT 1 FROM " + infoWindow.getFromClause() + " WHERE " + p_whereClause + dynWhere 
					+ " " + infoWindow.getOtherClause() + ") as t"
					
					);
			
			countSql = Env.parseContext(Env.getCtx(), p_WindowNo, sql.toString(), true);
			
		} else {
			
			sql =  new StringBuffer(Env.parseContext(Env.getCtx(), p_WindowNo, m_sqlCount, true));
			
			if (dynWhere.length() > 0)
				sql.append(dynWhere);   //  includes first AND
			
			countSql = Msg.parseTranslation(Env.getCtx(), sql.toString());	//	Variables
			if (countSql.trim().endsWith("WHERE"))
			{
				countSql = countSql.trim();
				countSql = countSql.substring(0, countSql.length() - 5);
			}
			countSql = MRole.getDefault().addAccessSQL(countSql, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);

		}
		if (log.isLoggable(Level.FINER))
			log.finer(countSql);
		m_count = -1;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(countSql, null);
			setParameters (pstmt, true);
			//initialId = 0; // clear initial query;
			rs = pstmt.executeQuery();
		
			if (rs.next())
				m_count = rs.getInt(1);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, countSql, e);
			m_count = -2;
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		if (log.isLoggable(Level.FINE))
			log.fine("#" + m_count + " - " + (System.currentTimeMillis() - start) + "ms");

		return true;
	}	//	testCount
			

	/**
	 *	Save Selection	- Called by dispose
	 */
	protected void saveSelection ()
	{
		// Already disposed
		if (contentPanel == null)
			return;

		if (log.isLoggable(Level.CONFIG))
			log.config("OK=" + m_ok);
		// clear prev selected result
		m_results.clear();

		if (!m_ok) // did not press OK
		{
			contentPanel = null;
			this.detach();
			return;
		}

		// Multi Selection
		if (p_multipleSelection)
		{
			m_results.addAll(getSelectedRowKeys());
		}
		else // singleSelection
		{
			Integer data = getSelectedRowKey();
			if (data != null)
				m_results.add(data);
		}

		log.config(getSelectedSQL());

		// Save Settings of detail info screens
		saveSelectionDetail();

	}	//	saveSelection

	/**
	 *  Get the key of currently selected row
	 *  @return selected key
	 */
	protected Integer getSelectedRowKey()
	{
		Integer key = contentPanel.getSelectedRowKey();
		
		return key;        
	}   //  getSelectedRowKey
	
	/**
     *  Get the keys of selected row/s based on layout defined in prepareTable
     *  @return IDs if selection present
     *  @author ashley
     */
    protected ArrayList<Integer> getSelectedRowKeys()
    {
        ArrayList<Integer> selectedDataList = new ArrayList<Integer>();
        
        if (contentPanel.getKeyColumnIndex() == -1)
        {
            return selectedDataList;
        }
        
        if (p_multipleSelection)
        {
        	int[] rows = contentPanel.getSelectedIndices();
            for (int row = 0; row < rows.length; row++)
            {
                Object data = contentPanel.getModel().getValueAt(rows[row], contentPanel.getKeyColumnIndex());
                if (data instanceof IDColumn)
                {
                    IDColumn dataColumn = (IDColumn)data;
                    selectedDataList.add(dataColumn.getRecord_ID());
                }
                else
                {
                    log.severe("For multiple selection, IDColumn should be key column for selection");
                }
            }
        }
        
        if (selectedDataList.size() == 0)
        {
        	int row = contentPanel.getSelectedRow();
    		if (row != -1 && contentPanel.getKeyColumnIndex() != -1)
    		{
    			Object data = contentPanel.getModel().getValueAt(row, contentPanel.getKeyColumnIndex());
    			if (data instanceof IDColumn)
    				selectedDataList.add(((IDColumn)data).getRecord_ID());
    			if (data instanceof Integer)
    				selectedDataList.add((Integer)data);
    		}
        }
      
        return selectedDataList;
    }   //  getSelectedRowKeys

	/**
	 * Get selected Keys as Collection
	 * 
	 * @deprecated use getSaveKeys
	 * @return selected keys (Integers)
	 */
	public Collection<Integer> getSelectedKeysCollection()
	{
		m_ok = true;
		saveSelection();
		if (!m_ok || m_results.size() == 0)
			return null;
		return m_results;
	}
    
	/**
	 * Save selected id, viewID of all process to map viewIDMap to save into T_Selection
	 */
	public Collection<KeyNamePair> getSaveKeys (int infoColumnID){
		// clear result from prev time
		m_viewIDMap.clear();
		
		if (p_multipleSelection)
        {
			Map <Integer, List<Object>> selectedRow = getSelectedRowInfo();
			
            for (Entry<Integer, List<Object>> selectedInfo : selectedRow.entrySet())
            {
            	// get key data column
                Integer keyData = selectedInfo.getKey();
                
                if (infoColumnID > 0){
                	// have viewID, get it
                	int dataIndex = columnDataIndex.get(infoColumnID) + p_layout.length;
                	
            		// get row data from model
					Object viewIDValue = selectedInfo.getValue().get(dataIndex);
                	
                	m_viewIDMap.add (new KeyNamePair(keyData, viewIDValue == null?null:viewIDValue.toString()));
                }else{
                	// hasn't viewID, set viewID value is null
                	m_viewIDMap.add (new KeyNamePair(keyData, null));
                }
                
            }
            
            return m_viewIDMap;
        }else{
        	// never has this case, because when have process, p_multipleSelection always is true
        	return null;
        }

	}
	/**
	 * need overrider at infoWindow to check isDisplay
	 * @return
	 */
	protected boolean isNeedAppendKeyViewData (){
		return false;
	}
	
	/**
	 * Check type of object is IDColumn
	 * @param keyData
	 * @param isCheckNull when true, raise exception when data is null
	 * @return
	 */
	protected boolean isIDColumn(Object keyData, boolean isCheckNull){
		if (isCheckNull && keyData == null){
			AdempiereException ex = getKeyNullException();
			log.severe(ex.getMessage());
			throw ex;
		}
		
		if (keyData != null && keyData instanceof IDColumn){
			return true;
		}
		
		return false;
	}
	
	/**
	 * call {@link #isIDColumn(Object, boolean)} without check null value
	 * @param keyData
	 * @return
	 */
	protected boolean isIDColumn(Object keyData){
		return isIDColumn(keyData, false);
	}
	
	/**
	 * get all selected record of current page and update to {@link #recordSelectedData}
	 * remove unselected record and add new selected record
	 * we maintain value of key, and extra value append by {@link #appendInfoColumnData(ResultSet, List, IInfoColumn[], List)} 
	 */
	protected void updateListSelected (){
		
		recordSelectedData.clear();
		int indices[] = contentPanel.getSelectedIndices();
		Arrays.sort(indices);
//		for (int i = indices.length -1; i >= 0; i--) {
		for (int i : indices ) {
			Integer keyCandidate = getColumnValue(i);
			List<Object> candidateRecord = (List<Object>)contentPanel.getModel().get(i);
			recordSelectedData.put(keyCandidate, candidateRecord);// add or update selected record info
		}
		
		/* This is buggy - keeping it in case it was overlook jtrinidad 08/10/2020
		 * 
		 * for (int rowIndex = 0; rowIndex < contentPanel.getModel().getRowCount();
		 * rowIndex++){ Integer keyCandidate = getColumnValue(rowIndex);
		 * 
		 * @SuppressWarnings("unchecked") List<Object> candidateRecord =
		 * (List<Object>)contentPanel.getModel().get(rowIndex);
		 * 
		 * if (contentPanel.getModel().isSelected(candidateRecord)){
		 * recordSelectedData.put(keyCandidate, candidateRecord);// add or update
		 * selected record info }else{ if
		 * (recordSelectedData.containsKey(keyCandidate)){// unselected record
		 * List<Object> recordSelected = recordSelectedData.get(keyCandidate); IDColumn
		 * idcSel = null; if (recordSelected.get(0) instanceof IDColumn) { idcSel =
		 * (IDColumn) recordSelected.get(0); } IDColumn idcCan = null; if
		 * (candidateRecord.get(0) instanceof IDColumn) { idcCan = (IDColumn)
		 * candidateRecord.get(0); } if (idcSel != null && idcCan != null &&
		 * idcSel.getRecord_ID().equals(idcCan.getRecord_ID())) { recordSelected.set(0,
		 * candidateRecord.get(0)); // set same IDColumn for comparison } if
		 * (recordSelected.equals(candidateRecord)) {
		 * recordSelectedData.remove(keyCandidate); } } }
		 * 
		 * }
		 */	
	}
	
	/**
	 * get data index of keyView
	 * @return
	 */
	protected int getIndexKeyColumnOfView (){
		if (keyColumnOfView == null){
			return contentPanel.getKeyColumnIndex();
		}else if (isNeedAppendKeyViewData()){
			return columnDataIndex.get(keyColumnOfView.getInfoColumnID()) + p_layout.length;
		}else{
			return indexKeyOfView;
		}
	}	
	
	/**
	 * go through all data record, in case key value is in {@link #recordSelectedData}, mark it as selected record
	 */
	protected void restoreSelectedInPage (){
		if (!p_multipleSelection)
			return;
		
		Collection<Object> lsSelectionRecord = new ArrayList<Object>();
		for (int rowIndex = 0; rowIndex < contentPanel.getModel().getRowCount(); rowIndex++){
			Integer keyViewValue = getColumnValue(rowIndex);
			if (recordSelectedData.containsKey(keyViewValue)){
				// TODO: maybe add logic to check value of current record (focus only to viewKeys value) is same as value save in lsSelectedKeyValue
				// because record can change by other user
				lsSelectionRecord.add(contentPanel.getModel().get(rowIndex));
			}
		}
		
		contentPanel.getModel().setSelection(lsSelectionRecord);
		updateListSelected();
	}
	
	
	protected AdempiereException getKeyNullException (){
		String errorMessage = String.format("has null value at column %1$s use as key of view in info window %2$s", 
				keyColumnOfView == null ? p_keyColumn : keyColumnOfView, infoWindow.getName());
		return new AdempiereException(errorMessage);
	}
	/**
	 * get keyView value at rowIndex and clumnIndex
	 * also check in case value is null will rise a exception
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	protected Integer getColumnValue (int rowIndex){
		
		int keyIndex = getIndexKeyColumnOfView();
		Integer keyValue = null;
    	// get row data from model
		Object keyColumValue = contentPanel.getModel().getDataAt(rowIndex, keyIndex);
		// throw exception when value is null
		if (keyColumValue == null){
			AdempiereException ex = getKeyNullException();
			log.severe(ex.getMessage());
			throw ex;
		}
		
		// IDColumn is recreate after change page, because use value of IDColumn
		if (keyColumValue != null && keyColumValue instanceof IDColumn){
			keyColumValue = ((IDColumn)keyColumValue).getRecord_ID();
		}
		
		if (keyColumValue instanceof Integer){
			keyValue = (Integer)keyColumValue;
		}else {
			String msg = "column play keyView should is integer";
			AdempiereException ex = new AdempiereException (msg);
			log.severe(msg);
			throw ex;
		}
		
		return (Integer)keyValue;
	}

	
	/**
	 * in case requery data, but want store selected record (example when run success a process)
	 * we must sync selected row, because some selected row maybe not at data list (process make it change not map with query)
	 * current 1000 line cache 
	 * because in case query get more 1000 record we can't sync or maintain selected record (ever maintain for current page will make user confuse).
	 * just clear selection
	 * in case < 1000 record is ok
	 * TODO:rewrite
	 */
	protected void syncSelectedAfterRequery (){
		if (isRequeryByRunSuccessProcess){
			isRequeryByRunSuccessProcess = false;
			//TODO:it's hard to ensure in case use keyViewId we can re-sync. some issue:
			// + after RunSuccessProcess maybe key of record is change.
			// + after RunSuccessProcess maybe value of viewID change.
			// + after RunSuccessProcess maybe some record is out of query result
			// + when load many page, sync at one time effect to performance
			// maybe make two list, just sync for first page, old list use for reference, 
			// when user change page will use it for restore selected record, synced record will copy to new list
		}
	}
	
	/**
	 * update list column key value of selected record and return this list
	 * @return {@link #recordSelectedData} after update 
	 */
	public Map<Integer, List<Object>> getSelectedRowInfo (){
		//updateListSelected();  jtrinidad - duplicate call
		return recordSelectedData;
	}
	
	/**
	 * Get selected Keys
	 * 
	 * @return selected keys (Integers)
	 */
	public Object[] getSelectedKeys()
	{
		if (!m_ok || m_results.size() == 0)
			return null;
		return m_results.toArray(new Integer[0]);
	} // getSelectedKeys;

	/**
	 * Get (first) selected Key
	 * 
	 * @return selected key
	 */
	public Object getSelectedKey()
	{
		if (!m_ok || m_results.size() == 0)
			return null;
		return m_results.get(0);
	} // getSelectedKey

	/**
	 *	Is cancelled?
	 *	- if pressed Cancel = true
	 *	- if pressed OK or window closed = false
	 *  @return true if cancelled
	 */
	public boolean isCancelled()
	{
		return m_cancel;
	}	//	isCancelled

	/**
	 *	Get where clause for (first) selected key
	 *  @return WHERE Clause
	 */
	public String getSelectedSQL()
	{
		// No results
		Object[] keys = getSelectedKeys();
		if (keys == null || keys.length == 0)
		{
			log.config("No Results - OK=" + m_ok + ", Cancel=" + m_cancel);
			return "";
		}
		//
		StringBuffer sb = new StringBuffer(getKeyColumn());
		if (keys.length > 1)
			sb.append(" IN (");
		else
			sb.append("=");

		// Add elements
		for (int i = 0; i < keys.length; i++)
		{
			if (getKeyColumn().endsWith("_ID"))
				sb.append(keys[i].toString()).append(",");
			else
				sb.append("'").append(keys[i].toString()).append("',");
		}

		sb.replace(sb.length() - 1, sb.length(), "");
		if (keys.length > 1)
			sb.append(")");
		return sb.toString();
	}	//	getSelectedSQL;

		
	/**
	 * query ADInfoWindow from ADInfoWindowID
	 */
	protected void loadInfoWindowData (){}

		
	/**
	 *  Get Table name Synonym
	 *  @return table name
	 */
	protected String getTableName()
	{
		return p_tableName;
	}   //  getTableName

	/**
	 *  Get Key Column Name
	 *  @return column name
	 */
	protected String getKeyColumn()
	{
		return p_keyColumn;
	}   //  getKeyColumn

	
	public String[] getEvents()
    {
        return InfoPanel.lISTENER_EVENTS;
    }
	/**
	 * enable all control button or disable all rely to selected record 
	 */
	protected void enableButtons (){
		boolean enable = (contentPanel.getSelectedCount() > 0);
		enableButtons(enable);
	}
	
	// Elaine 2008/11/28
	/**
	 * enable or disable all control button
	 *  Enable OK, History, Zoom if row/s selected
     *  ---
     *  Changes: Changed the logic for accommodating multiple selection
     *  @author ashley
	 */
	protected void enableButtons (boolean enable)
	{
		confirmPanel.getOKButton().setEnabled(enable); // red1 allow Process for
														// 1 or more records

		if (hasHistory())
			confirmPanel.getButton(ConfirmPanel.A_HISTORY).setEnabled(enable);
		if (hasZoom())
			confirmPanel.getButton(ConfirmPanel.A_ZOOM)
					.setEnabled(!enable ? enable : (contentPanel.getSelectedCount() == 1)); 
		if (hasProcess())
			confirmPanel.getButton(ConfirmPanel.A_PROCESS).setEnabled(enable);

		for (Button btProcess : btProcessList)
		{
			boolean isAlwaysOn = (boolean) btProcess.getAttribute(ATT_BTN_ALWAYS_ON);
			if (!isAlwaysOn) {
				btProcess.setEnabled(enable);
			}
		}
		if (btCbbProcess != null)
		{
			btCbbProcess.setEnabled(enable);
		}

		if (btMenuProcess != null)
		{
			btMenuProcess.setEnabled(enable);
		}

		if (cbbProcess != null)
		{
			cbbProcess.setEnabled(enable);
		}
	}   //  enableButtons
	//
		
	/**************************************************************************
	 *  Get dynamic WHERE part of SQL
	 *	To be overwritten by concrete classes
	 *  @return WHERE clause
	 */
	protected abstract String getSQLWhere();
      	
	/**
	 *  Set Parameters for Query
	 *	To be overwritten by concrete classes
	 *  @param pstmt statement
	 *  @param forCount for counting records
	 *  @throws SQLException
	 */
	protected abstract void setParameters (PreparedStatement pstmt, boolean forCount) 
		throws SQLException;
    /**
     * notify to search editor of a value change in the selection info
     * @param event event
    *
     */

	protected void showHistory()					{}
	/**
	 *  Has History (false)
	 *	To be overwritten by concrete classes
	 *  @return true if it has history (default false)
	 */
	protected boolean hasHistory()				{return false;}
	/**
	 *  Customize dialog
	 *	To be overwritten by concrete classes
	 */
	protected boolean hasProcess()				{return false;}

	/**
	 * Customize dialog - To be overwritten by concrete classes
	 */
	protected void customize()
	{
		ListitemRenderer<Object> render = contentPanel.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer renderer = (WListItemRenderer) render;
			String customize = renderer.getColumnCustomization();
			boolean userPreference = false;
			MRole currRole = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
			if (currRole.isCanSaveColumnWidthEveryone())
			{
				FDialog.ask(getWindowNo(), this, 
						"This change apply to everyone who has not set their own preference.", 
						new Callback<Boolean>() {

					@Override
					public void onCallback(Boolean userPreference) {
						customize0(customize, userPreference);
					}
					
				});
			}
			else
			{
				customize0(customize, userPreference);
			}
			
		}
	}

	private void customize0(String customize, boolean userPreference) {
		WListItemRenderer renderer;
		boolean ok = MTabCustomization.saveInfoData(Env.getCtx(), m_infoWindowID, Env.getAD_User_ID(Env.getCtx()),
				customize, null);
		if (ok && userPreference && Env.getAD_User_ID(Env.getCtx()) != MTabCustomization.SUPERUSER)
			ok = MTabCustomization.saveInfoData(Env.getCtx(), m_infoWindowID, MTabCustomization.SUPERUSER,
					customize, null);
		
		if (infoSubTabs != null && infoSubTabs.size() > 0) {
			int idx = 1;  // 0 is reserved for the main tab
			for (WListbox tbl : infoSubTabs) {
				renderer = (WListItemRenderer) tbl.getItemRenderer();
				String sc = renderer.getColumnCustomization();
				if (sc == null) {
					continue;
				}
				ok = MTabCustomization.saveInfoData(Env.getCtx(), m_infoWindowID, idx, Env.getAD_User_ID(Env.getCtx()),
						sc, null);
				if (ok && userPreference && Env.getAD_User_ID(Env.getCtx()) != MTabCustomization.SUPERUSER)
					ok = MTabCustomization.saveInfoData(Env.getCtx(), m_infoWindowID, idx, MTabCustomization.SUPERUSER,
							sc, null);					
				idx++;
			}
		}
		
		if (ok)
			setStatusLine("Preference save successfully.", false);
		else
			setStatusLine("Preference cannot be saved", true);
	}

	/**
	 * Has Customize (true) - To be overwritten by concrete classes
	 * 
	 * @return true if it has customize (default true)
	 */
	protected boolean hasCustomize()
	{
		return true;
	}

	/**
	 *  Has Reset (false)
	 *	To be overwritten by concrete classes
	 *  @return true if it has zoom (default false)
	 */
	protected boolean hasReset()					{return false;}
	
	/**
	 *  Has Zoom (false)
	 *	To be overwritten by concrete classes
	 *  @return true if it has zoom (default false)
	 */
	protected boolean hasZoom()					{return false;}
	/**
	 *  Save Selection Details
	 *	To be overwritten by concrete classes
	 */
	protected void saveSelectionDetail()          {}

	/**
	 * If enabled, user can key in lookup and avail of the Full Text search functionality
	 * @return
	 */
	protected boolean isSupportTokenSearch()
	{
		return false;
	}
	/**
	 * 	Get Zoom Window
	 *	@param tableName table name
	 *	@param isSOTrx sales trx
	 *	@return AD_Window_ID
	 */
	protected int getAD_Window_ID (String tableName, boolean isSOTrx)
	{
		if (!isSOTrx && m_PO_Window_ID > 0)
			return m_PO_Window_ID;
		if (m_SO_Window_ID > 0)
			return m_SO_Window_ID;
		//
		String sql = "SELECT AD_Window_ID, PO_Window_ID FROM AD_Table WHERE TableName=?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setString(1, tableName);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				m_SO_Window_ID = rs.getInt(1);
				m_PO_Window_ID = rs.getInt(2);
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		if (!isSOTrx && m_PO_Window_ID > 0)
			return m_PO_Window_ID;
		
		return m_SO_Window_ID;
	}	//	getAD_Window_ID
    
	public void onEvent(Event event)
	{
		if (event == null)
			return;


    	if (event.getTarget() == fieldTokenSearch) {
    		if (event instanceof InputEvent) {
//    			fieldName.setValue(null);
//    			fieldValue.setValue(null);
    			InputEvent evt = (InputEvent) event;
    			String text = evt.getValue();
    			if (text.length() <= MTable.TS_MIN_CHARACTERS) {
    				m_TSQuery = null;
    				return;
    			}
    			
    			m_TSQuery = StringUtils.rewritetoTSQuery(text);
    			//TODO - update query without requiring to press the refresh button
    			onUserQuery();
    		}
    		
    		return;
    	}
    	
		if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK)))
		{
			onOk();
		}
		else if (event.getTarget() == contentPanel && event.getName().equals(Events.ON_SELECT))
		{
			setStatusSelected ();
			SelectEvent<?, ?> selectEvent = (SelectEvent<?, ?>) event;

			if (selectEvent.getReference() != null && selectEvent.getReference() instanceof Listitem)
			{
				Listitem m_lastOnSelectItem = (Listitem) selectEvent.getReference();
				m_lastSelectedIndex = m_lastOnSelectItem.getIndex();
				enableButtons();
			}

		}
		else if (event.getTarget() == contentPanel && event.getName().equals("onAfterRender"))
		{
			// this is not being executed - onAfterRender is not firing 
			// this event selected item from listBox and model is sync.
			enableButtons();
		}
		else if (event.getTarget() == contentPanel && event.getName().equals(Events.ON_DOUBLE_CLICK))
		{

			if (event.getClass().equals(MouseEvent.class))
			{
				return;
			}
			if (!confirmPanel.getButton(ConfirmPanel.A_OK).isEnabled()) 
			{
				return;
			}
			if (contentPanel.isMultiple() && m_lastSelectedIndex >= 0)
			{

				contentPanel.setSelectedIndex(m_lastSelectedIndex);

				// model.clearSelection(); // TODO: Need to implement Clearing
				// Selection with firing event.
//				List<Object> lsSelectedItem = new ArrayList<Object>();
//				lsSelectedItem.add(model.getElementAt(m_lastSelectedIndex));
//				model.setSelection(lsSelectedItem);

				int m_keyColumnIndex = contentPanel.getKeyColumnIndex();
				for (int i = 0; i < contentPanel.getRowCount(); i++)
				{
					// Find the IDColumn Key
					Object data = contentPanel.getModel().getValueAt(i, m_keyColumnIndex);
					if (data instanceof IDColumn)
					{
						IDColumn dataColumn = (IDColumn) data;

						if (i == m_lastSelectedIndex)
						{
							dataColumn.setSelected(true);
						}
						else
						{
							dataColumn.setSelected(false);
						}
					}
				}
			}
			onDoubleClick();
			contentPanel.repaint();
			m_lastSelectedIndex = -1;
		}
		else if (event.getTarget().equals(confirmPanel.getButton(FORCE_SELECT_ALL)))
		{
				contentPanel.renderAll();
				contentPanel.selectAll();
				setStatusSelected();
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_REFRESH)))
		{
			initialId = 0;   //drop all context where reset button is fired
			onUserQuery();
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
		{
			m_cancel = true;
			Event closeEvent = new Event(Events.ON_CLOSE, this);
			Events.sendEvent(closeEvent);
//			dispose(false);
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_RESET)))
		{
			resetParameters();
		}
		// Elaine 2008/12/16
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_HISTORY)))
		{
			if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
			{
				showHistory();
			}
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE)))
		{
			if (!contentPanel.getChildren().isEmpty())
			{
				customize();
			}
		}
		//
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_ZOOM)))
		{
			if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
			{
				zoom();
				if (isLookup())
					this.detach();
			}
		}

		// handle event click into process button start
		else if (ON_RUN_PROCESS.equals(event.getName()))
		{
			// hand echo event after click button process
			runProcess(event.getData());
		}
		else if (Events.ON_CLICK.equals(event.getName()) && event.getTarget() != null
				&& event.getTarget() instanceof Menuitem && event.getTarget().getAttribute(PROCESS_ID_KEY) != null)
		{
			// handle event when click to menu item of info process
			preRunProcess((Integer) event.getTarget().getAttribute(PROCESS_ID_KEY));
		}
		else if (event.getTarget().equals(btCbbProcess))
		{
			// click bt process in case display drop down list
			Comboitem cbbSelectedItem = cbbProcess.getSelectedItem();

			if (cbbSelectedItem == null || cbbSelectedItem.getValue() == null)
			{
				// do nothing when no process is selected
				return;
			}

			MProcess selectedProcess = (MProcess) cbbSelectedItem.getValue();
			preRunProcess(selectedProcess.getAD_Process_ID());
		}
		else if (event.getName().equals(Events.ON_CLICK) && btProcessList.contains(event.getTarget()))
		{
			// click bt process in case display button list
			Button btProcess = (Button) event.getTarget();
			boolean alwaysOn = (boolean) btProcess.getAttribute(ATT_BTN_ALWAYS_ON);
			p_ReRunQuery = !alwaysOn;
			MInfoProcess infoprocess = (MInfoProcess) btProcess.getAttribute(ATT_INFO_PROCESS_KEY);
			if (infoprocess.isReport()) {
				//TODO jtrinidad - how on report
				runReport(infoprocess);
			} else {
				preRunProcess(infoprocess.getAD_Process_ID());
				
			}
		}
		else if (event.getTarget() == paging)
		{
			int pgNo = paging.getActivePage();
			if (pageNo != pgNo)
			{
				contentPanel.clearSelection();

				pageNo = pgNo;
				int start = pageNo * PAGE_SIZE;
				int end = start + PAGE_SIZE;
				if (end >= m_count)
					end = m_count - 1;
				
				List<Object> subList = readLine(start, end);
				
				//restore if table is multi select 
				boolean isMultiSelect = model.isMultiple();
				
				model = new ListModelTable(subList);
				model.setMultiple(isMultiSelect);
				model.setSorter(this);
				model.addTableModelListener(this);
				contentPanel.setData(model, null);
			}
		}
		else if (event.getName().equals(Events.ON_CHANGE))
		{
		}
		// else if
		// (event.getName().equals(WindowContainer.ON_WINDOW_CONTAINER_SELECTION_CHANGED_EVENT))
		// {
		// if (infoWindow != null)
		// SessionManager.getAppDesktop().updateHelpContext(X_AD_CtxHelp.CTXTYPE_Info,
		// infoWindow.getAD_InfoWindow_ID());
		// else
		// SessionManager.getAppDesktop().updateHelpContext(X_AD_CtxHelp.CTXTYPE_Home,
		// 0);
		// }
		else if (event.getName().equals(Events.ON_CTRL_KEY))
		{
			KeyEvent keyEvent = (KeyEvent) event;
			if (LayoutUtils.isReallyVisible(this))
			{
				this.onCtrlKeyEvent(keyEvent);
			}
		}
		else if (event.getName().equals(Events.ON_OK))
		{// on ok when focus at non parameter component. example grid result
			if (m_lookup && contentPanel.getSelectedIndex() >= 0)
			{
				// do nothing when parameter not change and at window mode, or
				// at dialog mode but select non record
				onOk();
			}
		}
		else if (event.getName().equals(Events.ON_CANCEL)
				|| (event.getTarget().equals(this) && event.getName().equals(Events.ON_CLOSE)))
		{
			m_cancel = true;
			dispose(false);
		}
		// when user push enter keyboard at input parameter field
		else
		{
			// onUserQuery(); // captured now on control key
		}
	} // onEvent

	public static final int	VK_ENTER	= '\r';
	public static final int	VK_ESCAPE	= 0x1B;

	public void onCtrlKeyEvent(KeyEvent keyEvent)
	{
		if (keyEvent.isAltKey() && !keyEvent.isCtrlKey() && keyEvent.isShiftKey())
		{ // Shift+Alt
			if (keyEvent.getKeyCode() == KeyEvent.DOWN)
			{ // Shift+Alt+Down
				// navigate to results
				if (contentPanel.getRowCount() > 0)
				{
					contentPanel.setFocus(true);
					contentPanel.setSelectedIndex(0);
				}
			}
		}
		else if (keyEvent.getKeyCode() == VK_ENTER)
		{ // Enter
			// do nothing, let on_ok at infoWindo do, at this is too soon to get
			// value from control, it's not bind
		}
	}

	/**
	 * Call query when user click to query button enter in parameter field
	 */
	public void onUserQuery()
	{
		if (validateParameters())
		{
			showBusyDialog();
			isQueryByUser = true;
			Clients.response(new AuEcho(this, "onQueryCallback", null));
		}
	}

	/**
	 * validate parameter before run query
	 * 
	 * @return
	 */
	public boolean validateParameters()
	{
		return true;
	}

	/**
	 * Call after load parameter panel to set init value can call when reset
	 * parameter implement this method at inheritance class with each parameter,
	 * remember call Env.setContext to set new value to env
	 */
	protected void initParameters()
	{

	}

	/**
	 * Update relate info when selection in main info change
	 */
	protected void updateSubcontent (){};
	
	/**
	 * Reset parameter to default value or to empty value? implement at
	 * inheritance class when reset parameter maybe need init again parameter,
	 * reset again default value
	 */
	protected void resetParameters()
	{
	}

	void preRunProcess(Integer processId)
	{
		// disable all control button when run process
		//enableButtons(false); jtrinidad - skip as users are forced to select/unselect items
		// call run process in next request to disable all button control
		Events.echoEvent(ON_RUN_PROCESS, this, processId.toString());
	}

	protected void runReport(MInfoProcess report)
	{
		MPrintFormat pf = MPrintFormat.get(Env.getCtx(), 0, report.getAD_Table_ID());
		if (pf == null) {
			pf = MPrintFormat.createFromTable(Env.getCtx(), report.getAD_Table_ID());
		}
		MQuery query = new MQuery(report.getAD_Table_ID());
		String s = report.getWhereClause();
		if (s.contains(INFO_SELECTED_IDS)) {
			ArrayList<Integer> list = getSelectedRowKeys();
			if (list.size() == 0) {
				return;  // no selected items
			}
			String ids = list.toString().replace("[", "")
		            .replace("]", "");
			s = s.replace(INFO_SELECTED_IDS, ids );
		}
		
		query.addRestriction(s);
				
		PrintInfo info = new PrintInfo(pf.getName(), pf.getAD_Table_ID(), 0);
		info.setAD_PInstance_ID(1);
		ReportEngine re = new ReportEngine (Env.getCtx(), pf, query, info);
		ReportCtl.preview(re);
	}
	/**
	 * Run a process. show process dialog, before start process, save id of
	 * record selected after run process, show message report result
	 * 
	 * @param processIdObj
	 */
	protected void runProcess(Object processIdObj)
	{
		if (isProcessRun) 
			return;
		
		final Integer processId = Integer.parseInt(processIdObj.toString());
		final MProcess m_process = MProcess.get(Env.getCtx(), processId);
		final ProcessInfo m_pi = new ProcessInfo(m_process.getName(), processId);
		m_pi.setAD_User_ID(Env.getAD_User_ID(Env.getCtx()));
		m_pi.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
		m_pi.setRecord_ID(m_Record_ID);

		MPInstance instance = new MPInstance(Env.getCtx(), processId, 0);
		instance.saveEx();
		final int pInstanceID = instance.getAD_PInstance_ID();
		// Execute Process
		m_pi.setAD_PInstance_ID(pInstanceID);

		isProcessRun = true;
		
		if (!p_multipleSelection) {
			//Save row value to context for use in Parameter panel - jtrinidad
			int nColumns = contentPanel.getColumnCount();
			int row = contentPanel.getSelectedRow();
			
			int n = 0;
			for (MInfoColumn col : infoColumns) {
				String var = col.getColumnName();
				Object obj = contentPanel.getValueAt(row, n);
				if (obj instanceof Integer) {
					int val = (int) obj;
					Env.setContext(Env.getCtx(), p_WindowNo, var, val);
				} else if (obj instanceof ValueNamePair) {
					ValueNamePair vp = (ValueNamePair) obj;
					Env.setContext(Env.getCtx(), p_WindowNo, var, vp.getID());
				} else if (obj instanceof KeyNamePair) {
					KeyNamePair vp = (KeyNamePair) obj;
					Env.setContext(Env.getCtx(), p_WindowNo, var, vp.getKey());
				} else if (obj instanceof Boolean) {
					Env.setContext(Env.getCtx(), p_WindowNo, var, (Boolean) obj);
				} else if (obj instanceof BigDecimal) {
					//don't set it for now
					//Env.setContext(Env.getCtx(), p_WindowNo, var,  (BigDecimal) obj);
				} else if (obj instanceof Timestamp) {
					Env.setContext(Env.getCtx(), p_WindowNo, var,  (Timestamp) obj);
				} else if (obj instanceof IDColumn) {
					IDColumn id = (IDColumn) obj;
					Env.setContext(Env.getCtx(), p_WindowNo, var,  id.getRecord_ID());
				} else {
					//String
					Env.setContext(Env.getCtx(), p_WindowNo, var,  (String) obj);
				}
				n++;
			}
		}
		// to let process end with message and requery
		WProcessCtl.process(p_WindowNo, m_pi, (Trx) null, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception
			{
				ProcessModalDialog processModalDialog = (ProcessModalDialog) event.getTarget();
				if (ProcessModalDialog.ON_BEFORE_RUN_PROCESS.equals(event.getName()))
				{
					updateListSelected();
					// store in T_Selection table selected rows for Execute
					// Process that retrieves from T_Selection in code.
					if (p_multipleSelection) {
						DB.createT_SelectionNew(pInstanceID, getSaveKeys(getInfoColumnIDFromProcess(processModalDialog.getAD_Process_ID())),
								null);	
						saveResultSelection(getInfoColumnIDFromProcess(processModalDialog.getAD_Process_ID()));					
//					DB.createT_Selection(pInstanceID, getSaveKeys(), 
//							getProcessIndex(processModalDialog.getAD_Process_ID()), null);
//					saveResultSelection();
						createT_Selection_InfoWindow(pInstanceID);
						m_pi.setInfoSelectionValues(m_values);
					}
				}
				else if (ProcessModalDialog.ON_WINDOW_CLOSE.equals(event.getName()))
				{
					if (processModalDialog.isCancel())
					{
						m_results.clear();
						// enable/disable control button rely selected record
						// status
						enableButtons();
					}
					else if (m_pi.isError())
					{
						FDialog.error(p_WindowNo, m_pi.getSummary());
						// enable/disable control button rely selected record
						// status
//						enableButtons();
						if (p_ReRunQuery) {
							Clients.response(new AuEcho(InfoPanel.this, "onQueryCallback", null));
						}
					}
					else if (!m_pi.isError())
					{
						// when success, show process info and it's summary
						ProcessInfoUtil.setLogFromDB(m_pi);
						String adMessage = m_pi.getTitle() + ":\n" + Msg.getMsg(getContext(), "Success") + "\n";
						String logInfo = m_pi.getLogInfo();
						if (logInfo.length() > 0) {
							adMessage = adMessage + logInfo;
						}
						FDialog.info(p_WindowNo, null, adMessage);
						//TODO - do not run query when process button is always on.
						if (p_ReRunQuery) {
							Clients.response(new AuEcho(InfoPanel.this, "onQueryCallback", null));
						}
					}
				}
				isProcessRun = false;
				contentPanel.clearSelection();
			}
		});
		
	}

	  /**
		 * save result values
		 */
		protected void saveResultSelection(int infoColumnId) {
			int m_keyColumnIndex = contentPanel.getKeyColumnIndex();
			if (m_keyColumnIndex == -1) {
				return;
			}

			m_values = new LinkedHashMap<KeyNamePair,LinkedHashMap<String,Object>>();
			
			if (p_multipleSelection) {
					
				Map <Integer, List<Object>> selectedRow = getSelectedRowInfo();
				
				// for selected rows
	            for (Entry<Integer, List<Object>> selectedInfo : selectedRow.entrySet())
	            {
	            	// get key and viewID
	                Integer keyData = selectedInfo.getKey();
	                KeyNamePair kp = null;
	                
	                if (infoColumnId > 0){
	                	int dataIndex = columnDataIndex.get(infoColumnId) + p_layout.length;
	                	Object viewIDValue = selectedInfo.getValue().get(dataIndex);
	                	kp = new KeyNamePair(keyData, viewIDValue == null ? null : viewIDValue.toString());
	                }else{
	                	kp = new KeyNamePair(keyData, null);
	                }
	                
	                // get Data
					LinkedHashMap<String, Object> values = new LinkedHashMap<String, Object>();
					for(int col  = 0 ; col < p_layout.length; col ++)
					{
						// layout has same columns as selectedInfo
						if (!p_layout[col].isReadOnly())
							values.put(p_layout[col].getColHeader(), selectedInfo.getValue().get(col));
					}
//					if(values.size() > 0)  - jtrinidad - should just save the selected IDs
						m_values.put(kp, values);
	            }
			}
		} // saveResultSelection

		
		/**
		 * Insert result values
		 * @param AD_PInstance_ID
		 */
		public void createT_Selection_InfoWindow(int AD_PInstance_ID)
		{
			if (m_values == null) {
				return;
			}
			int counter = 0;
			StringBuilder select = new StringBuilder();
			String insert = "INSERT INTO T_Selection_InfoWindow (AD_PINSTANCE_ID, T_SELECTION_ID, VIEWID, COLUMNNAME, VALUE_STRING, VALUE_NUMBER, VALUE_DATE) VALUES ( ";
			for (Entry<KeyNamePair, LinkedHashMap<String, Object>> records : m_values.entrySet())
			{
				// set Record ID
				KeyNamePair knPair = records.getKey();
				LinkedHashMap<String, Object> fields = records.getValue();
				for (Entry<String, Object> field : fields.entrySet())
				{
					counter++;
					if (counter > 1)
						select.append(" , ( ");

					select.append(AD_PInstance_ID).append(","); // AD_PINSTANCE_ID
					select.append(knPair.getKey()).append(","); // T_SELECTION_ID
					if (knPair.getName() == null) // VIEWID
						select.append("NULL,");
					else
						select.append("'").append(knPair.getName()).append("',");

					if (field.getKey() == null)// COLUMNNAME
						select.append("NULL,");
					else
						select.append("'").append(field.getKey()).append("',");

					// set Values as Like VALUE_STRING, VALUE_NUMBER, VALUE_DATE
					Object data = field.getValue();
					if (data instanceof IDColumn)
					{
						IDColumn id = (IDColumn) data;
						select.append("NULL,");
						select.append(id.getRecord_ID());
						select.append(",NULL::TIMESTAMP WITHOUT TIME ZONE");
					}
					else if (data instanceof String)
					{
						select.append("'").append(StringEscapeUtils.escapeSql((String) data)).append("'");
						select.append(",NULL");
						select.append(",NULL::TIMESTAMP WITHOUT TIME ZONE");
					}
					else if (data instanceof BigDecimal || data instanceof Integer || data instanceof Double)
					{
						select.append("NULL,");
						if (data instanceof Double)
						{
							BigDecimal value = BigDecimal.valueOf((Double) data);
							select.append(value);
						}
						else
							select.append(data);
						select.append(",NULL::TIMESTAMP WITHOUT TIME ZONE");
					}
					else if (data instanceof Integer)
					{
						select.append("NULL,");
						select.append((Integer) data);
						select.append(",NULL::TIMESTAMP WITHOUT TIME ZONE");
					}
					else if (data instanceof Timestamp || data instanceof Date)
					{
						select.append("NULL,");
						select.append("NULL,");
						select.append("'");
						if (data instanceof Timestamp)
						{
							Timestamp value = new Timestamp(((Date) data).getTime());
							select.append(value);
						}
						else
						{
							select.append(data);
						}
						select.append("'");
					}
					else if (data instanceof KeyNamePair)
					{

						select.append("'").append(StringEscapeUtils.escapeSql(((KeyNamePair) data).getName())).append("'");
						select.append(",NULL");
						select.append(",NULL::TIMESTAMP WITHOUT TIME ZONE");
						
					}
					else
					{
						if (data == null)
							select.append("NULL");
						else
							select.append("'").append(StringEscapeUtils.escapeSql((String) data)).append("'");
						select.append(",NULL");
						select.append(",NULL::TIMESTAMP WITHOUT TIME ZONE");
					}

					select.append(" ) ");
				}

				if (counter >= 1000)
				{
					DB.executeUpdateEx(insert + select.toString(), null);
					select = new StringBuilder();
					counter = 0;
				}
			}
			if (counter > 0)
			{
				DB.executeUpdateEx(insert + select.toString(), null);
			}

		} // createT_Selection_InfoWindow

	/**
	 * @param AD_PInstance_ID
	 * @param alias
	 * @param recordId
	 * @param trxName
	 * @return
	 */
	static public LinkedHashMap<String, Object> getInfoWindowValues(int AD_PInstance_ID, String alias, int recordId,
			String trxName)
	{
		LinkedHashMap<String, Object> values = new LinkedHashMap<String, Object>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Object> parameters = new ArrayList<Object>();
		try
		{
			StringBuilder sql = new StringBuilder(
					"SELECT ColumnName, Value_String, Value_Date, Value_Number FROM T_Selection_InfoWindow "
							+ "WHERE  AD_PInstance_ID = ? AND T_Selection_ID = ? ");
			parameters.add(AD_PInstance_ID);
			parameters.add(recordId);

			if (alias != null)
			{
				sql.append("AND ColumnName LIKE ?");
				parameters.add(alias.toUpperCase() + "_%");
			}
			
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			DB.setParameters(pstmt, parameters);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				String columnName = rs.getString(1);
				String valueString = rs.getString(2);
				Timestamp valueDate = rs.getTimestamp(3);
				BigDecimal valueBigDecimal = rs.getBigDecimal(4);
				if (valueString != null)
				{
					values.put(columnName, valueString);
					continue;
				}
				else if (valueDate != null)
				{
					values.put(columnName, valueDate);
					continue;
				}
				else if (valueBigDecimal != null)
				{
					values.put(columnName, valueBigDecimal);
					continue;
				}
			}
		}
		catch (SQLException ex)
		{
			throw new DBException(ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return values;
	} // getInfoWindowValues
	
	/**
	 * Get index of infoProcess have processId
	 * 
	 * @param processId
	 * @return
	 */
	protected int getProcessIndex(int processId)
	{
		int index = 0;
		for (int i = 0; i < infoProcessList.length; i++)
		{
			if (infoProcessList[i].getAD_Process_ID() == processId)
			{
				return index;
			}
			// just increase index when process is have ViewID column
			if (infoProcessList[i].getAD_InfoColumn_ID() > 0)
				index++;
		}
		return -1;
	}
    
    /**
     * Get InfoColumnID of infoProcess have processID is processId
     * @param processId
     * @return value InfoColumnID, -1 when has not any map
     */
    protected int getInfoColumnIDFromProcess (int processId){
    	if (infoProcessList == null) {
    		return -1;
    	}
    	for (int i = 0; i < infoProcessList.length; i++){
    		if (infoProcessList[i].getAD_Process_ID() == processId){
    			return infoProcessList[i].getAD_InfoColumn_ID();
    		}
    	}
    	return -1;
    }
    
	private void showBusyDialog() {
		if(progressWindow == null)
		{
			progressWindow = new BusyDialog();
			progressWindow.setPage(this.getPage());
			progressWindow.doHighlighted();
		}
	}

	private void hideBusyDialog()
	{
		if (progressWindow != null)
		{
			progressWindow.dispose();
			progressWindow = null;
		}
	}

    public void onQueryCallback(Event event)
    {
    	try
    	{
    		Listhead listHead = contentPanel.getListHead();
    		if (listHead != null) {
    			List<?> headers = listHead.getChildren();
    			for(Object obj : headers)
    			{
    				Listheader header = (Listheader) obj;
    				header.setSortDirection("natural");
    			}
    		}
    		m_sqlUserOrder="";
    		// event == null mean direct call from reset button
    		if (event == null)
    			m_count = 0;
    		else
    			executeQuery();
    		
            renderItems();            
        	// IDEMPIERE-1334 after refresh, restore prev selected item start         	
        	// just evaluate display logic of process button when requery by use click requery button
        	if (isQueryByUser){
        		bindInfoProcess();
                isRequeryByRunSuccessProcess = false;
        	}
        	if (isRequeryByRunSuccessProcess){
        		syncSelectedAfterRequery();
        		restoreSelectedInPage();
        	}
			// reset selected list
			recordSelectedData.clear();
        	// IDEMPIERE-1334 after refresh, restore prev selected item end
        	updateSubcontent ();
        	
        	//allow only when page is only 1
//        	confirmPanel.getButton(FORCE_SELECT_ALL).setEnabled(m_count < PAGE_SIZE);
       		contentPanel.renderAll();

        }
    	catch (Exception e) {
			log.severe(e.getMessage());
		}
    	finally
    	{
    		isQueryByUser = false;
    		hideBusyDialog();
    	}
    }

	/**
	 * evaluate display logic of button process empty method. implement at child
	 * class extend
	 */
	protected void bindInfoProcess()
	{}

	protected void onOk()
	{
		if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
		{
			dispose(true);
		}
	}
    
	private void onDoubleClick()
	{
		if (isLookup())
		{
			dispose(true);
		}
		else
		{
			zoom();
		}

	}

    public void tableChanged(WTableModelEvent event)
    {
    	enableButtons();
    }
    
    public void zoom()
    {
    	Integer recordId = contentPanel.getSelectedRowKey();
    	// prevent NPE when double click is raise but no record is selected
    	if (recordId == null)
    		return;
    	
    	if (listeners != null && listeners.size() > 0)
    	{
	        ValueChangeEvent event = new ValueChangeEvent(this,"zoom",
	                   contentPanel.getSelectedRowKey(),contentPanel.getSelectedRowKey());
	        fireValueChange(event);
    	}
    	else
    	{
    		int AD_Table_ID = MTable.getTable_ID(p_tableName);
    		if (AD_Table_ID <= 0)
    		{
    			if (p_keyColumn.endsWith("_ID")) 
    			{
    				AD_Table_ID = MTable.getTable_ID(p_keyColumn.substring(0, p_keyColumn.length() - 3));
    			}
    		}
    		if (AD_Table_ID > 0)
    			AEnv.zoom(AD_Table_ID, recordId);
    	}
    }
    
    public void addValueChangeListener(ValueChangeListener listener)
    {
        if (listener == null)
        {
            return;
        }
        
        listeners.add(listener);
    }
        
    public void fireValueChange(ValueChangeEvent event)
    {
        for (ValueChangeListener listener : listeners)
        {
           listener.valueChange(event);
        }
    }
    /**
     *  Dispose and save Selection
     *  @param ok OK pressed
     */
    public void dispose(boolean ok)
    {
        log.config("OK=" + ok);
        m_ok = ok;

        //  End Worker
        if (isLookup())
        {
        	saveSelection();
        }
        if (Window.MODE_EMBEDDED.equals(getAttribute(Window.MODE_KEY)))
        	SessionManager.getAppDesktop().closeActiveWindow();
        else {
        	this.detach();
//        	SessionManager.getAppDesktop().unregisterWindow(p_WindowNo);
        }
    }   //  dispose
        
	@SuppressWarnings("rawtypes")
	public void sort(Comparator cmpr, boolean ascending) {
		WListItemRenderer.ColumnComparator lsc = (WListItemRenderer.ColumnComparator) cmpr;
		if (m_useDatabasePaging)
		{
			int col = lsc.getColumnIndex();
			String colsql = p_layout[col].getColSQL().trim();
			int lastSpaceIdx = colsql.lastIndexOf(" ");
			if (lastSpaceIdx > 0)
			{
				String tmp = colsql.substring(0, lastSpaceIdx).trim();
				char last = tmp.charAt(tmp.length() - 1);
				if (tmp.toLowerCase().endsWith("as"))
				{
					colsql = colsql.substring(lastSpaceIdx).trim();
				}
				else if (!(last == '*' || last == '-' || last == '+' || last == '/' || last == '>' || last == '<' || last == '='))
				{
					tmp = colsql.substring(lastSpaceIdx).trim();
					if (tmp.startsWith("\"") && tmp.endsWith("\""))
					{
						colsql = colsql.substring(lastSpaceIdx).trim();
					}
					else
					{
						boolean hasAlias = true;
						for(int i = 0; i < tmp.length(); i++)
						{
							char c = tmp.charAt(i);
							if (Character.isLetterOrDigit(c))
							{
								continue;
							}
							else
							{
								hasAlias = false;
								break;
							}
						}
						if (hasAlias)
						{
							colsql = colsql.substring(lastSpaceIdx).trim();
						}
					}
				}
			}
			m_sqlUserOrder = " ORDER BY " + colsql;
			if (!ascending)
				m_sqlUserOrder += " DESC ";
			executeQuery();
			renderItems();
		}
		else
		{
			Collections.sort(line, lsc);
			renderItems();
		}
	}

    public boolean isLookup()
    {
    	return m_lookup;
    }

    /**
     * Modal window is either a lookup or fired from a window button
     * @return
     */
    public boolean isModal()
    {
    	return isLookup() || m_Record_ID > 0;
    }
    
    public void scrollToSelectedRow()
    {
    	if (contentPanel != null && contentPanel.getSelectedIndex() >= 0) {
    		Listitem selected = contentPanel.getItemAtIndex(contentPanel.getSelectedIndex());
    		if (selected != null) {
    			selected.focus();
    		}
    	}
    }
    
	public void setLine(List<Object> line) {
		this.line = line;
	}
	
	public int getWindowNo() {
		return p_WindowNo;
	}
	
	public int getRowCount() {
		return contentPanel.getRowCount();
	}
	
	/**
	 * @return the cacheStart
	 */
	protected int getCacheStart() {
		return cacheStart;
	}

 	/**
	 * @return the cacheEnd
	 */
	protected int getCacheEnd() {
		return cacheEnd;
	}
	
	protected boolean isUseDatabasePaging() {
		return m_useDatabasePaging;
	}
	
	@Override
	public void onPageAttached(Page newpage, Page oldpage)
	{
		super.onPageAttached(newpage, oldpage);
		SessionManager.getSessionApplication().getKeylistener().addEventListener(Events.ON_CTRL_KEY, this);
	}

	@Override
	public void onPageDetached(Page page)
	{
		super.onPageDetached(page);
		SessionManager.getSessionApplication().getKeylistener().removeEventListener(Events.ON_CTRL_KEY, this);
	}
	
	protected void initTableColumnWidth()
	{
		ListitemRenderer<Object> render = contentPanel.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer infoRenderer = (WListItemRenderer) render;

			MTabCustomization tabCustom = MTabCustomization.getInfo(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()),
					m_infoWindowID, null);

			if (tabCustom == null)
				tabCustom = MTabCustomization.getInfo(Env.getCtx(), MTabCustomization.SUPERUSER, m_infoWindowID, null);

			if (tabCustom != null)
			{
				infoRenderer.setHeaderColumnWidth(tabCustom.getcustom());
			}
			contentPanel.repaint();
		}
	}

	protected void initSubTableColumnWidth()
	{
	
		if (m_subTableInit) {
			return;
		}
		if (infoSubTabs != null && infoSubTabs.size() > 0) {
			int idx = 1;
			for (WListbox tbl : infoSubTabs) {
				WListItemRenderer renderer = (WListItemRenderer) tbl.getItemRenderer();
				MTabCustomization subTabCustom = MTabCustomization.getInfo(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()),
						m_infoWindowID, idx, null);
				if (subTabCustom == null) {
					subTabCustom = MTabCustomization.getInfo(Env.getCtx(), MTabCustomization.SUPERUSER, m_infoWindowID, idx, null);
				}
				if (subTabCustom != null) {
					renderer.setHeaderColumnWidth(subTabCustom.getcustom());
				}
				tbl.repaint();
				idx++;
			}
		}
		m_subTableInit = true;
	}
	public int getRecord_ID() {
		return m_Record_ID;
	}

	public void setRecord_ID(int m_Record_ID) {
		this.m_Record_ID = m_Record_ID;
	}

	public void setTabID(int tabID) 
	{
		this.m_Tab_ID = tabID;
		
		Env.setContext(Env.getCtx(), p_WindowNo, MTab.COLUMNNAME_AD_Tab_ID, tabID);
	}
	
	public void addSubTab(WListbox subTbl)
	{
		if (infoSubTabs == null) {
			infoSubTabs = new ArrayList<WListbox>();
		}
		
		infoSubTabs.add(subTbl);
	}
	
	/**
	 * Populate rows from sql adding the Tree View filter ID
	 */
	protected void populateFromTreeFilter()
	{

		this.executeQuery();
		this.renderItems();
		
//		List<Object> subList = readLine(0, 50);
//		model = new ListModelTable(subList);
//		model.setSorter(this);
//		model.setMultiple(p_multipleSelection);
//        model.addTableModelListener(this);
////        contentPanel.setData(model, null);
//
//        contentPanel.clearTable();
//        contentPanel.setModel(model);
//        renderItems();
	}

	public ListModelTable getMainModel() {
		return model;
	}
	

	public boolean isTSCapable()
	{
		return m_TSCapable;
	}
	//TODO - InfoColumn on InfoProcess
}	//	Info
