package org.adempiere.webui.panel;

import org.adempiere.webui.desktop.IDesktop;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.GridTab;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Anchorchildren;
import org.zkoss.zul.Anchorlayout;
import org.zkoss.zul.Style;
import org.zkoss.zul.Vlayout;

public class RecordContextController
{	
	private Anchorlayout dashboardLayout;
	private RecordAttachmentPanel pnlAttachment;
	private RecordRelatedPanel pnlRelatedRecord;
	private RecordReportPanel pnlReport;
	private RecordArchivePanel pnlArchivedDoc;
	
	public RecordContextController()
    {
		dashboardLayout = new Anchorlayout();
        dashboardLayout.setSclass("dashboard-layout");
        ZKUpdateUtil.setVflex(dashboardLayout, "1");
        ZKUpdateUtil.setHflex(dashboardLayout, "1");
    }

	public void render(Component parent, IDesktop desktopImpl)
    {
    	Style style = new Style();
		style.setContent(".z-anchorlayout { overflow:auto } .z-anchorchildren { overflow:visible } ");
		style.setPage(parent.getPage());
		
		parent.appendChild(dashboardLayout);
        
        Vlayout dashboardColumnLayout = new Vlayout();
        ZKUpdateUtil.setHflex(dashboardColumnLayout, "1");

        Anchorchildren dashboardColumn = new Anchorchildren();
		dashboardColumn.setAnchor("95% 100%");
		dashboardColumn.appendChild(dashboardColumnLayout);
        dashboardLayout.appendChild(dashboardColumn);

        pnlAttachment = new RecordAttachmentPanel();
        dashboardColumnLayout.appendChild(pnlAttachment);
        
        pnlRelatedRecord = new RecordRelatedPanel();
        dashboardColumnLayout.appendChild(pnlRelatedRecord);
        
    	pnlReport = new RecordReportPanel();
    	dashboardColumnLayout.appendChild(pnlReport);
        
    	pnlArchivedDoc = new RecordArchivePanel();
    	dashboardColumnLayout.appendChild(pnlArchivedDoc);
    }
	
	public void render(GridTab gridTab) {
		pnlAttachment.render(gridTab);
		pnlRelatedRecord.render(gridTab);
		pnlReport.render(gridTab);
		pnlArchivedDoc.render(gridTab);
	}
}
