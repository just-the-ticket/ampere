package org.adempiere.webui.panel;

/**
 * 
 * @author jtrinidad
 *
 */
public class DialogForm extends ADForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2621986133904560005L;

	public DialogForm() {
		super();
	}

	@Override
	protected void initForm() {
		

	}

	@Override
	public Mode getWindowMode() {
		return Mode.HIGHLIGHTED;
	}

	
}
