/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MBPartner;
import org.compiere.model.MQuery;
import org.compiere.model.MTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Vlayout;

/**
*	Search Business Partner and return selection
*   Based on InfoBPartner written by Jorg Janke
* 	@author Sendy Yagambrum
* 
* 	Zk Port
* 	@author Elaine
* 	@version	InfoBPartner.java Adempiere Swing UI 3.4.1 
*/


public class InfoBPartnerPanel extends InfoPanel implements EventListener<Event>, WTableModelListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5677624151607188344L;
	private Label lblValue ;
	private Textbox fieldValue ;
	private Label lblName;
	private Textbox fieldName ;
	private Label lblContact ;
	private Textbox fieldContact;
	private Label lblEMail ;
	private Textbox fieldEMail;
	private Label lblPostal;
	private Textbox fieldPostal;
	private Label lblPhone;
	private Textbox fieldPhone;
	private Checkbox checkAND ;
	private Checkbox checkCustomer;
	private Label sBPTypeLabel = new Label("Customer/Vendor/Both");
	private Listbox sBPTypeCombo;
	

	private int m_AD_User_ID_index = -1; // Elaine 2008/12/16
    private int m_C_BPartner_Location_ID_index = -1;
		
	/** SalesOrder Trx          */
	private boolean 		m_isSOTrx;
		
	/**	Logger			*/
	protected CLogger log = CLogger.getCLogger(getClass());
	private Borderlayout layout;
	private Vlayout southBody;
	
	Tabbox tabbedPane = new Tabbox();
	WListbox contactTbl = ListboxFactory.newDataTable();
    String m_sqlContact;
	WListbox locationTbl = ListboxFactory.newDataTable();
    String m_sqlLocation;
	private Vlayout centerBody;
	private int 				m_C_BPartner_ID = 0;

	
	/** From Clause             */
	private static String s_partnerFROM = "C_BPartner"
//		+ " LEFT OUTER JOIN C_BPartner_Location l ON (C_BPartner.C_BPartner_ID=l.C_BPartner_ID AND l.IsActive='Y')"
//		+ " LEFT OUTER JOIN AD_User c ON (C_BPartner.C_BPartner_ID=c.C_BPartner_ID AND (c.C_BPartner_Location_ID IS NULL OR c.C_BPartner_Location_ID=l.C_BPartner_Location_ID) AND c.IsActive='Y')" 
//		+ " LEFT OUTER JOIN C_Location a ON (l.C_Location_ID=a.C_Location_ID)";
			;
	
	/**  Array of Column Info    */
	private static ColumnInfo[] s_partnerLayout = {
		new ColumnInfo(" ", "C_BPartner.C_BPartner_ID", IDColumn.class, 0),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Value"), "C_BPartner.Value", String.class, 1),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "C_BPartner.Name", String.class, 2),
		new ColumnInfo(Msg.translate(Env.getCtx(), "SO_CreditAvailable"), "C_BPartner.SO_CreditLimit-C_BPartner.SO_CreditUsed AS SO_CreditAvailable", BigDecimal.class, true, true, null, -1, 3),
		new ColumnInfo(Msg.translate(Env.getCtx(), "SO_CreditUsed"), "C_BPartner.SO_CreditUsed", BigDecimal.class, 4),
		new ColumnInfo(Msg.translate(Env.getCtx(), "TotalOpenBalance"), "C_BPartner.TotalOpenBalance", BigDecimal.class, 5),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Revenue"), "C_BPartner.ActualLifetimeValue", BigDecimal.class, 6),
	};

	/**
	 *	Standard Constructor
	 *  @param  queryvalue   Query value Name or Value if contains numbers
	 *  @param isSOTrx  if false, query vendors only
	 *  @param whereClause where clause
	 * @param columnName 
	 */
	public InfoBPartnerPanel(String queryValue,int windowNo, boolean isSOTrx,boolean multipleSelection, String whereClause, String columnName)
	{		
		this(queryValue, windowNo, isSOTrx, multipleSelection, whereClause, true, columnName);
	}

	/**
	 *	Standard Constructor
	 *  @param  queryvalue   Query value Name or Value if contains numbers
	 *  @param isSOTrx  if false, query vendors only
	 *  @param whereClause where clause
	 */
	public InfoBPartnerPanel(String queryValue,int windowNo, boolean isSOTrx,boolean multipleSelection, String whereClause, boolean lookup, String columnName)
	{

		super (windowNo, "C_BPartner", "C_BPartner_ID",multipleSelection, whereClause, lookup, INFO_BPARTNER);
		setTitle(Msg.getMsg(Env.getCtx(), "InfoBPartner"));
		m_isSOTrx = isSOTrx;
        initComponents();
        init();
		initInfo(queryValue, whereClause);
        
        int no = contentPanel.getRowCount();
        setStatusLine(Integer.toString(no) + " " + Msg.getMsg(Env.getCtx(), "SearchRows_EnterQuery"), false);
        setStatusDB(Integer.toString(no));
        //
		if (queryValue != null && queryValue.length()>0)
		{
			 executeQuery();
             renderItems();
        } else {
        	if (!lookup) {
        		initialId = Env.getContextAsInt(Env.getCtx(), windowNo, "C_BPartner_ID");
        	} else if (columnName != null) {
        		initialId = Env.getContextAsInt(Env.getCtx(), windowNo, columnName);
        	}
			executeQuery();
            renderItems();
        }
		p_loadedOK = true; // Elaine 2008/07/28
		initialId = 0;
			
	}
	
	private void initComponents()
	{
		ZKUpdateUtil.setHflex(fieldTokenSearch, "true");
		fieldTokenSearch.addEventListener(Events.ON_CHANGING, this);
		
		lblValue = new Label();
		lblValue.setValue(Util.cleanAmp(Msg.translate(Env.getCtx(), "Value")));
		lblName = new Label();
		lblName.setValue(Util.cleanAmp(Msg.translate(Env.getCtx(), "Name")));
		lblContact = new Label();
		lblContact.setValue(Msg.translate(Env.getCtx(), "Contact"));
		lblEMail = new Label();
		lblEMail.setValue(Msg.getMsg(Env.getCtx(), "EMail"));
		lblPostal = new Label();
		lblPostal.setValue(Msg.getMsg(Env.getCtx(), "Postal"));
		lblPhone = new Label();
		lblPhone.setValue(Msg.translate(Env.getCtx(), "Phone"));
		
		fieldValue = new Textbox();
		fieldValue.setMaxlength(40);
		fieldName = new Textbox();
		fieldName.setMaxlength(40);
		fieldContact = new Textbox();
		fieldContact.setMaxlength(40);
		fieldEMail = new Textbox();
		fieldEMail.setMaxlength(40);
		fieldPostal = new Textbox();
		fieldPostal.setMaxlength(40);
		fieldPhone = new Textbox();
		fieldPhone.setMaxlength(40);
		
		checkAND = new Checkbox();
		checkAND.setLabel(Msg.getMsg(Env.getCtx(), "SearchAND"));
		checkAND.setChecked(true);
		checkAND.addEventListener(Events.ON_CHECK, this);

        sBPTypeCombo = new Listbox();
        sBPTypeCombo.addItem(new ValueNamePair("*", "*"));
        sBPTypeCombo.addItem(new ValueNamePair("Y", "Customer"));
        sBPTypeCombo.addItem(new ValueNamePair("N", "Vendor"));		
	}
	
	private void init()
	{
		ZKUpdateUtil.setWidth(sBPTypeCombo, "100%");
		ZKUpdateUtil.setWidth(fieldValue, "100%");
		ZKUpdateUtil.setWidth(fieldContact, "100%");
		ZKUpdateUtil.setWidth(fieldPhone, "100%");
		
		ZKUpdateUtil.setWidth(fieldName, "100%");
		ZKUpdateUtil.setWidth(fieldEMail, "100%");
		ZKUpdateUtil.setWidth(fieldPostal, "100%");
		
		Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		row.setSpans("1, 3, 1, 1, 1, 1");
		rows.appendChild(row);
		row.appendChild(lblTokenSearch.rightAlign());
		row.appendChild(fieldTokenSearch);
		row.appendChild(sBPTypeLabel.rightAlign());
		row.appendChild(sBPTypeCombo);

		sBPTypeCombo.addEventListener(Events.ON_SELECT, this);
		sBPTypeCombo.setMold("select");
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblValue.rightAlign());
		row.appendChild(fieldValue);
		row.appendChild(lblContact.rightAlign());
		row.appendChild(fieldContact);
		row.appendChild(lblPhone.rightAlign());
		row.appendChild(fieldPhone);
//		row.appendChild(checkCustomer);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblName.rightAlign());
		row.appendChild(fieldName);
		row.appendChild(lblEMail.rightAlign());
		row.appendChild(fieldEMail);
		row.appendChild(lblPostal.rightAlign());
		row.appendChild(fieldPostal);
//		row.appendChild(checkAND);
        

       	/**	From Clause							*/
    	String s_sqlFrom = "AD_User u";
    	/** Where Clause						*/
    	String s_sqlWhere = " u.C_BPartner_ID = ?"; 

        ColumnInfo[] s_layoutContact = new ColumnInfo[]{
        		new ColumnInfo(" ", "u.AD_User_ID", IDColumn.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "u.name", String.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Phone"), "u.phone", String.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Email"), "u.email", String.class)
        };
        m_sqlContact = contactTbl.prepareTable(s_layoutContact, s_sqlFrom, s_sqlWhere, false, "u")
        		+ " ORDER BY u.name";

        contactTbl.setMultiSelection(false);
        contactTbl.autoSize();
//        contactTbl.getModel().addTableModelListener(this);

        ColumnInfo[] s_layoutLocation = new ColumnInfo[]{
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "bpl.name", String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "Postal"), "l.postal", String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "City"), "l.city", String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "Address"), "l.address1", String.class)
        		};

        s_sqlFrom = "C_BPartner_Location bpl " +
        			" LEFT JOIN C_Location l ON bpl.c_location_id = l.c_location_id ";
        s_sqlWhere = "bpl.C_BPartner_ID = ? ";

        m_sqlLocation = locationTbl.prepareTable(s_layoutLocation, s_sqlFrom, s_sqlWhere, false, "bpl")
        		+ " ORDER BY bpl.name";

        locationTbl.setMultiSelection(false);
        locationTbl.autoSize();
//        locationTbl.getModel().addTableModelListener(this);
        
        tabbedPane.setStyle("min-height:250px");
        ZKUpdateUtil.setVflex(tabbedPane, "true");
		Tabpanels tabPanels = new Tabpanels();
		tabbedPane.appendChild(tabPanels);
		Tabs tabs = new Tabs();
		tabbedPane.appendChild(tabs);

		Tab tab = new Tab(Util.cleanAmp(Msg.translate(Env.getCtx(), "Contact")));
		tabs.appendChild(tab);
		Tabpanel desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(contactTbl);
		tabPanels.appendChild(desktopTabPanel);
		
		tab = new Tab(Msg.translate(Env.getCtx(), "Location"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(locationTbl);
		tabPanels.appendChild(desktopTabPanel);
		
		addSubTab(contactTbl);
		addSubTab(locationTbl);

		//
		int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 90 / 100;
		int width = SessionManager.getAppDesktop().getClientInfo().desktopWidth * 80 / 100;
		
		layout = new Borderlayout();
		ZKUpdateUtil.setWidth(layout, "100%");
		ZKUpdateUtil.setHeight(layout, "100%");
        if (isLookup())
        	layout.setStyle("border: none; position: relative");
        else
        	layout.setStyle("border: none; position: absolute");
        Center center = new Center();
        center.setAutoscroll(false);
		ZKUpdateUtil.setVflex(contentPanel, "true");
		layout.appendChild(center);
		centerBody = new Vlayout();
		ZKUpdateUtil.setVflex(centerBody, "true");
		centerBody.appendChild(contentPanel);
		center.appendChild(centerBody);
		South south = new South();
		int detailHeight = (height * 40 / 100);
//		ZKUpdateUtil.setHeight(south, detailHeight + "px");
		south.setCollapsible(true);
		south.setSplittable(true);
		ZKUpdateUtil.setVflex(south, "true");
		south.setTitle("Business Partner Details");
		south.setTooltiptext("Detailed information on the Business Partner");
		layout.appendChild(south);
		south.appendChild(tabbedPane);

        Borderlayout mainPanel = new Borderlayout();
//        ZKUpdateUtil.setWidth(mainPanel, "100%");
//        ZKUpdateUtil.setHeight(mainPanel, "100%");
        ZKUpdateUtil.setVflex(mainPanel, "true");
        ZKUpdateUtil.setHflex(mainPanel, "true");
        North north = new North();
        mainPanel.appendChild(north);
        north.appendChild(grid);
        center = new Center();
        mainPanel.appendChild(center);
        center.appendChild(layout);
        south = new South();
        mainPanel.appendChild(south);
        southBody = new Vlayout();
		ZKUpdateUtil.setWidth(southBody, "100%");
		south.appendChild(southBody);

		southBody.appendChild(confirmPanel);
		southBody.appendChild(new Separator());
		southBody.appendChild(statusBar);
        if (!isLookup())
        {
        	mainPanel.setStyle("position: absolute");
        }

		this.appendChild(mainPanel);
		layout.getSouth().setOpen(false);

		contentPanel.addActionListener(new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				int row = contentPanel.getSelectedRow();
				if (row >= 0) {
        			refresh();
        			layout.getSouth().setOpen(true);
        			enableButtons();
        			initSubTableColumnWidth();
				}
			}
		});
	}	
	
	private void refresh()
	{
		m_C_BPartner_ID = getSelectedRowKey();

		//int M_Product_ID = 0;
		String sql = m_sqlContact;
		//Add description to the query
//		sql = sql.replace(" FROM", ", DocumentNote FROM");
//		log.finest(sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{

			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_C_BPartner_ID);

			rs = pstmt.executeQuery();
			contactTbl.loadTable(rs);
			rs = pstmt.executeQuery();
			/*
			if(rs.next())
				if(rs.getString("DocumentNote") != null)
					fieldDescription.setText(rs.getString("DocumentNote"));
			*/
		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}


		sql = m_sqlLocation;
		log.finest(sql);
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_C_BPartner_ID);
			rs = pstmt.executeQuery();
			locationTbl.loadTable(rs);
			rs.close();
		} catch (Exception e) {
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

	}	//	refresh

	/**
	 *	Dynamic Init
	 *  @param value value
	 *  @param whereClause where clause
	 */
		
	private void initInfo(String value, String whereClause)
	{
		
			if (m_isSOTrx) {
				sBPTypeCombo.setSelectedIndex(1);
			} else {
				sBPTypeCombo.setSelectedIndex(2);
			}
			/**	From
				C_BPartner
				 LEFT OUTER JOIN C_BPartner_Location l ON (C_BPartner.C_BPartner_ID=l.C_BPartner_ID AND l.IsActive='Y') 
				 LEFT OUTER JOIN AD_User c ON (C_BPartner.C_BPartner_ID=c.C_BPartner_ID AND (c.C_BPartner_Location_ID IS NULL OR c.C_BPartner_Location_ID=l.C_BPartner_Location_ID) AND c.IsActive='Y') 
				 LEFT OUTER JOIN C_Location a ON (l.C_Location_ID=a.C_Location_ID)
			**/

			//	Create Grid
			StringBuffer where = new StringBuffer();
			where.append("C_BPartner.IsSummary='N' AND C_BPartner.IsActive='Y'");
			if (whereClause != null && whereClause.length() > 0)
				where.append(" AND ").append(whereClause);
			//
                          
			prepareTable(s_partnerLayout, s_partnerFROM, where.toString(), "C_BPartner.Value");
			
			if (value != null && value.length() > 0 && value.startsWith("@") && value.endsWith("@")) {
				String values[] = value.substring(1,value.length()-1).split("_");
//				fieldValue.setText(values[0]);
//					fieldName.setText(values[0]);
					fieldTokenSearch.setText(values[0]);
					m_TSQuery = org.adempiere.util.StringUtils.rewritetoTSQuery(values[0]);
			}
			else {
//					fieldName.setText(value);
					fieldTokenSearch.setText(value);
					m_TSQuery = org.adempiere.util.StringUtils.rewritetoTSQuery(value);
			}		
			
			// Get indexes
            for (int i = 0; i < p_layout.length; i++)
            {
            	// Elaine 2008/12/16
            	if (p_layout[i].getKeyPairColSQL().indexOf("AD_User_ID") != -1)
    				m_AD_User_ID_index = i;
            	//
                if (p_layout[i].getKeyPairColSQL().indexOf("C_BPartner_Location_ID") != -1)
                    m_C_BPartner_Location_ID_index = i;
            }
            //  Set Value
			if (value == null)
				value = "%";
			if (!value.endsWith("%"))
				value += "%";

			

			
//			
//			//	Put query string in Name if not numeric
//			if (value.equals("%"))
//				fieldName.setText(value);
//			//	No Numbers entered
//			else if ((value.indexOf('0')+value.indexOf('1')+value.indexOf('2')+value.indexOf('3')+value.indexOf('4') +value.indexOf('5')
//				+value.indexOf('6')+value.indexOf('7')+value.indexOf('8')+value.indexOf('9')) == -10)
//			{
//				if (value.startsWith("%"))
//					fieldName.setText(value);
//				else
//					fieldName.setText("%" + value);
//			}
//			//	Number entered
//			else
//				fieldValue.setText(value);
			
			
	}	//	initInfo

	/**
	 *  Set Parameters for Query.
	 *  (as defined in getSQLWhere)
	 *  @param pstmt pstmt
	 *  @param forCount for counting records
	 *  @throws SQLException
	 */
	public void setParameters(PreparedStatement pstmt, boolean forCount) throws SQLException
	{
		int index = 1;

		
		// => Token Search
		if (m_TSQuery != null && m_TSQuery.length() >= 5) {
			pstmt.setString(index++, m_TSQuery);
//			m_TSQuery = null;  //reset it immediately?
		}		
		if ( initialId > 0 )
		{
			pstmt.setInt(index++, initialId);
			log.fine("C_BPartner_ID=" + initialId);
		}		

		//	=> Value
		String value = fieldValue.getText().toUpperCase();
		if (!(value.equals("") || value.equals("%")))
		{
			if (!value.endsWith("%"))
				value += "%";
			pstmt.setString(index++, value);
			log.fine("Value: " + value);
		}
		//	=> Name
		String name = fieldName.getText().toUpperCase();
		if (!(name.equals("") || name.equals("%")))
		{
			if (!name.endsWith("%"))
				name += "%";
			pstmt.setString(index++, name);
			log.fine("Name: " + name);
		}
		//	=> Contact
		String contact = fieldContact.getText().toUpperCase();
		if (!(contact.equals("") || contact.equals("%")))
		{
			if (!contact.endsWith("%"))
				contact += "%";
			pstmt.setString(index++, contact);
			log.fine("Contact: " + contact);
		}
		//	=> EMail
		String email = fieldEMail.getText().toUpperCase();
		if (!(email.equals("") || email.equals("%")))
		{
			if (!email.endsWith("%"))
				email += "%";
			pstmt.setString(index++, email);
			log.fine("EMail: " + email);
		}
		//	=> Phone
		String phone = fieldPhone.getText().toUpperCase();
		if (!(phone.equals("") || phone.equals("%")))
		{
			if (!phone.endsWith("%"))
				phone += "%";
			pstmt.setString(index++, phone);
			log.fine("Phone: " + phone);
		}
		//	=> Postal
		String postal = fieldPostal.getText().toUpperCase();
		if (!(postal.equals("") || postal.equals("%")))
		{
			if (!postal.endsWith("%"))
				postal += "%";
			pstmt.setString(index++, postal);
			log.fine("Postal: " + postal);
		}
	}   //  setParameters

	/*************************************************************************/
	/*************************************************************************/
	/**
	 *	Construct SQL Where Clause and define parameters.
	 *  (setParameters needs to set parameters)
	 *  Includes first AND
	 *  @return WHERE clause
	 */
	public String getSQLWhere()
	{
		ArrayList<String> list = new ArrayList<String>();
		//  => Token Search
		if (m_TSQuery != null && m_TSQuery.length() >= 5) {
			MTable table = MTable.get(Env.getCtx(), MBPartner.Table_ID);
//			list.add("findkeyword(tscolumn, ?) ");
        	list.add(" findkeyword( " + table.getTSVDefinition() + ", ?)");

		}

		if (initialId > 0 )
		{
			list.add ("(C_BPartner.C_BPartner_ID) = ? ");
		} 
		//	=> Value
		String value = fieldValue.getText().toUpperCase();
		if (!(value.equals("") || value.equals("%")))
			list.add ("UPPER(C_BPartner.Value) LIKE ?");
		//	=> Name
		String name = fieldName.getText().toUpperCase();
		if (!(name.equals("") || name.equals("%")))
			list.add ("UPPER(C_BPartner.Name) LIKE ?");
		//	=> Contact
		String contact = fieldContact.getText().toUpperCase();
		if (!(contact.equals("") || contact.equals("%")))
//			list.add ("UPPER(c.Name) LIKE ?");
			list.add (" EXISTS (SELECT 1 FROM AD_User WHERE C_BPartner.C_BPartner_ID = AD_User.C_BPartner_ID AND  AD_User.Name ILIKE ?) ");
		//	=> EMail
		String email = fieldEMail.getText().toUpperCase();
		if (!(email.equals("") || email.equals("%")))
//			list.add ("UPPER(c.EMail) LIKE ?");
			list.add (" EXISTS (SELECT 1 FROM AD_User WHERE C_BPartner.C_BPartner_ID = AD_User.C_BPartner_ID AND  AD_User.EMail ILIKE ?) ");
		//	=> Phone
		String phone = fieldPhone.getText().toUpperCase();
		if (!(phone.equals("") || phone.equals("%")))
//			list.add ("UPPER(c.Phone) LIKE ?");
			list.add (" EXISTS (SELECT 1 FROM AD_User WHERE C_BPartner.C_BPartner_ID = AD_User.C_BPartner_ID AND  AD_User.Phone ILIKE ?) ");
		//	=> Postal
		String postal = fieldPostal.getText().toUpperCase();
		if (!(postal.equals("") || postal.equals("%")))
//			list.add ("UPPER(a.Postal) LIKE ?");
			list.add (" EXISTS (SELECT 1 FROM C_BPartner_Location bpl INNER JOIN C_Location l ON  bpl.C_Location_ID = l.C_Location_ID WHERE C_BPartner.C_BPartner_ID = bpl.C_BPartner_ID AND l.postal ILIKE ?) ");
		StringBuffer sql = new StringBuffer();
		
		

//		else {
//			initialId = 0; // drop the context - jtrinidad
//		}
		int size = list.size();
		//	Just one
		if (size == 1)
			sql.append(" AND ").append(list.get(0));
		else if (size > 1)
		{
			boolean AND = checkAND.isChecked();
			sql.append(" AND ");
			if (!AND)
				sql.append("(");
			for (int i = 0; i < size; i++)
			{
				if (i > 0)
					sql.append(AND ? " AND " : " OR ");
				sql.append(list.get(i));
			}
			if (!AND)
				sql.append(")");
		}
			//	Static SQL
//		if (checkCustomer.isChecked())
//		{
//			sql.append(" AND ");
//			if (m_isSOTrx)
//				sql.append ("C_BPartner.IsCustomer='Y'");
//			else
//				sql.append ("C_BPartner.IsVendor='Y'");
//		}
		if(sBPTypeCombo.getSelectedItem().getValue().equals("Y")) {
			sql.append ("AND C_BPartner.IsCustomer='Y'");
		} else	if(sBPTypeCombo.getSelectedItem().getValue().equals("N")) {
			sql.append ("AND C_BPartner.IsVendor='Y'");
		}
			
		return sql.toString();

	}	//	getSQLWhere
    
    /*************************************************************************/

    /**
     *  Save Selection Details
     *  Get Location/Partner Info
     */
    public void saveSelectionDetail()
    {
        int row = contentPanel.getSelectedRow();
        if (row == -1)
            return;

        int AD_User_ID = 0;
        int C_BPartner_Location_ID = 0;
        
        // Elaine 2008/12/16
        if (m_AD_User_ID_index != -1)
        {
            Object data =contentPanel.getValueAt(row, m_AD_User_ID_index);
            if (data instanceof KeyNamePair)
            	AD_User_ID = ((KeyNamePair)data).getKey();
        }
        //
        if (m_C_BPartner_Location_ID_index != -1)
        {
            Object data =contentPanel.getValueAt(row, m_C_BPartner_Location_ID_index);
            if (data instanceof KeyNamePair)
                C_BPartner_Location_ID = ((KeyNamePair)data).getKey();
        }
        //  publish for Callout to read
        Integer ID = getSelectedRowKey();
        Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "C_BPartner_ID", ID == null ? "0" : ID.toString());
        Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "AD_User_ID", String.valueOf(AD_User_ID));
        Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "C_BPartner_Location_ID", String.valueOf(C_BPartner_Location_ID));
       
    }   //  saveSelectionDetail
    
    // Elaine 2008/12/16
	/**************************************************************************
	 *	Show History
	 */
	protected void showHistory()
	{
		log.info("");
		Integer C_BPartner_ID = getSelectedRowKey();
		if (C_BPartner_ID == null)
			return;
		InvoiceHistory ih = new InvoiceHistory (this, C_BPartner_ID.intValue(), 
			0, 0, 0);
		ih.setVisible(true);
		ih = null;
	}	//	showHistory

	/**
	 *	Has History
	 *  @return true
	 */
	protected boolean hasHistory()
	{
		return true;
	}	//	hasHistory

	/**
	 *	Zoom
	 */
	public void zoom()
	{
		log.info( "InfoBPartner.zoom");
		Integer C_BPartner_ID = getSelectedRowKey();
		if (C_BPartner_ID == null)
			return;
	//	AEnv.zoom(MBPartner.Table_ID, C_BPartner_ID.intValue(), true);	//	SO

		MQuery query = new MQuery("C_BPartner");
		query.addRestriction("C_BPartner_ID", MQuery.EQUAL, C_BPartner_ID);
		query.setRecordCount(1);
		int AD_WindowNo = getAD_Window_ID("C_BPartner", true);	//	SO
		AEnv.zoom (AD_WindowNo, query);
	}	//	zoom

	/**
	 *	Has Zoom
	 *  @return true
	 */
	protected boolean hasZoom()
	{
		return true;
	}	//	hasZoom

	/**
	 * Customize
	 */
	protected void customize()
	{
		log.info("InfoBPartner.customize");
		super.customize();
	} // customize

	/**
	 * Has Customize
	 * 
	 * @return true
	 */
	protected boolean hasCustomize()
	{
		return true;
	}	//	hasCustomize
	//
	
    public void tableChanged(WTableModelEvent event)
    {
        
    }
    
	@Override
	public void onEvent(Event e)
	{
		if (e.getName().equals(Events.ON_CTRL_KEY))
		{
			if (LayoutUtils.isReallyVisible(this))
			{
				onUserQuery();
			}
		} else if (e.getTarget() == sBPTypeCombo) {
			onUserQuery();
			return;

		}
		super.onEvent(e);
	}

	@Override
	public String getSortDirection(Comparator cmpr) {
		return "natural";
	}
	
	/**
	 * is Info BPartner window support Token Search
	 * @return
	 */
	public boolean isTSCapable()
	{
		return true;
	}

	@Override
	public void onUserQuery() {
		contactTbl.setRowCount(0);
		locationTbl.setRowCount(0);
		
		super.onUserQuery();
	}
}
