package org.adempiere.webui.apps;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.South;


/**
 * 
 * @author jtrinidad
 *
 */
public class EventSummaryDialog extends Window {

	private static final long serialVersionUID = -779475945298887887L;

	ConfirmPanel confirmPanel = new ConfirmPanel();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy EEEEE");
	
	public EventSummaryDialog(List<Timestamp> dates) {
		
		super();

		Borderlayout mainLayout = new Borderlayout();
		
		Center center = new Center();
		WListbox list = ListboxFactory.newDataTable();
		list.setMultiple(true);
		list.setColumnClass(0, String.class, true);
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("Dates");
		for (Timestamp date : dates) {
			Vector<Object> line = new Vector<Object>();
			line.add(sdf.format(date));
			
			data.add(line);
		}
		ListModelTable model = new ListModelTable(data);
		list.setData(model, columnNames);
		WListItemRenderer renderer = (WListItemRenderer) list.getItemRenderer();
		renderer.getHeaders().get(0).setWidth("300px");
		
		center.appendChild(list);
		mainLayout.appendChild(center);
		
		confirmPanel.addActionListener(new EventListener<Event>() {
			
			@Override
			public void onEvent(Event event) throws Exception {
				onClose();
				
			}
		});

		South south = new South();
		south.appendChild(confirmPanel);
		mainLayout.appendChild(south);
		
		this.appendChild(mainLayout);
		
		ZKUpdateUtil.setHeight(this, "600px");
		ZKUpdateUtil.setWidth(this, "400px");

		setTitle("Summary of Dates");
		setShadow(true);		
	}

	public static void show(List<Timestamp> dates)
	{
		EventSummaryDialog dlg = new EventSummaryDialog(dates);
		AEnv.showWindow(dlg);
	}
}
