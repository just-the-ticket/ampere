package org.adempiere.webui.apps.form;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.FillMandatoryException;
import org.adempiere.util.Callback;
import org.adempiere.webui.apps.EventSummaryDialog;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Datebox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.DialogForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;
import org.zkoss.calendar.Calendars;
import org.zkoss.calendar.api.CalendarItem;
import org.zkoss.calendar.event.CalendarsEvent;
import org.zkoss.calendar.impl.SimpleCalendarItem;
import org.zkoss.calendar.impl.SimpleCalendarModel;
import org.zkoss.calendar.impl.SimpleDateFormatter;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Window.Mode;


/**
 * 
 * @author jtrinidad
 *
 */

public class WMultidateSelector 
	implements IFormController, EventListener<Event>, WTableModelListener
{

	protected CLogger log = CLogger.getCLogger(getClass());
	
	private DialogForm form = new DialogForm();
	
	
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Panel centerPanel = new Panel();
	private Panel southPanel = new Panel();
	private Grid centerLayout = GridFactory.newGridLayout();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true, false, true, false, false, false);
	
	private Calendars calendar = new Calendars();
	private SimpleCalendarModel calendarModel = new SimpleCalendarModel();
	private Button prevBtn = new Button();
	private Button nextBtn = new Button();
	
	private List<Timestamp> selectedDates = new ArrayList<Timestamp>();
	
	SimpleDateFormat eventDF = new SimpleDateFormat("dd/MM/yyyy E");
	
	private Label				fromLabel		= new Label(Msg.translate(Env.getCtx(), "From"));
	private Label				toLabel			= new Label(Msg.translate(Env.getCtx(), "To"));
	private Datebox				fromDateBox		= new Datebox();
	private Datebox				toDateBox		= new Datebox();
	private Checkbox			monCheckbox		= new Checkbox();
	private Checkbox			tueCheckbox		= new Checkbox();
	private Checkbox			wedCheckbox		= new Checkbox();
	private Checkbox			thuCheckbox		= new Checkbox();
	private Checkbox			friCheckbox		= new Checkbox();
	private Checkbox			satCheckbox		= new Checkbox();
	private Checkbox			sunCheckbox		= new Checkbox();
	private Checkbox			everyCheckbox	= new Checkbox();

	//Option to ask before running the process
	private String messagePrompt = null;
	
	public WMultidateSelector()
		
	{
		try {
			zkInit();
			dynInit();
		} catch (Exception e) {
			log.severe(e.getMessage());
			
		}
	}

	
	private void zkInit() throws Exception
	{
		//Form Init()
		ZKUpdateUtil.setWidth(form, "80%");
		ZKUpdateUtil.setHeight(form, "80%");
		
		form.appendChild(mainLayout);
		ZKUpdateUtil.setWidth(mainLayout, "100%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		
		calendar.setSclass("calendar-picker");
		calendar.setMold("month");
		calendar.addEventListener(CalendarsEvent.ON_ITEM_CREATE, this);
		calendar.addEventListener(CalendarsEvent.ON_ITEM_EDIT, this);
		calendar.setModel(calendarModel);;
		
		Button button = confirmPanel.createButton("list");
		button.setTooltiptext("View summary");
		confirmPanel.addButton(button);
		
		fromLabel.setWidth("60px");
		confirmPanel.appendChild(fromLabel);
		
		fromDateBox.setWidth("120px");
		fromDateBox.addEventListener(Events.ON_CHANGE, this);
		confirmPanel.appendChild(fromDateBox);
		
		toLabel.setWidth("60px");
		confirmPanel.appendChild(toLabel);
		
		toDateBox.setWidth("120px");
		toDateBox.addEventListener(Events.ON_CHANGE, this);
		confirmPanel.appendChild(toDateBox);
		
		
		monCheckbox.addEventListener(Events.ON_CHECK, this);
		monCheckbox.setText(Msg.translate(Env.getCtx(), "Monday"));
		confirmPanel.appendChild(monCheckbox);
		
		tueCheckbox.addEventListener(Events.ON_CHECK, this);
		tueCheckbox.setText(Msg.translate(Env.getCtx(), "Tuesday"));
		confirmPanel.appendChild(tueCheckbox);
	
		wedCheckbox.addEventListener(Events.ON_CHECK, this);
		wedCheckbox.setText(Msg.translate(Env.getCtx(), "Wednesday"));
		confirmPanel.appendChild(wedCheckbox);
		
		thuCheckbox.addEventListener(Events.ON_CHECK, this);
		thuCheckbox.setText(Msg.translate(Env.getCtx(), "Thursday"));
		confirmPanel.appendChild(thuCheckbox);
		
		friCheckbox.addEventListener(Events.ON_CHECK, this);
		friCheckbox.setText(Msg.translate(Env.getCtx(), "Friday"));
		confirmPanel.appendChild(friCheckbox);
		
		satCheckbox.addEventListener(Events.ON_CHECK, this);
		satCheckbox.setText(Msg.translate(Env.getCtx(), "Saturday"));
		confirmPanel.appendChild(satCheckbox);
		
		sunCheckbox.addEventListener(Events.ON_CHECK, this);
		sunCheckbox.setText(Msg.translate(Env.getCtx(), "Sunday"));
		confirmPanel.appendChild(sunCheckbox);
		
		everyCheckbox.addEventListener(Events.ON_CHECK, this);
		everyCheckbox.setText(Msg.translate(Env.getCtx(), "Everyday"));
		confirmPanel.appendChild(everyCheckbox);
		
		prevBtn.setIconSclass("z-icon-arrow-left");
		prevBtn.addEventListener(Events.ON_CLICK, this);
		nextBtn.setIconSclass("z-icon-arrow-right");
		nextBtn.addEventListener(Events.ON_CLICK, this);
		
		parameterPanel.appendChild(parameterLayout);
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		Rows rows = null;
		Row row = null;
		ZKUpdateUtil.setWidth(parameterLayout, "100%");
		rows = parameterLayout.newRows();
		row = rows.newRow();
		Hbox hbox = new Hbox();
		
		hbox.appendChild(prevBtn);
		hbox.appendChild(nextBtn);
		row.appendChild(hbox);
		
		centerPanel.appendChild(centerLayout);
		ZKUpdateUtil.setWidth(centerLayout, "100%");
		calendar.setHeight("600px");
		Center center = new Center();
		mainLayout.appendChild(center);
		center.setStyle("border: none");
		center.appendChild(centerPanel);
		centerPanel.appendChild(calendar);
//		tree = new Tree();
//		treeCols = new Treecols();
//		treeCol = new Treecol("");
//		treeCol2 = new Treecol();
//		centerPanel.appendChild(tree);
//		treeCols.appendChild(treeCol);
//		treeCols.appendChild(treeCol2);
//        tree.appendChild(treeCols); 		
		ZKUpdateUtil.setVflex(center, "flex");
		center.setAutoscroll(true);
		
		South south = new South();
		south.appendChild(southPanel);
		southPanel.appendChild(confirmPanel);	
		ZKUpdateUtil.setWidth(southPanel, "100%");
		mainLayout.appendChild(south);
		confirmPanel.addActionListener(this);
		
		
		SelectorDateFormatter dfmater = new SelectorDateFormatter();
		calendar.setDateFormatter(dfmater);
	}
	
	
	public void setMessagePrompt(String messagePrompt) {
		this.messagePrompt = messagePrompt;
	}


	private void dynInit() throws Exception
	{
//		getForm().getProcessInfo().getRecord_ID();
	}
	
	/**
	 * Display a dialog form to list all selected events
	 * This makes it convenient to display everything
	 * if the events are spread out through multiple
	 * months
	 */
	public void onSummary()
	{
//		List<Timestamp> list = 
		Collections.sort(selectedDates);
		EventSummaryDialog.show(selectedDates);
	}
	
	/**
	 * Form inherits this can use onProcess to do
	 * processing for the selected dates
	 * @throws Exception 
	 */
	public void onProcess(boolean response) throws Exception
	{
		
	}
	
	
	@Override
	public void tableChanged(WTableModelEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().equals(calendar)) {
			CalendarsEvent ce = (CalendarsEvent) event; 
			if (ce.getName().equals(CalendarsEvent.ON_ITEM_CREATE)) {
				//this is a temporary fix as Calendar on 3rd page shows +1 more instead of event
				Timestamp dDate = TimeUtil.trunc(new Timestamp(ce.getBeginDate().getTime()), TimeUtil.TRUNC_DAY);
				if (selectedDates.contains(dDate)) {
					selectedDates.remove(dDate);
					Date beginDate = new Date(dDate.getTime());
					Date endDate = new Date(TimeUtil.addDays(dDate, 1).getTime());
					List<CalendarItem> evtDelete = calendarModel.get(beginDate, endDate, null);
					if (evtDelete.size() == 1) {
						calendarModel.remove(evtDelete.get(0));
					}
					return;
				}
				selectedDates.add(dDate);
				SimpleCalendarItem evt = new SimpleCalendarItem();
				evt.setBeginDate(ce.getBeginDate());
				 
				Timestamp end =  TimeUtil.addDays(new Timestamp(ce.getBeginDate().getTime()), 1);
				evt.setEndDate(end);
				evt.setTitle("" + (selectedDates.size() -1) );
				evt.setContent(eventDF.format(ce.getBeginDate()));
//				evt.setHeaderColor("red");
//				evt.setContentColor("green");
				calendarModel.add(evt);
			} else if (ce.getName().equals(CalendarsEvent.ON_ITEM_EDIT)) {
				selectedDates.remove(calendarModel.indexOf(ce.getCalendarItem()));
				calendarModel.remove(ce.getCalendarItem());
			}
		} else if (event.getName().equals(Events.ON_CLICK)) {
			if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK))) {
//				boolean response = false;
				if (messagePrompt != null) {
					FDialog.ask(form.getWindowNo(), form, messagePrompt, new Callback<Boolean>() {
						
						@Override
						public void onCallback(Boolean result) {
							try {
								onProcess(result);
							} catch (Exception e) {
								log.severe(e.getMessage());
							}
							form.onClose();
							
						}
					});
				} else {
					onProcess(false);
					form.onClose();
				}
			} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL))) {
				form.onClose();
			} else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_RESET))) {
				calendarModel.clear();
				selectedDates.clear();
			} else if (event.getTarget().equals(confirmPanel.getButton("list"))) {
				onSummary();
			} else if (event.getTarget().equals(prevBtn)) {
				calendar.previousPage();
			} else if (event.getTarget().equals(nextBtn)) {
				calendar.nextPage();
			}
		}
		else if (event.getName().equals(Events.ON_CHECK))
		{
			validate(event.getTarget());

			if (event.getTarget().equals(monCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.MONDAY);
				if (monCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);
				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(tueCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.TUESDAY);
				if (tueCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);
				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(wedCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.WEDNESDAY);
				if (wedCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);
				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(thuCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.THURSDAY);
				if (thuCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);
				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(friCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.FRIDAY);
				if (friCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);

				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(satCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.SATURDAY);
				if (satCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);
				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(sunCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), DayOfWeek.SUNDAY);
				if (sunCheckbox.isChecked())
					createCalenderItems(dates);
				else
					removeItems(dates);
				everyCheckbox.setChecked(false);
			}
			else if (event.getTarget().equals(everyCheckbox))
			{
				List<Date> dates = getSpecificWeekdaysBetweenDates(fromDateBox.getValueInLocalDate(),
						toDateBox.getValueInLocalDate(), null);
				if (everyCheckbox.isChecked())
				{
					if (monCheckbox.isChecked() || tueCheckbox.isChecked() || wedCheckbox.isChecked()
							|| thuCheckbox.isChecked() || friCheckbox.isChecked() || satCheckbox.isChecked()
							|| sunCheckbox.isChecked())
					{
						removeItems(dates);
					}
					createCalenderItems(dates);
				}
				else
					removeItems(dates);

				monCheckbox.setChecked(everyCheckbox.isChecked());
				tueCheckbox.setChecked(everyCheckbox.isChecked());
				wedCheckbox.setChecked(everyCheckbox.isChecked());
				thuCheckbox.setChecked(everyCheckbox.isChecked());
				friCheckbox.setChecked(everyCheckbox.isChecked());
				satCheckbox.setChecked(everyCheckbox.isChecked());
				sunCheckbox.setChecked(everyCheckbox.isChecked());
			}
		}
	}


	private void removeItems(List<Date> dates)
	{
		Timestamp end = TimeUtil.addDays(new Timestamp(toDateBox.getValue().getTime()), 1);
		List<CalendarItem> items = calendarModel.get(fromDateBox.getValue(), end, null);
		removeCalenderItems(dates, items);
	}


	private void removeCalenderItems(List<Date> dates, List<CalendarItem> items)
	{
		for (CalendarItem ce : items)
		{
			if (dates.contains(ce.getBeginDate()))
			{
				int dateIndex = calendarModel.indexOf(ce);
				if (dateIndex >= 0)
				{
					calendarModel.remove(ce);
					if (dateIndex < selectedDates.size())
						selectedDates.remove(dateIndex);
				}
			}
		}
	}


	private void createCalenderItems(List<Date> dates)
	{
		for (Date dt : dates)
		{
			SimpleCalendarItem evt = new SimpleCalendarItem();
			evt.setBeginDate(dt);

			Timestamp end = TimeUtil.addDays(new Timestamp(dt.getTime()), 1);
			evt.setEndDate(end);
			evt.setTitle("" + (selectedDates.size() - 1));
			evt.setContent(eventDF.format(dt));
			Timestamp dDate = TimeUtil.trunc(new Timestamp(dt.getTime()), TimeUtil.TRUNC_DAY);
			
			if (!selectedDates.contains(dDate))
				selectedDates.add(dDate);
			calendarModel.add(evt);
		}
	}
	
    public static List<Date> getSpecificWeekdaysBetweenDates(LocalDate startDate, LocalDate endDate, DayOfWeek dayOfWeekToFind) {
        List<Date> specificWeekdays = new ArrayList<>();

		int interval = 1;
		LocalDate currentDate = startDate;
		if (dayOfWeekToFind != null)
		{
			// Find the first occurrence of the specified dayOfWeekToFind
			currentDate = startDate.with(java.time.temporal.TemporalAdjusters.nextOrSame(dayOfWeekToFind));
			interval = 7;
		}
        
        // Iterate from the first occurrence, adding 7 days each time, until the end date is reached
        while (!currentDate.isAfter(endDate)) {
            specificWeekdays.add(convertToDateViaInstant(currentDate));
            currentDate = currentDate.plusDays(interval);
        }

        return specificWeekdays;
    }
    
    public static Date convertToDateViaInstant(LocalDate dateToConvert) {
        return Date.from(dateToConvert.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }


	private void validate(Component component)
	{
		String errorMsg = "";
		if (fromDateBox.getValue() == null)
		{
			errorMsg = fromLabel.getValue();
		}

		if (toDateBox.getValue() == null)
		{
			if (!Util.isEmpty(errorMsg))
				errorMsg += ", ";
			errorMsg += toLabel.getValue();
		}
		
		if (!Util.isEmpty(errorMsg))
		{
			if(component instanceof Checkbox)
				((Checkbox) component).setChecked(false);
			throw new FillMandatoryException(errorMsg);
		}

		if (fromDateBox.getValue().compareTo(toDateBox.getValue()) > 0)
		{
			if(component instanceof Checkbox)
				((Checkbox) component).setChecked(false);
			throw new  AdempiereException("From date cannot be greater than To date");
		}
	}

	
	public List<Timestamp> getSelectedDates() {
		Collections.sort(selectedDates);

		return selectedDates;
	}

	
	@Override
	public ADForm getForm() {
		return form;
	}

	/**
	 * 
	 * @return
	 */
	public Mode getWindowMode() {
		return Mode.HIGHLIGHTED;
	}
	
	class SelectorDateFormatter extends SimpleDateFormatter 
	{

		private String _weekFormat = "EEE";
		private String _ppFormat = "dd/MMM EEE";

		private DateTimeFormatter _df, _wf, _tf, _pf;

		@Override
		public String getCaptionByDate(ZonedDateTime date, Locale locale) {
			return super.getCaptionByDate(date, locale);
		}

		@Override
		public String getCaptionByDateOfMonth(ZonedDateTime date, Locale locale) {
			if (_df ==null) {
				_df = DateTimeFormatter.ofPattern("dd/MM/yyyy", locale);
			}
				return date.format(_df);
		}

		@Override
		public String getCaptionByDayOfWeek(ZonedDateTime date, Locale locale) {
			if (_wf == null) {
				_wf = DateTimeFormatter.ofPattern(_weekFormat, locale);
			}
			return date.format(_wf).toUpperCase().substring(0, 3);
		}

		@Override
		public String getCaptionByTimeOfDay(ZonedDateTime date, Locale locale) {
			return super.getCaptionByTimeOfDay(date, locale);
		}

		@Override
		public String getCaptionByPopup(ZonedDateTime date, Locale locale) {
			if (_pf == null) {
				_pf = DateTimeFormatter.ofPattern(_ppFormat, locale);
			}
			return date.format(_pf).replace(".", "");
		}

		@Override
		public String getCaptionByWeekOfYear(ZonedDateTime date, Locale locale) {
			return super.getCaptionByWeekOfYear(date, locale);
		}


	}
}
