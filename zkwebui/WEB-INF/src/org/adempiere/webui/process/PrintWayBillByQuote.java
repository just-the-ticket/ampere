package org.adempiere.webui.process;

import java.io.File;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MQuery;
import org.compiere.model.MTable;
import org.compiere.model.PrintInfo;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/**
 * @author Logilite Technologies
 *
 * Process to Generate Report of the selected Order Lines from the Print Waybill by Trip info window
 *
 */
public class PrintWayBillByQuote extends SvrProcess
{
	private static int		RESOURCETYPE_DRIVERHOURS_VALUE	= 1000000;

	private static int		PRINTFORMAT_WAYBILL_ID			= 1000477;

	private static String	COLUMNNAME_PRINTORDERLINES		= "PrintOrderLines";
	private static String	COLUMNNAME_RESOURCETYPE			= "Resource Type";
	private static String	COLUMNNAME_RESOURCE				= "Resource";
	private static String	sql								= "SELECT ol.C_OrderLine_ID,a.S_Resource_ID FROM C_OrderLine ol "
																+ " INNER JOIN c_orderline_detail ld on (ol.c_orderline_id = ld.c_orderline_id)"
																+ " INNER JOIN S_ResourceAssignment a on (a.c_orderline_detail_id = ld.c_orderline_detail_id)"
																+ " WHERE ol.C_OrderLine_ID = ? AND a.S_ResourceType_ID = ?";

	@Override
	protected void prepare()
	{
		
	}

	@Override
	protected String doIt() throws Exception
	{
		List<KeyNamePair> keys = getInfoSelectionKeys();

		if (keys == null || keys.size() == 0)
		{
			return "No selected items";
		}
		
		Map<Integer, Map<Integer, List<Integer>>> groupedOrderLines = new HashMap<>();
		for (int i = 0; i < keys.size(); i++)
		{
			int orderID = 0;
			KeyNamePair key = keys.get(i);
			LinkedHashMap<String, Object> fields = getInfoSelectionValues().get(key);

			KeyNamePair resouceType = (KeyNamePair) fields.get(COLUMNNAME_RESOURCETYPE);
			int orderLineID  = key.getKey();
			KeyNamePair resource = (KeyNamePair) fields.get(COLUMNNAME_RESOURCE);
			int resourceID = resource.getKey();
			if (resouceType.getKey() != RESOURCETYPE_DRIVERHOURS_VALUE)
			{
				orderLineID = 0;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				
				try
				{
					pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
					pstmt.setInt(1, key.getKey());
					pstmt.setInt(2, RESOURCETYPE_DRIVERHOURS_VALUE);
					rs = pstmt.executeQuery();
					
					while (rs.next())
					{
						orderLineID = rs.getInt(1);
						resourceID = rs.getInt(2);
					}
				}
		        catch (SQLException ex)
		        {
		            throw new IllegalStateException("Could not get data");
		        }
		        finally
		        {
		            DB.close(rs, pstmt);
		        }
				if(orderLineID==0)
				{
					continue;
				}
			}
			MOrderLine orderLine = new MOrderLine(getCtx(), orderLineID, get_TrxName());
			orderID = orderLine.getC_Order_ID();

			if (!groupedOrderLines.containsKey(resourceID))
			{
				groupedOrderLines.put(resourceID, new HashMap<>());
			}

			Map<Integer, List<Integer>> ordersMap = groupedOrderLines.get(resourceID);
			if (!ordersMap.containsKey(orderID))
			{
				ordersMap.put(orderID, new ArrayList<>());
			}

			ordersMap.get(orderID).add(orderLine.getC_OrderLine_ID());
		}

		int i= 1;
		for (Map.Entry<Integer, Map<Integer, List<Integer>>> resourceEntry : groupedOrderLines.entrySet())
		{
			Map<Integer, List<Integer>> ordersMap = resourceEntry.getValue();

			for (Map.Entry<Integer, List<Integer>> orderEntry : ordersMap.entrySet())
			{
				int orderID = orderEntry.getKey();
				List<Integer> orderLines = orderEntry.getValue();

				StringBuilder orderLineIDs = new StringBuilder();
				for (int line : orderLines)
				{
					if (orderLineIDs.length() > 0)
					{
						orderLineIDs.append(", ");
					}
					orderLineIDs.append(line);
				}
				MOrder so = new MOrder(getCtx(), orderID, get_TrxName());
				File file = getReport(PRINTFORMAT_WAYBILL_ID, orderID, orderLineIDs, so.getDocumentNo()+"_"+i);
				openReportInNewTab(so.getDocumentNo()+"_"+i, file);
				i++;
			}
		}

		return null;
	}

	private void openReportInNewTab(String filename, File file) throws Exception
	{
		SimplePDFViewer pdfViewer = new SimplePDFViewer(filename, new FileInputStream(file));
		pdfViewer.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
		ZKUpdateUtil.setWidth(pdfViewer, "100%"); 

		final SimplePDFViewer wayBillByQuotePdfViewer = pdfViewer;
		if (wayBillByQuotePdfViewer != null)
		{
			//
			AEnv.executeAsyncDesktopTask(new Runnable() {

				@Override
				public void run()
				{
					SessionManager.getAppDesktop().showWindow(wayBillByQuotePdfViewer);
				}
			});
		}
	} // openReportInNewTab

	private File getReport(int AD_PrintFormat_ID, int orderID, StringBuilder keyValues, String fileName)
	{
		Env.setContext(Env.getCtx(), "-1|"+COLUMNNAME_PRINTORDERLINES, keyValues.toString());
		MPrintFormat pf = new MPrintFormat(Env.getCtx(), AD_PrintFormat_ID, get_TrxName());
		File file = null;
		MTable table = MTable.get(Env.getCtx(), pf.getAD_Table_ID());
		MQuery query = new MQuery(table.getTableName());

		query.addRestriction(MOrder.COLUMNNAME_C_Order_ID, MQuery.EQUAL, orderID);

		PrintInfo info = new PrintInfo(table.getTableName(), pf.getAD_Table_ID(), orderID);
		try
		{
			ReportEngine re = new ReportEngine(Env.getCtx(), pf, query, info, get_TrxName());

			file = File.createTempFile(fileName, ".pdf");
			re.createPDF(file);

		}
		catch (Exception e)
		{
			if (e.getLocalizedMessage().contains("org.postgresql.util.PSQLException: ERROR: column")
					&& e.getLocalizedMessage().contains("does not exist\n Position"))
			{
				throw new AdempiereException("Incorrect Print Format");
			}
			else
			{
				e.printStackTrace();
			}
		}
		return file;
	} // getReport

}
