package org.adempiere.webui.process;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.compiere.model.MEntityType;
import org.compiere.model.MPrintFontLibrary;
import org.compiere.model.MSysConfig;
import org.compiere.print.MPrintFont;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Ini;

import bsh.StringUtil;

import org.adempiere.pdf.Document;
import org.adempiere.webui.apps.AEnv;

public class CreateFontFromLibrary extends SvrProcess {

	
	private String		p_fontSizes = null;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null && para[i].getParameter_To() == null)
				;
			else if (name.equalsIgnoreCase("FontSizes"))
			{
				p_fontSizes = para[i].getParameterAsString().replace(" ", "");
			}
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		MPrintFontLibrary library = new MPrintFontLibrary(getCtx(), getRecord_ID(), get_TrxName());
		if (library.getAttachment().getEntryCount() == 0) {
			return "No font library attached. Please upload one.";
		}
		
		String filename = library.getAttachment().getEntry(0).getFileName();
		
		if (filename.contains(File.separator)) {
			String a[] = StringUtil.split(filename, File.separator);
			filename = a[a.length - 1];
			
		}
		
		if (!(filename.endsWith(".otf") 
				|| filename.endsWith(".ttf"))) {
			return "Font library must end either with .otf or .ttf";
		}
		
		String error = saveToFontDirectory(library.getAttachment().getEntry(0).getFile());

		if (!error.isEmpty()) {
			return error;
		}
		
		if (p_fontSizes == null || p_fontSizes.length() == 0) {
			return "Nothing to create";
		}
		
		String name = StringUtils.left(filename, filename.length() - 4);
		
		String existingFontSizes = DB.getSQLValueString(null, 
				"SELECT ARRAY_TO_STRING(ARRAY (SELECT DISTINCT (p.fontsize) FROM ad_printfont p WHERE p.AD_PrintFont_Library_ID = ? ORDER BY p.fontsize), ', ')"
				, getRecord_ID());
		
		ArrayList<String> existingSizes = new ArrayList<String>();
		Collections.addAll(existingSizes, StringUtils.split(existingFontSizes, ","));
		
		String n_sizes[] =StringUtils.split(p_fontSizes, ",");
		
		for (String s : n_sizes) {
			try {
				if (existingSizes.contains(s)) {
					continue;
				}
				int fontsize = Integer.parseInt(s);
				MPrintFont font = new MPrintFont(getCtx(), 0, get_TrxName());
				font.setAD_PrintFont_Library_ID(library.getAD_PrintFont_Library_ID());
				font.setCode(".");
				font.setIsEmbeddedFont(true);
				font.setFontFilename(filename);
				font.setFontSize(fontsize);
				font.setName(name + "-" + fontsize);
				font.save();
				addLog(0, null, null, font.getName() + " created.");
			} catch (Exception e) {
				
			}
		}
		
		return null;
	}

	private String saveToFontDirectory(File file)
	{
		String pdfFontDir = MSysConfig.getValue(Document.PDF_FONT_DIR, "");
		if (pdfFontDir == null || pdfFontDir.trim().length() == 0) {
			MSysConfig fontDir = new MSysConfig(getCtx(), 0, null);
			fontDir.setAD_Client_ID(0);
			fontDir.setAD_Org_ID(0);
			fontDir.setConfigurationLevel(MSysConfig.CONFIGURATIONLEVEL_System);
			fontDir.setEntityType("U");
			fontDir.setName(Document.PDF_FONT_DIR);
			String home = Ini.getAdempiereHome();
			if (!home.endsWith(File.separator)) {
				home = home +  File.separator; 
			}
			String path = home + "PdfFonts";
			fontDir.setValue(path);
			fontDir.save();
			MSysConfig.resetCache();
			pdfFontDir = MSysConfig.getValue(Document.PDF_FONT_DIR, "");
		}
		String filename = "";
		try {
			
			File directory = new File(pdfFontDir);
			if (!directory.exists()) {
				if (!directory.mkdir()) {
//					log.severe("Cannot create font directory - " + pdfFontDir);
					return "Cannot create font directory - " + pdfFontDir;
				}
			}
			filename = pdfFontDir +  File.separator + file.getName();
			
			File fontFile = new File(filename);
			if (fontFile.exists()) {
				return "";
			}
		
		
			FileUtils.copyFile(file, fontFile);
		} catch (IOException e) {
			log.severe(e.getMessage());
			return "Please check if attachment exists. Unable to copy to local filesystem - "  + filename;
		}
		return "";
	}
}
