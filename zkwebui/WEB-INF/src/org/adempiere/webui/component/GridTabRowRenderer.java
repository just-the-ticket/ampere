/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.editor.IZoomableEditor;
import org.adempiere.webui.editor.WButtonEditor;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WEditorPopupMenu;
import org.adempiere.webui.editor.WNumberEditor;
import org.adempiere.webui.editor.WPAttributeEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WebEditorFactory;
import org.adempiere.webui.event.ActionEvent;
import org.adempiere.webui.event.ActionListener;
import org.adempiere.webui.event.ContextMenuListener;
import org.adempiere.webui.panel.ADTabpanel;
import org.adempiere.webui.panel.AbstractADWindowPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.GridTabDataBinder;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.ADWindow;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridWindow;
import org.compiere.model.MSysConfig;
import org.compiere.model.Obscure;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.NamePair;
import org.compiere.util.Util;
import org.zkoss.zhtml.Input;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Paging;
import org.zkoss.zul.RendererCtrl;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

/**
 * Row renderer for GridTab grid.
 * @author hengsin
 * 
 * @author Teo Sarca, teo.sarca@gmail.com
 * 		<li>BF [ 2996608 ] GridPanel is not displaying time
 * 			https://sourceforge.net/tracker/?func=detail&aid=2996608&group_id=176962&atid=955896
 */
public class GridTabRowRenderer implements RowRenderer<Object>, RowRendererExt, RendererCtrl, EventListener<Event> {

	private static final CLogger s_log = CLogger.getCLogger(GridTabRowRenderer.class);
	public static final String GRID_ROW_INDEX_ATTR = "grid.row.index";
	private static final String CURRENT_ROW_STYLE = "gridtabrow-current";
	private static final String PROCESSED_ROW_STYLE = "gridtabrow-processed";
	private static final String ROW_STYLE = "gridtabrow";
	private static final int MAX_TEXT_LENGTH_DEFAULT = 60;
	// code to set row on refresh
	public static final int CODE_REFRESH = 1;
	// code to set row by click
	public static final int CODE_CLICK = 0;

	private GridTab gridTab;
	private int windowNo;
	private GridTabDataBinder dataBinder;
	private Map<GridField, WEditor> editors = new LinkedHashMap<GridField, WEditor>();
	private Paging paging;

	private Map<String, Map<Object, String>> lookupCache = null;
	private RowListener rowListener;

	private Grid grid = null;
	private GridPanel gridPanel = null;
	private Row currentRow;
	private Object[] currentValues;
	private boolean editing = false;
	private int currentRowIndex = -1;
	private AbstractADWindowPanel m_windowPanel;
	private ActionListener buttonListener;
	GridWindow gridWindow = null;
	
	// Row-wise Editors Map
	public Map<Row, ArrayList<WEditor>>			editorsListMap			= new LinkedHashMap<Row, ArrayList<WEditor>>();
	private Map<GridField, WEditor> readOnlyEditors = new LinkedHashMap<GridField, WEditor>();

	/**
	 * Flag detect this view has customized column or not value is set at
	 * {@link #render(Row, Object[], int)}
	 */
	private boolean isGridViewCustomized = false;

	/**
	 *
	 * @param gridTab
	 * @param windowNo
	 */
	public GridTabRowRenderer(GridTab gridTab, int windowNo) {
		this.gridTab = gridTab;
		this.windowNo = windowNo;
		this.dataBinder = new GridTabDataBinder(gridTab);
	}

	private WEditor getEditorCell(GridField gridField, Object object, int i) {
		// Remark: following Included tab is create new editor from factory with
		// GridField, otherwise line cause the NPE on each row.
		WEditor editor;
		if (gridTab.isIncluded())
			editor = WebEditorFactory.getEditor(gridField, true);
		else
			editor = editors.get(gridField);
		
		if (editor != null)  {
			if (editor instanceof WButtonEditor)
            {
				if (buttonListener != null)
				{
					((WButtonEditor)editor).addActionListener(buttonListener);	
				}
				else
				{
					Object window = SessionManager.getAppDesktop().findWindow(windowNo);
	            	if (window != null && window instanceof ADWindow)
	            	{
	            		AbstractADWindowPanel windowPanel = ((ADWindow)window).getADWindowPanel();
	            		((WButtonEditor)editor).addActionListener(windowPanel);
	            	}
				}
            }

			// Remark: following Included tab is set value from object,
			// otherwise all line rendered with duplicate values.
			if (gridTab.isIncluded())
				editor.setValue(object);
			else
				editor.setValue(gridField.getValue());
			
            //Stretch component to fill grid cell
            if (editor.getComponent() instanceof Textbox)
//				ZKUpdateUtil.setWidth((HtmlBasedComponent)editor.getComponent(), "94%");
            	ZKUpdateUtil.setHflex((HtmlBasedComponent)editor.getComponent(), "true");
            else
            	editor.fillHorizontal();

            if(editor instanceof WNumberEditor)
            {
            	//remove calculator button
            	if (gridTab.isIncluded() && editor.getComponent().getChildren().size() > 1) {
            		editor.getComponent().getChildren().remove(1);
            	}
            }
		}
		return editor;
	}

	private int getColumnIndex(GridField field) {
		GridField[] fields = gridTab.getFields();
		for(int i = 0; i < fields.length; i++) {
			if (fields[i] == field)
				return i;
		}
		return 0;
	}

	private Component createReadonlyCheckbox(Object value) {
		Checkbox checkBox = new Checkbox();
		if (value != null && "true".equalsIgnoreCase(value.toString()))
			checkBox.setChecked(true);
		else
			checkBox.setChecked(false);
		checkBox.setDisabled(true);
		return checkBox;
	}

	private String getDisplayText(Object value, GridField gridField)
	{
		if (value == null)
			return "";

		if (gridField.isEncryptedField())
		{
			return "********";
		}
		else if (!Util.isEmpty(gridField.getObscureType(), true))
		{
			return new Obscure("", gridField.getObscureType()).getObscuredValue(value.toString());
		}
		else if (gridField.isLookup())
    	{
			if (lookupCache != null)
			{
				Map<Object, String> cache = lookupCache.get(gridField.getColumnName());
				if (cache != null && cache.size() >0)
				{
					String text = cache.get(value);
					if (text != null)
					{
						return text;
					}
				}
			}
			NamePair namepair = gridField.getLookup().get(value);
			if (namepair != null)
			{
				String text = namepair.getName();
				if (lookupCache != null)
				{
					Map<Object, String> cache = lookupCache.get(gridField.getColumnName());
					if (cache == null)
					{
						cache = new HashMap<Object, String>();
						lookupCache.put(gridField.getColumnName(), cache);
					}
					cache.put(value, text);
				}
				return text;
			}
			else
				return "";
    	}
    	else if (gridTab.getTableModel().getColumnClass(getColumnIndex(gridField)).equals(Timestamp.class))
    	{
    		WEditor editor = WebEditorFactory.getEditor(gridField, true);    		
    		return  editor.getDisplayTextForGridView(value);
    	}
    	else if (DisplayType.isNumeric(gridField.getDisplayType()) && value instanceof Number)
    	{
    		return DisplayType.getNumberFormat(gridField.getDisplayType(), AEnv.getLanguage(Env.getCtx())).format(value);
    	}
    	else if (DisplayType.Button == gridField.getDisplayType())
    	{
    		return "";
    	}
    	else if (DisplayType.Image == gridField.getDisplayType())
    	{
    		if (value == null || (Integer)value <= 0)
    			return "";
    		else
    			return "...";
    	}
    	else
    		return value.toString();
	}

	private Component getDisplayComponent(int rowIndex, Object value, GridField gridField) {
		Component component;
		if (gridField.getDisplayType() == DisplayType.YesNo) {
			component = createReadonlyCheckbox(value);
		} else if (gridField.getDisplayType() == DisplayType.Button) {
			WButtonEditor editor = new WButtonEditor(gridField, rowIndex);
			editor.setValue(gridTab.getValue(rowIndex, gridField.getColumnName()));
			editor.setReadWrite(gridField.isEditable(true));
			editor.getComponent().setAttribute("grid.row.index", rowIndex);
			editor.addActionListener(buttonListener);
			component = editor.getComponent();
		} else {
			String text = getDisplayText(value, gridField);

			Label label = new Label();
			label.setMultiline(false);
			label.setStyle("white-space: nowrap; overflow: hidden; text-overflow: ellipsis;");
			setLabelText(text, label);	

			component = label;
		}
		return component;
	}

	/**
	 * @param text
	 * @param label
	 */
	private void setLabelText(String text, Label label) {
		String display = text;
		final int MAX_TEXT_LENGTH = MSysConfig.getIntValue(MSysConfig.MAX_TEXT_LENGTH_ON_GRID_VIEW, MAX_TEXT_LENGTH_DEFAULT,Env.getAD_Client_ID(Env.getCtx()));
		if (text != null && text.length() > MAX_TEXT_LENGTH)
			display = text.substring(0, MAX_TEXT_LENGTH - 3) + "...";

		label.setValue(display);

		final int MIN_TOOLTIP_TEXT_LENGTH = MSysConfig.getIntValue(MSysConfig.MIN_TOOLTIP_TEXT_LENGTH_ON_GRID_VIEW, MAX_TEXT_LENGTH_DEFAULT, Env.getAD_Client_ID(Env.getCtx()));
		if (text != null && (text.length() > MAX_TEXT_LENGTH || text.length() > MIN_TOOLTIP_TEXT_LENGTH))
			label.setTooltiptext(text);
	}

	/**
	 *
	 * @return active editor list
	 */
	public List<WEditor> getEditors() {
		List<WEditor> editorList = new ArrayList<WEditor>();
		if (!editors.isEmpty())
			editorList.addAll(editors.values());

		return editorList;
	}

	/**
	 * @param paging
	 */
	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	/**
	 * Detach all editor and optionally set the current value of the editor as cell label.
	 * @param updateCellLabel
	 */
	public void stopEditing(boolean updateCellLabel) {
		if (!editing) {
			return;
		} else {
			editing = false;
		}
		Row row = null;
		for (Entry<GridField, WEditor> entry : editors.entrySet()) {
			if (entry.getValue().getComponent().getParent() != null) {
				Component child = entry.getValue().getComponent();
				Cell cell = null;
				while (cell == null && child != null) {
					Component parent = child.getParent();
					if (parent instanceof Cell && parent.getParent() instanceof Row)
						cell = (Cell)parent;
					else
						child = parent;
				}
				Component component = cell.getFirstChild();
				if (updateCellLabel) {
					if (component instanceof Label) {
						Label label = (Label)component;
						label.getChildren().clear();
						String text = getDisplayText(entry.getValue().getValue(), entry.getValue().getGridField());
						setLabelText(text, label);
					} else if (component instanceof Checkbox) {
						Checkbox checkBox = (Checkbox)component;
						Object value = entry.getValue().getValue();
						if (value != null && "true".equalsIgnoreCase(value.toString()))
							checkBox.setChecked(true);
						else
							checkBox.setChecked(false);
					}
				}
				component.setVisible(true);
				if (row == null)
					row = ((Row)cell.getParent());

				entry.getValue().getComponent().detach();
				entry.getKey().removePropertyChangeListener(entry.getValue());
				entry.getValue().removeValuechangeListener(dataBinder);
			}
		}

		GridTableListModel model = (GridTableListModel) grid.getModel();
		model.setEditing(false);
	}

	/**
	 * @param row
	 * @param data
	 * @see RowRenderer#render(Row, Object)
	 */
	public void render(Row row, Object data, int index) throws Exception {
		//don't render if not visible
		int columnCount = 0;
		Object [] dataObj = (Object[]) data;
		GridField[] gridPanelFields = null;
		GridField[] gridTabFields = null;
		
		if (gridPanel != null)
		{
			if (!gridPanel.isVisible())
			{
				return;
			}
			else
			{
				gridPanelFields = gridPanel.getFields();
				columnCount = gridPanelFields.length;
				gridTabFields = gridTab.getFields();
				isGridViewCustomized = gridTabFields.length != gridPanelFields.length;
			}
		}

		if (grid == null)
			grid = (Grid) row.getParent().getParent();

		if (rowListener == null)
			rowListener = new RowListener((Grid)row.getParent().getParent());

		if (!isGridViewCustomized)
		{
			for (int i = 0; i < gridTabFields.length; i++)
			{
				if (gridPanelFields[i].getAD_Field_ID() != gridTabFields[i].getAD_Field_ID())
				{
					isGridViewCustomized = true;
					break;
				}
			}
		}
		
		if (!isGridViewCustomized)
		{
			currentValues = dataObj;
		}
		else
		{
			List<Object> dataList = new ArrayList<Object>();
			for (GridField gridField : gridPanelFields)
			{
				for (int i = 0; i < gridTabFields.length; i++)
				{
					if (gridField.getAD_Field_ID() == gridTabFields[i].getAD_Field_ID())
					{
						dataList.add(dataObj[i]);
						break;
					}
				}
			}
			currentValues = dataList.toArray(new Object[0]);
		}
		
		if (!isGridViewCustomized)
			columnCount = gridTab.getTableModel().getColumnCount();
		
		Grid grid = (Grid) row.getParent().getParent();
		org.zkoss.zul.Columns columns = grid.getColumns();

		int rowIndex = index;
		if (paging != null && paging.getPageSize() > 0) {
			rowIndex = (paging.getActivePage() * paging.getPageSize()) + rowIndex;
		}
		
		Cell cell = new Cell();
		cell.setWidth("28px");
		cell.setTooltiptext(Msg.getMsg(Env.getCtx(), "Select"));
		Checkbox selection = new Checkbox();
		selection.setAttribute(GRID_ROW_INDEX_ATTR, rowIndex);
		selection.setChecked(gridTab.isSelected(rowIndex));
		cell.setStyle("background-color: transparent !important;");
		selection.addEventListener(Events.ON_CHECK, this);

		if (!selection.isChecked()) {
			if (gridPanel.selectAll.isChecked()) {
				gridPanel.selectAll.setChecked(false);
			}
		}

		cell.appendChild(selection);
		row.appendChild(cell);

		// Editors List of Current Rendered Row
		ArrayList<WEditor> 	editorsList = new ArrayList<WEditor>();
		
		boolean processed = false;
		int colIndex = -1;
		for (int i = 0; i < columnCount; i++)
		{
			if (editors.get(gridPanelFields[i]) == null) {
				WEditor editor = WebEditorFactory.getEditor(gridPanelFields[i], true);
				editors.put(gridPanelFields[i], editor);
				if (editor instanceof WButtonEditor) {
					((WButtonEditor)editor).addActionListener(buttonListener);
				}
			}
//			WEditor readOnlyEditor = WebEditorFactory.getEditor(gridPanelFields[i], true);
//			if (readOnlyEditor != null) {
//				readOnlyEditor.setReadWrite(false);
//				readOnlyEditors.put(gridPanelFields[i], readOnlyEditor);
//			}

			if (gridPanelFields[i].getColumnName().equals("Processed") && (Boolean)currentValues[i]) {
				processed = true;
			}
			
			if ((!isGridViewCustomized && gridPanelFields[i].isDisplayedGrid()) || gridPanelFields[i].isToolbarButton())
			{
				continue;
			}
			colIndex++;

			cell = new Cell();
			String cellStyle = "height: 100%; cursor: pointer";
			org.zkoss.zul.Column column = (org.zkoss.zul.Column) columns.getChildren().get(colIndex);
			if (column.isVisible())
			{
				if (gridTab.isIncluded())
				{
					if (DisplayType.YesNo == gridPanelFields[i].getDisplayType()
							|| DisplayType.Image == gridPanelFields[i].getDisplayType()
							|| gridPanelFields[i].isHeading())
					{
						cellStyle += "text-align:center; ";
					}
					WEditor editor = getEditorCell(gridPanelFields[i], currentValues[i], i);
					editorsList.add(editor);
					// Heading Field as a button with field label and zoom
					if (gridPanelFields[i].isHeading())
					{
						WEditorPopupMenu popupMenu = editor.getPopupMenu();
						if (popupMenu != null && gridPanelFields[i].isHeading() && popupMenu.isZoomEnabled()
								&& (editor instanceof IZoomableEditor) && !(editor instanceof WButtonEditor))
						{
							Button button = new Button();
							button.addEventListener(Events.ON_CLICK,
									((ADTabpanel) m_windowPanel.curTabpanel).new ZoomListener(
											(IZoomableEditor) editor));
							button.setLabel(editor.getLabel().getValue());
							button.setIconSclass("z-icon-Find");
							cell.appendChild(button);
							ZKUpdateUtil.setWidth(button, "99%");
							String zclass = ((HtmlBasedComponent) cell).getZclass();
							if (zclass == null)
							{
								((HtmlBasedComponent) cell).setZclass("form-button");
							}
							else if (!zclass.contains("form-button"))
							{
								((HtmlBasedComponent) cell).setZclass("form-button " + zclass);
							}
						}
						else
						{
							org.adempiere.webui.component.Label label = new org.adempiere.webui.component.Label(
									gridPanelFields[i].getHeader());
							cell.appendChild(label);
							if (label.getDecorator() != null)
								cell.appendChild(label.getDecorator());
						}
					}
					else
					{
						cell.appendChild(editor.getComponent());
						editor.getComponent().addEventListener(Events.ON_FOCUS, gridPanel);
						
						if (editor instanceof WPAttributeEditor)
						{
							((WPAttributeEditor) editor).getComponent().getButton()
									.addEventListener(Events.ON_FOCUS, gridPanel);
						}
						else if (editor instanceof WSearchEditor)
						{
							((WSearchEditor) editor).getComponent().getButton()
									.addEventListener(Events.ON_FOCUS, gridPanel);
						}
						
						WEditorPopupMenu popupMenu = editor.getPopupMenu();

						if (editor instanceof WNumberEditor)
						{
//							((Table) ((WNumberEditor) editor).getComponent().getFirstChild()).setStyle("float:right;");
						}

						if (popupMenu != null)
						{
							popupMenu.addMenuListener((ContextMenuListener) editor);
							cell.appendChild(popupMenu);
						}
						if (!gridPanelFields[i].isDisplayed(true))
						{
							editor.setVisible(false);
						}
						editor.setReadWrite(gridPanelFields[i].isEditable(true));
					}
				}
				else
				{
					Component component = getDisplayComponent(rowIndex, currentValues[i], gridPanelFields[i]);
					cell.appendChild(component);
					// add hidden input component to help focusing to row
					if (!gridPanelFields[i].isHeading())
						cell.appendChild(createAnchorInput());

					if (DisplayType.YesNo == gridPanelFields[i].getDisplayType()
							|| DisplayType.Image == gridPanelFields[i].getDisplayType())
					{
						if (!cellStyle.endsWith(";")) {
							cellStyle += ";";
						}
						cellStyle += "text-align:center; ";
					}
					else if (DisplayType.isNumeric(gridPanelFields[i].getDisplayType()))
					{
						if (!cellStyle.endsWith(";")) {
							cellStyle += ";";
						}
						cellStyle += "text-align:right; ";
					}
				}
			}
			editorsListMap.put(row, editorsList);
			cell.setStyle(cellStyle);
			ZKUpdateUtil.setWidth(cell, "100%");
			cell.setAttribute("columnName", gridPanelFields[i].getColumnName());
			cell.addEventListener(Events.ON_CLICK, rowListener);
			if(!gridTab.isIncluded())
				cell.addEventListener(Events.ON_DOUBLE_CLICK, rowListener);
			else
			{
				editing = true;
				GridTableListModel model = (GridTableListModel) grid.getModel();
				model.setEditing(true);
			}

			row.appendChild(cell);
		}

		row.setSclass(ROW_STYLE);
		
		if (colIndex == -1)
		{
			org.zkoss.zul.Label label = new org.zkoss.zul.Label("No fields are marked as 'Displayed' or 'Displayed in Grid'.");
			row.appendChild(label);
			return;
		}
		
		if (rowIndex == gridTab.getCurrentRow() && gridTab.isIncluded())
		{
			setCurrentCell(rowIndex, 0, CODE_REFRESH);
			updateNavigationButton(0, gridTab.getRowCount());
			Events.echoEvent(GridPanel.EVENT_ON_SET_FOCUS_TO_FIRST_CELL, gridPanel, null);
		}
		else if (rowIndex == gridTab.getCurrentRow())
		{
			setCurrentRow(row);
		}
		row.addEventListener(Events.ON_CLICK, rowListener);
		row.addEventListener(Events.ON_OK, rowListener);
	}
	
	public void updateNavigationButton(int row, int totalRow)
	{
		if (totalRow == 1 || totalRow == 0 )
		{
			m_windowPanel.getToolbar().enableNavigation(false);
		}
		else if (row == 0)
		{
			m_windowPanel.getToolbar().enableFirstNavigation(false);
			m_windowPanel.getToolbar().enableLastNavigation(true);
		}
		else if (row == totalRow - 1)
		{
			m_windowPanel.getToolbar().enableFirstNavigation(true);
			m_windowPanel.getToolbar().enableLastNavigation(false);
		}
		else
		{
			m_windowPanel.getToolbar().enableNavigation(true);
		}

	}

	private Cell currentDiv	= null;
	
	public Cell getCurrentDiv()
	{
		return currentDiv;
	}

	public void addRemovePropertyChangeListener(boolean isAddListener, int col)
	{
		ArrayList<WEditor> editorsList = editorsListMap.get(getCurrentRow());
		if (editorsList != null)
		{
			if (isAddListener)
			{
				addEditorPropertyChangeListener(editorsList);
				gridPanel.dynamicDisplay(col);
			}
			else
			{
				removeEditorPropertyChangeListener(editorsList);
			}
		}
	} // addRemovePropertyChangeListener

	public void setCurrentCell(int row, int col, int code)
	{
		if (col < 0 || row < 0)
			return;
		while (!isEditable(row, col))
		{
			if (!(code == KeyEvent.RIGHT || code == KeyEvent.LEFT || code == CODE_REFRESH))
				break;
			else if (code == KeyEvent.RIGHT || code == CODE_REFRESH)
				++col;
			else if (code == KeyEvent.LEFT)
				--col;

			if (col < 0)
			{
				setFocusCell();
				return;
			}
		}

		
		if (code == KeyEvent.DOWN || code == KeyEvent.UP || code == CODE_CLICK || code == CODE_REFRESH)
		{
			// Remove current row property change listener
			ArrayList<WEditor> editorsList = editorsListMap.get(getCurrentRow());
			if (isAddRemoveListener(code))
			{
				// Remove current row property change listener
				addRemovePropertyChangeListener(false, col);
			}

		}
		
		if (row != currentRowIndex || code == CODE_REFRESH)
		{
			if (currentRow != null)
				setRowStyleClass(currentRow, ROW_STYLE);
			if (grid.getRows().getChildren().size() <= 0)
			{
				currentDiv = null;
				return;
			}
			currentRow = ((Row) grid.getRows().getChildren().get(row));
			currentRowIndex = row;
			gridTab.setCurrentRow(row + (paging.getPageSize() * paging.getActivePage()), true); // update index in tabular format and pagination
			setRowStyleClass(currentRow, CURRENT_ROW_STYLE);
		}

		setFocusToEditor();

		if (code == KeyEvent.DOWN || code == KeyEvent.UP || code == CODE_CLICK || code == CODE_REFRESH)
		{
			// Add property change listener to new current row
			ArrayList<WEditor> editorsList = editorsListMap.get(getCurrentRow());
			if (isAddRemoveListener(code))
			{
				// Remove current row property change listener
				addRemovePropertyChangeListener(true, col);
			}
		}

		if (grid.getCell(row, col) instanceof Cell)
			currentDiv = (Cell) grid.getCell(row, col);

		if (currentDiv != null && code != CODE_CLICK)
		{
			setFocusCell();
		}
		else if (currentDiv == null)
		{
			// To resolve the Stack Overflow Error when Current Div is NULL
//			s_log.log(Level.SEVERE, "Current Div is NULL for Row = " + row + " KeyCode = " + code + " in GridTab = " + gridTab);
			// setCurrentCell(row, 0, code);
		}
	}

	public Boolean isAddRemoveListener(int code)
	{
		if (code == KeyEvent.DOWN || code == KeyEvent.UP || code == CODE_CLICK
				|| code == CODE_REFRESH || code == KeyEvent.HOME)
			return true;
		return false;
	} // isAddRemoveListener

	private boolean isEditable(int row,int col)
	{
		Cell div = null;
		if (grid.getCell(row, col) instanceof Cell)
			div = (Cell) grid.getCell(row, col);
		else
			return true;
		
		if(div == null)
			return true;
		if (div.getChildren().size() <= 0)
			return false;
		
		if (div.getChildren().get(0) instanceof NumberBox && ( !((NumberBox) div.getChildren().get(0)).getDecimalbox().isDisabled() && !((NumberBox) div.getChildren().get(0)).getDecimalbox().isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Checkbox && ((Checkbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Combobox && ((Combobox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Textbox && (!((Textbox) div.getChildren().get(0)).isDisabled() && !((Textbox) div.getChildren().get(0)).isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Datebox && ((Datebox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof DatetimeBox && ((DatetimeBox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Locationbox && ((Locationbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Searchbox && (!((Searchbox) div.getChildren().get(0)).getTextbox().isDisabled() && !((Searchbox) div.getChildren().get(0)).getTextbox().isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Button && ((Button) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Combinationbox && ((Combinationbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof EditorBox && ((EditorBox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof EditTextBox && (!((EditTextBox) div.getChildren().get(0)).isDisabled() && !((EditTextBox) div.getChildren().get(0)).isReadonly()))
			return true;
		else
			return false;
	}
	
	public void setFocusCell()
	{
		if (currentDiv == null || currentDiv.getChildren().size() <= 0)
			return;
		
		if (currentDiv.getChildren().get(0) instanceof NumberBox)
			((NumberBox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Checkbox)
			((Checkbox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Combobox)
			((Combobox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Textbox)
		{
			((Textbox) currentDiv.getChildren().get(0)).focus();
			((Textbox) currentDiv.getChildren().get(0)).select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Datebox)
			((Datebox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof DatetimeBox)
			((DatetimeBox) currentDiv.getChildren().get(0)).getDatebox().focus();
		else if (currentDiv.getChildren().get(0) instanceof Locationbox)
			((Locationbox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Combinationbox)
		{
			((Combinationbox) currentDiv.getChildren().get(0)).getTextbox().focus();
			((Combinationbox) currentDiv.getChildren().get(0)).getTextbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Searchbox)
		{
			((Searchbox) currentDiv.getChildren().get(0)).getTextbox().focus();
			((Searchbox) currentDiv.getChildren().get(0)).getTextbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Button)
			((Button) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof EditorBox)
			((EditorBox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof EditTextBox)
			((EditTextBox) currentDiv.getChildren().get(0)).focus();
	}

	/**
	 * @param component
	 * @return
	 */
	private Input createAnchorInput() {
		Input input = new Input();
		input.setDynamicProperty("type", "text");
		input.setValue("");
		input.setDynamicProperty("readonly", "readonly");
		input.setStyle("border: none; display: none; width: 3px;");
		return input;
	}

	/**
	 * @param row
	 */
	public void setCurrentRow(Row row) {
		if (currentRow != null && currentRow.getParent() != null && currentRow != row) {
			setRowStyleClass(currentRow, ROW_STYLE);
		}
		currentRow = row;
		setRowStyleClass(currentRow, CURRENT_ROW_STYLE);
//		setFocusToEditor();

		currentRowIndex = gridTab.getCurrentRow();
//		if (currentRowIndex == gridTab.getCurrentRow()) {
			if (editing) {
				stopEditing(false);
				editCurrentRow();
			}
//		} else {
//			currentRowIndex = gridTab.getCurrentRow();
//			if (editing) {
//				stopEditing(false);
//			}
//		}
			}

	/**
	 * @return Row
	 */
	public Row getCurrentRow() {
		return currentRow;
	}

	/**
	 * @return current row index ( absolute )
	 */
	public int getCurrentRowIndex() {
		return currentRowIndex;
	}

	/**
	 * Enter edit mode
	 */
	public void editCurrentRow() {
		if (currentRow != null && currentRow.getParent() != null && currentRow.isVisible()
			&& grid != null && grid.isVisible() && grid.getParent() != null && grid.getParent().isVisible()) {
			GridField[] gridPanelFields = gridPanel.getFields();
			int columnCount = gridPanelFields.length;
			org.zkoss.zul.Columns columns = grid.getColumns();
			//skip selection column
			int colIndex = 0;
			if(!gridTab.isIncluded()) {
				for (int i = 0; i < columnCount; i++) {
					if ((!isGridViewCustomized && !gridPanelFields[i].isDisplayedGrid()) || gridPanelFields[i].isToolbarButton()) {
						continue;
					}
					colIndex ++;
					
					if (editors.get(gridPanelFields[i]) == null)
						editors.put(gridPanelFields[i], WebEditorFactory.getEditor(gridPanelFields[i], true));
					org.zkoss.zul.Column column = (org.zkoss.zul.Column) columns.getChildren().get(colIndex);
					if (column.isVisible()) {
						Cell div = (Cell) currentRow.getChildren().get(colIndex);
						WEditor editor = getEditorCell(gridPanelFields[i], currentValues[i], i);
						if (!(editor instanceof WButtonEditor))
							editor.addValueChangeListener(dataBinder);
						gridPanelFields[i].addPropertyChangeListener(editor);
						if (gridPanelFields[i].isHeading())
						{
							div.removeChild(div.getFirstChild());
							WEditorPopupMenu popupMenu = editor.getPopupMenu();
							if (popupMenu != null && gridPanelFields[i].isHeading() && popupMenu.isZoomEnabled()
									&& (editor instanceof IZoomableEditor) && !(editor instanceof WButtonEditor))
							{
								Button button = new Button();
								button.addEventListener(Events.ON_CLICK,
										((ADTabpanel) m_windowPanel.curTabpanel).new ZoomListener(
												(IZoomableEditor) editor));
								button.setLabel(editor.getLabel().getValue());
								button.setIconSclass("z-icon-Find");
								div.appendChild(button);
								ZKUpdateUtil.setWidth(button, "99%");
								String zclass = ((HtmlBasedComponent) div).getZclass();
								if (zclass == null)
								{
									((HtmlBasedComponent) div).setZclass("form-button");
								}
								else if (!zclass.contains("form-button"))
								{
									((HtmlBasedComponent) div).setZclass("form-button " + zclass);
								}
							}
							else
							{
								org.adempiere.webui.component.Label label = new org.adempiere.webui.component.Label(
										gridPanelFields[i].getHeader());
								div.appendChild(label);
								if (label.getDecorator() != null)
									div.appendChild(label.getDecorator());
							}
						}
						else
						{
							div.appendChild(editor.getComponent());
							WEditorPopupMenu popupMenu = editor.getPopupMenu();

							if (popupMenu != null)
							{
								popupMenu.addMenuListener((ContextMenuListener) editor);
								div.appendChild(popupMenu);
							}
							div.getFirstChild().setVisible(false);
							// check context
							if (!gridPanelFields[i].isDisplayed(true))
							{
								editor.setVisible(false);
							}
							editor.setReadWrite(gridPanelFields[i].isEditable(true));
						}
					}
				}
			}
			editing = true;

			GridTableListModel model = (GridTableListModel) grid.getModel();
			model.setEditing(true);

		}
	}

	/**
	 * @see RowRendererExt#getControls()
	 */
	public int getControls() {
		return DETACH_ON_RENDER;
	}

	/**
	 * @see RowRendererExt#newCell(Row)
	 */
	public Component newCell(Row row) {
		return null;
	}

	/**
	 * @see RowRendererExt#newRow(Grid)
	 */
	public Row newRow(Grid grid) {
		return null;
	}

	/**
	 * @see RendererCtrl#doCatch(Throwable)
	 */
	public void doCatch(Throwable ex) throws Throwable {
		lookupCache = null;
	}

	/**
	 * @see RendererCtrl#doFinally()
	 */
	public void doFinally() {
		lookupCache = null;
	}

	/**
	 * @see RendererCtrl#doTry()
	 */
	public void doTry() {
		lookupCache = new HashMap<String, Map<Object,String>>();
	}

	/**
	 * set focus to first active editor
	 */
	public void setFocusToEditor() {
		if (currentRow != null && currentRow.getParent() != null) {
			WEditor toFocus = null;
			WEditor firstEditor = null;
			for (WEditor editor : getEditors()) {
				if (editor.isHasFocus() && editor.isVisible() && editor.getComponent().getParent() != null) {
					toFocus = editor;
					break;
				}

				if (editor.isVisible() && editor.getComponent().getParent() != null) {
					if (toFocus == null && editor.isReadWrite()) {
						toFocus = editor;
					}
					if (firstEditor == null)
						firstEditor = editor;
				}
			}
			if (toFocus != null) {
				Component c = toFocus.getComponent();
				if (c instanceof EditorBox) {
					c = ((EditorBox)c).getTextbox();
				}
				Clients.response(new AuFocus(c));
			} else if (firstEditor != null) {
				Component c = firstEditor.getComponent();
				if (c instanceof EditorBox) {
					c = ((EditorBox)c).getTextbox();
				}
				Clients.response(new AuFocus(c));
			}
		}
	}

	/**
	 *
	 * @param gridPanel
	 */
	public void setGridPanel(GridPanel gridPanel) {
		this.gridPanel = gridPanel;
	}

	class RowListener implements EventListener<Event> {

		private Grid _grid;

		public RowListener(Grid grid) {
			_grid = grid;
		}

		public void onEvent(Event event) throws Exception {
			if (Events.ON_CLICK.equals(event.getName())) {
				if (Executions.getCurrent().getAttribute("gridView.onSelectRow") != null)
					return;
				Event evt = new Event(Events.ON_CLICK, _grid, event.getTarget());
				Events.sendEvent(_grid, evt);
				evt.stopPropagation();
			}
			else if (Events.ON_DOUBLE_CLICK.equals(event.getName())) {
				Event evt = new Event(Events.ON_DOUBLE_CLICK, _grid, _grid);
				Events.sendEvent(_grid, evt);
			}
			else if (Events.ON_OK.equals(event.getName())) {
				Event evt = new Event(Events.ON_OK, _grid, _grid);
				Events.sendEvent(_grid, evt);
			}
		}
	}

	/**
	 * @return boolean
	 */
	public boolean isEditing() {
		return editing;
	}

	/**
	 * @param windowPanel
	 */
	public void setADWindowPanel(AbstractADWindowPanel windowPanel) {
		if (this.m_windowPanel == windowPanel)
			return;
		
		this.m_windowPanel = windowPanel;
		
		buttonListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				WButtonEditor editor = (WButtonEditor) event.getSource();
				Integer rowIndex = (Integer) editor.getComponent().getAttribute("grid.row.index");
				if (rowIndex != null) {
					int newRowIndex = gridTab.navigate(rowIndex);
					if (newRowIndex == rowIndex) {
						m_windowPanel.actionPerformed(event);
					}
				} else {
					m_windowPanel.actionPerformed(event);
				}
			}
		};
	}
	
	/**
	 * Set Property change listener of editor field
	 * 
	 * @param editorsList
	 */
	private void addEditorPropertyChangeListener(ArrayList<WEditor> editorsList)
	{
		GridField[] fields = gridPanel.getFields();
		for (int i = 0; i < editorsList.size(); i++)
		{
			fields[i].removePropertyChangeListener(editorsList.get(i));
			fields[i].addPropertyChangeListener(editorsList.get(i));
			// listener already added in 'getEditorCell' method.
			if (editorsList.get(i) instanceof WButtonEditor)
				continue;
			editorsList.get(i).addValueChangeListener(dataBinder);
		}
	}
	
	/**
	 * Remove Property change listener of editor field
	 * 
	 * @param editorsList
	 */
	private void removeEditorPropertyChangeListener(ArrayList<WEditor> editorsList)
	{
		GridField[] fields = gridPanel.getFields();
		for (int i = 0; i < editorsList.size(); i++)
		{
			fields[i].removePropertyChangeListener(editorsList.get(i));
			if (editorsList.get(i) instanceof WButtonEditor)
				continue;
			editorsList.get(i).removeValuechangeListener(dataBinder);
		}
	}
	
	private void setRowStyleClass(Row row, String sclass) {
		if (row.getSclass()!=null && row.getSclass().contains(PROCESSED_ROW_STYLE)) {
			row.setSclass(sclass + " " + PROCESSED_ROW_STYLE);
		} else {
			row.setSclass(sclass);
		}
	}

	public void setRowTo(int row)
	{
		try
		{
			int pgIndex = row >= 0 ? row % paging.getPageSize() : 0;
			if (pgIndex >= 0 && pgIndex < grid.getRows().getChildren().size())
			{
				currentRow = (Row) grid.getRows().getChildren().get(pgIndex);
				currentRow.setStyle(GridTabRowRenderer.CURRENT_ROW_STYLE);
				setCurrentRow(currentRow);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getTarget() instanceof Checkbox) {
			Executions.getCurrent().setAttribute("gridView.onSelectRow", Boolean.TRUE);
			Checkbox checkBox = (Checkbox) event.getTarget();
			Events.sendEvent(gridPanel, new Event("onSelectRow", gridPanel, checkBox));
		}
	}
}
