package org.adempiere.webui.component;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.WTextEditorDialog;
import org.compiere.model.MTreeFavorite;
import org.compiere.model.MTreeFavoriteNode;
import org.compiere.model.MTreeNode;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.zkoss.lang.Objects;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Tree;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.event.TreeDataEvent;

public class SimpleFavoriteTreeModel extends DefaultTreeModel<Object> implements EventListener<Event>, TreeitemRenderer<Object>
{

	private static final long			serialVersionUID	= -2948153996477803421L;
	private static final CLogger		logger				= CLogger.getCLogger(SimpleFavoriteTreeModel.class);
	private boolean						itemDraggable;
	private int							currFolderID		= 0;
	private List<EventListener<Event>>	onDropListners		= new ArrayList<EventListener<Event>>();

	public SimpleFavoriteTreeModel(DefaultTreeNode<Object> root)
	{
		super(root);
	}

	/**
	 * Initialization of Tree.
	 * 
	 * @param tree
	 * @param AD_Tree_Favorite_ID
	 * @param windowNo
	 * @return
	 */
	public static SimpleFavoriteTreeModel initADTree(Tree tree, int AD_Tree_Favorite_ID, int windowNo)
	{
		return initADTree(tree, AD_Tree_Favorite_ID, windowNo, true, null);
	}

	/**
	 * Initialization of Tree.
	 * 
	 * @param tree
	 * @param AD_Tree_Favorite_ID
	 * @param windowNo
	 * @param editable
	 * @param trxName
	 * @return
	 */
	private static SimpleFavoriteTreeModel initADTree(Tree tree, int AD_Tree_Favorite_ID, int windowNo, boolean editable, String trxName)
	{
		MTreeFavorite mTreeFavorite = new MTreeFavorite(Env.getCtx(), AD_Tree_Favorite_ID, trxName);
		MTreeNode root = mTreeFavorite.getRoot();
		SimpleFavoriteTreeModel treeModel = SimpleFavoriteTreeModel.createFrom(root);
		treeModel.currFolderID = root.getNode_ID();

		treeModel.setItemDraggable(true);
		treeModel.addOnDropEventListener(new ADTreeFavoriteOnDropListener(tree, treeModel, mTreeFavorite, windowNo));

		tree.setPageSize(-1);
		try
		{
			tree.setItemRenderer(treeModel);
			tree.setModel(treeModel);
			//TODO : Might be need to code here for default expand collapse
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, "Failed to setup tree");
		}

		return treeModel;
	}

	/**
	 * Logic for creating Tree hierarchy
	 * 
	 * @param root
	 * @return
	 */
	public static SimpleFavoriteTreeModel createFrom(MTreeNode root)
	{
		SimpleFavoriteTreeModel model = null;
		Enumeration<?> nodeEnum = root.children();

		DefaultTreeNode<Object> stRoot = new DefaultTreeNode<Object>(root, new ArrayList<TreeNode<Object>>());
		while (nodeEnum.hasMoreElements())
		{
			MTreeNode childNode = (MTreeNode) nodeEnum.nextElement();
			DefaultTreeNode<Object> stNode = childNode.getChildCount() > 0 ? new DefaultTreeNode<Object>(childNode, new ArrayList<TreeNode<Object>>())
			                                                               : new DefaultTreeNode<Object>(childNode);
			stRoot.getChildren().add(stNode);
			if (childNode.getChildCount() > 0)
			{
				populate(stNode, childNode);
			}
		}
		model = new SimpleFavoriteTreeModel(stRoot);
		return model;
	}

	/**
	 * Populate Node
	 * 
	 * @param stNode
	 * @param root
	 */
	private static void populate(DefaultTreeNode<Object> stNode, MTreeNode root)
	{
		Enumeration<?> nodeEnum = root.children();
		while (nodeEnum.hasMoreElements())
		{
			MTreeNode childNode = (MTreeNode) nodeEnum.nextElement();
			DefaultTreeNode<Object> stChildNode = childNode.getChildCount() > 0 ? new DefaultTreeNode<Object>(childNode, new ArrayList<TreeNode<Object>>())
			                                                                    : new DefaultTreeNode<Object>(childNode);
			stNode.getChildren().add(stChildNode);
			if (childNode.getChildCount() > 0)
			{
				populate(stChildNode, childNode);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.zkoss.zul.TreeitemRenderer#render(org.zkoss.zul.Treeitem, java.lang.Object)
	 */
	@Override
	public void render(Treeitem ti, Object node, int index) throws Exception
	{
		DefaultTreeNode<?> stn = (DefaultTreeNode<?>) node;
		MTreeNode mtn = (MTreeNode) stn.getData();
		Treecell tc;
		ZKUpdateUtil.setHflex(ti, "true");
		if (!mtn.isSummary()) {
			tc = new Treecell(Objects.toString(node));
			tc.setIconSclass(getIconFile(mtn));
		}
		else {
			tc = new Treecell(Objects.toString(node));
			tc.setIconSclass("z-icon-Folder");
		}
		tc.setStyle("width : 100%");
		Treerow tr = null;
		if (ti.getTreerow() == null)
		{
			tr = new Treerow();
			ZKUpdateUtil.setWidth(tr, "100%");
			tr.setParent(ti);
			if (isItemDraggable())
			{
				tr.setDraggable("true");
			}
			if (!onDropListners.isEmpty())
			{
				ti.getTreerow().addEventListener(Events.ON_CLICK, this);
				ti.getTreerow().addEventListener(Events.ON_DOUBLE_CLICK, this);
				tr.setDroppable("true");
				tr.addEventListener(Events.ON_SELECT, this);
				tr.addEventListener(Events.ON_RIGHT_CLICK, this);
				tr.addEventListener(Events.ON_DROP, this);
			}

			Object data = ((DefaultTreeNode<?>) node).getData();
			if (data instanceof MTreeNode)
			{
				MTreeNode mNode = (MTreeNode) data;
				if (mNode.getColor() != null)
				{
					String hex = ZkCssHelper.createHexColorString(mNode.getColor());
					ZkCssHelper.appendStyle(tc, "color: #" + hex);
				}
				// default user action for Collapse/Expand
				ti.setOpen(!mNode.IsCollapsible());
				ti.setTooltiptext(mNode.getDescription());
				if (mNode.isSummary())
					ZkCssHelper.appendStyle(tc, "font-weight: bold");
			}
		}
		else
		{
			tr = ti.getTreerow();
			tr.getChildren().clear();
		}
		tc.setParent(tr);
		ti.setValue(node);
		ti.setTooltiptext(ti.getLabel());
	}

	/**
	 * Get Image icon for Menu
	 * 
	 * @param mt
	 * @return
	 */
	private String getIconFile(MTreeNode mt) {
		if (mt.isWindow())
			return "z-icon-Window";
		if (mt.isReport())
			return "z-icon-Report";
		if (mt.isProcess())
			return "z-icon-Process";
		if (mt.isInfo())
			return "z-icon-Info";
		return "z-icon-Window";
	}	

	/**
	 * Get value of Current Selected Folder in Tree.
	 * 
	 * @return
	 */
	public int getSelectedFolderID()
	{
		return currFolderID;
	}

	/**
	 * Set the current selected Menu folder in Tree.
	 * 
	 * @param mtnID
	 */
	public void setSelectedFolderID(int mtnID)
	{
		currFolderID = mtnID;
	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		Component comp = event.getTarget();
		String eventName = event.getName();

		if (Events.ON_DROP.equals(eventName) || Events.ON_RIGHT_CLICK.equals(eventName))
		{
			for (EventListener<Event> listener : onDropListners)
			{
				listener.onEvent(event);
			}
		}

		/**
		 * On click of menu to open that window
		 */
		if (Events.ON_CLICK.equals(eventName) || Events.ON_SELECT.equals(eventName))
		{
			if (comp instanceof Treerow)
			{
				Treerow treerow = (Treerow) comp;
				Treeitem treeitem = (Treeitem) treerow.getParent();

				DefaultTreeNode<Object> DefaultTreeNode = treeitem.getValue();
				MTreeNode mtn = (MTreeNode) DefaultTreeNode.getData();
				if (!mtn.isSummary())
				{
					int menuId = mtn.getMenu_ID();
					SessionManager.getAppDesktop().onMenuSelected(menuId);
					setSelectedFolderID(mtn.getParent_ID());
				}
				else
				{
					setSelectedFolderID(mtn.getNode_ID());
				}
			}
		}
		else if (Events.ON_DOUBLE_CLICK.equals(eventName))
		{
			if (comp instanceof Treerow)
			{
				Treerow treerow = (Treerow) comp;
				Treeitem treeitem = (Treeitem) treerow.getParent();

				DefaultTreeNode<Object> DefaultTreeNode = treeitem.getValue();
				MTreeNode mtn = (MTreeNode) DefaultTreeNode.getData();
				if (mtn.isSummary())
				{
					final WTextEditorDialog dialog = new WTextEditorDialog("Edit folder text", mtn.getName() == null ? ""
					                                                                                           : mtn.getName(),
					                                                 true, 100, false);

					dialog.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
					dialog.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							if (!dialog.isCancelled())
							{
								mtn.setName(dialog.getText());
								MTreeFavoriteNode treeFavNode = new MTreeFavoriteNode(Env.getCtx(), mtn.getNode_ID(), null);
								treeFavNode.setNodeName(dialog.getText());
								treeFavNode.saveEx();

								int path[] = SimpleFavoriteTreeModel.this.getPath(DefaultTreeNode);
								if (path != null && path.length > 0)
								{
									DefaultTreeNode<Object> parentNode = getRoot();
									int index = path.length - 1;
									for (int i = 0; i < index; i++)
									{
										parentNode = (DefaultTreeNode<Object>) getChild(parentNode, path[i]);
									}
									fireEvent(TreeDataEvent.CONTENTS_CHANGED, getPath(parentNode), path[index], path[index]);
								}
							}
						}
						
					});
					SessionManager.getAppDesktop().showWindow(dialog);
				}
			}
		}
	}

	public void removeNode(DefaultTreeNode<Object> treeNode)
	{
		int path[] = this.getPath(treeNode);

		if (path != null && path.length > 0)
		{
			DefaultTreeNode<Object> parentNode = getRoot();
			int index = path.length - 1;
			for (int i = 0; i < index; i++)
			{
				parentNode = (DefaultTreeNode<Object>) getChild(parentNode, path[i]);
			}
			parentNode.getChildren().remove(path[index]);
		}
	}

	public void addNode(DefaultTreeNode<Object> newNode)
	{
		DefaultTreeNode<Object> root = getRoot();
		root.getChildren().add(newNode);
	}

	/**
	 * @param newParent
	 * @param newNode
	 * @param index
	 * @return parent node. this could be a new instance created to replace the newParent node param
	 */
	public DefaultTreeNode<Object> addNode(DefaultTreeNode<Object> newParent, DefaultTreeNode<Object> newNode, int index)
	{
		DefaultTreeNode<Object> parent = newParent;
		if (newParent.getChildren() == null)
		{
			parent = new DefaultTreeNode<Object>(newParent.getData(), new ArrayList<TreeNode<Object>>());
			newParent.getParent().insert(parent, newParent.getParent().getIndex(newParent));
			removeNode(newParent);
		}

		parent.getChildren().add(index, newNode);
		return parent;
	}

	public void addOnDropEventListener(EventListener<Event> listener)
	{
		onDropListners.add(listener);
	}

	public void setItemDraggable(boolean isDraggable)
	{
		itemDraggable = isDraggable;
	}

	public boolean isItemDraggable()
	{
		return itemDraggable;
	}

	public DefaultTreeNode<Object> getRoot()
	{
		return (DefaultTreeNode<Object>) super.getRoot();
	}

	public DefaultTreeNode<Object> getParent(DefaultTreeNode<Object> treeNode)
	{
		int path[] = this.getPath(treeNode);

		if (path != null && path.length > 0)
		{
			DefaultTreeNode<Object> parentNode = getRoot();
			int index = path.length - 1;
			for (int i = 0; i < index; i++)
			{
				parentNode = (DefaultTreeNode<Object>) getChild(parentNode, path[i]);
			}
			return parentNode;
		}
		return null;
	}

	public DefaultTreeNode<Object> find(DefaultTreeNode<Object> fromNode, int recordId)
	{
		if (fromNode == null)
			fromNode = getRoot();
		MTreeNode data = (MTreeNode) fromNode.getData();
		if (data.getNode_ID() == recordId)
			return fromNode;
		if (isLeaf(fromNode))
			return null;
		int cnt = getChildCount(fromNode);
		for (int i = 0; i < cnt; i++)
		{
			DefaultTreeNode<Object> child = (DefaultTreeNode<Object>) getChild(fromNode, i);
			DefaultTreeNode<Object> treeNode = find(child, recordId);
			if (treeNode != null)
				return treeNode;
		}
		return null;
	}
}
