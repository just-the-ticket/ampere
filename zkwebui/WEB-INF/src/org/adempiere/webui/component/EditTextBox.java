package org.adempiere.webui.component;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.util.Util;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.EventHandler;

public class EditTextBox extends Panel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -6871236025715471920L;

	private Button				btn				= new Button();
	private Textbox				textBox				= new Textbox();
	private Combobox			comboBox			= new Combobox();

	private boolean				isAutoComplete		= false;

	protected Td				btnColumn;

	public EditTextBox()
	{
		this(false);
	}

	public EditTextBox(boolean isAutoComplete)
	{
		this.isAutoComplete = isAutoComplete;
		init();
	}

	private void init()
	{
		ZKUpdateUtil.setWidth(this, "100%");

		Table grid = new Table();
		grid.setStyle("border: none; padding: 0px; margin: 0px;");
		grid.setDynamicProperty("width", "100%");
		grid.setDynamicProperty("cellpadding", "0");
		grid.setDynamicProperty("cellspacing", "0");
		this.appendChild(grid);

		Tr tr = new Tr();
		tr.setStyle("width: 100%; border: none; padding: 0px; margin: 0px; white-space:nowrap; ");
		grid.appendChild(tr);

		Td td = new Td();
		td.setStyle("border: none; padding: 1px; margin: 0px;");
		td.appendChild(getTextbox());
		tr.appendChild(td);

		btnColumn = new Td();
		btnColumn.setSclass("editor-button");
		btnColumn.appendChild(btn);
		tr.appendChild(btnColumn);

		String style = ""; //AEnv.isFirefox2() ? "display: inline" : "display: inline-block";
		style = style + "border: none; padding: 0px; margin: 0px; background-color: transparent;";
		this.setStyle(style);


		btn.setIconSclass("z-icon-PickOpen");
		setButtonVisible(false);
		btn.setTabindex(100000);
		btn.setTooltip("Reset the value");
		LayoutUtils.addSclass("editor-button", btn);

		btn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception
			{
				getTextbox().setValue(null);
				getTextbox().setReadonly(!Util.isEmpty(getTextbox().getValue()));
				Events.echoEvent(Events.ON_CHANGE, getTextbox(), null);
			}
		});

		getTextbox().setStyle("display: inline; width: 100%;");
	}

	public org.zkoss.zul.Textbox getTextbox()
	{
		return isAutoComplete ? comboBox : textBox;
	}

	public void setObscureType(String obscureType)
	{
		if (getTextbox() instanceof Textbox)
		{
			((Textbox) getTextbox()).setObscureType(obscureType);
			if (!Util.isEmpty(obscureType, false))
			{
				setButtonVisible(true);
				getTextbox().setReadonly(!Util.isEmpty(getTextbox().getValue()));
			}
		}
	}

	private void setButtonVisible(boolean isVisible)
	{
		btn.setVisible(isVisible);
		btnColumn.setStyle(isVisible ? "border: none; padding: 0px; margin: 0px; display: null;" : "display: none;");
		getTextbox().setStyle(isVisible ? "display: inline; width: 100%;" : "width: 100%;");
	}

	public boolean isReadonly()
	{
		return getTextbox().isReadonly();
	}

	public void setReadonly(boolean readonly)
	{
		btn.setEnabled(!readonly);
		getTextbox().setReadonly(readonly);
		if (getTextbox() instanceof Textbox && ((Textbox) getTextbox()).isObscure())
		{
			getTextbox().setReadonly(!Util.isEmpty(getTextbox().getValue()));
		}
	}

	public void setMaxlength(int fieldLength)
	{
		getTextbox().setMaxlength(fieldLength);
	}

	public void setCols(int displayLength)
	{
		getTextbox().setCols(displayLength);
	}

	public void setMultiline(boolean isMultiline)
	{
		getTextbox().setMultiline(isMultiline);
	}

	public void setRows(int rows)
	{
		getTextbox().setRows(rows);
	}

	public int getRows()
	{
		return getTextbox().getRows();
	}

	public String getValue()
	{
		return getTextbox().getValue();
	}

	public void setValue(String value)
	{
		getTextbox().setValue(value);
		if (getTextbox() instanceof Textbox)
		{
			if (((Textbox) getTextbox()).isObscure())
			{
				getTextbox().setReadonly(!Util.isEmpty(getText(), true));
			}
		}
	}

	public void setType(String type)
	{
		getTextbox().setType(type);
	}

	public void setText(String text)
	{
		getTextbox().setText(text);
	}

	public String getText()
	{
		return getTextbox().getText();
	}

	@Override
	public boolean addEventListener(String evtnm, EventListener<?> listener)
	{
		return getTextbox().addEventListener(evtnm, listener);
	}

	@Override
	public void addEventHandler(String name, EventHandler evthd)
	{
		getTextbox().addEventHandler(name, evthd);
	}

	public boolean isDisabled()
	{
		return getTextbox().isDisabled();
	}

	public void setDisabled(boolean isDisabled)
	{
		getTextbox().setDisabled(isDisabled);
	}

	@Override
	public void setFocus(boolean focus)
	{
		getTextbox().setFocus(focus);
	}

	public void select()
	{
		getTextbox().select();
	}

	@Override
	public void focus()
	{
		getTextbox().focus();
	}

	public void setToolTipText(String tooltiptext)
	{
		getTextbox().setTooltiptext(tooltiptext);
	}

	@Override
	public void setSclass(String sclass)
	{
		getTextbox().setSclass(sclass);
	}
	
	@Override
	public boolean setVisible(boolean visible)
	{
		getTextbox().setStyle(visible ? "display: inline; width: 99%;" : "display: none;");
		return 	getTextbox().setVisible(visible);
	}
	
	@Override
	public boolean isVisible()
	{
		return getTextbox().isVisible();
	}
}
