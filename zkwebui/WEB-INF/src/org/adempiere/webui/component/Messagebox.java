/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.component;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.adempiere.util.Callback;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MQuery;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Separator;

/**
 * Messagebox : Replaces ZK's Messagebox
 *
 * @author Niraj Sohun
 * @date Jul 31, 2007
 */

public class Messagebox extends Window implements EventListener<Event>
{
	/**
	 * generated serial version ID
	 */
	private static final long				serialVersionUID	= -4957498533838144942L;
	private static final String				MESSAGE_PANEL_STYLE	= "text-align:left; overflow-y: auto; max-height: 340px; height: 100%; margin-left: 70px;";
	private String							msg					= new String("");
	private String							imgSrc				= new String("");

	private Html							lblMsg				= new Html();

	private Button							btnOk				= new Button();
	private Button							btnCancel			= new Button();
	private Button							btnYes				= new Button();
	private Button							btnNo				= new Button();
	private Button							btnAbort			= new Button();
	private Button							btnRetry			= new Button();
	private Button							btnIgnore			= new Button();
	private Button							btnZoomDocs			= new Button();

	// Table with multiple records
	private Map <Integer, List <Integer>>	mapZoomRecords		= null;

	private Image							img					= new Image();

	private int								returnValue;
	private Callback<Integer> callback;

	/** A OK button. */
	public static final int					OK					= 0x0001;

	/** A Cancel button. */
	public static final int					CANCEL				= 0x0002;

	/** A Yes button. */
	public static final int					YES					= 0x0010;

	/** A No button. */
	public static final int					NO					= 0x0020;

	/** A Abort button. */
	public static final int					ABORT				= 0x0100;

	/** A Retry button. */
	public static final int					RETRY				= 0x0200;

	/** A IGNORE button. */
	public static final int					IGNORE				= 0x0400;

	/** A symbol consisting of a question mark in a circle. */
	public static final String				QUESTION			= ThemeUtils.resolveImageURL( "Question-btn.png");

	/** A symbol consisting of an exclamation point in a triangle with a yellow background. */
	public static final String				EXCLAMATION			= ThemeUtils.resolveImageURL( "Warning-btn.png");

	/** A symbol of a lowercase letter i in a circle. */
	public static final String				INFORMATION			= ThemeUtils.resolveImageURL( "Info-btn.png");

	/** A symbol consisting of a white X in a circle with a red background. */
	public static final String				ERROR				= ThemeUtils.resolveImageURL( "Stop-btn.png");

	/** Contains no symbols. */
	public static final String				NONE				= null;

	public Messagebox()
	{
		super();
	}

	private void init()
	{
		Properties ctx = Env.getCtx();
		lblMsg.setContent(msg);
		lblMsg.setClientAttribute("style", "word-break: normal;");

		btnOk.setTooltiptext(Util.cleanAmp(Msg.getMsg(ctx, "OK")));
		btnOk.setIconSclass("z-icon-Ok");
		btnOk.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnOk);

		btnCancel.setTooltiptext(Util.cleanAmp(Msg.getMsg(ctx, "Cancel")));
		btnCancel.setIconSclass("z-icon-Cancel");
		btnCancel.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnCancel);

		btnYes.setTooltiptext(Util.cleanAmp(Msg.getMsg(ctx, "Yes")));
		btnYes.setIconSclass("z-icon-Ok");
		btnYes.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnYes);

		btnNo.setTooltiptext(Util.cleanAmp(Msg.getMsg(ctx, "No")));
		btnNo.setIconSclass("z-icon-Cancel");
		btnNo.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnNo);

		btnAbort.setLabel("Abort");
		btnAbort.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnAbort);

		btnRetry.setLabel("Retry");
		btnRetry.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnRetry);

		btnIgnore.setTooltiptext("Ignore");
		btnIgnore.setIconSclass("z-icon-Ignore");
		btnIgnore.addEventListener(Events.ON_CLICK, this);
		LayoutUtils.addSclass("action-text-button", btnIgnore);

		btnZoomDocs.setTooltiptext("Open Documents");
		btnZoomDocs.setIconSclass("z-icon-ZoomAcross");
		btnZoomDocs.addEventListener(Events.ON_CLICK, this);
		ZKUpdateUtil.setHeight(btnZoomDocs, "30px");
		LayoutUtils.addSclass("action-text-button", btnZoomDocs);

		Div pnlMessage = new Div();
		pnlMessage.setStyle(MESSAGE_PANEL_STYLE);
		pnlMessage.appendChild(lblMsg);

		Div pnlImage = new Div();
		img.setSrc(imgSrc);
//		img.setStyle("position: absolute; top: calc(50% - 22px);");

		pnlImage.setStyle("float:left;");
		pnlImage.setAlign("center");
		pnlImage.appendChild(img);
		ZKUpdateUtil.setHeight(pnlImage, "100%");
		
		Div mainDiv = new Div();
		ZKUpdateUtil.setHeight(mainDiv, "100%");
		ZKUpdateUtil.setWidth(mainDiv, "100%");
		
		Panel mainPanel = new Panel();
		mainPanel.appendChild(mainDiv);
		ZKUpdateUtil.setHeight(mainPanel, "100%");
		ZKUpdateUtil.setWidth(mainPanel, "100%");
		this.appendChild(mainPanel);

 		Div north = new Div();
		north.setAlign("center");
		north.setStyle("margin: 20px 0px 60px 0px; height: calc(100% - 80px); position: relative; top: 0px; min-height: 85px; min-width: 500px"); //trbl
		north.appendChild(pnlImage);
		north.appendChild(pnlMessage);
		mainDiv.appendChild(north);

		Hbox toolbar = new Hbox();
		
		Div lBtn = new Div();
		Div cBtn = new Div();
//		lBtn.setAlign("left");
		lBtn.setSclass("confirm-panel-left");
		cBtn.setSclass("confirm-panel-center");
//		lBtn.setWidth("500px");
		Hbox pnlButtonsStart = new Hbox();
		ZKUpdateUtil.setHeight(pnlButtonsStart, "52px");
		ZKUpdateUtil.setWidth(pnlButtonsStart, "100%");
		pnlButtonsStart.setAlign("left");
		pnlButtonsStart.setPack("end");
		pnlButtonsStart.appendChild(btnZoomDocs);
		btnZoomDocs.setVisible(mapZoomRecords != null);
		lBtn.appendChild(pnlButtonsStart);
		
		Div rBtn = new Div();
//		rBtn.setAlign("right");
		Hbox pnlButtons = new Hbox();
		rBtn.setSclass("confirm-panel-right");
		ZKUpdateUtil.setHeight(pnlButtons, "52px");
		pnlButtons.setAlign("right");
		pnlButtons.setPack("end");
		pnlButtons.appendChild(btnOk);
		pnlButtons.appendChild(btnCancel);
		pnlButtons.appendChild(btnYes);
		pnlButtons.appendChild(btnNo);
		pnlButtons.appendChild(btnAbort);
		pnlButtons.appendChild(btnRetry);
		pnlButtons.appendChild(btnIgnore);
		rBtn.appendChild(pnlButtons);

		Separator separator = new Separator();
		ZKUpdateUtil.setWidth(separator, "96%");
		separator.setBar(true);
		separator.setStyle("position: absolute; bottom: 48px;");
		mainDiv.appendChild(separator);

//		rBtn.setStyle("float : right");
		toolbar.appendChild(lBtn);
		toolbar.appendChild(cBtn);
		toolbar.appendChild(rBtn);
		toolbar.setSclass("confirm-panel");
		
		Div south = new Div();
		ZKUpdateUtil.setWidth(south, "700px");
		ZKUpdateUtil.setWidth(toolbar, "100%");
		south.appendChild(toolbar);
		south.setStyle("height: 55px; position: absolute; bottom: 0;");
		mainDiv.appendChild(south);
		
		this.setBorder("normal");
		this.setContentStyle("background-color:#ffffff; min-height:165px; min-width:285px; max-height:415px; max-width:735px;");
		this.setPosition("left, top");
		/*
		 * If this min/max width/height update then, 
		 * this.setContentStyle [min-height-35, min-width-15 ,max-height-35, max-width-15],
		 * north.setStyle [min-height-115, min-width],
		 * MESSAGE_PANEL_STYLE [max-height-100].
		 */
		this.setStyle("min-height:250px; min-width:725px; max-height:450px; max-width:725px;");
//		ZKUpdateUtil.setWidth(this, "725px");
		
	}
	
	public int show(String message, String title, int buttons, String icon, Callback<Integer> callback, Map <Integer, List <Integer>> mapZoomRecords)
	{
		return show(message, title, buttons, icon, callback, false, mapZoomRecords);
	}

	public int show(String message, String title, int buttons, String icon, Callback<Integer> callback, boolean modal, Map <Integer, List <Integer>> mapZoomRecords)
	{
		this.msg = message;
		this.imgSrc = icon;
		this.callback = callback;
		this.mapZoomRecords = mapZoomRecords;

		btnOk.setVisible(false);
		btnCancel.setVisible(false);
		btnYes.setVisible(false);
		btnNo.setVisible(false);
		btnRetry.setVisible(false);
		btnAbort.setVisible(false);
		btnIgnore.setVisible(false);

		if ((buttons & OK) != 0)
			btnOk.setVisible(true);

		if ((buttons & CANCEL) != 0)
			btnCancel.setVisible(true);

		if ((buttons & YES) != 0)
			btnYes.setVisible(true);

		if ((buttons & NO) != 0)
			btnNo.setVisible(true);

		if ((buttons & RETRY) != 0)
			btnRetry.setVisible(true);

		if ((buttons & ABORT) != 0)
			btnAbort.setVisible(true);

		if ((buttons & IGNORE) != 0)
			btnIgnore.setVisible(true);

		init();

		this.setTitle(title);
		this.setPosition("center");
		this.setClosable(true);
		this.setAttribute(Window.MODE_KEY, modal ? Window.MODE_MODAL : Window.MODE_HIGHLIGHTED);
		this.setSizable(true);

		this.setVisible(true);
		AEnv.showCenterScreen(this);

		return returnValue;
	}

	public static int showDialog(String message, String title, int buttons, String icon)
	{
		return showDialog(message, title, buttons, icon, null, null);
	}

	public static int showDialog(String message, String title, int buttons, String icon, Callback<Integer> callback, Map <Integer, List <Integer>> mapZoomRecords)
	{
		return showDialog(message, title, buttons, icon, callback, false, mapZoomRecords);
	}
	
	public static int showDialog(String message, String title, int buttons, String icon, Callback<Integer> callback, boolean modal, Map <Integer, List <Integer>> mapZoomRecords)
	{
		// track if desktop locked manually - release only when it's not lock

		boolean isDesktopLocked = false;
		if (Executions.getCurrent() == null)
		{
			if (!SessionManager.grantAccess(AEnv.getDesktop()))
			{
				return Messagebox.ABORT;
			}
			isDesktopLocked = true;
		}
		Messagebox msg = new Messagebox();

		int returnValue = msg.show(message, title, buttons, icon, callback, modal, mapZoomRecords);

		if (isDesktopLocked)
		{
			SessionManager.releaseDesktop(AEnv.getDesktop());
		}
		return returnValue;
	}

	public void onEvent(Event event) throws Exception
	{
		if (event == null)
			return;

		if (event.getTarget() == btnOk)
		{
			returnValue = OK;
		}
		else if (event.getTarget() == btnCancel)
		{
			returnValue = CANCEL;
		}
		else if (event.getTarget() == btnYes)
		{
			returnValue = YES;
		}
		else if (event.getTarget() == btnNo)
		{
			returnValue = NO;
		}
		else if (event.getTarget() == btnAbort)
		{
			returnValue = ABORT;
		}
		else if (event.getTarget() == btnRetry)
		{
			returnValue = RETRY;
		}
		else if (event.getTarget() == btnIgnore)
		{
			returnValue = IGNORE;
		}
		else if (event.getTarget() == btnZoomDocs)
		{
			if (mapZoomRecords != null && !mapZoomRecords.isEmpty() && mapZoomRecords.size() > 0)
			{
				for (Entry <Integer, List <Integer>> entry : mapZoomRecords.entrySet())
				{
					Integer tableID = entry.getKey();
					List <Integer> values = entry.getValue();
					if (values != null & values.size() > 0)
					{
						MQuery query = new MQuery(tableID);
						for (Integer record_ID : values)
						{
							query.addRestriction(query.getTableName().trim() + "_ID = " + record_ID, false, 0);
						}
						// Zoom records
						AEnv.zoom(query);
					}
				}
			}

			return;
		}

		try {
			this.detach();
		} catch (NullPointerException npe) {
			if (! (SessionManager.getSessionApplication() == null)) // IDEMPIERE-1937 - ignore when session was closed
				throw npe;
		}
	}
	
	@Override
	public void onPageDetached(Page page) {
		super.onPageDetached(page);
		if (callback != null)
		{
			callback.onCallback(returnValue);
		}
	}
}
