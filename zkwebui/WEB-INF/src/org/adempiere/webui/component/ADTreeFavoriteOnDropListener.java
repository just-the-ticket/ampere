package org.adempiere.webui.component;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MMenu;
import org.compiere.model.MTreeFavorite;
import org.compiere.model.MTreeFavoriteNode;
import org.compiere.model.MTreeNode;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

public class ADTreeFavoriteOnDropListener implements EventListener<Event>
{
	private SimpleFavoriteTreeModel	treeModel;
	private int						windowNo;
	private Tree					tree;
	private int						mTreeFavoriteID;
	private int						AD_Role_ID			= Env.getAD_Role_ID(Env.getCtx());
	private int						AD_Client_ID		= Env.getAD_Client_ID(Env.getCtx());
	private int						AD_Org_ID			= Env.getContextAsInt(Env.getCtx(), "#AD_Org_ID");
	private int						AD_User_ID			= Env.getContextAsInt(Env.getCtx(), "#AD_User_ID");
	private static final CLogger	log					= CLogger.getCLogger(ADTreeFavoriteOnDropListener.class);
	public static final String		MENU_ITEM_DELETE	= "DELETE";
	public static final String		MENU_ITEM_EXPAND	= "StartExpanded";
	public static final String		MENU_ITEM_COLLAPSE	= "StartCollapsed";

	public ADTreeFavoriteOnDropListener(Tree tree, SimpleFavoriteTreeModel treeModel, MTreeFavorite mTreeFavorite, int windowNo)
	{
		this.tree = tree;
		this.treeModel = treeModel;
		this.windowNo = windowNo;
		mTreeFavoriteID = mTreeFavorite.getTreeID(AD_Role_ID, AD_User_ID, AD_Client_ID);
	}

	/**
	 * Events For Right Click And Menu Item Dragged Source to Target Folder
	 */
	@Override
	public void onEvent(Event event) throws Exception
	{
		if (Events.ON_RIGHT_CLICK.equals(event.getName()))
		{
			MouseEvent me = (MouseEvent) event;
			Treeitem target = (Treeitem) ((Treerow) me.getTarget()).getParent();
			@SuppressWarnings("unchecked")
			DefaultTreeNode<Object> toNode = (DefaultTreeNode<Object>) target.getValue();
			menuItemList(toNode);
		}
		else if (event instanceof DropEvent)
		{
			DropEvent de = (DropEvent) event;
			log.fine("Source=" + de.getDragged() + " Target=" + de.getTarget());
			Component dragged = de.getDragged();
			
			if (de.getDragged() != de.getTarget())
			{

				Treeitem target = (Treeitem) ((Treerow) de.getTarget()).getParent();
				DefaultTreeNode<Object> stn_target = target.getValue();
				MTreeNode nd_target = (MTreeNode) stn_target.getData();
				MTreeFavorite mTreeFavorite = new MTreeFavorite(Env.getCtx(), 0, null);
				
				if(dragged instanceof Treerow)
				{
					Treerow treerow = (Treerow) dragged;
					Treeitem treeitem = (Treeitem) treerow.getParent();

					Treeitem src = (Treeitem) ((Treerow) de.getDragged()).getParent();

					Treerow tr = (Treerow) de.getDragged();
					String strDraggable = tr.getDraggable();


					/*
					 * Here Condition True when Node Moving Internally, False when Node Drag from Menu
					 * Bar to User Favorite Panel
					 */
					if (!strDraggable.equals("favourite"))
					{
						DefaultTreeNode<Object> stn_src = src.getValue();
						MTreeNode nd_src = (MTreeNode) stn_src.getData();

						/*
						 * True When Source is a Menu Item, Otherwise its Folder Item.
						 */
						if (!nd_src.isSummary())
						{
							int menuID = nd_src.getMenu_ID();
							boolean menuAvailable;
							/*
							 * True when Target is Menu Item, Otherwise its Folder Item.
							 */
							if (!nd_target.isSummary())
							{
								menuAvailable = mTreeFavorite.isMenuAvailable(menuID, nd_target.getParent_ID(), mTreeFavoriteID);
								if (nd_src.getParent_ID() == nd_target.getParent_ID())
									moveNode(src.getValue(), target.getValue());
								else if (menuAvailable)
									showDialog();
								else
									moveNode(src.getValue(), target.getValue());
							}
							else
							{
								menuAvailable = mTreeFavorite.isMenuAvailable(menuID, nd_target.getNode_ID(), mTreeFavoriteID);
								if (menuAvailable)
									showDialog();
								else
									moveNode(src.getValue(), target.getValue());
							}
						}
						else
						{
							moveNode(src.getValue(), target.getValue());
						}
					}
					else
					{
						int mID = Integer.valueOf((src.getValue().toString()));
						/*
						 * True when Target is Folder, Otherwise its Menu item.
						 */
						if (nd_target.isSummary())
						{
							if (mTreeFavorite.isMenuAvailable(mID, nd_target.getNode_ID(), mTreeFavoriteID))
								showDialog();
							else
								insertNodeMenu(mID, nd_target.getNode_ID(), stn_target);
						}
						else
						{
							DefaultTreeNode<Object> stn_target_parent = treeModel.getParent(stn_target);
							int pID = ((MTreeNode) stn_target_parent.getData()).getNode_ID();

							if (mTreeFavorite.isMenuAvailable(mID, pID, mTreeFavoriteID))
								showDialog();
							else
								insertNodeMenu(mID, pID, stn_target_parent);
						}
					}
				}
				else if(dragged instanceof Menuitem)
				{
					Menuitem item = (Menuitem) dragged;
					
					int mID = Integer.valueOf(item.getValue());
					
					// copy-pasted from above
					/*
					 * True when Target is Folder, Otherwise its Menu item.
					 */
					if (nd_target.isSummary())
					{
						if (mTreeFavorite.isMenuAvailable(mID, nd_target.getNode_ID(), mTreeFavoriteID))
							showDialog();
						else
							insertNodeMenu(mID, nd_target.getNode_ID(), stn_target);
					}
					else
					{
						DefaultTreeNode<Object> stn_target_parent = treeModel.getParent(stn_target);
						int pID = ((MTreeNode) stn_target_parent.getData()).getNode_ID();

						if (mTreeFavorite.isMenuAvailable(mID, pID, mTreeFavoriteID))
							showDialog();
						else
							insertNodeMenu(mID, pID, stn_target_parent);
					}
				}
			}
		}
	}

	/**
	 * Show Dialog for Warning
	 */
	private void showDialog()
	{
		FDialog.warn(0, Msg.getMsg(Env.getCtx(), "its.already.there"));
	}

	/**
	 * When Right click on Item show Delete Menupopup for Delete a node.
	 * 
	 * @param toNode
	 */
	private void menuItemList(DefaultTreeNode<Object> toNode)
	{
		int path[] = treeModel.getPath(toNode);
		Treeitem toItem = tree.renderItemByPath(path);

		tree.setSelectedItem(toItem);
		Events.sendEvent(tree, new Event(Events.ON_RIGHT_CLICK, tree));

		Menupopup popup = new Menupopup();
		Menuitem menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), "delete"), ThemeUtils.resolveImageURL( "Delete24.png"));
		menuItem.setValue(MENU_ITEM_DELETE);
		menuItem.setParent(popup);
		menuItem.addEventListener(Events.ON_CLICK, new DeleteListener(toNode));

		MTreeNode mtn = (MTreeNode) toNode.getData();
		if (mtn.isSummary())
		{
			MTreeFavoriteNode favNode = new MTreeFavoriteNode(Env.getCtx(), mtn.getNode_ID(), null);
			if (favNode.isCollapsible())
			{
				menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), MENU_ITEM_EXPAND), ThemeUtils.resolveImageURL( "Expanded16.png"));
				menuItem.setValue(MENU_ITEM_EXPAND);
			}
			else
			{
				menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), MENU_ITEM_COLLAPSE), ThemeUtils.resolveImageURL( "Collapsed16.png"));
				menuItem.setValue(MENU_ITEM_COLLAPSE);
			}
			menuItem.setParent(popup);
			menuItem.addEventListener(Events.ON_CLICK, new CollExpdListener(favNode));
		}

		popup.setPage(tree.getPage());
		popup.open(toItem.getTreerow(), "after_pointer");
	}

	/**
	 * Listener for Delete Node on Right click on MouseEvent
	 * 
	 * @author Logilite
	 */
	class DeleteListener implements EventListener<Event>
	{
		private DefaultTreeNode<Object> toNode;

		DeleteListener(DefaultTreeNode<Object> toNode)
		{
			this.toNode = toNode;
		}

		public void onEvent(Event event) throws Exception
		{
			if (Events.ON_CLICK.equals(event.getName()) && event.getTarget() instanceof Menuitem)
			{
				Menuitem menuItem = (Menuitem) event.getTarget();
				if (MENU_ITEM_DELETE.equals(menuItem.getValue()))
				{
					deleteNodeItem(toNode);
				}
			}
		}

		/**
		 * Delete a Specifically Node From Tree as well as Database
		 * 
		 * @param toNode
		 */
		private void deleteNodeItem(DefaultTreeNode<Object> toNode)
		{
			int nodeID = ((MTreeNode) toNode.getData()).getNode_ID();
			String deleteNodeQuery = "DELETE FROM ad_tree_favorite_node " + "WHERE ad_tree_favorite_node_id IN ( 	"
			                         + "	WITH RECURSIVE supplytree(ad_tree_favorite_node_id) AS (				"
			                         + " 	SELECT ad_tree_favorite_node_id, parent_id 								"
			                         + "	FROM ad_tree_favorite_node 												"
			                         + "	WHERE ad_tree_favorite_node_id = ?										"
			                         + "UNION ALL 																	"
			                         + "	SELECT si.ad_tree_favorite_node_id, si.parent_id 						"
			                         + "	FROM ad_tree_favorite_node As si										"
			                         + "	INNER JOIN supplytree AS sp	ON (si.parent_id=sp.ad_tree_favorite_node_id)"
			                         + ") 																			"
			                         + "SELECT  ad_tree_favorite_node_id 											"
			                         + "FROM supplytree 															"
			                         + "ORDER BY ad_tree_favorite_node_id)											";

			int noOfRowsDeleted = DB.executeUpdate(deleteNodeQuery, nodeID, null);
			if (noOfRowsDeleted > 0)
				treeModel.removeNode(toNode);
		}
	}

	/**
	 * Listener for set default start Collapse/Expand Node by Right click on
	 * MouseEvent.
	 * 
	 * @author Sachin
	 */
	class CollExpdListener implements EventListener<Event>
	{
		private MTreeFavoriteNode favNode;

		CollExpdListener(MTreeFavoriteNode favNode)
		{
			this.favNode = favNode;
		}

		public void onEvent(Event event) throws Exception
		{
			if (Events.ON_CLICK.equals(event.getName()) && event.getTarget() instanceof Menuitem)
			{
				Menuitem menuItem = (Menuitem) event.getTarget();
				if (MENU_ITEM_EXPAND.equals(menuItem.getValue()))
				{
					favNode.setIsCollapsible(false);
				}
				else if (MENU_ITEM_COLLAPSE.equals(menuItem.getValue()))
				{
					favNode.setIsCollapsible(true);
				}
				favNode.saveEx();
			}
		} // onEvent
	} // ColExpListener

	/**
	 * Insert Node into Tree it's contains only Menu type node, Dragged from
	 * Menu Tab.
	 * 
	 * @param menuID
	 * @param parentNodeID
	 * @param stn
	 */
	public void insertNodeMenu(int menuID, int parentNodeID, DefaultTreeNode<Object> stn)
	{
		MTreeFavoriteNode tFavNode = new MTreeFavoriteNode(Env.getCtx(), 0, null);
		tFavNode.setAD_Client_ID(AD_Client_ID);
		tFavNode.setAD_Org_ID(AD_Org_ID);
		tFavNode.setAD_Tree_Favorite_ID(mTreeFavoriteID);
		tFavNode.setSeqNo(0);
		tFavNode.setParent_ID(parentNodeID);
		tFavNode.setIsSummary(false);
		tFavNode.setAD_Menu_ID(menuID);
		if (!tFavNode.save())
		{
			throw new AdempiereException(Msg.getMsg(Env.getCtx(), "could.not.create.node"));
		}

		MMenu menu = new MMenu(Env.getCtx(), menuID, null);
		MTreeNode mNode = new MTreeNode(tFavNode.getAD_Tree_Favorite_Node_ID(), tFavNode.getSeqNo(), menu.getName(), menu.getName(), tFavNode.getParent_ID(), tFavNode.isSummary(),
		                                tFavNode.getAD_Menu_ID(), menu.getAction(), false);
		DefaultTreeNode<Object> node = new DefaultTreeNode<Object>(mNode);

		int index = 0;
		if (mNode.isSummary())
			index = stn.getChildren().indexOf(node) + 1;

		treeModel.addNode(stn, node, index);
		int[] path = treeModel.getPath(node);
		Treeitem ti = tree.renderItemByPath(path);
		tree.setSelectedItem(ti);
		Events.sendEvent(tree, new Event(Events.ON_SELECT, tree));
	}

	/**
	 * Internally movement of Tree node
	 * 
	 * @param movingNode
	 * @param toNode
	 */
	private void moveNode(DefaultTreeNode<Object> movingNode, DefaultTreeNode<Object> toNode)
	{
		log.info(movingNode.toString() + " to " + toNode.toString());

		if (movingNode == toNode)
			return;

		MTreeNode toMNode = (MTreeNode) toNode.getData();
		if (!toMNode.isSummary()) // drop on a child node
		{
			moveNode(movingNode, toNode, false);
		}
		else // drop on a summary node
		{
			// prompt user to select insert after or drop into the summary node
			int path[] = treeModel.getPath(toNode);
			Treeitem toItem = tree.renderItemByPath(path);

			tree.setSelectedItem(toItem);
			Events.sendEvent(tree, new Event(Events.ON_SELECT, tree));

			MenuListener listener = new MenuListener(movingNode, toNode);

			Menupopup popup = new Menupopup();
			Menuitem menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), "insert.after"));
			menuItem.setValue("InsertAfter");
			menuItem.setParent(popup);
			menuItem.addEventListener(Events.ON_CLICK, listener);

			menuItem = new Menuitem(Msg.getMsg(Env.getCtx(), "move.into"));
			menuItem.setValue("MoveInto");
			menuItem.setParent(popup);
			menuItem.addEventListener(Events.ON_CLICK, listener);
			popup.setPage(tree.getPage());
			popup.open(toItem.getTreerow(), "overlap");
		}
	} // moveNode

	/**
	 * It's specify the Moving node where to inserted... Like Insert After or
	 * Move Into.
	 * 
	 * @param movingNode
	 * @param toNode
	 * @param moveInto
	 */
	private void moveNode(DefaultTreeNode<Object> movingNode, DefaultTreeNode<Object> toNode, boolean moveInto)
	{
		DefaultTreeNode<Object> newParent;
		int index;

		// remove
		DefaultTreeNode<Object> oldParent = treeModel.getParent(movingNode);
		treeModel.removeNode(movingNode);

		// get new index
		if (!moveInto)
		{
			newParent = treeModel.getParent(toNode);
			index = newParent.getChildren().indexOf(toNode) + 1; // the next node
		}
		else
		// drop on a summary node
		{
			newParent = toNode;
			index = 0; // the first node
		}

		// insert
		treeModel.addNode(newParent, movingNode, index);

		int path[] = treeModel.getPath(movingNode);
		Treeitem movingItem = tree.renderItemByPath(path);
		tree.setSelectedItem(movingItem);
		Events.sendEvent(tree, new Event(Events.ON_SELECT, tree));

		// *** Save changes to disk
		Trx trx = Trx.get(Trx.createTrxName("ADTreeFavoriteNode"), true);
		try
		{
			MTreeNode oldMParent = (MTreeNode) oldParent.getData();
			for (int i = 0; i < oldParent.getChildCount(); i++)
			{
				DefaultTreeNode<Object> nd = (DefaultTreeNode<Object>) oldParent.getChildAt(i);
				MTreeNode md = (MTreeNode) nd.getData();
				StringBuffer sql = new StringBuffer("UPDATE ");
				sql.append(" AD_Tree_Favorite_Node ").append(" SET Parent_ID=").append(oldMParent.getNode_ID())
				   .append(", SeqNo=").append(i).append(", Updated=CURRENT_TIMESTAMP")
				   .append(" WHERE AD_Tree_Favorite_Node_ID=").append(md.getNode_ID());
				log.fine(sql.toString());
				DB.executeUpdate(sql.toString(), trx.getTrxName());
				md.setParent_ID(oldMParent.getNode_ID());
			}
			if (oldParent != newParent)
			{
				MTreeNode newMParent = (MTreeNode) newParent.getData();
				for (int i = 0; i < newParent.getChildCount(); i++)
				{
					DefaultTreeNode<Object> nd = (DefaultTreeNode<Object>) newParent.getChildAt(i);
					MTreeNode md = (MTreeNode) nd.getData();
					StringBuffer sql = new StringBuffer("UPDATE ");
					sql.append(" AD_Tree_Favorite_Node ").append(" SET Parent_ID=").append(newMParent.getNode_ID())
					   .append(", SeqNo=").append(i).append(", Updated=CURRENT_TIMESTAMP")
					   .append(" WHERE AD_Tree_Favorite_Node_ID=").append(md.getNode_ID());
					log.fine(sql.toString());
					DB.executeUpdate(sql.toString(), trx.getTrxName());
					md.setParent_ID(newMParent.getNode_ID());
				}
			}
			// COMMIT *********************
			trx.commit(true);
		}
		catch (Exception e)
		{
			trx.rollback();
			FDialog.error(windowNo, tree, "Tree Update Error", e.getLocalizedMessage());
		}
		finally
		{
			trx.close();
			trx = null;
		}
	}

	/**
	 * Listener for Movement of Node
	 * 
	 * @author Logilite
	 */
	class MenuListener implements EventListener<Event>
	{
		private DefaultTreeNode<Object>	movingNode;
		private DefaultTreeNode<Object>	toNode;

		MenuListener(DefaultTreeNode<Object> movingNode, DefaultTreeNode<Object> toNode)
		{
			this.movingNode = movingNode;
			this.toNode = toNode;
		}

		public void onEvent(Event event) throws Exception
		{
			if (Events.ON_CLICK.equals(event.getName()) && event.getTarget() instanceof Menuitem)
			{
				Menuitem menuItem = (Menuitem) event.getTarget();
				if ("InsertAfter".equals(menuItem.getValue()))
				{
					moveNode(movingNode, toNode, false);
				}
				else if ("MoveInto".equals(menuItem.getValue()))
				{
					moveNode(movingNode, toNode, true);
				}
			}
		}
	}
}
