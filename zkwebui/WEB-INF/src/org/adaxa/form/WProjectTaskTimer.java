package org.adaxa.form;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.util.Callback;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.WNumEditorDialog;
import org.adempiere.webui.window.WTextEditorDialog;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MActivity;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MProduct;
import org.compiere.model.MProject;
import org.compiere.model.MRole;
import org.compiere.model.MTabCustomization;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;
import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.au.out.AuScript;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vbox;

public class WProjectTaskTimer extends ADForm implements IFormController, ValueChangeListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7875645507898640830L;

	private CustomForm form = new CustomForm();
	
	private static CLogger log = CLogger.getCLogger(WProjectTaskTimer.class);


	/**
	 *	Timer Data JSON Tags 
	 */
	private static final String TDT_STATUS = "Status";
	private static final String TDT_STATUS_STARTED = "STARTED";
	private static final String TDT_STATUS_PAUSED = "PAUSED";
	private static final String TDT_STATUS_ENDED = "ENDED";

	private static final String TDT_ARRAY_DURATIONS = "Durations";
	private static final String TDT_DURATION_START_TIME = "StartTime";
	private static final String TDT_DURATION_START_TIME_MILLIS = "StartTimeMillis";
	private static final String TDT_DURATION_END_TIME = "EndTime";
	private static final String TDT_DURATION_END_TIME_MILLIS = "EndTimeMillis";
	private static final String TDT_DURATION_SEGMENT_DURATION = "SegmentDuration";

	private static final String TDT_LATEST_START_TIME = "LatestStartTime";
	private static final String TDT_LATEST_START_TIME_MILLIS = "LatestStartTimeMillis";
	private static final String TDT_TOTAL_DURATION = "TotalDuration";

	/**
	 * Custom Table ID / Tab ID
	 */
	private static final int TASK_TABLE = 0;
	private static final int ACTIVE_TIMER_TABLE = 1;
	private static final int PAST_ENTRIES_TABLE = 2;
	//
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private Label userLabel = new Label();
	private WTableDirEditor userField = null;
	private Button buttonRefresh = new Button();
	private Button buttonSave = new Button();
	private Button buttonLeave = new Button();
	private Button buttonCustomTasks = new Button();
	private Button buttonCustomActiveTimer = new Button();
	private Button buttonCustomPastEntries = new Button();
	
	private Label bpartnerLabel = new Label();
	private WSearchEditor bpartnerField = null;
	private WListbox taskTable = ListboxFactory.newDataTable();
	private WListbox timerTable = ListboxFactory.newDataTable();
	private WListbox pastEntriesTable = ListboxFactory.newDataTable();
	private WListItemRenderer timerRenderer = null;
	private WListItemRenderer pastEntriesRenderer = null;
	private Borderlayout infoPanel = new Borderlayout();
	private Panel taskPanel = new Panel();
	private Panel timerPanel = new Panel();
	private Label taskLabel = new Label();
	private Label timerLabel = new Label();
	private Borderlayout taskLayout = new Borderlayout();
	private Borderlayout timerLayout = new Borderlayout();
	Tabbox timerTabbedPane = new Tabbox();
	private Label taskInfo = new Label();
	private Label timerInfo = new Label();
	private Label pastEntriesInfo = new Label();
	
	private Combobox activityField = new Combobox();
	
	private int userID = 0;
	private int projectID = 0;
	private int bpartnerID = 0;
	
	private List<Button> startTimerButtonList = new ArrayList<Button>();
	private Map<Integer, Button> startButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> pauseButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> stopButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> deleteButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> editButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> addButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Label> durationLabelMap = new HashMap<Integer, Label>();
	private Map<Integer, Label> startLabelMap = new HashMap<Integer, Label>();
	
	private Map<Integer, TimerState> timerStateMap = new HashMap<Integer, TimerState>();

	private MUser user = null;
	
	private boolean isInit = true; //on first opening window
	private SimpleDateFormat startDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public WProjectTaskTimer() {
		super();
	}
	
	@Override
	protected void initForm() {		
		try
		{
			dynInit();
			zkInit();
			
			timerRenderer = (WListItemRenderer) timerTable.getItemRenderer();
			pastEntriesRenderer = (WListItemRenderer) pastEntriesTable.getItemRenderer();
			refreshTasks();
			refreshTimers();
			refreshPastEntries();
			isInit = false;
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
	}
	
	@Override
	public void onPageAttached(Page newpage, Page oldpage) {
		super.onPageAttached(newpage, oldpage);
		
		// update active timer values on interval, stop the interval loop when no element is found
		String script = "timer = setInterval(function() {"
				+ "labels = document.querySelectorAll(\"#" + form.getUuid() + " .duration-label.active-timer\");"
				+ "for (const label of labels) {"
				+ "	var duration = label.innerHTML.split(':');"
				+ "	var s = (+duration[0]) * 60 * 60 + (+duration[1]) * 60 + (+duration[2]) + 1;" // increment the seconds
				+ " dateObj = new Date(s * 1000);\r\n" 
				+ "	hours = dateObj.getUTCHours();" 
				+ "	minutes = dateObj.getUTCMinutes();" 
				+ "	seconds = dateObj.getSeconds();"
				+ " timeString = hours.toString().padStart(2, '0') + ':' + "
				+ " 	minutes.toString().padStart(2, '0') + ':' +"
				+ " 	seconds.toString().padStart(2, '0');"
				+ "	label.innerHTML = timeString;"
				+ "}"
				+ "durationLabels = document.querySelectorAll(\"#" + form.getUuid() + " .duration-label\");"
				+ "if(durationLabels.length == 0) {"
//				+ "	clearInterval(timer);"  jtrinidad - do not remove, otherwise timer will not run when no item on first load
				+ "}"
				+ "}, 1000);";
		AuScript aus = new AuScript(form, script);
		Clients.response(aus);
	}

	/**
	 *  Static Init
	 *  @throws Exception
	 */
	private void zkInit() throws Exception
	{
		//
		form.appendChild(mainLayout);
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		//
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		parameterPanel.appendChild(parameterLayout);
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		parameterLayout.setStyle("overflow: auto;");
		userLabel.setText(Msg.translate(Env.getCtx(), "AD_User_ID"));
		bpartnerLabel.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		taskLabel.setText(Msg.translate(Env.getCtx(), "Available Tasks"));
		taskLabel.setStyle("font-size: 125%;");
		timerLabel.setText(Msg.translate(Env.getCtx(), "Active Timers"));
		timerLabel.setStyle("font-size: 125%;");
		taskPanel.appendChild(taskLayout);
		timerPanel.appendChild(timerLayout);
		timerInfo.setText(".");
		taskInfo.setText(".");
		pastEntriesInfo.setText(".");
		
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		Rows rows = null;
		Row row = null;
		
		ZKUpdateUtil.setWidth(parameterLayout, "100%");
		rows = parameterLayout.newRows();
		row = rows.newRow();
		
		row.appendCellChild(userLabel.rightAlign());
		row.appendCellChild(userField.getComponent(), 2);
		ZKUpdateUtil.setWidth(userField.getComponent(), "99%");
		
		row.appendCellChild(bpartnerLabel.rightAlign());
		row.appendCellChild(bpartnerField.getComponent(), 2);
		ZKUpdateUtil.setWidth(bpartnerField.getComponent(), "99%");

		buttonRefresh.setIconSclass("z-icon-refresh");
		buttonRefresh.setLabel("Reload");
		buttonRefresh.setTooltiptext("Manually reload available tasks");
		buttonRefresh.setWidth("100px");
		row.appendCellChild(buttonRefresh, 1);

		buttonSave.setIconSclass("z-icon-Save");
		buttonSave.setLabel("Save");
		buttonSave.setTooltiptext("Manually save timer value.");
		buttonSave.setWidth("100px");
		row.appendCellChild(buttonSave, 1);
		
		buttonLeave.setIconSclass("z-icon-sign-out");
		buttonLeave.setLabel("Exit");
		buttonLeave.setTooltiptext("Pause all timers and leave.");
		buttonLeave.setWidth("100px");
		row.appendCellChild(buttonLeave, 1);

		buttonCustomTasks.setIconSclass("z-icon-Customize");
		buttonCustomActiveTimer.setIconSclass("z-icon-Customize");
		buttonCustomPastEntries.setIconSclass("z-icon-Customize");

		taskPanel.appendChild(taskLayout);
		ZKUpdateUtil.setWidth(taskPanel, "100%");
		ZKUpdateUtil.setHeight(taskPanel, "100%");
		ZKUpdateUtil.setWidth(taskLayout, "100%");
		ZKUpdateUtil.setHeight(taskLayout, "100%");
		taskLayout.setStyle("border: none");
		
		timerPanel.appendChild(timerLayout);
		ZKUpdateUtil.setWidth(timerPanel, "100%");
		ZKUpdateUtil.setHeight(timerPanel, "100%");
		ZKUpdateUtil.setWidth(timerLayout, "100%");
		ZKUpdateUtil.setHeight(timerLayout, "100%");
		timerLayout.setStyle("border: none");
		
		north = new North();
		north.setStyle("border: none");
		taskLayout.appendChild(north);
		north.appendChild(taskLabel);
		South south = new South();
		south.setStyle("border: none");
		taskLayout.appendChild(south);
		
		Hbox hbox = new Hbox();
		Space space = new Space();
		ZKUpdateUtil.setHflex(space, "1");
		hbox.appendChild(space);
		hbox.appendChild(taskInfo.rightAlign());
		hbox.appendChild(buttonCustomTasks);
		ZKUpdateUtil.setWidth(taskInfo, "100px");
		ZKUpdateUtil.setHflex(hbox, "1");
		south.appendChild(hbox);
		
		Center center = new Center();
		taskLayout.appendChild(center);
		center.appendChild(taskTable);
		ZKUpdateUtil.setHflex(taskTable, "1");
//		taskTable.setSclass("task-table");
		center.setStyle("border: none");

		center = new Center();
		timerLayout.appendChild(center);
		
		Tabpanels tabPanels = new Tabpanels();
		timerTabbedPane.appendChild(tabPanels);
		Tabs tabs = new Tabs();
		timerTabbedPane.appendChild(tabs);

		Tab tab = new Tab(Util.cleanAmp(Msg.translate(Env.getCtx(), "Active Tasks")));
		tabs.appendChild(tab);

		Tabpanel tabPanel = new Tabpanel();
		Vbox vbox = new Vbox();
		vbox.appendChild(timerTable);
		ZKUpdateUtil.setVflex(vbox, "1");
		ZKUpdateUtil.setHflex(vbox, "1");
//		ZKUpdateUtil.setWidth(vbox, "100%");
		ZKUpdateUtil.setVflex(timerTable, "1");
		ZKUpdateUtil.setHflex(timerTable, "1");

		
		hbox = new Hbox();
		space = new Space();
		ZKUpdateUtil.setHflex(space, "1");
		hbox.appendChild(space);
		hbox.appendChild(timerInfo.rightAlign());
		hbox.appendChild(buttonCustomActiveTimer);
		ZKUpdateUtil.setWidth(timerInfo, "100px");
		ZKUpdateUtil.setHflex(hbox, "1");
		vbox.appendChild(hbox);

		tabPanel.appendChild(vbox);
		tabPanels.appendChild(tabPanel);
		
		tab = new Tab(Util.cleanAmp(Msg.translate(Env.getCtx(), "Past Time Entries")));
		tabs.appendChild(tab);
		
		tabPanel = new Tabpanel();
		vbox = new Vbox();
		vbox.appendChild(pastEntriesTable);
		ZKUpdateUtil.setVflex(vbox, "1");
		ZKUpdateUtil.setHflex(vbox, "1");
		ZKUpdateUtil.setVflex(pastEntriesTable, "1");
		ZKUpdateUtil.setHflex(pastEntriesTable, "1");

		hbox = new Hbox();
		space = new Space();
		ZKUpdateUtil.setHflex(space, "1");
		hbox.appendChild(space);
		hbox.appendChild(pastEntriesInfo.rightAlign());
		hbox.appendChild(buttonCustomPastEntries);
		ZKUpdateUtil.setWidth(pastEntriesInfo, "100px");
		ZKUpdateUtil.setHflex(hbox, "1");
		vbox.appendChild(hbox);		
		tabPanel.appendChild(vbox);
		tabPanels.appendChild(tabPanel);

		ZKUpdateUtil.setVflex(timerTabbedPane, "1");
		center.appendChild(timerTabbedPane);
		
//		timerTable.setSclass("form-task-timer timer-table");
//		pastEntriesTable.setSclass("form-task-timer timer-table");
		center.setStyle("border: none");
		//
		center = new Center();
		ZKUpdateUtil.setVflex(center, "flex");
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);
		
		infoPanel.setStyle("border: none");
		ZKUpdateUtil.setWidth(infoPanel, "100%");
		ZKUpdateUtil.setHeight(infoPanel, "100%");
		
		north = new North();
		north.setStyle("border: none");
		ZKUpdateUtil.setHeight(north, "49%");
		infoPanel.appendChild(north);
		north.appendChild(taskPanel);
		north.setSplittable(true);
		center = new Center();
		center.setStyle("border: none");
		ZKUpdateUtil.setVflex(center, "flex");
		infoPanel.appendChild(center);
		center.appendChild(timerPanel);
		
		this.appendChild(form);
	}   //  jbInit

	/**
	 *  Dynamic Init (prepare dynamic fields)
	 *  @throws Exception if Lookups cannot be initialized
	 */
	public void dynInit() throws Exception
	{
		//  User
		int columnID = MColumn.get(Env.getCtx(), "S_Resource", "AD_User_ID").get_ID();
		MLookup lookupUser = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, columnID, DisplayType.TableDir);
		userField = new WTableDirEditor("AD_User_ID", true, true, true, lookupUser);
		userID = Env.getAD_User_ID(Env.getCtx());
		userField.setValue(userID);
		user = MUser.get(Env.getCtx(), userID);
		userField.addValueChangeListener(this);
		
		MRole role = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		if (role.isClientAccess(0, true))
			userField.setReadWrite(true);
		else
			userField.setReadWrite(false);
		
		buttonRefresh.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				refreshTasks();
			}
			
		});

		buttonSave.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				persistTimer(false);
			}
			
		});
		
		buttonLeave.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				persistTimer(true);
	        	SessionManager.getAppDesktop().closeActiveWindow();
			}
			
		});
		
		buttonCustomTasks.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				customize(taskTable, CustomForm.WPROJECT_TASKTIMER_ID, TASK_TABLE);
			}
			
		});

		buttonCustomActiveTimer.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				customize(timerTable, CustomForm.WPROJECT_TASKTIMER_ID, ACTIVE_TIMER_TABLE);
			}
			
		});

		buttonCustomPastEntries.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				customize(pastEntriesTable, CustomForm.WPROJECT_TASKTIMER_ID, PAST_ENTRIES_TABLE);
			}
			
		});


		//  BPartner
		columnID = MColumn.get(Env.getCtx(), "C_Project", "C_BPartner_ID").get_ID();
		MLookup lookupBP = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, columnID, DisplayType.Search);
		bpartnerField = new WSearchEditor("C_BPartner_ID", false, false, true, lookupBP);
		bpartnerField.addValueChangeListener(this);
		
		// Activity
		List<MActivity> activityList = new Query(Env.getCtx(), MActivity.Table_Name, "", null)
				.setClient_ID()
				.setOnlyActiveRecords(true)
				.list();
		for (MActivity a : activityList) {
			activityField.appendItem(a.getValue() + "-" + a.getName(), a.get_ID());
		}
		
	}   //  dynInit
	
	private Vector<String> getTaskColumnNames() {	
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "C_Project_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectPhase_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectTask_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Description"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Start"));
		return columnNames;
	}
	
	private void setTaskColumnClass(WListbox table) {
		int i = 0;
		table.setColumnClass(i++, String.class, true);	// Project
		table.setColumnClass(i++, String.class, true);	// Phase
		table.setColumnClass(i++, String.class, true);	// Task
		table.setColumnClass(i++, String.class, true);	// Description
		table.setColumnClass(i++, String.class, true);	// BP
		table.setColumnClass(i++, Button.class, false);	// Start Timer

//		table.autoSize();
		initTableColumnWidth(taskTable, CustomForm.WPROJECT_TASKTIMER_ID, TASK_TABLE);

	}
	
	private void clearTasks() {
		for (Button b : startTimerButtonList) {
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		startTimerButtonList.clear();
		taskTable.clear();
	}
	
	private void refreshTasks() {
		clearTasks();
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT CONCAT_WS('-', bp.Name, pj.Value, pj.name) as ProjectValue, ph.Name PhaseName, pt.C_ProjectTask_ID, pt.Name TaskName, "
				+ "pt.Description TaskDescription, bp.Name BPName,"
				+ " r.S_Resource_ID, pj.C_Project_ID, ph.C_ProjectPhase_ID ");
		sql.append("FROM C_Project pj ");
		sql.append("LEFT JOIN C_ProjectPhase ph ON (ph.C_Project_ID = pj.C_Project_ID AND ph.IsActive = 'Y') ");
		sql.append("LEFT JOIN C_ProjectTask pt ON (pt.C_ProjectPhase_ID = ph.C_ProjectPhase_ID AND pt.IsActive='Y') ");
		sql.append("INNER JOIN C_ProjectResource pr ON (pr.C_Project_ID = pj.C_Project_ID) ");
		sql.append("INNER JOIN S_Resource r ON (r.S_Resource_ID = pr.S_Resource_ID) ");
		sql.append("LEFT JOIN C_BPartner bp ON (bp.C_BPartner_ID = pj.C_BPartner_ID) ");
		sql.append("WHERE pj.IsActive='Y' ");
		sql.append("AND COALESCE(pr.AD_User_ID, r.AD_User_ID) = ? ");
		params.add(userID);
		if (projectID > 0) {
			sql.append("AND pj.C_Project_ID = ? ");
			params.add(projectID);
		}
		if (bpartnerID > 0) {
			sql.append("AND bp.C_BPartner_ID = ? ");
			params.add(bpartnerID);
		}
		sql.append("ORDER BY pj.Value, ph.Name, pt.Name");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			DB.setParameters(pstmt, params);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<Object> line = new Vector<Object>();
				line.add(rs.getString("ProjectValue"));
				line.add(rs.getString("PhaseName"));
				line.add(rs.getString("TaskName"));
				line.add(rs.getString("TaskDescription"));
				line.add(rs.getString("BPName"));
				
				Button startButton = new Button();
				startButton.setAttribute("C_Project_ID", rs.getInt("C_Project_ID"));
				startButton.setAttribute("C_ProjectPhase_ID", rs.getInt("C_ProjectPhase_ID"));
				startButton.setAttribute("C_ProjectTask_ID", rs.getInt("C_ProjectTask_ID"));
				startButton.setAttribute("S_Resource_ID", rs.getInt("S_Resource_ID"));
				startButton.setIconSclass("z-icon-Ok");
				startButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						Button button = (Button) event.getTarget();
						int taskID = (Integer) button.getAttribute("C_ProjectTask_ID");
						int phaseID = (Integer) button.getAttribute("C_ProjectPhase_ID");
						int projectID = (Integer) button.getAttribute("C_Project_ID");
						int resourceID = (Integer) button.getAttribute("S_Resource_ID");
						onCreateTimer(projectID, phaseID, taskID, resourceID);
					}
					
				});
				line.add(startButton);
				startTimerButtonList.add(startButton);
				
				data.add(line);
			}
		} catch (SQLException e) {
			throw new DBException(e, sql.toString());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		taskTable.setData(model, getTaskColumnNames());
		
		setTaskColumnClass(taskTable);
		taskInfo.setText(taskTable.getRowCount() + " result(s)");
		taskTable.setAutoResize(false);

	}
	
	private Vector<String> getTimerColumnNames() {	
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "C_Project_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectTask_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Description"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_Activity_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "StartDate"));
		columnNames.add(Msg.translate(Env.getCtx(), "Notes"));
		columnNames.add(Msg.translate(Env.getCtx(), "Edit Notes"));
		columnNames.add(Msg.translate(Env.getCtx(), "Duration"));
		columnNames.add(Msg.translate(Env.getCtx(), "Run Timer"));
		columnNames.add(Msg.translate(Env.getCtx(), "Pause"));
		columnNames.add(Msg.translate(Env.getCtx(), "Stop"));
		columnNames.add(Msg.translate(Env.getCtx(), "Delete"));
		columnNames.add(Msg.translate(Env.getCtx(), "Add"));
		return columnNames;
	}
	
	@Override
	public void onPageDetached(Page page) {
		super.onPageDetached(page);
		
		// update active timer values on interval, stop the interval loop when no element is found
		String script = "clearInterval(timer)";
		AuScript aus = new AuScript(form, script);
		Clients.response(aus);
	}

	private void setTimerColumnClass(IMiniTable table) {
		int i = 0;
		table.setColumnClass(i++, String.class, true);	// Project
		table.setColumnClass(i++, String.class, true);	// Task
		table.setColumnClass(i++, String.class, true);	// Description
		table.setColumnClass(i++, String.class, true);	// BP
		table.setColumnClass(i++, Combobox.class, false);	// Activity
		table.setColumnClass(i++, String.class, true);	// StartDate
		table.setColumnClass(i++, String.class, false);	// Notes
		table.setColumnClass(i++, Button.class, false);	// Edit Notes
		table.setColumnClass(i++, Label.class, true);	// Duration
		table.setColumnClass(i++, Button.class, false);	// Start
		table.setColumnClass(i++, Button.class, false);	// Pause
		table.setColumnClass(i++, Button.class, false);	// Stop
		table.setColumnClass(i++, Button.class, false);	// Delete
		table.setColumnClass(i++, Button.class, false);	// Add

		//  Table UI
//		table.autoSize();
		initTableColumnWidth(timerTable, CustomForm.WPROJECT_TASKTIMER_ID, ACTIVE_TIMER_TABLE);

	}
	
	private Vector<String> getPastEntriesColumnNames() {	
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "C_Project_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectTask_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Description"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_Activity_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "StartDate"));
		columnNames.add(Msg.translate(Env.getCtx(), "Notes"));
		columnNames.add(Msg.translate(Env.getCtx(), "Edit Notes"));
		columnNames.add(Msg.translate(Env.getCtx(), "Qty Entered"));
		columnNames.add(Msg.translate(Env.getCtx(), "Save"));
		return columnNames;
	}
	
	private void setPastEntriesColumnClass(IMiniTable table) {
		int i = 0;
		table.setColumnClass(i++, String.class, true);	// Project
		table.setColumnClass(i++, String.class, true);	// Task
		table.setColumnClass(i++, String.class, true);	// Description
		table.setColumnClass(i++, String.class, true);	// BP
		table.setColumnClass(i++, Combobox.class, false);	// Activity
		table.setColumnClass(i++, Timestamp.class, false);	// StartDate
		table.setColumnClass(i++, String.class, false);	// Notes
		table.setColumnClass(i++, Button.class, false);	// Edit Notes
		table.setColumnClass(i++, BigDecimal.class, false);	// Qty Entered
		table.setColumnClass(i++, Button.class, false);	// Save

		initTableColumnWidth(pastEntriesTable, CustomForm.WPROJECT_TASKTIMER_ID, PAST_ENTRIES_TABLE);
	}
	
	private void clearTimers() {
		for (Integer key : startButtonMap.keySet()) {
			Button b = startButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		startButtonMap.clear();
		
		for (Integer key : pauseButtonMap.keySet()) {
			Button b = pauseButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		pauseButtonMap.clear();
		
		for (Integer key : stopButtonMap.keySet()) {
			Button b = stopButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		stopButtonMap.clear();
		
		for (Integer key : deleteButtonMap.keySet()) {
			Button b = deleteButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		deleteButtonMap.clear();
		
		for (Integer key : addButtonMap.keySet()) {
			Button b = addButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		addButtonMap.clear();
		
		durationLabelMap.clear();
		timerStateMap.clear();
		timerTable.clear();
	}
	
	private void refreshTimers() {
		persistTimer(false);
		clearTimers();
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT te.S_TimesheetEntry_ID, CONCAT_WS(' ', bp.Name, pj.Value, pj.name) as  ProjectValue, pt.Name TaskName, pt.Description TaskDescription, bp.Name BPName, ");
		sql.append("te.C_Activity_ID, a.Name ActivityName, te.StartDate, COALESCE(te.Comments,'') Notes, te.TimerData ");
		sql.append("FROM S_TimesheetEntry te ");
		sql.append("INNER JOIN C_Project pj ON (pj.C_Project_ID = te.C_Project_ID) ");
		sql.append("LEFT OUTER JOIN C_ProjectTask pt ON (pt.C_ProjectTask_ID = te.C_ProjectTask_ID) ");
		sql.append("INNER JOIN C_BPartner bp ON (bp.C_BPartner_ID = pj.C_BPartner_ID) ");
		sql.append("LEFT JOIN C_Activity a ON (a.C_Activity_ID = te.C_Activity_ID) ");
		sql.append("WHERE te.IsActive='Y' AND te.Processed='N' AND te.EndDate IS NULL ");
		sql.append("AND te.C_BPartner_ID = ? ");
		sql.append("ORDER BY te.StartDate DESC");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			DB.setParameters(pstmt, new Object[]{user.getC_BPartner_ID()});
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("S_TimesheetEntry_ID");
				Vector<Object> line = new Vector<Object>();
				line.add(rs.getString("ProjectValue"));
				line.add(rs.getString("TaskName"));
				line.add(rs.getString("TaskDescription"));
				line.add(rs.getString("BPName"));
				
				Combobox activity = (Combobox) activityField.clone();
				activity.setAttribute("S_TimesheetEntry_ID", id);
				if (rs.getInt("C_Activity_ID") > 0)
					activity.setValue(rs.getInt("C_Activity_ID"));
				line.add(activity);
				
				activity.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						int row = timerRenderer.getRowPosition(event.getTarget());
						autoSaveRow(row);
					}
					
				});
				
				Label startDateLabel = new Label();
				Timestamp startDate = rs.getTimestamp("StartDate");
				if (startDate != null) {
					startDateLabel.setValue(startDateTimeFormat.format(startDate));
				}
				line.add(startDateLabel);
				startLabelMap.put(id, startDateLabel);
				
				line.add(rs.getString("Notes"));
				
				Button editNotesButton = new Button();
				editNotesButton.setAttribute("S_TimesheetEntry_ID", id);
				editNotesButton.setIconSclass("z-icon-pencil");
				editNotesButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onEditNotes(id, event.getTarget());
					}
					
				});
				line.add(editNotesButton);
				editButtonMap.put(id, editNotesButton);
				
				Label durationLabel = new Label();
				durationLabel.setSclass("duration-label");
				JSONObject jo = null;
				if (Util.isEmpty(rs.getString("TimerData"),true)) {
					durationLabel.setValue("00:00:00");
					line.add(durationLabel);
				} else {
					jo = new JSONObject(rs.getString("TimerData"));
					JSONArray ja = jo.getJSONArray(TDT_ARRAY_DURATIONS);
					long totalDuration = 0;
					for (int i = 0; i < ja.length(); i++) {
						 JSONObject duration = ja.getJSONObject(i);
						 if (duration.isNull(TDT_DURATION_END_TIME)) {
							 duration.put(TDT_DURATION_END_TIME_MILLIS, System.currentTimeMillis());
							 duration.put(TDT_DURATION_END_TIME, new Timestamp(duration.getLong("EndTimeMillis")));
							 long diff = duration.getLong(TDT_DURATION_END_TIME_MILLIS) - duration.getLong(TDT_DURATION_START_TIME_MILLIS);
							 duration.put(TDT_DURATION_SEGMENT_DURATION, diff);
							 totalDuration += diff;
						 } else {
							 totalDuration += duration.getLong(TDT_DURATION_SEGMENT_DURATION);
						 }
					}					
					durationLabel.setValue(getFormattedTime(totalDuration));
					line.add(durationLabel);
				}
				durationLabelMap.put(id, durationLabel);
				
				Button startButton = new Button();
				startButton.setAttribute("S_TimesheetEntry_ID", id);
				startButton.setIconSclass("z-icon-Play");
				startButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onStart(id);
					}
					
				});
				line.add(startButton);
				startButtonMap.put(id, startButton);
				
				Button pauseButton = new Button();
				pauseButton.setAttribute("S_TimesheetEntry_ID", id);
				pauseButton.setIconSclass("z-icon-Pause");
				pauseButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onPause(id);
					}
					
				});
				line.add(pauseButton);
				pauseButtonMap.put(id, pauseButton);
				
				Button stopButton = new Button();
				stopButton.setAttribute("S_TimesheetEntry_ID", id);
				stopButton.setIconSclass("z-icon-Stop");
				stopButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onStop(id, event.getTarget());
					}
					
				});
				line.add(stopButton);
				stopButtonMap.put(id, stopButton);
				
				Button deleteButton = new Button();
				deleteButton.setAttribute("S_TimesheetEntry_ID", id);
				deleteButton.setIconSclass("z-icon-XMark");
				deleteButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onDelete(id);
					}
					
				});
				line.add(deleteButton);
				deleteButtonMap.put(id, deleteButton);
				
				Button addButton = new Button();
				addButton.setAttribute("S_TimesheetEntry_ID", id);
				addButton.setIconSclass("z-icon-plus");
				addButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onAdd(id);
					}
					
				});
				line.add(addButton);
				addButtonMap.put(id, addButton);

				
				// set buttons state based on status
				if (jo != null) {
					String status = jo.getString(TDT_STATUS);
					if (status.equals(TDT_STATUS_STARTED)) {
//						if (isInit) {
//							//Run a cleanup - it was left running
//							//Do cleanup on onStart
//							pauseButton.setEnabled(false);
//						} else {
							startButton.setEnabled(false);
//						}
					} else if (status.equals(TDT_STATUS_PAUSED)) {
						pauseButton.setEnabled(false);
					} else {
						startButton.setEnabled(false);
						pauseButton.setEnabled(false);
						stopButton.setEnabled(false);
						deleteButton.setEnabled(false);
					}
					
					TimerState state = new TimerState();
					state.status = status;
					state.totalDuration = jo.getLong(TDT_TOTAL_DURATION);
					// window was closed without properly setting timer - assume it's been running
					// even window is closed
					state.latestStartTimeMillis = jo.getLong(TDT_LATEST_START_TIME_MILLIS);
					if (status.equals(TDT_STATUS_STARTED)) {
						state.totalDuration = state.totalDuration  + System.currentTimeMillis() - state.latestStartTimeMillis;
						durationLabel.setValue(getFormattedTime(state.totalDuration));						
						durationLabel.setSclass("duration-label active-timer");
					}
					timerStateMap.put(id, state);
				} else {
					pauseButton.setEnabled(false);
					stopButton.setEnabled(false);
				}
				
				data.add(line);
			}
		} catch (SQLException e) {
			throw new DBException(e, sql.toString());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(new WTableModelListener() {
			
			@Override
			public void tableChanged(WTableModelEvent event) {
				int rownum = event.getFirstRow();
				if (event.getColumn() == 6) {
					autoSaveRow(rownum);
					
					Button btnStart = (Button) timerTable.getValueAt(rownum, 9);
					int id =  (int) btnStart.getAttribute("S_TimesheetEntry_ID");
					TimerState state = timerStateMap.get(id);
					if (state != null) {
						Label durationLabel = durationLabelMap.get(id);
						long duration = System.currentTimeMillis() - state.latestStartTimeMillis + state.totalDuration;
//					log.severe(String.format("current milli=%s latest start milli=%s"
//							, System.currentTimeMillis(), state.latestStartTimeMillis ));
						durationLabel.setValue(getFormattedTime(duration));
					}
				}
			}
		});
		timerTable.setData(model, getTimerColumnNames());
		setTimerColumnClass(timerTable);
		timerInfo.setText(timerTable.getRowCount() + " result(s)");
		
		//disable sorting as it resets the timer
		ListitemRenderer<Object> render = timerTable.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer infoRenderer = (WListItemRenderer) render;
			for (int i=0; i < infoRenderer.getHeaders().size(); i++ ) {
				infoRenderer.getHeaders().get(i).setSort("none");
			}

		}

			
		timerTable.repaint();
	}

	private void refreshPastEntries() {
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT te.S_TimesheetEntry_ID, CONCAT_WS(' ', bp.Name, pj.Value, pj.name) as ProjectValue, pt.Name TaskName, pt.Description TaskDescription, bp.Name BPName, ");
		sql.append("te.C_Activity_ID, a.Name ActivityName, te.StartDate, COALESCE(te.Comments,'') Notes, te.QtyEntered ");
		sql.append("FROM S_TimesheetEntry te ");
		sql.append("INNER JOIN C_Project pj ON (pj.C_Project_ID = te.C_Project_ID) ");
		sql.append("LEFT OUTER JOIN C_ProjectTask pt ON (pt.C_ProjectTask_ID = te.C_ProjectTask_ID) ");
		sql.append("INNER JOIN C_BPartner bp ON (bp.C_BPartner_ID = pj.C_BPartner_ID) ");
		sql.append("LEFT JOIN C_Activity a ON (a.C_Activity_ID = te.C_Activity_ID) ");
		sql.append("WHERE te.IsActive='Y' AND te.Processed='N' AND te.EndDate IS NOT NULL ");
		sql.append("AND te.C_BPartner_ID = ? ");
		sql.append("ORDER BY te.EndDate DESC");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			DB.setParameters(pstmt, new Object[]{user.getC_BPartner_ID()});
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("S_TimesheetEntry_ID");
				Vector<Object> line = new Vector<Object>();
				line.add(rs.getString("ProjectValue"));
				line.add(rs.getString("TaskName"));
				line.add(rs.getString("TaskDescription"));
				line.add(rs.getString("BPName"));
				
				Combobox activity = (Combobox) activityField.clone();
				activity.setAttribute("S_TimesheetEntry_ID", id);
				if (rs.getInt("C_Activity_ID") > 0)
					activity.setValue(rs.getInt("C_Activity_ID"));
				
				activity.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						int row = pastEntriesRenderer.getRowPosition(event.getTarget());
						enableSaveButton(row);
					}
					
				});
				
				line.add(activity);
				
//				line.add(startDateTimeFormat.format(rs.getTimestamp("StartDate")));
				line.add(rs.getTimestamp("StartDate"));
				line.add(rs.getString("Notes"));
				
				Button editNotesButton = new Button();
				editNotesButton.setAttribute("S_TimesheetEntry_ID", id);
				editNotesButton.setIconSclass("z-icon-pencil");
				editNotesButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onEditPastNotes(event.getTarget());
					}
					
				});
				line.add(editNotesButton);
				
				line.add(rs.getBigDecimal("QtyEntered"));
				
				Button saveButton = new Button();
				saveButton.setEnabled(false);
				saveButton.setAttribute("S_TimesheetEntry_ID", id);
				saveButton.setIconSclass("z-icon-save");
				saveButton.addActionListener(new EventListener<Event>() {
					
					@Override
					public void onEvent(Event event) throws Exception {
						onSavePastNotes(id, event.getTarget());
					}
					
				});
				line.add(saveButton);
				
				data.add(line);
			}
		} catch (SQLException e) {
			throw new DBException(e, sql.toString());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(new WTableModelListener() {
			
			@Override
			public void tableChanged(WTableModelEvent event) {
				int rownum = event.getFirstRow();
				if (rownum >= 0) {
					enableSaveButton(rownum);
				}
				
			}
		});
		pastEntriesTable.setData(model, getPastEntriesColumnNames());
		setPastEntriesColumnClass(pastEntriesTable);
		pastEntriesInfo.setText(pastEntriesTable.getRowCount() + " result(s)");
	}

	private void onCreateTimer(int projectID, int phaseID, int taskID, int resourceID) {
		MProject project = new MProject(Env.getCtx(), projectID, null);
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), 0, null);
		te.setAD_Org_ID(project.getAD_Org_ID());
		te.setC_ProjectTask_ID(taskID);
		te.setC_ProjectPhase_ID(phaseID);
		te.setC_Project_ID(projectID);
		te.setS_Resource_ID(resourceID);
		te.setM_Product_ID(MProduct.forS_Resource_ID(Env.getCtx(), resourceID, null).get_ID());
		te.setC_BPartner_ID(user.getC_BPartner_ID());
		te.setIsBillable(true);
		te.setDateAcct(new Timestamp(System.currentTimeMillis()));
		te.saveEx();
		
		refreshTimers();
	}
	
	private void onStart(int id) {
		Button startButton = startButtonMap.get(id);
		Button pauseButton = pauseButtonMap.get(id);
		Button stopButton = stopButtonMap.get(id);
		startButton.setEnabled(false);
		pauseButton.setEnabled(true);
		stopButton.setEnabled(true);

		
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
		JSONObject jo = te.getTimerData();
		if ( jo == null )
		{
			jo = new JSONObject();
			jo.put(TDT_TOTAL_DURATION, 0);
		} else {
			//make a copy of the old value, otherwise PO.save will not detect the changes
			jo = new JSONObject(te.getTimerData().toString());
		}
		
		JSONArray ja = null;
		if (jo.isNull(TDT_ARRAY_DURATIONS))
			ja = new JSONArray();
		else
			ja = jo.getJSONArray(TDT_ARRAY_DURATIONS);
		
		//remove unterminated segment - possibly left running
		int deleteID = -1;
		for (int i = 0; i < ja.length(); i++) {
			 JSONObject duration = ja.getJSONObject(i);
			 if (duration.isNull(TDT_DURATION_END_TIME)) {
				 deleteID = i;
				 break;
			 }
		}
		if (deleteID >= 0) {
			ja.remove(deleteID);
		}
		
		JSONObject duration = new JSONObject();
		duration.put(TDT_DURATION_START_TIME_MILLIS, System.currentTimeMillis());
		duration.put(TDT_DURATION_START_TIME, new Timestamp(duration.getLong(TDT_DURATION_START_TIME_MILLIS)));
		ja.put(duration);
		jo.put(TDT_ARRAY_DURATIONS, ja);
		jo.put(TDT_LATEST_START_TIME_MILLIS, duration.getLong(TDT_DURATION_START_TIME_MILLIS));
		jo.put(TDT_LATEST_START_TIME, duration.get(TDT_DURATION_START_TIME));
		jo.put(TDT_STATUS, TDT_STATUS_STARTED);
		
		if (te.getStartDate() == null) {
			te.setStartDate(new Timestamp(duration.getLong(TDT_DURATION_START_TIME_MILLIS)));
			te.setDateAcct(TimeUtil.addDays(te.getStartDate(), 0));
			Label startLabel = startLabelMap.get(id);
			startLabel.setValue(DisplayType.getTimestampFormat_Default().format(te.getStartDate()));
		}
		te.setTimerData(jo);
		te.saveEx();
		
		TimerState state;
		if (timerStateMap.containsKey(id))
			state = timerStateMap.get(id);
		else
			state = new TimerState();
		state.status = TDT_STATUS_STARTED;
		state.totalDuration = jo.getLong(TDT_TOTAL_DURATION);
		state.latestStartTimeMillis = jo.getLong(TDT_LATEST_START_TIME_MILLIS);
//		log.severe(String.format("START current milli=%s latest start milli=%s"
//				, System.currentTimeMillis(), state.latestStartTimeMillis ));
		timerStateMap.put(id, state);
		
		Label durationLabel = durationLabelMap.get(id);
		durationLabel.setSclass("duration-label active-timer");
	}
	
	private void onPause(int id) {
		Button startButton = startButtonMap.get(id);
		Button pauseButton = pauseButtonMap.get(id);
		Button stopButton = stopButtonMap.get(id);
		startButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(true);
		
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
		JSONObject jo = new JSONObject(te.getTimerData().toString());
		JSONArray ja = jo.getJSONArray(TDT_ARRAY_DURATIONS);
		long totalDuration = 0;
		for (int i = 0; i < ja.length(); i++) {
			 JSONObject duration = ja.getJSONObject(i);
			 if (duration.isNull(TDT_DURATION_END_TIME)) {
				 long endTime = System.currentTimeMillis();
				 duration.put(TDT_DURATION_END_TIME_MILLIS, endTime );
				 duration.put(TDT_DURATION_END_TIME, new Timestamp(endTime));
				 long diff = endTime - duration.getLong(TDT_DURATION_START_TIME_MILLIS);
				 duration.put(TDT_DURATION_SEGMENT_DURATION, diff);
				 totalDuration += diff;
			 } else {
				 totalDuration += duration.getLong(TDT_DURATION_SEGMENT_DURATION);
			 }
		}
		jo.put(TDT_TOTAL_DURATION, totalDuration);
		jo.put(TDT_STATUS, TDT_STATUS_PAUSED);
		
		te.setTimerData(jo);
		te.saveEx();
		
		TimerState state;
		if (timerStateMap.containsKey(id)) {
			state = timerStateMap.get(id);
		} else {
			state = new TimerState();
			
		}
		state.status = TDT_STATUS_PAUSED;
		state.totalDuration = totalDuration;
		
		timerStateMap.put(id, state);
		
		Label durationLabel = durationLabelMap.get(id);
		durationLabel.setSclass("duration-label");
		durationLabel.setValue(getFormattedTime(totalDuration));
	}

	private void onStop(int id, Component source) {
		int row = timerRenderer.getRowPosition(source);
		Combobox activityField = (Combobox) timerTable.getValueAt(row, 4); // activity
		String notes = timerTable.getValueAt(row, 6).toString(); // notes
		
		int activityID = 0;
		if (activityField.getSelectedItem() != null) {
			activityID = activityField.getSelectedItem().getValue();
		}
		
		String errorMessage = "";
		if (activityID <= 0 && notes.isEmpty()) {
			errorMessage = "@C_Activity_ID@, @Note@ ";
		} else if (notes.isEmpty()) {
			errorMessage = "@Note@";
		} else if (activityID <= 0) {
			errorMessage = "@C_Activity_ID@ ";
		}
			
		if (errorMessage.isEmpty()) {
			MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
			JSONObject jo = new JSONObject(te.getTimerData().toString());

			JSONArray ja = jo.getJSONArray(TDT_ARRAY_DURATIONS);
			long totalDuration = 0;
			for (int i = 0; i < ja.length(); i++) {
				 JSONObject duration = ja.getJSONObject(i);
				 if (duration.isNull("EndTime")) {
					 duration.put("EndTimeMillis", System.currentTimeMillis());
					 duration.put("EndTime", new Timestamp(duration.getLong("EndTimeMillis")));
					 long diff = duration.getLong("EndTimeMillis") - duration.getLong(TDT_DURATION_START_TIME_MILLIS);
					 duration.put(TDT_DURATION_SEGMENT_DURATION, diff);
					 totalDuration += diff;
				 } else {
					 totalDuration += duration.getLong(TDT_DURATION_SEGMENT_DURATION);
				 }
			}
			jo.put(TDT_TOTAL_DURATION, totalDuration);
			jo.put(TDT_STATUS, TDT_STATUS_ENDED);
			
			Duration duration = Duration.ofMillis(totalDuration);
			long seconds = duration.getSeconds();
			BigDecimal qtyMin = BigDecimal.valueOf(seconds / 60f);
			qtyMin = qtyMin.setScale(0, RoundingMode.HALF_UP);
			te.setQtyEntered(qtyMin);
			te.setTimerData(jo);
			te.setEndDate(new Timestamp(System.currentTimeMillis()));
			te.setC_Activity_ID(activityID);
			te.setComments(notes);
			te.saveEx();
			
			refreshTimers();
			refreshPastEntries();
		} else {
			FDialog.warn(m_WindowNo, Msg.parseTranslation(Env.getCtx(), "@FillMandatory@"  + errorMessage));
		}
	}
	
	private void onEditNotes(int id, Component source) {
		int row = timerRenderer.getRowPosition(source);
		String notes = timerTable.getValueAt(row, 6).toString(); // notes
		
		final WTextEditorDialog dialog = new WTextEditorDialog("Edit Notes", notes, true, 0, false);
		dialog.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		dialog.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (!dialog.isCancelled()) {
					timerTable.setValueAt(dialog.getText(), row, 6);
				}
			}
		});
		SessionManager.getAppDesktop().showWindow(dialog);
	}
	
	private void onEditPastNotes(Component source) {
		int row = pastEntriesRenderer.getRowPosition(source);
		String notes = pastEntriesTable.getValueAt(row, 6).toString(); // notes
		
		final WTextEditorDialog dialog = new WTextEditorDialog("Edit Notes", notes, true, 0, false);
		dialog.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		dialog.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (!dialog.isCancelled()) {
					pastEntriesTable.setValueAt(dialog.getText(), row, 6);
				}
			}
		});
		SessionManager.getAppDesktop().showWindow(dialog);
	}
	
	private void onSavePastNotes(int id, Component source) {
		int row = pastEntriesRenderer.getRowPosition(source);
		Combobox activityField = (Combobox) pastEntriesTable.getValueAt(row, 4); // activity
		int activityID = 0;
		if (activityField.getSelectedItem() != null) {
			activityID = activityField.getSelectedItem().getValue();
		}
		
		String notes = pastEntriesTable.getValueAt(row, 6).toString(); // notes
		BigDecimal qtyEntered = (BigDecimal) pastEntriesTable.getValueAt(row, 8);
		Button btnSave = (Button) pastEntriesTable.getValueAt(row, 9);
		Timestamp startDate =  (Timestamp) pastEntriesTable.getValueAt(row, 5);
		
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
		te.setC_Activity_ID(activityID);
		te.setQtyEntered(qtyEntered);
		te.setComments(notes);
		te.setStartDate(startDate);
		te.saveEx();
		
		btnSave.setEnabled(false);
	}
	
	/**
	 * save the current timer record 
	 */
	private void persistTimer(boolean stopAll)
	{
		for (Integer id : stopButtonMap.keySet()) {
			MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
			if (te.getTimerData() == null) {
				//nothing to persist
				continue;
			}
			JSONObject jo = new JSONObject(te.getTimerData().toString());
			String status = jo.getString(TDT_STATUS);
			if (status.equals(TDT_STATUS_STARTED)) {
				JSONArray ja = jo.getJSONArray(TDT_ARRAY_DURATIONS);
				long totalDuration = 0;
				for (int i = 0; i < ja.length(); i++) {
					JSONObject duration = ja.getJSONObject(i);
					if (duration.isNull(TDT_DURATION_END_TIME)) {
						duration.put(TDT_DURATION_END_TIME_MILLIS, System.currentTimeMillis());
						duration.put(TDT_DURATION_END_TIME, new Timestamp(duration.getLong("EndTimeMillis")));
						long diff = duration.getLong(TDT_DURATION_END_TIME_MILLIS) - duration.getLong(TDT_DURATION_START_TIME_MILLIS);
						duration.put(TDT_DURATION_SEGMENT_DURATION, diff);
						totalDuration += diff;
					} else {
						totalDuration += duration.getLong(TDT_DURATION_SEGMENT_DURATION);
					}
				}
				
				jo.put(TDT_TOTAL_DURATION, totalDuration);
				if (!stopAll) {
					JSONObject duration = new JSONObject();
					duration.put(TDT_DURATION_START_TIME_MILLIS, System.currentTimeMillis());
					duration.put(TDT_DURATION_START_TIME, new Timestamp(duration.getLong(TDT_DURATION_START_TIME_MILLIS)));
					ja.put(duration);
					jo.put(TDT_LATEST_START_TIME_MILLIS, duration.getLong(TDT_DURATION_START_TIME_MILLIS));
					jo.put(TDT_LATEST_START_TIME, duration.get(TDT_DURATION_START_TIME));
					jo.put(TDT_STATUS, TDT_STATUS_STARTED);
					
				} else {
					jo.put(TDT_STATUS, TDT_STATUS_PAUSED);
				}
				
				te.setTimerData(jo);
				te.saveEx();
			}
		}
	}

	private void onDelete(int id) {
		FDialog.ask(m_WindowNo, this, "DeleteRecord?", new Callback<Boolean>() {

			@Override
			public void onCallback(Boolean result) 
			{
				if (result)
				{
					MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
					te.deleteEx(true);
					refreshTimers();
				}					
			}
		});
	}
	
	private void onAdd(int id) {
//		int row = timerRenderer.getRowPosition(source);
//		String notes = timerTable.getValueAt(row, 6).toString(); // notes
		
		final WNumEditorDialog dialog = new WNumEditorDialog("Add time in Minutes", null, true);
		dialog.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		dialog.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (!dialog.isCancelled()) {
//					timerTable.setValueAt(dialog.getText(), row, 6);
					
					MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
					JSONObject jo;
					JSONArray ja;
					Timestamp endtime = TimeUtil.getDay(null);
					long addedTime = dialog.getNumber().multiply(new BigDecimal(1000*60)).longValue();
					
					if (te.getTimerData() != null) {
						jo = new JSONObject(te.getTimerData().toString());
						ja = jo.getJSONArray(TDT_ARRAY_DURATIONS);
					} else {
						jo = new JSONObject();
						jo.put(TDT_LATEST_START_TIME_MILLIS, endtime.getTime());
						jo.put(TDT_STATUS, TDT_STATUS_PAUSED);
						ja = new JSONArray();
						jo.put(TDT_ARRAY_DURATIONS, ja);
					}
					JSONObject addDuration = new JSONObject();
					addDuration.put(TDT_DURATION_END_TIME, endtime );
					addDuration.put(TDT_DURATION_SEGMENT_DURATION, addedTime);
					ja.put(addDuration);
					
					TimerState state;
					long totalDuration;
					if (timerStateMap.containsKey(id)) {
						state = timerStateMap.get(id);
						if (state.status.equals(TDT_STATUS_STARTED)) {
							totalDuration = System.currentTimeMillis() - state.latestStartTimeMillis + state.totalDuration + addedTime;
							
							log.severe(String.format("Total Duration=%s state.totalduration=%s addedtime=%s"
							, totalDuration, state.totalDuration, addedTime ));							
							
						} else {
							totalDuration = state.totalDuration + addedTime;
						}
							
					}
					else {
						totalDuration = addedTime;
						state = new TimerState();
						state.latestStartTimeMillis = endtime.getTime();
						state.status = TDT_STATUS_PAUSED;
						timerStateMap.put(id, state);
					}
					
					state.totalDuration = totalDuration;

					jo.put(TDT_TOTAL_DURATION, totalDuration);
					te.setTimerData(jo);
					te.saveEx();

					Label durationLabel = durationLabelMap.get(id);
					durationLabel.setValue(getFormattedTime(totalDuration));
				}
			}
		});
		SessionManager.getAppDesktop().showWindow(dialog);

	}
	
	protected void customize(WListbox table, int customFormID, int customTabID)
	{
		ListitemRenderer<Object> render = table.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer renderer = (WListItemRenderer) render;
			String customize = renderer.getColumnCustomization();
			boolean userPreference = false;
			MRole currRole = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
			if (currRole.isCanSaveColumnWidthEveryone())
			{
				FDialog.ask(getWindowNo(), this, 
						"This change apply to everyone who has not set their own preference.", 
						new Callback<Boolean>() {

					@Override
					public void onCallback(Boolean userPreference) {
						customize0(customFormID, customTabID, customize, userPreference);
					}
					
				});
			}
			else
			{
				customize0(customFormID, customTabID, customize, userPreference);
			}
			
		}
	}

	private void customize0(int customFormID, int customTabID, String customize, boolean userPreference) {
		boolean ok = MTabCustomization.saveInfoData(Env.getCtx(), customFormID, customTabID, Env.getAD_User_ID(Env.getCtx()),
				customize, null);
		if (ok && userPreference && Env.getAD_User_ID(Env.getCtx()) != MTabCustomization.SUPERUSER)
			ok = MTabCustomization.saveInfoData(Env.getCtx(), customFormID, customTabID, MTabCustomization.SUPERUSER,
					customize, null);
		
		if (ok)
			FDialog.info(m_WindowNo, null, "Preference save successfully.");
		else
			FDialog.info(m_WindowNo, null, "Preference cannot be saved");
	}
	
	protected void initTableColumnWidth(WListbox table, int customFormID, int customTabID)
	{
		ListitemRenderer<Object> render = table.getItemRenderer();
		if (render instanceof WListItemRenderer)
		{
			WListItemRenderer infoRenderer = (WListItemRenderer) render;

			MTabCustomization tabCustom = MTabCustomization.getInfo(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()),
					customFormID, customTabID, null);

			if (tabCustom == null)
				tabCustom = MTabCustomization.getInfo(Env.getCtx(), MTabCustomization.SUPERUSER, customFormID, customTabID, null);

			if (tabCustom != null)
			{
				infoRenderer.setHeaderColumnWidth(tabCustom.getcustom());
			}
			//Custom hide unwanted column
			//TODO - proper configuration when project uses task / phase
			infoRenderer.getHeaders().get(1).setVisible(false);
			infoRenderer.getHeaders().get(2).setVisible(false);
			infoRenderer.getHeaders().get(3).setVisible(false);
//			infoRenderer.getHeaders().get(0).setWidth("400px");
			table.repaint();
		}
	}

	private String getFormattedTime(long millis) {
		Duration duration = Duration.ofMillis(millis);
		long seconds = duration.getSeconds();
		long HH = seconds / 3600;
		long MM = (seconds % 3600) / 60;
		long SS = seconds % 60;
		String timeInHHMMSS = String.format("%02d:%02d:%02d", HH, MM, SS);
		return timeInHHMMSS;
	}
	
	@Override
	public void valueChange(ValueChangeEvent e) {
		String name = e.getPropertyName();
		Object value = e.getNewValue();
		log.config(name + "=" + value);
		if (value == null)
			value = Integer.valueOf(0);

		if (name.equals("AD_User_ID"))
		{
			userID = (Integer) value;
			if ( userID >= 0 )
				user  = MUser.get(Env.getCtx(), userID);
		}
		else if (name.equals("C_Project_ID"))
			projectID = (Integer) value;
		else if (name.equals("C_BPartner_ID"))
			bpartnerID = (Integer) value;
		
		refreshTasks();
	}
	
	private void enableSaveButton(int rownum)
	{
		Button btnSave = (Button) pastEntriesTable.getValueAt(rownum, 9);
		btnSave.setEnabled(true);
	}
	
	private void autoSaveRow(int row)
	{
		Button btnStart = (Button) timerTable.getValueAt(row, 9);
		
		int id =  (int) btnStart.getAttribute("S_TimesheetEntry_ID");
		String notes = timerTable.getValueAt(row, 6).toString(); // notes
		int activityID = 0;
		Combobox activityField = (Combobox) timerTable.getValueAt(row, 4); // activity
		if (activityField.getSelectedItem() != null) {
			activityID = activityField.getSelectedItem().getValue();
		}
			
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
		te.setC_Activity_ID(activityID);
		te.setComments(notes);
		te.saveEx();
	}
	/**
	 * Called by org.adempiere.webui.panel.ADForm.openForm(int)
	 * @return
	 */
	public ADForm getForm() {
		return form;
	}
	
	class TimerState {
		String status;
		Long totalDuration;
		Long latestStartTimeMillis;
		
		public String toString() {
			return "TimerState[Status=" + status + ", TotalDuration=" + totalDuration + ", LatestStartTimeMillis=" + latestStartTimeMillis + "]";
		}
	}

}
