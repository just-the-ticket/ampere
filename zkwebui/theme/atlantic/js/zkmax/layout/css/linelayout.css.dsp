<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-linelayout {
  height: 100%;
  width: 100%;
}
.z-linelayout-first {
  flex: 1 0 0;
  display: flex;
  justify-content: space-around;
  align-items: flex-end;
  overflow: hidden;
}
.z-linelayout-cave {
  display: flex;
  justify-content: space-around;
  align-items: flex-end;
  position: relative;
}
.z-linelayout-last {
  flex: 1 0 0;
  display: flex;
  justify-content: space-around;
  align-items: flex-start;
  overflow: hidden;
}
.z-linelayout-line {
  background: #E3E3E3;
  position: absolute;
  z-index: 99;
}
.z-linelayout-vertical {
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
}
.z-linelayout-vertical .z-linelayout-first {
  flex-direction: column;
}
.z-linelayout-vertical .z-linelayout-cave {
  padding: 0 24px;
  flex-direction: column;
}
.z-linelayout-vertical .z-linelayout-last {
  flex-direction: column;
}
.z-linelayout-vertical .z-linelayout-line {
  height: 100%;
  left: calc(50% - 5px / 2);
  width: 5px;
}
.z-linelayout-vertical .z-lineitem {
  flex-direction: column;
}
.z-linelayout-horizontal {
  display: flex;
  flex-direction: column;
}
.z-linelayout-horizontal .z-linelayout-first {
  flex-direction: row;
}
.z-linelayout-horizontal .z-linelayout-cave {
  padding: 24px 0;
}
.z-linelayout-horizontal .z-linelayout-last {
  flex-direction: row;
}
.z-linelayout-horizontal .z-linelayout-line {
  width: 100%;
  top: calc(50% - 5px / 2);
  height: 5px;
}
.z-lineitem {
  flex: 1 0 0;
  display: flex;
  overflow: hidden;
}
.z-lineitem > * {
  flex-shrink: 0;
  margin: auto;
}
.z-lineitem-stretch {
  width: 100%;
  height: 100%;
}
.z-lineitem-point {
  border: 0;
  border-radius: 50%;
  overflow: hidden;
  box-shadow: none;
  height: 28px;
  width: 28px;
  font-size: 20px;
  color: #FFFFFF;
  z-index: 100;
  background: rgba(50, 111, 112, 0.8);
}
.z-lineitem-point-hidden {
  visibility: hidden;
}
.z-lineitem-point-image {
  vertical-align: baseline;
  display: inline-flex;
  height: inherit;
  width: inherit;
}
.z-lineitem-point-icon {
  height: inherit;
  width: inherit;
}
.z-lineitem-point-inner {
  height: inherit;
  width: inherit;
  background-size: cover;
  line-height: 28px;
  text-align: center;
}
.z-lineitem-point-inner:before {
  position: relative;
  top: 0;
  left: 0;
}
