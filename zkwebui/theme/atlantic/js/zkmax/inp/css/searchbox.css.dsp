<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-searchbox {
  display: inline-block;
  position: relative;
  height: 32px;
  width: 180px;
  border: 1px solid #326F70;
  border-radius: 0;
  color: #000000;
  background: #FFFFFF;
  cursor: pointer;
}
.z-searchbox:hover {
  border-color: rgba(50, 111, 112, 0.7);
  background: #FFFFFF;
}
.z-searchbox-focus,
.z-searchbox-open {
  border-color: rgba(50, 111, 112, 0.9);
}
.z-searchbox:active {
  border-color: #668788;
  background: #668788;
  color: #FFFFFF;
}
.z-searchbox:active .z-searchbox-placeholder {
  color: #FFFFFF;
}
.z-searchbox:active .z-searchbox-icon {
  color: #FFFFFF;
}
.z-searchbox[disabled] {
  color: #ACACAC;
  background: rgba(102, 135, 136, 0.35);
  cursor: default;
  border-color: #326F70;
}
.z-searchbox[disabled]-icon {
  color: #ACACAC;
}
.z-searchbox[disabled] .z-searchbox-icon {
  color: #ACACAC;
}
.z-searchbox:before {
  content: '';
  display: inline-block;
  vertical-align: middle;
  height: 100%;
}
.z-searchbox-label {
  user-select: none;
  display: inline-block;
  width: 100%;
  height: 100%;
  line-height: calc(32px - 2px);
  padding: 0 5px;
  padding-right: 34px;
  vertical-align: middle;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.z-searchbox-placeholder {
  user-select: none;
  display: none;
  width: 100%;
  height: 100%;
  line-height: calc(32px - 2px);
  padding: 0 5px;
  padding-right: 18px;
  vertical-align: middle;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #ACACAC;
}
.z-searchbox-icon {
  color: #000000;
  font-size: 114%;
  position: absolute;
  right: 10px;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.z-searchbox-clear {
  right: 124%;
}
.z-searchbox-popup {
  position: absolute;
  z-index: 1000;
  border: 1px solid #E3E3E3;
  border-radius: 0;
  background: #FFFFFF;
  padding: 0;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  color: #000000;
}
.z-searchbox-shadow {
  box-shadow: none;
}
.z-searchbox .z-searchbox-popup {
  display: none;
}
.z-searchbox-search {
  height: 32px;
  width: calc(100% - 16px);
  padding: 0 5px;
  border: 1px solid #326F70;
  border-radius: 0;
  background: #FFFFFF;
  margin: 4px 8px;
}
.z-searchbox-search::placeholder {
  color: #ACACAC;
  opacity: 1;
}
.z-searchbox-search::-webkit-input-placeholder {
  color: #ACACAC;
  opacity: 1;
}
.z-searchbox-search::-moz-placeholder {
  color: #ACACAC;
  opacity: 1;
}
.z-searchbox-search:-ms-input-placeholder {
  color: #ACACAC;
  opacity: 1;
}
.z-searchbox-search:focus {
  border-color: #668788;
}
.z-searchbox-cave {
  margin: 0;
  padding: 0;
  max-height: 350px;
  overflow: auto;
}
.z-searchbox-item {
  height: 32px;
  white-space: nowrap;
  list-style: none;
  display: block;
  padding: 4px 8px;
  cursor: pointer;
  vertical-align: middle;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.z-searchbox-item:before {
  content: '';
  display: inline-block;
  vertical-align: middle;
  height: 100%;
}
.z-searchbox-item.z-searchbox-selected {
  background: rgba(102, 135, 136, 0.2);
  color: #000000;
}
.z-searchbox-item:hover {
  background: rgba(50, 111, 112, 0.7);
  color: #FFFFFF;
}
.z-searchbox-item.z-searchbox-active {
  background: #668788;
  color: #FFFFFF;
}
.z-searchbox-item-check {
  display: inline-block;
  vertical-align: middle;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  border-radius: 0;
  width: 18px;
  height: 18px;
  margin-right: 8px;
}
.z-searchbox-item.z-searchbox-selected > .z-searchbox-item-check {
  color: #668788;
  background: #FFFFFF;
  display: inline-block;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  font-size: inherit;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-rendering: auto;
  transform: translate(0, 0);
}
.z-searchbox-item.z-searchbox-selected > .z-searchbox-item-check::before {
  content: "\f00c";
}
