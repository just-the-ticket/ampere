<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-cascader {
  display: inline-block;
  position: relative;
  height: 32px;
  width: 180px;
  border: 1px solid #E3E3E3;
  color: #000000;
  background: #FFFFFF;
  cursor: pointer;
}
.z-cascader:active {
  border-color: #668788;
  background: #668788;
  color: #FFFFFF;
}
.z-cascader:hover {
  border-color: rgba(50, 111, 112, 0.7);
}
.z-cascader-focus,
.z-cascader-open:hover {
  border-color: rgba(50, 111, 112, 0.9);
}
.z-cascader-disabled {
  color: #ACACAC !important;
  background-color: rgba(102, 135, 136, 0.35) !important;
  cursor: default !important;
}
.z-cascader-disabled:focus,
.z-cascader-disabled:hover {
  border-color: #E3E3E3;
}
.z-cascader-open {
  border: 1px solid rgba(50, 111, 112, 0.9);
}
.z-cascader:before {
  content: '';
  display: inline-block;
  vertical-align: middle;
  height: 100%;
}
.z-cascader-label {
  user-select: none;
  display: inline-block;
  width: 100%;
  height: 100%;
  padding: 0 8px;
  padding-right: 18px;
  vertical-align: middle;
  line-height: 30px;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.z-cascader-placeholder {
  user-select: none;
  display: none;
  width: 100%;
  height: 100%;
  padding: 0 8px;
  padding-right: 18px;
  vertical-align: middle;
  line-height: 30px;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #ACACAC;
}
.z-cascader-icon {
  color: #ACACAC;
  font-size: 114%;
  position: absolute;
  right: 10px;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.z-cascader-popup {
  min-height: 32px;
  position: absolute;
  z-index: 1000;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  color: #000000;
  white-space: nowrap;
}
.z-cascader-shadow {
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16), 0 2px 4px 0 rgba(0, 0, 0, 0.24);
}
.z-cascader .z-cascader-popup {
  display: none;
}
.z-cascader-cave {
  margin: 0;
  padding: 0px;
  border-left: 1px solid #d9d9d9;
  max-height: 350px;
  overflow: auto;
  display: inline-block;
  vertical-align: top;
}
.z-cascader-cave:first-child {
  border-left: 0;
}
.z-cascader-item {
  white-space: nowrap;
  list-style: none;
  display: block;
  padding: 0px 28px 0px 5px;
  cursor: pointer;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  user-select: none;
  min-width: 148px;
  height: 32px;
  line-height: 32px;
  position: relative;
}
.z-cascader-item > label {
  display: block;
}
.z-cascader-item .z-cascader-icon {
  top: 0;
  right: 5px;
  line-height: 32px;
  transform: none;
  margin-left: 4px;
}
.z-cascader-item.z-cascader-selected {
  background-color: rgba(102, 135, 136, 0.2);
}
.z-cascader-item.z-cascader-active,
.z-cascader-item:hover {
  background-color: rgba(50, 111, 112, 0.7);
  color: #FFFFFF;
}
.z-cascader-item.z-cascader-active .z-cascader-icon,
.z-cascader-item:hover .z-cascader-icon {
  color: #FFFFFF;
}
