<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-popup {
  position: absolute;
  top: 0;
  left: 0;
}
.z-popup-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  height: 100%;
  padding: 8px;
  line-height: normal;
  background: rgba(50, 111, 112, 0.8);
}
.z-notification {
  color: #FFFFFF;
  position: absolute;
  top: 0;
  left: 0;
}
.z-notification-left,
.z-notification-right,
.z-notification-up,
.z-notification-down {
  border: 10px solid transparent;
}
.z-notification-left {
  border-right-color: #333333;
}
.z-notification-right {
  border-left-color: #333333;
}
.z-notification-up {
  border-down-color: #333333;
}
.z-notification-down {
  border-top-color: #333333;
}
.z-notification-info .z-notification-content {
  background: #59A755;
}
.z-notification-info .z-notification-left {
  border-right-color: #59A755;
}
.z-notification-info .z-notification-right {
  border-left-color: #59A755;
}
.z-notification-info .z-notification-up {
  border-down-color: #59A755;
}
.z-notification-info .z-notification-down {
  border-top-color: #59A755;
}
.z-notification-warning .z-notification-content {
  background: #CC6D33;
}
.z-notification-warning .z-notification-left {
  border-right-color: #CC6D33;
}
.z-notification-warning .z-notification-right {
  border-left-color: #CC6D33;
}
.z-notification-warning .z-notification-up {
  border-down-color: #CC6D33;
}
.z-notification-warning .z-notification-down {
  border-top-color: #CC6D33;
}
.z-notification-error .z-notification-content {
  background: #AE312E;
}
.z-notification-error .z-notification-left {
  border-right-color: #AE312E;
}
.z-notification-error .z-notification-right {
  border-left-color: #AE312E;
}
.z-notification-error .z-notification-up {
  border-down-color: #AE312E;
}
.z-notification-error .z-notification-down {
  border-top-color: #AE312E;
}
.z-notification-icon {
  position: absolute;
  top: 50%;
  left: 0;
  z-index: 1;
}
.z-notification-icon.z-icon-times {
  margin-top: -6px;
  left: 3px;
}
.z-notification-icon.z-icon-times-circle,
.z-notification-icon.z-icon-exclamation-circle,
.z-notification-icon.z-icon-info-circle {
  font-size: 24px;
  margin-top: -12px;
  left: 12px;
}
.z-notification-pointer + .z-notification-icon {
  left: 14px;
}
.z-notification-left + .z-notification-icon {
  left: 24px;
}
.z-notification-up + .z-notification-icon {
  margin-top: -6px;
}
.z-notification-down + .z-notification-icon {
  margin-top: -18px;
}
.z-notification-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  width: 250px;
  min-height: 130px;
  padding: 15px 18px 15px 45px;
  position: relative;
  overflow: hidden;
}
.z-notification-pointer ~ .z-notification-content {
  display: table-cell;
  width: 125px;
  height: 60px;
  min-height: 60px;
  padding: 5px 18px 5px 45px;
  vertical-align: middle;
}
.z-notification-pointer {
  display: none;
  width: 0;
  height: 0;
  border: 10px solid transparent;
  position: absolute;
  z-index: 100;
}
.z-notification-close {
  font-size: 12px;
  display: block;
  width: 12px;
  height: 12px;
  position: absolute;
  top: 8px;
  right: 8px;
  cursor: pointer;
}
.z-notification-close:hover {
  background: #FFFFFF;
}
.z-notification-pointer ~ .z-notification-close {
  top: 5px;
  right: 5px;
}
.z-notification-right ~ .z-notification-close {
  top: 5px;
  right: 17px;
}
.z-notification-up ~ .z-notification-close {
  top: 17px;
}
.z-notification-info .z-notification-close:hover {
  color: #59A755;
}
.z-notification-warning .z-notification-close:hover {
  color: #CC6D33;
}
.z-notification-error .z-notification-close:hover {
  color: #AE312E;
}
.z-toast {
  color: #FFFFFF;
  position: relative;
  will-change: opacity;
}
.z-toast-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  width: 320px;
  min-height: 51px;
  padding: 16px 40px 16px 45px;
  position: relative;
  overflow: hidden;
}
.z-toast-close {
  font-size: 114%;
  display: block;
  width: 12px;
  height: 12px;
  position: absolute;
  top: 50%;
  right: 12px;
  cursor: pointer;
  margin-top: -6px;
}
.z-toast-close:hover {
  background: #FFFFFF;
}
.z-toast-icon {
  position: absolute;
  z-index: 1;
}
.z-toast-icon.z-icon-times {
  left: 2px;
}
.z-toast-icon.z-icon-times-circle,
.z-toast-icon.z-icon-exclamation-circle,
.z-toast-icon.z-icon-info-circle {
  top: 50%;
  font-size: 24px;
  margin-top: -12px;
  left: 12px;
}
.z-toast-error .z-toast-content {
  background-color: #AE312E;
}
.z-toast-error .z-toast-close:hover {
  color: #AE312E;
}
.z-toast-info .z-toast-content {
  background-color: #59A755;
}
.z-toast-info .z-toast-close:hover {
  color: #59A755;
}
.z-toast-warning .z-toast-content {
  background-color: #CC6D33;
}
.z-toast-warning .z-toast-close:hover {
  color: #CC6D33;
}
.z-toast-position {
  display: -ms-flexbox;
  display: flex;
  position: fixed;
  -ms-flex-direction: column;
  flex-direction: column;
  z-index: 1800;
}
.z-toast-position > .z-toast {
  margin-top: 10px;
}
.z-toast-position-top-left {
  -ms-flex-direction: column-reverse;
  flex-direction: column-reverse;
  top: 0;
  left: 0;
}
.z-toast-position-top-center {
  -ms-flex-direction: column-reverse;
  flex-direction: column-reverse;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  -ms-transform: translateX(-50%);
}
.z-toast-position-top-right {
  -ms-flex-direction: column-reverse;
  flex-direction: column-reverse;
  top: 0;
  right: 0;
}
.z-toast-position-middle-left {
  top: 50%;
  transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  left: 0;
}
.z-toast-position-middle-center {
  top: 50%;
  transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  left: 50%;
  transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
}
.z-toast-position-middle-right {
  top: 50%;
  transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  right: 0;
}
.z-toast-position-bottom-left {
  bottom: 0;
  left: 0;
}
.z-toast-position-bottom-center {
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);
  -ms-transform: translateX(-50%);
}
.z-toast-position-bottom-right {
  bottom: 0;
  right: 0;
}
