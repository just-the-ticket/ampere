<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-auxhead th:first-child {
  border-left: none;
  border-left-width: 0;
}
.z-auxhead th:first-child.z-auxhead-border {
  border-left: 1px solid #FFFFFF;
}
.z-auxhead-bar {
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
}
.z-auxheader {
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
  padding: 0;
  background: #326F70;
  position: relative;
  overflow: hidden;
  white-space: nowrap;
}
.z-auxheader-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  padding: 4px 8px;
  line-height: 28px;
  overflow: hidden;
}
