CREATE OR REPLACE FUNCTION ampere.get_bomlines(p_product_id numeric, OUT bomlevel integer, OUT m_product_id numeric, OUT m_productbom_id numeric, OUT bomqty numeric, OUT m_locator_id numeric, OUT isexplode character)
 RETURNS SETOF record
 LANGUAGE plpgsql
AS $function$
BEGIN
  return query
WITH RECURSIVE rpl (bomlevel, m_product_id, m_productbom_id, bomqty, m_locator_id, isexplode) AS (
	SELECT 1, b.m_product_id, b.m_productbom_id, b.bomqty
	, p.m_locator_id
	, CASE WHEN (p.isbom = 'Y' AND p.isphantom = 'Y') THEN 'Y'::char(1)  ELSE 'N'::char(1) END as isexplode 
	FROM m_product_bom b
	INNER JOIN m_product p ON b.m_productbom_id = p.m_product_id
	WHERE b.m_product_id = p_product_id
	UNION ALL
	SELECT parent.bomlevel+1, parent.m_product_id, child.m_productbom_id, child.bomqty*parent.bomqty as bomqty
	, cp.m_locator_id
	, CASE WHEN (cp.isbom = 'Y' AND cp.isphantom = 'Y') THEN 'Y'::char(1)  ELSE 'N'::char(1) END as isexplode 
	FROM rpl parent, m_product_bom child, m_product cp
	WHERE parent.m_productbom_id = child.m_product_id
	and child.m_productbom_id = cp.m_product_id
	and parent.isexplode = 'Y'
    )
    select rpl.bomlevel, rpl.m_product_id, rpl.m_productbom_id, rpl.bomqty, rpl.m_locator_id, rpl.isexplode
    from rpl ;
END;
$function$
