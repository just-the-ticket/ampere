copy "c_elementvalue" ("c_elementvalue_id", "value", "name", "accountsign", "accounttype", "ad_client_id", "ad_org_id", "c_bankaccount_id", "c_currency_id", "c_element_id", "created", "createdby", "description", "isactive", "isbankaccount", "isdoccontrolled", "isforeigncurrency", "issummary", "postactual", "postbudget", "postencumbrance", "poststatistical", "updated", "updatedby", "validfrom", "validto") from STDIN;
419	21101	Tree Farm Payable	C	L	11	11	\N	\N	105	2021-12-02 17:03:55	100	Tree Farm Payable	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:45:37	0	\N	\N
421	43000	Sideline Revenue	C	R	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
422	46000	Royalties Revenue	C	R	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
423	47000	Unearned revenue	C	R	11	11	\N	\N	105	2021-01-20 15:21:41	100	We have invoiced, but not delivered yet	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:41	0	\N	\N
424	48000	Not invoiced revenue	C	R	11	11	\N	\N	105	2021-01-20 15:21:41	100	We delivered but have not invoiced yet	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:41	0	\N	\N
425	49	Other Revenue	C	R	11	0	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-12-23 14:13:39	100	\N	\N
426	59101	Trade Discounts	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	Granted Trade discounts (corrects Product Revenue)	Y	N	Y	N	N	Y	Y	Y	Y	2021-12-23 13:14:55	100	\N	\N
427	59201	Payment discount revenue	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	Granted early payment discount to customers	Y	N	Y	N	N	Y	Y	Y	Y	2021-12-23 13:14:22	100	\N	\N
428	59300	Promotion Discounts	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-12-23 13:09:44	100	\N	\N
429	5	Cost of Goods Sold	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
430	51100	Product CoGs	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	Cost of Goods Sold	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:41	0	\N	\N
431	51200	Product Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	Default Product or Service costs (I.e. not on inventory)	Y	N	Y	N	N	Y	Y	Y	Y	2021-09-24 10:55:55	100	\N	\N
432	52000	Services Purchases	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
433	53000	Sideline Purchases	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
434	54000	Freight in	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
435	55	Returns	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
436	55100	Returns Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
437	55200	Returns to Vendors	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
438	56	Inventory CoGs	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
439	56100	Inventory Shrinkage	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	Physical Inventory Gain/Loss	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:41	0	\N	\N
440	56200	Inventory Write Down Below Cost	D	E	11	11	\N	\N	105	2021-01-20 15:21:41	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
441	56300	Inventory Adjustment	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	Inventory Actual Accounting Value Adjustment	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:42	0	\N	\N
442	56400	Inventory Revaluation	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	Difference to (lower cost) or market	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:42	0	\N	\N
443	58	CoGS Variances	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
444	58100	Invoice price variance	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	Difference between product cost and invoice price (IPV)	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:42	0	\N	\N
445	58200	Purchase price variance	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	Difference between purchase price and standard costs (PPV)	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:42	0	\N	\N
446	59	CoGs/Sales Discounts	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-12-23 13:21:02	100	\N	\N
447	59100	Trade discounts received	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	Received Trade Discounts (corrects Product expense)	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:42	0	\N	\N
448	59200	Payment discount revenue	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	Granted early payment discount from vendors	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:42	0	\N	\N
449	6	Expenses	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
450	60	Payroll Expenses	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
451	60130	Salaries	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
452	60210	Vacation Pay	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
453	60220	Vacation Pay Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
454	60240	Sick Pay Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
455	60300	Retirement (Pension, Profit Sharing, 401k)	D	E	11	0	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:52:38	101	\N	\N
456	60420	FICA Tax Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
457	60430	Federal/State Unemployment	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
458	60440	Federal/State Unemployment Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
459	60520	Health Insurance Premium Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
460	60580	Health Insurance Employee Contribution	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
461	60590	Health Insurance Cobra Payments	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
462	60600	Workers Compensation Insurance	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
463	60710	Disability Insurance Premium	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
464	60720	Disability Insurance Premium Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
465	60790	Disability Insurance Employee Contribution	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
466	60810	Life Insurance Premium	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
467	60820	Life Insurance Premium Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
468	60890	Life Insurance Employee Contribution	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
469	60900	Other Benefit Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
470	60910	Staff Gifts & Perks	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
471	60920	Staff Picnic (100% Deductible)	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
472	60990	Payroll Processing Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
473	61	Occupancy Cost	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
474	61100	Rent Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:42	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
475	61200	Utilities	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
476	61300	Other Occupancy Costs	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
477	62	Advertising	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
478	62100	Media Advertising	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
479	62200	Catalog, Newsletter	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
480	62300	Events	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
481	62500	Web Hosting	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
482	62800	Other Advertising & Promotion	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
483	62900	Vendor Advertising Allowances	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
484	63100	Telephone	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
485	63200	Mobile Telephone	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
486	63300	Network Charges	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
487	64100	Legal Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
488	64200	Accounting Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
489	64300	Inventory Verification Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
490	65	Stationary & Supplies	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
491	65100	Stationary & Supplies - Office Use	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
492	65500	Janitorial Supplies	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
493	66100	Data Processing Supplies	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
494	66200	Data Processing Rental	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
495	66300	Outside Computer Services	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
496	67	Depreciation Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
497	67120	Building Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
498	67150	Leasehold Improvement Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
499	67210	Furniture Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
500	67220	Fixtures Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
501	67230	Equipment Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
502	67250	Data Processing Equipment Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
503	67260	Software Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
504	68	Business Travel	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
505	68200	Business Travel - Auto Reimbursement	D	E	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
506	1	Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
507	11	Cash	D	A	11	11	\N	\N	105	2021-01-20 15:21:43	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
508	11100	Checking Account	D	A	11	11	\N	\N	105	2021-01-20 15:21:43	100	Bank Asset	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:43	0	\N	\N
509	11110	Checking In-Transfer	D	A	11	11	\N	\N	105	2021-01-20 15:21:43	100	Bank transactions in transit	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:43	0	\N	\N
510	11120	Checking Unidentified Receipts	D	A	11	11	\N	\N	105	2021-01-20 15:21:43	100	Receipts from unidentified customer	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:43	0	\N	\N
511	11130	Checking Unallocated Receipts	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Received, unallocated payments	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
512	11200	Checking Account 2	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
513	11300	Savings Account	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
514	11800	Cash in Registers	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
515	11900	Petty Cash	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Cash Book Asset	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
516	11910	Petty Cash In-Transfer	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Cash Book Transfer	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
517	12	Accounts Receivable	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
518	12110	Accounts Receivable - Trade	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Accounts Receivables	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
519	12120	A/R Non Sufficient Funds Returned Checks	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
520	12180	A/R Trade Allowance for Bad Debit	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
521	12190	Not invoiced receivables	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	We delivered but have not invoiced yet	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
522	122	Credit Card in Transit	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
523	12210	In Transit A/R Amex	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
524	12220	In Transit A/R Master Card	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
525	12230	In Transit A/R Visa	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
526	12290	A/R Credit Cards Allowance for Bad Debit	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
527	12300	Vendor Allowances Receivable	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
528	124	Loans Receivable	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
529	12410	Loans Receivable Employees	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
530	12440	Loans Receivable Others	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
531	125	Prepayments	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
532	12510	Vendor prepayment	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Prepayments for future expense	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
533	12520	Employee Expense Prepayment	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Expense advances	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
534	12610	Tax credit A/R	D	A	11	0	\N	\N	105	2021-01-20 15:21:44	100	Tax to be reimbursed - before tax declaration	Y	N	Y	N	N	Y	Y	Y	Y	2022-11-15 22:51:28	101	\N	\N
535	12620	Tax receivables	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	Tax to receive based on tax declaration	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:44	0	\N	\N
536	12900	A/R Miscellaneous	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
537	13	Investments	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
538	13100	US Government Obligations	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
539	13200	Tax-Exempt Securities	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
540	13300	Other Investments	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
541	14	Inventory	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
542	15	Prepaid Expenses, Deposits & Other Current Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
543	151	Prepaid Expenses	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
544	15110	Prepaid Insurance	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
545	15120	Prepaid Rent	D	A	11	11	\N	\N	105	2021-01-20 15:21:44	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
546	15130	Prepaid Interest	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
547	15190	Prepaid Others	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
548	152	Deposits	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
549	15210	Deposit Rent	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
550	15220	Deposit Utilities	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
551	15290	Deposit Others	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
552	153	Other Current Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
553	15300	Other Current Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
554	16	Land and Building	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
555	16100	Land	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
556	16200	Building	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
557	16300	Land Improvements	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
558	16400	Building Improvements	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
559	16500	Leasehold Improvements	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
560	17	Furniture, Fixtures & Equipment	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
561	17100	Furniture	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
562	17200	Fixtures	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
563	17300	Equipment	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
564	17400	Vehicles	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
565	17500	Data Processing Equipment	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
566	17600	Software	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
567	17710	Project asset	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	Created Asset	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:45	0	\N	\N
568	17720	Project WIP	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	Asset Work in Progress	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:45	0	\N	\N
569	18120	Building Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
570	18130	Land Improvement Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
571	18140	Building Improvement Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
572	18210	Furniture Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
573	18220	Fixtures Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
574	18230	Equipment Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
575	18240	Vehicles Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
576	18250	Data Processing Equipment Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
577	19	Other Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
578	191	Intangible Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
579	19110	Amortizable Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
580	19130	Customer Lists	D	A	11	11	\N	\N	105	2021-01-20 15:21:45	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
581	19140	Other Intangible Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
582	19150	Accumulated Amortization	D	A	11	0	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:51:41	101	\N	\N
583	19900	Other Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
584	2	Liabilities	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
585	21	Accounts Payables	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
586	211	Accounts Payables Trade	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
587	21190	Not invoiced receipts	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Received products/services from vendors, but not invoiced	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
588	21200	Accounts Payable Services	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Accounts Payables for non-product revenue	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
589	21300	Payment selection	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Selected A/P invoices for payment	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
590	215	Customer Payables	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
591	21520	Merchandise Credits Unredeemed	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
592	21530	Gift Certificated Unredeemed	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
593	21540	Special Order Deposits	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
594	21550	Mail Order Deposits	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
595	216	Tax Payables	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
596	21610	Tax due	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Tax to be paid - before tax declaration	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
597	21620	Tax liability	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Tax to be paid based on tax declaration	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
598	21700	Withholding (Tax)	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Withholding for 1099 or Quality Guarantee	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
599	21710	Withholding (Other)	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
600	21800	Intercompany Due To	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	Default Payables account for intercompany trx	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:46	0	\N	\N
601	22	Accrued Expenses	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
602	22100	Accrued Payroll	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
603	22200	Payroll Withholdings	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
604	22210	Federal Tax Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
605	22220	FICA Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
606	22230	Medicare Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
607	22240	State Tax Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
608	22250	Local Tax Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
609	22260	401k & Pension Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
610	22270	Garnishment Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
611	22290	Miscellaneous Withholding	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
612	22300	Accrued Payroll Taxes	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
613	22330	Federal Unemployment Employers Contribution	C	L	11	0	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:52:08	101	\N	\N
614	22340	State Unemployment Employers Contribution	C	L	11	11	\N	\N	105	2021-01-20 15:21:46	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
615	22400	Sales Tax Collected	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
616	22600	Accrued Retirement Plan Expense	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
617	22900	Accrued Expenses Others	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
618	23100	Short Term Obligation	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
619	23900	Current Portion of Long Term Debt	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
620	24	Long Term Liabilities	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
621	24200	Mortgage	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
622	24300	Loan from Owner/Stockholder	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
623	24800	Other Long Term Obligation	C	L	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
624	3	Owner's Equity/Net Worth	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
625	31	Capital	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
626	31000	Capital/Common Stock	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
627	31010	Capital in excess of Par on Common Stock	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
628	31110	Capital in excess of Par on Preferred Stock	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
629	32	Earnings	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
630	32200	Drawings	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
631	32900	Retained Earnings	C	O	11	11	\N	\N	105	2021-01-20 15:21:47	100	Year end processing to balance account (Income Summary)	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:47	0	\N	\N
632	4	Sales	C	R	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
633	42000	Services Revenue	C	R	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
634	57000	Direct Labor	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
635	58300	Purchase price variance Offset	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	Offset Account for Purchase price variance (PPV)	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:47	0	\N	\N
636	60120	Wages Owners	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
637	60230	Sick Pay	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
638	60410	FICA Tax	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
639	60510	Health Insurance Premium	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
640	62400	Joint Advertising	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
641	63	Telephone and Communications	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
642	64	Professional Services	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
643	64500	Other Consulting Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
644	66	Data Processing	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
645	66900	Data Processing Other Expense	D	E	11	0	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:53:02	101	\N	\N
646	67140	Building Improvement Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
647	67240	Vehicles Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
648	68100	Business Travel - Plane/Train/Taxi	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
649	68300	Business Travel - Hotel	D	E	11	11	\N	\N	105	2021-01-20 15:21:47	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
650	68400	Business Meals & Entertainment (50%)	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
651	68500	Staff Meeting Food (100%)	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
652	68600	Employee expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	Default employee expenses	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:48	0	\N	\N
653	68900	Business Travel Other Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
654	69	Insurance	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
655	69100	Business Insurance	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
656	69200	Real Estate Insurance	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
657	69300	Vehicle Insurance	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
658	69400	Other Insurance	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
659	70	Payment Processor Costs	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
660	70100	Credit Card Service Charges	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
661	70200	Bank Service Charges	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	Bank expenses	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:48	0	\N	\N
662	70900	Other Payment Service Charges	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
663	71	Dues & Subscription	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
664	71100	Association Membership Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
665	71200	Subscription Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
666	72	Office Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
667	72100	Office Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
668	73	Posting & Shipping	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
669	73100	Postage Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
670	73290	Customer Postage Fees Received	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
671	73300	Shipping Expenses	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
672	74	Taxes and Licenses	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
673	74200	Inventory & Use Taxes	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
674	74300	Real Estate Taxes	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
675	74400	Business Licenses & Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
676	75	Education	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
677	75200	Education Travel	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
678	75300	Education Meals & Entertainment (50%)	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
679	75900	Education Other	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
680	76100	Office Equipment Rent	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
681	76200	Store Equipment Rent	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
682	77	Repairs & Maintenance	D	E	11	11	\N	\N	105	2021-01-20 15:21:48	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
683	77100	Building Repairs & Maintenance	D	E	11	0	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:54:20	101	\N	\N
684	77200	Furniture, Equipment Repairs & Maintenance	D	E	11	0	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:54:26	101	\N	\N
685	77400	Software Maintenance	D	E	11	0	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:54:45	101	\N	\N
686	77500	Vehicle Repairs & Maintenance	D	E	11	0	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:54:49	101	\N	\N
687	77900	Repairs & Maintenance Other	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
688	78	Other Operating Expenses	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
689	78200	Collection Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
690	78300	Petty Cash Over/Short	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Petty Cash Differences	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
691	78400	Recruitment Costs	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
692	78500	Vehicle Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
693	78600	Amortization Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
694	78800	Franchise Fee / Royalty Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
695	78900	Miscellaneous Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
696	79	Default/Suspense Accounts	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Temporary accounts - balance should be zero	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
697	79100	Default account	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Default Account (if no other account is defined) V1.1	Y	N	Y	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
698	79200	Suspense balancing	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Difference to make journal balance in source currency - needs to be solved	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
699	79300	Suspense error	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Import did not find account - needs to be solved	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
700	79400	Cash book expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Default other expense for petty cash transactions	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
701	49500	Cash book receipts	C	R	11	0	\N	\N	105	2021-01-20 15:21:49	100	Default other revenue for petty cash transactions	Y	N	Y	N	N	Y	Y	Y	Y	2021-12-23 14:15:29	100	\N	\N
702	79600	Charge expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:49	100	Default other expense	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
703	49700	Charge revenue	C	R	11	0	\N	\N	105	2021-01-20 15:21:49	100	Default other revenue	Y	N	Y	N	N	Y	Y	Y	Y	2021-12-23 14:16:03	100	\N	\N
704	80	Other Income	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
705	80100	Interest Income	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	Bank interest revenue	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
706	80200	Dividend Income	C	R	11	0	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:55:11	101	\N	\N
707	80300	Rental Income	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
708	80400	Sales Tax Commission	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
709	805	Currency Gain	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
710	80510	Bank revaluation gain	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	Foreign currency bank account gain	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
711	80520	Bank settlement gain	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	Difference between payment and bank account currency	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
712	80530	Unrealized gain	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	Difference between foreign currency receivables/payables and current rate	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:49	0	\N	\N
713	80700	Capital Gains Income	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
714	80800	Fixed Asset Sale Gain	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
715	80900	Other Income	C	R	11	11	\N	\N	105	2021-01-20 15:21:49	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
716	82	Other Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
717	82100	Interest Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	Bank interest expense	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
718	82200	Mortgage Interest Expense	D	E	11	0	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:55:27	101	\N	\N
719	82400	Charitable Contributions	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
720	825	Currency Loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
721	82510	Bank revaluation loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	Foreign currency bank account loss	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
722	82530	Unrealized loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	Difference between foreign currency receivables/payables and current rate	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
723	82540	Realized loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	Difference between invoice and payment currency	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
724	82550	Currency balancing	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	Rounding difference to make journal balance in accounting currency	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
725	82700	Capital Gains Loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
726	82800	Fixed Asset Sale Loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-12-21 15:27:34	100	\N	\N
727	82900	Other Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
728	89	Income Tax & Summary	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
729	89200	State Income Tax	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
730	89300	Local Income Tax	D	E	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
731	91	Costing	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
732	911	Profit Center Costing Distribution	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
733	912	Project Costing Distribution	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
734	95	Commitment Accounting	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
735	951	PO Encumbrance	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
736	952	PO Commitment	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
737	955	SO Future Revenue	N	M	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
738	12240	In Transit A/R ATM Cards	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
739	12420	Loans Receivable Owners/Shareholders	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
740	126	Tax receivables	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
741	12800	Intercompany Due From	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	Default Receivables account for intercompany trx	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
742	14120	Product asset	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	Product Inventory Account	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
743	177	Project Assets	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
744	18	Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
745	18150	Leasehold Improvement Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
746	18260	Software Accumulated Depreciation	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
747	19120	Goodwill	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
748	19200	Cash Surrender Value of Life Insurance	D	A	11	11	\N	\N	105	2021-01-20 15:21:50	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
749	21100	Accounts Payable Trade	C	L	11	11	\N	\N	105	2021-01-20 15:21:50	100	Accounts Payables	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:50	0	\N	\N
750	21510	Customer Prepayments	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	Prepayments for future revenue	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
751	22310	FICA Employers Contribution	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
752	22500	Accrued Use Tax	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
753	23	Current Note Payables	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
754	24100	Long Term Obligation	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
755	24900	Current Portion of Long Term Debt	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
756	31100	Preferred Stock	C	O	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
757	32100	Dividends	C	O	11	0	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:52:21	101	\N	\N
758	41000	Trade Revenue	C	R	11	11	\N	\N	105	2021-01-20 15:21:51	100	Default Product or Service revenue	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
759	73200	Customer Package Expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
760	74100	Tax expense	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	Sales Tax paid to Vendors	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
761	75100	Education Course Fees	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
762	76	Equipment Rent	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
763	76900	Equipment Rent Other	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
764	77300	Data Processing Repairs & Maintenance	D	E	11	0	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2022-11-15 22:54:30	101	\N	\N
765	78100	Bad Debts Write-off	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	Receivables write-off - bad debt	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
766	78700	Penalties	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
767	80540	Realized gain	C	R	11	11	\N	\N	105	2021-01-20 15:21:51	100	Difference between invoice and payment currency	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
768	82300	Uninsured Casualty Loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
769	82520	Bank settlement loss	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	Difference between payment and bank account currency	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
770	89100	Federal Income Tax	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
771	89900	Income Summary	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	Year end processing to balance account (Retained Earnings)	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
772	60110	Wages	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
773	67130	Land Improvement Depreciation	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
774	121	Accounts Receivable - Trade	D	A	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
775	74900	Licenses & Fees - Other	D	E	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
776	14100	General Trade Inventory	D	A	11	11	\N	\N	105	2021-01-20 15:21:51	100	Inventory Account	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-20 15:21:51	0	\N	\N
777	22320	Medicare Employers Contribution	C	L	11	11	\N	\N	105	2021-01-20 15:21:51	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-22 02:20:13	0	\N	\N
778	51800	Commissions Paid	D	E	11	0	\N	\N	105	2021-12-20 12:34:10	100	Commissions Paid	Y	N	N	N	N	Y	Y	Y	Y	2021-12-23 14:34:35	100	\N	\N
780	51400	Inventory Clearing	D	E	11	0	\N	\N	105	2021-09-24 10:56:49	100	Default Product costs (I.e. on inventory)	Y	N	Y	N	N	Y	Y	Y	Y	2021-12-23 14:34:55	100	\N	\N
781	51300	Cost Adjustments	D	E	11	0	\N	\N	105	2021-09-24 10:57:25	100	Product Cost Adjustments	Y	N	Y	N	N	Y	Y	Y	Y	2021-12-23 14:39:12	100	\N	\N
782	12115	Accounts Receivable Services - Trade	D	A	11	11	\N	\N	105	2021-10-14 10:52:27	100	Accounts Receivables for Services	Y	N	Y	N	N	Y	Y	Y	Y	2021-10-14 10:52:27	100	\N	\N
783	7	7-Expenses	D	E	11	11	\N	\N	105	2021-12-14 11:27:59	100	7-Expenses	Y	N	N	N	Y	Y	Y	Y	Y	2021-12-23 13:30:56	100	\N	\N
50000	953	SO Commitment	N	M	11	0	\N	\N	105	2021-12-01 02:55:03	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-12-01 02:55:03	100	\N	\N
50001	58400	Using Variance	D	E	11	0	\N	\N	105	2021-01-01 11:54:00	100	Account for Using Variance	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 11:54:36	100	\N	\N
50002	58500	Method Change Variance	D	E	11	0	\N	\N	105	2021-01-01 11:55:10	100	Account for Method Change Variance	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 11:55:10	100	\N	\N
50003	58600	Rate Variance	D	E	11	0	\N	\N	105	2021-01-01 11:56:54	100	Account for Rate Variance	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 11:56:54	100	\N	\N
50004	58700	Mix Variance	D	E	11	0	\N	\N	105	2021-01-01 11:58:12	100	Account for Rate Variance	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 11:58:12	100	\N	\N
50005	14130	Work In Process	D	A	11	0	\N	\N	105	2021-01-01 12:00:37	100	Work In Process Account	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 12:00:37	100	\N	\N
50006	14140	Floor Stock	D	A	11	0	\N	\N	105	2021-01-01 12:04:23	100	Floor Stock Account	Y	N	N	N	N	Y	Y	Y	Y	2021-01-01 12:04:23	100	\N	\N
50007	83100	Labor (Absorbed)	D	E	11	0	\N	\N	105	2021-01-01 12:09:17	100	Labor Absorbed account	Y	N	N	N	N	Y	Y	Y	Y	2021-01-01 12:17:35	100	\N	\N
50008	83	Expense (Absorbed)	D	E	11	0	\N	\N	105	2021-01-01 12:15:08	100	\N	Y	N	N	N	Y	Y	Y	Y	Y	2021-01-01 12:15:08	100	\N	\N
50009	83200	Burden (Absorbed)	D	E	11	0	\N	\N	105	2021-01-01 12:16:45	100	Burden Absorbed account	Y	N	N	N	N	Y	Y	Y	Y	2021-01-01 12:16:45	100	\N	\N
50010	51110	Cost Of Production	D	E	11	0	\N	\N	105	2021-01-01 12:21:42	100	Cost Of Production Account	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 12:36:57	100	\N	\N
50011	51130	Outside Processing (Subcontract)	D	E	11	0	\N	\N	105	2021-01-01 12:22:45	100	Outside Processing Account	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-07 10:37:29	100	\N	\N
50014	83300	Overhead (Applied)	N	M	11	0	\N	\N	105	2021-01-01 12:31:38	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-01-08 19:23:23	100	\N	\N
50016	51120	Scrap	D	E	11	0	\N	\N	105	2021-01-01 12:38:19	100	Scrap Account	Y	N	Y	N	N	Y	Y	Y	Y	2021-01-01 12:38:19	100	\N	\N
50017	58800	Average Cost Variance	D	E	11	0	\N	\N	105	2022-03-08 20:56:26	100	Account for Average Cost Variance	Y	N	Y	N	N	Y	Y	Y	Y	2022-03-08 20:56:26	100	\N	\N
50018	17690	Asset Additions	D	A	11	0	\N	\N	105	2021-02-17 16:13:20	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-17 16:13:20	100	\N	\N
50019	17692	Revaluation amount	D	A	11	0	\N	\N	105	2021-02-17 16:14:01	100	\N	Y	N	N	N	N	Y	Y	Y	Y	2021-02-17 16:14:01	100	\N	\N
50020	21622	Tax suspense for Consolidation	C	L	11	0	\N	\N	105	2021-02-25 09:46:42	100	Tax to be paid based on tax declaration	Y	N	Y	N	N	Y	Y	Y	Y	2021-02-25 09:46:42	100	\N	\N
50021	11810	Cash for Store Central	N	A	11	0	\N	\N	105	2022-05-30 14:44:38	100	Cash for Store Central	Y	N	Y	N	N	Y	Y	Y	Y	2022-05-30 14:44:38	100	\N	\N
50023	11820	Cash for Store Central In-Transfer	N	A	11	0	\N	\N	105	2022-05-30 16:03:24	100	Cash for Store Central In-Transfer	Y	N	Y	N	N	Y	Y	Y	Y	2022-05-30 16:03:24	100	\N	\N
50024	11830	Safe for Store Central	N	A	11	0	\N	\N	105	2022-05-30 16:22:11	100	Safe for Store Central	Y	N	Y	N	N	Y	Y	Y	Y	2022-05-30 16:22:11	100	\N	\N
50025	11840	Safe In-Transfer for Store Central	N	A	11	0	\N	\N	105	2022-05-30 16:36:20	100	Safe In-Transfer for Store Central	Y	N	Y	N	N	Y	Y	Y	Y	2022-05-30 16:36:20	100	\N	\N
\.
