copy "m_discountschema" ("m_discountschema_id", "name", "ad_client_id", "ad_org_id", "created", "createdby", "cumulativelevel", "description", "discounttype", "flatdiscount", "isactive", "isbpartnerflatdiscount", "isquantitybased", "processing", "script", "updated", "updatedby", "validfrom") from STDIN;
100	Sales	11	0	2021-12-29 20:05:43	0	\N	\N	P	\N	Y	N	Y	\N	\N	2021-02-04 13:38:57	0	2021-01-01 00:00:00
101	Purchase	11	11	2021-12-29 20:05:43	0	\N	\N	P	\N	Y	N	Y	\N	\N	2021-02-04 13:38:57	0	2021-05-21 00:00:00
102	5% Discount if 100+	11	11	2022-06-14 23:45:23	100	L	Just an example of additional 5% when ordering more then 100 units per line	B	0	Y	N	Y	N	\N	2022-06-14 23:45:23	100	2022-01-01 00:00:00
\.
