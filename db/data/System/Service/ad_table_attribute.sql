copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
51512	c_servicelevel_pkey	0	0	337	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (c_servicelevel_id)	CP	\N	2022-11-02 05:57:56.041854	100
51513	mproduct_cservicelevel	0	0	337	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51514	crevrecplan_cservicelevel	0	0	337	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_revenuerecognition_plan_id) REFERENCES c_revenuerecognition_plan(c_revenuerecognition_plan_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51515	c_servicelevelline_pkey	0	0	338	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (c_servicelevelline_id)	CP	\N	2022-11-02 05:57:56.041854	100
51516	cservicelevel_line	0	0	338	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_servicelevel_id) REFERENCES c_servicelevel(c_servicelevel_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52664	s_expensetype_pkey	0	0	481	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_expensetype_id)	CP	\N	2022-11-02 05:57:56.041854	100
52665	cuom_sexpensetype	0	0	481	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_uom_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52666	mproductcategory_sexpensetype	0	0	481	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_product_category_id) REFERENCES m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52667	ctaxcategory_sexpensetype	0	0	481	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_taxcategory_id) REFERENCES c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52668	s_resource_pkey	0	0	487	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_resource_id)	CP	\N	2022-11-02 05:57:56.041854	100
52669	aduser_sresource	0	0	487	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52670	mwarehouse_sresource	0	0	487	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_warehouse_id) REFERENCES m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52671	sresourcetype_sresource	0	0	487	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_resourcetype_id) REFERENCES s_resourcetype(s_resourcetype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52672	s_resourceassignment_pkey	0	0	485	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_resourceassignment_id)	CP	\N	2022-11-02 05:57:56.041854	100
52673	sresource_sresourceassignment	0	0	485	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_resource_id) REFERENCES s_resource(s_resource_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52678	s_resourceunavailable_pkey	0	0	482	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_resourceunavailable_id)	CP	\N	2022-11-02 05:57:56.041854	100
52679	sresource_sresunavailable	0	0	482	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_resource_id) REFERENCES s_resource(s_resource_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52680	s_timeexpense_pkey	0	0	486	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_timeexpense_id)	CP	\N	2022-11-02 05:57:56.041854	100
52681	mpricelist_stimeexpense	0	0	486	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_pricelist_id) REFERENCES m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52682	mwarehouse_stimeexpense	0	0	486	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_warehouse_id) REFERENCES m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52683	cbpartner_stimeexpense	0	0	486	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52684	s_timeexpenseline_pkey	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_timeexpenseline_id)	CP	\N	2022-11-02 05:57:56.041854	100
52685	corderline_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_orderline_id) REFERENCES c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52686	stimetype_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_timetype_id) REFERENCES s_timetype(s_timetype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52687	cactivity_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_activity_id) REFERENCES c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52688	ccampaign_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_campaign_id) REFERENCES c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52689	cinvoiceline_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_invoiceline_id) REFERENCES c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52690	ccurrency_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52691	cbpartner_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52692	mproduct_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52693	cproject_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_project_id) REFERENCES c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52694	sresourceassign_steline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_resourceassignment_id) REFERENCES s_resourceassignment(s_resourceassignment_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52695	cuom_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_uom_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52696	stimeexpense_line	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_timeexpense_id) REFERENCES s_timeexpense(s_timeexpense_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52697	cprojecttask_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_projecttask_id) REFERENCES c_projecttask(c_projecttask_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52698	cprojectphase_stimeexpenseline	0	0	488	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_projectphase_id) REFERENCES c_projectphase(c_projectphase_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52699	s_timesheetentry_key	0	0	53781	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_timesheetentry_id)	CP	\N	2022-11-02 05:57:56.041854	100
52700	s_timetype_pkey	0	0	581	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_timetype_id)	CP	\N	2022-11-02 05:57:56.041854	100
52701	s_training_pkey	0	0	538	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_training_id)	CP	\N	2022-11-02 05:57:56.041854	100
52702	ctaxcategory_straining	0	0	538	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_taxcategory_id) REFERENCES c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52703	mproductcategory_straining	0	0	538	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_product_category_id) REFERENCES m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52704	cuom_straining	0	0	538	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (c_uom_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52705	s_training_class_pkey	0	0	537	2022-11-02 05:57:56.041854	100	Service	\N	Y	PRIMARY KEY (s_training_class_id)	CP	\N	2022-11-02 05:57:56.041854	100
52706	mproduct_strainingclass	0	0	537	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52707	straining_strainingclass	0	0	537	2022-11-02 05:57:56.041854	100	Service	\N	Y	FOREIGN KEY (s_training_id) REFERENCES s_training(s_training_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
53685	c_servicelevel_processed_check	0	0	337	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53686	c_servicelevel_isactive_check	0	0	337	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53687	c_servicelevelline_processed_check	0	0	338	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53688	c_servicelevelline_isactive_check	0	0	338	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54206	s_expensetype_isinvoiced_check	0	0	481	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isinvoiced = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54207	s_expensetype_isactive_check	0	0	481	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54208	s_resource_ismanufacturingresource_check	0	0	487	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((ismanufacturingresource = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54209	s_resource_isavailable_check	0	0	487	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isavailable = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54210	s_resource_isactive_check	0	0	487	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54211	chk_col_53272	0	0	487	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((percentutilization >= (0)::numeric))	CC	\N	2022-11-02 12:40:39.392787	100
54212	s_resourceassignment_isconfirmed_check	0	0	485	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isconfirmed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54213	s_resourceassignment_isactive_check	0	0	485	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54226	s_resourceunavailable_isactive_check	0	0	482	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54227	s_timeexpense_processed_check	0	0	486	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54228	s_timeexpense_isactive_check	0	0	486	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54229	s_timeexpenseline_isactive_check	0	0	488	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54230	s_timeexpenseline_isinvoiced_check	0	0	488	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isinvoiced = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54231	s_timeexpenseline_istimereport_check	0	0	488	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((istimereport = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54232	s_timesheetentry_isactive_check	0	0	53781	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54233	s_timesheetentry_isapproved_check	0	0	53781	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isapproved = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54234	s_timesheetentry_isbillable_check	0	0	53781	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isbillable = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54235	s_timesheetentry_processed_check	0	0	53781	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54236	s_training_isactive_check	0	0	538	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54237	s_training_class_isactive_check	0	0	537	2022-11-02 12:40:39.392787	100	Service	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
