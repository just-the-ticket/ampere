copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
203	Purchasing	\N	0	\N	\N	0	\N	\N	2000-05-22 21:36:26	0	\N	Requisition-to-Invoice	Y	N	N	N	Y	2020-10-16 23:45:42	100
204	Material Receipt	W	0	\N	\N	0	\N	184	2000-05-22 21:39:28	0	Vendor Shipments (Receipts)	Requisition-to-Invoice	Y	Y	N	N	N	2000-01-02 00:00:00	0
205	Purchase Order	W	0	\N	\N	0	\N	181	2000-05-22 21:41:21	0	Manage Purchase Orders	Requisition-to-Invoice	Y	Y	N	N	N	2000-01-02 00:00:00	0
206	AP Invoice	W	0	\N	\N	0	\N	183	2000-05-23 15:04:37	0	AP Invoice Entry	Requisition-to-Invoice	Y	Y	N	N	N	2022-02-28 00:35:53	100
312	Match PO/MR/Invoice	X	0	108	\N	0	\N	\N	2002-02-08 20:39:48	0	Match Purchase Orders, Receipts, Vendor Invoices	Requisition-to-Invoice	Y	Y	N	N	N	2022-02-28 00:34:26	100
315	Matched Invoices	W	0	\N	\N	0	\N	107	2002-02-23 20:30:23	0	View Matched Invoices	Requisition-to-Invoice	Y	Y	N	N	N	2005-11-25 15:02:24	100
316	Matched POs	W	0	\N	\N	0	\N	228	2002-02-23 20:30:40	0	View Matched Purchase Orders	Requisition-to-Invoice	Y	Y	N	N	N	2022-02-17 22:08:53	100
360	Expense Invoice (Alpha)	W	0	\N	\N	0	\N	262	2003-05-08 07:44:26	0	Payables expense invoices - This is Alpha Functionality	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:08:32	100
452	RfQ Topic	W	0	\N	\N	0	\N	314	2004-02-19 13:45:55	0	Maintain RfQ Topics and Subscribers	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:07:53	100
454	RfQ	W	0	\N	\N	0	\N	315	2004-02-19 13:46:22	0	Manage Request for Quotations	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:07:57	100
463	Requisition	W	0	\N	\N	0	\N	322	2004-03-12 00:19:12	0	Material Requisition	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 13:58:33	100
466	RfQ Response	W	0	\N	\N	0	\N	324	2004-03-23 23:23:35	0	Manage RfQ Responses	Requisition-to-Invoice	N	Y	N	Y	N	2019-12-05 14:08:02	100
467	RfQ Response	R	0	\N	\N	0	264	\N	2004-03-24 01:31:54	0	Detail RfQ Responses	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:08:10	100
468	RfQ Unanswered	R	0	\N	\N	0	263	\N	2004-03-24 01:32:22	0	Outstanding RfQ Responses	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:08:05	100
471	Open Requisitions	R	0	\N	\N	0	270	\N	2004-03-25 15:56:45	0	Detail Open Requisition Information	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:08:19	100
493	Material Receipt Details	R	0	\N	\N	0	293	\N	2004-07-20 21:48:20	0	Material Receipt Detail Information	Requisition-to-Invoice	Y	Y	N	N	N	2000-01-02 00:00:00	0
516	Invoice Batch	W	0	\N	\N	0	\N	342	2005-04-02 20:57:29	100	Expense Invoice Batch	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:08:46	100
549	Create PO from Requisition	P	0	\N	\N	0	337	\N	2005-10-24 18:18:21	100	Create Purchase Orders from Requisitions	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 13:58:35	100
53966	Purchasing from Replenishment	X	0	53053	\N	0	\N	\N	2015-03-23 13:36:19	100	\N	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 13:58:49	100
53973	Suggested PO x Whse	P	0	\N	\N	0	53770	\N	2015-04-01 14:27:09	100	\N	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 13:58:40	100
53978	Receipt Scanner	X	0	53055	\N	0	\N	\N	2015-04-08 13:35:14	100	Material Receipt using Barcode Scanner	Requisition-to-Invoice	N	Y	N	N	N	2019-12-05 14:08:26	100
\.
