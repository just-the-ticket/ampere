copy "ad_message" ("ad_message_id", "value", "ad_client_id", "ad_org_id", "created", "createdby", "entitytype", "isactive", "msgtext", "msgtip", "msgtype", "updated", "updatedby") from STDIN;
101	0	0	0	2001-07-31 17:10:43	0	D	Y	zero	\N	I	2005-11-28 16:57:34	100
102	1	0	0	2001-07-31 17:07:18	0	D	Y	one	\N	I	2005-11-28 16:57:39	100
103	2	0	0	2001-07-31 17:07:39	0	D	Y	two	\N	I	2005-11-28 16:57:44	100
104	3	0	0	2001-07-31 17:07:57	0	D	Y	three	\N	I	2005-11-28 16:57:46	100
105	4	0	0	2001-07-31 17:08:18	0	D	Y	four	\N	I	2005-11-28 16:57:48	100
106	5	0	0	2001-07-31 17:08:39	0	D	Y	five	\N	I	2005-11-28 16:57:50	100
107	6	0	0	2001-07-31 17:09:07	0	D	Y	six	\N	I	2005-11-28 16:57:52	100
108	7	0	0	2001-07-31 17:09:31	0	D	Y	seven	\N	I	2005-11-28 16:57:54	100
109	8	0	0	2001-07-31 17:09:52	0	D	Y	eight	\N	I	2005-11-28 16:57:56	100
110	9	0	0	2001-07-31 17:10:15	0	D	Y	nine	\N	I	2005-11-28 16:58:00	100
111	About	0	0	1999-07-25 00:00:00	0	D	Y	About 	\N	M	2000-01-02 00:00:00	0
112	Access	0	0	1999-05-21 00:00:00	0	D	Y	Access	\N	I	2000-01-02 00:00:00	0
113	AccessCannotChange	0	0	1999-07-11 00:00:00	0	D	Y	You cannot change the record	You don't have the privileges	E	2000-01-02 00:00:00	0
114	AccessCannotDelete	0	0	1999-06-07 00:00:00	0	D	Y	You cannot delete this record	You don't have the privileges	E	2000-01-02 00:00:00	0
115	AccessCannotInsert	0	0	1999-06-07 00:00:00	0	D	Y	You cannot insert a record	You don't have the privileges	E	2000-01-02 00:00:00	0
116	AccessCannotUpdate	0	0	1999-07-14 00:00:00	0	D	Y	You cannot update this record	You don't have the privileges	E	2000-01-02 00:00:00	0
117	AccessClientOrg	0	0	2000-01-12 15:56:09	0	D	Y	Client & Organization Data	\N	I	2000-01-02 00:00:00	0
118	AccessNotDeleteable	0	0	1999-06-07 00:00:00	0	D	Y	You cannot delete records of this file for audit reasons	If not a transaction, you can deactivate the recorrd with deselecting the 'Active' flag 	E	2000-01-02 00:00:00	0
119	AccessOrg	0	0	2000-01-12 15:55:16	0	D	Y	Organization Data	\N	I	2000-01-02 00:00:00	0
120	AccessShared	0	0	2000-01-12 15:58:33	0	D	Y	Shared Data	\N	I	2000-01-02 00:00:00	0
121	AccessSystem	0	0	2000-01-12 15:57:03	0	D	Y	System Data	\N	I	2000-01-02 00:00:00	0
122	AccessSystemClient	0	0	2000-01-12 15:57:56	0	D	Y	System & Client Data	\N	I	2000-01-02 00:00:00	0
123	AccessTableNoUpdate	0	0	2000-01-12 15:46:21	0	D	Y	With your current role, you cannot update this information	You don't have the privileges	E	2000-01-02 00:00:00	0
124	AccessTableNoView	0	0	2000-01-12 15:40:45	0	D	Y	With your current role and settings, you cannot view this information	You don't have the privileges (your Role does not allow to access the information) - or - set profile (e.g. if you want to see accounting records, Show Accounting must be set)	E	2000-01-02 00:00:00	0
125	Adempiere	0	0	1999-05-21 00:00:00	0	D	Y	Adempiere, Inc.	http://www.adempiere.org	I	2000-01-02 00:00:00	0
126	AccortoAddress	0	0	1999-05-21 00:00:00	0	D	Y	40 Old Tannery Rd Monroe CT 06460 USA	Phone (203) 445-8182	I	2000-01-02 00:00:00	0
127	AccortoOnline	0	0	1999-12-15 14:23:52	0	D	Y	Adempiere Online	\N	E	2000-01-02 00:00:00	0
128	AccountCombination	0	0	1999-10-06 00:00:00	0	D	Y	Account Combination	\N	M	2000-01-02 00:00:00	0
129	AccountNewUpdate	0	0	2000-08-12 20:44:40	0	D	Y	Create new Account or update Alias	\N	M	2000-01-02 00:00:00	0
130	AccountNotUpdated	0	0	2000-08-12 20:42:56	0	D	Y	Account not updated	\N	E	2005-11-28 16:58:48	100
131	AcctViewer	0	0	2001-11-25 20:50:14	0	D	Y	Account Viewer	\N	M	2000-01-02 00:00:00	0
132	ActionNotAllowedHere	0	0	2000-04-26 08:56:44	0	D	Y	This action is not allowed in this context	\N	E	2000-01-02 00:00:00	0
133	ActionNotSupported	0	0	2001-01-18 20:09:04	0	D	Y	This action is not supported	\N	E	2000-01-02 00:00:00	0
134	Address	0	0	1999-12-16 16:39:34	0	D	Y	Address	\N	E	2000-01-02 00:00:00	0
135	Advanced	0	0	2000-09-15 16:27:13	0	D	Y	&Advanced	\N	M	2005-11-29 18:41:17	100
136	Alias	0	0	1999-10-05 00:00:00	0	D	Y	Alias	\N	M	2000-01-02 00:00:00	0
137	AllRecords	0	0	2000-04-13 21:49:45	0	D	Y	All records	\N	E	2000-01-02 00:00:00	0
138	AllocateStatus	0	0	2001-01-20 13:18:57	0	D	Y	Prefer selecting a matching invoice and payment pair and process each at a time.	\N	I	2000-01-02 00:00:00	0
139	AlreadyPosted	0	0	2000-02-04 18:55:48	0	D	Y	Record cannot be changed	\N	E	2000-01-02 00:00:00	0
140	Amount	0	0	2000-05-14 20:46:23	0	D	Y	Amount	\N	I	2000-01-02 00:00:00	0
141	AmountDue	0	0	2001-07-28 20:46:47	0	D	Y	Amount Due	\N	M	2000-01-02 00:00:00	0
142	AmountPay	0	0	2001-07-28 20:47:30	0	D	Y	Payment Amount	\N	M	2000-01-02 00:00:00	0
143	AmtFrom	0	0	2001-10-23 21:27:16	0	D	Y	From Amount	\N	M	2000-01-02 00:00:00	0
144	AmtTo	0	0	2001-10-23 21:27:41	0	D	Y	Amount To	\N	M	2000-01-02 00:00:00	0
145	Application	0	0	1999-08-15 00:00:00	0	D	Y	Application	\N	M	2000-01-02 00:00:00	0
146	AppliedAmt	0	0	2001-01-27 19:49:19	0	D	Y	Applied	\N	M	2000-01-02 00:00:00	0
147	AtFirstRecord	0	0	1999-05-21 00:00:00	0	D	Y	At first record	\N	I	2000-01-02 00:00:00	0
148	AtLastRecord	0	0	1999-05-21 00:00:00	0	D	Y	At last record	\N	I	2000-01-02 00:00:00	0
149	Attachment	0	0	1999-08-10 00:00:00	0	D	Y	Attachment	\N	M	2000-01-02 00:00:00	0
150	AttachmentDelete?	0	0	1999-08-10 00:00:00	0	D	Y	Do you want to delete this (complete) Attachment?	\N	I	2000-01-02 00:00:00	0
151	AttachmentNew	0	0	1999-08-10 00:00:00	0	D	Y	Select a file to attach to this entity	\N	I	2000-01-02 00:00:00	0
152	AttachmentNotFound	0	0	2000-02-27 10:35:59	0	D	Y	Attachment not found	\N	E	2000-01-02 00:00:00	0
153	AttachmentNull	0	0	1999-08-10 00:00:00	0	D	Y	Cannot add Attachment to this entity	Attachments require a single key and this entity is probably an Association (with two keys) or an entity without an unique numeric key.	E	2000-01-02 00:00:00	0
154	AutoWriteOff	0	0	2001-01-27 19:47:37	0	D	Y	Automatic Write-Off	Automatic calculation of write-off amount to close the open item	I	2000-01-02 00:00:00	0
155	AutoCommit	0	0	2001-08-15 15:43:36	0	D	Y	Automatic Commit	Automatic saving of data	I	2000-01-02 00:00:00	0
156	AutoLogin	0	0	2001-08-15 15:48:25	0	D	Y	Automatic Login	Log in with current user ID automatically	I	2000-01-02 00:00:00	0
157	Available	0	0	2000-04-11 15:59:27	0	D	Y	Available Choices	\N	E	2000-01-02 00:00:00	0
158	BPartnerNotFound	0	0	2000-02-29 21:08:08	0	D	Y	Business Partner not found	\N	E	2000-01-02 00:00:00	0
159	BPartnerNotSaved	0	0	2000-02-29 21:09:19	0	D	Y	Business Partner not saved	\N	E	2000-01-02 00:00:00	0
160	BarAdd	0	0	1999-12-24 08:32:00	0	D	Y	Add to Bar	\N	E	2000-01-02 00:00:00	0
161	BarChart	0	0	2000-04-15 15:16:42	0	D	Y	Bar Chart	\N	E	2000-01-02 00:00:00	0
162	BarRemove	0	0	1999-12-24 08:32:14	0	D	Y	Remove from Bar	\N	E	2000-01-02 00:00:00	0
163	BusPartner	0	0	1999-08-15 00:00:00	0	D	Y	Business Partner	\N	M	2000-01-02 00:00:00	0
164	Calculator	0	0	1999-06-07 00:00:00	0	D	Y	Calculator	\N	I	2000-01-02 00:00:00	0
165	Calendar	0	0	2000-04-26 20:11:29	0	D	Y	Calendar	\N	E	2000-01-02 00:00:00	0
166	Cancel	0	0	1999-08-17 00:00:00	0	D	Y	&Cancel	\N	M	2005-12-02 19:01:06	100
167	CancelQuery	0	0	1999-07-25 00:00:00	0	D	Y	&Cancel Query	\N	I	2005-12-02 19:01:16	100
168	CannotChangeDocType	0	0	2000-02-05 16:16:45	0	D	Y	Cannot change Document Type	\N	E	2000-01-02 00:00:00	0
169	CannotDelete	0	0	2000-02-09 21:50:03	0	D	Y	You cannot delete this record (you might be able to e-activate it)	\N	E	2005-01-29 12:45:12	100
170	CannotDeleteTrx	0	0	2000-05-14 11:26:58	0	D	Y	Cannot delete completed/processed transactions	\N	E	2000-01-02 00:00:00	0
171	CashBookChangeIgnored	0	0	2001-01-02 05:28:31	0	D	Y	Change of the default Cashbook ignored.\nPlease change Cashbook only after completion of transaction	\N	I	2005-11-28 16:58:59	100
172	ChargeCreated	0	0	2001-02-06 18:52:21	0	D	Y	Charge created	\N	I	2005-11-28 16:59:03	100
173	ChargeFromAccount	0	0	2001-02-06 21:45:11	0	D	Y	Create Charge from Account	\N	I	2005-11-28 16:59:10	100
174	ChargeGenerate	0	0	2001-02-06 18:48:23	0	D	Y	Generate Charges	\N	M	2000-01-02 00:00:00	0
175	ChargeNewAccount	0	0	2001-02-06 21:44:24	0	D	Y	Create Account and Charge	\N	I	2005-11-28 16:59:19	100
176	ChargeNotCreated	0	0	2001-02-06 18:53:08	0	D	Y	Charge NOT created	\N	E	2000-01-02 00:00:00	0
177	City	0	0	1999-12-16 16:39:47	0	D	Y	City	\N	E	2000-01-02 00:00:00	0
178	Client	0	0	1999-05-21 00:00:00	0	D	Y	Client	\N	I	2000-01-02 00:00:00	0
179	ClientOrgGroup	0	0	1999-05-21 00:00:00	0	D	Y	Shared Services	\N	M	2000-01-02 00:00:00	0
180	Close	0	0	1999-07-25 00:00:00	0	D	Y	Close Window	\N	M	2000-01-02 00:00:00	0
181	Combination	0	0	1999-10-05 00:00:00	0	D	Y	Combination	\N	M	2000-01-02 00:00:00	0
182	Connection	0	0	2000-04-26 20:15:12	0	D	Y	Connection	\N	I	2000-01-02 00:00:00	0
183	Contact	0	0	2000-01-15 13:06:15	0	D	Y	Contact	\N	E	2000-01-02 00:00:00	0
184	ControlAmtError	0	0	2001-05-09 21:22:46	0	D	Y	Control Amount differs from Balance	\N	E	2000-01-02 00:00:00	0
185	ConversionRateCommandError	0	0	1999-08-30 00:00:00	0	D	Y	Error while executing currency conversion routine	\N	E	2000-01-02 00:00:00	0
186	ConvertedAmount	0	0	2001-01-11 20:34:17	0	D	Y	Converted	\N	M	2000-01-02 00:00:00	0
187	Copied	0	0	2000-09-28 16:54:26	0	D	Y	Copied	\N	M	2000-01-02 00:00:00	0
188	Copy	0	0	1999-07-25 00:00:00	0	D	Y	&Copy Record	\N	M	2005-11-28 21:03:03	100
189	CopyEnter	0	0	1999-05-21 00:00:00	0	D	Y	Update copied record and save or ignore	\N	M	2000-01-02 00:00:00	0
190	CopyError	0	0	1999-08-15 00:00:00	0	D	Y	Could not copy: 	\N	E	2000-01-02 00:00:00	0
191	Country	0	0	1999-12-16 20:28:48	0	D	Y	Country	\N	E	2000-01-02 00:00:00	0
192	Create	0	0	2001-02-06 21:52:09	0	D	Y	Create	\N	M	2000-01-02 00:00:00	0
193	CreateNew	0	0	2000-10-14 19:47:48	0	D	Y	Create New Record	\N	M	2000-01-02 00:00:00	0
194	Created	0	0	2000-05-03 20:49:43	0	D	Y	Created	\N	E	2000-01-02 00:00:00	0
195	CreditCardExpFormat	0	0	2001-01-01 15:48:09	0	D	Y	Credit Card Expiration Date Format must be "MMYY"	\N	E	2000-01-02 00:00:00	0
196	CreditCardExpMonth	0	0	2001-01-01 15:54:53	0	D	Y	Credit Card Expiration Date Month invalid	\N	E	2000-01-02 00:00:00	0
197	CreditCardExpYear	0	0	2001-01-01 15:55:31	0	D	Y	Credit Card Expiration Date Year invalid	\N	E	2000-01-02 00:00:00	0
198	CreditCardExpired	0	0	2001-01-01 15:45:17	0	D	Y	Credit Card is expired	\N	E	2000-01-02 00:00:00	0
199	CreditCardNumberError	0	0	2001-01-01 16:08:30	0	D	Y	Credit Card Number not valid	\N	E	2000-01-02 00:00:00	0
200	CreditCardNumberProblem?	0	0	2001-01-01 16:06:16	0	D	Y	There seems to be a Credit Card Number problem.\nContinue?	\N	I	2000-01-02 00:00:00	0
201	CreditCardVVError	0	0	2001-01-01 16:15:50	0	D	Y	Credit Card Validation Code not correct.	You find the four digit validation number printed on AMEX right above the credit card number. On other cards it is a three digit number printed on the signature field after the credit card number.	E	2000-01-02 00:00:00	0
202	CreditLimitOver	0	0	1999-12-31 03:21:40	0	D	Y	Note: Over Credit Limit 	\N	E	2000-01-02 00:00:00	0
203	CurrencyConversion	0	0	2001-10-26 21:25:15	0	D	Y	Currency Conversion	\N	M	2000-01-02 00:00:00	0
204	CurrentSettings	0	0	2001-10-25 17:37:15	0	D	Y	Current Settings	\N	I	2000-01-02 00:00:00	0
205	Customize	0	0	1999-06-18 00:00:00	0	D	Y	Customize	\N	I	2000-01-02 00:00:00	0
206	DBExecuteError	0	0	2001-07-28 20:53:05	0	D	Y	Database Error.	\N	E	2000-01-02 00:00:00	0
207	DataRefreshed	0	0	1999-07-11 00:00:00	0	D	Y	Data requeried from database	\N	I	2005-11-28 20:59:59	100
208	Database	0	0	2000-04-26 20:15:56	0	D	Y	Database	\N	I	2000-01-02 00:00:00	0
209	Date	0	0	2000-04-26 20:16:20	0	D	Y	Date	\N	I	2000-01-02 00:00:00	0
210	DateFrom	0	0	2000-11-01 11:16:42	0	D	Y	Date from	\N	M	2000-01-02 00:00:00	0
211	DateTo	0	0	2000-11-01 11:43:49	0	D	Y	Date to	\N	M	2000-01-02 00:00:00	0
212	DebugLevel	0	0	2001-08-15 15:38:20	0	D	Y	Trace Level	\N	I	2000-01-02 00:00:00	0
213	DefaultError	0	0	2000-04-09 17:24:45	0	D	Y	Default Error 	\N	E	2000-01-02 00:00:00	0
214	Delete	0	0	1999-05-21 00:00:00	0	D	Y	&Delete record	\N	M	2005-11-28 17:00:14	100
215	DeleteError	0	0	1999-08-15 00:00:00	0	D	Y	Could not delete record: 	\N	E	2000-01-02 00:00:00	0
216	DeleteErrorDependent	0	0	2000-07-06 18:41:55	0	D	Y	Record not deleted - dependent record found	\N	E	2000-01-02 00:00:00	0
217	DeleteRecord?	0	0	1999-05-21 00:00:00	0	D	Y	Do you want to delete the record?	\N	I	2000-01-02 00:00:00	0
218	Deleted	0	0	1999-08-15 00:00:00	0	D	Y	Deleted	\N	I	2000-01-02 00:00:00	0
219	Description	0	0	2000-01-15 13:16:36	0	D	Y	Description	\N	E	2000-01-02 00:00:00	0
220	Detail	0	0	1999-05-21 00:00:00	0	D	Y	Detail record	\N	M	2000-01-02 00:00:00	0
221	Difference	0	0	2001-01-09 21:00:12	0	D	Y	Difference	\N	M	2000-01-02 00:00:00	0
222	Discount	0	0	2000-05-29 21:40:44	0	D	Y	Trade Discount	\N	E	2000-01-02 00:00:00	0
223	DiscountDate	0	0	2001-07-31 17:52:22	0	D	Y	Discount Date	\N	M	2000-01-02 00:00:00	0
224	DiscountsInvalid	0	0	2000-05-29 21:37:40	0	D	Y	Category Discount invalid (above or below 100)	\N	E	2000-01-02 00:00:00	0
225	DisplayDocumentInfo	0	0	2001-11-25 20:59:57	0	D	Y	Display Document Info	\N	I	2000-01-02 00:00:00	0
226	DisplayQty	0	0	2001-11-25 20:55:50	0	D	Y	Display Quantity	\N	M	2000-01-02 00:00:00	0
227	DisplaySourceInfo	0	0	2001-11-25 20:57:57	0	D	Y	Display Source Info	\N	I	2000-01-02 00:00:00	0
228	DocBeingProcessed	0	0	1999-09-08 00:00:00	0	D	Y	Document is being processed	\N	I	2006-01-21 18:29:42	100
229	DocumentCopy	0	0	2000-04-05 03:10:00	0	D	Y	Copy	\N	E	2000-01-02 00:00:00	0
230	Drill	0	0	2000-04-10 08:47:22	0	D	Y	Drill	\N	E	2000-01-02 00:00:00	0
231	EFT	0	0	2001-07-28 20:30:35	0	D	Y	EFT	\N	M	2000-01-02 00:00:00	0
232	EMail	0	0	2000-04-20 12:44:06	0	D	Y	EMail	\N	I	2000-01-02 00:00:00	0
233	EMailSupport	0	0	2000-05-29 22:24:52	0	D	Y	EMail to Support	\N	E	2000-01-02 00:00:00	0
234	Edit	0	0	1999-05-21 00:00:00	0	D	Y	&Edit	\N	M	2000-01-02 00:00:00	0
235	Editor	0	0	2000-07-06 20:58:17	0	D	Y	Editor	\N	E	2000-01-02 00:00:00	0
236	End	0	0	1999-12-15 12:19:40	0	D	Y	e&Xit Window	\N	E	2005-12-06 18:07:57	100
237	EnterQuery	0	0	1999-07-25 00:00:00	0	D	Y	Enter &Query	\N	I	2005-12-02 19:02:48	100
238	TreeSearchText	0	0	1999-12-24 08:31:08	0	D	Y	Enter text to search for in tree	\N	M	2005-11-29 18:20:43	100
239	Environment	0	0	1999-05-21 00:00:00	0	D	Y	Environment	\N	M	2000-01-02 00:00:00	0
240	Error	0	0	1999-12-13 21:14:25	0	D	Y	Error: 	\N	E	2000-01-02 00:00:00	0
241	Execure Query	0	0	1999-07-25 00:00:00	0	D	Y	Execute Query	\N	I	2000-01-02 00:00:00	0
242	Exit	0	0	1999-05-21 00:00:00	0	D	Y	Exit Application	\N	M	2000-01-02 00:00:00	0
243	ExitApplication?	0	0	1999-05-21 00:00:00	0	D	Y	Are you sure you want to exit the application?	\N	I	2000-01-02 00:00:00	0
244	ExpandAll	0	0	1999-06-08 00:00:00	0	D	Y	Expand all	\N	I	2000-01-02 00:00:00	0
245	ExpandTree	0	0	1999-12-24 08:30:19	0	D	Y	&Expand Tree	\N	I	2005-11-29 18:08:09	100
246	Expense	0	0	2001-02-06 21:36:00	0	D	Y	Expense	\N	M	2000-01-02 00:00:00	0
247	Expires	0	0	2000-05-14 20:51:38	0	D	Y	Expires (MMYY)	\N	E	2000-01-02 00:00:00	0
248	Export	0	0	1999-07-25 00:00:00	0	D	Y	Export	\N	I	2000-01-02 00:00:00	0
249	ExportExcel	0	0	1999-08-15 00:00:00	0	D	Y	Export to Excel	\N	M	2000-01-02 00:00:00	0
250	ExportRecords	0	0	1999-08-15 00:00:00	0	D	Y	Export Records	\N	M	2000-01-02 00:00:00	0
251	Failure	0	0	2000-04-28 23:48:25	0	D	Y	Process failed: 	\N	E	2000-01-02 00:00:00	0
252	Field	0	0	2001-01-09 16:58:13	0	D	Y	Field	\N	M	2000-01-02 00:00:00	0
253	File	0	0	2000-07-06 20:55:19	0	D	Y	&File	\N	M	2000-01-02 00:00:00	0
254	FileCSV	0	0	2000-03-07 16:19:16	0	D	Y	csv - Excel Comma Separated Values file	\N	E	2000-01-02 00:00:00	0
255	FileCannotCreate	0	0	2000-04-09 21:30:21	0	D	Y	Cannot create file	\N	E	2000-01-02 00:00:00	0
256	FileHTML	0	0	2000-03-07 16:21:15	0	D	Y	html - HTML file	\N	E	2000-01-02 00:00:00	0
257	FileImport	0	0	2000-09-20 21:47:42	0	D	Y	Import File Loader	\N	M	2000-01-02 00:00:00	0
258	FileImportFile	0	0	2001-10-17 19:56:32	0	D	Y	<Select File to Load>	\N	I	2000-01-02 00:00:00	0
259	FileImportFileInfo	0	0	2001-10-17 19:57:38	0	D	Y	Select the file to load matching the Import Format	\N	I	2000-01-02 00:00:00	0
260	FileImportNoFormat	0	0	2000-09-26 17:44:20	0	D	Y	Select a Import Format	\N	M	2000-01-02 00:00:00	0
261	FileImportR/I	0	0	2000-09-26 17:43:00	0	D	Y	Rows in file / loaded and ready to import:	\N	M	2000-01-02 00:00:00	0
262	FileInvalidExtension	0	0	2000-04-09 21:31:54	0	D	Y	Invalid File Extension	\N	E	2000-01-02 00:00:00	0
263	FilePDF	0	0	2000-03-07 16:20:44	0	D	Y	pdf - Acrobat PDF file	\N	E	2000-01-02 00:00:00	0
264	FilePS	0	0	2000-04-09 21:52:26	0	D	Y	ps - Postscript file	\N	E	2000-01-02 00:00:00	0
265	FileRTF	0	0	2000-04-09 20:52:33	0	D	Y	rtf - Rich Text Format file	\N	E	2000-01-02 00:00:00	0
266	FileTXT	0	0	2000-03-07 16:21:40	0	D	Y	txt - Tab delimited file	\N	E	2000-01-02 00:00:00	0
267	FileWriteError	0	0	2000-04-09 22:08:12	0	D	Y	Error writing file	\N	E	2000-01-02 00:00:00	0
268	FileXML	0	0	2000-04-09 20:51:58	0	D	Y	xml - XML file	\N	E	2000-01-02 00:00:00	0
269	FillMandatory	0	0	1999-07-11 00:00:00	0	D	Y	Fill mandatory fields: 	\N	E	2000-01-02 00:00:00	0
270	Find	0	0	1999-07-25 00:00:00	0	D	Y	&Lookup Record	\N	M	2005-11-29 14:19:51	100
271	FindCustAvailable	0	0	1999-08-17 00:00:00	0	D	Y	Available Columns	\N	M	2000-01-02 00:00:00	0
272	FindCustHint	0	0	1999-08-17 00:00:00	0	D	Y	Select columns and order them	\N	M	2000-01-02 00:00:00	0
273	FindCustSelected	0	0	1999-08-17 00:00:00	0	D	Y	Selected Columns	\N	M	2000-01-02 00:00:00	0
274	FindCustomize	0	0	1999-08-17 00:00:00	0	D	Y	Customize Find	\N	M	2000-01-02 00:00:00	0
275	FindHint	0	0	1999-08-14 00:00:00	0	D	Y	Select a row or enter search criteria	\N	M	2000-01-02 00:00:00	0
276	FindTip	0	0	2000-09-15 16:23:52	0	D	Y	Enter Query criteria with optional % wildcard (case insensitive)	\N	M	2000-01-02 00:00:00	0
277	FindZeroRecords	0	0	2000-08-20 18:18:40	0	D	Y	No Records found	\N	M	2000-01-02 00:00:00	0
278	First	0	0	1999-12-15 14:25:41	0	D	Y	First record	\N	E	2000-01-02 00:00:00	0
279	FirstPage	0	0	2000-04-09 19:57:31	0	D	Y	First Page	\N	E	2000-01-02 00:00:00	0
280	From	0	0	2000-05-05 18:08:12	0	D	Y	From	\N	E	2000-01-02 00:00:00	0
281	FunctionNotFound	0	0	1999-07-14 00:00:00	0	D	Y	Callout Function not found	\N	E	2000-01-02 00:00:00	0
282	FunctionProblem	0	0	1999-07-14 00:00:00	0	D	Y	Callout Function Error	\N	E	2000-01-02 00:00:00	0
283	Generate	0	0	2000-09-04 14:38:11	0	D	Y	Generate	\N	M	2000-01-02 00:00:00	0
284	Go	0	0	2000-07-06 20:57:26	0	D	Y	&Go	\N	M	2000-01-02 00:00:00	0
285	GoToPage	0	0	2000-04-09 19:54:07	0	D	Y	Go To Page	\N	E	2000-01-02 00:00:00	0
286	GraphXName	0	0	2000-04-15 15:20:32	0	D	Y	Column for Name (X-Axis)	\N	E	2000-01-02 00:00:00	0
287	Greeting	0	0	2000-03-19 21:22:40	0	D	Y	Greeting	\N	E	2000-01-02 00:00:00	0
288	GroupBy	0	0	2001-11-25 21:02:06	0	D	Y	Summarize	\N	I	2000-01-02 00:00:00	0
289	GroupedBy	0	0	2000-04-15 15:22:04	0	D	Y	Group by	\N	E	2000-01-02 00:00:00	0
290	Heading	0	0	2000-04-11 15:58:18	0	D	Y	Heading	\N	E	2000-01-02 00:00:00	0
291	Help	0	0	1999-05-21 00:00:00	0	D	Y	&Help	\N	M	2000-01-02 00:00:00	0
292	History	0	0	2000-03-08 18:40:30	0	D	Y	Histor&Y records	\N	E	2005-11-29 14:23:47	100
293	Home	0	0	2000-07-06 20:58:55	0	D	Y	&Menu	\N	M	2005-11-29 15:35:45	100
294	Host	0	0	2000-09-09 10:18:41	0	D	Y	Host	\N	M	2000-01-02 00:00:00	0
295	Ignore	0	0	1999-05-21 00:00:00	0	D	Y	&Undo changes	\N	M	2005-11-29 14:03:53	100
296	IgnoreError	0	0	1999-08-15 00:00:00	0	D	Y	Could not ignore changes: 	\N	E	2000-01-02 00:00:00	0
297	Ignored	0	0	1999-08-15 00:00:00	0	D	Y	Changes ignored	\N	I	2000-01-02 00:00:00	0
298	Info	0	0	2000-01-13 09:26:44	0	D	Y	Info	\N	M	2000-01-02 00:00:00	0
299	InfoAccount	0	0	2000-01-13 09:28:59	0	D	Y	Account Info	\N	M	2000-01-02 00:00:00	0
300	InfoBPartner	0	0	2000-01-13 09:27:58	0	D	Y	Business Partner Info	\N	M	2000-01-02 00:00:00	0
301	InfoInOut	0	0	2001-10-23 21:17:32	0	D	Y	Shipment Info	\N	M	2000-01-02 00:00:00	0
302	InfoInvoice	0	0	2001-10-23 21:15:39	0	D	Y	Invoice Info	\N	M	2000-01-02 00:00:00	0
303	InfoOrder	0	0	2001-10-23 21:13:57	0	D	Y	Order Info	\N	M	2000-01-02 00:00:00	0
304	InfoPayment	0	0	2001-10-23 21:16:10	0	D	Y	Payment Info	\N	M	2000-01-02 00:00:00	0
305	InfoProduct	0	0	2000-01-13 09:27:17	0	D	Y	Product &Info	\N	M	2005-11-28 17:09:40	100
306	Inserted	0	0	2000-02-04 18:00:17	0	D	Y	Inserted	\N	I	2000-01-02 00:00:00	0
307	InvGenerate	0	0	2000-09-04 14:37:26	0	D	N	Generate Invoices (manual)	\N	M	2000-01-02 00:00:00	0
308	InvGenerateGen	0	0	2000-09-04 14:41:14	0	D	Y	Generating invoices	\N	I	2000-01-02 00:00:00	0
309	InvGenerateInfo	0	0	2001-07-26 17:31:37	0	D	Y	Invoices are generated depending on the "Invoicing Rule" selection in the Order	\N	I	2000-01-02 00:00:00	0
310	InvGenerateSel	0	0	2000-09-04 14:39:38	0	D	Y	Select shipments to generate invoices	\N	I	2000-01-02 00:00:00	0
311	InventoryProductMultiple	0	0	2000-02-04 17:57:39	0	D	Y	One or more Product is more than once in the count list.  Lines deactivated.	\N	E	2000-01-02 00:00:00	0
312	Invoice	0	0	2000-05-03 20:50:08	0	D	Y	Invoice	\N	I	2000-01-02 00:00:00	0
313	InvoiceCreateDocAlreadyExists	0	0	2001-01-02 22:29:24	0	D	Y	Receipt (Shipment) already exists for this invoice	\N	E	2000-01-02 00:00:00	0
314	InvoiceCreateDocNotCompleted	0	0	2001-01-02 22:25:35	0	D	Y	Document must be completed first	\N	M	2000-01-02 00:00:00	0
315	ItemInsert	0	0	1999-12-24 08:33:32	0	D	Y	Insert Item here	\N	E	2000-01-02 00:00:00	0
316	ItemMove	0	0	1999-12-24 08:33:06	0	D	Y	Move Item	\N	E	2000-01-02 00:00:00	0
317	Last	0	0	1999-12-15 14:27:19	0	D	Y	Last record	\N	E	2000-01-02 00:00:00	0
318	LastPage	0	0	2000-04-09 19:58:08	0	D	Y	Last Page	\N	E	2000-01-02 00:00:00	0
319	Length	0	0	2001-10-17 20:08:27	0	D	Y	Length	\N	M	2000-01-02 00:00:00	0
320	Level	0	0	2000-04-11 15:55:35	0	D	Y	Level	\N	E	2000-01-02 00:00:00	0
321	Load	0	0	2000-08-19 11:16:06	0	D	Y	Load	\N	M	2000-01-02 00:00:00	0
322	Loading	0	0	1999-07-12 00:00:00	0	D	Y	Loading ....	\N	I	2000-01-02 00:00:00	0
323	Location	0	0	1999-12-16 16:38:06	0	D	Y	Location/Address	\N	E	2000-01-02 00:00:00	0
324	LocationNew	0	0	1999-12-16 16:38:52	0	D	Y	Enter new Location/Address	\N	E	2000-01-02 00:00:00	0
325	LocationUpdate	0	0	1999-12-16 16:39:14	0	D	Y	Update Location/Address	\N	E	2000-01-02 00:00:00	0
326	Login	0	0	2000-11-26 21:46:12	0	D	Y	Login	\N	M	2000-01-02 00:00:00	0
327	LoginSuccess	0	0	2000-11-26 22:00:03	0	D	Y	Login (success)	\N	M	2000-01-02 00:00:00	0
328	Margin	0	0	2000-04-06 15:24:14	0	D	Y	Margin	\N	E	2000-01-02 00:00:00	0
329	MatchFrom	0	0	2002-02-10 22:22:39	0	D	Y	Match From	\N	M	2000-01-02 00:00:00	0
330	MatchMode	0	0	2002-02-10 22:23:55	0	D	Y	Search Mode	\N	M	2000-01-02 00:00:00	0
331	MatchTo	0	0	2002-02-10 22:23:12	0	D	Y	Match To	\N	M	2000-01-02 00:00:00	0
332	Matched	0	0	2002-02-10 22:19:22	0	D	Y	Matched	\N	M	2000-01-02 00:00:00	0
333	Matching	0	0	2002-02-10 22:18:51	0	D	Y	Matching	\N	M	2000-01-02 00:00:00	0
334	Menu	0	0	1999-12-24 08:26:40	0	D	Y	&Menu	\N	M	2005-11-29 14:27:02	100
335	MenuGoTo	0	0	1999-08-15 00:00:00	0	D	Y	Go to Menu	\N	M	2000-01-02 00:00:00	0
336	MenuStartError	0	0	2000-09-09 10:17:47	0	D	Y	Menu cannot load: 	\N	E	2000-01-02 00:00:00	0
337	MessageNotSent	0	0	2000-04-20 22:19:34	0	D	Y	Message not sent.\n\nProblem: 	\N	E	2000-01-02 00:00:00	0
338	MessageSent	0	0	2000-04-20 22:20:34	0	D	Y	Message sent.	\N	E	2000-01-02 00:00:00	0
339	Messages	0	0	2000-08-22 21:12:38	0	D	Y	 Messages	\N	M	2000-01-02 00:00:00	0
340	Multi	0	0	1999-07-25 00:00:00	0	D	Y	Grid t&Oggle	\N	M	2005-11-29 14:22:03	100
341	MultiCurrency	0	0	2001-01-11 20:32:25	0	D	Y	Multi-Currency	\N	M	2000-01-02 00:00:00	0
342	N	0	0	2001-11-05 01:20:51	0	D	Y	No	\N	I	2000-01-02 00:00:00	0
343	Name	0	0	2000-01-13 20:23:04	0	D	Y	&Name	\N	I	2005-12-08 20:58:20	100
344	Navigate	0	0	1999-07-25 00:00:00	0	D	Y	Navigate	\N	M	2000-01-02 00:00:00	0
345	NavigateOrUpdate	0	0	1999-05-21 00:00:00	0	D	Y	Navigate or Update record	\N	I	2000-01-02 00:00:00	0
346	NavigateOrUpdateQueryActive	0	0	1999-08-15 00:00:00	0	D	Y	<Query active> Navigate or Update record	\N	I	2000-01-02 00:00:00	0
347	New	0	0	1999-07-25 00:00:00	0	D	Y	&New Record	\N	M	2005-11-28 17:00:06	100
348	NewEnter	0	0	1999-07-11 00:00:00	0	D	Y	Enter data for new record and save or ignore	\N	I	2000-01-02 00:00:00	0
349	NewError	0	0	1999-08-15 00:00:00	0	D	Y	Could not add new record:	\N	E	2000-01-02 00:00:00	0
350	NewValue	0	0	2000-04-11 16:01:31	0	D	Y	New Value	\N	E	2000-01-02 00:00:00	0
351	Next	0	0	1999-05-21 00:00:00	0	D	Y	Next record	\N	I	2000-01-02 00:00:00	0
352	NextPage	0	0	2000-04-09 19:55:40	0	D	Y	Next Page	\N	E	2000-01-02 00:00:00	0
353	NextSequenceNoError	0	0	2000-01-03 09:38:34	0	D	Y	Did not find next Sequence Number	Contact System Administrator	E	2000-01-02 00:00:00	0
354	No	0	0	1999-08-18 00:00:00	0	D	Y	No	\N	I	2000-01-02 00:00:00	0
355	NoMessageFound	0	0	2001-05-09 21:23:44	0	D	Y	Message Not Found	\N	M	2000-01-02 00:00:00	0
356	NoOfLines	0	0	2001-07-31 21:26:35	0	D	Y	Number of lines	\N	M	2000-01-02 00:00:00	0
357	NoOfPayments	0	0	2001-07-28 20:29:22	0	D	Y	Number of Payments	\N	M	2000-01-02 00:00:00	0
358	NoRecordsFound	0	0	1999-05-21 00:00:00	0	D	Y	No records found; Enter data for new record	\N	I	2000-01-02 00:00:00	0
359	NoValidAcctInfo	0	0	1999-10-01 00:00:00	0	D	Y	Did not find valid Accounting Information.	\N	I	2000-01-02 00:00:00	0
360	NotActive	0	0	2001-08-15 15:46:13	0	D	Y	NOT Active	\N	I	2000-01-02 00:00:00	0
361	NotEnoughStocked	0	0	2000-10-15 14:59:19	0	D	Y	Not sufficient on stock:	\N	M	2000-01-02 00:00:00	0
362	NotFound	0	0	2000-02-01 15:31:30	0	D	Y	* Not found *	\N	M	2000-01-02 00:00:00	0
363	NotMatched	0	0	2002-02-10 22:19:52	0	D	Y	Not Matched	\N	M	2000-01-02 00:00:00	0
364	NotUnique	0	0	2000-09-26 17:45:04	0	D	Y	Not Unique:	\N	M	2000-01-02 00:00:00	0
365	NumberRowsColumns	0	0	2000-04-10 08:46:26	0	D	Y	Number of Rows x Columns	\N	E	2000-01-02 00:00:00	0
366	OK	0	0	2000-04-09 19:53:36	0	D	Y	&OK	\N	M	2005-12-02 19:00:38	100
367	OldTrx	0	0	1999-12-21 22:14:59	0	D	Y	Show old/completed Transactions	\N	E	2000-01-02 00:00:00	0
368	OldValue	0	0	2000-04-11 16:00:59	0	D	Y	Old Value	\N	E	2000-01-02 00:00:00	0
369	Online	0	0	2000-08-12 20:39:33	0	D	Y	Online	\N	M	2000-01-02 00:00:00	0
370	OnlyCustomers	0	0	2000-01-15 13:11:40	0	D	Y	Customers only	\N	E	2000-01-02 00:00:00	0
371	OnlyDue	0	0	2001-07-28 20:48:18	0	D	Y	Only Due Invoices	\N	M	2000-01-02 00:00:00	0
372	OnlyVendors	0	0	2000-05-22 14:31:10	0	D	Y	Only Vendors	\N	E	2000-01-02 00:00:00	0
373	Open	0	0	2000-08-19 11:16:36	0	D	Y	Open	\N	M	2000-01-02 00:00:00	0
374	OpenAmt	0	0	2001-01-27 19:48:46	0	D	Y	Open	\N	M	2000-01-02 00:00:00	0
375	Options	0	0	2000-04-15 15:23:08	0	D	Y	Options	\N	E	2000-01-02 00:00:00	0
376	OrderBy	0	0	2000-04-11 15:56:14	0	D	Y	Order by	\N	E	2000-01-02 00:00:00	0
377	OrderSummary	0	0	2000-04-06 15:33:28	0	D	Y	{0} Line(s) - {1,number,#,##0.00} -  Total: {2,number,#,##0.00}  {3}  =  {4,number,#,##0.00}	\N	I	2000-01-02 00:00:00	0
378	Organization	0	0	1999-05-21 00:00:00	0	D	Y	Org	\N	I	2000-01-02 00:00:00	0
379	OtherProcessActive	0	0	2000-02-04 18:53:22	0	D	Y	Another Process for this record is active, retry later	\N	E	2000-01-02 00:00:00	0
380	Page	0	0	2000-04-09 19:52:33	0	D	Y	Page	\N	E	2000-01-02 00:00:00	0
381	PageBreak	0	0	2000-05-15 21:24:04	0	D	Y	Page break	\N	E	2000-01-02 00:00:00	0
382	PageOf	0	0	2000-04-05 03:08:24	0	D	Y	Page {P} of {N}	\N	E	2000-01-02 00:00:00	0
383	PageTwo	0	0	2000-04-09 17:51:33	0	D	Y	Two Pages	\N	E	2000-01-02 00:00:00	0
384	PageWhole	0	0	2000-04-09 17:51:02	0	D	Y	Whole Page	\N	E	2000-01-02 00:00:00	0
385	PageWidth	0	0	2000-04-09 17:50:02	0	D	Y	Page Width	\N	E	2000-01-02 00:00:00	0
386	Parameter	0	0	2000-05-03 20:44:26	0	D	Y	Parameter	\N	I	2000-01-02 00:00:00	0
387	ParameterMissing	0	0	2001-10-30 02:58:49	0	D	Y	Error:  Parameter missing	\N	E	2000-01-02 00:00:00	0
388	Parent	0	0	1999-05-21 00:00:00	0	D	Y	Parent Record	\N	M	2000-01-02 00:00:00	0
389	Password	0	0	2000-11-26 21:51:10	0	D	Y	Password	\N	M	2000-01-02 00:00:00	0
390	Payment	0	0	2000-05-14 20:45:56	0	D	Y	Payment	\N	E	2000-01-02 00:00:00	0
391	PaymentAllocation	0	0	2001-01-09 16:56:41	0	D	Y	Payment Allocation	\N	M	2000-01-02 00:00:00	0
392	PaymentBankAccountNotValid	0	0	2001-01-01 15:38:51	0	D	Y	Bank Account Number is not valid	\N	E	2000-01-02 00:00:00	0
393	PaymentBankCheckNotValid	0	0	2001-01-01 15:41:11	0	D	Y	Bank Check Number is not valid	\N	E	2000-01-02 00:00:00	0
394	PaymentBankRoutingNotValid	0	0	2001-01-01 15:43:42	0	D	Y	Bank Routing Number is not valid	\N	E	2000-01-02 00:00:00	0
395	PaymentCompleteDocument	0	0	2001-01-24 18:50:39	0	D	Y	Enter the payment after completing the transaction	\N	E	2000-01-02 00:00:00	0
396	PaymentCreated	0	0	2001-01-13 19:15:03	0	D	Y	Created Payment:	\N	M	2000-01-02 00:00:00	0
397	PaymentDiscount	0	0	2001-01-09 21:00:47	0	D	Y	Payment discount	\N	M	2000-01-02 00:00:00	0
398	PaymentError	0	0	2001-01-24 18:52:46	0	D	Y	An error occurred when processing the payment:	\N	E	2000-01-02 00:00:00	0
399	PaymentNoProcessor	0	0	2001-01-01 15:34:24	0	D	Y	No Payment Processor for this Payment Type.	Create Payment Processor for your Bank Account.	E	2000-01-02 00:00:00	0
400	PaymentNotProcessed	0	0	2001-02-01 17:12:23	0	D	Y	Payment process failed	\N	E	2006-01-21 18:29:25	100
401	PaymentProcessed	0	0	2001-02-01 17:11:29	0	D	Y	Payment successfully processed	\N	I	2006-01-21 18:29:32	100
402	PaymentZero	0	0	2001-01-02 05:23:36	0	D	Y	The amount is zero.\nEnter the payment after completing the transaction	\N	E	2000-01-02 00:00:00	0
403	PeriodNextNotFound	0	0	1999-07-16 00:00:00	0	D	Y	Next Calendar Period not found	Please set up future Periods 	I	2000-01-02 00:00:00	0
404	PeriodNotFound	0	0	1999-07-16 00:00:00	0	D	Y	Calendar Period not found	Check setup of Calendar and Accounting Schema as well as Accounting Schema assignment to Client or Organization	I	2000-01-02 00:00:00	0
405	PeriodNotValid	0	0	1999-07-16 00:00:00	0	D	Y	No valid period for this accounting date.	Check your calendar setup.	I	2000-01-02 00:00:00	0
406	PieChart	0	0	2000-04-15 15:15:33	0	D	Y	Pie Chart	\N	E	2000-01-02 00:00:00	0
407	PostDocNotComplete	0	0	2001-11-25 20:46:44	0	D	Y	Document must be Complete or Closed to view Accounting	\N	E	2000-01-02 00:00:00	0
408	PostImmediate?	0	0	2001-11-25 20:49:28	0	D	Y	Post now and create Accounting entries?	\N	I	2000-01-02 00:00:00	0
409	PostServerError	0	0	2001-11-25 20:51:08	0	D	Y	Posting: Server Error	\N	E	2000-01-02 00:00:00	0
410	Postal	0	0	1999-12-16 16:39:59	0	D	Y	ZIP	\N	E	2000-01-02 00:00:00	0
411	PostalAdd	0	0	2001-07-29 20:07:04	0	D	Y	ZIP+4	\N	M	2000-01-02 00:00:00	0
412	PostingError-E	0	0	2001-05-09 21:30:22	0	D	Y	Posting Error	\N	E	2000-01-02 00:00:00	0
413	PostingError-N	0	0	2001-12-07 20:38:48	0	D	Y	Posting Error	\N	E	2000-01-02 00:00:00	0
414	PostingError-b	0	0	2001-05-09 21:25:40	0	D	Y	Posting Error: Source Not Balanced	\N	E	2000-01-02 00:00:00	0
415	PostingError-c	0	0	2001-05-09 21:28:12	0	D	Y	Posting Error: Source Currency not convertible	Define exchange rate from the source currency to your accounting currencies	E	2000-01-02 00:00:00	0
416	PostingError-i	0	0	2001-05-09 21:29:59	0	D	Y	Posting Error: Invalid Account	Account is no longer Active; Reactivate account value or choose new	E	2000-01-02 00:00:00	0
417	PostingError-p	0	0	2001-05-09 21:28:56	0	D	Y	Posting Error: Period Closed	(Re-) Open Period or change Accounting Date	E	2000-01-02 00:00:00	0
418	Preference	0	0	1999-12-17 18:54:41	0	D	Y	Preference	\N	E	2000-01-02 00:00:00	0
419	Preferences	0	0	1999-07-25 00:00:00	0	D	Y	Preferences	\N	I	2000-01-02 00:00:00	0
420	Previous	0	0	1999-05-21 00:00:00	0	D	Y	Previous record	\N	M	2000-01-02 00:00:00	0
421	PreviousPage	0	0	2000-04-09 19:56:41	0	D	Y	Previous Page	\N	E	2000-01-02 00:00:00	0
422	PriceHistory	0	0	2000-05-29 21:39:16	0	D	Y	Price History	\N	E	2000-01-02 00:00:00	0
423	PriceListVersion	0	0	2000-01-13 20:24:11	0	D	Y	Pricelist Version	\N	E	2000-01-02 00:00:00	0
424	PriceListVersionNotFound	0	0	2001-10-26 21:26:44	0	D	Y	No active and valid price list version found	\N	E	2000-01-02 00:00:00	0
425	Print	0	0	1999-08-15 00:00:00	0	D	Y	&Print	\N	M	2005-11-28 17:03:25	100
426	PrintCustomize	0	0	2001-10-14 15:54:07	0	D	Y	Customize Report	\N	M	2000-01-02 00:00:00	0
427	PrintCustomizeDefined	0	0	2002-02-14 18:15:39	0	D	Y	Defined [Displayed]	\N	I	2000-01-02 00:00:00	0
428	PrintInvoices	0	0	2000-05-03 20:51:43	0	D	Y	Print Invoices	\N	I	2000-01-02 00:00:00	0
429	PrintOnlyRecentInvoice?	0	0	2000-05-12 20:43:25	0	D	Y	Print only most recent Invoice ?\n(No: print all invoices of order)	\N	I	2000-01-02 00:00:00	0
430	PrintScreen	0	0	1999-12-15 12:17:30	0	D	Y	Print Screen	\N	M	2000-01-02 00:00:00	0
431	PageSetup	0	0	1999-08-15 00:00:00	0	D	Y	Page Setup	\N	M	2000-01-02 00:00:00	0
432	PrintShipments	0	0	2002-03-20 15:45:01	0	D	Y	Print Shipments	\N	I	2000-01-02 00:00:00	0
433	Printed	0	0	2000-04-11 16:00:14	0	D	Y	Printed	\N	E	2000-01-02 00:00:00	0
434	Printer	0	0	2000-04-26 20:16:46	0	D	Y	Printer	\N	E	2000-01-02 00:00:00	0
435	Printing	0	0	2000-04-13 22:14:23	0	D	Y	Printing	\N	E	2000-01-02 00:00:00	0
436	PrintoutOK?	0	0	2000-05-03 20:59:08	0	D	Y	Is the printout OK ?	\N	E	2000-01-02 00:00:00	0
437	Process	0	0	2001-01-09 21:03:16	0	D	Y	Process	\N	M	2000-01-02 00:00:00	0
438	ProcessCancelled	0	0	2000-01-27 21:18:35	0	D	Y	Process cancelled	\N	M	2000-01-02 00:00:00	0
439	ProcessFailed	0	0	1999-09-09 00:00:00	0	D	Y	Process failed	\N	E	2000-01-02 00:00:00	0
440	ProcessNoProcedure	0	0	2000-01-26 22:56:34	0	D	Y	Cannot start process - No procedure name	\N	M	2000-01-02 00:00:00	0
441	ProcessOK	0	0	1999-09-09 00:00:00	0	D	Y	Process finished successful	\N	I	2000-01-02 00:00:00	0
442	ProcessRunError	0	0	2000-01-26 22:59:45	0	D	Y	Process failed during execution	\N	E	2000-01-02 00:00:00	0
443	ProcessStatus-E	0	0	1999-09-12 00:00:00	0	D	Y	Error:     	\N	I	2000-01-02 00:00:00	0
444	ProcessStatus-I	0	0	1999-09-12 00:00:00	0	D	Y	Information: 	\N	I	2000-01-02 00:00:00	0
445	ProcessStatus-W	0	0	1999-09-12 00:00:00	0	D	Y	Warning: 	\N	I	2000-01-02 00:00:00	0
446	ProcessSubmitError	0	0	2000-01-26 22:57:20	0	D	Y	Process could not be submitted	\N	E	2000-01-02 00:00:00	0
447	Processing	0	0	2000-02-07 17:16:54	0	D	Y	Processing ... Please wait ...	\N	E	2000-01-02 00:00:00	0
448	QtyAvailable	0	0	2000-04-06 15:23:21	0	D	Y	Available	\N	E	2000-01-02 00:00:00	0
449	Quantity	0	0	2001-03-31 12:39:33	0	D	Y	Quantity	\N	M	2000-01-02 00:00:00	0
450	Query	0	0	1999-12-15 14:24:50	0	D	Y	Query Enter	\N	E	2000-01-02 00:00:00	0
451	QueryCancel	0	0	1999-07-25 00:00:00	0	D	Y	Query Cancel	\N	M	2000-01-02 00:00:00	0
452	QueryEnter	0	0	1999-07-25 00:00:00	0	D	Y	Query Enter	\N	M	2000-01-02 00:00:00	0
453	QueryEnterNow	0	0	1999-08-15 00:00:00	0	D	Y	Enter Query criteria	\N	I	2000-01-02 00:00:00	0
454	QueryExecute	0	0	1999-07-25 00:00:00	0	D	Y	Query Execute	\N	M	2000-01-02 00:00:00	0
455	QueryInclude?	0	0	2000-02-28 17:48:23	0	D	Y	Include in Query	\N	E	2000-01-02 00:00:00	0
456	QueryNoRecords-Again?	0	0	2000-03-03 13:00:35	0	D	Y	No records with given criteria.  Do you want to change the query criteria?	\N	E	2005-04-18 23:02:55	100
457	R/O	0	0	1999-09-30 00:00:00	0	D	Y	Record is Read Only	\N	I	2000-01-02 00:00:00	0
458	RePost	0	0	2001-11-25 21:05:23	0	D	Y	Re-Post 	\N	I	2000-01-02 00:00:00	0
459	RePostInfo	0	0	2001-11-25 21:06:03	0	D	Y	Repost document recreates accounting for this document	\N	I	2000-01-02 00:00:00	0
460	Record	0	0	1999-07-25 00:00:00	0	D	Y	Record	\N	M	2000-01-02 00:00:00	0
461	RecordError	0	0	1999-12-13 22:02:47	0	D	Y	Problems accessing the record: 	\N	E	2000-01-02 00:00:00	0
462	RecordFound	0	0	1999-08-15 00:00:00	0	D	Y	Record found	\N	I	2000-01-02 00:00:00	0
463	RecordSaved	0	0	1999-07-11 00:00:00	0	D	Y	Record saved	\N	I	2000-01-02 00:00:00	0
464	Records	0	0	1999-05-21 00:00:00	0	D	Y	Records	\N	M	2000-01-02 00:00:00	0
465	Refresh	0	0	1999-05-21 00:00:00	0	D	Y	Re&Query	\N	M	2005-11-28 21:00:12	100
466	RefreshAll	0	0	2001-01-15 21:24:50	0	D	Y	ReQuery all	\N	I	2005-11-28 21:00:25	100
467	RefreshError	0	0	1999-08-15 00:00:00	0	D	Y	Could not requery data: 	\N	E	2005-11-28 21:00:34	100
468	Refreshed	0	0	1999-08-15 00:00:00	0	D	Y	Data requeried	\N	I	2005-11-28 21:00:43	100
469	Refreshing	0	0	2000-01-25 15:50:37	0	D	Y	Requerying data ...	\N	E	2005-11-28 21:00:55	100
470	Region	0	0	1999-12-16 20:28:33	0	D	Y	Region/State	\N	E	2000-01-02 00:00:00	0
471	Remaining	0	0	2001-07-28 20:48:53	0	D	Y	Remaining	\N	M	2000-01-02 00:00:00	0
472	Remittance	0	0	2001-07-31 21:08:54	0	D	Y	Remittance Advice	\N	I	2000-01-02 00:00:00	0
473	Report	0	0	1999-12-17 18:55:04	0	D	Y	&Report	\N	M	2005-11-28 17:04:35	100
474	ReportFind	0	0	2001-07-26 17:29:55	0	D	Y	Report Find	\N	M	2000-01-02 00:00:00	0
475	ReportSave	0	0	2000-03-07 14:45:10	0	D	Y	Save Report as PDF, CSV, HTML or TXT file	\N	I	2005-11-28 17:04:41	100
476	Reports	0	0	1999-12-24 08:27:06	0	D	Y	Reports	\N	I	2005-11-28 17:04:45	100
477	RequestActionEMailError	0	0	2001-12-03 21:47:28	0	D	Y	Could not send EMail	\N	E	2000-01-02 00:00:00	0
478	RequestActionEMailInfo	0	0	2001-12-03 22:10:11	0	D	Y	EMail from {0} to {1}	\N	I	2000-01-02 00:00:00	0
479	RequestActionEMailNoFrom	0	0	2001-12-03 21:44:52	0	D	Y	Cannot send EMail:\nNo originator (from) address - Check User	\N	I	2005-11-28 17:03:48	100
480	RequestActionEMailNoSMTP	0	0	2001-12-03 21:40:42	0	D	Y	Cannot send EMail:\nNo Mail Server (SMTP) found; Check Client Info	\N	I	2005-11-28 17:03:50	100
481	RequestActionEMailNoTo	0	0	2001-12-03 21:43:38	0	D	Y	Cannot send EMail:\nNo destination (to) address - Check Contact	\N	I	2005-11-28 17:03:53	100
482	RequestActionEMailOK	0	0	2001-12-03 21:47:50	0	D	Y	EMail sent	\N	I	2000-01-02 00:00:00	0
483	RequestActionTransfer	0	0	2001-12-03 21:52:35	0	D	Y	Request {0} was transferred by {1} from {2} to {3} 	\N	I	2005-11-28 17:03:56	100
484	RequestAlert	0	0	2001-12-08 18:38:10	0	D	Y	Alert: Request {0} overdue	\N	I	2000-01-02 00:00:00	0
485	RequestEscalate	0	0	2001-12-08 18:39:28	0	D	Y	Escalated Request {0} to {1}	\N	I	2000-01-02 00:00:00	0
486	RequiredEnter	0	0	1999-08-15 00:00:00	0	D	Y	Enter required information: 	\N	I	2000-01-02 00:00:00	0
487	Revenue	0	0	2000-05-22 19:44:29	0	D	Y	Revenue	\N	E	2000-01-02 00:00:00	0
488	ReversedBy	0	0	2000-05-24 17:11:01	0	D	Y	Reversed by document	\N	E	2000-01-02 00:00:00	0
489	RoleInconsistent	0	0	2000-11-26 21:56:54	0	D	Y	Role and Client/Organization inconsistent	\N	E	2005-11-28 17:04:02	100
490	SO_CreditAvailable	0	0	2000-05-22 19:43:36	0	D	Y	Credit available	\N	E	2000-01-02 00:00:00	0
491	SQLError20200	0	0	2000-04-20 11:18:15	0	D	Y	Product is reserved or was already delivered or invoiced.  Set quantity to zero.\nDetails: 	\N	E	2000-01-02 00:00:00	0
492	SQLErrorNotUnique	0	0	2000-04-20 11:15:03	0	D	Y	Entry in a key field was not unique - a record with that value already exists.\n\nDetails: 	\N	E	2000-01-02 00:00:00	0
493	SQLErrorReferenced	0	0	2000-04-20 11:16:15	0	D	Y	The record is referenced by other records.\n\nDetails: 	\N	E	2000-01-02 00:00:00	0
494	SameBPartner	0	0	2002-02-10 22:24:28	0	D	Y	Same Business Partner	\N	M	2000-01-02 00:00:00	0
495	SameProduct	0	0	2002-02-10 22:25:18	0	D	Y	Same Product	\N	M	2000-01-02 00:00:00	0
496	SameQty	0	0	2002-02-10 22:25:48	0	D	Y	Same Quantity	\N	M	2000-01-02 00:00:00	0
497	Save	0	0	1999-05-21 00:00:00	0	D	Y	&Save changes	\N	M	2005-11-28 17:00:37	100
498	SaveChanges?	0	0	1999-05-21 00:00:00	0	D	Y	Do you want to save changes?	\N	M	2000-01-02 00:00:00	0
499	SaveCookie	0	0	2000-11-26 21:54:08	0	D	Y	Save Info in Cookie	\N	M	2000-01-02 00:00:00	0
500	SaveError	0	0	1999-08-15 00:00:00	0	D	Y	Could not save changes: 	\N	E	2000-01-02 00:00:00	0
501	SaveErrorDataChanged	0	0	2000-07-06 20:12:52	0	D	Y	Could not save changes - data was changed after query.	System will re-query record.	E	2000-01-02 00:00:00	0
502	SaveErrorNotUnique	0	0	2000-07-06 20:34:09	0	D	Y	Could not save record - Require unique data: 	Please change information.	E	2000-01-02 00:00:00	0
503	SaveErrorRowNotFound	0	0	2000-07-06 20:26:27	0	D	Y	Record not saved. Row not found!	\N	E	2000-01-02 00:00:00	0
504	SaveIgnored	0	0	1999-07-11 00:00:00	0	D	Y	Not saved	\N	I	2000-01-02 00:00:00	0
505	SavePostError	0	0	1999-12-13 21:54:28	0	D	Y	Could not post record: 	\N	E	2000-01-02 00:00:00	0
506	Saved	0	0	1999-08-15 00:00:00	0	D	Y	Record saved	\N	I	2000-01-02 00:00:00	0
507	Schema	0	0	2000-09-09 10:19:12	0	D	Y	Schema	\N	M	2000-01-02 00:00:00	0
508	Script	0	0	2001-12-28 17:33:39	0	D	Y	Script	\N	M	2000-01-02 00:00:00	0
509	ScriptEditor	0	0	2001-12-28 17:37:00	0	D	Y	Script Editor	\N	I	2000-01-02 00:00:00	0
510	ScriptError	0	0	2001-12-28 17:34:10	0	D	Y	Script Error	\N	E	2000-01-02 00:00:00	0
511	ScriptHelp	0	0	2001-12-28 17:34:48	0	D	Y	Script Help	\N	I	2000-01-02 00:00:00	0
512	ScriptResult	0	0	2001-12-28 17:37:36	0	D	Y	Result	\N	I	2000-01-02 00:00:00	0
513	ScriptResultVariable	0	0	2001-12-28 17:38:11	0	D	Y	Result Variable	\N	I	2000-01-02 00:00:00	0
514	ScriptVariables	0	0	2001-12-28 17:35:32	0	D	Y	Available Variables	\N	I	2000-01-02 00:00:00	0
515	Search	0	0	1999-05-21 00:00:00	0	D	Y	Search records	\N	M	2000-01-02 00:00:00	0
516	SearchCriteria	0	0	1999-06-18 00:00:00	0	D	Y	Search Criteria	\N	I	2000-01-02 00:00:00	0
517	SearchError	0	0	1999-12-13 22:00:57	0	D	Y	Could not locate record: 	\N	E	2000-01-02 00:00:00	0
518	SearchAND	0	0	2000-01-15 13:07:00	0	D	Y	All / Any	\N	M	2000-01-02 00:00:00	0
519	SearchANDInfo	0	0	2001-10-22 15:49:30	0	D	Y	Search for using All criteria (AND) - or Any criteria (OR)	\N	I	2000-01-02 00:00:00	0
520	SearchNothing	0	0	2000-01-16 15:57:34	0	D	Y	Nothing to search on here	\N	E	2000-01-02 00:00:00	0
521	SearchOK	0	0	2000-01-16 16:03:39	0	D	Y	Select Record & Return	\N	E	2000-01-02 00:00:00	0
522	SearchQueryError	0	0	2000-02-10 22:02:07	0	D	Y	Query Error - probably wrong criteria entered	\N	E	2000-01-02 00:00:00	0
523	SearchRows_EnterQuery	0	0	2000-01-13 20:20:43	0	D	Y	Rows found - Enter query criteria (optionally with %)	\N	E	2000-01-02 00:00:00	0
524	Select	0	0	1999-05-21 00:00:00	0	D	Y	Select	\N	M	2000-01-02 00:00:00	0
525	SelectAvailableChoices	0	0	1999-06-18 00:00:00	0	D	Y	Available Choices	\N	I	2000-01-02 00:00:00	0
526	SelectDocument	0	0	2001-11-25 20:54:43	0	D	Y	Select Document	\N	I	2000-01-02 00:00:00	0
527	SelectFile	0	0	2002-02-14 18:13:50	0	D	Y	Select File	\N	I	2000-01-02 00:00:00	0
528	SelectHint	0	0	1999-06-18 00:00:00	0	D	Y	Search and select an entry	\N	I	2000-01-02 00:00:00	0
529	SelectMenuItem	0	0	2001-08-15 15:59:04	0	D	Y	Select Menu Item	\N	M	2000-01-02 00:00:00	0
530	SelectProgram	0	0	1999-12-24 08:28:29	0	D	Y	Select Program	\N	E	2000-01-02 00:00:00	0
531	SelectSelectColumns	0	0	1999-06-18 00:00:00	0	D	Y	Select Columns for Select Window	\N	I	2000-01-02 00:00:00	0
532	SelectSelectHint	0	0	1999-06-18 00:00:00	0	D	Y	Move columns wanted into the Selected box and sequence their order	\N	I	2000-01-02 00:00:00	0
533	SelectSelectedChoices	0	0	1999-06-18 00:00:00	0	D	Y	Selected Choices	\N	I	2000-01-02 00:00:00	0
534	Selected	0	0	2000-02-07 17:25:49	0	D	Y	Selected	\N	E	2000-01-02 00:00:00	0
535	SendEMail	0	0	1999-08-18 00:00:00	0	D	Y	Send EMail	\N	M	2000-01-02 00:00:00	0
536	SendMail	0	0	2000-09-09 10:16:06	0	D	Y	Send Mail	\N	M	2000-01-02 00:00:00	0
537	Sequence	0	0	2000-04-11 15:57:44	0	D	Y	Sequence	\N	E	2000-01-02 00:00:00	0
538	SequenceDocNotFound	0	0	1999-09-08 00:00:00	0	D	Y	Could not find document sequence:	Check Document Definition and Document Sequence Rules	E	2000-01-02 00:00:00	0
539	SequenceNextNotFound	0	0	1999-09-08 00:00:00	0	D	Y	Could not find next Table ID sequence	Check with your System Administrator	E	2000-01-02 00:00:00	0
540	SequenceNotFound	0	0	1999-08-15 00:00:00	0	D	Y	Could not find Sequence for: 	\N	E	2000-01-02 00:00:00	0
541	ServerObjects	0	0	2002-05-24 00:42:06	0	D	Y	Create Objects on Server	Create complex objects on Application Server (slow communication lines)	I	2005-01-07 14:16:50	100
542	ShipmentCreateDocNotCompleted	0	0	2001-04-01 21:51:22	0	D	Y	Document must be completed first	\N	M	2000-01-02 00:00:00	0
543	ShowAcctTab	0	0	2001-08-15 15:45:21	0	D	Y	Show Accounting Tabs	Show accounting information	I	2000-01-02 00:00:00	0
544	ShowTrlTab	0	0	2001-08-15 15:44:20	0	D	Y	Show Translation Tabs	Show translation information	I	2000-01-02 00:00:00	0
545	Single	0	0	1999-07-25 00:00:00	0	D	Y	Single-Record Display	\N	M	2000-01-02 00:00:00	0
546	SortBy	0	0	2001-11-25 21:01:09	0	D	Y	Sort by	\N	I	2000-01-02 00:00:00	0
547	Sorted	0	0	2000-08-22 17:22:47	0	D	Y	Sorted	\N	M	2000-01-02 00:00:00	0
548	Standard	0	0	2001-03-24 16:26:27	0	D	Y	Standard	\N	I	2000-01-02 00:00:00	0
549	Start	0	0	2000-04-28 22:28:25	0	D	Y	Start	\N	E	2000-01-02 00:00:00	0
550	StartProcess?	0	0	1999-12-15 15:41:23	0	D	Y	Do you want to start the Process?	\N	E	2000-01-02 00:00:00	0
551	StartReport	0	0	1999-12-24 08:59:02	0	D	Y	Start selected Report	\N	I	2005-11-28 17:04:47	100
552	StartReports	0	0	1999-12-17 23:47:41	0	D	Y	Start Reports	\N	I	2005-11-28 17:04:49	100
553	StartSearch	0	0	2000-01-15 13:22:31	0	D	Y	Start Search ...	\N	E	2000-01-02 00:00:00	0
554	Subject	0	0	2000-05-05 18:09:02	0	D	Y	Subject	\N	E	2000-01-02 00:00:00	0
555	Success	0	0	2000-01-03 11:56:41	0	D	Y	Process completed successfully	\N	E	2000-01-02 00:00:00	0
556	Sum	0	0	2001-01-09 21:06:12	0	D	Y	Sum	\N	I	2000-01-02 00:00:00	0
557	System	0	0	2000-04-11 08:15:10	0	D	Y	System	\N	I	2005-12-30 09:30:07	100
558	Tab	0	0	2001-01-09 16:57:50	0	D	Y	Tab	\N	M	2000-01-02 00:00:00	0
559	TaskError	0	0	2000-09-04 14:13:20	0	D	Y	Cannot execute task: 	\N	E	2000-01-02 00:00:00	0
560	Tasks	0	0	2000-08-22 21:13:10	0	D	Y	 Tasks	\N	M	2000-01-02 00:00:00	0
561	TaxCriteriaNotFound	0	0	2000-01-18 11:13:58	0	D	Y	Criteria for Tax not found	\N	E	2000-01-02 00:00:00	0
562	TaxNotFound	0	0	2000-01-18 11:11:52	0	D	Y	Could not find tax	\N	M	2000-01-02 00:00:00	0
563	Test	0	0	1999-12-12 22:08:31	0	D	Y	Test	\N	E	2000-01-02 00:00:00	0
564	Timeout	0	0	2000-01-26 23:01:36	0	D	Y	Timeout - Try to check results later	\N	M	2000-01-02 00:00:00	0
565	To	0	0	2000-05-05 18:08:39	0	D	Y	To	\N	E	2000-01-02 00:00:00	0
566	Today	0	0	2001-10-14 15:55:04	0	D	Y	Today	\N	M	2000-01-02 00:00:00	0
567	Tools	0	0	2000-07-06 20:57:45	0	D	Y	&Tools	\N	M	2005-11-29 14:02:39	100
568	Totals	0	0	2000-04-15 15:14:50	0	D	Y	Totals	\N	E	2000-01-02 00:00:00	0
569	TrxCurrency	0	0	2001-01-11 20:33:39	0	D	Y	Trx	\N	M	2000-01-02 00:00:00	0
570	UITheme	0	0	2001-08-15 15:42:58	0	D	Y	User Interface Theme	Window  - Metal - Adempiere	I	2000-01-02 00:00:00	0
571	UnderLimitPrice	0	0	2000-03-07 14:47:58	0	D	Y	Under Limit Price	\N	E	2000-01-02 00:00:00	0
572	Update	0	0	1999-05-21 00:00:00	0	D	Y	Update	\N	I	2000-01-02 00:00:00	0
573	UpdateCopied	0	0	1999-07-11 00:00:00	0	D	Y	Update data for copied record and save or ignore	\N	I	2000-01-02 00:00:00	0
574	Updated	0	0	2000-02-04 18:00:54	0	D	Y	Updated	\N	I	2000-01-02 00:00:00	0
575	User	0	0	2000-04-11 15:56:58	0	D	Y	User	\N	E	2000-01-02 00:00:00	0
576	UserInterface	0	0	2001-08-15 15:41:33	0	D	Y	User Interface	\N	I	2000-01-02 00:00:00	0
577	UserMessage	0	0	2001-05-09 21:24:34	0	D	Y	User defined message	\N	M	2000-01-02 00:00:00	0
578	UserPwdError	0	0	2000-11-26 21:55:29	0	D	Y	User and Password inconsistent	\N	E	2000-01-02 00:00:00	0
579	VPayPrint	0	0	2001-07-28 20:28:28	0	D	Y	Payment print/export	\N	M	2000-01-02 00:00:00	0
580	VPayPrintNeedRemittance	0	0	2001-07-31 21:14:44	0	D	Y	For some payments, there were not enough room for all remittance lines.\nDo you want to print separate Remittance advice ?	\N	I	2005-11-28 17:02:20	100
581	VPayPrintNoDoc	0	0	2001-07-29 19:54:57	0	D	Y	No Bank Account Documents (Checks) for this Bank Account and Payment Rule.	\N	E	2000-01-02 00:00:00	0
582	VPayPrintNoRecords	0	0	2001-07-28 20:54:46	0	D	Y	No Payments available for Print/Export	\N	I	2000-01-02 00:00:00	0
583	VPayPrintPrintRemittance	0	0	2001-07-31 21:12:56	0	D	Y	Do you want to print Remittance Advice ?	\N	I	2005-11-28 17:02:24	100
584	VPayPrintProcess	0	0	2001-07-28 21:20:58	0	D	Y	Generate EFT Payments	\N	I	2005-11-28 17:02:26	100
585	VPayPrintSuccess?	0	0	2001-07-31 21:22:33	0	D	Y	Is the payment print correct ?	\N	I	2005-11-28 17:02:30	100
586	VPaySelect	0	0	2001-07-28 20:26:03	0	D	Y	Payment selection (manual)	\N	M	2000-01-02 00:00:00	0
587	VPaySelectGenerate?	0	0	2001-07-28 20:50:34	0	D	Y	Generate Payments from Payment Selection?	\N	I	2005-11-28 17:02:33	100
588	VPaySelectNoBank	0	0	2001-07-29 17:30:02	0	D	Y	No Bank Account with Document (e.g. Check)	\N	I	2005-11-28 17:02:36	100
589	VPaySelectPrint?	0	0	2001-07-28 20:51:59	0	D	Y	Print/export generated Payments?	\N	I	2005-11-28 17:02:45	100
590	VSetup	0	0	2001-07-28 20:23:40	0	D	Y	Initial Client Setup	\N	M	2000-01-02 00:00:00	0
591	VTrxMaterial	0	0	2001-07-28 20:25:25	0	D	Y	Material Transaction Info	\N	M	2000-01-02 00:00:00	0
592	Value	0	0	2000-01-13 20:22:27	0	D	Y	&Key	\N	I	2005-12-08 20:58:00	100
593	ValuePreference	0	0	2001-10-25 17:36:25	0	D	Y	Value Preference	\N	M	2000-01-02 00:00:00	0
594	ValuePreferenceDeleted	0	0	2001-10-25 17:39:22	0	D	Y	Preference Value deleted	\N	I	2005-11-28 17:02:47	100
595	ValuePreferenceInserted	0	0	2001-10-25 17:40:13	0	D	Y	Preference Value set	\N	I	2005-11-28 17:02:49	100
596	ValuePreferenceNotFound	0	0	2001-10-25 17:39:51	0	D	Y	Preference Value NOT found	\N	I	2005-11-28 17:02:52	100
597	ValuePreferenceNotInserted	0	0	2001-10-25 17:40:44	0	D	Y	Preference Value NOT set	\N	I	2005-11-28 17:03:03	100
598	ValuePreferenceSetFor	0	0	2001-10-25 17:37:46	0	D	Y	For level	\N	M	2000-01-02 00:00:00	0
599	View	0	0	2000-07-06 20:56:56	0	D	Y	&View	\N	M	2000-01-02 00:00:00	0
600	ViewerGraph	0	0	2001-11-25 21:04:15	0	D	Y	Graph	\N	M	2000-01-02 00:00:00	0
601	ViewerOptions	0	0	2001-12-02 13:50:39	0	D	Y	Enter Selection and Display criteria and start Query	\N	M	2000-01-02 00:00:00	0
602	ViewerQuery	0	0	2001-11-25 21:02:52	0	D	Y	&Enter Query	\N	M	2005-12-08 20:02:48	100
603	ViewerResult	0	0	2001-11-25 21:03:32	0	D	Y	View Result	\N	M	2000-01-02 00:00:00	0
604	WFCannotStart	0	0	1999-08-15 00:00:00	0	D	Y	Cannot start Workflow - Definition not correct	\N	E	2000-01-02 00:00:00	0
605	WFExit	0	0	1999-06-09 00:00:00	0	D	Y	Exit Workflow	\N	M	2000-01-02 00:00:00	0
606	WFNext	0	0	1999-06-09 00:00:00	0	D	Y	Next workflow step	\N	M	2000-01-02 00:00:00	0
607	WFPrevious	0	0	1999-06-09 00:00:00	0	D	Y	Previous workflow step	\N	M	2000-01-02 00:00:00	0
608	WFStart	0	0	1999-06-09 00:00:00	0	D	Y	Back to start of workflow	\N	M	2000-01-02 00:00:00	0
609	WLoginBrowserNote	0	0	2001-10-30 02:56:49	0	D	Y	Please upgrade your Browser, if dependent field selections remain empty after change of Role.	\N	I	2000-01-02 00:00:00	0
610	WLoginNoDB	0	0	2001-10-30 02:57:45	0	D	Y	Database currently not available	\N	E	2000-01-02 00:00:00	0
611	WaitingPayment	0	0	2000-04-26 08:48:51	0	D	Y	Waiting for Payment (if required you can close the order)	\N	E	2000-01-02 00:00:00	0
612	Warehouse	0	0	2000-01-13 20:23:27	0	D	Y	&Warehouse	\N	I	2005-12-08 20:59:04	100
613	Who	0	0	2000-12-17 22:54:11	0	D	Y	Record Info 	\N	M	2000-01-02 00:00:00	0
614	Window	0	0	2001-01-09 16:57:30	0	D	Y	Window	\N	M	2000-01-02 00:00:00	0
615	WriteOff	0	0	2001-01-09 21:01:33	0	D	Y	Write-off	\N	M	2000-01-02 00:00:00	0
616	Y	0	0	2001-11-05 01:20:30	0	D	Y	Yes	\N	I	2000-01-02 00:00:00	0
617	Yes	0	0	1999-08-18 00:00:00	0	D	Y	Yes	\N	I	2000-01-02 00:00:00	0
618	Zoom	0	0	1999-05-21 00:00:00	0	D	Y	&Zoom	\N	M	2005-11-28 17:06:51	100
619	ZoomDocument	0	0	2001-05-12 17:01:30	0	D	Y	Document Zoom	\N	I	2000-01-02 00:00:00	0
620	of	0	0	2000-04-09 19:53:07	0	D	Y	of	\N	I	2005-12-24 12:44:40	100
621	PrintOnlyRecentShipment?	0	0	2002-06-15 04:25:41	0	D	Y	Print only most recent Shipment ?\n(No: print all shipments of order)	\N	I	2000-01-02 00:00:00	0
622	VAssignmentDialog	0	0	2002-06-22 21:49:21	0	D	Y	Resource Assignment	\N	I	2000-01-02 00:00:00	0
623	ExpenseSummary	0	0	2002-06-22 23:43:05	0	D	Y	{0} Line(s) - Total: {1,number,#,##0.00} {2}	\N	I	2000-01-02 00:00:00	0
624	Week	0	0	2002-06-23 21:30:48	0	D	Y	Week	\N	I	2000-01-02 00:00:00	0
625	Day	0	0	2002-06-23 21:31:13	0	D	Y	Day	\N	I	2000-01-02 00:00:00	0
626	Month	0	0	2002-06-23 21:31:32	0	D	Y	Month	\N	I	2000-01-02 00:00:00	0
627	InfoResource	0	0	2002-06-23 21:32:14	0	D	Y	Resource Info	\N	M	2000-01-02 00:00:00	0
628	ResourceNotAvailable	0	0	2002-06-23 21:38:54	0	D	Y	Resource is not available or not active	\N	I	2000-01-02 00:00:00	0
629	ResourceUnAvailable	0	0	2002-06-23 21:41:46	0	D	Y	Resource is unavailable	\N	I	2000-01-02 00:00:00	0
630	NonBusinessDay	0	0	2002-06-23 21:44:22	0	D	Y	Not a business day	\N	I	2000-01-02 00:00:00	0
631	ResourceNotInSlotTime	0	0	2002-06-23 21:46:05	0	D	Y	Time not available	\N	I	2000-01-02 00:00:00	0
632	ResourceNotInSlotDay	0	0	2002-07-03 04:33:15	0	D	Y	Day not  available	\N	I	2000-01-02 00:00:00	0
633	InfoSchedule	0	0	2002-07-03 04:37:30	0	D	Y	Schedule Info	\N	M	2000-01-02 00:00:00	0
634	Landscape	0	0	2002-07-24 17:16:10	0	D	Y	Landscape	\N	I	2000-01-02 00:00:00	0
635	Portrait	0	0	2002-07-24 17:16:50	0	D	Y	Portrait	\N	I	2000-01-02 00:00:00	0
636	NoCurrencyConversion	0	0	2002-07-26 18:25:53	0	D	Y	No Currency Conversion Rate between currencies found	\N	E	2000-01-02 00:00:00	0
637	Operator	0	0	2002-08-04 14:04:22	0	D	Y	Operator	\N	I	2000-01-02 00:00:00	0
638	QueryValue	0	0	2002-08-04 14:04:54	0	D	Y	Query Value	\N	I	2000-01-02 00:00:00	0
639	QueryValue2	0	0	2002-08-04 14:05:28	0	D	Y	To Query Value	\N	I	2000-01-02 00:00:00	0
640	ValidationError	0	0	2002-08-04 14:06:05	0	D	Y	Validation Error	\N	E	2000-01-02 00:00:00	0
641	DataRows	0	0	2002-08-05 22:28:38	0	D	Y	Data Rows	\N	I	2000-01-02 00:00:00	0
642	DataCols	0	0	2002-08-05 22:29:31	0	D	Y	Data Columns	\N	I	2000-01-02 00:00:00	0
643	NoDocPrintFormat	0	0	2002-08-16 22:23:01	0	D	Y	No Document Print Format defined	\N	E	2000-01-02 00:00:00	0
644	PrintFormatTrl	0	0	2002-08-24 15:48:56	0	D	Y	Translate Print Format	If you do not have Multi-Lingual Document enabled, you can translate a Report here.	I	2000-01-02 00:00:00	0
645	Translate	0	0	2002-08-24 15:51:44	0	D	Y	Translate	\N	M	2000-01-02 00:00:00	0
646	NewReport	0	0	2002-08-24 15:52:31	0	D	Y	New Report	\N	M	2000-01-02 00:00:00	0
647	Average	0	0	2002-08-24 15:53:44	0	D	Y	Average	\N	I	2000-01-02 00:00:00	0
648	Count	0	0	2002-08-24 15:54:08	0	D	Y	Count	\N	I	2000-01-02 00:00:00	0
649	ScreenShot	0	0	2002-08-29 16:32:25	0	D	Y	Screen Shot	\N	I	2000-01-02 00:00:00	0
650	DatabaseVersionError	0	0	2002-09-02 17:43:41	0	D	Y	Database Version Error	The program assumes Database version {0}, but Database has Version {1}. \nThis is likely to cause hard to fix errors. \nPlease stop and migrate the database immediately.	E	2000-01-02 00:00:00	0
651	AlwaysPrintPreview	0	0	2002-09-07 19:30:08	0	D	Y	Always Preview Print	Don't print direct - even Documents	I	2000-01-02 00:00:00	0
652	All	0	0	2002-09-14 13:37:43	0	D	Y	All	\N	I	2000-01-02 00:00:00	0
653	Year	0	0	2002-09-14 13:38:05	0	D	Y	Year	\N	I	2000-01-02 00:00:00	0
654	VOnlyCurrentDays	0	0	2002-09-14 13:39:20	0	D	Y	How long back in History?	View history records back in time.	I	2000-01-02 00:00:00	0
655	ToBeMatched	0	0	2002-10-01 21:47:26	0	D	Y	To be matched	\N	I	2000-01-02 00:00:00	0
656	StorePassword	0	0	2002-10-26 00:00:17	0	D	Y	Store Password	Store Password for fast Login (security risk)	I	2000-01-02 00:00:00	0
657	InfoCashLine	0	0	2002-11-11 16:47:02	0	D	Y	Cash Journal Info	\N	M	2000-01-02 00:00:00	0
658	InfoAssignment	0	0	2002-11-11 16:48:30	0	D	Y	Resource Info	\N	M	2000-01-02 00:00:00	0
659	AbsoluteAmt	0	0	2002-11-11 17:29:26	0	D	Y	Absolute Amount	\N	I	2000-01-02 00:00:00	0
660	Import	0	0	2003-01-04 15:28:18	0	D	Y	Import	\N	I	2000-01-02 00:00:00	0
661	Errors	0	0	2003-01-13 18:02:28	0	D	Y	Errors	\N	E	2000-01-02 00:00:00	0
662	Purchase	0	0	2003-01-13 18:03:29	0	D	Y	Purchase	\N	I	2000-01-02 00:00:00	0
663	NoAppsServer	0	0	2003-01-15 15:41:19	0	D	Y	No Application Server found	\N	I	2000-01-02 00:00:00	0
664	Optional	0	0	2003-01-15 21:19:20	0	D	Y	Optional	\N	I	2000-01-02 00:00:00	0
665	LoadAccountingValues	0	0	2003-01-15 21:22:50	0	D	Y	Load Accounting Values	Format: Accounting__.csv	I	2000-01-02 00:00:00	0
666	TaxNoExemptFound	0	0	2003-01-17 17:03:04	0	D	Y	No Tax Exempt Rate found (define a tax exempt tax rate)	\N	E	2005-04-18 23:04:08	100
667	NoDBConnection	0	0	2003-01-19 17:21:51	0	D	Y	No Database Connection	\N	I	2000-01-02 00:00:00	0
668	InOutGenerateGen	0	0	2003-01-26 21:30:18	0	D	Y	Generating shipments	\N	I	2000-01-02 00:00:00	0
670	InOutGenerateSel	0	0	2003-01-26 21:31:17	0	D	Y	Select orders to generate shipments	\N	I	2000-01-02 00:00:00	0
671	InOutGenerateInfo	0	0	2003-01-26 21:34:26	0	D	Y	Shipments are generated depending on the "Delivery Rule" selection in the Order	\N	I	2000-01-02 00:00:00	0
672	Shipment	0	0	2003-01-26 23:32:01	0	D	Y	Shipment	\N	I	2000-01-02 00:00:00	0
673	Sent	0	0	2003-01-30 18:19:55	0	D	Y	Sent	\N	I	2000-01-02 00:00:00	0
674	RestrictSelection	0	0	2003-01-30 18:30:42	0	D	Y	You need to restrict the selection.	\N	E	2000-01-02 00:00:00	0
675	BeginningBalance	0	0	2003-02-15 00:55:35	0	D	Y	Beginning Balance	\N	I	2000-01-02 00:00:00	0
676	InvalidArguments	0	0	2003-03-15 10:51:10	0	D	Y	Invalid Arguments - Check Parameters	\N	E	2000-01-02 00:00:00	0
677	ChargeExclusively	0	0	2003-03-17 23:35:28	0	D	Y	Charge cannot be selected, if there is an existing Product selection (either-or). A charge needs to have it's own line.	\N	E	2005-04-18 23:01:51	100
678	PaymentDocTypeInvoiceInconsistent	0	0	2003-03-18 17:00:28	0	D	Y	Payment Document Type and Invoice type (AP/AR) inconsistent.	\N	E	2000-01-02 00:00:00	0
679	InvoiceReSelect	0	0	2003-03-19 19:28:12	0	D	Y	(Re) Select Invoice	\N	E	2000-01-02 00:00:00	0
680	OnlinePaymentFailed	0	0	2003-04-17 22:45:10	0	D	Y	Online Payment Failed	\N	E	2000-01-02 00:00:00	0
688	LoadError	0	0	2003-07-19 00:00:00	0	D	Y	Load Error	\N	E	2000-01-02 00:00:00	0
689	MergeFrom	0	0	2003-08-09 13:23:58	0	D	Y	Merge From (deleted)	\N	I	2000-01-02 00:00:00	0
690	MergeTo	0	0	2003-08-09 13:24:27	0	D	Y	Merge To (surviving)	\N	I	2000-01-02 00:00:00	0
691	MergeQuestion	0	0	2003-08-09 13:33:59	0	D	Y	Merge From entity to To entity ?\n** NO Undo nor Trace -- You need to have a Backup **	\N	I	2000-01-02 00:00:00	0
692	MergeSuccess	0	0	2003-08-09 18:58:27	0	D	Y	Merge Success	\N	I	2000-01-02 00:00:00	0
693	MergeError	0	0	2003-08-09 18:59:29	0	D	Y	Merge Error - Please review:	\N	E	2000-01-02 00:00:00	0
694	LanguageSetupError	0	0	2003-08-17 19:56:08	0	D	Y	Language Setup Error	Check Language Setup	E	2000-01-02 00:00:00	0
695	PAttributeNoAttributeSet	0	0	2003-08-26 23:13:30	0	D	Y	No Product Attribute Set defined	\N	E	2000-01-02 00:00:00	0
696	PAttributeNoInfo	0	0	2003-08-26 23:20:01	0	D	Y	No Product Attribute Information	\N	E	2000-01-02 00:00:00	0
697	PAttributeNoSelection	0	0	2003-08-30 00:12:09	0	D	Y	No Products with Attributes to select	\N	E	2000-01-02 00:00:00	0
698	Context	0	0	2003-09-30 12:37:46	0	D	Y	Context	\N	I	2000-01-02 00:00:00	0
699	InfoAsset	0	0	2003-10-01 16:01:33	0	D	Y	Asset Info	\N	M	2000-01-02 00:00:00	0
700	Reset	0	0	2003-10-02 20:54:39	0	D	Y	Reset	\N	I	2000-01-02 00:00:00	0
701	PAttribute	0	0	2003-10-03 00:48:22	0	D	Y	Product Attributes	\N	I	2000-01-02 00:00:00	0
702	PAttributeInstance	0	0	2003-10-03 23:15:04	0	D	Y	Product Attribute Instance	\N	I	2000-01-02 00:00:00	0
703	SQL	0	0	2003-10-04 17:57:41	0	D	Y	SQL Statement	\N	I	2000-01-02 00:00:00	0
704	Lock	0	0	2003-10-04 17:58:18	0	D	Y	Private Record Lock	\N	I	2000-01-02 00:00:00	0
705	InfoPAttribute	0	0	2003-10-04 20:51:52	0	D	Y	Product Attribute Info	\N	M	2000-01-02 00:00:00	0
706	AccessCannotReport	0	0	2003-10-21 22:45:47	0	D	Y	You cannot create Reports for this Information.	You don't have the privileges	E	2000-01-02 00:00:00	0
707	AccessCannotExport	0	0	2003-10-21 22:46:12	0	D	Y	You cannot export this Information.	You don't have the privileges	E	2000-01-02 00:00:00	0
708	AccessClient	0	0	2003-10-25 12:30:18	0	D	Y	Client Data	\N	I	2000-01-02 00:00:00	0
709	RecordAccessDialog	0	0	2003-10-26 16:14:29	0	D	Y	Record Access Dialog	\N	M	2000-01-02 00:00:00	0
710	RoleInfo	0	0	2003-10-31 21:01:04	0	D	Y	Role Info	\N	M	2000-01-02 00:00:00	0
711	Include	0	0	2003-10-31 22:19:34	0	D	Y	Include	\N	I	2000-01-02 00:00:00	0
712	Exclude	0	0	2003-10-31 22:20:04	0	D	Y	Exclude	\N	I	2000-01-02 00:00:00	0
713	AdempiereSys	0	0	2003-11-02 16:07:02	0	D	Y	Dictionary Maintenance	Only for internal Adempiere Dictionary Maintenence - DO NOT SELECT	I	2000-01-02 00:00:00	0
714	NoTranslation	0	0	2003-12-04 15:58:04	0	D	Y	No Translation	\N	I	2000-01-02 00:00:00	0
715	SelectRecord	0	0	2003-12-06 12:21:11	0	D	Y	Select existing Record	\N	I	2000-01-02 00:00:00	0
716	NewRecord	0	0	2003-12-06 12:23:33	0	D	Y	New Record	\N	I	2000-01-02 00:00:00	0
717	EditRecord	0	0	2003-12-06 12:23:57	0	D	Y	Edit Record	\N	I	2000-01-02 00:00:00	0
718	ShowAll	0	0	2003-12-06 13:37:04	0	D	Y	Show All	\N	I	2000-01-02 00:00:00	0
719	LinesWithoutProductAttribute	0	0	2003-12-06 21:11:58	0	D	Y	Document has Lines without MANDATORY Product Attribute Set Instance - complete first	\N	E	2000-01-02 00:00:00	0
720	SelectExisting	0	0	2003-12-07 14:52:15	0	D	Y	Select existing record	\N	I	2000-01-02 00:00:00	0
721	Size	0	0	2003-12-10 00:59:32	0	D	Y	Size	\N	I	2000-01-02 00:00:00	0
722	Bold	0	0	2003-12-10 00:59:46	0	D	Y	Bold	\N	I	2000-01-02 00:00:00	0
723	Italic	0	0	2003-12-10 01:00:02	0	D	Y	Italic	\N	I	2000-01-02 00:00:00	0
724	Underline	0	0	2003-12-10 01:00:20	0	D	Y	Underline	\N	I	2000-01-02 00:00:00	0
725	FontFamily	0	0	2003-12-10 01:01:33	0	D	Y	Font Family	\N	I	2000-01-02 00:00:00	0
726	FontStyle	0	0	2003-12-10 01:01:50	0	D	Y	Font Style	\N	I	2000-01-02 00:00:00	0
727	Left	0	0	2003-12-10 01:02:52	0	D	Y	Left	\N	I	2000-01-02 00:00:00	0
728	Center	0	0	2003-12-10 01:03:06	0	D	Y	Center	\N	I	2000-01-02 00:00:00	0
729	Right	0	0	2003-12-10 01:03:17	0	D	Y	Right	\N	I	2000-01-02 00:00:00	0
730	Align	0	0	2003-12-10 01:04:35	0	D	Y	Align	\N	I	2000-01-02 00:00:00	0
731	Font	0	0	2003-12-10 01:12:03	0	D	Y	Font	\N	I	2000-01-02 00:00:00	0
732	SystemNotSetupForReplication	0	0	2003-12-10 21:22:32	0	D	Y	System Not Setup for Replication (see log)	\N	I	2000-01-02 00:00:00	0
734	Mean	0	0	2003-12-17 21:44:49	0	D	Y	Mean	\N	I	2000-01-02 00:00:00	0
735	Min	0	0	2003-12-17 21:45:10	0	D	Y	Minimum	\N	I	2000-01-02 00:00:00	0
736	Max	0	0	2003-12-17 21:45:23	0	D	Y	Maximum	\N	I	2000-01-02 00:00:00	0
737	Variance	0	0	2003-12-17 21:45:49	0	D	Y	Variance	\N	I	2000-01-02 00:00:00	0
738	Deviation	0	0	2003-12-17 21:46:31	0	D	Y	Std.Deviation	\N	I	2000-01-02 00:00:00	0
739	RunningTotal	0	0	2003-12-18 17:40:00	0	D	Y	Running Total	\N	I	2000-01-02 00:00:00	0
740	SelectProduct	0	0	2003-12-30 11:06:29	0	D	Y	Select Product	\N	I	2000-01-02 00:00:00	0
741	MemoryInfo	0	0	2004-01-06 23:58:57	0	D	Y	Total VM Memory {0,number,integer} kB - Free {1,number,integer} kB	\N	I	2000-01-02 00:00:00	0
743	FileSSV	0	0	2004-01-10 00:14:25	0	D	Y	ssv - Semicolon Separated Values file	\N	E	2000-01-02 00:00:00	0
744	AttachmentSave	0	0	2004-01-15 13:06:26	0	D	Y	Save Attachment to Disk	\N	I	2000-01-02 00:00:00	0
745	AttachmentDeleteEntry?	0	0	2004-01-15 16:45:14	0	D	Y	Do you want to delete this Attachment entry?	\N	I	2000-01-02 00:00:00	0
746	FileJPEG	0	0	2004-01-16 20:25:13	0	D	Y	jpg - JPEG Graphic file	\N	E	2000-01-02 00:00:00	0
747	AssetDeliveryTemplate	0	0	2004-01-20 22:36:13	0	D	Y	Download {0} for {1}\n\nVersion = {2} - Lot = {3} - SerNo = {4}\nGuarantee Date = {5,date,short}\n\nThank you for using Adempiere Customer Asset Management\n	\N	I	2000-01-02 00:00:00	0
748	AllocationWriteOffWarn	0	0	2004-03-22 12:31:37	0	D	Y	You are writing off a significant percentage of the open amount.	Deselect Automatic Write-off to manually enter the write-off amount	I	2000-01-02 00:00:00	0
749	Answer	0	0	2004-03-25 12:34:12	0	D	Y	Answer	\N	I	2000-01-02 00:00:00	0
750	Forward	0	0	2004-03-25 12:34:28	0	D	Y	Forward	\N	I	2000-01-02 00:00:00	0
751	WFNoActivities	0	0	2004-03-25 12:52:02	0	D	Y	You have no Activities	\N	I	2000-01-02 00:00:00	0
752	WFActivities	0	0	2004-03-25 12:52:58	0	D	Y	Your Workflow Activities	\N	I	2000-01-02 00:00:00	0
753	WorkflowResult	0	0	2004-04-15 01:49:12	0	D	Y	Workflow Result	\N	I	2000-01-02 00:00:00	0
754	CannotForward	0	0	2004-04-16 21:43:56	0	D	Y	Cannot Forward	\N	E	2000-01-02 00:00:00	0
755	WFActiveForRecord	0	0	2004-04-20 20:08:05	0	D	Y	Active Workflow for this Record exists (complete first):	\N	I	2000-01-02 00:00:00	0
756	PeriodClosed	0	0	2004-05-08 22:13:16	0	D	Y	Period Closed	\N	E	2000-01-02 00:00:00	0
757	WorkFlow	0	0	2004-05-09 09:50:46	0	D	Y	Active &Workflows	\N	M	2005-11-28 17:11:09	100
758	NoZoomTarget	0	0	2004-05-10 22:28:39	0	D	Y	No Zoom Target for this Record	\N	I	2005-02-08 18:43:15	100
759	ZoomAcross	0	0	2004-05-10 23:14:07	0	D	Y	&Zoom Across (where used)	\N	I	2005-11-28 17:06:42	100
760	Order	0	0	2004-05-12 01:29:40	0	D	Y	Order	\N	I	2000-01-02 00:00:00	0
761	CounterDoc	0	0	2004-05-12 01:30:54	0	D	Y	Counter Document	\N	I	2000-01-02 00:00:00	0
762	WinSize	0	0	2004-05-12 22:01:19	0	D	Y	Set Window Size	\N	M	2000-01-02 00:00:00	0
763	WinSizeSet	0	0	2004-05-13 14:23:26	0	D	Y	Set Window Size for all users\n(Cancel to reset to default)	\N	I	2000-01-02 00:00:00	0
764	DocControlledError	0	0	2004-05-17 12:23:23	0	D	Y	Account used is Document Controlled - Do not use for manual Journals.	\N	E	2000-01-02 00:00:00	0
765	WarehouseOrgConflict	0	0	2004-06-19 11:29:16	0	D	Y	Warehouse Organization is not Document Organization	\N	I	2000-01-02 00:00:00	0
766	BPartnerCreditStop	0	0	2004-07-02 17:38:43	0	D	Y	Business Partner is on Credit Stop	\N	I	2000-01-02 00:00:00	0
767	BPartnerCreditHold	0	0	2004-07-02 17:38:43	0	D	Y	Business Partner is on Credit Hold	\N	I	2000-01-02 00:00:00	0
768	BPartnerOverOCreditHold	0	0	2004-07-02 17:38:43	0	D	Y	Business Partner with this Order over Credit Hold	\N	I	2000-01-02 00:00:00	0
769	BPartnerOverSCreditHold	0	0	2004-07-02 17:38:43	0	D	Y	Business Partner with open Shipments over Credit Hold	\N	I	2000-01-02 00:00:00	0
770	NotInvoicedAmt	0	0	2004-07-02 21:05:05	0	D	Y	Not Invoiced Amount	\N	I	2000-01-02 00:00:00	0
771	OldPasswordMandatory	0	0	2004-07-04 12:27:03	0	D	Y	Old Password is Mandatory	\N	E	2000-01-02 00:00:00	0
772	OldPasswordNoMatch	0	0	2004-07-04 12:27:03	0	D	Y	Old Password does not match	\N	E	2000-01-02 00:00:00	0
773	NoPOSForUser	0	0	2004-07-09 16:09:42	0	D	Y	No POS Terminal defined for User	\N	I	2000-01-02 00:00:00	0
774	SelectPOS	0	0	2004-07-09 16:09:42	0	D	Y	Select POS Terminal	\N	I	2000-01-02 00:00:00	0
775	Register	0	0	2004-07-10 16:51:58	0	D	Y	Register	\N	I	2000-01-02 00:00:00	0
776	Summary	0	0	2004-07-10 16:52:23	0	D	Y	Summary	\N	I	2000-01-02 00:00:00	0
777	Product	0	0	2004-07-10 16:54:59	0	D	Y	&Product	\N	I	2005-12-08 21:12:11	100
778	BPartner	0	0	2004-07-10 16:55:31	0	D	Y	&Business Partner	\N	I	2005-12-08 21:07:08	100
779	Checkout	0	0	2004-07-10 18:15:07	0	D	Y	Checkout	\N	I	2000-01-02 00:00:00	0
780	CashGiven	0	0	2004-07-10 18:16:12	0	D	Y	Cash Given	\N	I	2000-01-02 00:00:00	0
781	CashReturn	0	0	2004-07-10 18:16:35	0	D	Y	Return	\N	I	2000-01-02 00:00:00	0
782	Cash	0	0	2004-07-10 18:16:46	0	D	Y	Cash	\N	I	2000-01-02 00:00:00	0
783	CreditCardExp	0	0	2004-07-10 18:19:56	0	D	Y	Exp MM/YY	\N	I	2000-01-02 00:00:00	0
784	CurrentLine	0	0	2004-07-10 18:22:53	0	D	Y	Current Line	\N	I	2000-01-02 00:00:00	0
785	Logout	0	0	2004-07-10 20:08:56	0	D	Y	Log Out	\N	I	2000-01-02 00:00:00	0
786	Plus	0	0	2004-07-11 12:11:57	0	D	Y	Plus (add)	\N	I	2000-01-02 00:00:00	0
787	Minus	0	0	2004-07-11 12:12:12	0	D	Y	Minus (subtract)	\N	I	2000-01-02 00:00:00	0
788	PAttributeNoInstanceAttribute	0	0	2003-08-26 23:13:30	0	D	Y	Product Attribute Set has no Instance attributes (e.g. Serial No)	\N	E	2000-01-02 00:00:00	0
789	NoQtyAvailable	0	0	2004-07-24 01:08:23	0	D	Y	No Inventory Available:	\N	E	2000-01-02 00:00:00	0
790	ProductUOMConversionRateError	0	0	2004-07-26 14:23:34	0	D	Y	The Product UoM needs to be the smallest UoM	Divide Rate  must be >= 1 and always result in a valid UoM unit. \nExample: To convert Each to Pair, the Multiply Rate is 0.5	E	2000-01-02 00:00:00	0
791	ProductUOMConversionUOMError	0	0	2004-07-26 14:23:34	0	D	Y	Select the Product UoM as the From Unit of Measure:	\N	E	2000-01-02 00:00:00	0
792	QtyUnconfirmed	0	0	2004-07-26 20:26:03	0	D	Y	Unconfirmed Qty	\N	I	2005-10-26 22:45:11	100
793	Replenishment	0	0	2004-08-26 01:23:38	0	D	Y	Inventory Replenishment	\N	I	2000-01-02 00:00:00	0
794	RequestDue	0	0	2004-08-30 22:55:17	0	D	Y	Request {0} - Due	\N	I	2000-01-02 00:00:00	0
795	RequestInactive	0	0	2004-08-30 22:56:57	0	D	Y	Inactivity Alert: Request {0}	\N	I	2000-01-02 00:00:00	0
796	WorflowNotValid	0	0	2004-09-01 21:49:28	0	D	Y	Workflow Not Valid	\N	I	2005-04-18 23:05:21	100
797	ActivityInactivity	0	0	2004-09-01 21:50:51	0	D	Y	Inactivity Alert: Workflow Activity {0}	\N	I	2000-01-02 00:00:00	0
798	ActivityEndWaitTime	0	0	2004-09-01 21:56:51	0	D	Y	Alert Workflow Activity: Max Wait Time Ended {0}	\N	I	2000-01-02 00:00:00	0
799	ActivityOverPriority	0	0	2004-09-02 01:31:11	0	D	Y	Alert Workflow Activity Over Priority {0}	\N	I	2000-01-02 00:00:00	0
800	Failed	0	0	2004-09-25 22:44:34	0	D	Y	Failed	\N	I	2000-01-02 00:00:00	0
801	Voided	0	0	2004-10-01 12:16:12	0	D	Y	** Voided	\N	I	2000-01-02 00:00:00	0
802	UnbalancedJornal	0	0	2004-10-09 22:38:24	0	D	Y	Unbalanced Journal and Suspense Balancing not enabled	\N	E	2000-01-02 00:00:00	0
803	Selection	0	0	2004-10-09 23:09:25	0	D	Y	Selection	\N	I	2000-01-02 00:00:00	0
804	Display	0	0	2004-10-09 23:10:08	0	D	Y	Display	\N	I	2000-01-02 00:00:00	0
805	ProductNotOnPriceList	0	0	2004-10-10 21:05:12	0	D	Y	Product is not on PriceList	\N	I	2000-01-02 00:00:00	0
806	Added	0	0	2004-10-26 11:01:03	0	D	Y	Added	\N	I	2000-01-02 00:00:00	0
807	AccountSetupError	0	0	2004-11-06 12:14:38	0	D	Y	Error while creating the Accounting Setup. \nCheck the error log and the format of the Accounting*.cvs file - No duplicate Accounts and all all defaut accounts must be included (even if you will not use them).	\N	E	2000-01-02 00:00:00	0
808	InternalUseNeedsCharge	0	0	2004-11-26 21:35:05	0	D	Y	For Internal Use of Inventory you need to define a Charge	\N	E	2000-01-02 00:00:00	0
809	SaveFile	0	0	2005-01-02 00:51:45	100	D	Y	Save to File	\N	I	2005-01-02 00:51:45	100
810	ErrorsOnly	0	0	2005-01-02 00:54:45	100	D	Y	Errors Only	\N	I	2005-01-02 00:54:45	100
811	TraceLevel	0	0	2005-01-02 00:57:40	100	D	Y	Trace Level	Detail of Trace Information - INFO is usually sufficient	I	2005-01-02 00:57:40	100
812	TraceInfo	0	0	2005-01-02 00:58:14	100	D	Y	Trace Information	\N	I	2005-01-02 00:58:14	100
813	TraceFile	0	0	2005-01-02 01:03:34	100	D	Y	Trace File	Create Trace file in Adempiere directory or in user home directory	I	2005-01-02 01:03:34	100
814	ServerProcess	0	0	2005-01-07 14:16:38	100	D	Y	Run Processes on Server	Run Processes on Application Server (slow communication lines)	I	2005-01-07 14:16:38	100
815	RequestNew	0	0	2005-02-08 22:09:25	100	D	Y	Create New Request	\N	I	2005-02-08 22:09:25	100
816	RequestAll	0	0	2005-02-08 22:09:52	100	D	Y	Show all Requests	\N	I	2005-02-08 22:09:52	100
817	RequestActive	0	0	2005-02-08 22:10:37	100	D	Y	Show active Requests	\N	I	2005-02-08 22:10:37	100
818	ArchivedDocuments	0	0	2005-02-09 14:55:27	100	D	Y	Documents	\N	I	2005-02-09 14:55:27	100
819	ArchivedReports	0	0	2005-02-09 14:56:07	100	D	Y	Reports	\N	I	2005-02-09 14:56:07	100
820	ArchivedNone	0	0	2005-02-09 14:56:51	100	D	Y	No Archives for this Record	\N	I	2005-02-09 14:56:51	100
821	Request	0	0	2005-02-09 15:06:34	100	D	Y	&Check Requests	\N	I	2005-11-29 14:35:59	100
822	Archive	0	0	2005-02-09 15:07:02	100	D	Y	&Archived Documents/Reports	\N	I	2005-11-28 21:30:47	100
823	Archived	0	0	2005-02-09 15:29:50	100	D	Y	Document Archived	\N	I	2005-02-09 15:29:50	100
824	ArchiveError	0	0	2005-02-09 15:30:21	100	D	Y	Archive Error	\N	I	2005-02-09 15:30:21	100
825	ArchivedReportsAll	0	0	2005-02-09 15:47:59	100	D	Y	All Reports	\N	I	2005-02-09 15:47:59	100
826	Org0NotAllowed	0	0	2005-02-21 21:46:26	100	D	Y	Organization * (0) not allowed	\N	E	2005-02-21 21:46:26	100
827	CreditMemo	0	0	2005-02-26 02:16:09	100	D	Y	Credit Memo	\N	I	2005-02-26 02:16:09	100
828	InvoiceBatchSummary	0	0	2005-04-03 00:41:26	100	D	Y	{0} Line(s) {1,number,#,##0.00}  - Total: {2,number,#,##0.00}	\N	I	2005-04-03 00:41:26	100
829	AfterMigration	0	0	2005-04-25 00:46:57	100	D	Y	Press OK to start 'After Migration' process ... may take a few minutes	\N	I	2005-04-25 00:46:57	100
830	Invalid	0	0	2005-04-26 01:08:01	100	D	Y	Invalid:	\N	E	2005-04-26 01:08:01	100
831	ResourceAssignmentNotDeleted	0	0	2005-04-27 22:01:29	100	D	Y	Resource Assignment could Not be deleted.  It may be used in an Order or Invoice.  \nDelete Order/Invoice line with the Assignment.	\N	I	2005-04-27 22:01:29	100
832	true	0	0	2005-05-05 23:04:35	100	D	Y	Yes	\N	I	2005-05-05 23:04:35	100
833	false	0	0	2005-05-05 23:04:50	100	D	Y	No	\N	I	2005-05-05 23:04:50	100
834	RequestUpdate	0	0	2005-05-16 00:36:11	100	D	Y	Request Update	\N	I	2005-05-16 00:36:11	100
835	ShowAdvancedTab	0	0	2005-05-17 16:42:05	100	D	Y	Show Advanced Tabs	Show Tabs with advanced functionality like Matching, Allocation, etc.	M	2005-05-17 16:42:05	100
836	RequestStatusTimeout	0	0	2005-05-20 22:01:04	100	D	Y	Request Status Timeout	\N	I	2005-05-20 22:01:04	100
837	DocumentStatusChanged	0	0	2005-07-05 13:19:33	100	D	Y	Document Status changed - Requery Record	\N	E	2005-07-05 13:19:33	100
838	InfoHighRecordCount	0	0	2005-07-11 18:07:53	100	D	Y	Will retrieve high number of records - \nDo you want to continue?	\N	I	2005-07-11 18:07:53	100
839	CreateNewNode	0	0	2005-07-22 08:25:10	100	D	Y	Create new node	\N	I	2005-07-22 08:25:10	100
840	DeleteNode	0	0	2005-07-22 08:29:07	100	D	Y	Delete Node	\N	I	2005-07-22 08:29:07	100
841	AddLine	0	0	2005-07-22 08:30:15	100	D	Y	Add Line	\N	I	2005-07-22 08:30:15	100
842	DeleteLine	0	0	2005-07-22 08:30:41	100	D	Y	Delete Line	\N	I	2005-11-12 14:41:14	100
843	NotApproved	0	0	2005-07-25 11:22:30	100	D	Y	Not Approved	\N	I	2005-07-25 11:22:30	100
844	ATP	0	0	2005-08-26 13:06:59	100	D	Y	Available to Promise	\N	I	2005-08-26 13:06:59	100
845	QtyInsufficient	0	0	2005-08-31 18:47:05	100	D	Y	Insufficient Qty on Hand	\N	E	2005-08-31 18:47:13	100
846	PaymentIsAllocated	0	0	2005-09-03 10:10:57	100	D	Y	Payment is allocated (Charge, Invoice, Order)	\N	E	2005-09-03 11:05:34	100
847	CostingLevelClient	0	0	2005-09-13 21:44:15	100	D	Y	Costing Level defined on Client Level - You cannot specify Organization nor Attribute	\N	E	2005-09-13 21:44:15	100
848	CannotDeleteUsed	0	0	2005-09-15 17:42:50	100	D	Y	You cannot delete this record (it is used)	\N	E	2005-09-15 17:42:50	100
849	AlreadyExists	0	0	2005-09-21 15:51:22	100	D	Y	A record already exists	\N	E	2005-09-21 15:51:22	100
850	UnsubscribeInfo	0	0	2005-09-23 14:13:22	100	D	Y	You subscribed to this Interest Area. To unsubscribe, log in, go to Interest Area and click Un-subscribe.	\N	I	2005-09-23 14:25:10	100
851	Force	0	0	2005-10-07 16:04:19	100	D	Y	Force	\N	I	2005-10-11 13:02:59	100
852	FindOverMax	0	0	2005-10-09 13:50:54	100	D	Y	Query returned more records then allowed in role - requery	\N	I	2005-10-09 13:50:54	100
853	AutoNew	0	0	2005-10-09 15:05:05	100	D	Y	Automatic New Record	When no record exists, automatically create a New Record	I	2005-10-09 15:05:18	100
854	ForceInfo	0	0	2005-10-11 13:03:34	100	D	Y	Force Posting - Overwrite Locking	\N	I	2005-10-18 12:09:34	100
855	Alternative	0	0	2005-10-20 10:07:16	100	D	Y	Alternative	\N	I	2005-10-20 10:07:16	100
856	QtyUnconfirmedMove	0	0	2005-10-26 22:45:38	100	D	Y	Unconfirmed Move	\N	I	2005-10-26 22:45:38	100
857	ConnectionProfileChange	0	0	2005-11-20 14:22:10	100	D	Y	Connection Profile changed - You need to re-login to be effective.	\N	I	2005-11-20 14:22:10	100
858	WorkflowActivities	0	0	2005-11-29 17:57:36	100	D	Y	Workflow &Activities	\N	M	2005-11-29 17:59:03	100
859	WorkflowPanel	0	0	2005-11-29 17:59:37	100	D	Y	&Workflow	\N	M	2005-11-29 18:42:44	100
860	TreeSearch	0	0	2005-11-29 18:20:21	100	D	Y	&Lookup	\N	M	2005-11-29 18:20:34	100
861	TabSwitchJumpGo	0	0	2005-12-08 10:17:48	100	D	Y	Please select the parent tab first:	\N	E	2005-12-08 10:19:11	100
862	TabSwitchJump	0	0	2005-12-08 10:18:55	100	D	Y	Please select the parent tab before selecting a dependent tab	\N	E	2005-12-08 10:18:55	100
863	Ok	0	0	2005-12-08 11:05:28	100	D	Y	&OK	\N	M	2005-12-08 11:05:35	100
864	DocumentNo	0	0	2005-12-08 21:02:27	100	D	Y	&Document No	\N	I	2005-12-08 21:02:27	100
865	CacheWindow	0	0	2005-12-14 17:17:17	100	D	Y	Cache Windows	Cache Window Definitions on Client	I	2005-12-14 17:30:48	100
866	PAPanel	0	0	2005-12-24 11:05:10	100	D	Y	&Performance	\N	I	2005-12-24 11:05:10	100
867	SystemName	0	0	2005-12-30 09:30:35	100	D	Y	System Name	Name your Adempiere System installation, e.g. Joe Block, Inc.	I	2005-12-30 09:30:35	100
868	SupportContract	0	0	2005-12-30 12:56:01	100	D	Y	Existing Support Contract	Sign up for Adempiere Support - also supports the product development	I	2005-12-30 13:07:11	100
869	SupportContractUnits	0	0	2005-12-30 12:56:31	100	D	Y	Supported Users	Max Users of Support Contract Purchased	I	2005-12-30 12:59:58	100
870	InsufficientQtyAvailable	0	0	2006-01-08 14:18:34	100	D	Y	Insufficient Inventory Available:	\N	E	2006-01-08 14:18:34	100
871	DocProcessed	0	0	2006-01-21 18:30:06	100	D	Y	Document Processed	\N	I	2006-01-21 18:30:06	100
872	QtyNotReserved	0	0	2006-01-26 20:20:42	100	D	Y	Not reserved	\N	I	2006-01-26 20:20:42	100
873	ProcessRunning	0	0	2006-02-17 09:28:02	100	D	Y	Process started ... still running (requery later)	\N	I	2006-02-17 09:28:02	100
874	StartBackground	0	0	2006-02-17 11:33:11	100	D	Y	Start as Background Process	\N	I	2006-02-17 18:48:21	100
875	AttributeGrid	0	0	2006-03-11 14:18:44	100	D	Y	Attribute Grid	\N	I	2006-03-11 14:20:54	100
876	Mode	0	0	2006-03-11 14:24:21	100	D	Y	Mode	\N	I	2006-03-11 14:24:21	100
877	ModeView	0	0	2006-03-11 14:26:17	100	D	Y	View	\N	I	2006-03-11 14:26:17	100
878	ModePO	0	0	2006-03-11 14:26:55	100	D	Y	Create PO	\N	I	2006-03-11 14:27:04	100
879	ModePrice	0	0	2006-03-11 14:28:05	100	D	Y	Price update	\N	I	2006-03-11 14:28:05	100
880	URLnotValid	0	0	2006-03-17 09:22:28	100	D	Y	URL not valid or cannot be displayed.	\N	I	2006-03-17 09:22:28	100
881	NoLines	0	0	2006-04-18 09:59:40	100	D	Y	No Document Lines found	\N	I	2006-04-18 09:59:40	100
882	Chat	0	0	2006-04-22 16:39:57	100	D	Y	Chat	\N	I	2006-04-22 16:39:57	100
883	NotValid	0	0	2006-04-24 13:47:18	100	D	Y	Not Valid	\N	I	2006-04-24 13:47:18	100
884	SchedulerResult	0	0	2006-06-12 17:50:53	100	D	Y	Scheduler Result	\N	I	2006-06-12 17:50:53	100
885	PaymentBPBankNotFound	0	0	2006-10-28 00:00:00	0	D	Y	Bank account for business partner not found	\N	E	2006-10-28 00:00:00	0
886	UserNotVerified	0	0	2006-10-29 00:00:00	0	D	Y	User EMail address was not verified - Please verify EMail	\N	E	2006-10-29 00:00:00	0
887	UserNotSubscribed	0	0	2006-10-29 00:00:00	0	D	Y	User Subscription not found	\N	E	2006-10-29 00:00:00	0
888	SelectAll	0	0	2006-10-30 00:00:00	0	D	Y	Select &All	\N	M	2006-10-31 00:00:00	0
50000	CloseAllWindows	0	0	2006-12-12 01:06:41	0	D	Y	Close All Windows	\N	I	2006-12-12 01:06:41	0
50001	CloseOtherWindows	0	0	2006-12-12 01:06:41	0	D	Y	Close Other Windows	\N	I	2006-12-12 01:06:41	0
50002	ValidateConnectionOnStartup	0	0	2006-12-12 01:06:41	0	D	Y	Validate Connection on Startup	\N	I	2006-12-12 01:06:41	0
50003	SingleInstancePerWindow	0	0	2006-12-12 01:06:41	0	D	Y	Single Instance per Window	\N	I	2006-12-12 01:06:41	0
50004	OpenWindowMaximized	0	0	2006-12-12 01:06:41	0	D	Y	Open Window Maximized	\N	I	2006-12-12 01:06:41	0
50005	DeleteSelection	0	0	2006-12-12 01:06:41	0	D	Y	Delete Selected Items	\N	I	2006-12-12 01:06:41	0
50006	SaveParentFirst	0	0	2006-12-12 01:06:41	0	D	Y	Save Parent Tab First	\N	I	2006-12-12 01:06:41	0
50007	Charset	0	0	2007-02-12 00:00:00	0	D	Y	Charset	Charset used for import / export	I	2007-02-12 00:00:00	0
50008	ImplementationVendor	0	0	2007-02-26 00:00:00	0	D	Y	Implementation Vendor	\N	I	2007-02-26 00:00:00	0
50009	ImplementationVersion	0	0	2007-02-26 00:00:00	0	D	Y	Implementation Version	\N	I	2007-02-26 00:00:00	0
50010	StoreAttachmentWarning	0	0	2007-02-26 12:30:00	100	D	Y	If you change the attachment storage method, the old attachments are no longer available to your client.	\N	I	2007-02-26 12:30:00	100
50011	AttachmentPathWarning	0	0	2007-02-26 12:30:00	100	D	Y	Make sure to copy the attachments to the new path!	\N	I	2007-02-26 12:30:00	100
50014	ProductCategoryLoopDetected	0	0	2007-04-24 00:00:00	100	D	Y	A loop in the product category tree has been detected - the old value will be restored	\N	E	2007-04-24 00:00:00	100
50015	StoreArchiveWarning	0	0	2007-02-26 00:00:00	100	D	Y	If you change the archive storage method, the old archive entries are no longer available to your client.	\N	I	2007-02-26 00:00:00	100
50016	ArchivePathWarning	0	0	2007-02-26 00:00:00	100	D	Y	Make sure to copy the archive entries to the new path!	\N	I	2007-02-26 00:00:00	100
50017	PrintPreview	0	0	2007-06-12 18:00:00	100	D	Y	Print preview	\N	I	2007-06-12 18:00:00	100
50018	CC	0	0	2007-08-02 00:15:12	100	D	Y	CC	\N	M	2007-08-02 00:15:12	100
52000	OrderOrRMA	0	0	2007-07-05 00:00:00	100	D	Y	Either Order or RMA can be process on this document.	\N	E	2007-07-05 00:00:00	100
52001	VendorRMA	0	0	2007-07-05 00:00:00	100	D	Y	Vendor RMA	\N	I	2007-07-05 00:00:00	100
52003	1000_rupees	0	0	2008-03-26 13:20:02.286	100	D	Y	1000 Rupees	 	I	2008-03-26 13:20:02.286	100
52004	100_dollars	0	0	2008-03-26 13:20:02.288	100	D	Y	100 dollars	 	I	2008-03-26 13:20:02.288	100
52005	100_rupees	0	0	2008-03-26 13:20:02.29	100	D	Y	100 Rupees	 	I	2008-03-26 13:20:02.29	100
52006	10_dollars	0	0	2008-03-26 13:20:02.292	100	D	Y	10 dollars	 	I	2008-03-26 13:20:02.292	100
52007	10_rupees	0	0	2008-03-26 13:20:02.307	100	D	Y	10 Rupees	 	I	2008-03-26 13:20:02.307	100
52008	1_cent	0	0	2008-03-26 13:20:02.312	100	D	Y	1 cent	1 cent	I	2008-03-26 13:20:02.312	100
52009	1_dollar	0	0	2008-03-26 13:20:02.326	100	D	Y	1 dollar	 	I	2008-03-26 13:20:02.326	100
52011	2000_rupees	0	0	2008-03-26 13:20:02.329	100	D	Y	2000 Rupees	 	I	2008-03-26 13:20:02.329	100
52012	200_rupees	0	0	2008-03-26 13:20:02.33	100	D	Y	200 Rupees	 	I	2008-03-26 13:20:02.33	100
52013	20_cents	0	0	2008-03-26 13:20:02.332	100	D	Y	20 cents	20 cents	I	2008-03-26 13:20:02.332	100
52014	20_dollars	0	0	2008-03-26 13:20:02.333	100	D	Y	20 dollars	 	I	2008-03-26 13:20:02.333	100
52015	25_rupees	0	0	2008-03-26 13:20:02.338	100	D	Y	25 Rupees	 	I	2008-03-26 13:20:02.338	100
52016	500_rupees	0	0	2008-03-26 13:20:02.339	100	D	Y	500 Rupees	 	I	2008-03-26 13:20:02.339	100
52017	50_cents	0	0	2008-03-26 13:20:02.383	100	D	Y	50 cents	50 cents	I	2008-03-26 13:20:02.383	100
52018	50_dollars	0	0	2008-03-26 13:20:02.387	100	D	Y	50 dollars	 	I	2008-03-26 13:20:02.387	100
52019	50_rupees	0	0	2008-03-26 13:20:02.388	100	D	Y	50 Rupees	 	I	2008-03-26 13:20:02.388	100
52020	5_cents	0	0	2008-03-26 13:20:02.39	100	D	Y	5 cents	5 cents	I	2008-03-26 13:20:02.39	100
52021	5_dollars	0	0	2008-03-26 13:20:02.391	100	D	Y	5 dollars	 	I	2008-03-26 13:20:02.391	100
52022	5_rupees	0	0	2008-03-26 13:20:02.393	100	D	Y	5 Rupees	 	I	2008-03-26 13:20:02.393	100
52023	Due0	0	0	2008-03-26 13:20:02.394	100	D	Y	Due 0	 	I	2008-03-26 13:20:02.394	100
52024	Due0_30	0	0	2008-03-26 13:20:02.396	100	D	Y	Due 0-30	 	I	2008-03-26 13:20:02.396	100
52025	account.type	0	0	2008-03-26 13:20:02.398	100	D	Y	Account Type	 	I	2008-03-26 13:20:02.398	100
52026	accounts.receivable	0	0	2008-03-26 13:20:02.4	100	D	Y	Accounts Receivable-Trade	 	I	2008-03-26 13:20:02.4	100
52027	activate	0	0	2008-03-26 13:20:02.434	100	D	Y	Activate	 	I	2008-03-26 13:20:02.434	100
52028	activate.vendor	0	0	2008-03-26 13:20:02.435	100	D	Y	Activate Vendor	 	I	2008-03-26 13:20:02.435	100
52029	actual.price	0	0	2008-03-26 13:20:02.437	100	D	Y	Actual Price	 	I	2008-03-26 13:20:02.437	100
52030	add	0	0	2008-03-26 13:20:02.438	100	D	Y	Add	 	I	2008-03-26 13:20:02.438	100
52031	add.black.listed.cheque	0	0	2008-03-26 13:20:02.465	100	D	Y	Add Black Listed Cheque	 	I	2008-03-26 13:20:02.465	100
52032	add.customer.fidelity.card	0	0	2008-03-26 13:20:02.467	100	D	Y	Add Customer for fidelity card	 	I	2008-03-26 13:20:02.467	100
52033	add.customer.for.fidelity.card	0	0	2008-03-26 13:20:02.469	100	D	Y	Add Customer for fidelity card	 	I	2008-03-26 13:20:02.469	100
52034	add.record	0	0	2008-03-26 13:20:02.496	100	D	Y	Add Record	 	I	2008-03-26 13:20:02.496	100
52035	add.to.cart	0	0	2008-03-26 13:20:02.497	100	D	Y	Add To Cart	 	I	2008-03-26 13:20:02.497	100
52036	address	0	0	2008-03-26 13:20:02.499	100	D	Y	Address	 	I	2008-03-26 13:20:02.499	100
52037	adjust.cash.book	0	0	2008-03-26 13:20:02.5	100	D	Y	Adjust Cash Book	 	I	2008-03-26 13:20:02.5	100
52038	advanced	0	0	2008-03-26 13:20:02.502	100	D	Y	Advanced	 	I	2008-03-26 13:20:02.502	100
52039	aging	0	0	2008-03-26 13:20:02.503	100	D	Y	Aging	 	I	2008-03-26 13:20:02.503	100
52040	ajax.error	0	0	2008-03-26 13:20:02.504	100	D	Y	Some error occured while communicating with the server. Please try again.	 	I	2008-03-26 13:20:02.504	100
52041	all	0	0	2008-03-26 13:20:02.506	100	D	Y	All	 	I	2008-03-26 13:20:02.506	100
52042	all.products	0	0	2008-03-26 13:20:02.507	100	D	Y	All products	 	I	2008-03-26 13:20:02.507	100
52043	amount.difference	0	0	2008-03-26 13:20:02.508	100	D	Y	Difference	 	I	2008-03-26 13:20:02.508	100
52044	amount.expense	0	0	2008-03-26 13:20:02.51	100	D	Y	Expense	 	I	2008-03-26 13:20:02.51	100
52045	amount.receipt	0	0	2008-03-26 13:20:02.511	100	D	Y	Receipt	 	I	2008-03-26 13:20:02.511	100
52046	amount.refunded	0	0	2008-03-26 13:20:02.513	100	D	Y	Amount refunded	 	I	2008-03-26 13:20:02.513	100
52047	amount.tendered	0	0	2008-03-26 13:20:02.514	100	D	Y	Amount Tendered	 	I	2008-03-26 13:20:02.514	100
52048	amount.transfer	0	0	2008-03-26 13:20:02.515	100	D	Y	Transfer	 	I	2008-03-26 13:20:02.515	100
52049	application.name	0	0	2008-03-26 13:20:02.517	100	D	Y	Application Name	 	I	2008-03-26 13:20:02.517	100
52050	application.version	0	0	2008-03-26 13:20:02.518	100	D	Y	Application Version	 	I	2008-03-26 13:20:02.518	100
52051	asset	0	0	2008-03-26 13:20:02.519	100	D	Y	Assets	 	I	2008-03-26 13:20:02.519	100
52052	at.least.one	0	0	2008-03-26 13:20:02.521	100	D	Y	At least one	 	I	2008-03-26 13:20:02.521	100
52053	attach.image	0	0	2008-03-26 13:20:02.523	100	D	Y	Attach Image	 	I	2008-03-26 13:20:02.523	100
52054	attribute	0	0	2008-03-26 13:20:02.525	100	D	Y	Attribute	Attribute	I	2008-03-26 13:20:02.525	100
52055	attribute.set	0	0	2008-03-26 13:20:02.527	100	D	Y	Attribute Set	Attribute Set	I	2008-03-26 13:20:02.527	100
52056	available.menu	0	0	2008-03-26 13:20:02.528	100	D	Y	Available Menus	 	I	2008-03-26 13:20:02.528	100
52057	back	0	0	2008-03-26 13:20:02.53	100	D	Y	Back	 	I	2008-03-26 13:20:02.53	100
52058	barcode	0	0	2008-03-26 13:20:02.531	100	D	Y	Barcode	Barcode	I	2008-03-26 13:20:02.531	100
52059	barcode.already.exists	0	0	2008-03-26 13:20:02.532	100	D	Y	Barcode already exists!	 	I	2008-03-26 13:20:02.532	100
52060	black.listed.cheques	0	0	2008-03-26 13:20:02.534	100	D	Y	Black Listed Cheques	 	I	2008-03-26 13:20:02.534	100
52061	bpartner.info	0	0	2008-03-26 13:20:02.535	100	D	Y	Business Partner Info	 	I	2008-03-26 13:20:02.535	100
52062	bpartner.trx.details	0	0	2008-03-26 13:20:02.537	100	D	Y	Business Partner Trx Details	 	I	2008-03-26 13:20:02.537	100
52063	brand	0	0	2008-03-26 13:20:02.547	100	D	Y	Brand	 	I	2008-03-26 13:20:02.547	100
52064	cal.period.curr	0	0	2008-03-26 13:20:02.55	100	D	Y	Calculation Period And Curr.	 	I	2008-03-26 13:20:02.55	100
52065	card	0	0	2008-03-26 13:20:02.551	100	D	Y	Card	 	I	2008-03-26 13:20:02.551	100
52066	card.amount	0	0	2008-03-26 13:20:02.553	100	D	Y	Card Amount	 	I	2008-03-26 13:20:02.553	100
52067	card.amt.entered	0	0	2008-03-26 13:20:02.554	100	D	Y	Card Amount Entered	 	I	2008-03-26 13:20:02.554	100
52068	card.amt.tendered	0	0	2008-03-26 13:20:02.556	100	D	Y	Card amount tendered	 	I	2008-03-26 13:20:02.556	100
52069	card.amt.total	0	0	2008-03-26 13:20:02.558	100	D	Y	Card Amount Total	 	I	2008-03-26 13:20:02.558	100
52070	card.is.empty	0	0	2008-03-26 13:20:02.56	100	D	Y	The Cart is Empty !	 	I	2008-03-26 13:20:02.56	100
52071	card.no	0	0	2008-03-26 13:20:02.562	100	D	Y	Card No	 	I	2008-03-26 13:20:02.562	100
52072	card.total	0	0	2008-03-26 13:20:02.567	100	D	Y	Card Total	 	I	2008-03-26 13:20:02.567	100
52073	cart.addmore	0	0	2008-03-26 13:20:02.569	100	D	Y	Add More	 	I	2008-03-26 13:20:02.569	100
52074	cart.empty	0	0	2008-03-26 13:20:02.571	100	D	Y	Cart is empty!	 	I	2008-03-26 13:20:02.571	100
52075	cart.has	0	0	2008-03-26 13:20:02.572	100	D	Y	Cart has	 	I	2008-03-26 13:20:02.572	100
52076	cart.items	0	0	2008-03-26 13:20:02.574	100	D	Y	 items	 	I	2008-03-26 13:20:02.574	100
52077	cart.remove	0	0	2008-03-26 13:20:02.575	100	D	Y	Remove	 	I	2008-03-26 13:20:02.575	100
52078	cash	0	0	2008-03-26 13:20:02.577	100	D	Y	Cash	 	I	2008-03-26 13:20:02.577	100
52079	cash.amount	0	0	2008-03-26 13:20:02.578	100	D	Y	Cash Amount	 	I	2008-03-26 13:20:02.578	100
52080	cash.book	0	0	2008-03-26 13:20:02.58	100	D	Y	Cash Book	 	I	2008-03-26 13:20:02.58	100
52081	cash.book.adjust	0	0	2008-03-26 13:20:02.581	100	D	Y	The Cash Book has been adjusted.	 	I	2008-03-26 13:20:02.581	100
52082	cash.book.adjusted	0	0	2008-03-26 13:20:02.583	100	D	Y	The Cash Book has been adjusted	 	I	2008-03-26 13:20:02.583	100
52083	cash.book.history	0	0	2008-03-26 13:20:02.585	100	D	Y	Cash Book History	 	I	2008-03-26 13:20:02.585	100
52084	cash.line.detail	0	0	2008-03-26 13:20:02.587	100	D	Y	Cash Line Details	 	I	2008-03-26 13:20:02.587	100
52085	cash.payment	0	0	2008-03-26 13:20:02.588	100	D	Y	Cash Payment	 	I	2008-03-26 13:20:02.588	100
52086	cash.receipt	0	0	2008-03-26 13:20:02.59	100	D	Y	Cash Receipt	 	I	2008-03-26 13:20:02.59	100
52087	cash.refunded	0	0	2008-03-26 13:20:02.592	100	D	Y	Cash refunded	 	I	2008-03-26 13:20:02.592	100
52088	cash.tendered	0	0	2008-03-26 13:20:02.593	100	D	Y	Cash tendered	 	I	2008-03-26 13:20:02.593	100
52089	cash.to.transfer	0	0	2008-03-26 13:20:02.595	100	D	Y	Cash to transfer	 	I	2008-03-26 13:20:02.595	100
52090	cash.total	0	0	2008-03-26 13:20:02.597	100	D	Y	Cash Total	 	I	2008-03-26 13:20:02.597	100
52091	cellphone	0	0	2008-03-26 13:20:02.598	100	D	Y	Cellphone	 	I	2008-03-26 13:20:02.598	100
52092	checkout	0	0	2008-03-26 13:20:02.599	100	D	Y	Checkout	 	I	2008-03-26 13:20:02.599	100
52093	cheque	0	0	2008-03-26 13:20:02.601	100	D	Y	Cheque	 	I	2008-03-26 13:20:02.601	100
52094	cheque.amount	0	0	2008-03-26 13:20:02.602	100	D	Y	Cheque Amount	 	I	2008-03-26 13:20:02.602	100
52095	cheque.amt.entered	0	0	2008-03-26 13:20:02.604	100	D	Y	Cheque Amount Entered	 	I	2008-03-26 13:20:02.604	100
52096	cheque.amt.tendered	0	0	2008-03-26 13:20:02.606	100	D	Y	Cheque amount tendered	 	I	2008-03-26 13:20:02.606	100
52097	cheque.amt.total	0	0	2008-03-26 13:20:02.607	100	D	Y	Cheque Amount Total	 	I	2008-03-26 13:20:02.607	100
52098	cheque.no	0	0	2008-03-26 13:20:02.609	100	D	Y	Cheque No	 	I	2008-03-26 13:20:02.609	100
52099	cheque.total	0	0	2008-03-26 13:20:02.61	100	D	Y	Cheque Total	 	I	2008-03-26 13:20:02.61	100
52100	choose.attribute	0	0	2008-03-26 13:20:02.613	100	D	Y	Choose Attribute	 	I	2008-03-26 13:20:02.613	100
52101	choose.your.till	0	0	2008-03-26 13:20:02.614	100	D	Y	Please choose your till number	 	I	2008-03-26 13:20:02.614	100
52102	clear	0	0	2008-03-26 13:20:02.616	100	D	Y	Clear	 	I	2008-03-26 13:20:02.616	100
52103	close	0	0	2008-03-26 13:20:02.617	100	D	Y	Close	 	I	2008-03-26 13:20:02.617	100
52104	close.till	0	0	2008-03-26 13:20:02.619	100	D	Y	Close Till	 	I	2008-03-26 13:20:02.619	100
52105	closed	0	0	2008-03-26 13:20:02.62	100	D	Y	Closed	 	I	2008-03-26 13:20:02.62	100
52106	closing.balance	0	0	2008-03-26 13:20:02.622	100	D	Y	Closing Balance	 	I	2008-03-26 13:20:02.622	100
52107	cogs	0	0	2008-03-26 13:20:02.624	100	D	Y	CoGS	 	I	2008-03-26 13:20:02.624	100
52108	colour	0	0	2008-03-26 13:20:02.625	100	D	Y	Colour	 	I	2008-03-26 13:20:02.625	100
52109	commission.details	0	0	2008-03-26 13:20:02.627	100	D	Y	Commission Details	 	I	2008-03-26 13:20:02.627	100
52110	completed	0	0	2008-03-26 13:20:02.628	100	D	Y	Completed	 	I	2008-03-26 13:20:02.628	100
52111	continue	0	0	2008-03-26 13:20:02.629	100	D	Y	Continue	 	I	2008-03-26 13:20:02.629	100
52112	create.bpartner	0	0	2008-03-26 13:20:02.631	100	D	Y	Create Business Partner	 	I	2008-03-26 13:20:02.631	100
52113	create.garment	0	0	2008-03-26 13:20:02.632	100	D	Y	Create Garment	 	I	2008-03-26 13:20:02.632	100
52114	create.payment	0	0	2008-03-26 13:20:02.633	100	D	Y	Create Payment	 	I	2008-03-26 13:20:02.633	100
52115	credit.check	0	0	2008-03-26 13:20:02.635	100	D	Y	Credit Check	 	I	2008-03-26 13:20:02.635	100
52116	credit.details	0	0	2008-03-26 13:20:02.636	100	D	Y	Credit Details	 	I	2008-03-26 13:20:02.636	100
52117	credit.hold	0	0	2008-03-26 13:20:02.637	100	D	Y	Credit Hold	 	I	2008-03-26 13:20:02.637	100
52118	credit.memo	0	0	2008-03-26 13:20:02.639	100	D	Y	Credit Memo	 	I	2008-03-26 13:20:02.639	100
52119	credit.ok	0	0	2008-03-26 13:20:02.64	100	D	Y	Credit OK	 	I	2008-03-26 13:20:02.64	100
52120	credit.order	0	0	2008-03-26 13:20:02.641	100	D	Y	Credit Order	 	I	2008-03-26 13:20:02.641	100
52121	credit.order.discount	0	0	2008-03-26 13:20:02.643	100	D	Y	Credit Order Discount	 	I	2008-03-26 13:20:02.643	100
52122	credit.stop	0	0	2008-03-26 13:20:02.644	100	D	Y	Credit Stop	 	I	2008-03-26 13:20:02.644	100
52123	credit.watch	0	0	2008-03-26 13:20:02.646	100	D	Y	Credit Watch	 	I	2008-03-26 13:20:02.646	100
52124	current.month	0	0	2008-03-26 13:20:02.648	100	D	Y	Current Month	 	I	2008-03-26 13:20:02.648	100
52125	current.till.amount	0	0	2008-03-26 13:20:02.649	100	D	Y	Current Till Amount	 	I	2008-03-26 13:20:02.649	100
52126	current.week	0	0	2008-03-26 13:20:02.65	100	D	Y	Current Week	 	I	2008-03-26 13:20:02.65	100
52127	current.year	0	0	2008-03-26 13:20:02.652	100	D	Y	Current Year	 	I	2008-03-26 13:20:02.652	100
52128	custom	0	0	2008-03-26 13:20:02.653	100	D	Y	Custom	 	I	2008-03-26 13:20:02.653	100
52129	custom.sales.reports	0	0	2008-03-26 13:20:02.654	100	D	Y	Custom Sales Report	 	I	2008-03-26 13:20:02.654	100
52130	customer	0	0	2008-03-26 13:20:02.655	100	D	Y	Customer	 	I	2008-03-26 13:20:02.655	100
52131	customer.id	0	0	2008-03-26 13:20:02.657	100	D	Y	Customer ID	 	I	2008-03-26 13:20:02.657	100
52132	customer.info	0	0	2008-03-26 13:20:02.658	100	D	Y	Customer Info	 	I	2008-03-26 13:20:02.658	100
52133	customer.returned.order	0	0	2008-03-26 13:20:02.66	100	D	Y	Customer Returned Order	 	I	2008-03-26 13:20:02.66	100
52134	customervendor	0	0	2008-03-26 13:20:02.662	100	D	Y	Customer/Vendor	 	I	2008-03-26 13:20:02.662	100
52135	date.created	0	0	2008-03-26 13:20:02.664	100	D	Y	Date Created	 	I	2008-03-26 13:20:02.664	100
52136	date.from	0	0	2008-03-26 13:20:02.666	100	D	Y	From	 	I	2008-03-26 13:20:02.666	100
52137	date.ordered	0	0	2008-03-26 13:20:02.667	100	D	Y	Date Ordered	Date Ordered	I	2008-03-26 13:20:02.667	100
52138	date.range	0	0	2008-03-26 13:20:02.668	100	D	Y	Date Range	 	I	2008-03-26 13:20:02.668	100
52139	date.to	0	0	2008-03-26 13:20:02.67	100	D	Y	to	 	I	2008-03-26 13:20:02.67	100
52140	deactivate	0	0	2008-03-26 13:20:02.672	100	D	Y	De-activate	 	I	2008-03-26 13:20:02.672	100
52141	deactivate.vendor	0	0	2008-03-26 13:20:02.674	100	D	Y	Deactivate Vendor	 	I	2008-03-26 13:20:02.674	100
52142	dealer.name	0	0	2008-03-26 13:20:02.676	100	D	Y	Dealer Name	 	I	2008-03-26 13:20:02.676	100
52143	default	0	0	2008-03-26 13:20:02.677	100	D	Y	Default	 	I	2008-03-26 13:20:02.677	100
52144	delete	0	0	2008-03-26 13:20:02.679	100	D	Y	Delete	 	I	2008-03-26 13:20:02.679	100
52145	delete.selected	0	0	2008-03-26 13:20:02.68	100	D	Y	Delete Selected	 	I	2008-03-26 13:20:02.68	100
52146	design	0	0	2008-03-26 13:20:02.682	100	D	Y	Design	 	I	2008-03-26 13:20:02.682	100
52147	discount.amt	0	0	2008-03-26 13:20:02.683	100	D	Y	Discount	 	I	2008-03-26 13:20:02.683	100
52148	discounted.price	0	0	2008-03-26 13:20:02.684	100	D	Y	Discounted Price	 	I	2008-03-26 13:20:02.684	100
52149	display	0	0	2008-03-26 13:20:02.686	100	D	Y	Display	 	I	2008-03-26 13:20:02.686	100
52150	display.all.records	0	0	2008-03-26 13:20:02.687	100	D	Y	Do you want to display all records?	 	I	2008-03-26 13:20:02.687	100
52151	doc.basis.type	0	0	2008-03-26 13:20:02.692	100	D	Y	Doc Basis Type	 	I	2008-03-26 13:20:02.692	100
52152	download.csv	0	0	2008-03-26 13:20:02.694	100	D	Y	Download CSV	 	I	2008-03-26 13:20:02.694	100
52153	drafted	0	0	2008-03-26 13:20:02.695	100	D	Y	Drafted	 	I	2008-03-26 13:20:02.695	100
52154	dunning.letters.printed.successfully	0	0	2008-03-26 13:20:02.707	100	D	Y	Dunning letters have been printed successfully	 	I	2008-03-26 13:20:02.707	100
52155	edit	0	0	2008-03-26 13:20:02.71	100	D	Y	Edit	 	I	2008-03-26 13:20:02.71	100
52156	edit.attribute	0	0	2008-03-26 13:20:02.711	100	D	Y	Edit Attribute	 	I	2008-03-26 13:20:02.711	100
52157	edit.black.listed.cheque	0	0	2008-03-26 13:20:02.713	100	D	Y	Edit Black Listed Cheque	 	I	2008-03-26 13:20:02.713	100
52158	edit.customer	0	0	2008-03-26 13:20:02.715	100	D	Y	Edit Customer	 	I	2008-03-26 13:20:02.715	100
52159	edit.price	0	0	2008-03-26 13:20:02.716	100	D	Y	Edit Price	 	I	2008-03-26 13:20:02.716	100
52160	edit.price.list	0	0	2008-03-26 13:20:02.718	100	D	Y	Edit Price List	 	I	2008-03-26 13:20:02.718	100
52161	edit.product	0	0	2008-03-26 13:20:02.719	100	D	Y	Edit Product	 	I	2008-03-26 13:20:02.719	100
52162	edit.related.products	0	0	2008-03-26 13:20:02.721	100	D	Y	Edit related products details	 	I	2008-03-26 13:20:02.721	100
52163	edit.role	0	0	2008-03-26 13:20:02.722	100	D	Y	Edit Role	 	I	2008-03-26 13:20:02.722	100
52164	edit.user	0	0	2008-03-26 13:20:02.724	100	D	Y	Edit User	 	I	2008-03-26 13:20:02.724	100
52165	edit.vendor	0	0	2008-03-26 13:20:02.726	100	D	Y	Edit Vendor	 	I	2008-03-26 13:20:02.726	100
52166	enable.printing	0	0	2008-03-26 13:20:02.728	100	D	Y	Enable printing	 	I	2008-03-26 13:20:02.728	100
52167	enter	0	0	2008-03-26 13:20:02.73	100	D	Y	ENTER	 	I	2008-03-26 13:20:02.73	100
52168	excl.vat	0	0	2008-03-26 13:20:02.732	100	D	Y	(excl. VAT)	 	I	2008-03-26 13:20:02.732	100
52169	fast.moving.item	0	0	2008-03-26 13:20:02.733	100	D	Y	Fast Moving Items Report	 	I	2008-03-26 13:20:02.733	100
52170	fast.moving.item.current.month	0	0	2008-03-26 13:20:02.735	100	D	Y	Fast Moving Items Report (Current Month)	 	I	2008-03-26 13:20:02.735	100
52171	fast.moving.item.today	0	0	2008-03-26 13:20:02.737	100	D	Y	Fast Moving Items Report (Today)	 	I	2008-03-26 13:20:02.737	100
52172	filter.by	0	0	2008-03-26 13:20:02.739	100	D	Y	Filter By	 	I	2008-03-26 13:20:02.739	100
52173	filter.type	0	0	2008-03-26 13:20:02.74	100	D	Y	Choose the type of filter	 	I	2008-03-26 13:20:02.74	100
52174	first	0	0	2008-03-26 13:20:02.742	100	D	Y	First	 	I	2008-03-26 13:20:02.742	100
52175	fixed	0	0	2008-03-26 13:20:02.743	100	D	Y	Fixed	 	I	2008-03-26 13:20:02.743	100
52176	float.amt.change	0	0	2008-03-26 13:20:02.745	100	D	Y	This is the float amount for today. Please Enter float amount for the next day if needs to be changed.	 	I	2008-03-26 13:20:02.745	100
52177	footer.copyright	0	0	2008-03-26 13:20:02.747	100	D	Y	All Contents ©	2006 Tamak ICT	I	2008-03-26 13:20:02.747	100
52178	found.none	0	0	2008-03-26 13:20:02.748	100	D	Y	Found None	 	I	2008-03-26 13:20:02.748	100
52179	from	0	0	2008-03-26 13:20:02.774	100	D	Y	From	 	I	2008-03-26 13:20:02.774	100
52180	garment.template	0	0	2008-03-26 13:20:02.776	100	D	Y	Garment Template	 	I	2008-03-26 13:20:02.776	100
52181	generate.commission	0	0	2008-03-26 13:20:02.778	100	D	Y	Generate Commission	 	I	2008-03-26 13:20:02.778	100
52182	goods.received.note	0	0	2008-03-26 13:20:02.779	100	D	Y	Goods Received Note	 	I	2008-03-26 13:20:02.779	100
52183	goods.returned.note	0	0	2008-03-26 13:20:02.782	100	D	Y	Goods Returned Note	 	I	2008-03-26 13:20:02.782	100
52184	hide.details	0	0	2008-03-26 13:20:02.783	100	D	Y	Hide Details	 	I	2008-03-26 13:20:02.783	100
52185	import.black.listed	0	0	2008-03-26 13:20:02.785	100	D	Y	Import Black Listed Cheques	 	I	2008-03-26 13:20:02.785	100
52186	import.blacklisted.message1	0	0	2008-03-26 13:20:02.786	100	D	Y	This utility is to import the a list of Black Listed Cheques from a csv file into the system,<br>The csv file should look like the one shown below including the header:	 	I	2008-03-26 13:20:02.786	100
52187	import.list	0	0	2008-03-26 13:20:02.788	100	D	Y	Import List	 	I	2008-03-26 13:20:02.788	100
52188	import.product.message	0	0	2008-03-26 13:20:02.789	100	D	Y	This utility is to import the products and Stock in Hand from a csv file into the system,<br> For importing the garment products, the CSV file name should containg the word 'Garment' <br>The csv file should look like the one shown below including the header	 	I	2008-03-26 13:20:02.789	100
52189	incl.vat	0	0	2008-03-26 13:20:02.791	100	D	Y	(incl. VAT)	 	I	2008-03-26 13:20:02.791	100
52190	inprogress	0	0	2008-03-26 13:20:02.793	100	D	Y	InProgress	 	I	2008-03-26 13:20:02.793	100
52191	invalid	0	0	2008-03-26 13:20:02.794	100	D	Y	Invalid	 	I	2008-03-26 13:20:02.794	100
52192	invoice.no	0	0	2008-03-26 13:20:02.796	100	D	Y	Invoice No	 	I	2008-03-26 13:20:02.796	100
52193	invoke	0	0	2008-03-26 13:20:02.797	100	D	Y	Invoke	 	I	2008-03-26 13:20:02.797	100
52194	invoke.customer.returned.order	0	0	2008-03-26 13:20:02.799	100	D	Y	Invoke	 	I	2008-03-26 13:20:02.799	100
52195	invoke.partial	0	0	2008-03-26 13:20:02.8	100	D	Y	Invoke Partial POS Order	 	I	2008-03-26 13:20:02.8	100
52196	items	0	0	2008-03-26 13:20:02.803	100	D	Y	Items	 	I	2008-03-26 13:20:02.803	100
52197	last	0	0	2008-03-26 13:20:02.805	100	D	Y	Last	 	I	2008-03-26 13:20:02.805	100
52198	last.2.months	0	0	2008-03-26 13:20:02.807	100	D	Y	Last 2 Months	 	I	2008-03-26 13:20:02.807	100
52199	last.2.weeks	0	0	2008-03-26 13:20:02.808	100	D	Y	Last 2 Weeks	 	I	2008-03-26 13:20:02.808	100
52200	last.3.months	0	0	2008-03-26 13:20:02.809	100	D	Y	Last 3 Months	 	I	2008-03-26 13:20:02.809	100
52201	last.3.weeks	0	0	2008-03-26 13:20:02.811	100	D	Y	Last 3 Weeks	 	I	2008-03-26 13:20:02.811	100
52202	last.6.month	0	0	2008-03-26 13:20:02.813	100	D	Y	Last 6 Months	 	I	2008-03-26 13:20:02.813	100
52203	license.name	0	0	2008-03-26 13:20:02.814	100	D	Y	License Name	License Name	I	2008-03-26 13:20:02.814	100
52204	license.valid	0	0	2008-03-26 13:20:02.816	100	D	Y	License Valid	License Valid	I	2008-03-26 13:20:02.816	100
52205	licensed.distribution	0	0	2008-03-26 13:20:02.818	100	D	Y	Licensed Distribution	 	I	2008-03-26 13:20:02.818	100
52206	licensed.module	0	0	2008-03-26 13:20:02.819	100	D	Y	Licensed Modules	 	I	2008-03-26 13:20:02.819	100
52207	licensing.info	0	0	2008-03-26 13:20:02.821	100	D	Y	Licensing Information	 	I	2008-03-26 13:20:02.821	100
52208	list	0	0	2008-03-26 13:20:02.822	100	D	Y	List	 	I	2008-03-26 13:20:02.822	100
52209	login.home.all.contents	0	0	2008-03-26 13:20:02.823	100	D	Y	All content	 	I	2008-03-26 13:20:02.823	100
52210	login.home.loginForgot	0	0	2008-03-26 13:20:02.825	100	D	Y	Forgot password?	 	I	2008-03-26 13:20:02.825	100
52211	login.home.message1	0	0	2008-03-26 13:20:02.827	100	D	Y	Please enter your username and password	 	I	2008-03-26 13:20:02.827	100
52212	login.home.message2	0	0	2008-03-26 13:20:02.829	100	D	Y	Please enter PIN	 	I	2008-03-26 13:20:02.829	100
52213	login.home.password	0	0	2008-03-26 13:20:02.831	100	D	Y	Password	 	I	2008-03-26 13:20:02.831	100
52214	login.home.username	0	0	2008-03-26 13:20:02.833	100	D	Y	Username	 	I	2008-03-26 13:20:02.833	100
52215	login.password.backToLogin	0	0	2008-03-26 13:20:02.834	100	D	Y	 	 Click Here to go back to the Login Screen	I	2008-03-26 13:20:02.834	100
52216	login.password.passwordSent	0	0	2008-03-26 13:20:02.836	100	D	Y	Your password has been sent you should receive it in a few minutes	 	I	2008-03-26 13:20:02.836	100
52217	marked.price	0	0	2008-03-26 13:20:02.837	100	D	Y	Marked Price	 	I	2008-03-26 13:20:02.837	100
52218	max.active.users	0	0	2008-03-26 13:20:02.839	100	D	Y	Max Active Users	 	I	2008-03-26 13:20:02.839	100
52219	max.sold.item	0	0	2008-03-26 13:20:02.84	100	D	Y	Max sold Item	 	I	2008-03-26 13:20:02.84	100
52220	menus	0	0	2008-03-26 13:20:02.842	100	D	Y	Menus	 	I	2008-03-26 13:20:02.842	100
52221	min.item.sold	0	0	2008-03-26 13:20:02.843	100	D	Y	Min Item Sold	 	I	2008-03-26 13:20:02.843	100
52222	mixed	0	0	2008-03-26 13:20:02.845	100	D	Y	Mixed	 	I	2008-03-26 13:20:02.845	100
52223	mobile	0	0	2008-03-26 13:20:02.846	100	D	Y	Moblie No	 	I	2008-03-26 13:20:02.846	100
52224	model	0	0	2008-03-26 13:20:02.849	100	D	Y	Model	 	I	2008-03-26 13:20:02.849	100
52225	month	0	0	2008-03-26 13:20:02.85	100	D	Y	Month	 	I	2008-03-26 13:20:02.85	100
52226	net.amt	0	0	2008-03-26 13:20:02.852	100	D	Y	Net Amount	 	I	2008-03-26 13:20:02.852	100
52227	net.cash.trx	0	0	2008-03-26 13:20:02.853	100	D	Y	Net Cash Trx	 	I	2008-03-26 13:20:02.853	100
52228	net.transaction	0	0	2008-03-26 13:20:02.855	100	D	Y	Net Transaction	 	I	2008-03-26 13:20:02.855	100
52229	new.attribute.value	0	0	2008-03-26 13:20:02.857	100	D	Y	New Attribute Value	 	I	2008-03-26 13:20:02.857	100
52230	new.credit.order	0	0	2008-03-26 13:20:02.859	100	D	Y	New Credit Order	 	I	2008-03-26 13:20:02.859	100
52231	new.customer	0	0	2008-03-26 13:20:02.861	100	D	Y	New Customer	 	I	2008-03-26 13:20:02.861	100
52232	new.customer.return.order	0	0	2008-03-26 13:20:02.863	100	D	Y	New Customer Return Order	 	I	2008-03-26 13:20:02.863	100
52233	new.goods.received.note	0	0	2008-03-26 13:20:02.865	100	D	Y	New Goods Receive Note	 	I	2008-03-26 13:20:02.865	100
52234	new.goods.returned.note	0	0	2008-03-26 13:20:02.866	100	D	Y	New Goods Returned Note	 	I	2008-03-26 13:20:02.866	100
52235	new.order	0	0	2008-03-26 13:20:02.867	100	D	Y	New Order	 	I	2008-03-26 13:20:02.867	100
52236	new.price	0	0	2008-03-26 13:20:02.869	100	D	Y	New Price	 	I	2008-03-26 13:20:02.869	100
52237	no	0	0	2008-03-26 13:20:02.871	100	D	Y	No	 	I	2008-03-26 13:20:02.871	100
52238	no.customer.was.found.for	0	0	2008-03-26 13:20:02.872	100	D	Y	No customer was found for	 	I	2008-03-26 13:20:02.872	100
52239	no.shipment	0	0	2008-03-26 13:20:02.873	100	D	Y	No Shipment	 	I	2008-03-26 13:20:02.873	100
52240	no.vendor.was.found.for	0	0	2008-03-26 13:20:02.876	100	D	Y	No vendor was found for	 	I	2008-03-26 13:20:02.876	100
52241	normal.template	0	0	2008-03-26 13:20:02.878	100	D	Y	Normal Template	 	I	2008-03-26 13:20:02.878	100
52242	not.authorised.to.give.discount	0	0	2008-03-26 13:20:02.879	100	D	Y	It seems that you are not authorised to give discounts	 	I	2008-03-26 13:20:02.879	100
52243	not.found	0	0	2008-03-26 13:20:02.881	100	D	Y	Not Found	 	I	2008-03-26 13:20:02.881	100
52244	notes	0	0	2008-03-26 13:20:02.882	100	D	Y	Notes	Notes	I	2008-03-26 13:20:02.882	100
52245	num.active.users	0	0	2008-03-26 13:20:02.884	100	D	Y	No of Active Users	 	I	2008-03-26 13:20:02.884	100
52246	num.operations	0	0	2008-03-26 13:20:02.885	100	D	Y	Number of Operations	 	I	2008-03-26 13:20:02.885	100
52247	old.attribute.value	0	0	2008-03-26 13:20:02.887	100	D	Y	Old Attribute Value	 	I	2008-03-26 13:20:02.887	100
52248	open.invoices	0	0	2008-03-26 13:20:02.889	100	D	Y	Open Invoices	 	I	2008-03-26 13:20:02.889	100
52249	opening.balance	0	0	2008-03-26 13:20:02.891	100	D	Y	Opening Balance	 	I	2008-03-26 13:20:02.891	100
52250	order.no	0	0	2008-03-26 13:20:02.892	100	D	Y	Order No	 	I	2008-03-26 13:20:02.892	100
52251	order.type	0	0	2008-03-26 13:20:02.894	100	D	Y	Order Type	 	I	2008-03-26 13:20:02.894	100
52252	organisation.name	0	0	2008-03-26 13:20:02.897	100	D	Y	Organisations	 	I	2008-03-26 13:20:02.897	100
52253	partial.pos.order	0	0	2008-03-26 13:20:02.899	100	D	Y	Partial POS Order	 	I	2008-03-26 13:20:02.899	100
52254	partial.pos.order.history	0	0	2008-03-26 13:20:02.9	100	D	Y	Partial POS Order History	 	I	2008-03-26 13:20:02.9	100
52255	payment.allocation	0	0	2008-03-26 13:20:02.901	100	D	Y	Payment Allocation	 	I	2008-03-26 13:20:02.901	100
52256	payment.by	0	0	2008-03-26 13:20:02.903	100	D	Y	Payment By	 	I	2008-03-26 13:20:02.903	100
52257	payment.details	0	0	2008-03-26 13:20:02.904	100	D	Y	Payment Details	 	I	2008-03-26 13:20:02.904	100
52258	payment.num	0	0	2008-03-26 13:20:02.906	100	D	Y	Payment No	Payment No	I	2008-03-26 13:20:02.906	100
52259	payment.status	0	0	2008-03-26 13:20:02.907	100	D	Y	Payment Status	Payment Status	I	2008-03-26 13:20:02.907	100
52260	payment.terms	0	0	2008-03-26 13:20:02.911	100	D	Y	Payment Terms	 	I	2008-03-26 13:20:02.911	100
52261	payment.type	0	0	2008-03-26 13:20:02.913	100	D	Y	Payment Type	 	I	2008-03-26 13:20:02.913	100
52262	periodic.cash.details	0	0	2008-03-26 13:20:02.915	100	D	Y	Periodic Cash Book Details	 	I	2008-03-26 13:20:02.915	100
52263	please.enter.pin	0	0	2008-03-26 13:20:02.916	100	D	Y	Please enter PIN	 	I	2008-03-26 13:20:02.916	100
52264	pmenu.administration	0	0	2008-03-26 13:20:02.918	100	D	Y	Administration	 	I	2008-03-26 13:20:02.918	100
52265	pmenu.cash.sales	0	0	2008-03-26 13:20:02.919	100	D	Y	Cash Sales	 	I	2008-03-26 13:20:02.919	100
52266	pmenu.credit.sales	0	0	2008-03-26 13:20:02.92	100	D	Y	Credit Sales	 	I	2008-03-26 13:20:02.92	100
52267	pmenu.performance.analysis	0	0	2008-03-26 13:20:02.922	100	D	Y	Performance Analysis	 	I	2008-03-26 13:20:02.922	100
52268	pmenu.purchases	0	0	2008-03-26 13:20:02.925	100	D	Y	Purchases	 	I	2008-03-26 13:20:02.925	100
52269	pmenu.stock	0	0	2008-03-26 13:20:02.926	100	D	Y	Stock	 	I	2008-03-26 13:20:02.926	100
52270	pos.info	0	0	2008-03-26 13:20:02.928	100	D	Y	POS Info	 	I	2008-03-26 13:20:02.928	100
52271	pos.info.current.month	0	0	2008-03-26 13:20:02.929	100	D	Y	POS Info  (Current Month)	 	I	2008-03-26 13:20:02.929	100
52272	pos.info.custom	0	0	2008-03-26 13:20:02.931	100	D	Y	POS Info  (Custom)	 	I	2008-03-26 13:20:02.931	100
52273	pos.info.today	0	0	2008-03-26 13:20:02.932	100	D	Y	POS Info (Today)	 	I	2008-03-26 13:20:02.932	100
52274	pos.name	0	0	2008-03-26 13:20:02.934	100	D	Y	POS Name	 	I	2008-03-26 13:20:02.934	100
52275	pos.order	0	0	2008-03-26 13:20:02.935	100	D	Y	POS Order	 	I	2008-03-26 13:20:02.935	100
52276	pos.order.history	0	0	2008-03-26 13:20:02.936	100	D	Y	POS Order History	 	I	2008-03-26 13:20:02.936	100
52277	posPassowrd.welcome	0	0	2008-03-26 13:20:02.939	100	D	Y	Welcome to Posterita POS	 	I	2008-03-26 13:20:02.939	100
52278	postalAddr	0	0	2008-03-26 13:20:02.963	100	D	Y	Postal Address	 	I	2008-03-26 13:20:02.963	100
52279	preference.themes	0	0	2008-03-26 13:20:02.965	100	D	Y	Themes	 	I	2008-03-26 13:20:02.965	100
52280	preferences	0	0	2008-03-26 13:20:02.967	100	D	Y	Preferences	 	I	2008-03-26 13:20:02.967	100
52281	prepared.order	0	0	2008-03-26 13:20:02.968	100	D	Y	Prepared Order	 	I	2008-03-26 13:20:02.968	100
52282	prev	0	0	2008-03-26 13:20:02.97	100	D	Y	Prev	 	I	2008-03-26 13:20:02.97	100
52283	print.barcode	0	0	2008-03-26 13:20:02.971	100	D	Y	Print Barcode	 	I	2008-03-26 13:20:02.971	100
52284	print.dunning.letters	0	0	2008-03-26 13:20:02.973	100	D	Y	Print Dunning Letters	 	I	2008-03-26 13:20:02.973	100
52285	print.fidelity.card	0	0	2008-03-26 13:20:02.975	100	D	Y	Print Fidelity Card	 	I	2008-03-26 13:20:02.975	100
52286	print.order	0	0	2008-03-26 13:20:02.977	100	D	Y	Print Order	 	I	2008-03-26 13:20:02.977	100
52287	print.pdf	0	0	2008-03-26 13:20:02.978	100	D	Y	Print PDF	 	I	2008-03-26 13:20:02.978	100
52288	printer.enabled	0	0	2008-03-26 13:20:02.98	100	D	Y	Printer enabled	 	I	2008-03-26 13:20:02.98	100
52289	printer.name	0	0	2008-03-26 13:20:02.981	100	D	Y	Printer name	 	I	2008-03-26 13:20:02.981	100
52290	product.already.exists	0	0	2008-03-26 13:20:02.983	100	D	Y	Product name already exists!	 	I	2008-03-26 13:20:02.983	100
52291	product.id	0	0	2008-03-26 13:20:02.984	100	D	Y	Product ID	 	I	2008-03-26 13:20:02.984	100
52292	product.info	0	0	2008-03-26 13:20:02.986	100	D	Y	Product Info	 	I	2008-03-26 13:20:02.986	100
52293	product.type.item	0	0	2008-03-26 13:20:02.987	100	D	Y	Item	 	I	2008-03-26 13:20:02.987	100
52294	product.type.services	0	0	2008-03-26 13:20:02.991	100	D	Y	Services	 	I	2008-03-26 13:20:02.991	100
52295	product.updated	0	0	2008-03-26 13:20:02.993	100	D	Y	The Products have been updated	 	I	2008-03-26 13:20:02.993	100
52296	profit.margin	0	0	2008-03-26 13:20:02.995	100	D	Y	Profit Margin	 	I	2008-03-26 13:20:02.995	100
52297	purchase.price	0	0	2008-03-26 13:20:02.996	100	D	Y	Purchase Price	 	I	2008-03-26 13:20:02.996	100
52298	qty.received	0	0	2008-03-26 13:20:02.997	100	D	Y	Qty Received	 	I	2008-03-26 13:20:02.997	100
52299	qty.returned.by.customer	0	0	2008-03-26 13:20:02.999	100	D	Y	Qty Returned By Customer	 	I	2008-03-26 13:20:02.999	100
52300	qty.returned.to.supplier	0	0	2008-03-26 13:20:03	100	D	Y	Qty Returned to Supplier	 	I	2008-03-26 13:20:03	100
52301	qty.sold	0	0	2008-03-26 13:20:03.002	100	D	Y	Qty Sold	 	I	2008-03-26 13:20:03.002	100
52302	reason.message	0	0	2008-03-26 13:20:03.004	100	D	Y	Reason	 	I	2008-03-26 13:20:03.004	100
52303	record.found	0	0	2008-03-26 13:20:03.005	100	D	Y	Records found	 	I	2008-03-26 13:20:03.005	100
52304	records.per.page	0	0	2008-03-26 13:20:03.007	100	D	Y	records per page	 	I	2008-03-26 13:20:03.007	100
52305	remote.printing	0	0	2008-03-26 13:20:03.008	100	D	Y	Remote printing	 	I	2008-03-26 13:20:03.008	100
52306	remove.customer.fidelity.card	0	0	2008-03-26 13:20:03.009	100	D	Y	Remove Customer for fidelity card	 	I	2008-03-26 13:20:03.009	100
52307	report.filter.settings	0	0	2008-03-26 13:20:03.011	100	D	Y	Report Filter Settings	 	I	2008-03-26 13:20:03.011	100
52308	reprint	0	0	2008-03-26 13:20:03.013	100	D	Y	Reprint	 	I	2008-03-26 13:20:03.013	100
52309	returned.order	0	0	2008-03-26 13:20:03.014	100	D	Y	Returned Order	 	I	2008-03-26 13:20:03.014	100
52310	revenue	0	0	2008-03-26 13:20:03.016	100	D	Y	Revenue	 	I	2008-03-26 13:20:03.016	100
52311	sales.details	0	0	2008-03-26 13:20:03.017	100	D	Y	Sales Details	 	I	2008-03-26 13:20:03.017	100
52312	sales.order	0	0	2008-03-26 13:20:03.019	100	D	Y	Sales Order	 	I	2008-03-26 13:20:03.019	100
52313	sales.price	0	0	2008-03-26 13:20:03.02	100	D	Y	Sales Price	 	I	2008-03-26 13:20:03.02	100
52314	sales.reports	0	0	2008-03-26 13:20:03.023	100	D	Y	Sales Reports	 	I	2008-03-26 13:20:03.023	100
52315	save	0	0	2008-03-26 13:20:03.024	100	D	Y	Save	 	I	2008-03-26 13:20:03.024	100
52316	save.as.csv	0	0	2008-03-26 13:20:03.026	100	D	Y	Save as CSV	 	I	2008-03-26 13:20:03.026	100
52317	search	0	0	2008-03-26 13:20:03.027	100	D	Y	Search	 	I	2008-03-26 13:20:03.027	100
52318	search.customer	0	0	2008-03-26 13:20:03.029	100	D	Y	Search Customer	 	I	2008-03-26 13:20:03.029	100
52319	search.customer.notfound	0	0	2008-03-26 13:20:03.03	100	D	Y	No customers were found for	 	I	2008-03-26 13:20:03.03	100
52320	search.product	0	0	2008-03-26 13:20:03.032	100	D	Y	Search Product	 	I	2008-03-26 13:20:03.032	100
52321	search.product.notfound	0	0	2008-03-26 13:20:03.034	100	D	Y	No products were found for:	 	I	2008-03-26 13:20:03.034	100
52322	search.result.displaying	0	0	2008-03-26 13:20:03.036	100	D	Y	Displaying	 	I	2008-03-26 13:20:03.036	100
52323	search.result.of	0	0	2008-03-26 13:20:03.039	100	D	Y	of	 	I	2008-03-26 13:20:03.039	100
52324	search.result.to	0	0	2008-03-26 13:20:03.041	100	D	Y	to	 	I	2008-03-26 13:20:03.041	100
52325	search.results	0	0	2008-03-26 13:20:03.043	100	D	Y	Search Results	 	I	2008-03-26 13:20:03.043	100
52326	select	0	0	2008-03-26 13:20:03.045	100	D	Y	Select	 	I	2008-03-26 13:20:03.045	100
52327	select.all	0	0	2008-03-26 13:20:03.046	100	D	Y	Select All	 	I	2008-03-26 13:20:03.046	100
52328	select.bpartner.type	0	0	2008-03-26 13:20:03.048	100	D	Y	Select partner type	 	I	2008-03-26 13:20:03.048	100
52329	select.range	0	0	2008-03-26 13:20:03.049	100	D	Y	Select Range	 	I	2008-03-26 13:20:03.049	100
52330	selected.customers	0	0	2008-03-26 13:20:03.054	100	D	Y	Selected Customers	 	I	2008-03-26 13:20:03.054	100
52331	settle.payment	0	0	2008-03-26 13:20:03.093	100	D	Y	Settle Payment	 	I	2008-03-26 13:20:03.093	100
52332	settle.payment.message	0	0	2008-03-26 13:20:03.095	100	D	Y	NOTE:-The Cash Payments would have effect only when the cash book is closed	 	I	2008-03-26 13:20:03.095	100
52333	shipment.no	0	0	2008-03-26 13:20:03.097	100	D	Y	Shipment No	 	I	2008-03-26 13:20:03.097	100
52334	shipment.required	0	0	2008-03-26 13:20:03.099	100	D	Y	Shipment Required	 	I	2008-03-26 13:20:03.099	100
52335	shipment.status	0	0	2008-03-26 13:20:03.1	100	D	Y	Shipment Status	Shipment Status	I	2008-03-26 13:20:03.1	100
52336	show.details	0	0	2008-03-26 13:20:03.102	100	D	Y	Show Details	 	I	2008-03-26 13:20:03.102	100
52337	size	0	0	2008-03-26 13:20:03.104	100	D	Y	Size	 	I	2008-03-26 13:20:03.104	100
52338	slow.moving.item	0	0	2008-03-26 13:20:03.106	100	D	Y	Slow Moving Items	 	I	2008-03-26 13:20:03.106	100
52339	smenu.adjust.cashbook	0	0	2008-03-26 13:20:03.108	100	D	Y	Adjust Cash Book	 	I	2008-03-26 13:20:03.108	100
52340	smenu.adjust.inventory.id	0	0	2008-03-26 13:20:03.11	100	D	Y	Adjust Inventory	 	I	2008-03-26 13:20:03.11	100
52341	smenu.adjust.stock.id	0	0	2008-03-26 13:20:03.111	100	D	Y	Stock Adjustment	 	I	2008-03-26 13:20:03.111	100
52342	smenu.bpartner.sales.details	0	0	2008-03-26 13:20:03.112	100	D	Y	Business Partner Sales Details	 	I	2008-03-26 13:20:03.112	100
52343	smenu.bpartners	0	0	2008-03-26 13:20:03.114	100	D	Y	Business Partners	 	I	2008-03-26 13:20:03.114	100
52344	smenu.cash.sales	0	0	2008-03-26 13:20:03.116	100	D	Y	Cash Sales	 	I	2008-03-26 13:20:03.116	100
52345	smenu.cash.sales.customer.complusory	0	0	2008-03-26 13:20:03.117	100	D	Y	Cash Sales (Customer Compulsory)	 	I	2008-03-26 13:20:03.117	100
52346	smenu.cash.sales.history	0	0	2008-03-26 13:20:03.119	100	D	Y	Cash Sales History	 	I	2008-03-26 13:20:03.119	100
52347	smenu.cash.sales.multiple.payments	0	0	2008-03-26 13:20:03.12	100	D	Y	Cash Sales (Discount/Multiple Payments)	 	I	2008-03-26 13:20:03.12	100
52348	smenu.cashbook.history	0	0	2008-03-26 13:20:03.122	100	D	Y	Cash Book History	 	I	2008-03-26 13:20:03.122	100
52349	smenu.cashbook.report	0	0	2008-03-26 13:20:03.123	100	D	Y	Cash Book Report	 	I	2008-03-26 13:20:03.123	100
52350	smenu.check.repair.database.integrity	0	0	2008-03-26 13:20:03.126	100	D	Y	Check/Repair Database Integrity	 	I	2008-03-26 13:20:03.126	100
52351	smenu.close.till	0	0	2008-03-26 13:20:03.128	100	D	Y	Close Till	 	I	2008-03-26 13:20:03.128	100
52352	smenu.complete.prepared.order	0	0	2008-03-26 13:20:03.129	100	D	Y	Complete Prepared Order	 	I	2008-03-26 13:20:03.129	100
52353	smenu.create.unallocated.payment.id	0	0	2008-03-26 13:20:03.131	100	D	Y	Create General Payments	 	I	2008-03-26 13:20:03.131	100
52354	smenu.credit.memo.history.id	0	0	2008-03-26 13:20:03.133	100	D	Y	Credit Memo History	 	I	2008-03-26 13:20:03.133	100
52355	smenu.credit.sales	0	0	2008-03-26 13:20:03.134	100	D	Y	Credit Sales	 	I	2008-03-26 13:20:03.134	100
52356	smenu.credit.sales.history	0	0	2008-03-26 13:20:03.136	100	D	Y	Credit Sales History	 	I	2008-03-26 13:20:03.136	100
52357	smenu.creditmemo.from.creditorder.id	0	0	2008-03-26 13:20:03.137	100	D	Y	Invoke Credit Memo	 	I	2008-03-26 13:20:03.137	100
52358	smenu.current.money.in.terminal	0	0	2008-03-26 13:20:03.139	100	D	Y	Current Money in Terminal	 	I	2008-03-26 13:20:03.139	100
52359	smenu.customer.return.history.id	0	0	2008-03-26 13:20:03.14	100	D	Y	Customer Return History	 	I	2008-03-26 13:20:03.14	100
52360	smenu.customer.returned.order	0	0	2008-03-26 13:20:03.142	100	D	Y	Customer Returned Order	 	I	2008-03-26 13:20:03.142	100
52361	smenu.customers	0	0	2008-03-26 13:20:03.144	100	D	Y	Customers	 	I	2008-03-26 13:20:03.144	100
52362	smenu.document.history	0	0	2008-03-26 13:20:03.146	100	D	Y	Document History	 	I	2008-03-26 13:20:03.146	100
52363	smenu.dunning.letters	0	0	2008-03-26 13:20:03.148	100	D	Y	Dunning Letters	 	I	2008-03-26 13:20:03.148	100
52364	smenu.edit.product.attribute.value	0	0	2008-03-26 13:20:03.15	100	D	Y	Edit Product Attribute Value	 	I	2008-03-26 13:20:03.15	100
52365	smenu.fast.moving.items	0	0	2008-03-26 13:20:03.151	100	D	Y	Fast Moving Items	 	I	2008-03-26 13:20:03.151	100
52366	smenu.generate.commission	0	0	2008-03-26 13:20:03.152	100	D	Y	Generate Commission	 	I	2008-03-26 13:20:03.152	100
52367	smenu.goods.received.note	0	0	2008-03-26 13:20:03.154	100	D	Y	Goods Received Note	 	I	2008-03-26 13:20:03.154	100
52368	smenu.goods.received.note.history	0	0	2008-03-26 13:20:03.155	100	D	Y	Goods Received Note History	 	I	2008-03-26 13:20:03.155	100
52369	smenu.goods.returned.note	0	0	2008-03-26 13:20:03.158	100	D	Y	Goods Returned Note	 	I	2008-03-26 13:20:03.158	100
52370	smenu.goods.returned.note.history	0	0	2008-03-26 13:20:03.159	100	D	Y	Goods Returned Note History	 	I	2008-03-26 13:20:03.159	100
52371	smenu.inventory.history.id	0	0	2008-03-26 13:20:03.161	100	D	Y	Inventory History	 	I	2008-03-26 13:20:03.161	100
52372	smenu.invoke.customer.returned.order	0	0	2008-03-26 13:20:03.163	100	D	Y	Invoke Customer Returned Order	 	I	2008-03-26 13:20:03.163	100
52373	smenu.logout	0	0	2008-03-26 13:20:03.165	100	D	Y	Logout	 	I	2008-03-26 13:20:03.165	100
52374	smenu.open.cashdrawer	0	0	2008-03-26 13:20:03.166	100	D	Y	Open Cash Drawer	 	I	2008-03-26 13:20:03.166	100
52375	smenu.order.history	0	0	2008-03-26 13:20:03.168	100	D	Y	Order History	 	I	2008-03-26 13:20:03.168	100
52376	smenu.payment.allocation.history	0	0	2008-03-26 13:20:03.169	100	D	Y	Payment Allocations History	 	I	2008-03-26 13:20:03.169	100
52377	smenu.payment.term	0	0	2008-03-26 13:20:03.171	100	D	Y	Payment Term	 	I	2008-03-26 13:20:03.171	100
52378	smenu.performance.analysis.report	0	0	2008-03-26 13:20:03.172	100	D	Y	Performance Analysis Report	 	I	2008-03-26 13:20:03.172	100
52379	smenu.preferences	0	0	2008-03-26 13:20:03.174	100	D	Y	Preferences	 	I	2008-03-26 13:20:03.174	100
52380	smenu.prepare.order	0	0	2008-03-26 13:20:03.175	100	D	Y	Prepare Order	 	I	2008-03-26 13:20:03.175	100
52381	smenu.prepared.order.history	0	0	2008-03-26 13:20:03.177	100	D	Y	Prepared Order History	 	I	2008-03-26 13:20:03.177	100
52382	smenu.products	0	0	2008-03-26 13:20:03.179	100	D	Y	Products	 	I	2008-03-26 13:20:03.179	100
52383	smenu.quick.cash.sales	0	0	2008-03-26 13:20:03.18	100	D	Y	Quick Cash Sales	 	I	2008-03-26 13:20:03.18	100
52384	smenu.role	0	0	2008-03-26 13:20:03.182	100	D	Y	Role	 	I	2008-03-26 13:20:03.182	100
52385	smenu.sales.report.per.terminal	0	0	2008-03-26 13:20:03.183	100	D	Y	Sales Report per Terminal	 	I	2008-03-26 13:20:03.183	100
52386	smenu.settle.payment.credit.sales	0	0	2008-03-26 13:20:03.185	100	D	Y	Settle Payment on a Credit Sales	 	I	2008-03-26 13:20:03.185	100
52388	smenu.stock	0	0	2008-03-26 13:20:03.189	100	D	Y	Stock	 	I	2008-03-26 13:20:03.189	100
52389	smenu.stock.movement	0	0	2008-03-26 13:20:03.192	100	D	Y	Stock Movement	 	I	2008-03-26 13:20:03.192	100
52390	smenu.tax	0	0	2008-03-26 13:20:03.194	100	D	Y	Tax	 	I	2008-03-26 13:20:03.194	100
52391	smenu.users	0	0	2008-03-26 13:20:03.195	100	D	Y	Users	 	I	2008-03-26 13:20:03.195	100
52392	smenu.vendors	0	0	2008-03-26 13:20:03.197	100	D	Y	Vendors	 	I	2008-03-26 13:20:03.197	100
52393	smenu.view.last.generated.commission	0	0	2008-03-26 13:20:03.198	100	D	Y	Last Generated Commission	 	I	2008-03-26 13:20:03.198	100
52394	smenu.view.licensing	0	0	2008-03-26 13:20:03.2	100	D	Y	Licensing	 	I	2008-03-26 13:20:03.2	100
52395	status	0	0	2008-03-26 13:20:03.201	100	D	Y	Status	Status	I	2008-03-26 13:20:03.201	100
52396	stock.in.hand	0	0	2008-03-26 13:20:03.203	100	D	Y	Stock in Hand	 	I	2008-03-26 13:20:03.203	100
52397	stock.inquiry	0	0	2008-03-26 13:20:03.205	100	D	Y	Stock Inquiry	 	I	2008-03-26 13:20:03.205	100
52398	stock.movement	0	0	2008-03-26 13:20:03.206	100	D	Y	Stock Movement	 	I	2008-03-26 13:20:03.206	100
52399	stock.movement.report	0	0	2008-03-26 13:20:03.208	100	D	Y	Stock Movement Report	 	I	2008-03-26 13:20:03.208	100
52400	submit	0	0	2008-03-26 13:20:03.209	100	D	Y	Submit	 	I	2008-03-26 13:20:03.209	100
52401	summary.by.periods	0	0	2008-03-26 13:20:03.211	100	D	Y	Summary By Period	 	I	2008-03-26 13:20:03.211	100
52402	switch.to.paging	0	0	2008-03-26 13:20:03.212	100	D	Y	Switch to Paging	 	I	2008-03-26 13:20:03.212	100
52403	tamak.pos	0	0	2008-03-26 13:20:03.214	100	D	Y	Posterita POS	 	I	2008-03-26 13:20:03.214	100
52404	tax.credit	0	0	2008-03-26 13:20:03.216	100	D	Y	Tax Credit	 	I	2008-03-26 13:20:03.216	100
52405	tax.due	0	0	2008-03-26 13:20:03.217	100	D	Y	Tax Due	 	I	2008-03-26 13:20:03.217	100
52406	texttile.products.only	0	0	2008-03-26 13:20:03.218	100	D	Y	Textile Products Only	 	I	2008-03-26 13:20:03.218	100
52407	the.cart.has	0	0	2008-03-26 13:20:03.221	100	D	Y	The Cart has	 	I	2008-03-26 13:20:03.221	100
52408	till.balance.entered	0	0	2008-03-26 13:20:03.223	100	D	Y	Till Balance Entered	 	I	2008-03-26 13:20:03.223	100
52409	till.management	0	0	2008-03-26 13:20:03.225	100	D	Y	Till Mgt	 	I	2008-03-26 13:20:03.225	100
52410	till.no	0	0	2008-03-26 13:20:03.234	100	D	Y	Till No	 	I	2008-03-26 13:20:03.234	100
52411	time.hour	0	0	2008-03-26 13:20:03.237	100	D	Y	h	 	I	2008-03-26 13:20:03.237	100
52412	time.minute	0	0	2008-03-26 13:20:03.239	100	D	Y	m	 	I	2008-03-26 13:20:03.239	100
52413	to	0	0	2008-03-26 13:20:03.241	100	D	Y	To	 	I	2008-03-26 13:20:03.241	100
52414	today	0	0	2008-03-26 13:20:03.243	100	D	Y	Today	 	I	2008-03-26 13:20:03.243	100
52415	total	0	0	2008-03-26 13:20:03.244	100	D	Y	Total	 	I	2008-03-26 13:20:03.244	100
52416	total.price	0	0	2008-03-26 13:20:03.246	100	D	Y	Total Price	 	I	2008-03-26 13:20:03.246	100
52417	trade.revenue	0	0	2008-03-26 13:20:03.248	100	D	Y	Trade Revenue	 	I	2008-03-26 13:20:03.248	100
52418	unallocated.payments	0	0	2008-03-26 13:20:03.249	100	D	Y	Unallocated Payments	 	I	2008-03-26 13:20:03.249	100
52419	update.details	0	0	2008-03-26 13:20:03.251	100	D	Y	Update Details	 	I	2008-03-26 13:20:03.251	100
52420	user	0	0	2008-03-26 13:20:03.253	100	D	Y	User	 	I	2008-03-26 13:20:03.253	100
52421	user.details	0	0	2008-03-26 13:20:03.255	100	D	Y	User's Details	 	I	2008-03-26 13:20:03.255	100
52422	user.info	0	0	2008-03-26 13:20:03.257	100	D	Y	Information	 	I	2008-03-26 13:20:03.257	100
52423	vat	0	0	2008-03-26 13:20:03.258	100	D	Y	VAT	 	I	2008-03-26 13:20:03.258	100
52424	vendor.details	0	0	2008-03-26 13:20:03.26	100	D	Y	Vendor Details	 	I	2008-03-26 13:20:03.26	100
52425	vendor.ref	0	0	2008-03-26 13:20:03.262	100	D	Y	Vendor Ref	 	I	2008-03-26 13:20:03.262	100
52426	vendors	0	0	2008-03-26 13:20:03.263	100	D	Y	Vendors	 	I	2008-03-26 13:20:03.263	100
52427	view	0	0	2008-03-26 13:20:03.265	100	D	Y	View	 	I	2008-03-26 13:20:03.265	100
52428	view.attributes	0	0	2008-03-26 13:20:03.267	100	D	Y	View Attributes	View Attributes	I	2008-03-26 13:20:03.267	100
52429	view.by	0	0	2008-03-26 13:20:03.268	100	D	Y	View By	 	I	2008-03-26 13:20:03.268	100
52430	view.info	0	0	2008-03-26 13:20:03.27	100	D	Y	View Info	 	I	2008-03-26 13:20:03.27	100
52431	view.product.cart	0	0	2008-03-26 13:20:03.272	100	D	Y	View Product Cart	 	I	2008-03-26 13:20:03.272	100
52432	view.report	0	0	2008-03-26 13:20:03.274	100	D	Y	View Report	 	I	2008-03-26 13:20:03.274	100
52433	view.role	0	0	2008-03-26 13:20:03.286	100	D	Y	View Role	 	I	2008-03-26 13:20:03.286	100
52434	view.vendor	0	0	2008-03-26 13:20:03.288	100	D	Y	View Vendor	 	I	2008-03-26 13:20:03.288	100
52435	year	0	0	2008-03-26 13:20:03.306	100	D	Y	Year	 	I	2008-03-26 13:20:03.306	100
52436	yes	0	0	2008-03-26 13:20:03.308	100	D	Y	Yes	 	I	2008-03-26 13:20:03.308	100
52437	price.x	0	0	2008-03-26 13:20:03.31	100	D	Y	Price(excl. VAT)	Prix(excl. TVA)	I	2008-03-26 13:20:03.31	100
52438	LogOut	0	0	2008-03-26 13:20:03.311	100	D	Y	Log Out	Sortir	I	2008-03-26 13:20:03.311	100
52439	smenu.price.check	0	0	2008-03-26 13:20:03.312	100	D	Y	Price Check	Verifier Prix	I	2008-03-26 13:20:03.312	100
52440	next	0	0	2008-03-26 13:20:03.313	100	D	Y	Next	Suivant	I	2008-03-26 13:20:03.313	100
52441	smenu.price.list	0	0	2008-03-28 16:00:00	100	D	Y	Price List	 	I	2008-03-28 16:00:00	100
52442	pmenu.help	0	0	2008-03-28 16:00:00	100	D	Y	Help	 	I	2008-03-28 16:00:00	100
52443	list.price	0	0	2008-03-28 16:00:00	100	D	Y	List Price	 	I	2008-03-28 16:00:00	100
52444	standard.price	0	0	2008-03-28 16:00:00	100	D	Y	Standard Price	 	I	2008-03-28 16:00:00	100
52445	limit.price	0	0	2008-03-28 16:00:00	100	D	Y	Limit Price	 	I	2008-03-28 16:00:00	100
52446	tax.incl	0	0	2008-03-28 16:00:00	100	D	Y	Tax Incl	 	I	2008-03-28 16:00:00	100
52447	tax.excl	0	0	2008-03-28 16:00:00	100	D	Y	Tax Excl	 	I	2008-03-28 16:00:00	100
52448	type.of.price.list	0	0	2008-03-28 16:00:00	100	D	Y	Type of Price List	 	I	2008-03-28 16:00:00	100
52449	purchase	0	0	2008-03-28 16:00:00	100	D	Y	Purchase	 	I	2008-03-28 16:00:00	100
52450	sales	0	0	2008-03-28 16:00:00	100	D	Y	Sales	 	I	2008-03-28 16:00:00	100
52451	isPresentForProduct	0	0	2008-03-28 16:00:00	100	D	Y	Present for Product	 	I	2008-03-28 16:00:00	100
52452	smenu.organisation	0	0	2008-06-09 00:00:00	100	D	Y	Organisation	\N	I	2008-06-09 00:00:00	100
52453	smenu.terminal	0	0	2008-06-09 00:00:00	100	D	Y	Terminals	Maintain POS Terminals	I	2008-06-09 00:00:00	100
52454	smenu.cashbook	0	0	2008-06-09 00:00:00	100	D	Y	Cash Books	Maintain Cash Books	I	2008-06-09 00:00:00	100
52455	document.status.completed	0	0	2008-06-09 00:00:00	100	D	Y	Completed	\N	I	2008-06-09 00:00:00	100
52456	document.status.drafted	0	0	2008-06-09 00:00:00	100	D	Y	Drafted	\N	I	2008-06-09 00:00:00	100
52457	document.status.inprogress	0	0	2008-06-09 00:00:00	100	D	Y	In Progress	\N	I	2008-06-09 00:00:00	100
52458	document.status.invalid	0	0	2008-06-09 00:00:00	100	D	Y	Invalid	\N	I	2008-06-09 00:00:00	100
52459	document.status.closed	0	0	2008-06-09 00:00:00	100	D	Y	Closed	\N	I	2008-06-09 00:00:00	100
52460	payment.rule.cash	0	0	2008-06-09 00:00:00	100	D	Y	Cash	\N	I	2008-06-09 00:00:00	100
52461	payment.rule.card	0	0	2008-06-09 00:00:00	100	D	Y	Card	\N	I	2008-06-09 00:00:00	100
52462	payment.rule.cheque	0	0	2008-06-09 00:00:00	100	D	Y	Cheque	\N	I	2008-06-09 00:00:00	100
52463	payment.rule.mixed	0	0	2008-06-09 00:00:00	100	D	Y	Mixed	\N	I	2008-06-09 00:00:00	100
52464	payment.rule.credit	0	0	2008-06-09 00:00:00	100	D	Y	Credit	\N	I	2008-06-09 00:00:00	100
52465	AmountPaid	0	0	2008-06-09 00:00:00	100	D	Y	Amount Paid	\N	I	2008-06-09 00:00:00	100
52467	smenu.currency	0	0	2008-04-25 14:30:00	100	D	Y	Currency	 	I	2008-04-25 14:30:00	100
52468	smenu.delete.price.on.pricelist	0	0	2008-04-25 14:30:00	100	D	Y	Delete Price On Price List	 	I	2008-04-25 14:30:00	100
52470	html	0	0	2008-06-20 10:40:00	100	D	Y	HTML	 	I	2008-06-20 10:40:00	100
52471	pdf	0	0	2008-06-20 10:40:00	100	D	Y	PDF	 	I	2008-06-20 10:40:00	100
52472	csv	0	0	2008-06-20 10:40:00	100	D	Y	CSV	 	I	2008-06-20 10:40:00	100
52473	smenu.sales.report	0	0	2008-06-20 10:40:00	100	D	Y	Sales Report	 	I	2008-06-20 10:40:00	100
52474	smenu.best.selling.items	0	0	2008-06-20 10:40:00	100	D	Y	Best Selling Items	 	I	2008-06-20 10:40:00	100
52475	smenu.stock.sales.report	0	0	2008-06-20 10:40:00	100	D	Y	Stock Sales Report	 	I	2008-06-20 10:40:00	100
52476	smenu.transfer.stock	0	0	2008-06-20 10:40:00	100	D	Y	Stock Transfer	 	I	2008-06-20 10:40:00	100
52477	smenu.inventory.move	0	0	2008-06-20 10:40:00	100	D	Y	Inventory Move	 	I	2008-06-20 10:40:00	100
52478	smenu.move.confirmation	0	0	2008-06-20 10:40:00	100	D	Y	Move Confirmation	 	I	2008-06-20 10:40:00	100
52479	smenu.barcode.printing	0	0	2008-06-20 10:40:00	100	D	Y	Barcode Printing	 	I	2008-06-20 10:40:00	100
52480	basePriceList	0	0	2008-06-20 10:40:00	100	D	Y	Base Price List	 	I	2008-06-20 10:40:00	100
52481	isDeleteOldRecords	0	0	2008-06-20 10:40:00	100	D	Y	Delete Old Records	 	I	2008-06-20 10:40:00	100
52482	sales.price.list	0	0	2008-06-20 10:40:00	100	D	Y	Sales Price List	 	I	2008-06-20 10:40:00	100
52483	purchase.price.list	0	0	2008-06-20 10:40:00	100	D	Y	Purchase Price List	 	I	2008-06-20 10:40:00	100
52484	pos.terminal	0	0	2008-06-20 10:40:00	100	D	Y	Terminal	 	I	2008-06-20 10:40:00	100
52485	pos.version	0	0	2008-06-20 10:40:00	100	D	Y	Version	 	I	2008-06-20 10:40:00	100
52486	pmenu.new.reports	0	0	2008-06-20 10:40:00	100	D	Y	Reports	 	I	2008-06-20 10:40:00	100
52487	isCreatePriceList	0	0	2008-06-20 10:40:00	100	D	Y	Create Price List	 	I	2008-06-20 10:40:00	100
52488	discount.upto.price.limit	0	0	2008-12-21 03:58:43.310358	100	D	Y	Enforce Discount Upto Limit Price	\N	I	2008-12-21 03:58:43.310358	100
52489	over.ride.limit.price	0	0	2008-12-21 03:58:43.315644	100	D	Y	Enforce Overide Limit Price	\N	I	2008-12-21 03:58:43.315644	100
52490	discount.allowed.total	0	0	2008-12-21 03:58:43.316687	100	D	Y	Allowed Discount On Total Sales	\N	I	2008-12-21 03:58:43.316687	100
52491	smenu.pos.purchase.report	0	0	2008-12-21 03:58:43.319215	100	D	Y	Purchases Report	\N	I	2008-12-21 03:58:43.319215	100
52492	smenu.settle.payment.credit.purchase	0	0	2008-12-21 03:58:43.320168	100	D	Y	Settle Payment For Purchases On Credit	\N	I	2008-12-21 03:58:43.320168	100
52493	smenu.user.manual	0	0	2008-12-21 03:58:43.321162	100	D	Y	User Manual	\N	I	2008-12-21 03:58:43.321162	100
52494	smenu.contactus	0	0	2008-12-21 03:58:43.328451	100	D	Y	Contact Us	\N	I	2008-12-21 03:58:43.328451	100
52495	discount.amt.order.level	0	0	2008-12-21 03:58:43.329404	100	D	Y	Discount Amount	\N	I	2008-12-21 03:58:43.329404	100
52496	write.off.amt.order.level	0	0	2008-12-21 03:58:43.330352	100	D	Y	Writeoff Amount	\N	I	2008-12-21 03:58:43.330352	100
53001	WarehouseStock	0	0	2007-07-18 00:00:00	100	D	Y	Item Availability in other Warehouses	\N	M	2007-07-18 00:00:00	100
53002	BookmarkExist	0	0	2007-07-13 00:00:00	0	D	Y	This Bookmark already exist	Selected Menu Item is already registered on Bookmarks Bar	E	2007-07-13 00:00:00	0
53003	CannotDeleteCashGenInvoice	0	0	2020-06-25 14:46:07.19364	0	D	Y	Cannot delete line with generated Invoice	\N	E	2020-06-25 14:46:04.692696	0
53004	CannotChangeCashGenInvoice	0	0	2020-06-25 14:46:07.19364	0	D	Y	Cannot change line with generated Invoice	\N	E	2020-06-25 14:46:04.692696	0
53005	QueryName	0	0	2007-10-10 00:00:00	100	D	Y	Query Name	\N	I	2007-10-10 00:00:00	100
53006	OverUnderAmt	0	0	2007-10-26 01:01:50	100	D	Y	Over/Under Payment	\N	I	2007-10-26 01:01:50	100
53007	LogMigrationScript	0	0	2007-11-11 01:54:49	100	D	Y	Log Migration Script	Log Migration Script - Save migration scripts file in %TEMP%/migration_script_*.sql	I	2007-11-11 01:54:49	100
53008	Vendor	0	0	2007-11-15 21:19:10	100	D	Y	Vendor	\N	I	2007-11-15 21:19:10	100
53009	Can't Save Org Level	0	0	2007-12-15 13:24:57	100	D	Y	This is a system or client parameter, you can't save it as organization parameter	\N	E	2007-12-15 13:24:57	100
53010	Can't Save Client Level	0	0	2007-12-15 13:25:16	100	D	Y	This is a system parameter, you can't save it as client parameter	\N	E	2007-12-15 13:25:16	100
53011	MRP-020	0	0	2007-12-17 08:45:52	0	D	Y	Create	Indicates that a supply order should be created to satisfy a negative projected on hand. This message is created if the flag 'Create Plan' is No.	I	2009-01-21 13:53:33	100
53012	MRP-030	0	0	2007-12-17 08:45:54	0	D	Y	De-Expedite	Indicates that a schedule supply order is due before it is needed and should be delayed, or demand rescheduled to an earlier date.	I	2009-01-21 13:01:11	100
53013	MRP-040	0	0	2007-12-17 08:45:54	0	D	Y	Expedite	Indicates that a scheduled supply order is due after is needed and should be rescheduled to an earlier date, or demand rescheduled to a later date.	I	2009-01-21 13:05:19	100
53014	MRP-050	0	0	2007-12-17 08:45:55	0	D	Y	Cancel	Indicate that a scheduled supply order is no longer needed and should be deleted.	I	2007-12-17 08:45:55	0
53015	MRP-060	0	0	2007-12-17 08:45:57	0	D	Y	Release Due For	Indicate tha a supply order should be released. if it is a draft order , it must also be approved.	I	2009-01-21 14:05:30	100
53016	MRP-070	0	0	2007-12-17 08:45:58	0	D	Y	Release Past Due For	Indicates that a supply order was not released when it was due, and should be either released or expedited now, or the demand rescheduled for a later date.\t	I	2009-01-21 13:12:51	100
53017	MRP-080	0	0	2007-12-17 08:45:59	0	D	Y	Quantity  Less than Minimum	Indicates that a supply order was created for a quantity less than the minimum quantity set in Product Planning Data	I	2009-01-21 12:24:27	100
53018	MRP-090	0	0	2007-12-17 08:46:01	0	D	Y	Quantity  Less than Maximum	Indicates that a supply order was created for a quantity for a quantity greater than than maximum quantity set in the Product Planning Data	I	2009-01-21 12:33:17	100
53019	MRP-100	0	0	2007-12-17 08:46:02	0	D	Y	Time Fence Conflict	Indicates that there is an unsatisfied material requirement inside the planning time fence for this product. You should either manually schedule and expedite orders to fill this demand or delay fulfillment of the requirement that created the demand.	I	2009-01-21 13:19:19	100
53020	MRP-110	0	0	2007-12-17 08:46:03	0	D	Y	Past Due	Indicates that a schedule supply order receipt is past due.	I	2009-01-21 13:15:03	100
53021	MRP-120	0	0	2007-12-17 08:46:04	0	D	Y	No Data Planning	indicates that there is no data planning for this product	I	2009-01-21 12:06:51	100
53022	MRP-001	0	0	2007-12-17 08:46:05	0	D	Y	Beginning Available Less Than Zero	Indicates that the quantity on hand is less that safety stock	I	2009-01-21 11:19:44	100
53023	WrongScriptValue	0	0	2008-01-24 22:27:19	100	D	Y	Wrong Script Value - format for JSR 223 Script must be engine:scriptName where supported engines must be something like groovy, jython, beanshell	\N	E	2008-01-24 22:27:19	100
53024	ShowAllWindow	0	0	2008-02-01 12:15:39	100	D	Y	Show All Windows	\N	I	2008-02-01 12:15:39	100
53025	BuildVersionError	0	0	2008-02-13 00:10:26	100	D	Y	Build Version Error	The program assumes build version {0}, but database has build version {1}. \nThis is likely to cause hard to fix errors. \nPlease contact administrator.	E	2008-02-13 00:10:26	100
53026	FileXLS	0	0	2008-02-18 12:02:11	0	D	Y	xls - Excel file	\N	E	2008-02-18 12:02:11	0
53027	FromSameWarehouseOnly	0	0	2008-02-21 12:53:13	100	D	Y	Only from same warehouse	Show only orders with the same warehouse as selected in the material receipt	I	2008-02-21 12:53:13	100
53029	AllocDate	0	0	2008-05-08 11:13:10	100	D	Y	Choose the date of the most recent payment that you plan to allocate, but it must be in an open accounting period or it will not post.	Prefer most recent payment date, within open period.	I	2008-05-08 11:13:10	100
53031	InfoMRP	0	0	2008-05-17 21:58:27	0	D	Y	MRP Info	\N	M	2008-05-17 21:58:27	0
53033	InfoCRP	0	0	2008-05-17 21:59:35	0	D	Y	CRP Info	\N	M	2008-05-17 21:59:35	0
53035	DRP-010	0	0	2008-05-19 01:27:57	0	D	Y	Do not exist Transit Warehouse to this Organization	Do not exist Transit Warehouse to this Organization	I	2008-05-19 01:27:57	0
53036	DRP-020	0	0	2008-05-19 01:28:47	0	D	Y	Org targer do not linked to BPartner	Org targer do not linked to BPartner	I	2008-05-19 01:28:47	0
53038	CheckMissingTrl	0	0	2008-06-20 14:06:43	0	D	Y	Check Missing Translation Records	\N	E	2008-06-20 14:06:43	0
53039	DRP-001	0	0	2008-06-23 00:30:56	0	D	Y	Do not exist default Locator for this Warehouse	Do not exist default Locator for this Warehouse	I	2009-01-21 12:35:18	100
53040	DRP-030	0	0	2008-06-23 00:31:52	0	D	Y	Do not exist Shipper for Create Distribution Order	Do not exist Shipper for Create Distribution Order	I	2008-06-23 00:31:52	0
53043	SaveUomError	0	0	2008-07-22 15:46:13	100	D	Y	UOM can't be changed if the product has movements or costs	\N	E	2008-07-22 15:46:13	100
53044	PaymentAllocateIgnored	0	0	2008-10-01 22:10:18	100	D	Y	Payment Allocate must not exists if the payment header has charge/invoice/order.	\N	E	2008-10-01 22:10:18	100
53045	PaymentAllocateSumInconsistent	0	0	2008-10-01 22:17:41	100	D	Y	Payment Amount must be equal to the sum of Allocate amounts.	\N	E	2008-10-01 22:17:41	100
53046	AlreadyPostedTo	0	0	2008-11-21 14:21:30	0	D	Y	Already posted to this account.	\N	E	2008-11-21 14:21:30	0
53047	InvoiceFullyMatched	0	0	2008-12-08 11:37:49	0	D	Y	Invoice is fully matched.	\N	E	2008-12-08 11:37:49	0
53048	MRP-130	0	0	2008-12-08 18:04:06	0	D	Y	No Current Vendor Selected	Indicates that the Product has no Current Vendor selected	I	2008-12-08 18:04:06	0
53049	SeeAttachment	0	0	2008-12-12 10:46:44	0	D	Y	Check attached file	\N	I	2008-12-12 10:46:44	0
53050	NoVendorForProduct	0	0	2008-12-23 12:19:53	0	D	Y	No Vendor for Product : 	\N	E	2008-12-23 12:19:53	0
53051	DRP-040	0	0	2009-01-21 13:26:01	100	D	Y	Shipment Due	Indicates that a shipment for a Order Distribution is due.You should be taken at the source warehouse to ensure that the order is received on time.	I	2009-01-21 13:36:55	100
53052	DRP-050	0	0	2009-01-21 13:28:14	100	D	Y	Shipment Past Due	Indicates that a shipment for a Order Distribution is past due. You should either delay the orders created the requirement for the product or expedite them when the product does arrive.	I	2009-01-21 13:37:58	100
53053	DRP-060	0	0	2009-01-21 13:30:22	100	D	Y	No source of supply	Indicates that the Product Planning Data for this product does not specify a valid network distribution.	I	2009-01-21 13:30:22	100
53054	MRP-140	0	0	2009-01-21 13:40:08	100	D	Y	Beginning Quantity Less Than Zero	Indicates that the quantity on hand is negative.	I	2009-01-21 13:40:08	100
53055	MRP-150	0	0	2009-01-21 15:05:43	100	D	Y	Past Due Demand	Indicates that a demand order is past due.	I	2009-01-21 15:05:43	100
53056	BPartnerNoShipToAddress	0	0	2009-05-05 12:30:26	0	D	Y	Business Partner has no Ship To Address	\N	E	2009-05-05 12:30:26	0
53057	BPartnerNoBillToAddress	0	0	2009-05-05 12:32:00	0	D	Y	Business Partner has no Bill To Address	\N	E	2009-05-05 12:32:00	0
53058	BPartnerNoAddress	0	0	2009-05-05 12:48:42	0	D	Y	Business Partner has no Address	\N	E	2009-05-05 12:48:42	0
53059	CurrentRecordModified	0	0	2009-05-08 13:16:41	100	D	Y	Current record was changed by another user, please ReQuery	\N	E	2009-05-08 13:16:41	100
53060	ParentRecordModified	0	0	2009-05-08 13:17:20	100	D	Y	Record on parent tab was changed by another user, please navigate to tab and ReQuery	\N	E	2009-05-08 13:17:20	100
53072	InActiveAccount	0	0	2009-05-11 14:02:00	0	D	Y	In active account	\N	E	2009-05-11 14:02:00	0
53073	PostingTypeActualError	0	0	2009-05-11 14:02:52	0	D	Y	Account does not have Post Actual attribute	\N	E	2009-05-11 14:02:52	0
53074	PostingTypeBudgetError	0	0	2009-05-11 14:03:12	0	D	Y	Account does not have Post Budget attribute	\N	E	2009-05-11 14:03:12	0
53075	PostingTypeStatisticalError	0	0	2009-05-11 14:03:36	0	D	Y	Account does not have Post Statistical attribute	\N	E	2009-05-11 14:03:36	0
53076	WindowTabCollapsible	0	0	2009-05-22 16:19:08	100	D	Y	Window Tab Collapsible	Make the vertical tab of window collapsible	I	2009-05-22 16:19:08	100
53077	WindowTabPlacement	0	0	2009-05-22 16:21:24	100	D	Y	Window Tab Placement	Placement of vertical tab, supported options are left ( default ) and right	I	2009-05-22 16:21:24	100
53078	CityNotFound	0	0	2009-05-27 11:21:48	0	D	Y	The city does not exist and can not be created from here.	\N	E	2009-05-27 11:21:48	0
53079	MRP-999	0	0	2009-06-05 13:20:36	0	D	Y	Unknown MRP Error	Indicates that there was an unclassified error durring MRP process	I	2009-06-05 13:20:36	0
53080	MRP-160	0	0	2009-06-05 13:27:16	0	D	Y	Cannot Create Document	Indicates that there was an error durring document creation	I	2009-06-05 13:27:16	0
53081	NoUOMConversion	0	0	2009-06-11 08:43:49	0	D	Y	No UOM conversion found	\N	E	2009-06-11 08:43:49	0
53082	CustomerRMA	0	0	2009-09-12 18:40:21	100	D	Y	Customer RMA	\N	I	2009-09-12 18:40:21	100
53083	ClientAccountingNotEnabled	0	0	2009-09-13 18:15:24	100	D	Y	Client Accounting is not enabled	In order to run this process you need to enable client accounting, this can be done in window System Configurator, setting the parameter CLIENT_ACCOUNTING to [I]mmediate or [Q]ueue	E	2009-09-13 18:15:24	100
53084	NotExistsBPLinkedforOrgError	0	0	2009-10-11 02:05:20	0	D	Y	Not exists a Business Partner linked for Organization	\N	E	2009-10-11 02:05:20	0
53085	PrintMovements	0	0	2009-10-11 02:11:23	0	D	Y	Print Movements	\N	I	2009-10-11 02:11:23	0
53086	InventoryMoveGenerateSel	0	0	2009-10-12 21:53:44	0	D	Y	Select Distribution Orders to generate Inventory Moves	\N	I	2009-10-12 21:53:44	0
53087	InventoryMoveGenerateInfo	0	0	2009-10-12 21:55:40	0	D	Y	Inventory Moves are generated depending on the "Delivery Rule" selection in the Distribution Order	\N	I	2009-10-12 21:55:40	0
53088	MRelationType_Err_KeyColumns_2P	0	0	2009-11-13 15:49:24	100	D	Y	PO {0} has {1} key columns. Needs to have exactly one.	\N	E	2009-11-13 15:49:24	100
53089	MRelationType_Err_Window_3P	0	0	2009-11-13 15:51:16	100	D	Y	Neither reference {0} nor table {1} have an AD_Window_ID. IsSOTrx: {2}	\N	E	2009-11-13 15:51:16	100
53090	positive.number	0	0	2009-11-18 11:28:01	0	D	Y	Must be a positive number!	 	I	2009-11-18 11:28:01	0
53091	RememberMe	0	0	2009-12-14 20:32:29	100	D	Y	Remember Me	\N	I	2009-12-14 20:32:29	100
53092	And/Or	0	0	2010-02-08 15:00:52	100	D	Y	And/Or	\N	I	2010-02-08 15:00:52	100
53093	AND	0	0	2010-02-08 15:01:19	100	D	Y	AND	\N	I	2010-02-08 15:01:19	100
53094	OR	0	0	2010-02-08 15:01:34	100	D	Y	OR	\N	I	2010-02-08 15:01:34	100
53095	InvalidCronPattern	0	0	2010-02-22 09:31:41	100	D	Y	Invalid cron scheduling pattern	Invalid cron scheduling pattern - check syntax	E	2010-02-22 09:31:41	100
53096	SubTotal	0	0	2010-03-24 11:02:58	100	D	Y	Subtotal	\N	I	2010-03-24 11:02:58	100
53097	AmountTendered	0	0	2010-03-24 11:04:47	100	D	Y	Tender Amount	\N	I	2010-03-24 11:04:47	100
53098	AmountReturned	0	0	2010-03-24 11:05:03	100	D	Y	Change	\N	I	2010-03-24 11:05:03	100
53099	SessionTimeoutText	0	0	2010-03-26 01:55:36	100	D	Y	The page or component you request is no longer available. This is normally caused by timeout or rebooting the server.	\N	I	2010-03-26 01:55:36	100
53117	Cc	0	0	2010-11-10 11:06:52	100	D	Y	Cc	\N	E	2010-11-10 11:06:52	100
53118	NegativeOnhandExists	0	0	2010-11-29 12:28:55	100	D	Y	Negative on hand exists in this warehouse	\N	E	2010-11-29 12:28:55	100
53119	ErrorPaymentSchedule	0	0	2010-12-08 20:10:51	100	D	Y	Error in payment schedule	Check the payment schedule.  If the payment term has a schema then the document must have a schedule (is auto-generated if empty) and the schedule must sum the total of the document.  If the payment term doesn't have a schema, then the document must not have schedule.	E	2010-12-08 20:10:51	100
53120	Key_Clear	0	0	2010-12-11 09:42:16	100	D	Y	c	\N	I	2010-12-11 09:42:16	100
53121	Clear	0	0	2010-12-11 09:42:36	100	D	Y	c	\N	I	2010-12-11 09:42:36	100
53122	TableMustHaveProcessButton	0	0	2010-12-11 17:52:59	100	D	Y	The table doesn't have associated button for the process	Choose a table with a column associated to the process	E	2010-12-11 17:52:59	100
53123	MustFillTable	0	0	2010-12-11 17:54:07	100	D	Y	To use Record ID you need to choose a table	Fill the table field first	E	2010-12-11 17:54:07	100
53124	NoRecordID	0	0	2010-12-11 17:54:57	100	D	Y	Record ID doesn't exist in the table	Fill properly the Record ID field with a valid ID on the chosen table	E	2010-12-11 17:54:57	100
53125	FilesOfType	0	0	2010-12-11 19:24:41	100	D	Y	Files of Type:	\N	I	2010-12-11 19:24:41	100
53126	SaveBeforeClose	0	0	2010-12-16 13:51:48	100	D	Y	Please save changes before closing	\N	I	2010-12-16 13:51:48	100
53127	Credits	0	0	2010-12-16 14:08:26	100	D	Y	Credit	\N	I	2010-12-16 14:08:26	100
53128	DateTime	0	0	2010-12-16 14:33:47	100	D	Y	Date/Time	\N	I	2010-12-16 14:34:00	100
53129	Class.Method	0	0	2010-12-16 14:34:29	100	D	Y	Class.Method	\N	I	2010-12-16 14:34:29	100
53130	Message	0	0	2010-12-16 14:34:55	100	D	Y	Message	\N	I	2010-12-16 14:34:55	100
53131	Trace	0	0	2010-12-16 14:35:30	100	D	Y	Trace	\N	I	2010-12-16 14:35:30	100
53132	NegativeInventoryDisallowed	0	0	2010-12-31 12:43:23	100	D	Y	Negative inventory disallowed in this warehouse	\N	E	2010-12-31 12:43:23	100
53136	Total	0	0	2011-07-04 12:21:51	100	D	Y	Total	\N	I	2011-07-04 12:21:51	100
53137	ChargeDifference	0	0	2011-07-05 11:41:29	100	D	Y	Charge to apply difference amount to.	\N	I	2011-07-05 11:41:29	100
53138	ProductOrChargeRequired	0	0	2011-07-28 15:10:34	100	D	Y	Product or Charge is required if quantity is non-zero.	\N	I	2011-07-28 15:10:34	100
53141	CopyReport	0	0	2011-08-19 15:02:59	100	D	Y	Copy Report	\N	M	2011-08-19 15:02:59	100
53145	SavedParameter	0	0	2011-09-08 15:47:39	100	D	Y	Saved Parameter Values	\N	I	2011-11-02 16:34:58	0
53146	SavedParameterTip	0	0	2011-09-08 15:51:54	100	D	Y	Enter a name to store the currently selected parameters for future use.	\N	I	2011-11-02 16:34:58	0
53147	LastRun	0	0	2011-09-08 15:54:29	100	D	Y	Last Run	\N	I	2011-09-08 15:54:29	100
53148	Add	0	0	2011-09-23 18:53:11	100	D	Y	Add	\N	I	2011-09-23 18:53:11	100
53149	Reply	0	0	2011-09-23 18:54:35	100	D	Y	Reply	\N	I	2011-09-23 18:54:35	100
53161	RMACannotBeVoided	0	0	2011-11-22 20:15:33	100	D	Y	This RMA cannot be voided because it has associated returns, please void the returns first	\N	E	2011-11-22 20:15:33	100
53162	Path	0	0	2012-03-06 17:12:46	100	D	Y	Path	\N	I	2012-06-08 15:17:23	0
53163	MimeType	0	0	2012-03-06 17:13:07	100	D	Y	MIME Type	\N	I	2012-06-08 15:17:23	0
53164	TextPreview	0	0	2012-03-06 17:13:28	100	D	Y	Text Preview	\N	I	2012-06-08 15:17:23	0
53165	SearchRepository	0	0	2012-03-06 17:14:34	100	D	Y	Search Repository	\N	I	2012-06-08 15:17:23	0
53166	StoreImageWarning	0	0	2012-04-13 15:52:30	100	D	Y	If you change the image storage method, the old image entries are no longer available to your client.	\N	I	2012-04-13 15:52:30	100
53272	DateSelectionFuture	0	0	2014-05-28 17:47:27	100	D	Y	Please confirm Date, it looks in future :	\N	I	2014-05-28 17:47:27	100
53273	DateSelectionOld	0	0	2014-05-28 17:47:40	100	D	Y	Date is outside window of +/- 60 days. Please review.	\N	I	2016-04-11 16:27:20	0
53278	add.as.root	0	0	2014-08-05 16:41:40	100	D	Y	Add As Root	\N	I	2014-08-05 16:41:40	100
53279	add.folder	0	0	2014-08-05 16:41:56	100	D	Y	Add Folder	\N	I	2014-08-05 16:41:56	100
53280	could.not.create.node	0	0	2014-08-05 16:42:14	100	D	Y	Could not create Node.	\N	I	2014-08-05 16:42:14	100
53281	insert.after	0	0	2014-08-05 16:42:27	100	D	Y	Insert After	\N	I	2014-08-05 16:42:27	100
53282	its.already.there	0	0	2014-08-05 16:42:43	100	D	Y	It's already there !	\N	I	2014-08-05 16:42:43	100
53283	move.into	0	0	2014-08-05 16:42:56	100	D	Y	Move Into	\N	I	2014-08-05 16:42:56	100
53288	ZKHome	0	0	2014-10-13 16:20:16	100	D	Y	Home	\N	I	2014-10-13 16:20:16	100
53289	ColumnWidthNotSaved	0	0	2014-11-17 10:25:43	100	D	Y	Column width not saved.	\N	E	2014-11-17 10:25:43	100
53290	ColumnWidthSaved	0	0	2014-11-17 10:26:07	100	D	Y	Column Width Saved Successfully.	\N	I	2014-11-17 10:26:07	100
53291	SaveColumnWidth	0	0	2014-11-17 10:26:28	100	D	Y	Save Column Width	\N	I	2014-11-17 10:26:28	100
53309	POS.IsCancel	0	0	2015-10-29 18:16:57	100	D	Y	Cancel Order	\N	I	2015-10-29 18:16:57	100
53310	POS.MustCreateOrder	0	0	2015-10-29 18:20:30	100	D	Y	You must create an Order first	\N	I	2015-10-29 18:20:30	100
53311	POS.DeleteOrder	0	0	2015-10-29 18:21:43	100	D	Y	Do you want to delete Order?	\N	I	2015-10-29 18:21:43	100
53312	POS.OrderIsAlreadyCompleted	0	0	2015-10-29 18:22:33	100	D	Y	The order is already completed. Do you want to void it?	\N	I	2015-10-29 18:22:33	100
53313	IsCreditSale	0	0	2015-10-29 18:29:09	100	D	Y	Credit Sale	\N	I	2015-10-29 18:29:09	100
53314	POS.AlternateDT	0	0	2015-10-29 18:31:16	100	D	Y	Alternate Document Type	\N	I	2015-10-29 18:31:16	100
53315	POS.OrderIsNotProcessed	0	0	2015-11-05 01:35:38	100	D	Y	Order is not Drafted nor Completed. Try to delete it other way	\N	I	2015-11-05 01:35:38	100
53317	changeRole	0	0	2016-01-30 12:02:44	100	D	Y	Change Role	\N	I	2016-01-30 12:02:44	100
53318	SerialNoInquiryForm	0	0	2016-02-17 13:33:37	100	D	Y	Serial No Inquiry Form	\N	I	2016-02-17 13:33:37	100
53319	FileXLSX	0	0	2016-03-18 15:01:29	100	D	Y	xlsx - XLSX file	\N	E	2016-03-18 15:01:29	100
53324	OpenInGridMode	0	0	2016-06-24 19:20:10	100	D	Y	Open in Grid Mode	\N	I	2016-06-24 19:20:10	100
53325	QuickEntry	0	0	2016-06-24 19:26:22	100	D	Y	Quick Entry	\N	I	2016-06-24 19:26:22	100
53338	StartExpanded	0	0	2017-01-31 14:40:06	100	D	Y	Start Expanded	\N	I	2017-01-31 14:40:06	100
53339	StartCollapsed	0	0	2017-01-31 14:40:17	100	D	Y	Start Collapsed	\N	I	2017-01-31 14:40:17	100
200000	UnprocessedDocs	0	0	2012-01-27 09:43:01	100	D	Y	Unprocessed Documents	\N	M	2012-01-27 09:43:01	100
200001	Map	0	0	2012-02-11 12:14:41	100	D	Y	Map	\N	I	2012-02-11 12:14:41	100
200002	Route	0	0	2012-02-11 12:15:05	100	D	Y	Route	\N	I	2012-02-11 12:15:05	100
200003	ChargeOrProductMandatory	0	0	2012-03-07 15:18:34	100	D	Y	On this document is mandatory to fill the product or the charge	\N	I	2012-03-07 15:18:34	100
400001	Search Menu	0	0	2022-04-06 21:36:01	100	D	Y	Search Menu [Ctrl + k]	\N	I	2022-10-30 23:31:00	100
400002	Attachments	0	0	2022-04-06 21:36:26	100	D	Y	Attachments	\N	I	2022-04-06 21:36:26	100
400003	Related records	0	0	2022-04-06 21:36:45	100	D	Y	Related records	\N	I	2022-04-06 21:36:45	100
400004	Archived Docs	0	0	2022-04-06 21:37:09	100	D	Y	Archived Docs	\N	I	2022-04-06 21:37:09	100
400005	Language	0	0	2022-04-06 21:37:22	100	D	Y	Language	\N	I	2022-04-06 21:37:22	100
400006	Business Partner Info	0	0	2022-04-06 21:37:54	100	D	Y	Business Partner Info	\N	I	2022-04-06 21:37:54	100
520010	1_rupee	0	0	2008-03-26 13:20:02.328	100	D	Y	1 Rupee	 	I	2008-03-26 13:20:02.328	100
523847	smenu.slow.moving.items	0	0	2008-03-26 13:20:03.187	100	D	Y	Slow Moving Items	 	I	2008-03-26 13:20:03.187	100
523848	ParentComplete	0	0	2009-12-09 15:20:29	100	D	Y	The parent record (document) is already processed	\N	E	2009-12-09 15:21:00	100
523849	FillShipLineOrCharge	0	0	2009-12-09 15:20:57	100	D	Y	Shipment/Receipt Line or charge should be entered	\N	E	2009-12-09 15:20:57	100
523850	JustShipLineOrCharge	0	0	2009-12-09 15:21:38	100	D	Y	Either shipment/receipt line or charge should be selected	\N	E	2009-12-09 15:21:38	100
523851	AmtReturned>Shipped	0	0	2009-12-09 15:22:20	100	D	Y	Amount to be returned is greater than the amount shipped	\N	E	2009-12-09 15:22:20	100
523852	InOutLineAlreadyEntered	0	0	2009-12-09 15:23:02	100	D	Y	Shipment/Receipt line is already defined in another line	\N	E	2009-12-09 15:23:02	100
523853	RMA.IsSOTrx <> InOut.IsSOTrx	0	0	2009-12-09 15:23:35	100	D	Y	Shipment/Receipt has different Sales/Purchase transaction than RMA	\N	E	2009-12-09 15:23:35	100
\.
