copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50949	c_campaign_value	0	0	274	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	UNIQUE (ad_client_id, value)	CU	\N	2022-11-02 05:57:56.041854	100
50950	c_campaign_pkey	0	0	274	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_campaign_id)	CP	\N	2022-11-02 05:57:56.041854	100
50951	cchannel_ccampaign	0	0	274	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_channel_id) REFERENCES c_channel(c_channel_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50973	c_channel_name	0	0	275	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50974	c_channel_pkey	0	0	275	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_channel_id)	CP	\N	2022-11-02 05:57:56.041854	100
50975	adprintcolor_cchannel	0	0	275	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (ad_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50998	c_commission_pkey	0	0	429	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_commission_id)	CP	\N	2022-11-02 05:57:56.041854	100
50999	cbpartner_ccommission	0	0	429	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51000	ccharge_ccommission	0	0	429	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_charge_id) REFERENCES c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51001	ccurrency_ccommission	0	0	429	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51002	c_commissionamt_pkey	0	0	430	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_commissionamt_id)	CP	\N	2022-11-02 05:57:56.041854	100
51003	ccommentrun_ccommissionamt	0	0	430	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_commissionrun_id) REFERENCES c_commissionrun(c_commissionrun_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51004	ccomline_ccomamt	0	0	430	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_commissionline_id) REFERENCES c_commissionline(c_commissionline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51005	c_commissiondetail_pkey	0	0	437	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_commissiondetail_id)	CP	\N	2022-11-02 05:57:56.041854	100
51006	ccommissionamt_ccomdetail	0	0	437	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_commissionamt_id) REFERENCES c_commissionamt(c_commissionamt_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51007	cinvoiceline_ccommissiondet	0	0	437	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_invoiceline_id) REFERENCES c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51008	ccurrency_ccommissiondetail	0	0	437	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51009	corderline_ccommissiondetail	0	0	437	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_orderline_id) REFERENCES c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51010	c_commissionline_pkey	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_commissionline_id)	CP	\N	2022-11-02 05:57:56.041854	100
51011	cbpgroup_commissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_bp_group_id) REFERENCES c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51012	mproductcat_ccommissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (m_product_category_id) REFERENCES m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51013	csalesregion_ccommissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_salesregion_id) REFERENCES c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51014	adorgtrx_ccommissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51015	cbpartner_ccommissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51016	ccommission_ccommissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_commission_id) REFERENCES c_commission(c_commission_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51017	mproduct_ccommissionline	0	0	431	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51018	c_commissionrun_pkey	0	0	436	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_commissionrun_id)	CP	\N	2022-11-02 05:57:56.041854	100
51019	ccommission_ccommissionrun	0	0	436	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (c_commission_id) REFERENCES c_commission(c_commission_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51020	c_contactactivity_key	0	0	53354	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_contactactivity_id)	CP	\N	2022-11-02 05:57:56.041854	100
51249	c_opportunity_key	0	0	53337	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_opportunity_id)	CP	\N	2022-11-02 05:57:56.041854	100
51508	c_salesregion_value	0	0	230	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	UNIQUE (ad_client_id, value)	CU	\N	2022-11-02 05:57:56.041854	100
51509	c_salesregion_pkey	0	0	230	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	PRIMARY KEY (c_salesregion_id)	CP	\N	2022-11-02 05:57:56.041854	100
51510	salesrep_csalesregion	0	0	230	2022-11-02 05:57:56.041854	100	Sales and Marketing	\N	Y	FOREIGN KEY (salesrep_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52932	c_commissionamt_comline	0	0	430	2022-11-02 06:15:05.061147	100	Sales and Marketing	\N	Y	 USING btree (c_commissionline_id)	I	\N	2022-11-02 06:15:05.061147	100
52933	c_commissionamt_run	0	0	430	2022-11-02 06:15:05.061147	100	Sales and Marketing	\N	Y	 USING btree (c_commissionrun_id)	I	\N	2022-11-02 06:15:05.061147	100
52934	c_commissionline_commission	0	0	431	2022-11-02 06:15:05.061147	100	Sales and Marketing	\N	Y	 USING btree (c_commission_id)	I	\N	2022-11-02 06:15:05.061147	100
53413	c_campaign_isactive_check	0	0	274	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53420	c_channel_isactive_check	0	0	275	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53434	c_commission_createfrom_check	0	0	429	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((createfrom = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53435	c_commission_isactive_check	0	0	429	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53436	c_commission_listdetails_check	0	0	429	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((listdetails = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53437	c_commissionamt_isactive_check	0	0	430	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53438	c_commissiondetail_isactive_check	0	0	437	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53439	c_commissionline_isactive_check	0	0	431	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53440	c_commissionline_ispositiveonly_check	0	0	431	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((ispositiveonly = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53441	c_commissionline_commissionorders_check	0	0	431	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((commissionorders = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53442	c_commissionrun_processed_check	0	0	436	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53443	c_commissionrun_isactive_check	0	0	436	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53444	c_contactactivity_iscomplete_check	0	0	53354	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((iscomplete = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53445	c_contactactivity_isactive_check	0	0	53354	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53574	c_opportunity_isactive_check	0	0	53337	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53681	c_salesregion_isactive_check	0	0	230	2022-11-02 12:40:39.392787	100	Sales and Marketing	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
