copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
116	Project	W	0	\N	\N	0	\N	130	1999-06-29 00:00:00	0	Maintain Projects	Project Management	Y	Y	N	Y	N	2006-03-26 19:42:05	100
160	Projects	\N	0	\N	\N	0	\N	\N	1999-12-02 12:58:59	0	\N	Project Management	Y	N	N	Y	Y	2022-02-27 23:37:24	100
258	Project Reporting	W	0	\N	\N	0	\N	208	2001-03-15 16:52:50	0	Maintain Project Reporting Cycles	Project Management	N	Y	N	N	N	2019-12-05 14:13:05	100
260	Project Status Summary	R	0	\N	\N	0	162	\N	2001-03-27 14:10:53	0	Project Status of Project Cycle	Project Management	N	Y	N	Y	N	2000-01-02 00:00:00	0
364	Project Type	W	0	\N	\N	0	\N	265	2003-05-28 22:08:57	0	Maintain Project Type and Phase	Project Management	Y	Y	N	Y	N	2000-01-02 00:00:00	0
387	Project (Lines/Issues)	W	0	\N	\N	0	\N	286	2003-07-10 15:52:32	0	Maintain Sales Order and Work Order Details	Project Management	N	Y	N	Y	N	2019-12-05 14:02:57	100
398	Project Cycle Report	R	0	\N	\N	0	218	\N	2003-08-17 17:04:31	0	Report Projects based on Project Cycle	Project Management	N	Y	N	Y	N	2019-12-05 14:13:10	100
401	Issue to Project	P	0	\N	\N	0	224	\N	2003-09-02 20:27:08	0	Issue Material to Project from Receipt or manual Inventory Location	Project Management	N	Y	N	N	N	2022-02-28 00:20:27	100
402	Generate PO from Project	P	0	\N	\N	0	225	\N	2003-09-02 20:27:29	0	Generate PO from Project Line(s)	Project Management	N	Y	N	N	N	2022-02-28 00:20:22	100
403	Project Detail Accounting Report	R	0	\N	\N	0	226	\N	2003-09-02 21:02:47	0	Accounting Fact Details of Project	Project Management	Y	Y	N	Y	N	2000-01-02 00:00:00	0
404	Project Lines not Issued	R	0	\N	\N	0	228	\N	2003-09-04 16:13:36	0	Lists Project Lines of a Work Order or Asset Project, which are not issued to the Project	Project Management	Y	Y	N	N	N	2000-01-02 00:00:00	0
405	Project POs not Issued	R	0	\N	\N	0	229	\N	2003-09-04 16:13:58	0	Lists Project Lines with generated Purchase Orders of a Work Order or Asset Project, which are not issued to the Project	Project Management	Y	Y	N	N	N	2000-01-02 00:00:00	0
410	Project Margin (Work Order)	R	0	\N	\N	0	234	\N	2003-10-12 23:34:57	0	Work Order Project Lines (planned revenue) vs. Project Issues (costs)	Project Management	N	Y	N	N	N	2019-12-05 14:13:00	100
53815	Project Report	P	0	\N	\N	0	53619	\N	2014-04-10 20:24:20	100	\N	Project Management	Y	Y	N	N	N	2014-04-10 20:24:20	100
\.
