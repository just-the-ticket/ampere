copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
50001	AD_Package_Type	L	0	0	2006-12-11 23:46:19	0	Defines the type of packages available	Application Packaging	\N	Y	N	2006-12-12 00:07:56	0	\N
50002	Package_Releases	L	0	0	2006-12-11 23:46:22	0	List of Package Releases	Application Packaging	Indicates what release the package is for.  Entering no specific release indicates the package is for all releases.	Y	N	2006-12-12 00:11:11	0	\N
50004	AD_Package_Build_Type	L	0	0	2006-12-11 23:46:43	0	\N	Application Packaging	\N	Y	N	2006-12-12 00:14:08	0	\N
50005	AD_Package_Source_Type	L	0	0	2006-12-11 23:47:44	0	List of package source types	Application Packaging	\N	Y	N	2006-12-12 00:16:27	0	\N
\.
