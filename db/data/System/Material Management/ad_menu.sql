copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
179	Physical Inventory	W	0	\N	\N	0	\N	168	1999-12-19 21:31:09	0	Enter Physical Inventory	Material Management	Y	Y	N	N	N	2005-07-24 13:09:54	100
181	Inventory Move	W	0	\N	\N	0	\N	170	1999-12-19 21:36:30	0	Inventory Move	Material Management	Y	Y	N	N	N	2005-07-24 13:09:44	100
183	Inventory	\N	0	\N	\N	0	\N	\N	1999-12-20 11:06:25	0	\N	Material Management	Y	N	N	Y	Y	2022-02-18 10:35:13	100
196	Replenish Report	R	0	\N	\N	0	125	\N	2000-05-11 21:11:25	0	Inventory Replenish Report	Material Management	N	Y	N	N	N	2019-12-05 14:12:38	100
197	Transaction Summary	R	0	\N	\N	0	124	\N	2000-05-11 21:12:53	0	Product Transaction Summary	Material Management	Y	Y	N	N	N	2022-02-17 22:46:55	100
228	Production	W	0	\N	\N	0	\N	191	2000-10-15 13:05:29	0	Production based on Bill of Materials	Material Management	N	Y	N	N	N	2019-12-05 14:11:29	100
229	Material Transactions	X	0	103	\N	0	\N	\N	2000-10-31 22:25:09	0	Material Transactions	Material Management	Y	Y	N	N	N	2000-01-02 00:00:00	0
256	Product Transaction Value	R	0	\N	\N	0	161	\N	2001-02-25 22:31:58	0	Product Transaction Value	Material Management	N	Y	N	N	N	2022-02-17 22:37:50	100
292	Material Transactions (indirect use)	W	0	\N	\N	0	\N	223	2001-07-28 19:48:20	0	Material Transactions (indirect use)	Material Management	N	Y	N	Y	N	2000-01-02 00:00:00	0
311	Inventory Valuation Report	R	0	\N	\N	0	180	\N	2002-01-17 22:03:17	0	Inventory Valuation Report	Material Management	Y	Y	N	N	N	2006-01-11 18:41:13	100
411	Storage Detail	R	0	\N	\N	0	236	\N	2003-12-05 00:13:39	0	Storage Detail Report	Material Management	Y	Y	N	N	N	2000-01-02 00:00:00	0
412	Transaction Detail	R	0	\N	\N	0	237	\N	2003-12-05 00:13:58	0	Transaction Detail Report	Material Management	Y	Y	N	N	N	2000-01-02 00:00:00	0
426	BOM Drop	X	0	114	\N	0	\N	\N	2003-12-30 10:44:27	0	Drop (expand) Bill of Materials	Material Management	N	Y	N	N	N	2019-12-05 14:11:45	100
477	Demand	W	0	\N	\N	0	\N	329	2004-04-17 11:13:03	0	Maintain Material Demand	Material Management	N	Y	N	Y	N	2019-12-05 14:12:35	100
479	Ship/Receipt Confirm	W	0	\N	\N	0	\N	330	2004-05-10 17:36:55	0	Material Shipment or Receipt Confirmation	Material Management	N	Y	N	Y	N	2019-12-05 14:11:32	100
481	Open Confirmation Details	R	0	\N	\N	0	284	\N	2004-05-18 21:19:57	0	Open Shipment or Receipt Confirmation Details	Material Management	N	Y	N	N	N	2019-12-05 14:11:38	100
482	Open Confirmations	R	0	\N	\N	0	285	\N	2004-05-18 21:19:57	0	Open Shipment or Receipt Confirmations	Material Management	N	Y	N	N	N	2019-12-05 14:11:35	100
503	Internal Use Inventory	W	0	\N	\N	0	\N	341	2004-11-26 21:16:56	0	Enter Internal Use of Inventory	Material Management	Y	Y	N	N	N	2000-01-02 00:00:00	0
504	Product UOM Convert	P	0	\N	\N	0	311	\N	2004-11-27 23:41:49	0	Brack-up or repackage same Products with different UOM	Material Management	N	Y	N	N	N	2019-12-05 14:12:12	100
515	Material Reference	R	0	\N	\N	0	322	\N	2005-03-30 01:20:47	100	Material Transactions Cross Reference (used/resourced)	Material Management	N	Y	N	Y	N	2019-12-05 14:12:17	100
537	Storage Cleanup	P	0	\N	\N	0	325	\N	2005-05-29 00:47:02	100	Inventory Storage Cleanup	Material Management	N	Y	N	N	N	2019-12-05 14:11:54	100
53253	Storage per Product	R	0	\N	\N	0	53189	\N	2009-12-01 17:07:29	100	\N	Material Management	Y	Y	N	N	N	2009-12-01 17:07:29	100
53626	Stocktake	X	0	53037	\N	0	\N	\N	2013-09-23 13:55:44	100	Physical Inventory stock count form	Material Management	N	Y	N	N	N	2019-12-05 14:12:22	100
53749	Correct Product Reservation	P	0	\N	\N	0	53553	\N	2014-01-16 22:12:36	100	\N	Material Management	Y	Y	N	N	N	2014-01-15 22:12:36	100
53946	Storage Detail by Locator	R	0	\N	\N	0	53750	\N	2015-02-12 15:38:49	0	Storage Detail Report	Material Management	Y	Y	N	N	N	2015-02-12 15:38:49	0
53968	Seasonality	W	0	\N	\N	0	\N	53408	2015-03-31 10:52:48	100	\N	Material Management	N	Y	N	N	N	2019-12-05 14:11:08	100
53970	Product Availability by PCategory	P	0	\N	\N	0	53768	\N	2015-04-01 14:25:46	100	\N	Material Management	N	Y	N	N	N	2019-12-05 14:11:22	100
53971	Product Availability by BP	P	0	\N	\N	0	53766	\N	2015-04-01 14:26:41	100	\N	Material Management	N	Y	N	N	N	2019-12-05 14:11:25	100
53972	Product Availability by BP Summary	P	0	\N	\N	0	53767	\N	2015-04-01 14:26:54	100	\N	Material Management	N	Y	N	N	N	2016-04-11 16:52:26	0
53974	Generate Default Product Replenishment	P	0	\N	\N	0	53771	\N	2015-04-07 11:26:15	100	\N	Material Management	N	Y	N	N	N	2019-12-05 14:11:05	100
53977	Movement Scanner	X	0	53054	\N	0	\N	\N	2015-04-08 13:34:39	100	Inventory Move using Barcode Scanner	Material Management	N	Y	N	N	N	2019-12-05 14:11:00	100
54112	Inventory Valuation Report By Locator	R	0	\N	\N	0	53899	\N	2016-11-03 11:22:46	100	Inventory Valuation Report By Locator	Material Management	Y	Y	N	N	N	2016-11-03 11:22:46	100
\.
