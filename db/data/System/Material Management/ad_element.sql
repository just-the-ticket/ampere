copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
269	DatePromised	0	0	15	\N	1999-11-19 10:07:43	0	Date Order was promised	Material Management	7	The Date Promised indicates the date, if any, that an Order was promised for.	Y	Date Promised	\N	\N	\N	\N	Date Promised	2016-04-11 16:46:55	0
446	Lot	0	0	10	\N	1999-11-19 10:07:43	0	Lot number (alphanumeric)	Material Management	40	The Lot Number indicates the specific lot that a product was part of.	Y	Lot No	\N	\N	\N	\N	Lot No	2016-04-11 16:36:05	0
522	PriorityRule	0	0	17	154	1999-11-19 10:07:43	0	Priority of a document	Material Management	1	The Priority indicates the importance (high, medium, low) of this document	Y	Priority	\N	\N	\N	\N	Priority	2000-01-02 00:00:00	0
568	SerNo	0	0	10	\N	1999-11-19 10:07:43	0	Product Serial Number 	Material Management	40	The Serial Number identifies a tracked, warranted product.  It can only be used when the quantity is 1.	Y	Serial No	\N	\N	\N	\N	Serial No	2016-04-11 16:36:06	0
1027	M_Inventory_ID	0	0	13	\N	1999-12-19 20:40:45	0	Parameters for a Physical Inventory	Material Management	22	The Physical Inventory indicates a unique parameters for a physical inventory.	Y	Phys.Inventory	\N	\N	\N	\N	Phys.Inventory	2000-01-02 00:00:00	0
1029	M_LocatorTo_ID	0	0	31	191	1999-12-19 20:40:45	0	Location inventory is moved to	Material Management	22	The Locator To indicates the location where the inventory is being moved to.	Y	Locator To	\N	\N	\N	\N	Locator To	2000-01-02 00:00:00	0
1030	M_Movement_ID	0	0	13	\N	1999-12-19 20:40:45	0	Movement of Inventory	Material Management	22	The Inventory Movement uniquely identifies a group of movement lines.	Y	Inventory Move	\N	\N	\N	\N	Move	2000-01-02 00:00:00	0
1032	M_Production_ID	0	0	13	\N	1999-12-19 20:40:45	0	Plan for producing a product	Material Management	22	The Production uniquely identifies a Production Plan	Y	Production	\N	\N	\N	\N	Production	2000-01-02 00:00:00	0
1048	QtyBook	0	0	29	\N	1999-12-19 20:40:45	0	Book Quantity	Material Management	22	The Quantity Book indicates the line count stored in the system for a product in inventory	Y	Quantity book	\N	\N	\N	\N	Qty book	2000-01-02 00:00:00	0
1049	QtyCount	0	0	29	\N	1999-12-19 20:40:45	0	Counted Quantity	Material Management	22	The Quantity Count indicates the actual inventory count taken for a product in inventory	Y	Quantity count	\N	\N	\N	\N	Qty count	2000-01-02 00:00:00	0
1096	GenerateList	0	0	28	\N	2000-01-24 18:04:25	0	Generate List	Material Management	1	\N	Y	Generate List	\N	\N	\N	\N	Generate List	2000-01-02 00:00:00	0
1138	UpdateQty	0	0	28	\N	2000-01-24 18:04:26	0	\N	Material Management	1	\N	Y	Update Quantities	\N	\N	\N	\N	Update Quantities	2000-01-02 00:00:00	0
1257	QtyToOrder	0	0	29	\N	2000-05-11 18:31:14	0	\N	Material Management	22	\N	Y	Quantity to Order	\N	\N	\N	\N	Qty to Order	2016-04-11 16:46:54	0
1324	DateReceived	0	0	15	\N	2000-10-11 21:50:04	0	Date a product was received	Material Management	7	The Date Received indicates the date that product was received.	Y	Date received	\N	\N	\N	\N	Date received	2000-01-02 00:00:00	0
1341	IsCreated	0	0	17	319	2000-10-15 11:57:59	0	\N	Material Management	1	\N	Y	Records created	\N	\N	\N	\N	Records created	2000-01-02 00:00:00	0
1342	M_ProductionPlan_ID	0	0	19	\N	2000-10-15 11:57:59	0	Plan for how a product is produced	Material Management	22	The Production Plan identifies the items and steps in generating a product.	Y	Production Plan	\N	\N	\N	\N	Production Plan	2000-01-02 00:00:00	0
1343	ProductionQty	0	0	29	\N	2000-10-15 11:57:59	0	Quantity of products to produce	Material Management	22	The Production Quantity identifies the number of products to produce	Y	Production Quantity	\N	\N	\N	\N	Production Qty	2000-01-02 00:00:00	0
1676	QtyRange	0	0	\N	\N	2001-11-18 16:27:47	0	\N	Material Management	\N	\N	Y	Inventory Quantity	\N	\N	\N	\N	Inventory Quantity	2000-01-02 00:00:00	0
1726	CostStandardAmt	0	0	37	\N	2002-01-17 21:11:55	0	Value in Standard Costs	Material Management	22	\N	Y	Standard Cost Value	\N	\N	\N	\N	Standard Cost Value	2000-01-02 00:00:00	0
1727	DateValue	0	0	15	\N	2002-01-17 21:11:55	0	Date of valuation	Material Management	7	\N	Y	Valuation Date	\N	\N	\N	\N	Valuation Date	2000-01-02 00:00:00	0
1728	PriceLimitAmt	0	0	37	\N	2002-01-17 21:11:55	0	Value with limit price	Material Management	22	\N	Y	Limit price Value	\N	\N	\N	\N	Limit price Value	2000-01-02 00:00:00	0
1729	PriceListAmt	0	0	37	\N	2002-01-17 21:11:55	0	Valuation with List Price	Material Management	22	\N	Y	List price Value	\N	\N	\N	\N	List price Value	2000-01-02 00:00:00	0
1730	PricePOAmt	0	0	37	\N	2002-01-17 21:11:55	0	Valuation with PO Price	Material Management	22	\N	Y	PO Price Value	\N	\N	\N	\N	PO Price Value	2000-01-02 00:00:00	0
1731	PriceStdAmt	0	0	37	\N	2002-01-17 21:11:55	0	Valuation with standard price	Material Management	22	\N	Y	Std Price Value	\N	\N	\N	\N	Std Price Value	2000-01-02 00:00:00	0
1936	GuaranteeDate	0	0	15	\N	2003-01-23 00:10:32	0	Date when guarantee expires	Material Management	7	Date when the normal guarantee or availability expires	Y	Guarantee Date	\N	\N	\N	\N	Guarantee date	2016-04-11 16:36:03	0
2021	M_Lot_ID	0	0	13	\N	2003-05-05 20:33:24	0	Product Lot Definition	Material Management	22	The individual Lot of a Product	Y	Lot	\N	\N	\N	\N	Lot	2000-01-02 00:00:00	0
2109	LocatorValue	0	0	10	\N	2003-06-07 19:49:51	0	Key of the Warehouse Locator	Material Management	40	\N	Y	Locator Key	\N	\N	\N	\N	Locator Key	2000-01-02 00:00:00	0
2202	InventoryType	0	0	17	292	2003-10-07 15:10:01	0	Type of inventory difference	Material Management	1	The type of inventory difference determines which account is used. The default is the Inventory Difference account defined for the warehouse.  Alternatively, you could select any charge.  This allows you to account for Internal Use or extraordinary inventory losses.	Y	Inventory Type	\N	\N	\N	\N	Inventory Type	2000-01-02 00:00:00	0
2237	GoodForDays	0	0	11	\N	2003-12-04 23:32:04	0	Shelf Life Days remaining to Guarantee Date (minus minimum guarantee days)	Material Management	22	Shelf Life of products with Guarantee Date instance compared to today minus the minimum guaranteed days.\n(Guarantee Date-Today) – Min Guarantee Days	Y	Good for Days	\N	\N	\N	\N	Good Days	2010-01-13 10:38:15	0
2238	QtyAvailable	0	0	29	\N	2003-12-04 23:32:04	0	Available Quantity (On Hand - Reserved)	Material Management	22	Quantity available to promise = On Hand minus Reserved Quantity	Y	Available Quantity	\N	\N	\N	\N	Qty Available	2000-01-02 00:00:00	0
2239	ShelfLifeRemainingPct	0	0	11	\N	2003-12-04 23:32:04	0	Remaining shelf life in percent based on Guarantee Date	Material Management	22	(Guarantee Date-Today) / Guarantee Days	Y	Remaining Shelf Life %	\N	\N	\N	\N	Rem Shelf Life %	2000-01-02 00:00:00	0
2265	ShelfLifeDays	0	0	11	\N	2003-12-07 16:21:46	0	Shelf Life in days based on Product Instance Guarantee Date	Material Management	22	Shelf Life of products with Guarantee Date instance compared to today.	Y	Shelf Life Days	\N	\N	\N	\N	Shelf Life Days	2000-01-02 00:00:00	0
2386	ConfirmedQty	0	0	29	\N	2004-02-19 10:36:37	0	Confirmation of a received quantity	Material Management	22	Confirmation of a received quantity	Y	Confirmed Quantity	\N	\N	\N	\N	Confirmed Qty	2000-01-02 00:00:00	0
2422	PickedQty	0	0	29	\N	2004-02-19 10:36:37	0	\N	Material Management	22	\N	Y	Picked Qty	\N	\N	\N	\N	Picked Qty	2016-04-11 16:07:52	0
2435	ScrappedQty	0	0	29	\N	2004-02-19 10:36:37	0	The Quantity scrapped due to QA issues	Material Management	22	\N	Y	Scrapped Quantity	\N	\N	\N	\N	Scrapped Qty	2000-01-02 00:00:00	0
2436	TargetQty	0	0	29	\N	2004-02-19 10:36:37	0	Target Movement Quantity	Material Management	22	The Quantity which should have been received	Y	Target Quantity	\N	\N	\N	\N	Target Qty	2000-01-02 00:00:00	0
2453	M_RequisitionLine_ID	0	0	13	\N	2004-03-11 23:54:41	0	Material Requisition Line	Material Management	22	\N	Y	Requisition Line	\N	\N	\N	\N	Requisition Line	2000-01-02 00:00:00	0
2495	M_Demand_ID	0	0	19	\N	2004-04-17 11:16:43	0	Material Demand	Material Management	22	Material Demand can be based on Forecast, Requisitions, Open Orders	Y	Demand	\N	\N	\N	\N	Demand	2000-01-02 00:00:00	0
2496	M_DemandDetail_ID	0	0	13	\N	2004-04-17 11:16:43	0	Material Demand Line Source Detail	Material Management	22	Source Link for Material Demand Lines	Y	Demand Detail	\N	\N	\N	\N	Demand Detail	2000-01-02 00:00:00	0
2497	M_DemandLine_ID	0	0	13	\N	2004-04-17 11:16:43	0	Material Demand Line	Material Management	22	Demand for a product in a period	Y	Demand Line	\N	\N	\N	\N	Demand Line	2000-01-02 00:00:00	0
2499	M_ForecastLine_ID	0	0	30	\N	2004-04-17 11:16:43	0	Forecast Line	Material Management	22	Forecast of Product Qyantity by Period	Y	Forecast Line	\N	\N	\N	\N	Forecast Line	2010-02-15 13:06:23	0
2500	QtyCalculated	0	0	29	\N	2004-04-17 12:04:47	0	Calculated Quantity	Material Management	22	\N	Y	Calculated Quantity	\N	\N	\N	\N	Calculated Qty	2008-06-25 22:51:20	0
2519	ConfirmType	0	0	17	320	2004-05-10 17:22:03	0	Type of confirmation	Material Management	2	\N	Y	Confirmation Type	\N	\N	\N	\N	Confirm Type	2000-01-02 00:00:00	0
2523	M_InOutConfirm_ID	0	0	13	\N	2004-05-10 17:22:03	0	Material Shipment or Receipt Confirmation	Material Management	22	Confirmation of Shipment or Receipt - Created from the Shipment/Receipt	Y	Ship/Receipt Confirmation	\N	\N	\N	\N	Ship/Receipt Confirm	2000-01-02 00:00:00	0
2524	M_InOutLineConfirm_ID	0	0	13	\N	2004-05-10 17:22:03	0	Material Shipment or Receipt Confirmation Line	Material Management	22	Confirmation details	Y	Ship/Receipt Confirmation Line	\N	\N	\N	\N	Ship/Receipt Confirm Line	2000-01-02 00:00:00	0
2525	CreatePackage	0	0	28	\N	2004-05-12 11:19:18	0	\N	Material Management	1	\N	Y	Create Package	\N	\N	\N	\N	Create Package	2000-01-02 00:00:00	0
2526	DifferenceQty	0	0	29	\N	2004-05-12 11:19:18	0	Difference Quantity	Material Management	22	\N	Y	Difference	\N	\N	\N	\N	Difference	2000-01-02 00:00:00	0
2531	IsCancelled	0	0	20	\N	2004-05-17 22:51:57	0	The transaction was cancelled	Material Management	1	\N	Y	Cancelled	\N	\N	\N	\N	Cancelled	2000-01-02 00:00:00	0
2533	ApprovalAmt	0	0	12	\N	2004-06-10 18:22:14	0	Document Approval Amount	Material Management	22	Approval Amount for Workflow	Y	Approval Amount	\N	\N	\N	\N	Approval Amt	2000-01-02 00:00:00	0
2543	IsInDispute	0	0	20	\N	2004-06-14 22:05:00	0	Document is in dispute	Material Management	1	The document is in dispute. Use Requests to track details.	Y	In Dispute	\N	\N	\N	\N	In Dispute	2000-01-02 00:00:00	0
2560	ConfirmationNo	0	0	10	\N	2004-07-02 14:15:14	0	Confirmation Number	Material Management	20	\N	Y	Confirmation No	\N	\N	\N	\N	Confirmation No	2000-01-02 00:00:00	0
2627	ReplenishmentCreate	0	0	17	329	2004-08-25 22:54:39	0	Create from Replenishment	Material Management	1	\N	Y	Create	\N	\N	\N	\N	Create	2000-01-02 00:00:00	0
2654	QtyInternalUse	0	0	29	\N	2004-11-26 21:01:11	0	Internal Use Quantity removed from Inventory	Material Management	22	Quantity of product inventory used internally (positive if taken out - negative if returned)	Y	Internal Use Qty	\N	\N	\N	\N	Internal Use	2000-01-02 00:00:00	0
2658	M_Product_To_ID	0	0	\N	\N	2004-11-27 23:42:43	0	Product to be converted to (must have UOM Conversion defined to From Product)	Material Management	\N	\N	Y	To Product	\N	\N	\N	\N	To Product	2000-01-02 00:00:00	0
2687	Search_Invoice_ID	0	0	30	336	2005-04-01 15:51:08	100	Search Invoice Identifier	Material Management	22	The Invoice Document.	Y	Search Invoice	\N	\N	\N	\N	Search Invoice	2005-04-01 15:51:08	100
2688	Search_Order_ID	0	0	30	290	2005-04-01 15:52:48	100	Order Identifier	Material Management	22	Order is a control document.	Y	Search Order	\N	\N	\N	\N	Search Order	2005-04-01 15:52:48	100
2689	Search_InOut_ID	0	0	30	295	2005-04-01 16:00:21	100	Material Shipment Document	Material Management	22	The Material Shipment / Receipt 	Y	Search Shipment/Receipt	\N	\N	\N	\N	Search Shipment/Receipt	2005-04-01 16:00:21	100
2799	M_AttributeSetInstanceTo_ID	0	0	35	\N	2005-05-29 00:49:29	100	Target Product Attribute Set Instance	Material Management	10	\N	Y	Attribute Set Instance To	\N	\N	\N	\N	Attribute Set Instance To	2005-05-29 00:52:45	100
2962	CostAmt	0	0	12	\N	2006-01-09 17:54:23	100	Value with Cost	Material Management	22	\N	Y	Cost Value	\N	\N	\N	\N	Cost Value	2016-04-11 16:31:56	0
2968	InventoryCountSet	0	0	\N	\N	2006-01-19 13:10:47	100	Set the value of the inventory count to Zero or On Hand Quantity	Material Management	\N	\N	Y	Set Inventory Count to	\N	\N	\N	\N	Set Inventory Count to	2006-01-19 13:15:02	100
52053	QtyCsv	0	0	29	\N	2008-12-21 03:58:43.440582	100	\N	Material Management	131089	\N	Y	QtyCsv	\N	\N	\N	\N	QtyCsv	2008-12-21 03:58:43.440582	100
53457	Reversal_ID	0	0	18	337	2008-04-22 17:27:18	0	ID of document reversal	Material Management	22	\N	Y	Reversal ID	\N	\N	\N	\N	Reversal ID	2008-04-22 17:27:18	0
53689	ReversalLine_ID	0	0	18	295	2008-09-28 14:39:22	0	Use to keep the reversal line ID for reversing costing purpose	Material Management	22	\N	Y	Reversal Line	\N	\N	\N	\N	Reversal Line	2008-09-28 14:39:22	0
54087	SumQtyOnHand	0	0	29	\N	2009-12-01 16:57:45	100	Summary of product on hand in all locators	Material Management	131089	\N	Y	Sum Qty on Hand	\N	\N	\N	\N	Sum Qty on Hand	2009-12-01 16:59:05	100
55236	QtyUsed	0	0	12	\N	2011-07-27 15:15:22	100	\N	Material Management	10	\N	Y	Quantity Used	\N	\N	\N	\N	Quantity Used in production	2011-07-27 15:15:22	100
55237	IsEndProduct	0	0	20	\N	2011-07-27 15:15:27	100	End Product of production	Material Management	1	\N	Y	End Product	\N	\N	\N	\N	End Product	2011-07-27 15:15:27	100
55238	M_QualityTestResult_ID	0	0	13	\N	2011-07-27 15:15:57	100	\N	Material Management	22	\N	Y	Quality Test Result	\N	\N	\N	\N	Quality Test Result	2011-07-27 15:15:57	100
55239	M_QualityTest_ID	0	0	19	\N	2011-07-27 15:16:20	100	\N	Material Management	10	\N	Y	Quality Test	\N	\N	\N	\N	Quality Test	2011-07-27 15:16:20	100
55240	IsQCPass	0	0	20	\N	2011-07-27 15:16:23	100	\N	Material Management	1	\N	Y	QC Pass	\N	\N	\N	\N	QC Pass	2011-07-27 15:16:23	100
55241	ExpectedResult	0	0	10	\N	2011-07-27 15:16:28	100	\N	Material Management	22	\N	Y	Expected Result	\N	\N	\N	\N	Expected Result	2011-07-27 15:16:28	100
56351	FirstCountQty	0	0	29	\N	2013-09-17 08:39:05	100	\N	Material Management	22	\N	Y	First Count Qty	\N	\N	\N	\N	1st Count	2013-09-17 08:39:05	100
56352	SecondCountQty	0	0	29	\N	2013-09-17 08:40:00	100	\N	Material Management	22	\N	Y	Second Count Qty	\N	\N	\N	\N	2nd Count	2013-09-17 08:40:00	100
56353	CountEntered	0	0	20	\N	2013-09-17 08:41:33	100	\N	Material Management	1	\N	Y	Count Entered	\N	\N	\N	\N	Entered	2013-09-17 08:41:33	100
56778	IsStocktake	0	0	20	\N	2013-12-08 18:33:43	100	\N	Material Management	1	Enable stocktake enhancements, including facility to record first and second counts.	Y	Stocktake	\N	\N	\N	\N	Stocktake	2013-12-08 18:33:43	100
56782	UpdateQtyCount	0	0	28	\N	2013-12-09 10:15:06	100	\N	Material Management	1	\N	Y	Update Quantity Count	\N	\N	\N	\N	UpdateQtyCount	2013-12-09 10:15:06	100
57919	Apr	0	0	\N	\N	2015-03-24 15:46:02	100	\N	Material Management	\N	\N	Y	April	\N	\N	\N	\N	April	2016-04-11 16:47:17	0
57920	Aug	0	0	\N	\N	2015-03-24 15:46:03	100	\N	Material Management	\N	\N	Y	August	\N	\N	\N	\N	August	2016-04-11 16:47:17	0
57921	Dec	0	0	\N	\N	2015-03-24 15:46:05	100	\N	Material Management	\N	\N	Y	December	\N	\N	\N	\N	December	2016-04-11 16:47:18	0
57922	Feb	0	0	\N	\N	2015-03-24 15:46:06	100	\N	Material Management	\N	\N	Y	February	\N	\N	\N	\N	February	2016-04-11 16:47:18	0
57923	Jan	0	0	\N	\N	2015-03-24 15:46:08	100	\N	Material Management	\N	\N	Y	January	\N	\N	\N	\N	January	2016-04-11 16:47:18	0
57924	Jul	0	0	\N	\N	2015-03-24 15:46:09	100	\N	Material Management	\N	\N	Y	July	\N	\N	\N	\N	July	2016-04-11 16:47:19	0
57925	Jun	0	0	\N	\N	2015-03-24 15:46:10	100	\N	Material Management	\N	\N	Y	June	\N	\N	\N	\N	June	2016-04-11 16:47:19	0
57926	Mar	0	0	\N	\N	2015-03-24 15:46:11	100	\N	Material Management	\N	\N	Y	March	\N	\N	\N	\N	March	2016-04-11 16:47:20	0
57927	May	0	0	\N	\N	2015-03-24 15:46:12	100	\N	Material Management	\N	\N	Y	May	\N	\N	\N	\N	May	2016-04-11 16:47:20	0
57928	Nov	0	0	\N	\N	2015-03-24 15:46:13	100	\N	Material Management	\N	\N	Y	November	\N	\N	\N	\N	November	2016-04-11 16:47:20	0
57929	Oct	0	0	\N	\N	2015-03-24 15:46:14	100	\N	Material Management	\N	\N	Y	October	\N	\N	\N	\N	October	2016-04-11 16:47:21	0
57930	Sep	0	0	\N	\N	2015-03-24 15:46:14	100	\N	Material Management	\N	\N	Y	September	\N	\N	\N	\N	September	2016-04-11 16:47:21	0
58026	IsWIP	0	0	20	\N	2015-07-12 11:35:32	100	\N	Material Management	1	\N	Y	Work In Progress	\N	\N	\N	\N	WIP	2015-07-12 11:35:32	100
58354	M_Production_Batch_ID	0	0	\N	\N	2015-11-20 15:21:32	100	\N	Material Management	\N	\N	Y	Production Batch	\N	\N	\N	\N	Production Batch	2015-11-20 15:21:32	100
58355	CountOrder	0	0	\N	\N	2015-11-20 15:23:25	100	\N	Material Management	\N	\N	Y	Order Count	\N	\N	\N	\N	Order Count	2016-04-11 16:54:41	0
58356	QtyCompleted	0	0	\N	\N	2015-11-20 15:23:27	100	\N	Material Management	\N	\N	Y	Qty Completed	\N	\N	\N	\N	Qty Completed	2016-04-11 16:54:41	0
58413	IsCreateMove	0	0	20	\N	2016-04-20 16:09:00	100	\N	Material Management	1	\N	Y	Create Move	\N	\N	\N	\N	Create Move	2016-04-20 16:09:00	100
58925	CurrentCostValue	0	0	12	\N	2016-05-25 14:09:11	100	\N	Material Management	\N	\N	Y	Current Cost Value	\N	\N	\N	\N	Current Cost Value	2016-07-01 00:52:03	0
58926	FutureCostValue	0	0	12	\N	2016-05-25 14:09:13	100	\N	Material Management	\N	\N	Y	Future Cost Value	\N	\N	\N	\N	Future Cost Value	2016-07-01 00:52:03	0
58927	AmountDiff	0	0	12	\N	2016-05-25 14:09:15	100	\N	Material Management	\N	\N	Y	Amount Diff	\N	\N	\N	\N	Amount Diff	2016-07-01 00:52:03	0
59024	isGroupByLocator	0	0	20	\N	2016-10-31 11:41:52	100	\N	Material Management	1	\N	Y	Group By Locator	\N	\N	\N	\N	Group By Locator	2016-10-31 11:41:52	100
59043	IsUseThisTime	0	0	20	\N	2016-11-07 17:36:40	100	Preserve this item.  Exclude from delete Process.	Material Management	1	Flag a line to be excluded in the delete process.	Y	Use This Time	\N	\N	\N	\N	Use This Time	2016-11-07 17:36:40	100
\.
