copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
52385	mrp_run_clientorg_check	0	0	53974	2022-11-02 05:57:56.041854	100	Manufacturing Processes	\N	Y	UNIQUE (ad_client_id, ad_org_id)	CU	\N	2022-11-02 05:57:56.041854	100
52386	mrp_run_key	0	0	53974	2022-11-02 05:57:56.041854	100	Manufacturing Processes	\N	Y	PRIMARY KEY (mrp_run_id)	CP	\N	2022-11-02 05:57:56.041854	100
52387	mrp_runline_key	0	0	53975	2022-11-02 05:57:56.041854	100	Manufacturing Processes	\N	Y	PRIMARY KEY (mrp_runline_id)	CP	\N	2022-11-02 05:57:56.041854	100
52388	mrp_run_mrp_runline	0	0	53975	2022-11-02 05:57:56.041854	100	Manufacturing Processes	\N	Y	FOREIGN KEY (mrp_run_id) REFERENCES mrp_run(mrp_run_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
54066	mrp_run_isdeleteplannedpo_check	0	0	53974	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((isdeleteplannedpo = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54067	mrp_run_isdeleteunconfirmedproduction_check	0	0	53974	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((isdeleteunconfirmedproduction = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54068	mrp_run_isexcludekanbanproduct_check	0	0	53974	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((isexcludekanbanproduct = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54069	mrp_run_isactive_check	0	0	53974	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54070	mrp_run_isprocessing_check	0	0	53974	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((isprocessing = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54071	mrp_runline_isactive_check	0	0	53975	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54072	mrp_runline_hassupplydemand_check	0	0	53975	2022-11-02 12:40:39.392787	100	Manufacturing Processes	\N	Y	CHECK ((hassupplydemand = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
