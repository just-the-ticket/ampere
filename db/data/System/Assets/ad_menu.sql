copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
342	Asset	W	0	\N	\N	0	\N	251	2003-01-23 00:51:59	0	Asset used internally or by customers	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
343	Asset Group	W	0	\N	\N	0	\N	252	2003-01-23 01:02:42	0	Group of Assets	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
345	Assets	\N	0	\N	\N	0	\N	\N	2003-01-23 01:48:52	0	\N	Assets	Y	N	N	N	Y	2008-05-30 17:40:09	100
348	Deliver Assets	P	0	\N	\N	0	201	\N	2003-01-29 21:28:40	0	Deliver Customer Assets electronically	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
399	Customer Assets	R	0	\N	\N	0	222	\N	2003-08-28 17:03:25	0	Report Customer Assets with Delivery Count	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
400	Asset Delivery Details	R	0	\N	\N	0	223	\N	2003-08-28 17:03:52	0	Report Asset Deliveries Details	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
430	Registration Attributes	W	0	\N	\N	0	\N	300	2004-01-08 21:02:47	0	Asset Registration Attributes	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
431	Registration	W	0	\N	\N	0	\N	301	2004-01-08 21:10:19	0	User Asset Registration	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
474	Asset Delivery Month	R	0	\N	\N	0	274	\N	2004-04-13 11:23:30	0	Report Asset Deliveries Summary per month	Assets	N	Y	N	Y	N	2000-01-02 00:00:00	0
53133	Customer Service	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:09	100	Customer Related Assets	Assets	Y	N	N	N	Y	2008-05-30 17:40:09	100
53134	Fixed Assets	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:14	100	Applications to setup and maintain fixed assets	Assets	Y	N	N	N	Y	2008-05-30 17:40:14	100
53135	Processing	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:15	100	Process Fixed Assets	Assets	Y	N	N	N	Y	2008-05-30 17:40:15	100
53136	Asset Revaluation	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:15	100	Process Asset Revaluations	Assets	Y	N	N	N	Y	2008-05-30 17:40:15	100
53137	Asset Revaluation Processing	W	0	\N	\N	0	\N	53044	2008-05-30 17:40:16	100	Process Revaluation of Assets	Assets	N	Y	N	N	N	2008-05-30 17:40:16	100
53138	Asset Revaluation Index	W	0	\N	\N	0	\N	53045	2008-05-30 17:40:16	100	Set the Revaluate Assets Index or Factors	Assets	N	Y	N	N	N	2008-05-30 17:40:16	100
53139	Splits Transfers and Disposals	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:17	100	Process Assets Splits Transfers and Disposals	Assets	Y	N	N	N	Y	2008-05-30 17:40:17	100
53140	Transfer Asset Entry	W	0	\N	\N	0	\N	53046	2008-05-30 17:40:18	100	Create Transfer Asset Entry	Assets	N	Y	N	N	N	2008-05-30 17:40:18	100
53141	Disposed Asset Entry	W	0	\N	\N	0	\N	53047	2008-05-30 17:40:18	100	Create Disposed Asset Entry	Assets	N	Y	N	N	N	2008-05-30 17:40:18	100
53142	Asset Disposal Expense Entry Rpt	R	0	\N	\N	0	53097	\N	2008-05-30 17:40:19	100	Used to review Assets Disposed Entry	Assets	N	Y	N	N	N	2008-05-30 17:40:19	100
53143	Asset Transfer Entry	R	0	\N	\N	0	53099	\N	2008-05-30 17:40:19	100	Used to review Assets Transfered Entry 	Assets	N	Y	N	N	N	2008-05-30 17:40:19	100
53144	Asset Split	W	0	\N	\N	0	\N	53048	2008-05-30 17:40:20	100	Split Assets Process	Assets	N	Y	N	N	N	2008-05-30 17:40:20	100
53145	Asset Disposal	W	0	\N	\N	0	\N	53049	2008-05-30 17:40:20	100	Dispose of Assets	Assets	N	Y	N	N	N	2008-05-30 17:40:20	100
53146	Asset Transfers	W	0	\N	\N	0	\N	53050	2008-05-30 17:40:21	100	Process transfers of assets	Assets	N	Y	N	N	N	2008-05-30 17:40:21	100
53147	Split Asset Entry	W	0	\N	\N	0	\N	53051	2008-05-30 17:40:22	100	Create Split Asset Entries	Assets	N	Y	N	N	N	2008-05-30 17:40:22	100
53148	Asset Split Entry	R	0	\N	\N	0	53107	\N	2008-05-30 17:40:23	100	Used to review Assets Split Entry 	Assets	N	Y	N	N	N	2008-05-30 17:40:23	100
53149	Depreciation Processing	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:24	100	Applications to Process Fixed Assets to the GL	Assets	Y	N	N	N	Y	2008-05-30 17:40:24	100
53150	Depreciation Expense Entry	R	0	\N	\N	0	53109	\N	2008-05-30 17:40:24	100	Used to review Depreciation Expense Entry not yet booked	Assets	N	Y	N	N	N	2008-05-30 17:40:24	100
53151	Build Depreciation Workfile	W	0	\N	\N	0	\N	53052	2008-05-30 17:40:25	100	Build Depreciation Expense File	Assets	N	Y	N	N	N	2008-05-30 17:40:25	100
53152	Post Depreciation Entry	W	0	\N	\N	0	\N	53053	2008-05-30 17:40:26	100	Create Depreciation Entry	Assets	N	Y	N	N	N	2008-05-30 17:40:26	100
53153	Asset Depreciation Forecast	R	0	\N	\N	0	53113	\N	2008-05-30 17:40:26	100	Used to review Assets Forecast	Assets	N	Y	N	N	N	2008-05-30 17:40:26	100
53154	Build Depreciation Forecast	W	0	\N	\N	0	\N	53054	2008-05-30 17:40:27	100	\N	Assets	N	Y	N	N	N	2008-05-30 17:40:27	100
53155	Reporting	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:28	100	Reporting for fixed assets	Assets	Y	N	N	N	Y	2008-05-30 17:40:28	100
53156	RV_Asset_Parent_Report	R	0	\N	\N	0	53115	\N	2008-05-30 17:40:28	100	\N	Assets	N	Y	N	N	N	2008-05-30 17:40:28	100
53157	RV_Depreciation_Table	R	0	\N	\N	0	53117	\N	2008-05-30 17:40:30	100	\N	Assets	N	Y	N	N	N	2008-05-30 17:40:30	100
53158	Spread Report	R	0	\N	\N	0	53119	\N	2008-05-30 17:40:30	100	\N	Assets	N	Y	N	N	N	2008-05-30 17:40:30	100
53159	RV_Asset_Depreciation_Method	R	0	\N	\N	0	53121	\N	2008-05-30 17:40:31	100	List Depreciation Methods	Assets	N	Y	N	N	N	2008-05-30 17:40:31	100
53160	RV_Asset_Convention_Rpt	R	0	\N	\N	0	53125	\N	2008-05-30 17:40:31	100	Asset Convention Report	Assets	N	Y	N	N	N	2008-05-30 17:40:31	100
53161	RV_Depreciation_Calculation_Methods	R	0	\N	\N	0	53129	\N	2008-05-30 17:40:32	100	List Depreciation Calculation Methods Available	Assets	N	Y	N	N	N	2008-05-30 17:40:32	100
53162	RV_Asset_Group_Defaults	R	0	\N	\N	0	53131	\N	2008-05-30 17:40:33	100	Lists Asset Group Settings & Defaults	Assets	N	Y	N	N	N	2008-05-30 17:40:33	100
53163	Setup and Maintain	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:33	100	Setup and maintain assets	Assets	Y	N	N	N	Y	2008-05-30 17:40:33	100
53164	Fixed Assets Setup	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:34	100	Setup and Maintain Fixed Assets	Assets	Y	N	N	N	Y	2008-05-30 17:40:34	100
53165	Inbound Charges for GL	P	0	\N	\N	0	53133	\N	2008-05-30 17:40:37	100	Process invoice charges from GL	Assets	N	Y	N	N	N	2008-05-30 17:40:37	100
53166	Inbound Asset Entry	W	0	\N	\N	0	\N	53055	2008-05-30 17:40:37	100	Create Inbound Asset Entry	Assets	N	Y	N	N	N	2008-05-30 17:40:37	100
53167	Inbound Charge Entry Report	R	0	\N	\N	0	53135	\N	2008-05-30 17:40:38	100	Used to review Assets Inbound Entry 	Assets	N	Y	N	N	N	2008-05-30 17:40:38	100
53168	Inbound Charges for AP	P	0	\N	\N	0	53137	\N	2008-05-30 17:40:38	100	Process invoice charges from AP	Assets	N	Y	N	N	N	2008-05-30 17:40:38	100
53169	Import File Loader	X	0	101	\N	0	\N	\N	2008-05-30 17:40:39	100	Load flat Files into import tables	Assets	N	Y	N	N	N	2011-11-02 16:35:05	0
53170	Post Imported Assets	W	0	\N	\N	0	\N	53056	2008-05-30 17:40:40	100	Import Fixed Assets	Assets	N	Y	N	N	N	2008-05-30 17:40:40	100
53171	Depreciation Setup	\N	0	\N	\N	0	\N	\N	2008-05-30 17:40:40	100	Applications to setup and maintain depreciation	Assets	Y	N	N	N	Y	2008-05-30 17:40:40	100
53172	Depreciation Period Spread Type	W	0	\N	\N	0	\N	53057	2008-05-30 17:40:41	100	Period Spread Type	Assets	N	Y	N	N	N	2008-05-30 17:40:41	100
53173	Depreciation Methods	W	0	\N	\N	0	\N	53058	2008-05-30 17:40:42	100	Depreciation Methods	Assets	N	Y	N	N	N	2008-05-30 17:40:42	100
53174	Depreciation Tables	W	0	\N	\N	0	\N	53059	2008-05-30 17:40:43	100	Allows users to create multiple depreciation schedules	Assets	N	Y	N	N	N	2008-05-30 17:40:43	100
53175	Depreciation First Year Conventions 	W	0	\N	\N	0	\N	53060	2008-05-30 17:40:43	100	Setup for depreciation Setups	Assets	N	Y	N	N	N	2008-05-30 17:40:43	100
53176	Depreciation Calculation Method	W	0	\N	\N	0	\N	53061	2008-05-30 17:40:44	100	Define Calculation Methods used in depreciation expense calculation	Assets	N	Y	N	N	N	2008-05-30 17:40:44	100
\.
