copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
173	BillTo_ID	0	0	18	159	1999-11-19 10:07:43	0	Bill to Address	Data Import	22	The Bill/Invoice To indicates the address to use when remitting bills	Y	Invoice To	Bill From Address	The Bill/Invoice From indicated the address where the invoice is created by the vendor	Invoice From	Invoice From	Invoice To	2000-01-02 00:00:00	0
1312	AD_ImpFormat_Row_ID	0	0	13	\N	2000-09-15 14:49:49	0	\N	Data Import	22	\N	Y	Format Field	\N	\N	\N	\N	Format Field	2000-01-02 00:00:00	0
1314	DataFormat	0	0	10	\N	2000-09-15 14:49:49	0	Format String in Java Notation, e.g. ddMMyy	Data Import	20	The Date Format indicates how dates are defined on the record to be imported.  It must be in Java Notation	Y	Data Format	\N	\N	\N	\N	Data Format	2000-01-02 00:00:00	0
1316	DecimalPoint	0	0	10	\N	2000-09-15 14:49:49	0	Decimal Point in the data file - if any	Data Import	1	\N	Y	Decimal Point	\N	\N	\N	\N	Decimal Point	2000-01-02 00:00:00	0
1317	DivideBy100	0	0	20	\N	2000-09-15 14:49:49	0	Divide number by 100 to get correct amount	Data Import	1	\N	Y	Divide by 100	\N	\N	\N	\N	Divide by 100	2000-01-02 00:00:00	0
1318	EndNo	0	0	11	\N	2000-09-15 14:49:49	0	\N	Data Import	22	\N	Y	End No	\N	\N	\N	\N	End No	2000-01-02 00:00:00	0
1319	FormatType	0	0	17	209	2000-09-15 14:49:49	0	Format of the data	Data Import	1	The Format is a drop down list box for selecting the format type (text, tab delimited, XML, etc) of the file to be imported	Y	Format	\N	\N	\N	\N	Format	2000-01-02 00:00:00	0
1886	SalesRep_Name	0	0	10	\N	2002-09-11 16:13:37	0	\N	Data Import	60	\N	Y	Sales Representative	\N	\N	Company Agent	Company Agent	Sales Rep	2000-01-02 00:00:00	0
1906	BPartner_Value	0	0	10	\N	2003-01-11 15:04:52	0	The Key of the Business Partner	Data Import	40	\N	Y	Business Partner Key	\N	\N	\N	\N	Partner Key	2010-02-15 13:05:25	0
1910	ElementValue	0	0	10	\N	2003-01-11 15:04:52	0	Key of the element	Data Import	40	\N	Y	Element Key	\N	\N	\N	\N	Element Key	2000-01-02 00:00:00	0
1913	I_Product_ID	0	0	13	\N	2003-01-11 15:04:52	0	Import Item or Service	Data Import	22	\N	Y	Import Product	\N	\N	\N	\N	Import Product	2000-01-02 00:00:00	0
1914	I_ReportLine_ID	0	0	13	\N	2003-01-11 15:04:52	0	Import Report Line Set values	Data Import	22	\N	Y	Import Report Line Set	\N	\N	\N	\N	Import Report Line Set	2000-01-02 00:00:00	0
1916	ProductCategory_Value	0	0	10	\N	2003-01-11 15:04:52	0	\N	Data Import	40	\N	Y	Product Category Key	\N	\N	\N	\N	Product Category Key	2000-01-02 00:00:00	0
1917	ReportLineSetName	0	0	10	\N	2003-01-11 15:04:52	0	Name of the Report Line Set	Data Import	60	\N	Y	Report Line Set Name	\N	\N	\N	\N	Report Line Set Name	2000-01-02 00:00:00	0
2043	I_Inventory_ID	0	0	13	\N	2003-05-28 21:40:00	0	Import Inventory Transactions	Data Import	22	\N	Y	Import Inventory	\N	\N	\N	\N	Import Inventory	2000-01-02 00:00:00	0
2070	WarehouseValue	0	0	10	\N	2003-05-29 21:58:21	0	Key of the Warehouse	Data Import	40	Key to identify the Warehouse	Y	Warehouse Key	\N	\N	\N	\N	Warehouse Key	2010-02-15 13:06:15	0
2084	AcctSchemaName	0	0	10	\N	2003-06-07 19:49:50	0	Name of the Accounting Schema	Data Import	60	\N	Y	Account Schema Name	\N	\N	\N	\N	AcctSchema Name	2000-01-02 00:00:00	0
2091	BankAccountNo	0	0	10	\N	2003-06-07 19:49:50	0	Bank Account Number	Data Import	20	\N	Y	Bank Account No	\N	\N	\N	\N	Bank Account No	2000-01-02 00:00:00	0
2092	BatchDescription	0	0	10	\N	2003-06-07 19:49:50	0	Description of the Batch	Data Import	255	\N	Y	Batch Description	\N	\N	\N	\N	Batch Description	2000-01-02 00:00:00	0
2093	BatchDocumentNo	0	0	10	\N	2003-06-07 19:49:50	0	Document Number of the Batch	Data Import	30	\N	Y	Batch Document No	\N	\N	\N	\N	Batch Document No	2000-01-02 00:00:00	0
2095	CategoryName	0	0	10	\N	2003-06-07 19:49:50	0	Name of the Category	Data Import	60	\N	Y	Category Name	\N	\N	\N	\N	Category Name	2000-01-02 00:00:00	0
2097	ClientValue	0	0	10	\N	2003-06-07 19:49:50	0	Key of the Client	Data Import	40	\N	Y	Client Key	\N	\N	\N	\N	Client Key	2000-01-02 00:00:00	0
2098	DocTypeName	0	0	10	\N	2003-06-07 19:49:50	0	Name of the Document Type	Data Import	60	\N	Y	Document Type Name	\N	\N	\N	\N	DocType Name	2000-01-02 00:00:00	0
2102	I_BankStatement_ID	0	0	13	\N	2003-06-07 19:49:51	0	Import of the Bank Statement	Data Import	22	\N	Y	Import Bank Statement	\N	\N	\N	\N	Import Bank Statement	2000-01-02 00:00:00	0
2103	I_GLJournal_ID	0	0	13	\N	2003-06-07 19:49:51	0	Import General Ledger Journal	Data Import	22	\N	Y	Import GL Journal	\N	\N	\N	\N	Import GL Journal	2000-01-02 00:00:00	0
2104	I_Order_ID	0	0	13	\N	2003-06-07 19:49:51	0	Import Orders	Data Import	22	\N	Y	Import Order	\N	\N	\N	\N	Import Order	2000-01-02 00:00:00	0
2105	I_Payment_ID	0	0	13	\N	2003-06-07 19:49:51	0	Import Payment	Data Import	22	\N	Y	Import Payment	\N	\N	\N	\N	Import Payment	2000-01-02 00:00:00	0
2114	OrgTrxValue	0	0	10	\N	2003-06-07 19:49:51	0	Key of the Transaction Organization	Data Import	40	\N	Y	Trx Org Key	\N	\N	\N	\N	Trx Org Key	2000-01-02 00:00:00	0
2116	PaymentTermValue	0	0	10	\N	2003-06-07 19:49:51	0	Key of the Payment Term	Data Import	40	\N	Y	Payment Term Key	\N	\N	\N	\N	Payment Term Key	2000-01-02 00:00:00	0
2128	I_Invoice_ID	0	0	13	\N	2003-06-07 21:13:27	0	Import Invoice	Data Import	22	\N	Y	Import Invoice	\N	\N	\N	\N	Import Invoice	2000-01-02 00:00:00	0
2169	IsImportOnlyNoErrors	0	0	\N	\N	2003-08-21 10:36:00	0	Only start the import, if there are no validation Errors	Data Import	\N	\N	Y	Import only if No Errors	\N	\N	\N	\N	Import No Errors	2000-01-02 00:00:00	0
2170	AD_OrgDoc_ID	0	0	18	130	2003-08-27 12:12:59	0	Document Organization (independent from account organization)	Data Import	22	\N	Y	Document Org	\N	\N	\N	\N	Document Org	2000-01-02 00:00:00	0
2171	JournalDocumentNo	0	0	10	\N	2003-08-27 12:12:59	0	Document number of the Journal	Data Import	30	\N	Y	Journal Document No	\N	\N	\N	\N	Journal Doc No	2000-01-02 00:00:00	0
2279	ConversionTypeValue	0	0	10	\N	2003-12-21 00:11:12	0	Key value for the Currency Conversion Rate Type	Data Import	40	The date type key for the conversion of foreign currency transactions	Y	Currency Type Key	\N	\N	\N	\N	Currency Conversion Type	2000-01-02 00:00:00	0
2298	PaymentDocumentNo	0	0	10	\N	2003-12-25 14:02:45	0	Document number of the Payment	Data Import	30	\N	Y	Payment Document No	\N	\N	\N	\N	Payment Document No	2000-01-02 00:00:00	0
2302	CreateReciprocalRate	0	0	20	\N	2003-12-29 18:34:49	0	Create Reciprocal Rate from current information	Data Import	1	If selected, the imported USD->EUR rate is used to create/calculate the reciprocal rate EUR->USD.	Y	Create Reciprocal Rate	\N	\N	\N	\N	Create Reciprocal	2000-01-02 00:00:00	0
2303	I_Conversion_Rate_ID	0	0	13	\N	2003-12-29 18:34:49	0	Import Currency Conversion Rate	Data Import	22	\N	Y	Import Conversion Rate	\N	\N	\N	\N	Import Conversion Rate	2000-01-02 00:00:00	0
2304	ISO_Code_To	0	0	10	\N	2003-12-29 18:34:49	0	Three letter ISO 4217 Code of the To Currency	Data Import	3	For details - http://www.unece.org/trade/rec/rec09en.htm	Y	ISO Currency To Code	\N	\N	\N	\N	ISO Currency To	2000-01-02 00:00:00	0
2333	WorkingTime	0	0	11	\N	2004-01-01 23:37:18	0	Workflow Simulation Execution Time	Data Import	22	Amount of time the performer of the activity needs to perform the task in Duration Unit	Y	Working Time	\N	\N	\N	\N	Working Time	2010-02-15 13:06:08	0
2498	M_Forecast_ID	0	0	13	\N	2004-04-17 11:16:43	0	Material Forecast	Data Import	22	Material Forecast	Y	Forecast	\N	\N	\N	\N	Forecast	2010-02-15 13:06:11	0
2561	I_InOutLineConfirm_ID	0	0	13	\N	2004-07-02 14:15:14	0	Material Shipment or Receipt Confirmation Import Line	Data Import	22	Import Confirmation Line Details	Y	Ship/Receipt Confirmation Import Line	\N	\N	\N	\N	Ship/Receipt Confirm Import Line	2000-01-02 00:00:00	0
2851	IsCreateNewBatch	0	0	20	\N	2005-10-08 10:08:52	100	If selected a new batch is created	Data Import	1	Note that the balance check does not check that individual batches are balanced.	Y	Create New Batch	\N	\N	\N	\N	New Batch	2005-10-08 10:10:33	100
2852	IsCreateNewJournal	0	0	20	\N	2005-10-08 10:08:53	100	If selected a new journal within the batch is created	Data Import	1	Note that the balance check does not check that individual journals are balanced.	Y	Create New Journal	\N	\N	\N	\N	New Journal	2005-10-08 10:12:53	100
53222	ActivityValue	0	0	10	\N	2007-09-04 22:54:47	100	\N	Data Import	40	\N	Y	Activity Value	\N	\N	\N	\N	Activity Value	2010-03-29 16:11:46	100
53258	IsCreatePlan	0	0	20	\N	2007-12-17 03:28:36	0	Indicates whether planned orders will be generated by MRP	Data Import	1	Indicates whether planned orders will be generated by MRP, if this flag is not just MRP generate a 'Create' action notice	Y	Create Plan	\N	\N	\N	\N	Create Plan	2010-02-15 13:05:42	0
53261	IsMPS	0	0	20	\N	2007-12-17 03:28:45	0	\N	Data Import	1	\N	Y	Is MPS	\N	\N	\N	\N	Is MPS	2010-02-15 13:05:43	0
53264	Order_Max	0	0	29	\N	2007-12-17 03:28:56	0	Maximum order quantity in UOM	Data Import	10	The Maximum Order Quantity indicates the biggest quantity of this product which can be ordered.	Y	Maximum Order Qty	\N	\N	\N	\N	Maximum Order Qty	2010-02-15 13:05:53	0
53265	Order_Period	0	0	29	\N	2007-12-17 03:29:02	0	\N	Data Import	22	\N	Y	Order Period	\N	\N	\N	\N	Order Period	2010-02-15 13:05:56	0
53266	Order_Policy	0	0	17	53228	2007-12-17 03:29:08	0	\N	Data Import	3	\N	Y	Order Policy	\N	\N	\N	\N	Order Policy	2010-02-15 13:05:57	0
53267	Order_Qty	0	0	29	\N	2007-12-17 03:29:10	0	\N	Data Import	22	\N	Y	Order Qty	\N	\N	\N	\N	Order Qty	2010-02-15 13:05:59	0
53269	Planner_ID	0	0	18	110	2007-12-17 03:29:18	0	\N	Data Import	22	\N	Y	Planner	\N	\N	\N	\N	Planner	2010-02-15 13:06:00	0
53270	TimeFence	0	0	29	\N	2007-12-17 03:29:22	0	\N	Data Import	22	\N	Y	Time Fence	\N	\N	\N	\N	Time Fence	2010-02-15 13:06:05	0
53271	TransfertTime	0	0	29	\N	2007-12-17 03:29:25	0	\N	Data Import	10	\N	Y	Transfert Time	\N	\N	\N	\N	Transfert Time	2010-02-15 13:06:07	0
53272	Yield	0	0	11	\N	2007-12-17 03:29:35	0	The Yield is the percentage of a lot that is expected to be of acceptable wuality may fall below 100 percent	Data Import	22	ADempiere Calculate the total yield for a product from the yield for each activity when the process Workflow Cost Roll-Up is executed.\n\nThe expected yield for an Activity can be expressed as:\n\nYield = Acceptable Units at Activity End x 100\n\nThe Total manufacturing yield for a product is determined by multiplying the yied percentage for each activity.\n\nManufacturing Yield = Yield % for Activity 10 x Yied % for Activity 20 , etc \n\nTake care when setting yield to anything but 100% particularly when yied is used for multiples activities\n\n	Y	Yield %	\N	\N	\N	\N	Yield %	2010-02-15 13:06:09	0
53354	SafetyStock	0	0	29	\N	2008-03-01 22:25:08	0	Safety stock is a term used to describe a level of stock that is maintained below the cycle stock to buffer against stock-outs	Data Import	22	Safety stock is defined as extra units of inventory carried as protection against possible stockouts. It is held when an organization cannot accurately predict demand and/or lead time for the product.\n\nRereference:\nhttp://en.wikipedia.org/wiki/Safety_stock	Y	Safety Stock Qty	\N	\N	\N	\N	Safety Stock Qty	2010-02-15 13:06:02	0
53797	I_PriceList_ID	0	0	13	\N	2009-03-17 23:18:11	100	\N	Data Import	22	\N	Y	Import Price List	\N	\N	\N	\N	Import Price List	2009-03-17 23:18:11	100
54095	IsUpdateCosting	0	0	\N	\N	2009-12-05 16:35:27	100	\N	Data Import	\N	\N	Y	Update Costing	\N	\N	\N	\N	Update Costing	2009-12-05 16:35:27	100
54096	IsImportPriceList	0	0	\N	\N	2009-12-10 12:32:23	100	\N	Data Import	\N	\N	Y	Import List price	\N	\N	\N	\N	Import List price	2009-12-10 12:32:23	100
54097	IsImportPriceStd	0	0	\N	\N	2009-12-10 12:34:14	100	\N	Data Import	\N	\N	Y	Import Standard Price	\N	\N	\N	\N	Import Standard Price	2009-12-10 12:34:14	100
54098	IsImportPriceLimit	0	0	\N	\N	2009-12-10 12:35:33	100	\N	Data Import	\N	\N	Y	Import Limit Price	\N	\N	\N	\N	Import Limit Price	2009-12-10 12:35:33	100
54113	I_HR_Movement_ID	0	0	13	\N	2010-02-08 18:18:58	100	\N	Data Import	10	\N	Y	Payroll Movement Import	\N	\N	\N	\N	Payroll Movement Import	2010-02-08 18:18:58	100
54114	ConceptValue	0	0	10	\N	2010-02-08 18:19:34	100	Value of the Concept	Data Import	40	\N	Y	Concept Value	\N	\N	\N	\N	Concept Value	2010-02-08 18:19:34	100
54115	ProcessName	0	0	10	\N	2010-02-08 18:19:37	100	Name of the Process	Data Import	60	\N	Y	Process Name	\N	\N	\N	\N	Process Name	2010-02-08 18:19:37	100
54117	I_ProductPlanning_ID	0	0	13	\N	2010-02-15 13:05:12	0	\N	Data Import	10	\N	Y	Import Product Planning	\N	\N	\N	\N	Import Product Planning	2010-02-15 13:05:12	0
54118	NetworkDistributionValue	0	0	10	\N	2010-02-15 13:06:17	0	Key of the Network Distribution	Data Import	40	\N	Y	Network Distribution Key	\N	\N	\N	\N	Network Distribution Key	2010-02-15 13:06:17	0
54120	ForecastValue	0	0	10	\N	2010-02-15 13:06:19	0	Key of the Forecast	Data Import	40	\N	Y	Forecast Key	\N	\N	\N	\N	Forecast Key	2010-02-15 13:06:19	0
54121	ResourceValue	0	0	10	\N	2010-02-15 13:06:20	0	Key of the Resource	Data Import	40	\N	Y	Resource Key	\N	\N	\N	\N	Resource Key	2010-02-15 13:06:20	0
54122	PlannerValue	0	0	10	\N	2010-02-15 13:06:22	0	Search Key of the Planning	Data Import	40	\N	Y	Planner Key	\N	\N	\N	\N	Planner Key	2010-02-15 13:06:22	0
54129	C_OrderSourceValue	0	0	10	\N	2010-03-08 16:23:39	100	\N	Data Import	40	\N	Y	Order Source Key	\N	\N	\N	\N	Order Source Key	2010-03-08 16:23:39	100
54158	SeparatorChar	0	0	10	\N	2010-06-03 10:06:56	100	\N	Data Import	1	\N	Y	Separator Character	\N	\N	\N	\N	Separator Character	2010-06-03 10:06:56	100
55261	Operand1Name	0	0	10	\N	2011-08-23 17:37:17	100	Name of element to use for operand 1	Data Import	60	\N	Y	Operand 1 Name	\N	\N	\N	\N	Operand 1 Name	2011-08-23 17:37:17	100
55262	Operand2Name	0	0	10	\N	2011-08-23 17:37:48	100	Name of element to use for operand 2	Data Import	60	\N	Y	Operand 2 Name	\N	\N	\N	\N	Operand 2 Name	2011-08-23 17:37:48	100
55264	Ship_Location_ID	0	0	18	159	2011-08-24 16:47:09	100	Ship To Business Partner Location name	Data Import	10	\N	Y	Ship To BP Location	\N	\N	\N	\N	Ship To BP Location	2011-09-05 15:31:13	100
55273	Ship_C_Location_ID	0	0	21	\N	2011-08-24 16:47:39	100	Address to ship to	Data Import	10	\N	Y	Ship To Address	\N	\N	\N	\N	Ship To Address	2011-09-05 15:31:13	100
55435	AD_Import_ID	0	0	13	\N	2012-02-28 16:28:08	100	\N	Data Import	22	\N	Y	Data Import	\N	\N	\N	\N	Data Import	2012-02-28 16:28:08	100
55436	Source_Table_ID	0	0	18	53290	2012-02-28 16:30:27	100	Source table for importing data from	Data Import	22	The source table is a data staging table that can be populated by the file import loader.	Y	Source Table	\N	\N	\N	\N	Source Table	2012-02-28 16:30:27	100
55437	Target_Table_ID	0	0	18	53290	2012-02-28 16:31:05	100	Target table for importing data to	Data Import	22	The target table is the destination for the imported data	Y	Target Table	\N	\N	\N	\N	Target Table	2012-02-28 16:31:05	100
55438	AD_ImportValidation_ID	0	0	13	\N	2012-02-28 16:38:30	100	\N	Data Import	22	\N	Y	Import Validation	\N	\N	\N	\N	Import Validation	2012-02-28 16:38:30	100
55439	AD_ImportColumn_ID	0	0	13	\N	2012-02-28 17:19:23	100	\N	Data Import	22	\N	Y	Import Column Mapping	\N	\N	\N	\N	Import Column Mapping	2012-02-28 17:19:23	100
55440	Source_Column_ID	0	0	18	3	2012-02-28 17:21:39	100	Source column for import	Data Import	22	\N	Y	Source Column	\N	\N	\N	\N	Source Column	2012-02-28 17:21:39	100
55441	Target_Column_ID	0	0	18	3	2012-02-28 17:22:02	100	Target column for import	Data Import	22	\N	Y	Target Column	\N	\N	\N	\N	Target Column	2012-02-28 17:22:02	100
55447	I_User_ID	0	0	13	\N	2012-03-13 15:33:11	100	\N	Data Import	22	\N	Y	User/Contact import	\N	\N	\N	\N	User/Contact import	2012-03-13 15:33:11	100
56460	AssetValue	0	0	\N	\N	2013-10-11 16:30:42	100	\N	Data Import	\N	\N	Y	Asset Value	\N	\N	\N	\N	Asset Value	2013-10-11 16:30:42	100
56461	BudgetCode	0	0	\N	\N	2013-10-11 16:32:59	100	\N	Data Import	\N	\N	Y	Budget Code 	\N	\N	\N	\N	Budget Code	2013-10-11 16:32:59	100
56462	CampaignValue	0	0	\N	\N	2013-10-11 16:38:42	100	\N	Data Import	\N	\N	Y	Campaign Value	\N	\N	\N	\N	Campaign Value	2013-10-11 16:38:42	100
56463	I_Budget_ID	0	0	\N	\N	2013-10-11 16:41:11	100	\N	Data Import	\N	\N	Y	I_Budget_ID	\N	\N	\N	\N	I_Budget_ID	2013-10-11 16:41:11	100
56464	Month_0_Amt	0	0	\N	\N	2013-10-11 16:49:12	100	\N	Data Import	\N	\N	Y	Month_0_Amt	\N	\N	\N	\N	Month_0_Amt	2013-10-11 16:49:12	100
56465	Month_1_Amt	0	0	\N	\N	2013-10-11 16:50:21	100	\N	Data Import	\N	\N	Y	Month_1_Amt	\N	\N	\N	\N	Month_1_Amt	2013-10-11 16:50:21	100
56466	Month_10_Amt	0	0	\N	\N	2013-10-11 16:50:53	100	\N	Data Import	\N	\N	Y	Month_10_Amt	\N	\N	\N	\N	Month_10_Amt	2013-10-11 16:50:53	100
56467	Month_11_Amt	0	0	\N	\N	2013-10-11 16:51:24	100	\N	Data Import	\N	\N	Y	Month_11_Amt	\N	\N	\N	\N	Month_11_Amt	2013-10-11 16:51:24	100
56468	Month_2_Amt	0	0	\N	\N	2013-10-11 16:51:59	100	\N	Data Import	\N	\N	Y	Month_2_Amt	\N	\N	\N	\N	Month_2_Amt	2013-10-11 16:51:59	100
56469	Month_3_Amt	0	0	\N	\N	2013-10-11 16:52:26	100	\N	Data Import	\N	\N	Y	Month_3_Amt	\N	\N	\N	\N	Month_3_Amt	2013-10-11 16:52:26	100
56470	Month_4_Amt	0	0	\N	\N	2013-10-11 16:53:02	100	\N	Data Import	\N	\N	Y	Month_4_Amt	\N	\N	\N	\N	Month_4_Amt	2013-10-11 16:53:02	100
56471	Month_5_Amt	0	0	\N	\N	2013-10-11 16:53:39	100	\N	Data Import	\N	\N	Y	Month_5_Amt	\N	\N	\N	\N	Month_5_Amt	2013-10-11 16:53:39	100
56472	Month_6_Amt	0	0	\N	\N	2013-10-11 16:54:07	100	\N	Data Import	\N	\N	Y	Month_6_Amt	\N	\N	\N	\N	Month_6_Amt	2013-10-11 16:54:07	100
56473	Month_7_Amt	0	0	\N	\N	2013-10-11 16:54:32	100	\N	Data Import	\N	\N	Y	Month_7_Amt	\N	\N	\N	\N	Month_7_Amt	2013-10-11 16:54:32	100
56474	Month_8_Amt	0	0	\N	\N	2013-10-11 16:54:59	100	\N	Data Import	\N	\N	Y	Month_8_Amt	\N	\N	\N	\N	Month_8_Amt	2013-10-11 16:54:59	100
56475	Month_9_Amt	0	0	\N	\N	2013-10-11 16:55:27	100	\N	Data Import	\N	\N	Y	Month_9_Amt	\N	\N	\N	\N	Month_9_Amt	2013-10-11 16:55:27	100
56476	No_Of_Periods	0	0	\N	\N	2013-10-11 16:59:03	100	\N	Data Import	\N	\N	Y	No_Of_Periods	\N	\N	\N	\N	No_Of_Periods	2013-10-11 16:59:03	100
56477	SalesRegionValue	0	0	\N	\N	2013-10-11 17:03:56	100	\N	Data Import	\N	\N	Y	Sales Region Value	\N	\N	\N	\N	Sales Region Value	2013-10-11 17:03:56	100
57238	C_DocType_Payment_ID	0	0	\N	\N	2014-06-26 10:38:40	100	\N	Data Import	\N	\N	Y	Payment Document Type	\N	\N	\N	\N	Payment Document Type	2014-06-26 10:38:40	100
57239	IsChargeOnPayment	0	0	\N	\N	2014-06-26 10:41:00	100	\N	Data Import	\N	\N	Y	Charge On Payment	\N	\N	\N	\N	Charge On Payment	2014-06-26 10:41:00	100
57240	Imp_Invoice	0	0	\N	\N	2014-06-26 10:43:43	100	\N	Data Import	\N	\N	Y	Import Invoice From Statement	\N	\N	\N	\N	Import Invoice From Statement	2014-06-26 10:43:43	100
57241	Imp_Payment	0	0	\N	\N	2014-06-26 10:45:48	100	\N	Data Import	\N	\N	Y	Import Payment From Statement	\N	\N	\N	\N	Import Payment From Statement	2014-06-26 10:45:48	100
57244	SimulateImport	0	0	\N	\N	2014-07-04 13:21:48	100	\N	Data Import	\N	\N	Y	Simulate Import	\N	\N	\N	\N	Simulate Import	2014-07-04 13:21:48	100
57481	Month_0_Qty	0	0	\N	\N	2014-10-06 11:01:26	0	\N	Data Import	\N	\N	Y	Month_0_Qty	\N	\N	\N	\N	Month_0_Qty	2014-10-06 11:01:26	0
57482	Month_1_Qty	0	0	\N	\N	2014-10-06 11:01:47	0	\N	Data Import	\N	\N	Y	Month_1_Qty	\N	\N	\N	\N	Month_1_Qty	2014-10-06 11:01:47	0
57483	Month_2_Qty	0	0	\N	\N	2014-10-06 11:01:56	0	\N	Data Import	\N	\N	Y	Month_2_Qty	\N	\N	\N	\N	Month_2_Qty	2014-10-06 11:01:56	0
57484	Month_3_Qty	0	0	\N	\N	2014-10-06 11:02:05	0	\N	Data Import	\N	\N	Y	Month_3_Qty	\N	\N	\N	\N	Month_3_Qty	2014-10-06 11:02:05	0
57485	Month_4_Qty	0	0	\N	\N	2014-10-06 11:02:14	0	\N	Data Import	\N	\N	Y	Month_4_Qty	\N	\N	\N	\N	Month_4_Qty	2014-10-06 11:02:14	0
57486	Month_5_Qty	0	0	\N	\N	2014-10-06 11:02:22	0	\N	Data Import	\N	\N	Y	Month_5_Qty	\N	\N	\N	\N	Month_5_Qty	2014-10-06 11:02:22	0
57487	Month_6_Qty	0	0	\N	\N	2014-10-06 11:02:30	0	\N	Data Import	\N	\N	Y	Month_6_Qty	\N	\N	\N	\N	Month_6_Qty	2014-10-06 11:02:30	0
57488	Month_7_Qty	0	0	\N	\N	2014-10-06 11:02:39	0	\N	Data Import	\N	\N	Y	Month_7_Qty	\N	\N	\N	\N	Month_7_Qty	2014-10-06 11:02:39	0
57489	Month_8_Qty	0	0	\N	\N	2014-10-06 11:02:48	0	\N	Data Import	\N	\N	Y	Month_8_Qty	\N	\N	\N	\N	Month_8_Qty	2014-10-06 11:02:48	0
57490	Month_9_Qty	0	0	\N	\N	2014-10-06 11:03:02	0	\N	Data Import	\N	\N	Y	Month_9_Qty	\N	\N	\N	\N	Month_9_Qty	2014-10-06 11:03:02	0
57491	Month_10_Qty	0	0	\N	\N	2014-10-06 11:03:10	0	\N	Data Import	\N	\N	Y	Month_10_Qty	\N	\N	\N	\N	Month_10_Qty	2014-10-06 11:03:10	0
57492	Month_11_Qty	0	0	\N	\N	2014-10-06 11:03:18	0	\N	Data Import	\N	\N	Y	Month_11_Qty	\N	\N	\N	\N	Month_11_Qty	2014-10-06 11:03:18	0
57493	Jnl_Line_Description	0	0	\N	\N	2014-10-06 11:36:59	0	\N	Data Import	\N	\N	Y	Jnl Line Description	\N	\N	\N	\N	Jnl Line Description	2014-10-06 11:36:59	0
57907	OrderDocumentNo	0	0	\N	\N	2015-03-16 11:24:10	100	\N	Data Import	\N	\N	Y	Order Document No	\N	\N	\N	\N	Order Document No	2015-03-16 11:24:10	100
57991	ShipAddress1	0	0	\N	\N	2015-05-14 12:50:48	100	\N	Data Import	\N	\N	Y	Ship Address 1	\N	\N	\N	\N	Ship Address 1	2015-05-14 12:50:48	100
57992	ShipAddress2	0	0	\N	\N	2015-05-14 12:51:34	100	\N	Data Import	\N	\N	Y	Ship Address 2	\N	\N	\N	\N	Ship Address 2	2015-05-14 12:51:34	100
57993	ShipCity	0	0	\N	\N	2015-05-14 12:52:23	100	\N	Data Import	\N	\N	Y	Ship City	\N	\N	\N	\N	Ship City	2015-05-14 12:52:23	100
57994	ShipPostCode	0	0	\N	\N	2015-05-14 12:53:28	100	\N	Data Import	\N	\N	Y	Ship PostCode	\N	\N	\N	\N	Ship PostCode	2015-05-14 12:53:28	100
57995	ShipRegionName	0	0	\N	\N	2015-05-14 12:54:28	100	\N	Data Import	\N	\N	Y	Ship Region	\N	\N	\N	\N	Ship Region	2015-05-14 12:54:28	100
57996	Ship_C_Region_ID	0	0	\N	\N	2015-05-14 12:55:56	100	\N	Data Import	\N	\N	Y	Ship Region	\N	\N	\N	\N	Ship Region	2015-05-14 12:55:56	100
57997	ShipCountryCode	0	0	\N	\N	2015-05-14 12:57:21	100	\N	Data Import	\N	\N	Y	Ship Country	\N	\N	\N	\N	Ship Country	2015-05-14 12:57:21	100
57998	Ship_C_Country_ID	0	0	\N	\N	2015-05-14 12:58:20	100	\N	Data Import	\N	\N	Y	Ship Country	\N	\N	\N	\N	Ship Country	2015-05-14 12:58:20	100
58002	ShipContactName	0	0	\N	\N	2015-05-19 13:15:03	100	\N	Data Import	\N	\N	Y	Ship Contact Name	\N	\N	\N	\N	Ship Contact Name	2015-05-19 13:15:03	100
58003	ShipAD_User_ID	0	0	\N	\N	2015-05-19 13:16:10	100	\N	Data Import	\N	\N	Y	Ship User/Contact	\N	\N	\N	\N	Ship User/Contact	2015-05-19 13:16:10	100
58009	ShipAddress3	0	0	\N	\N	2015-06-03 18:12:11	100	\N	Data Import	\N	\N	Y	Ship Address 3	\N	\N	\N	\N	Ship Address 3	2015-06-03 18:12:11	100
58010	ShipAddress4	0	0	\N	\N	2015-06-03 18:12:20	100	\N	Data Import	\N	\N	Y	Ship Address 4	\N	\N	\N	\N	Ship Address 4	2015-06-03 18:12:20	100
58315	BPGroupName	0	0	\N	\N	2015-09-18 18:20:33	100	\N	Data Import	\N	\N	Y	BP Group Name	\N	\N	\N	\N	BP Group Name	2015-09-18 18:20:33	100
58337	Ext_Category_ID	0	0	\N	\N	2015-10-16 14:06:04	100	\N	Data Import	\N	\N	Y	Ext Category ID	\N	\N	\N	\N	Ext Category ID	2015-10-16 14:06:04	100
58338	Ext_Product_BOM_ID	0	0	\N	\N	2015-10-19 10:45:59	100	This ID stores 3rd Party Application BOM/Bundle Product ID	Data Import	\N	\N	Y	External Product BOM ID	\N	\N	\N	\N	External Product BOM ID	2015-10-19 10:45:59	100
59051	I_Column_ID	0	0	\N	\N	2016-12-02 14:24:45	100	\N	Data Import	\N	\N	Y	Import Column ID	\N	\N	\N	\N	Import Column ID	2016-12-02 14:24:45	100
59052	ReferenceName	0	0	10	\N	2016-12-02 15:53:01	100	System Reference and Validation Name	Data Import	60	The Reference could be a display type, list or table validation.	Y	Reference Name	\N	\N	\N	\N	Reference Name	2016-12-02 15:53:01	100
59053	ReferenceValueName	0	0	10	\N	2016-12-02 15:56:41	100	Required to specify, if data type is Table or List	Data Import	22	The Reference Value indicates where the reference values are stored.  It must be specified if the data type is Table or List.  	Y	Reference Key Name	\N	\N	\N	\N	Reference Key Name	2016-12-02 15:56:41	100
59054	SeqNoField	0	0	11	\N	2016-12-07 15:55:35	100	Method of ordering records; lowest number comes first	Data Import	22	The Sequence indicates the order of columns in form view	Y	Field Sequence	\N	\N	\N	\N	Field Sequence	2016-12-07 15:55:35	100
59055	WindowName	0	0	10	\N	2016-12-07 16:15:38	100	Data entry or display window name	Data Import	60	The Window name field identifies a Window in the system.	Y	Window Name	\N	\N	\N	\N	Window Name	2016-12-07 16:15:38	100
59056	TabName	0	0	10	\N	2016-12-07 16:16:19	100	Tab Name within a Window	Data Import	60	The Tab indicates a tab that displays within a window.	Y	Tab Name	\N	\N	\N	\N	Tab Name	2016-12-07 16:16:19	100
59057	FieldGroupName	0	0	10	\N	2016-12-07 16:17:04	100	Logical grouping of fields	Data Import	60	The Field Group indicates the logical group that this field belongs to (History, Amounts, Quantities)	Y	Field Group Name	\N	\N	\N	\N	Field Group Name	2016-12-07 16:17:04	100
1000028	IsDeleteImported	0	0	\N	\N	2016-04-12 09:52:20	0	\N	Data Import	\N	\N	Y	IsDeleteImported	\N	\N	\N	\N	IsDeleteImported	2016-04-12 09:52:20	0
\.
