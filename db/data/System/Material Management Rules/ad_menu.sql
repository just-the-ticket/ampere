copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
107	Unit of Measure	W	0	\N	\N	0	\N	120	1999-06-11 00:00:00	0	Maintain Unit of Measure 	Material Management Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
125	Warehouse & Locators	W	0	\N	\N	0	\N	139	1999-08-09 00:00:00	0	Maintain Warehouses and Locators	Material Management Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
126	Product	W	0	\N	\N	0	\N	140	1999-08-09 00:00:00	0	Maintain Products	Material Management Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
128	Shipper	W	0	\N	\N	0	\N	142	1999-08-09 00:00:00	0	Maintain Shippers	Material Management Rules	Y	Y	N	N	N	2005-07-24 13:08:47	100
130	Product Category	W	0	\N	\N	0	\N	144	1999-08-09 00:00:00	0	Maintain Product Categories	Material Management Rules	Y	Y	N	N	N	2005-07-24 13:07:48	100
132	Price List	W	0	\N	\N	0	\N	146	1999-08-09 00:00:00	0	Maintain Product Price Lists	Material Management Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
167	Inventory Setup	\N	0	\N	\N	0	\N	\N	1999-12-02 13:24:22	0	\N	Material Management Rules	Y	N	N	Y	Y	2022-02-27 23:59:00	100
187	Perpetual Inventory	W	0	\N	\N	0	\N	175	2000-01-25 10:23:08	0	Maintain Perpetual Inventory Rules	Material Management Rules	N	Y	N	N	N	2019-12-05 14:01:41	100
188	Vendor Details	W	0	\N	\N	0	\N	176	2000-01-25 10:57:58	0	Maintain Vendor Details	Material Management Rules	N	Y	N	N	N	2019-12-05 14:10:25	100
227	Vendor Selection	R	0	\N	\N	0	115	\N	2000-10-12 22:31:13	0	Products with more than one vendor	Material Management Rules	N	Y	N	N	N	2019-12-05 14:00:50	100
310	Discount Schema	W	0	\N	\N	0	\N	233	2001-12-28 21:33:27	0	Maintain Trade Discount Schema	Material Management Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
381	Freight Category	W	0	\N	\N	0	\N	282	2003-06-07 21:51:15	0	Maintain Freight Categories	Material Management Rules	N	Y	N	N	N	2019-12-05 14:10:34	100
421	Product Organisation	P	0	\N	\N	0	245	\N	2003-12-23 23:52:01	0	Set and verify Organisation ownership of Products	Material Management Rules	Y	Y	N	N	N	2016-04-11 16:07:05	0
422	Warehouse Organisation	P	0	\N	\N	0	244	\N	2003-12-23 23:52:21	0	Set and verify Organisation ownership of Warehouse	Material Management Rules	Y	Y	N	N	N	2016-04-11 16:07:05	0
490	Price List Schema	W	0	\N	\N	0	\N	337	2001-12-28 21:01:25	0	Maintain Price List Schema	Material Management Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
534	Product BOM	W	0	\N	\N	0	\N	354	2005-05-15 15:43:20	100	Maintain Product Bill of Materials	Material Management Rules	N	Y	N	N	N	2008-05-11 16:57:44	0
585	Verify BOMs	P	0	\N	\N	0	346	\N	2006-04-25 19:23:01	100	Verify BOM Structures	Material Management Rules	N	Y	N	N	N	2019-12-05 14:01:16	100
53210	Promotion Group	W	0	\N	\N	0	\N	53073	2009-04-10 10:55:48	100	Grouping of product for promotion setup	Material Management Rules	N	Y	N	Y	N	2019-12-05 14:10:54	100
53211	Promotion	W	0	\N	\N	0	\N	53074	2009-04-10 10:56:44	100	Setup promotion rule	Material Management Rules	N	Y	N	Y	N	2019-12-05 14:10:50	100
53965	Warehouse Info	X	0	53052	\N	0	\N	\N	2015-03-18 13:37:44	100	\N	Material Management Rules	Y	Y	N	N	N	2015-03-18 13:37:44	100
\.
