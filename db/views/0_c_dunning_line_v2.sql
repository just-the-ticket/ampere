CREATE OR REPLACE VIEW c_dunning_line_v2 AS 
 SELECT d.c_dunningrunentry_id,
    d.datetrx,
    d.documentno,
    d.amt,
    d.paydate,
    d.allocatedamt,
    d.balance,
    d.ad_client_id,
    d.ad_org_id,
    d.isactive,
    d.created,
    d.createdby,
    d.updated,
    d.updatedby
   FROM c_dunning_line_mv d
  ORDER BY d.paydate, d.datetrx, d.updated;