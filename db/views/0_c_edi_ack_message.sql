CREATE OR REPLACE VIEW c_edi_ack_message AS 
 SELECT m.action,
    m.ad_client_id,
    m.ad_org_id,
    m.ad_table_id,
    m.c_edi_interchange_id,
    m.c_edi_message_id,
    m.created,
    m.createdby,
    m.errormsg,
    m.isactive,
    m.messagereferenceno,
    m.messagetype,
    m.record_id,
    m.updated,
    m.updatedby,
    a.c_edi_ack_id
   FROM ((c_edi_message m
     LEFT JOIN c_edi_interchange i ON ((i.c_edi_interchange_id = m.c_edi_interchange_id)))
     LEFT JOIN c_edi_ack a ON ((a.c_edi_interchange_id = m.c_edi_interchange_id)));