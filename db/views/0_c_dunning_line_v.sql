CREATE OR REPLACE VIEW c_dunning_line_v AS 
 SELECT drl.ad_client_id,
    drl.ad_org_id,
    drl.isactive,
    drl.created,
    drl.createdby,
    COALESCE(a.updated, drl.updated) AS updated,
    drl.updatedby,
    'en_US'::character varying(5) AS ad_language,
    drl.c_dunningrunline_id,
    drl.c_dunningrunentry_id,
    drl.amt,
    drl.convertedamt,
    drl.daysdue,
    drl.timesdunned,
    drl.interestamt,
    drl.feeamt,
    COALESCE(a.overunderamt, drl.totalamt) AS totalamt,
    drl.c_invoice_id,
    COALESCE(i.issotrx, p.isreceipt) AS issotrx,
    COALESCE(i.documentno, p.documentno) AS documentno,
    COALESCE(i.docstatus, p.docstatus) AS docstatus,
    COALESCE(i.dateinvoiced, p.datetrx) AS datetrx,
    paymenttermduedate(i.c_paymentterm_id, (i.dateinvoiced)::timestamp with time zone) AS duedate,
    COALESCE(i.c_doctype_id, p.c_doctype_id) AS c_doctype_id,
    COALESCE(dt.printname, dtp.printname) AS documenttype,
    COALESCE(i.description, p.description) AS description,
    COALESCE(i.c_currency_id, p.c_currency_id) AS c_currency_id,
    COALESCE(i.c_campaign_id, p.c_campaign_id) AS c_campaign_id,
    COALESCE(i.c_project_id, p.c_project_id) AS c_project_id,
    COALESCE(i.c_activity_id, p.c_activity_id) AS c_activity_id,
    COALESCE(i.user1_id, p.user1_id) AS user1_id,
    COALESCE(i.user2_id, p.user2_id) AS user2_id,
    COALESCE(i.dateacct, p.dateacct) AS dateacct,
    COALESCE(i.c_conversiontype_id, i.c_conversiontype_id) AS c_conversiontype_id,
    COALESCE(i.ad_orgtrx_id, p.ad_orgtrx_id) AS ad_orgtrx_id,
    i.poreference,
    i.dateordered,
    to_char(i.dateinvoiced, 'dd-Mon-YY'::text) AS dateinvoiced,
    i.isindispute,
    pt.name AS paymentterm,
    i.c_charge_id,
    i.chargeamt,
    i.totallines,
    i.grandtotal,
    i.grandtotal AS amtinwords,
    i.m_pricelist_id,
        CASE
            WHEN (i.ispaid = 'Y'::bpchar) THEN 'paid'::text
            ELSE 'due'::text
        END AS ispaid,
    p.isallocated,
    p.tendertype,
    p.discountamt,
    COALESCE(p.dateacct, p.datetrx) AS paydate,
        CASE
            WHEN ((h.docstatus = 'CO'::bpchar) AND (p.isallocated = 'Y'::bpchar)) THEN a.amount
            ELSE NULL::numeric
        END AS allocatedamt,
    p.payamt AS paidamt,
    p.documentno AS paymentdocumentno
   FROM (((((((((c_dunningrunline drl
     JOIN c_dunningrunentry dre ON ((drl.c_dunningrunentry_id = dre.c_dunningrunentry_id)))
     JOIN c_dunningrun dr ON ((dr.c_dunningrun_id = dre.c_dunningrun_id)))
     LEFT JOIN c_invoice i ON ((drl.c_invoice_id = i.c_invoice_id)))
     LEFT JOIN c_doctype dt ON ((i.c_doctype_id = dt.c_doctype_id)))
     LEFT JOIN c_paymentterm pt ON ((i.c_paymentterm_id = pt.c_paymentterm_id)))
     LEFT JOIN c_allocationline a ON ((a.c_invoice_id = i.c_invoice_id)))
     LEFT JOIN c_allocationhdr h ON ((a.c_allocationhdr_id = h.c_allocationhdr_id)))
     LEFT JOIN c_payment p ON ((a.c_payment_id = p.c_payment_id)))
     LEFT JOIN c_doctype dtp ON ((p.c_doctype_id = dtp.c_doctype_id)));