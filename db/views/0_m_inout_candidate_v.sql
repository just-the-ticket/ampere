CREATE OR REPLACE VIEW m_inout_candidate_v AS 
 SELECT o.ad_client_id,
    o.ad_org_id,
    o.c_bpartner_id,
    o.c_order_id,
    o.documentno,
    o.dateordered,
    o.c_doctype_id,
    o.poreference,
    o.description,
    o.salesrep_id,
    l.m_warehouse_id,
    sum(((l.qtyordered - l.qtydelivered) * l.priceactual)) AS totallines,
    min(
        CASE
            WHEN (l.c_charge_id IS NOT NULL) THEN 'Y'::text
            WHEN (((s.qtyonhand - l.qtyordered) + l.qtydelivered) >= (0)::numeric) THEN 'Y'::text
            ELSE 'N'::text
        END) AS isgoodtoship,
    o.datepromised
   FROM ((c_order o
     JOIN c_orderline l ON ((o.c_order_id = l.c_order_id)))
     LEFT JOIN ( SELECT lc.m_warehouse_id,
            st.m_product_id,
            sum(st.qtyonhand) AS qtyonhand
           FROM (m_storage st
             JOIN m_locator lc ON ((st.m_locator_id = lc.m_locator_id)))
          GROUP BY lc.m_warehouse_id, st.m_product_id) s ON (((s.m_product_id = l.m_product_id) AND (o.m_warehouse_id = s.m_warehouse_id))))
  WHERE (((o.docstatus)::bpchar = 'CO'::bpchar) AND (o.isdelivered = 'N'::bpchar) AND (o.c_doctype_id IN ( SELECT c_doctype.c_doctype_id
           FROM c_doctype
          WHERE ((c_doctype.docbasetype = 'SOO'::bpchar) AND (c_doctype.docsubtypeso <> ALL (ARRAY['ON'::bpchar, 'OB'::bpchar, 'WR'::bpchar]))))) AND (o.deliveryrule <> 'M'::bpchar) AND ((l.m_product_id IS NULL) OR (EXISTS ( SELECT 1
           FROM m_product p
          WHERE ((l.m_product_id = p.m_product_id) AND (p.isexcludeautodelivery = 'N'::bpchar))))) AND (l.qtyordered <> l.qtydelivered) AND ((l.m_product_id IS NOT NULL) OR (l.c_charge_id IS NOT NULL)) AND (NOT (EXISTS ( SELECT 1
           FROM (m_inoutline iol
             JOIN m_inout io ON ((iol.m_inout_id = io.m_inout_id)))
          WHERE ((iol.c_orderline_id = l.c_orderline_id) AND (io.docstatus = ANY (ARRAY['IP'::bpchar, 'WC'::bpchar])))))))
  GROUP BY o.ad_client_id, o.ad_org_id, o.c_bpartner_id, o.c_order_id, o.documentno, o.dateordered, o.c_doctype_id, o.poreference, o.description, o.salesrep_id, l.m_warehouse_id;